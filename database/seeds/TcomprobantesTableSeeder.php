<?php

use Illuminate\Database\Seeder;

class TcomprobantesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('tcomprobantes')->delete();
        
        \DB::table('tcomprobantes')->insert(array (
            0 => 
            array (
                'id' => 1,
                'nombre' => 'Ingreso',
                'created_at' => '2017-05-18 10:50:31',
                'updated_at' => '2017-05-18 10:50:31',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'nombre' => 'Factura',
                'created_at' => '2017-05-18 10:50:31',
                'updated_at' => '2017-07-18 10:53:05',
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'nombre' => 'Regalo',
                'created_at' => '2017-05-18 10:50:31',
                'updated_at' => '2017-05-18 10:50:31',
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'nombre' => 'Otros',
                'created_at' => '2018-07-27 16:20:19',
                'updated_at' => '2018-07-27 16:21:56',
                'deleted_at' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'nombre' => 'Traslado',
                'created_at' => '2018-07-27 17:19:03',
                'updated_at' => '2018-07-27 17:20:14',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}