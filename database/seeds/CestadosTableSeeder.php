<?php

use Illuminate\Database\Seeder;

class CestadosTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('cestados')->delete();
        
        \DB::table('cestados')->insert(array (
            0 => 
            array (
                'id' => 1,
                'descripcion' => 'CREADA',
                'created_at' => '2017-05-18 10:50:31',
                'updated_at' => '2018-08-17 10:06:32',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'descripcion' => 'ANULADA',
                'created_at' => '2017-05-18 10:50:31',
                'updated_at' => '2018-08-17 10:06:44',
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'descripcion' => 'RECIBIDA',
                'created_at' => '2018-08-17 10:01:05',
                'updated_at' => '2018-08-17 10:03:27',
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'descripcion' => 'CANCELADA',
                'created_at' => '2018-08-17 10:03:49',
                'updated_at' => '2018-08-17 10:03:49',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}