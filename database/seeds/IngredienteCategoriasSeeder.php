<?php

use Illuminate\Database\Seeder;

class IngredienteCategoriasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\Models\IngredienteCategoria::class,1)->create(['nombre' => "Carne"]);
        factory(\App\Models\IngredienteCategoria::class,1)->create(['nombre' => "Especias"]);
        factory(\App\Models\IngredienteCategoria::class,1)->create(['nombre' => "Platano"]);
    }
}
