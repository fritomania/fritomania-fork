<?php

use Illuminate\Database\Seeder;

class CombosTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('combos')->delete();
        
        \DB::table('combos')->insert(array (
            0 => 
            array (
                'id' => 1,
                'item_id' => 12,
                'piezas' => 12,
                'bebidas' => 2,
                'created_at' => '2018-09-15 14:47:34',
                'updated_at' => '2018-09-15 14:47:34',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'item_id' => 13,
                'piezas' => 22,
                'bebidas' => 1,
                'created_at' => '2018-09-15 15:04:46',
                'updated_at' => '2018-09-15 15:04:46',
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'item_id' => 14,
                'piezas' => 2,
                'bebidas' => 1,
                'created_at' => '2018-09-15 16:37:27',
                'updated_at' => '2018-09-15 16:37:27',
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'item_id' => 15,
                'piezas' => 1,
                'bebidas' => 1,
                'created_at' => '2018-09-15 16:38:04',
                'updated_at' => '2018-09-15 16:38:04',
                'deleted_at' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'item_id' => 16,
                'piezas' => 1,
                'bebidas' => 1,
                'created_at' => '2018-09-15 16:40:19',
                'updated_at' => '2018-09-15 16:40:19',
                'deleted_at' => NULL,
            ),
            5 => 
            array (
                'id' => 6,
                'item_id' => 17,
                'piezas' => 1,
                'bebidas' => 1,
                'created_at' => '2018-09-15 16:47:16',
                'updated_at' => '2018-09-15 16:47:16',
                'deleted_at' => NULL,
            ),
            6 => 
            array (
                'id' => 7,
                'item_id' => 23,
                'piezas' => 7,
                'bebidas' => 2,
                'created_at' => '2018-09-15 16:50:41',
                'updated_at' => '2018-09-15 16:50:41',
                'deleted_at' => NULL,
            ),
            7 => 
            array (
                'id' => 8,
                'item_id' => 66,
                'piezas' => 2,
                'bebidas' => 2,
                'created_at' => '2018-09-15 16:52:38',
                'updated_at' => '2018-09-15 16:52:38',
                'deleted_at' => NULL,
            ),
            8 => 
            array (
                'id' => 9,
                'item_id' => 67,
                'piezas' => 2,
                'bebidas' => 2,
                'created_at' => '2018-09-15 20:43:57',
                'updated_at' => '2018-09-15 20:43:57',
                'deleted_at' => NULL,
            ),
            9 => 
            array (
                'id' => 10,
                'item_id' => 68,
                'piezas' => 2,
                'bebidas' => 2,
                'created_at' => '2018-09-15 20:44:42',
                'updated_at' => '2018-09-15 20:44:42',
                'deleted_at' => NULL,
            ),
            10 => 
            array (
                'id' => 11,
                'item_id' => 69,
                'piezas' => 2,
                'bebidas' => 2,
                'created_at' => '2018-09-15 20:45:11',
                'updated_at' => '2018-09-15 20:45:11',
                'deleted_at' => NULL,
            ),
            11 => 
            array (
                'id' => 12,
                'item_id' => 70,
                'piezas' => 2,
                'bebidas' => 1,
                'created_at' => '2018-09-15 20:45:37',
                'updated_at' => '2018-09-15 20:45:37',
                'deleted_at' => NULL,
            ),
            12 => 
            array (
                'id' => 13,
                'item_id' => 71,
                'piezas' => 2,
                'bebidas' => 2,
                'created_at' => '2018-09-15 20:46:09',
                'updated_at' => '2018-09-15 20:46:09',
                'deleted_at' => NULL,
            ),
            13 => 
            array (
                'id' => 14,
                'item_id' => 3,
                'piezas' => 2,
                'bebidas' => 0,
                'created_at' => '2018-12-31 11:16:00',
                'updated_at' => '2018-12-31 11:16:00',
                'deleted_at' => NULL,
            ),
            14 => 
            array (
                'id' => 15,
                'item_id' => 2,
                'piezas' => 100,
                'bebidas' => 0,
                'created_at' => '2019-03-05 13:04:05',
                'updated_at' => '2019-03-05 13:04:05',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}