<?php

use Illuminate\Database\Seeder;

class PestadosTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('pestados')->delete();
        
        \DB::table('pestados')->insert(array (
            0 => 
            array (
                'id' => 1,
                'descripcion' => 'Solicitado',
                'created_at' => '2017-07-13 10:25:34',
                'updated_at' => '2017-07-13 10:25:34',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'descripcion' => 'Ingresado',
                'created_at' => '2017-07-13 10:25:43',
                'updated_at' => '2017-07-13 10:25:43',
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'descripcion' => 'Entregado',
                'created_at' => '2017-07-13 10:25:53',
                'updated_at' => '2017-07-13 10:25:53',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}