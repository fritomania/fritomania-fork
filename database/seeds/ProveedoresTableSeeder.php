<?php

use Illuminate\Database\Seeder;

class ProveedoresTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('proveedores')->delete();


        factory(\App\Models\Proveedor::class,1)->create([
            'nit' => '00000',
            'nombre' => 'Proveedor de prueba 1',
            'razon_social' => 'Proveedor de prueba 1 S.A',
            'correo' => 'info@itsb.cl',
        ]);

        factory(\App\Models\Proveedor::class,1)->create([
            'nit' => '00000',
            'nombre' => 'Proveedor de prueba 2',
            'razon_social' => 'Proveedor de prueba 2 S.A',
            'correo' => 'info@itsb.cl',
        ]);

        factory(\App\Models\Proveedor::class,1)->create([
            'nit' => '00000',
            'nombre' => 'Proveedor de prueba 3',
            'razon_social' => 'Proveedor de prueba 3 S.A',
            'correo' => 'info@itsb.cl',
        ]);
        
    }
}