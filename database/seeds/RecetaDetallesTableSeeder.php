<?php

use Illuminate\Database\Seeder;

class RecetaDetallesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('receta_detalles')->delete();
        
        \DB::table('receta_detalles')->insert(array (
            0 => 
            array (
                'id' => 1,
                'item_id' => 45,
                'receta_id' => 1,
                'cantidad' => '1.00',
                'created_at' => '2018-08-27 11:06:29',
                'updated_at' => '2018-08-27 11:06:29',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'item_id' => 34,
                'receta_id' => 1,
                'cantidad' => '2.00',
                'created_at' => '2018-08-27 11:06:29',
                'updated_at' => '2018-08-27 11:06:29',
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'item_id' => 45,
                'receta_id' => 2,
                'cantidad' => '1.00',
                'created_at' => '2018-08-27 12:46:50',
                'updated_at' => '2018-08-27 12:46:50',
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'item_id' => 25,
                'receta_id' => 2,
                'cantidad' => '1.00',
                'created_at' => '2018-08-27 12:46:50',
                'updated_at' => '2018-08-27 12:46:50',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}