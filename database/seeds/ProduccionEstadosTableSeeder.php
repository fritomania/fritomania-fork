<?php

use Illuminate\Database\Seeder;

class ProduccionEstadosTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('produccion_estados')->delete();
        
        \DB::table('produccion_estados')->insert(array (
            0 => 
            array (
                'id' => 1,
                'nombre' => 'SOLICITADA',
                'created_at' => '2018-08-10 17:25:44',
                'updated_at' => '2018-08-10 17:25:44',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'nombre' => 'INCOMPLETA',
                'created_at' => '2018-08-10 17:25:59',
                'updated_at' => '2018-08-10 17:25:59',
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'nombre' => 'COMPLETADA',
                'created_at' => '2018-08-10 17:26:46',
                'updated_at' => '2018-08-10 17:26:46',
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'nombre' => 'CANCELADA',
                'created_at' => '2018-08-10 17:26:56',
                'updated_at' => '2018-08-10 17:26:56',
                'deleted_at' => NULL,
            ),
            4 =>
            array (
                'id' => 5,
                'nombre' => 'ANULADA',
                'created_at' => '2018-08-10 17:26:56',
                'updated_at' => '2018-08-10 17:26:56',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}