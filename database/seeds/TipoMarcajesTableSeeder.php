<?php

use Illuminate\Database\Seeder;

class TipoMarcajesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('tipo_marcajes')->delete();
        
        \DB::table('tipo_marcajes')->insert(array (
            0 => 
            array (
                'id' => 1,
                'nombre' => 'Entrada',
                'created_at' => '2018-09-05 23:06:34',
                'updated_at' => '2018-09-05 23:06:34',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'nombre' => 'Salida',
                'created_at' => '2018-09-05 23:06:44',
                'updated_at' => '2018-09-05 23:06:44',
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'nombre' => 'Inicio Almuerzo',
                'created_at' => '2018-09-05 23:07:10',
                'updated_at' => '2018-09-05 23:07:10',
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'nombre' => 'Fin Almuerzo',
                'created_at' => '2018-09-05 23:07:20',
                'updated_at' => '2018-09-05 23:07:20',
                'deleted_at' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'nombre' => 'Inicio merienda',
                'created_at' => '2018-09-05 23:08:30',
                'updated_at' => '2018-09-05 23:08:30',
                'deleted_at' => NULL,
            ),
            5 => 
            array (
                'id' => 6,
                'nombre' => 'Fin merienda',
                'created_at' => '2018-09-05 23:08:39',
                'updated_at' => '2018-09-05 23:08:39',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}