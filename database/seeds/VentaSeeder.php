<?php

use App\Models\Caja;
use App\Models\Venta;
use App\Models\VentaDetalle;
use Illuminate\Database\Seeder;

class VentaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if(!(Caja::find(1))){

            factory(Caja::class,1)->create();
        }

        factory(Venta::class,10)->create()
            ->each(function (Venta $venta){
                factory(VentaDetalle::class,random_int(5,25))->create(['venta_id' => $venta->id]);
            });
    }
}
