<?php

use Illuminate\Database\Seeder;

class MarcasTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('marcas')->delete();
        
        \DB::table('marcas')->insert(array (
            0 => 
            array (
                'id' => 1,
                'nombre' => 'MA',
                'created_at' => '2017-05-18 10:53:01',
                'updated_at' => '2017-05-18 10:53:32',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'nombre' => 'MB',
                'created_at' => '2017-05-18 10:53:11',
                'updated_at' => '2017-05-18 10:53:54',
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'nombre' => 'MC',
                'created_at' => '2017-05-18 10:53:19',
                'updated_at' => '2017-05-18 10:53:44',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}