<?php

use Illuminate\Database\Seeder;

class IcategoriasTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('icategorias')->delete();
        
        \DB::table('icategorias')->insert(array (
            0 => 
            array (
                'id' => 1,
                'nombre' => 'Producto',
                'descripcion' => NULL,
                'show_web' => 0,
                'created_at' => '2018-07-21 14:03:34',
                'updated_at' => '2018-07-21 16:27:50',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'nombre' => 'Bebidas',
                'descripcion' => '',
                'show_web' => 1,
                'created_at' => '2018-07-21 14:03:34',
                'updated_at' => '2018-07-21 14:03:34',
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'nombre' => 'Materia Prima',
                'descripcion' => NULL,
                'show_web' => 0,
                'created_at' => '2018-07-21 14:03:34',
                'updated_at' => '2018-07-21 16:27:35',
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'nombre' => 'Combos',
                'descripcion' => NULL,
                'show_web' => 1,
                'created_at' => '2018-07-21 16:28:07',
                'updated_at' => '2018-07-21 16:28:07',
                'deleted_at' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'nombre' => 'Adicionales',
                'descripcion' => NULL,
                'show_web' => 0,
                'created_at' => '2018-07-21 16:28:07',
                'updated_at' => '2018-07-21 16:28:07',
                'deleted_at' => NULL,
            ),
            5 => 
            array (
                'id' => 6,
                'nombre' => 'Salsas',
                'descripcion' => NULL,
                'show_web' => 0,
                'created_at' => '2018-07-21 16:28:07',
                'updated_at' => '2018-07-21 16:28:07',
                'deleted_at' => NULL,
            ),
            6 => 
            array (
                'id' => 7,
                'nombre' => 'Hamburguesas',
                'descripcion' => NULL,
                'show_web' => 1,
                'created_at' => '2019-01-12 09:46:39',
                'updated_at' => '2019-01-12 09:46:40',
                'deleted_at' => NULL,
            ),
            7 => 
            array (
                'id' => 8,
                'nombre' => 'Postres',
                'descripcion' => NULL,
                'show_web' => 1,
                'created_at' => '2019-01-12 09:46:53',
                'updated_at' => '2019-01-12 09:46:54',
                'deleted_at' => NULL,
            ),
            8 => 
            array (
                'id' => 9,
                'nombre' => 'Cocina',
                'descripcion' => 'Artículos que deben pasar por el proceso de cocción',
                'show_web' => 0,
                'created_at' => '2019-01-19 19:51:25',
                'updated_at' => '2019-01-19 19:51:25',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}