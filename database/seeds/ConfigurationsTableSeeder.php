<?php

use Illuminate\Database\Seeder;

class ConfigurationsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('configurations')->delete();
        
        \DB::table('configurations')->insert(array (
            0 => 
            array (
                'id' => 1,
                'key' => 'locale',
                'value' => 'es',
                'created_at' => '2017-05-21 08:41:01',
                'updated_at' => '2017-05-30 08:49:42',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'key' => 'nombre_negocio',
                'value' => 'Empresa',
                'created_at' => '2017-05-21 08:45:22',
                'updated_at' => '2017-09-05 20:44:11',
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'key' => 'dire_negocio',
                'value' => '2av 1-29 Zona 4',
                'created_at' => '2017-05-25 12:07:46',
                'updated_at' => '2017-05-25 12:07:46',
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'key' => 'muni_negocio',
                'value' => 'Sanarate',
                'created_at' => '2017-05-25 12:08:01',
                'updated_at' => '2017-05-25 12:08:01',
                'deleted_at' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'key' => 'depto_negocio',
                'value' => 'El Progreso',
                'created_at' => '2017-05-25 12:08:22',
                'updated_at' => '2017-05-25 12:08:22',
                'deleted_at' => NULL,
            ),
            5 => 
            array (
                'id' => 6,
                'key' => 'pais_negocio',
                'value' => 'Guatemala',
                'created_at' => '2017-05-25 12:08:55',
                'updated_at' => '2017-05-25 12:08:55',
                'deleted_at' => NULL,
            ),
            6 => 
            array (
                'id' => 7,
                'key' => 'mail_negocio',
                'value' => 'ejemplo@ejemplo.com',
                'created_at' => '2017-05-25 12:09:24',
                'updated_at' => '2017-05-25 12:09:24',
                'deleted_at' => NULL,
            ),
            7 => 
            array (
                'id' => 8,
                'key' => 'tel_negocio',
                'value' => '1234-5678',
                'created_at' => '2017-05-25 12:11:00',
                'updated_at' => '2017-09-05 20:44:35',
                'deleted_at' => NULL,
            ),
            8 => 
            array (
                'id' => 9,
                'key' => 'pdf_render',
                'value' => 'snappy',
                'created_at' => '2017-06-01 09:05:31',
                'updated_at' => '2017-06-01 09:05:31',
                'deleted_at' => NULL,
            ),
            9 => 
            array (
                'id' => 10,
                'key' => 'nombre_factura',
                'value' => 'clip_art',
                'created_at' => '2017-06-28 16:33:58',
                'updated_at' => '2017-06-28 16:33:58',
                'deleted_at' => NULL,
            ),
            10 => 
            array (
                'id' => 11,
                'key' => 'fpdf_pedido',
                'value' => 'jose_catalan',
                'created_at' => '2017-07-21 09:04:27',
                'updated_at' => '2017-07-21 09:04:27',
                'deleted_at' => NULL,
            ),
            11 => 
            array (
                'id' => 12,
                'key' => 'delay_fade_out_div_alert',
                'value' => '3000',
                'created_at' => '2017-08-01 20:02:05',
                'updated_at' => '2017-08-01 20:02:05',
                'deleted_at' => NULL,
            ),
            12 => 
            array (
                'id' => 13,
                'key' => 'color_skin',
                'value' => 'danger',
                'created_at' => '2017-11-11 17:29:17',
                'updated_at' => '2018-10-09 00:43:34',
                'deleted_at' => NULL,
            ),
            13 => 
            array (
                'id' => 14,
                'key' => 'view_home',
                'value' => '/dashboard',
                'created_at' => '2017-11-12 09:55:56',
                'updated_at' => '2018-04-16 19:54:37',
                'deleted_at' => NULL,
            ),
            14 => 
            array (
                'id' => 15,
                'key' => 'abrir_caja_auto',
                'value' => '0',
                'created_at' => '2018-04-16 17:38:31',
                'updated_at' => '2018-04-16 19:54:55',
                'deleted_at' => NULL,
            ),
            15 => 
            array (
                'id' => 16,
                'key' => 'timezone',
                'value' => 'America/Santiago',
                'created_at' => '2018-08-22 15:09:44',
                'updated_at' => '2018-08-23 01:22:40',
                'deleted_at' => NULL,
            ),
            16 => 
            array (
                'id' => 17,
                'key' => 'name',
                'value' => 'Fritomania',
                'created_at' => '2018-08-22 16:26:03',
                'updated_at' => '2018-08-22 16:26:03',
                'deleted_at' => NULL,
            ),
            17 => 
            array (
                'id' => 18,
                'key' => 'divisa',
                'value' => '$',
                'created_at' => '2018-08-22 19:35:15',
                'updated_at' => '2018-08-22 19:35:15',
                'deleted_at' => NULL,
            ),
            18 => 
            array (
                'id' => 19,
                'key' => 'cantidad_decimales',
                'value' => '2',
                'created_at' => '2018-08-24 18:37:07',
                'updated_at' => '2018-08-24 18:37:07',
                'deleted_at' => NULL,
            ),
            19 => 
            array (
                'id' => 20,
                'key' => 'separador_miles',
                'value' => '.',
                'created_at' => '2018-08-24 18:37:25',
                'updated_at' => '2018-08-24 18:40:22',
                'deleted_at' => NULL,
            ),
            20 => 
            array (
                'id' => 21,
                'key' => 'separador_decimal',
                'value' => ',',
                'created_at' => '2018-08-24 18:37:51',
                'updated_at' => '2018-08-24 18:37:51',
                'deleted_at' => NULL,
            ),
            21 => 
            array (
                'id' => 22,
                'key' => 'cantidad_decimales_precio',
                'value' => '0',
                'created_at' => '2018-08-28 23:42:56',
                'updated_at' => '2018-08-28 23:42:56',
                'deleted_at' => NULL,
            ),
            22 => 
            array (
                'id' => 23,
                'key' => 'dias_producto_es_nuevo',
                'value' => '31',
                'created_at' => '2018-10-09 18:35:18',
                'updated_at' => '2018-10-09 18:36:55',
                'deleted_at' => NULL,
            ),
            23 => 
            array (
                'id' => 24,
                'key' => 'mail_pruebas',
                'value' => 'altamiranoesdras@gmail.com',
                'created_at' => '2018-10-10 18:59:52',
                'updated_at' => '2018-10-10 18:59:53',
                'deleted_at' => NULL,
            ),
            24 => 
            array (
                'id' => 25,
                'key' => 'url_videos_youtube',
                'value' => 'https://www.youtube.com/embed/zpOULjyy-n8?rel=0',
                'created_at' => '2018-11-05 17:15:42',
                'updated_at' => '2018-11-05 17:15:42',
                'deleted_at' => NULL,
            ),
            25 => 
            array (
                'id' => 26,
                'key' => 'orden_auto',
                'value' => '1',
                'created_at' => '2018-12-31 11:02:23',
                'updated_at' => '2018-12-31 11:02:23',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}