<?php

use App\Models\Ingrediente;
use App\Models\IngredienteCategoria;
use Illuminate\Database\Seeder;

class IngredientesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        Storage::deleteDirectory('ingredientes');
        Storage::makeDirectory('ingredientes');

        $ingredientes = [
            IngredienteCategoria::CARNES => ['Res','Pollo','Pernil','Lomo Negro'],
            IngredienteCategoria::ESPECIAS => ['Lechuga','Palta','Repollo','Brocoly'],
            IngredienteCategoria::PLATANO => ['Platano Amarillo','Platano Verde'],
        ];

        foreach ($ingredientes as $cat => $ingrediente) {
            foreach ($ingrediente as $index => $item) {
                factory(Ingrediente::class,1)->create([
                    'ingrediente_categoria_id' => $cat,
                    'nombre' => $item
                ]);
            }
        }
    }
}
