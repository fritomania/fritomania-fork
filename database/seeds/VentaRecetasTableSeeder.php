<?php

use Illuminate\Database\Seeder;

class VentaRecetasTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('venta_recetas')->delete();
        
        \DB::table('venta_recetas')->insert(array (
            0 => 
            array (
                'id' => 1,
                'item_id' => 72,
                'activa' => 1,
                'nombre' => 'default',
                'created_at' => '2018-12-27 16:18:44',
                'updated_at' => '2018-12-27 16:18:44',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'item_id' => 73,
                'activa' => 1,
                'nombre' => 'default',
                'created_at' => '2019-01-02 20:11:40',
                'updated_at' => '2019-01-02 20:11:40',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}