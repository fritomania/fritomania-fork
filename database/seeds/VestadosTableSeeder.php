<?php

use Illuminate\Database\Seeder;

class VestadosTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('vestados')->delete();
        
        \DB::table('vestados')->insert(array (
            0 => 
            array (
                'id' => 1,
                'descripcion' => 'Pagada',
                'created_at' => '2017-05-18 10:50:31',
                'updated_at' => '2018-08-15 17:41:24',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'descripcion' => 'Anulada',
                'created_at' => '2017-05-18 10:50:31',
                'updated_at' => '2017-05-18 10:50:31',
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'descripcion' => 'Enviada a Cocina',
                'created_at' => '2018-08-15 17:43:21',
                'updated_at' => '2018-08-15 17:45:11',
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'descripcion' => 'Cocinando',
                'created_at' => '2018-08-15 17:43:36',
                'updated_at' => '2018-08-15 17:43:36',
                'deleted_at' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'descripcion' => 'Lista',
                'created_at' => '2018-08-15 17:43:48',
                'updated_at' => '2018-08-15 17:43:48',
                'deleted_at' => NULL,
            ),
            5 => 
            array (
                'id' => 6,
                'descripcion' => 'En Ruta',
                'created_at' => '2018-08-15 17:44:21',
                'updated_at' => '2018-08-15 17:44:21',
                'deleted_at' => NULL,
            ),
            6 => 
            array (
                'id' => 7,
                'descripcion' => 'Entregada',
                'created_at' => '2018-08-15 17:44:47',
                'updated_at' => '2018-08-15 17:44:47',
                'deleted_at' => NULL,
            ),
            7 => 
            array (
                'id' => 8,
                'descripcion' => 'Temporal',
                'created_at' => '2018-09-18 08:22:36',
                'updated_at' => '2018-09-18 08:22:36',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}