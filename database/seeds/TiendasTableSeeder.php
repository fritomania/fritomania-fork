<?php

use App\Models\Tienda;
use Illuminate\Database\Seeder;

class TiendasTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {

        factory(Tienda::class,1)->create([
            'nombre' => 'Materia Prima (Fabrica)',
            'direccion' => 'Pedro León Ugalde 1386, Santiago, Chile',
            'telefono' => '+56(9) 9954-3479',
        ]);
        factory(Tienda::class,1)->create([
            'nombre' => 'Producción (Fabrica)',
            'direccion' => 'Pedro León Ugalde 1386, Santiago, Chile',
            'telefono' => '+56(9) 9954-3479',
        ]);
        factory(Tienda::class,1)->create([
            'nombre' => 'Cocina Producción (Fabrica)',
            'direccion' => 'Pedro León Ugalde 1386, Santiago, Chile',
            'telefono' => '+56(9) 9954-3479',
        ]);
        factory(Tienda::class,1)->create([
            'nombre' => 'Sta Zita 9111, Las Condes',
            'direccion' => 'Las Condes Sta Zita 9111',
            'telefono' => '1-649-257-6976',
            'id_map' => 'ChIJuQ5Jz_rPYpYRz7dc1aKK20g'
        ]);
        factory(Tienda::class,1)->create([
            'nombre' => 'José Pedro Alessandri',
            'direccion' => '3984 Clint Centers Schuppebury, VA 83550-8756',
            'telefono' => '553.542.8069',
            'id_map' => 'ChIJ-dPNIl3PYpYR3jR43SOxI2Y'
        ]);
        factory(Tienda::class,1)->create([
            'nombre' => 'Sta. Rosa 83',
            'direccion' => '88681 Schiller Drives Suite 987 West Coby, CO 11454-4429',
            'telefono' => '449-396-6535',
            'id_map' => 'ChIJozi5BqDFYpYRAI1wTxj8vNs '
        ]);

    }
}