<?php

use Illuminate\Database\Seeder;

class ItemSalsasTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('item_salsas')->delete();
        
        \DB::table('item_salsas')->insert(array (
            0 => 
            array (
                'item' => 72,
                'salsa' => 86,
            ),
            1 => 
            array (
                'item' => 73,
                'salsa' => 87,
            ),
            2 => 
            array (
                'item' => 72,
                'salsa' => 88,
            ),
            3 => 
            array (
                'item' => 73,
                'salsa' => 88,
            ),
        ));
        
        
    }
}