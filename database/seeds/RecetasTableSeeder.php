<?php

use Illuminate\Database\Seeder;

class RecetasTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('recetas')->delete();
        
        \DB::table('recetas')->insert(array (
            0 => 
            array (
                'id' => 1,
                'item_id' => 4,
                'cantidad' => '1.00',
                'procedimiento' => NULL,
                'user_id' => 1,
                'created_at' => '2018-08-27 11:06:29',
                'updated_at' => '2018-08-27 11:06:29',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'item_id' => 5,
                'cantidad' => '1.00',
                'procedimiento' => NULL,
                'user_id' => 1,
                'created_at' => '2018-08-27 12:46:50',
                'updated_at' => '2018-08-27 12:46:50',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}