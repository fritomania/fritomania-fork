<?php

use Illuminate\Database\Seeder;

class TipoPagosTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('tipo_pagos')->delete();
        
        \DB::table('tipo_pagos')->insert(array (
            0 => 
            array (
                'id' => 1,
                'nombre' => 'Efectivo',
                'created_at' => '2018-08-26 15:21:25',
                'updated_at' => '2018-08-26 15:21:25',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'nombre' => 'Tarjeta Débito',
                'created_at' => '2018-08-26 15:21:34',
                'updated_at' => '2018-08-26 15:22:17',
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'nombre' => 'Tarjeta de crédito',
                'created_at' => '2018-08-26 15:21:45',
                'updated_at' => '2018-08-26 15:21:45',
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'nombre' => 'WebPay',
                'created_at' => '2018-09-19 15:36:52',
                'updated_at' => '2018-09-19 15:36:52',
                'deleted_at' => NULL,
            ),
            4 =>
            array (
                'id' => 5,
                'nombre' => 'Khipu',
                'created_at' => '2018-09-19 15:36:52',
                'updated_at' => '2018-09-19 15:36:52',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}