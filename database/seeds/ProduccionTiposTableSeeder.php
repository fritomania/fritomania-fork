<?php

use Illuminate\Database\Seeder;

class ProduccionTiposTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('produccion_tipos')->delete();
        
        \DB::table('produccion_tipos')->insert(array (
            0 => 
            array (
                'id' => 1,
                'nombre' => 'Materia Prima a Producto Final',
                'created_at' => '2018-08-09 13:01:44',
                'updated_at' => '2018-08-09 13:01:44',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'nombre' => 'Producto final a Combos',
                'created_at' => '2018-08-09 13:02:10',
                'updated_at' => '2018-08-09 13:02:10',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}