<?php

use App\Models\Produccione;
use App\Models\ProductoConsumido;
use App\Models\ProductoRealizado;
use App\Models\ProductoSolicitado;
use Illuminate\Database\Seeder;

class ProduccionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Produccione::class,10)->create()
            ->each(function (Produccione $produccion){
                $dets = random_int(5,15);
                factory(ProductoSolicitado::class,$dets)->create(['produccione_id' => $produccion->id]);
                factory(ProductoRealizado::class,$dets)->create(['produccione_id' => $produccion->id]);
                factory(ProductoConsumido::class,$dets)->create(['produccione_id' => $produccion->id]);
            });
    }
}
