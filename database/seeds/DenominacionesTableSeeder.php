<?php

use Illuminate\Database\Seeder;

class DenominacionesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('denominaciones')->delete();
        
        \DB::table('denominaciones')->insert(array (
            0 => 
            array (
                'id' => 1,
                'monto' => '20000.00',
                'created_at' => '2018-09-05 18:42:55',
                'updated_at' => '2018-09-05 18:42:55',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'monto' => '10000.00',
                'created_at' => '2018-09-05 18:43:09',
                'updated_at' => '2018-09-05 18:43:09',
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'monto' => '5000.00',
                'created_at' => '2018-09-05 18:43:17',
                'updated_at' => '2018-09-05 18:43:17',
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'monto' => '2000.00',
                'created_at' => '2018-09-05 18:43:26',
                'updated_at' => '2018-09-05 18:43:26',
                'deleted_at' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'monto' => '1000.00',
                'created_at' => '2018-09-05 18:43:37',
                'updated_at' => '2018-09-05 18:43:37',
                'deleted_at' => NULL,
            ),
            5 => 
            array (
                'id' => 6,
                'monto' => '500.00',
                'created_at' => '2018-09-05 18:44:09',
                'updated_at' => '2018-09-05 18:44:09',
                'deleted_at' => NULL,
            ),
            6 => 
            array (
                'id' => 7,
                'monto' => '100.00',
                'created_at' => '2018-09-05 18:44:22',
                'updated_at' => '2018-09-05 18:44:22',
                'deleted_at' => NULL,
            ),
            7 => 
            array (
                'id' => 8,
                'monto' => '50.00',
                'created_at' => '2018-09-05 18:44:29',
                'updated_at' => '2018-09-05 18:44:29',
                'deleted_at' => NULL,
            ),
            8 => 
            array (
                'id' => 9,
                'monto' => '10.00',
                'created_at' => '2018-09-05 18:44:37',
                'updated_at' => '2018-09-05 18:44:37',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}