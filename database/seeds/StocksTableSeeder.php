<?php

use App\Models\Inistock;
use App\Models\Stock;
use App\Models\Tienda;
use Illuminate\Database\Seeder;

class StocksTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        $items = \App\Models\Item::all();

        foreach ($items as $index => $item) {

//            $stock = $item->stock;
            $stock = $item->esInventariable() ? 1000 : 0;

            $tienda= array_random([Tienda::LOCAL_1, Tienda::LOCAL_2]);

            if($item->materia_prima){
                factory(Stock::class,1)->create([
                    'item_id' => $item->id,
                    'cantidad' => $stock,
                    'cnt_ini' => $stock,
                    'tienda_id'=> Tienda::BODEGA_MP
                ])->each(function (Stock $stock){
                    factory(Inistock::class,1)->create([
                        'item_id' => $stock->item_id,
                        'tienda_id' => $stock->tienda_id,
                        'cantidad' => $stock->cantidad
                    ]);
                });
                factory(Stock::class,1)->create([
                    'item_id' => $item->id,
                    'cantidad' => $stock,
                    'cnt_ini' => $stock,
                    'tienda_id'=> Tienda::BODEGA_PRODUCCION
                ])->each(function (Stock $stock){
                    factory(Inistock::class,1)->create([
                        'item_id' => $stock->item_id,
                        'tienda_id' => $stock->tienda_id,
                        'cantidad' => $stock->cantidad
                    ]);
                });
            }else{
                factory(Stock::class,1)->create([
                    'item_id' => $item->id,
                    'cantidad' => $stock,
                    'cnt_ini' => $stock,
                    'tienda_id'=> Tienda::LOCAL_1
                ])->each(function (Stock $stock){
                    factory(Inistock::class,1)->create([
                        'item_id' => $stock->item_id,
                        'tienda_id' => $stock->tienda_id,
                        'cantidad' => $stock->cantidad
                    ]);
                });

                factory(Stock::class,1)->create([
                    'item_id' => $item->id,
                    'cantidad' => $stock,
                    'cnt_ini' => $stock,
                    'tienda_id'=> Tienda::LOCAL_2
                ])->each(function (Stock $stock){
                    factory(Inistock::class,1)->create([
                        'item_id' => $stock->item_id,
                        'tienda_id' => $stock->tienda_id,
                        'cantidad' => $stock->cantidad
                    ]);
                });

                factory(Stock::class,1)->create([
                    'item_id' => $item->id,
                    'cantidad' => $stock,
                    'cnt_ini' => $stock,
                    'tienda_id'=> Tienda::LOCAL_3
                ])->each(function (Stock $stock){
                    factory(Inistock::class,1)->create([
                        'item_id' => $stock->item_id,
                        'tienda_id' => $stock->tienda_id,
                        'cantidad' => $stock->cantidad
                    ]);
                });
            }


        }
        
        
    }
}