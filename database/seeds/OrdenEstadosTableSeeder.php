<?php

use Illuminate\Database\Seeder;

class OrdenEstadosTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('orden_estados')->delete();
        
        \DB::table('orden_estados')->insert(array (
            0 => 
            array (
                'id' => 1,
                'nombre' => 'Pendiente',
                'created_at' => '2018-08-09 12:17:06',
                'updated_at' => '2018-08-09 12:17:06',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'nombre' => 'En Proceso',
                'created_at' => '2018-08-09 12:17:19',
                'updated_at' => '2018-08-09 12:17:19',
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'nombre' => 'Finalizada',
                'created_at' => '2018-08-09 12:17:33',
                'updated_at' => '2018-08-09 12:17:33',
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'nombre' => 'Entregando',
                'created_at' => '2018-08-09 12:17:50',
                'updated_at' => '2018-08-09 12:17:50',
                'deleted_at' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'nombre' => 'Despachando',
                'created_at' => '2018-08-09 12:18:09',
                'updated_at' => '2018-08-09 12:18:09',
                'deleted_at' => NULL,
            ),
            5 => 
            array (
                'id' => 6,
                'nombre' => 'Cancelada',
                'created_at' => '2018-08-09 12:18:15',
                'updated_at' => '2018-08-09 12:18:15',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}