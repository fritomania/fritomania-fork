<?php

use Illuminate\Database\Seeder;

class UserDespachaUserTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('user_despacha_user')->delete();
        
        \DB::table('user_despacha_user')->insert(array (
            0 => 
            array (
                'user_sol' => 2,
                'user_des' => 1,
            ),
            1 => 
            array (
                'user_sol' => 3,
                'user_des' => 1,
            ),
            2 => 
            array (
                'user_sol' => 3,
                'user_des' => 2,
            ),
            3 => 
            array (
                'user_sol' => 4,
                'user_des' => 1,
            ),
            4 => 
            array (
                'user_sol' => 5,
                'user_des' => 1,
            ),
            5 => 
            array (
                'user_sol' => 5,
                'user_des' => 2,
            ),
            6 => 
            array (
                'user_sol' => 5,
                'user_des' => 3,
            ),
            7 => 
            array (
                'user_sol' => 6,
                'user_des' => 1,
            ),
            8 => 
            array (
                'user_sol' => 7,
                'user_des' => 1,
            ),
            9 => 
            array (
                'user_sol' => 8,
                'user_des' => 1,
            ),
            10 => 
            array (
                'user_sol' => 9,
                'user_des' => 1,
            ),
            11 => 
            array (
                'user_sol' => 10,
                'user_des' => 1,
            ),
            12 => 
            array (
                'user_sol' => 10,
                'user_des' => 2,
            ),
            13 => 
            array (
                'user_sol' => 10,
                'user_des' => 3,
            ),
            14 => 
            array (
                'user_sol' => 11,
                'user_des' => 1,
            ),
            15 => 
            array (
                'user_sol' => 12,
                'user_des' => 1,
            ),
            16 => 
            array (
                'user_sol' => 13,
                'user_des' => 1,
            ),
            17 => 
            array (
                'user_sol' => 14,
                'user_des' => 1,
            ),
        ));
        
        
    }
}