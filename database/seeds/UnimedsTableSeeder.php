<?php

use Illuminate\Database\Seeder;

class UnimedsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('unimeds')->delete();
        
        \DB::table('unimeds')->insert(array (
            0 => 
            array (
                'id' => 1,
                'magnitude_id' => 1,
                'simbolo' => 'U',
                'nombre' => 'Unidad',
                'created_at' => '2017-07-28 10:06:37',
                'updated_at' => '2017-07-28 10:06:37',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'magnitude_id' => 4,
                'simbolo' => 'Kl',
                'nombre' => 'Kilos',
                'created_at' => '2017-07-28 10:07:02',
                'updated_at' => '2018-08-12 11:23:56',
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'magnitude_id' => 4,
                'simbolo' => 'Gm',
                'nombre' => 'Gramos',
                'created_at' => '2017-07-28 10:07:30',
                'updated_at' => '2017-07-28 10:07:30',
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'id' => 12,
                'magnitude_id' => 2,
                'simbolo' => 'Ltr',
                'nombre' => 'Litros',
                'created_at' => '2018-08-12 11:25:15',
                'updated_at' => '2018-08-12 11:25:15',
                'deleted_at' => NULL,
            ),
            4 => 
            array (
                'id' => 13,
                'magnitude_id' => 2,
                'simbolo' => 'CC',
                'nombre' => 'Centímetros Cúbicos',
                'created_at' => '2018-08-12 11:25:58',
                'updated_at' => '2018-08-12 11:25:58',
                'deleted_at' => NULL,
            ),
            5 => 
            array (
                'id' => 14,
                'magnitude_id' => 2,
                'simbolo' => 'ml',
                'nombre' => 'Mililitros',
                'created_at' => '2018-08-12 11:26:14',
                'updated_at' => '2018-08-12 11:26:14',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}