<?php

use Illuminate\Database\Seeder;

class RolUserTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('rol_user')->delete();
        
        \DB::table('rol_user')->insert(array (
            0 => 
            array (
                'rol_id' => 1,
                'user_id' => 1,
            ),
            1 => 
            array (
                'rol_id' => 2,
                'user_id' => 1,
            ),
            2 => 
            array (
                'rol_id' => 3,
                'user_id' => 1,
            ),
            3 => 
            array (
                'rol_id' => 4,
                'user_id' => 1,
            ),
            4 => 
            array (
                'rol_id' => 5,
                'user_id' => 1,
            ),
            5 => 
            array (
                'rol_id' => 3,
                'user_id' => 2,
            ),
            6 => 
            array (
                'rol_id' => 3,
                'user_id' => 3,
            ),
            7 => 
            array (
                'rol_id' => 3,
                'user_id' => 4,
            ),
            8 => 
            array (
                'rol_id' => 7,
                'user_id' => 6,
            ),
            9 => 
            array (
                'rol_id' => 6,
                'user_id' => 7,
            ),
            10 => 
            array (
                'rol_id' => 4,
                'user_id' => 8,
            ),
            11 => 
            array (
                'rol_id' => 4,
                'user_id' => 9,
            ),
            12 => 
            array (
                'rol_id' => 3,
                'user_id' => 10,
            ),
            13 => 
            array (
                'rol_id' => 7,
                'user_id' => 11,
            ),
            14 => 
            array (
                'rol_id' => 6,
                'user_id' => 12,
            ),
            15 => 
            array (
                'rol_id' => 4,
                'user_id' => 13,
            ),
            16 => 
            array (
                'rol_id' => 4,
                'user_id' => 14,
            ),
            17 => 
            array (
                'rol_id' => 3,
                'user_id' => 15,
            ),
            18 => 
            array (
                'rol_id' => 3,
                'user_id' => 16,
            ),
            19 => 
            array (
                'rol_id' => 3,
                'user_id' => 17,
            ),
            20 => 
            array (
                'rol_id' => 3,
                'user_id' => 18,
            ),
            21 => 
            array (
                'rol_id' => 3,
                'user_id' => 19,
            ),
            22 => 
            array (
                'rol_id' => 3,
                'user_id' => 20,
            ),
            23 => 
            array (
                'rol_id' => 6,
                'user_id' => 22,
            ),
            24 => 
            array (
                'rol_id' => 4,
                'user_id' => 23,
            ),
            25 => 
            array (
                'rol_id' => 4,
                'user_id' => 21,
            ),
        ));
        
        
    }
}