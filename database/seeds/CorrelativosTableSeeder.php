<?php

use Illuminate\Database\Seeder;

class CorrelativosTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('correlativos')->delete();
        
        \DB::table('correlativos')->insert(array (
            0 => 
            array (
                'id' => 1,
                'tabla' => 'solicitudes',
                'anio' => 2018,
                'max' => 0,
                'created_at' => '2018-08-31 20:19:30',
                'updated_at' => '2018-08-31 20:19:30',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'tabla' => 'compras',
                'anio' => 2018,
                'max' => 0,
                'created_at' => '2018-08-31 20:19:18',
                'updated_at' => '2018-08-31 20:19:18',
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'tabla' => 'ventas',
                'anio' => 2018,
                'max' => 0,
                'created_at' => '2018-09-14 17:39:43',
                'updated_at' => '2018-09-14 17:39:43',
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'tabla' => 'producciones',
                'anio' => 2018,
                'max' => 0,
                'created_at' => '2018-09-18 08:18:06',
                'updated_at' => '2018-09-18 08:18:06',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}