<?php

use Illuminate\Database\Seeder;

class IcategoriaItemTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('icategoria_item')->delete();
        
        \DB::table('icategoria_item')->insert(array (
            0 => 
            array (
                'icategoria_id' => 2,
                'item_id' => 18,
            ),
            1 => 
            array (
                'icategoria_id' => 2,
                'item_id' => 19,
            ),
            2 => 
            array (
                'icategoria_id' => 2,
                'item_id' => 21,
            ),
            3 => 
            array (
                'icategoria_id' => 2,
                'item_id' => 24,
            ),
            4 => 
            array (
                'icategoria_id' => 2,
                'item_id' => 25,
            ),
            5 => 
            array (
                'icategoria_id' => 2,
                'item_id' => 30,
            ),
            6 => 
            array (
                'icategoria_id' => 2,
                'item_id' => 51,
            ),
            7 => 
            array (
                'icategoria_id' => 2,
                'item_id' => 65,
            ),
            8 => 
            array (
                'icategoria_id' => 2,
                'item_id' => 80,
            ),
            9 => 
            array (
                'icategoria_id' => 2,
                'item_id' => 81,
            ),
            10 => 
            array (
                'icategoria_id' => 4,
                'item_id' => 3,
            ),
            11 => 
            array (
                'icategoria_id' => 4,
                'item_id' => 12,
            ),
            12 => 
            array (
                'icategoria_id' => 4,
                'item_id' => 13,
            ),
            13 => 
            array (
                'icategoria_id' => 4,
                'item_id' => 14,
            ),
            14 => 
            array (
                'icategoria_id' => 4,
                'item_id' => 15,
            ),
            15 => 
            array (
                'icategoria_id' => 4,
                'item_id' => 16,
            ),
            16 => 
            array (
                'icategoria_id' => 4,
                'item_id' => 17,
            ),
            17 => 
            array (
                'icategoria_id' => 4,
                'item_id' => 20,
            ),
            18 => 
            array (
                'icategoria_id' => 4,
                'item_id' => 23,
            ),
            19 => 
            array (
                'icategoria_id' => 4,
                'item_id' => 66,
            ),
            20 => 
            array (
                'icategoria_id' => 4,
                'item_id' => 67,
            ),
            21 => 
            array (
                'icategoria_id' => 4,
                'item_id' => 68,
            ),
            22 => 
            array (
                'icategoria_id' => 4,
                'item_id' => 69,
            ),
            23 => 
            array (
                'icategoria_id' => 4,
                'item_id' => 70,
            ),
            24 => 
            array (
                'icategoria_id' => 4,
                'item_id' => 71,
            ),
            25 => 
            array (
                'icategoria_id' => 5,
                'item_id' => 82,
            ),
            26 => 
            array (
                'icategoria_id' => 5,
                'item_id' => 83,
            ),
            27 => 
            array (
                'icategoria_id' => 5,
                'item_id' => 84,
            ),
            28 => 
            array (
                'icategoria_id' => 5,
                'item_id' => 85,
            ),
            29 => 
            array (
                'icategoria_id' => 6,
                'item_id' => 86,
            ),
            30 => 
            array (
                'icategoria_id' => 6,
                'item_id' => 87,
            ),
            31 => 
            array (
                'icategoria_id' => 6,
                'item_id' => 88,
            ),
            32 => 
            array (
                'icategoria_id' => 7,
                'item_id' => 20,
            ),
            33 => 
            array (
                'icategoria_id' => 7,
                'item_id' => 66,
            ),
            34 => 
            array (
                'icategoria_id' => 7,
                'item_id' => 67,
            ),
            35 => 
            array (
                'icategoria_id' => 7,
                'item_id' => 68,
            ),
            36 => 
            array (
                'icategoria_id' => 7,
                'item_id' => 73,
            ),
            37 => 
            array (
                'icategoria_id' => 7,
                'item_id' => 78,
            ),
        ));
        
        
    }
}