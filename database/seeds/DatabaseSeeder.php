<?php

use App\Models\Ingrediente;
use App\Models\IngredienteCategoria;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
//        File::deleteDirectory(public_path('img/items'));
//        File::makeDirectory(public_path('img/items'));

        $this->call(OptionsTableSeeder::class);
        $this->call(RolsTableSeeder::class);
        $this->call(EstadosItemsTableSeeder::class);
        $this->call(CestadosTableSeeder::class);
        $this->call(VestadosTableSeeder::class);
        $this->call(IcategoriasTableSeeder::class);
        $this->call(MagnitudesTableSeeder::class);
        $this->call(UnimedsTableSeeder::class);
        $this->call(TcomprobantesTableSeeder::class);
        $this->call(ClientesTableSeeder::class);
        $this->call(ProveedoresTableSeeder::class);
        $this->call(HorariosTableSeeder::class);
        $this->call(MarcasTableSeeder::class);
        $this->call(ConfigurationsTableSeeder::class);

        $this->call(TiendasTableSeeder::class);

        $this->call(EmpleadosTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(OptionUserTableSeeder::class);
        $this->call(RolUserTableSeeder::class);
        $this->call(PestadosTableSeeder::class);
        $this->call(CorrelativosTableSeeder::class);

        $this->call(SolicitudEstadosTableSeeder::class);
        $this->call(NotificacionTiposTableSeeder::class);
        $this->call(OrdenEstadosTableSeeder::class);
        $this->call(ProduccionTiposTableSeeder::class);
        $this->call(ProduccionEstadosTableSeeder::class);

        if(App::environment()=='local') {
            $this->call(ItemsTableSeeder::class);
            $this->call(StocksTableSeeder::class);
            $this->call(IcategoriaItemTableSeeder::class);
            $this->call(RecetasTableSeeder::class);
            $this->call(RecetaDetallesTableSeeder::class);
        }


        $this->call(TipoPagosTableSeeder::class);
        $this->call(UserDespachaUserTableSeeder::class);
        $this->call(DenominacionesTableSeeder::class);
        $this->call(TipoMarcajesTableSeeder::class);


        if(App::environment()=='local'){
            $this->call(CombosTableSeeder::class);
            $this->call(ComboDetallesTableSeeder::class);
            $this->call(VentaSeeder::class);
            $this->call(ComprasSeeder::class);
            $this->call(SolicitudStockSeeder::class);
            $this->call(ProduccionSeeder::class);
        }

        $this->call(IngredienteCategoriasSeeder::class);
        $this->call(IngredientesSeeder::class);

        if(App::environment()=='local') {
            $this->call(VentaRecetasTableSeeder::class);
            $this->call(VentaRecetaDetallesTableSeeder::class);
            $this->call(IngredientesOpcionalesTableSeeder::class);
            $this->call(ItemExtrasTableSeeder::class);
            $this->call(ItemSalsasTableSeeder::class);
        }


    }
}
