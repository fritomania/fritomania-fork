<?php

use Illuminate\Database\Seeder;

class RolsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('rols')->delete();
        
        \DB::table('rols')->insert(array (
            0 => 
            array (
                'id' => 1,
                'descripcion' => 'SuperAdmin',
                'created_at' => '2018-08-22 12:07:47',
                'updated_at' => '2018-08-22 12:07:47',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'descripcion' => 'admin',
                'created_at' => '2018-08-22 12:07:47',
                'updated_at' => '2018-08-22 12:07:47',
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'descripcion' => 'empleado',
                'created_at' => '2018-08-22 12:07:47',
                'updated_at' => '2018-08-22 12:07:47',
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'descripcion' => 'repartidor',
                'created_at' => '2018-08-22 12:07:47',
                'updated_at' => '2018-08-22 12:07:47',
                'deleted_at' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'descripcion' => 'user',
                'created_at' => '2018-08-22 12:07:47',
                'updated_at' => '2018-08-22 12:07:47',
                'deleted_at' => NULL,
            ),
            5 => 
            array (
                'id' => 6,
                'descripcion' => 'cocina',
                'created_at' => '2018-08-22 17:17:56',
                'updated_at' => '2018-08-22 17:17:57',
                'deleted_at' => NULL,
            ),
            6 => 
            array (
                'id' => 7,
                'descripcion' => 'caja',
                'created_at' => '2018-08-29 11:54:38',
                'updated_at' => '2018-08-29 11:54:39',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}