<?php

use Illuminate\Database\Seeder;

class VentaRecetaDetallesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('venta_receta_detalles')->delete();
        
        \DB::table('venta_receta_detalles')->insert(array (
            0 => 
            array (
                'id' => 1,
                'venta_receta_id' => 1,
                'ingrediente_id' => 2,
                'cantidad' => '1.00',
                'created_at' => '2018-12-27 16:18:44',
                'updated_at' => '2018-12-31 14:47:03',
                'deleted_at' => '2018-12-31 14:47:03',
            ),
            1 => 
            array (
                'id' => 2,
                'venta_receta_id' => 1,
                'ingrediente_id' => 1,
                'cantidad' => '1.00',
                'created_at' => '2018-12-27 23:04:50',
                'updated_at' => '2018-12-31 14:47:11',
                'deleted_at' => '2018-12-31 14:47:11',
            ),
            2 => 
            array (
                'id' => 3,
                'venta_receta_id' => 1,
                'ingrediente_id' => 9,
                'cantidad' => '1.00',
                'created_at' => '2018-12-31 14:53:38',
                'updated_at' => '2018-12-31 14:53:38',
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'venta_receta_id' => 1,
                'ingrediente_id' => 1,
                'cantidad' => '1.00',
                'created_at' => '2018-12-31 14:58:36',
                'updated_at' => '2018-12-31 14:58:36',
                'deleted_at' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'venta_receta_id' => 1,
                'ingrediente_id' => 5,
                'cantidad' => '1.00',
                'created_at' => '2018-12-31 14:58:55',
                'updated_at' => '2018-12-31 14:58:55',
                'deleted_at' => NULL,
            ),
            5 => 
            array (
                'id' => 6,
                'venta_receta_id' => 1,
                'ingrediente_id' => 6,
                'cantidad' => '1.00',
                'created_at' => '2018-12-31 14:59:06',
                'updated_at' => '2018-12-31 14:59:06',
                'deleted_at' => NULL,
            ),
            6 => 
            array (
                'id' => 7,
                'venta_receta_id' => 1,
                'ingrediente_id' => 7,
                'cantidad' => '1.00',
                'created_at' => '2018-12-31 14:59:19',
                'updated_at' => '2018-12-31 14:59:19',
                'deleted_at' => NULL,
            ),
            7 => 
            array (
                'id' => 8,
                'venta_receta_id' => 1,
                'ingrediente_id' => 8,
                'cantidad' => '1.00',
                'created_at' => '2018-12-31 14:59:34',
                'updated_at' => '2018-12-31 14:59:34',
                'deleted_at' => NULL,
            ),
            8 => 
            array (
                'id' => 9,
                'venta_receta_id' => 2,
                'ingrediente_id' => 1,
                'cantidad' => '1.00',
                'created_at' => '2019-01-02 20:11:40',
                'updated_at' => '2019-01-02 20:11:40',
                'deleted_at' => NULL,
            ),
            9 => 
            array (
                'id' => 10,
                'venta_receta_id' => 2,
                'ingrediente_id' => 7,
                'cantidad' => '1.00',
                'created_at' => '2019-01-02 20:12:22',
                'updated_at' => '2019-01-02 20:12:30',
                'deleted_at' => '2019-01-02 20:12:30',
            ),
            10 => 
            array (
                'id' => 11,
                'venta_receta_id' => 2,
                'ingrediente_id' => 7,
                'cantidad' => '1.00',
                'created_at' => '2019-01-02 20:12:39',
                'updated_at' => '2019-01-02 20:12:39',
                'deleted_at' => NULL,
            ),
            11 => 
            array (
                'id' => 12,
                'venta_receta_id' => 2,
                'ingrediente_id' => 5,
                'cantidad' => '1.00',
                'created_at' => '2019-01-02 20:12:47',
                'updated_at' => '2019-01-02 20:12:47',
                'deleted_at' => NULL,
            ),
            12 => 
            array (
                'id' => 13,
                'venta_receta_id' => 2,
                'ingrediente_id' => 8,
                'cantidad' => '1.00',
                'created_at' => '2019-01-02 20:12:56',
                'updated_at' => '2019-01-02 20:12:56',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}