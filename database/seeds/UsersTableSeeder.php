<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('users')->delete();
        
        \DB::table('users')->insert(array (
            0 => 
            array (
                'id' => 1,
                'tienda_id' => 1,
                'name' => 'Administrador Sistema',
                'username' => 'admin',
                'email' => 'info@itsb.cl',
                'password' => bcrypt("123"),
                'created_at' => '2018-08-30 02:51:07',
                'updated_at' => '2018-09-06 21:02:54',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'tienda_id' => 1,
                'name' => 'Admin Materia',
                'username' => 'materia',
                'email' => 'info@itsb.cl',
                'password' => bcrypt("123"),
                'created_at' => '2018-08-30 02:51:07',
                'updated_at' => '2018-09-06 21:01:16',
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'tienda_id' => 2,
                'name' => 'Admin Produccion',
                'username' => 'produccion',
                'email' => 'info@itsb.cl',
                'password' => bcrypt("123"),
                'created_at' => '2018-08-30 02:51:07',
                'updated_at' => '2018-09-06 21:01:49',
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'tienda_id' => 3,
                'name' => 'Cocina Producción',
                'username' => 'cocinap',
                'email' => 'info@itsb.cl',
                'password' => bcrypt("123"),
                'created_at' => '2018-08-30 02:51:07',
                'updated_at' => '2018-09-06 21:02:10',
                'deleted_at' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'tienda_id' => 4,
                'name' => 'Admin Local 1',
                'username' => 'adminl1',
                'email' => 'info@itsb.cl',
                'password' => bcrypt("123"),
                'created_at' => '2018-08-30 02:51:07',
                'updated_at' => '2018-09-06 21:02:34',
                'deleted_at' => NULL,
            ),
            5 => 
            array (
                'id' => 6,
                'tienda_id' => 4,
                'name' => 'Caja Local 1',
                'username' => 'cajal1',
                'email' => 'info@itsb.cl',
                'password' => bcrypt("123"),
                'created_at' => '2018-08-30 02:51:08',
                'updated_at' => '2018-09-05 13:09:55',
                'deleted_at' => NULL,
            ),
            6 => 
            array (
                'id' => 7,
                'tienda_id' => 4,
                'name' => 'Cocina Local 1',
                'username' => 'cocinal1',
                'email' => 'info@itsb.cl',
                'password' => bcrypt("123"),
                'created_at' => '2018-08-30 02:51:08',
                'updated_at' => '2018-08-30 02:51:08',
                'deleted_at' => NULL,
            ),
            7 => 
            array (
                'id' => 8,
                'tienda_id' => 4,
                'name' => 'Repartidor 1 local 1',
                'username' => 'Delivery1l1',
                'email' => 'info@itsb.cl',
                'password' => bcrypt("123"),
                'created_at' => '2018-08-30 02:51:08',
                'updated_at' => '2018-08-30 02:51:08',
                'deleted_at' => NULL,
            ),
            8 => 
            array (
                'id' => 9,
                'tienda_id' => 4,
                'name' => 'Repartidor 2 local 1',
                'username' => 'Delivery2l1',
                'email' => 'info@itsb.cl',
                'password' => bcrypt("123"),
                'created_at' => '2018-08-30 02:51:08',
                'updated_at' => '2018-08-30 02:51:08',
                'deleted_at' => NULL,
            ),
            9 => 
            array (
                'id' => 10,
                'tienda_id' => 5,
                'name' => 'Admin local 2',
                'username' => 'adminl2',
                'email' => 'info@itsb.cl',
                'password' => bcrypt("123"),
                'created_at' => '2018-08-30 02:51:08',
                'updated_at' => '2018-09-06 21:04:38',
                'deleted_at' => NULL,
            ),
            10 => 
            array (
                'id' => 11,
                'tienda_id' => 5,
                'name' => 'Caja Local 2',
                'username' => 'cajal2',
                'email' => 'info@itsb.cl',
                'password' => bcrypt("123"),
                'created_at' => '2018-08-30 02:51:08',
                'updated_at' => '2018-09-06 21:04:54',
                'deleted_at' => NULL,
            ),
            11 => 
            array (
                'id' => 12,
                'tienda_id' => 5,
                'name' => 'Cocina 1 Local 2',
                'username' => 'cocina1l2',
                'email' => 'info@itsb.cl',
                'password' => bcrypt("123"),
                'created_at' => '2018-08-30 02:51:08',
                'updated_at' => '2018-09-27 16:42:45',
                'deleted_at' => NULL,
            ),
            12 => 
            array (
                'id' => 13,
                'tienda_id' => 5,
                'name' => 'Repartidor 1 local 2',
                'username' => 'Delivery1l2',
                'email' => 'info@itsb.cl',
                'password' => bcrypt("123"),
                'created_at' => '2018-08-30 02:51:08',
                'updated_at' => '2018-08-30 02:51:08',
                'deleted_at' => NULL,
            ),
            13 => 
            array (
                'id' => 14,
                'tienda_id' => 5,
                'name' => 'Repartidor 2 local 2',
                'username' => 'Delivery2l2',
                'email' => 'info@itsb.cl',
                'password' => bcrypt("123"),
                'created_at' => '2018-08-30 02:51:08',
                'updated_at' => '2018-08-30 02:51:08',
                'deleted_at' => NULL,
            ),
            14 => 
            array (
                'id' => 15,
                'tienda_id' => 6,
                'name' => 'Administrador Local 3',
                'username' => 'adminl3',
                'email' => 'info@itsb.cl',
                'password' => bcrypt("123"),
                'created_at' => '2018-09-25 17:38:19',
                'updated_at' => '2018-09-25 17:38:54',
                'deleted_at' => NULL,
            ),
            15 => 
            array (
                'id' => 16,
                'tienda_id' => 6,
                'name' => 'Caja Local 3',
                'username' => 'cajal3',
                'email' => 'info@itsb.cl',
                'password' => bcrypt("123"),
                'created_at' => '2018-09-25 17:44:59',
                'updated_at' => '2018-09-25 17:44:59',
                'deleted_at' => NULL,
            ),
            16 => 
            array (
                'id' => 17,
                'tienda_id' => 6,
                'name' => 'Cocina 1 Local 3',
                'username' => 'cocina1l3',
                'email' => 'info@itsb.cl',
                'password' => bcrypt("123"),
                'created_at' => '2018-09-25 17:48:00',
                'updated_at' => '2018-09-27 16:37:18',
                'deleted_at' => NULL,
            ),
            17 => 
            array (
                'id' => 18,
                'tienda_id' => 6,
                'name' => 'Repartidor 1 local 3',
                'username' => 'Delivery1l3',
                'email' => 'info@itsb.cl',
                'password' => bcrypt("123"),
                'created_at' => '2018-09-25 17:51:39',
                'updated_at' => '2018-09-25 17:51:39',
                'deleted_at' => NULL,
            ),
            18 => 
            array (
                'id' => 19,
                'tienda_id' => 6,
                'name' => 'Repartidor 2 local 1',
                'username' => 'Delivery2l3',
                'email' => 'info@itsb.cl',
                'password' => bcrypt("123"),
                'created_at' => '2018-09-25 17:53:44',
                'updated_at' => '2018-09-25 17:53:44',
                'deleted_at' => NULL,
            ),
            19 => 
            array (
                'id' => 20,
                'tienda_id' => 6,
                'name' => 'Cocina 2 Local 3',
                'username' => 'cocina2l3',
                'email' => 'info@itsb.cl',
                'password' => bcrypt("123"),
                'created_at' => '2018-09-27 16:38:41',
                'updated_at' => '2018-09-27 16:38:41',
                'deleted_at' => NULL,
            ),
            20 => 
            array (
                'id' => 21,
                'tienda_id' => 6,
                'name' => 'Delivery 3 Local 3',
                'username' => 'Delivery3l3',
                'email' => 'info@itsb.cl',
                'password' => bcrypt("123"),
                'created_at' => '2018-09-27 16:40:53',
                'updated_at' => '2018-09-27 16:40:53',
                'deleted_at' => NULL,
            ),
            21 => 
            array (
                'id' => 22,
                'tienda_id' => 5,
                'name' => 'Cocina 2 Local 2',
                'username' => 'cocina2l2',
                'email' => 'info@itsb.cl',
                'password' => bcrypt("123"),
                'created_at' => '2018-09-27 16:43:51',
                'updated_at' => '2018-09-27 16:44:36',
                'deleted_at' => NULL,
            ),
            22 => 
            array (
                'id' => 23,
                'tienda_id' => 5,
                'name' => 'Delivery 3 Local 2',
                'username' => 'Delivery3l2',
                'email' => 'info@itsb.cl',
                'password' => bcrypt("123"),
                'created_at' => '2018-09-27 16:46:21',
                'updated_at' => '2018-09-27 16:46:21',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}