<?php

use Illuminate\Database\Seeder;

class EmpleadosTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('empleados')->delete();
        
        \DB::table('empleados')->insert(array (
            0 =>
                array (
                    'id' => 1,
                    'tienda_id' => 1,
                    'nombres' => 'Administrador',
                    'apellidos' => 'Del Sitema',
                    'telefono' => '00000000',
                    'correo' => 'admin@serve.com',
                    'sueldo_diario' => '0.00',
                    'porcentaje_comision' => 0,
                    'genero' => 'M',
                    'puesto' => NULL,
                    'fecha_contratacion' => NULL,
                    'direccion' => NULL,
                    'fecha_nacimiento' => NULL,
                    'created_at' => '2017-07-03 14:05:05',
                    'updated_at' => '2017-07-03 14:05:05',
                    'deleted_at' => NULL,
                ),
            1 =>
                array (
                    'id' => 2,
                    'tienda_id' => 1,
                    'nombres' => 'Empleado',
                    'apellidos' => 'Pruebas',
                    'telefono' => '12345678',
                    'correo' => 'empleado@server.com',
                    'sueldo_diario' => '45.00',
                    'porcentaje_comision' => 0,
                    'genero' => 'M',
                    'puesto' => NULL,
                    'fecha_contratacion' => NULL,
                    'direccion' => NULL,
                    'fecha_nacimiento' => NULL,
                    'created_at' => '2017-06-14 10:10:42',
                    'updated_at' => '2017-06-14 10:10:42',
                    'deleted_at' => NULL,
                ),
            2 =>
                array (
                    'id' => 3,
                    'tienda_id' => 2,
                    'nombres' => 'Empleado',
                    'apellidos' => 'Pruebas 2',
                    'telefono' => '78945612',
                    'correo' => 'empleado2@server.com',
                    'sueldo_diario' => '50.00',
                    'porcentaje_comision' => 0,
                    'genero' => 'M',
                    'puesto' => NULL,
                    'fecha_contratacion' => NULL,
                    'direccion' => NULL,
                    'fecha_nacimiento' => NULL,
                    'created_at' => '2017-06-14 10:22:40',
                    'updated_at' => '2017-06-14 10:22:40',
                    'deleted_at' => NULL,
                ),

        ));
        
        
    }
}