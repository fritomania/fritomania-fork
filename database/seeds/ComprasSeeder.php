<?php

use App\Models\Compra;
use App\Models\CompraDetalle;
use Illuminate\Database\Seeder;

class ComprasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        factory(Compra::class,10)->create()
            ->each(function (Compra $compra){
                factory(CompraDetalle::class,random_int(5,25))->create(['compra_id' => $compra->id]);
            });
    }
}
