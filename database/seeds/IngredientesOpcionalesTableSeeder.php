<?php

use Illuminate\Database\Seeder;

class IngredientesOpcionalesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('ingredientes_opcionales')->delete();
        
        \DB::table('ingredientes_opcionales')->insert(array (
            0 => 
            array (
                'ingrediente_id' => 1,
                'venta_receta_detalle_id' => 4,
                'cantidad' => NULL,
            ),
            1 => 
            array (
                'ingrediente_id' => 1,
                'venta_receta_detalle_id' => 9,
                'cantidad' => NULL,
            ),
            2 => 
            array (
                'ingrediente_id' => 2,
                'venta_receta_detalle_id' => 4,
                'cantidad' => NULL,
            ),
            3 => 
            array (
                'ingrediente_id' => 2,
                'venta_receta_detalle_id' => 9,
                'cantidad' => NULL,
            ),
            4 => 
            array (
                'ingrediente_id' => 3,
                'venta_receta_detalle_id' => 4,
                'cantidad' => NULL,
            ),
            5 => 
            array (
                'ingrediente_id' => 3,
                'venta_receta_detalle_id' => 9,
                'cantidad' => NULL,
            ),
            6 => 
            array (
                'ingrediente_id' => 4,
                'venta_receta_detalle_id' => 4,
                'cantidad' => NULL,
            ),
            7 => 
            array (
                'ingrediente_id' => 4,
                'venta_receta_detalle_id' => 9,
                'cantidad' => NULL,
            ),
            8 => 
            array (
                'ingrediente_id' => 9,
                'venta_receta_detalle_id' => 3,
                'cantidad' => NULL,
            ),
            9 => 
            array (
                'ingrediente_id' => 10,
                'venta_receta_detalle_id' => 3,
                'cantidad' => NULL,
            ),
        ));
        
        
    }
}