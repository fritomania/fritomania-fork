<?php

use App\Models\SolicitudDetalle;
use App\Models\Solicitude;
use Illuminate\Database\Seeder;

class SolicitudStockSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Solicitude::class,10)->create()
            ->each(function (Solicitude $solicitud){
                factory(SolicitudDetalle::class,random_int(5,15))->create(['solicitude_id' => $solicitud->id]);
            });
    }
}
