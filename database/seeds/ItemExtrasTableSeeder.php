<?php

use Illuminate\Database\Seeder;

class ItemExtrasTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('item_extras')->delete();
        
        \DB::table('item_extras')->insert(array (
            0 => 
            array (
                'item' => 72,
                'extra' => 82,
            ),
            1 => 
            array (
                'item' => 72,
                'extra' => 83,
            ),
        ));
        
        
    }
}