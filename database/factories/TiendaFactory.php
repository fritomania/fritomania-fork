<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\Tienda::class, function (Faker $faker) {
    return [
//        'admin' => \App\User::all()->random()->id,
        'nombre' => $faker->name,
        'direccion' => $faker->address,
        'telefono' => $faker->phoneNumber
    ];
});
