<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\RecetaDetalle::class, function (Faker $faker) {
    return [
        'item_id' => \App\Models\Item::materiaPrima()->get()->random()->id,
        'receta_id' => \App\Models\Receta::all()->random()->id,
        'cantidad' => rand(1,10)
    ];
});
