<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\Repartidore::class, function (Faker $faker) {
    return [
        'nombres' => $faker->name,
        'apellidos' => $faker->lastName,
        'hora_inicia' => 0,
        'hora_fin' => 0,
        'user_id' => \App\User::all()->random()->id
    ];
});
