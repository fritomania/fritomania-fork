<?php

use App\Models\Item;
use Faker\Generator as Faker;


$factory->define(Item::class, function (Faker $faker) {

    $precio_compra = $faker->randomFloat(2,100,150);
    $precio_venta = $precio_compra * 1.3;
    $precio_mayoreo = $precio_venta * .9;
    return [
        'nombre' => $faker->randomElement(['Cheese Pizza', 'Hamburger', 'Cheeseburger', 'Bacon Burger', 'Bacon Cheeseburger',
            'Little Hamburger', 'Little Cheeseburger', 'Little Bacon Burger', 'Little Bacon Cheeseburger',
            'Veggie Sandwich', 'Cheese Veggie Sandwich', 'Grilled Cheese',
            'Cheese Dog', 'Bacon Dog', 'Bacon Cheese Dog', 'Pasta', 'Beer', 'Bud Light', 'Budweiser', 'Miller Lite',
            'Milk Shake', 'Tea', ' Sweet Tea', 'Coffee', 'Hot Tea',
            'Champagne', 'Wine', 'Limonade', 'Coca_cola', 'Diet-Coke',
            'Water', 'Sprite', 'Orange Juice', 'Iced Coffee'
        ]),
        'descripcion'=> $faker->paragraph,
        'codigo' => $faker->randomNumber(5),
        'precio_venta' => $precio_venta,
        'precio_compra' => $precio_compra,
        'precio_mayoreo' => $precio_mayoreo,
        'cantidad_mayoreo' => rand(10, 20),
        'precio_promedio' => 0,
        'stock' => rand(20, 100),
        'imagen' => '/img/items/'.\Faker\Provider\Image::image(public_path() . '/img/items', 600, 600, 'food', false),
        'inventariable' => rand(0,1),
        'perecedero' => rand(0,1),
        'marca_id' => \App\Models\Marca::all()->random()->id,
        'unimed_id' => \App\Models\Unimed::all()->random()->id,
        'icategoria_id' => \App\Models\Icategoria::all()->random()->id,
        'iestado_id' => 1,
        'web' => rand(0,1)
    ];

});
