<?php

use App\Models\Produccione;
use App\Models\ProduccionEstado;
use App\Models\ProduccionTipo;
use App\User;
use Faker\Generator as Faker;

$factory->define(Produccione::class, function (Faker $faker) {

    return [
        'numero' => $faker->numberBetween(5,4899),
        'produccion_tipo_id' => ProduccionTipo::MateriaPrimaAProductoFinal,
        'produccion_estado_id' => $faker->randomElement([ProduccionEstado::INCOMPLETA,ProduccionEstado::COMPLETADA]),
        'user_id' => User::all()->random()->id
    ];
});
