<?php

use App\Models\Item;
use App\Models\Produccione;
use App\Models\ProductoRealizado;
use App\User;
use Faker\Generator as Faker;

$factory->define(ProductoRealizado::class, function (Faker $faker) {
    return [
        'produccione_id' => Produccione::all()->random()->id,
        'item_id' => Item::all()->random()->id,
        'cantidad' => random_int(5,40),
        'user_id' => User::all()->random()->id
    ];
});
