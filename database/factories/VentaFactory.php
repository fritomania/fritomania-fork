<?php

use App\Models\Caja;
use App\Models\Cliente;
use App\Models\Tienda;
use App\Models\TipoPago;
use App\Models\Vestado;
use App\User;
use Faker\Generator as Faker;

$factory->define(\App\Models\Venta::class, function (Faker $faker) {

    $client = Cliente::all()->random();
    $tipoPago = TipoPago::all()->random();
    $codWebPay = $tipoPago->id==TipoPago::WEBPAY ? random_int(1245,4852) : null;

    return [
        'cliente_id' => $client->id,
        'serie' => random_int(550000, 999999),
        'fecha' => today(),
        'numero' => $faker->randomNumber(8),
        'delivery' => rand(0,1),
        'recibido' => 1000  ,
        'pagada' => rand(0,1),
        'vestado_id' => Vestado::all()->random()->id,
        'caja_id' => Caja::all()->random()->id,
        'user_id' => User::all()->random()->id,
        'tienda_id' => $faker->randomElement([Tienda::LOCAL_1, Tienda::LOCAL_2, Tienda::LOCAL_3]),
        'tipo_pago_id' => $tipoPago->id,
        'observaciones' => $faker->paragraph(),
        'direccion' => $client->direccion,
        'nombre_entrega' => $client->full_name,
        'telefono' => $client->telefono,
        'correo' => $client->email,
        'web' => random_int(0,1),
        'cod_web_pay' => $codWebPay,
    ];
});
