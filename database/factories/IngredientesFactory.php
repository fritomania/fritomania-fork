<?php

use App\Models\IngredienteCategoria;
use Faker\Generator as Faker;

$factory->define(\App\Models\Ingrediente::class, function (Faker $faker) {

    $cat = IngredienteCategoria::all()->random()->id;

    return [
        'ingrediente_categoria_id' => $cat,
        'nombre' => $faker->unique()->word,
        'imagen' => \Faker\Provider\Image::image(storage_path() . '/app/public/ingredientes', 600, 350, 'food', false),
    ];
});


