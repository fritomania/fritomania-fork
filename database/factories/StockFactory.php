<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\Stock::class, function (Faker $faker) {

    $cantidad = rand(20,70);
    return [
        'tienda_id' => \App\Models\Tienda::all()->random()->id,
        'item_id' => \App\Models\Item::all()->random()->id,
        'lote' => null,
//        'fecha_ing',
//        'fecha_ven',
        'cantidad' => $cantidad,
        'cnt_ini' => $cantidad,
        'orden_salida' => 0
    ];
});
