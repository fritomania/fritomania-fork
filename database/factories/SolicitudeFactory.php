<?php

use App\Models\SolicitudEstado;
use App\Models\Tienda;
use App\User;
use Faker\Generator as Faker;

$factory->define(\App\Models\Solicitude::class, function (Faker $faker) {

    return [
        'tienda_solicita' => Tienda::all()->random()->id,
        'tienda_entrega' => Tienda::all()->random()->id,
        'numero' => 0,
        'observaciones' => $faker->paragraph,
        'user_id' => User::all()->random()->id,
        'user_despacha' => null,
        'solicitud_estado_id' => SolicitudEstado::all()->random()->id,
    ];
});
