<?php

use App\Models\Item;
use App\Models\Produccione;
use App\Models\ProductoSolicitado;
use Faker\Generator as Faker;

$factory->define(ProductoSolicitado::class, function (Faker $faker) {
    return [
        'produccione_id' => Produccione::all()->random()->id,
        'item_id' => Item::all()->random()->id,
        'cantidad' => random_int(5,40),
        'estado' => $faker->randomElement([ProductoSolicitado::PENDIENTE,ProductoSolicitado::INCOMPLETO,ProductoSolicitado::COMPLETADO])
    ];
});
