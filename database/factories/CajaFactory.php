<?php

use App\Models\Caja;
use App\Models\Tienda;
use App\User;
use Faker\Generator as Faker;

$factory->define(Caja::class, function (Faker $faker) {
    return [
        'monto_apertura' => rand(10,100),
        'fecha_cierre' => null,
        'monto_cierre' => 0,
        'user_id' => User::all()->random()->id,
        'tienda_id' => Tienda::all()->random()->id
    ];
});
