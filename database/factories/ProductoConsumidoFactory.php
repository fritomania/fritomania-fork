<?php

use App\Models\Item;
use App\Models\Produccione;
use App\Models\ProductoConsumido;
use App\User;
use Faker\Generator as Faker;

$factory->define(ProductoConsumido::class, function (Faker $faker) {
    return [
        'produccione_id' => Produccione::all()->random()->id,
        'item_id' => Item::all()->random()->id,
        'cantidad_receta' => random_int(5,40),
        'cantidad_ingresada' => random_int(5,40),
        'user_id' => User::all()->random()->id
    ];
});
