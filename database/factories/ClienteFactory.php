<?php

use App\Models\Cliente;
use Faker\Generator as Faker;

$factory->define(Cliente::class, function (Faker $faker) {
    return [
        'nit' => $faker->randomNumber(  8),
        'dpi' => $faker->randomNumber(9).$faker->randomNumber(4),
        'nombres' => $faker->name,
        'apellidos' => $faker->lastName,
        'telefono' => $faker->randomNumber(8),
        'email' => $faker->email,
        'genero' => $faker->randomElement(['M', 'F']),
        'direccion' => $faker->address
    ];
});
