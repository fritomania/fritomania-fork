<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\VentaDetalle::class, function (Faker $faker) {

    $item= \App\Models\Item::all()->random();

    return [
        'venta_id' => \App\Models\Venta::all()->random()->id,
        'item_id' => $item->id,
        'cantidad' => random_int(1,25),
        'precio' => $item->precio_venta,
        'descuento' => rand(0,35)/100
    ];
});
