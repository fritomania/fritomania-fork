<?php

use App\Models\IngredienteCategoria;
use Faker\Generator as Faker;

$factory->define(IngredienteCategoria::class, function (Faker $faker) {
    return [
        'nombre' => $faker->unique()->word
    ];
});
