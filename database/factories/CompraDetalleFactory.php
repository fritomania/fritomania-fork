<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\CompraDetalle::class, function (Faker $faker) {

    $item= \App\Models\Item::all()->random();

    return [
        'compra_id' => '',
        'item_id' => $item->id,
        'cantidad' => rand(3,50),
        'precio' => $item->precio_compra,
        'descuento' => $faker->randomFloat(2, 0.0,0.25),
    ];
});
