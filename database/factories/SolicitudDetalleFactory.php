<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\SolicitudDetalle::class, function (Faker $faker) {
    return [
        'solicitude_id' => \App\Models\Solicitude::all()->random()->id,
        'item_id' => \App\Models\Item::all()->random()->id,
        'cantidad' => rand(5,20),
        'precio' => 0
    ];
});
