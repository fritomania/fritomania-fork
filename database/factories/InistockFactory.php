<?php

use App\Models\Item;
use App\Models\Tienda;
use App\User;
use Faker\Generator as Faker;

$factory->define(\App\Models\Inistock::class, function (Faker $faker) {
    return [
        'item_id' => Item::all()->random()->id,
        'tienda_id' => Tienda::all()->random()->id,
        'cantidad' => 0,
        'user_id' => User::all()->random()->id
    ];
});
