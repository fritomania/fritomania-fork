<?php

use App\Models\Tienda;
use App\User;
use Faker\Generator as Faker;

$factory->define(\App\Models\Compra::class, function (Faker $faker) {

    $dias = random_int(-60,0);
    $credito = random_int(0,1);
    $fechaPromesa = hoyYmas( $dias + 7 );
    $created = hoyYmas( $dias );
    $estado = \App\Models\Cestado::all()->random()->id;

    if($credito == 1){
        $fechaCredito = hoyYmas($dias + 30 );
    }else{
        $fechaCredito = Null;
    }

    if($estado == 3 ){
        $fechaIngreso = hoyYmas( $dias + random_int(-3,7));
        $fechaSistema = hoyYmas($dias + random_int(-3,0));

        if($credito == 1){
            $pagada = random_int(0,1);
        }else{
            $pagada = 1;
        }
    }else{
        $fechaIngreso = Null;
        $fechaSistema = Null;
        if($estado == 1){
            $pagada = 0;
        }else{
            $pagada = random_int(0,1);
        }
    }

//    dd($fechaSistema);

    return [
        'proveedor_id' => \App\Models\Proveedor::all()->random()->id,
        'tcomprobante_id' => 2,
        'fecha_ingreso' => fechaDb($fechaIngreso),
        'fecha_ingreso_plan' => fechaDb($fechaPromesa),
        'fecha' => fechaDb($fechaSistema),
        'credito' => $credito,
        'serie' => $faker->randomNumber(4),
        'numero' => $faker->randomNumber(7),
        'pagada' => $pagada,
        'user_id' => User::all()->random()->id,
        'tienda_id' => Tienda::all()->random()->id,
        'cestado_id' => $estado,
        'fecha_limite_credito' => fechaDb($fechaCredito),
        'created_at' => fechaDb($created),
    ];
});
