<?php

use App\Models\Proveedor;
use Faker\Generator as Faker;

$factory->define(Proveedor::class, function (Faker $faker) {
    return [
        'nit' => $faker->randomNumber(  8),
        'nombre' => $faker->name,
        'razon_social' => $faker->company,
        'correo' => $faker->email,
        'telefono_movil' => $faker->randomNumber(8),
        'telefono_oficina' => $faker->randomNumber(8),
        'direccion' => $faker->address,
        'observaciones' => ''
    ];

});
