<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\Receta::class, function (Faker $faker) {
    return [
        'item_id'=> \App\Models\Item::productoFinal()->get()->random()->id,
        'cantidad' => 1,
        'procedimiento'=> '',
        'user_id' => \App\User::all()->random()->id
    ];
});
