<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToCompraVentaTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('compra_venta', function(Blueprint $table)
		{
			$table->foreign('compra_id', 'fk_compras_has_ventas_compras1')->references('id')->on('compras')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('venta_id', 'fk_compras_has_ventas_ventas1')->references('id')->on('ventas')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('compra_venta', function(Blueprint $table)
		{
			$table->dropForeign('fk_compras_has_ventas_compras1');
			$table->dropForeign('fk_compras_has_ventas_ventas1');
		});
	}

}
