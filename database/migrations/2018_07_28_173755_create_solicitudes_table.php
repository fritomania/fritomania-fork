<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSolicitudesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('solicitudes', function(Blueprint $table)
		{
			$table->integer('id', true);
            $table->integer('tienda_solicita')->index('fk_solicitudes_tiendas1_idx')->nullable();
            $table->integer('tienda_entrega')->index('fk_solicitudes_tiendas2_idx')->nullable();
			$table->integer('numero')->nullable();
			$table->text('observaciones')->nullable();
			$table->integer('user_id')->unsigned()->index('fk_solicitudes_users1_idx');
			$table->integer('user_despacha')->unsigned()->index('fk_solicitudes_users2_idx')->nullable();
			$table->dateTime('fecha_despacha')->nullable();
            $table->dateTime('fecha_cancela')->nullable();
            $table->dateTime('fecha_anula')->nullable();
            $table->integer('solicitud_estado_id')->index('fk_solicitudes_solicitud_estados1_idx');
            $table->timestamps();
            $table->softDeletes();


            $table->foreign('tienda_solicita')->references('id')->on('tiendas');
            $table->foreign('tienda_entrega')->references('id')->on('tiendas');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('solicitudes');
	}

}
