<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToProductoConsumidosTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('producto_consumidos', function(Blueprint $table)
        {
            $table->foreign('item_id', 'fk_producto_consumidos_items1')->references('id')->on('items')->onUpdate('NO ACTION')->onDelete('NO ACTION');
            $table->foreign('produccione_id', 'fk_producto_consumidos_producciones1')->references('id')->on('producciones')->onUpdate('NO ACTION')->onDelete('NO ACTION');
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('producto_consumidos', function(Blueprint $table)
        {
            $table->dropForeign('fk_producto_consumidos_items1');
            $table->dropForeign('fk_producto_consumidos_producciones1');
        });
    }

}
