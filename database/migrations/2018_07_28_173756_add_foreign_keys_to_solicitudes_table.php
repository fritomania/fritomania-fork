<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToSolicitudesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('solicitudes', function(Blueprint $table)
		{
			$table->foreign('solicitud_estado_id', 'fk_solicitudes_solicitud_estados1')->references('id')->on('solicitud_estados')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('user_id', 'fk_solicitudes_users1')->references('id')->on('users')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('user_despacha', 'fk_solicitudes_users2')->references('id')->on('users')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('solicitudes', function(Blueprint $table)
		{
			$table->dropForeign('fk_solicitudes_solicitud_estados1');
			$table->dropForeign('fk_solicitudes_users1');
			$table->dropForeign('fk_solicitudes_users2');
		});
	}

}
