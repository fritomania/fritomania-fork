<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVistaTopTenMes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
            create or replace view top_ten_mes as
            select 
                i.id,i.nombre,count(*) cnt 
            from 
                venta_detalles d inner join items i on d.item_id=i.id
            where 
                venta_id in (
                    select 
                        id 
                    from 
                        ventas 
                    where 
                        date(created_at) between curdate()-30 and curdate()
                        and vestado_id not in (2,8)
                )
            group by 
                1,2 
            order by 
                3 desc limit 10
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('DROP VIEW IF EXISTS top_ten_mes');
    }
}
