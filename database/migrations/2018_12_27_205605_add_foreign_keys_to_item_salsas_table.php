<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToItemSalsasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('item_salsas', function(Blueprint $table)
		{
			$table->foreign('item', 'fk_items_has_items_items3')->references('id')->on('items')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('salsa', 'fk_items_has_items_items4')->references('id')->on('items')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('item_salsas', function(Blueprint $table)
		{
			$table->dropForeign('fk_items_has_items_items3');
			$table->dropForeign('fk_items_has_items_items4');
		});
	}

}
