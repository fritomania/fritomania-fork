<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEgresosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('egresos', function(Blueprint $table)
		{
			$table->integer('venta_detalle_id')->index('fk_egresos_venta_detalles1_idx');
			$table->integer('stock_id')->index('fk_venta_has_stock_stock1_idx');
			$table->decimal('cantidad');
			$table->primary(['venta_detalle_id','stock_id']);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('egresos');
	}

}
