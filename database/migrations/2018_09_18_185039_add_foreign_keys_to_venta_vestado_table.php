<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToVentaVestadoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('venta_vestado', function(Blueprint $table)
		{
			$table->foreign('venta_id', 'fk_ventas_has_vestados_ventas1')->references('id')->on('ventas')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('vestado_id', 'fk_ventas_has_vestados_vestados1')->references('id')->on('vestados')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('venta_vestado', function(Blueprint $table)
		{
			$table->dropForeign('fk_ventas_has_vestados_ventas1');
			$table->dropForeign('fk_ventas_has_vestados_vestados1');
		});
	}

}
