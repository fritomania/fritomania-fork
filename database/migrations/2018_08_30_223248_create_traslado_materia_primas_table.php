<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTrasladoMateriaPrimasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('traslado_materia_primas', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('produccione_id')->index('fk_traslado_materia_prima_producciones1_idx');
			$table->integer('item_id')->index('fk_traslado_materia_prima_items1_idx');
			$table->decimal('cantidad', 12,4);
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('traslado_materia_primas');
	}

}
