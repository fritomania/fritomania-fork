<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOrdenDetallesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('orden_detalles', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('ordene_id')->index('fk_orden_detalles_ordenes1_idx');
			$table->integer('item_id')->index('fk_orden_detalles_items1_idx');
			$table->decimal('precio', 10,10,0);
			$table->decimal('cantidad', 10);
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('orden_detalles');
	}

}
