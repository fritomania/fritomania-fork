<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProductoSolicitadosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('producto_solicitados', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('produccione_id')->index('fk_producto_solicitados_producciones1_idx');
			$table->integer('item_id')->index('fk_producto_solicitados_items1_idx');
			$table->decimal('cantidad', 12,4);
			$table->enum('estado', array('PENDIENTE','INCOMPLETO','COMPLETADO'))->nullable()->default('PENDIENTE');
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('producto_solicitados');
	}

}
