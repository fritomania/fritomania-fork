<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEgresoMateriaPrimaTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('egreso_materia_prima', function(Blueprint $table)
		{
			$table->integer('stock_id');
			$table->integer('traslado_materia_prima_id')->index('fk_egreso_materia_prima_traslado_materia_primas1_idx');
            $table->decimal('cantidad',12,4);
			$table->primary(['stock_id','traslado_materia_prima_id']);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('egreso_materia_prima');
	}

}
