<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToCajasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('cajas', function(Blueprint $table)
		{
			$table->foreign('tienda_id', 'cajas_tiendas_fk')->references('id')->on('tiendas')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('user_id', 'cajas_users_fk')->references('id')->on('users')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('cajas', function(Blueprint $table)
		{
			$table->dropForeign('cajas_tiendas_fk');
			$table->dropForeign('cajas_users_fk');
		});
	}

}
