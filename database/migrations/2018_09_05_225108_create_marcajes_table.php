<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMarcajesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('marcajes', function(Blueprint $table)
        {
            $table->integer('id',true);
            $table->integer('tipo_marcaje_id')->index('fk_marcajes_tipo_marcajes1_idx');
            $table->integer('user_id')->unsigned()->index('fk_marcajes_users1_idx');
            $table->date('fecha');
            $table->time('hora');
            $table->timestamps();
            $table->softDeletes();
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('marcajes');
    }

}
