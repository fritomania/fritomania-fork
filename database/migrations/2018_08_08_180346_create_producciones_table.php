<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProduccionesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('producciones', function(Blueprint $table)
        {
            $table->integer('id',true);
            $table->string('numero', 45)->nullable()->unique('numero_UNIQUE');
            $table->integer('produccion_tipo_id')->index('fk_producciones_produccion_tipos1_idx');
            $table->integer('produccion_estado_id')->index('fk_producciones_produccion_estados1_idx');
            $table->integer('user_id')->unsigned()->index('fk_producciones_users1_idx');
            $table->dateTime('fecha_cancela')->nullable();
            $table->dateTime('fecha_anula')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('producciones');
    }

}
