<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToPedidoVentasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('pedido_ventas', function(Blueprint $table)
		{
			$table->foreign('pedido_id', 'fk_pedidos_has_ventas_pedidos1')->references('id')->on('pedidos')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('venta_id', 'fk_pedidos_has_ventas_ventas1')->references('id')->on('ventas')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('pedido_ventas', function(Blueprint $table)
		{
			$table->dropForeign('fk_pedidos_has_ventas_pedidos1');
			$table->dropForeign('fk_pedidos_has_ventas_ventas1');
		});
	}

}
