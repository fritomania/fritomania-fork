<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToTiendaDespachaTiendaTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('tienda_despacha_tienda', function(Blueprint $table)
		{
			$table->foreign('tienda_despacha', 'fk_tiendas_has_tiendas_tiendas1')->references('id')->on('tiendas')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('tiendas_solicita', 'fk_tiendas_has_tiendas_tiendas2')->references('id')->on('tiendas')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('tienda_despacha_tienda', function(Blueprint $table)
		{
			$table->dropForeign('fk_tiendas_has_tiendas_tiendas1');
			$table->dropForeign('fk_tiendas_has_tiendas_tiendas2');
		});
	}

}
