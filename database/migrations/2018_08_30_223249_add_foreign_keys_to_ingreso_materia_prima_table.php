<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToIngresoMateriaPrimaTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('ingreso_materia_prima', function(Blueprint $table)
		{
			$table->foreign('stock_id', 'fk_ingreso_materia_prima_stocks1')->references('id')->on('stocks')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('traslado_materia_prima_id', 'fk_ingreso_materia_prima_traslado_materia_primas1')->references('id')->on('traslado_materia_primas')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('ingreso_materia_prima', function(Blueprint $table)
		{
			$table->dropForeign('fk_ingreso_materia_prima_stocks1');
			$table->dropForeign('fk_ingreso_materia_prima_traslado_materia_primas1');
		});
	}

}
