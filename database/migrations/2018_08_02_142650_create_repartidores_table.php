<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRepartidoresTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('repartidores', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('nombres', 100);
			$table->string('apellidos', 100);
			$table->integer('hora_inicia')->nullable();
			$table->integer('hora_fin')->nullable();
			$table->integer('user_id')->unsigned()->index('fk_repartidores_users1_idx');
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('repartidores');
	}

}
