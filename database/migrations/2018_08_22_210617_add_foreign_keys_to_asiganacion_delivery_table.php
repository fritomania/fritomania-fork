<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToAsiganacionDeliveryTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('asiganacion_delivery', function(Blueprint $table)
		{
			$table->foreign('user_id', 'fk_users_has_ventas_users1')->references('id')->on('users')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('venta_id', 'fk_users_has_ventas_ventas1')->references('id')->on('ventas')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('asiganacion_delivery', function(Blueprint $table)
		{
			$table->dropForeign('fk_users_has_ventas_users1');
			$table->dropForeign('fk_users_has_ventas_ventas1');
		});
	}

}
