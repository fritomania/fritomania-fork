<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNotificacionesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('notificaciones', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('mensaje');
			$table->integer('user_id')->unsigned()->index('fk_notificaciones_users1_idx');
			$table->integer('notificacion_tipo_id')->index('fk_notificaciones_notificacion_tipos1_idx')->default(1);
			$table->boolean('vista')->default(0);
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('notificaciones');
	}

}
