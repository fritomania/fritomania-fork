<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToNotificacionesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('notificaciones', function(Blueprint $table)
		{
			$table->foreign('notificacion_tipo_id', 'fk_notificaciones_notificacion_tipos1')->references('id')->on('notificacion_tipos')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('user_id', 'fk_notificaciones_users1')->references('id')->on('users')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('notificaciones', function(Blueprint $table)
		{
			$table->dropForeign('fk_notificaciones_notificacion_tipos1');
			$table->dropForeign('fk_notificaciones_users1');
		});
	}

}
