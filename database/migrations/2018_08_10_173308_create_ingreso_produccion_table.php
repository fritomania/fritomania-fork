<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateIngresoProduccionTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('ingreso_produccion', function(Blueprint $table)
		{
			$table->integer('producto_realizado_id')->index('fk_producto_realizados_has_stocks_producto_realizados1_idx');
			$table->integer('stock_id')->index('fk_producto_realizados_has_stocks_stocks1_idx');
			$table->decimal('cantidad', 12,4);
			$table->primary(['producto_realizado_id','stock_id']);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('ingreso_produccion');
	}

}
