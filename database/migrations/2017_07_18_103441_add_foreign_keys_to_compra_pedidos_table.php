<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToCompraPedidosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('compra_pedidos', function(Blueprint $table)
		{
			$table->foreign('compra_id', 'fk_compras_has_pedidos_compras1')->references('id')->on('compras')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('pedido_id', 'fk_compras_has_pedidos_pedidos1')->references('id')->on('pedidos')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('compra_pedidos', function(Blueprint $table)
		{
			$table->dropForeign('fk_compras_has_pedidos_compras1');
			$table->dropForeign('fk_compras_has_pedidos_pedidos1');
		});
	}

}
