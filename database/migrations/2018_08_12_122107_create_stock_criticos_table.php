<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateStockCriticosTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stock_criticos', function(Blueprint $table)
        {
            $table->integer('item_id')->index('fk_items_has_tiendas_items1_idx');
            $table->integer('tienda_id')->index('fk_items_has_tiendas_tiendas1_idx');
            $table->decimal('cantidad', 10);
            $table->primary(['item_id','tienda_id']);
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('stock_criticos');
    }

}
