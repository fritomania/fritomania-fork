<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToStockCriticosTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('stock_criticos', function(Blueprint $table)
        {
            $table->foreign('item_id', 'fk_items_has_tiendas_items1')->references('id')->on('items')->onUpdate('NO ACTION')->onDelete('NO ACTION');
            $table->foreign('tienda_id', 'fk_items_has_tiendas_tiendas1')->references('id')->on('tiendas')->onUpdate('NO ACTION')->onDelete('NO ACTION');
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('stock_criticos', function(Blueprint $table)
        {
            $table->dropForeign('fk_items_has_tiendas_items1');
            $table->dropForeign('fk_items_has_tiendas_tiendas1');
        });
    }

}
