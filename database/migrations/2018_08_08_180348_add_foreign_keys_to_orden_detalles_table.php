<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToOrdenDetallesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('orden_detalles', function(Blueprint $table)
		{
			$table->foreign('item_id', 'fk_orden_detalles_items1')->references('id')->on('items')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('ordene_id', 'fk_orden_detalles_ordenes1')->references('id')->on('ordenes')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('orden_detalles', function(Blueprint $table)
		{
			$table->dropForeign('fk_orden_detalles_items1');
			$table->dropForeign('fk_orden_detalles_ordenes1');
		});
	}

}
