<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateItemExtrasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('item_extras', function(Blueprint $table)
		{
			$table->integer('item')->index('fk_items_has_items_items1_idx');
			$table->integer('extra')->index('fk_items_has_items_items2_idx');
			$table->primary(['item','extra']);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('item_extras');
	}

}
