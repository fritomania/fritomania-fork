<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCpagosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('cpagos', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('compra_id')->index('fk_cpagos_compras1_idx');
			$table->decimal('monto',10,0);
			$table->integer('user_id')->unsigned()->index('fk_cpagos_users1_idx');
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('cpagos');
	}

}
