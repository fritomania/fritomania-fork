<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEmpleadosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('empleados', function(Blueprint $table)
		{
			$table->integer('id', true);
            $table->integer('tienda_id')->index('fk_empleados_tiendas1_idx');
			$table->string('nombres', 100);
			$table->string('apellidos', 100);
			$table->char('telefono', 8)->nullable()->unique('telefono_UNIQUE');
			$table->string('correo', 100)->nullable()->unique('correo_UNIQUE');
			$table->decimal('sueldo_diario', 10,0)->default(0.00);
			$table->tinyInteger('porcentaje_comision',false,true)->default(0);
            $table->enum('genero',['M', 'F']);
			$table->string('puesto', 50)->nullable();
			$table->date('fecha_contratacion')->nullable();
			$table->text('direccion', 65535)->nullable();
			$table->date('fecha_nacimiento')->nullable();
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('empleados');
	}

}
