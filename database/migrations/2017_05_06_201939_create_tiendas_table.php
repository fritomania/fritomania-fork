<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTiendasTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tiendas', function(Blueprint $table)
        {
            $table->integer('id', true);
            $table->string('nombre', 100)->unique('nombre_UNIQUE');
            $table->integer('admin')->unsigned()->nullable()->index('fk_tiendas_users1_idx');
            $table->text('direccion', 65535)->nullable();
            $table->char('telefono', 30)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tiendas');
    }

}
