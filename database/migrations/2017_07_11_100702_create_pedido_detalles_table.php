<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePedidoDetallesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('pedido_detalles', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('pedido_id')->index('fk_pedido_detalles_pedidos1_idx');
			$table->integer('item_id')->index('fk_pedido_detalles_items1_idx');
			$table->decimal('cantidad');
			$table->decimal('precio',10,0);
			$table->decimal('descuento',10,0)->default(0.00);
			$table->boolean('ingreso')->default(1);
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('pedido_detalles');
	}

}
