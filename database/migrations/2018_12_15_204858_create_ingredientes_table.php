<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateIngredientesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ingredientes', function(Blueprint $table)
        {
            $table->integer('id', true);
            $table->integer('ingrediente_categoria_id')->index('fk_ingredientes_ingrediente_tipos1_idx');
            $table->string('nombre')->unique('nombre_UNIQUE');
            $table->string('imagen')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ingredientes');
    }

}
