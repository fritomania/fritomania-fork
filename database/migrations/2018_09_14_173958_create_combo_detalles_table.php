<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateComboDetallesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('combo_detalles', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('combo_id')->index('fk_combo_detalles_combos1_idx');
			$table->integer('item_id')->index('fk_combo_detalles_items1_idx');
			$table->enum('tipo',['BEBIDA','PIEZA'])->default('PIEZA');
			$table->integer('cantidad');
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('combo_detalles');
	}

}
