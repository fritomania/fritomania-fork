<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToVentaRecetaDetallesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('venta_receta_detalles', function(Blueprint $table)
		{
			$table->foreign('ingrediente_id', 'fk_venta_receta_detalles_ingredientes1')->references('id')->on('ingredientes')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('venta_receta_id', 'fk_venta_receta_detalles_venta_recetas1')->references('id')->on('venta_recetas')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('venta_receta_detalles', function(Blueprint $table)
		{
			$table->dropForeign('fk_venta_receta_detalles_ingredientes1');
			$table->dropForeign('fk_venta_receta_detalles_venta_recetas1');
		});
	}

}
