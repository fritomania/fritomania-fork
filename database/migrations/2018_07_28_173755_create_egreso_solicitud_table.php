<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEgresoSolicitudTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('egreso_solicitud', function(Blueprint $table)
		{
			$table->integer('solicitud_detalle_id')->index('fk_solicitud_detalles_has_stocks_solicitud_detalles1_idx');
			$table->integer('stock_id')->index('fk_solicitud_detalles_has_stocks_stocks1_idx');
			$table->decimal('cantidad', 10);
			$table->primary(['solicitud_detalle_id','stock_id']);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('egreso_solicitud');
	}

}
