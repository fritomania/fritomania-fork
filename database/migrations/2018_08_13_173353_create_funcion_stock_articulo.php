<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFuncionStockArticulo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared("
            CREATE FUNCTION stockArticulo(id_item int(11),id_tienda int(11)) 
            RETURNS decimal(10,2)
                SQL SECURITY DEFINER NOT DETERMINISTIC CONTAINS SQL
            BEGIN
                if id_tienda is null then
                    RETURN (select IFNULL(SUM(cantidad),0) from stocks where item_id=id_item);
                else
                    RETURN (select IFNULL(SUM(cantidad),0) from stocks where item_id=id_item and tienda_id=id_tienda);
               end if;
            END
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared("drop function if exists stockArticulo");
    }
}
