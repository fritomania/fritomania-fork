<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFnCostoPromedioItem extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared("
            CREATE FUNCTION costoPromedioItem(itemId int) 
            RETURNS decimal(8,2)
                SQL SECURITY DEFINER NOT DETERMINISTIC CONTAINS SQL
            BEGIN
                return (
                    select 
                        ifnull(round( sum(d.precio*d.cantidad) / sum(d.cantidad) , 2 ),0) costo
                    from 
                        compras c inner join compra_detalles d on d.compra_id=c.id
                    where 
                        c.cestado_id != 2
                        and c.tcomprobante_id in (2,4)
                        and d.deleted_at is null
                        and c.deleted_at is null
                        and d.precio>0
                        and d.item_id=itemId
                );
            END
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared("DROP FUNCTION costoPromedioItem");
    }
}
