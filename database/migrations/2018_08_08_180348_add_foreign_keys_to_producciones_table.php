<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToProduccionesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('producciones', function(Blueprint $table)
        {
            $table->foreign('produccion_estado_id', 'fk_producciones_produccion_estados1')->references('id')->on('produccion_estados')->onUpdate('NO ACTION')->onDelete('NO ACTION');
            $table->foreign('produccion_tipo_id', 'fk_producciones_produccion_tipos1')->references('id')->on('produccion_tipos')->onUpdate('NO ACTION')->onDelete('NO ACTION');
            $table->foreign('user_id', 'fk_producciones_users1')->references('id')->on('users')->onUpdate('NO ACTION')->onDelete('NO ACTION');
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('producciones', function(Blueprint $table)
        {
            $table->dropForeign('fk_producciones_produccion_estados1');
            $table->dropForeign('fk_producciones_produccion_tipos1');
            $table->dropForeign('fk_producciones_users1');
        });
    }

}
