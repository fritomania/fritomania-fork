<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUserDespachaUserTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('user_despacha_user', function(Blueprint $table)
		{
			$table->integer('user_sol')->unsigned()->index('fk_users_has_users_users1_idx');
			$table->integer('user_des')->unsigned()->index('fk_users_has_users_users2_idx');
			$table->primary(['user_sol','user_des']);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('user_despacha_user');
	}

}
