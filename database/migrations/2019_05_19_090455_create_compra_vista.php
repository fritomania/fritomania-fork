<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompraVista extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
            CREATE OR REPLACE VIEW vista_compras AS
            select 
                c.id
                ,DATE_FORMAT(c.created_at,'%d/%m/%Y') as creada
                ,DATE_FORMAT(c.fecha,'%d/%m/%Y') as fecha_documento
                ,DATE_FORMAT(c.fecha_ingreso,'%d/%m/%Y') as fecha_ingreso
                ,DATE_FORMAT(c.fecha_ingreso_plan,'%d/%m/%Y') as fecha_ingreso_plan
                ,DATE_FORMAT(c.fecha_limite_credito,'%d/%m/%Y') as fecha_credito
                ,concat(c.serie,' ',c.numero) as ns
                ,p.nombre proveedor
                ,tc.nombre tipo
                ,ce.descripcion estado
                ,time(c.created_at) as hora
                ,u.name usuario
                ,c.credito
                ,c.pagada
                ,c.cestado_id
                ,c.tienda_id
                ,c.user_id
                ,c.proveedor_id
                ,saldoCompra(c.id,c.credito) as saldo
                ,c.created_at
            from 
                compras c left join proveedores p on c.proveedor_id=p.id
                inner join tcomprobantes tc on c.tcomprobante_id= tc.id
                inner join cestados ce on c.cestado_id= ce.id
                left join users u on u.id=c.user_id
            order by 
                c.created_at desc
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('DROP VIEW IF EXISTS vista_compras');
    }
}
