<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDescuentosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('descuentos', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('item_id')->index('fk_descuentos_items1_idx');
			$table->tinyInteger('porcentaje',false,true);
			$table->date('fecha_inicio')->nullable();
			$table->date('fecha_expira')->nullable();
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('descuentos');
	}

}
