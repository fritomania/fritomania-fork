<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFnSaldoVenta extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared("
            CREATE FUNCTION saldoVenta(ventaId int,credito tinyint(1)) 
            RETURNS decimal(10)
                SQL SECURITY DEFINER NOT DETERMINISTIC CONTAINS SQL
            BEGIN
                if credito=0 then
                    return 0;
                else
                    RETURN totalVenta(ventaId)-(select IFNULL(SUM(monto),0) from vpagos where venta_id = ventaId and deleted_at is null);
                end if;
            END
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared("DROP FUNCTION saldoVenta");
    }
}
