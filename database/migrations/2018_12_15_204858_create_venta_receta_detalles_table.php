<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateVentaRecetaDetallesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('venta_receta_detalles', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('venta_receta_id')->index('fk_venta_receta_detalles_venta_recetas1_idx');
			$table->integer('ingrediente_id')->index('fk_venta_receta_detalles_ingredientes1_idx');
			$table->decimal('cantidad', 10)->nullable();
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('venta_receta_detalles');
	}

}
