<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProveedoresTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('proveedores', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->char('nit', 10)->nullable();
			$table->string('nombre', 45);
			$table->string('razon_social')->nullable()->unique('razon_social_UNIQUE');
			$table->string('correo', 100)->nullable();
			$table->char('telefono_movil', 8)->nullable();
			$table->char('telefono_oficina', 8)->nullable();
			$table->text('direccion', 65535)->nullable();
			$table->text('observaciones', 65535)->nullable();
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('proveedores');
	}

}
