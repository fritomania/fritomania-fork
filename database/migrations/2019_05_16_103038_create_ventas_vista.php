<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVentasVista extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
            create or replace view vista_ventas as
            select 
                v.id
                ,concat(COALESCE(c.nit,''),' ',c.nombres,' ',c.apellidos) as cliente
                ,concat(v.serie,' ',v.numero) as ns
                ,DATE_FORMAT(v.fecha,'%d/%m/%Y') as fecha
                ,time(v.created_at) as hora
                ,s.descripcion as estado
                ,u.name as usuario
                ,tp.nombre as tipo_pago
                ,u.id as user_id
                ,v.credito
                ,v.pagada
				,v.descuento
                ,v.vestado_id
                ,v.cliente_id
                ,v.tipo_pago_id
                ,v.tienda_id
                ,saldoVenta(v.id,v.credito) as saldo
                ,v.created_at
            from 
                ventas v inner join vestados s on v.vestado_id=s.id
                left join users u on v.user_id= u.id
                left join clientes c on c.id= v.cliente_id
                left join tipo_pagos tp on tp.id = v.tipo_pago_id
            order by
                v.created_at desc
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('DROP VIEW IF EXISTS vista_ventas');
    }
}
