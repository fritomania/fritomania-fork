<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCajasTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cajas', function(Blueprint $table)
        {
            $table->integer('id', true);
            $table->decimal('monto_apertura', 10, 0);
            $table->decimal('monto_cierre', 10, 0)->default(0);
            $table->dateTime('fecha_cierre')->nullable();
            $table->integer('user_id')->unsigned()->index('user_id');
            $table->integer('tienda_id')->index('caja_id');
            $table->timestamps();
            $table->softDeletes();
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cajas');
    }

}
