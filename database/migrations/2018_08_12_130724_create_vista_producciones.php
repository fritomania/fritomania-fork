<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVistaProducciones extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
            Create or replace view vista_producciones as
            select 
                p.*
                ,pt.nombre as tipo
                ,pe.nombre as estado 
                ,u.name as usuario

            from 
                producciones p
					 join produccion_tipos pt
					 		on p.produccion_tipo_id = pt.id
                join produccion_estados pe
					 		on p.produccion_estado_id = pe.id
                join users u
					 		on p.user_id = u.id
            order by 
                p.created_at desc
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('DROP VIEW IF EXISTS vista_producciones');
    }
}
