<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToOrdenesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('ordenes', function(Blueprint $table)
		{
			$table->foreign('orden_estado_id', 'fk_ordenes_orden_estados1')->references('id')->on('orden_estados')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('user_id', 'fk_ordenes_users1')->references('id')->on('users')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('receptor', 'fk_ordenes_users2')->references('id')->on('users')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('cocinero', 'fk_ordenes_users3')->references('id')->on('users')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('ordenes', function(Blueprint $table)
		{
			$table->dropForeign('fk_ordenes_orden_estados1');
			$table->dropForeign('fk_ordenes_users1');
			$table->dropForeign('fk_ordenes_users2');
			$table->dropForeign('fk_ordenes_users3');
		});
	}

}
