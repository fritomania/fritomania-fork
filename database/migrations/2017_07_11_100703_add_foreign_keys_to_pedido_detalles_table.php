<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToPedidoDetallesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('pedido_detalles', function(Blueprint $table)
		{
			$table->foreign('item_id', 'fk_pedido_detalles_items1')->references('id')->on('items')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('pedido_id', 'fk_pedido_detalles_pedidos1')->references('id')->on('pedidos')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('pedido_detalles', function(Blueprint $table)
		{
			$table->dropForeign('fk_pedido_detalles_items1');
			$table->dropForeign('fk_pedido_detalles_pedidos1');
		});
	}

}
