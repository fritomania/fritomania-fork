<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateIngredientesOpcionalesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('ingredientes_opcionales', function(Blueprint $table)
		{
			$table->integer('ingrediente_id')->index('fk_ingredientes_opcionales_ingredientes1_idx');
			$table->integer('venta_receta_detalle_id')->index('fk_ingredientes_opcionales_detalles_idx');
			$table->decimal('cantidad', 10)->nullable();
			$table->primary(['ingrediente_id','venta_receta_detalle_id'],'ingredientes_opcionales_primary');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('ingredientes_opcionales');
	}

}
