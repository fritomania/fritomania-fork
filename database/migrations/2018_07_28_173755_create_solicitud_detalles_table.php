<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSolicitudDetallesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('solicitud_detalles', function(Blueprint $table)
        {
            $table->integer('id', true);
            $table->integer('solicitude_id')->index('fk_solicitud_detalles_solicitudes1_idx');
            $table->integer('item_id')->index('fk_solicitud_detalles_items1_idx');
            $table->decimal('cantidad', 12,4);
            $table->decimal('precio', 12);
            $table->timestamps();
            $table->softDeletes();
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('solicitud_detalles');
    }

}
