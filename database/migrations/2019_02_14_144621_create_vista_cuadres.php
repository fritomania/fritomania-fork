<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVistaCuadres extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
            create or replace view vista_cuadres as
            select 
                c.*,
                date_format(c.fecha,'%d/%m/%Y') as fecha_format, 
                t.nombre tienda,
                u.name usuario
            from 
                cuadres c left join tiendas t on c.tienda_id=t.id 
                left join users u on c.user_id=u.id
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("DROP VIEW IF EXISTS vista_cuadres");
    }
}
