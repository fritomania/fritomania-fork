<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateVpagosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('vpagos', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('venta_id')->index('fk_vpagos_ventas1_idx');
			$table->decimal('monto',10,0);
			$table->integer('user_id')->unsigned()->index('fk_vpagos_users1_idx');
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('vpagos');
	}

}
