<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateIngresosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('ingresos', function(Blueprint $table)
		{
			$table->integer('compra_detalle_id')->index('fk_compra_detalles_has_stocks_compra_detalles1_idx');
			$table->integer('stock_id')->index('fk_compra_detalles_has_stocks_stocks1_idx');
			$table->decimal('cantidad');
			$table->primary(['compra_detalle_id','stock_id']);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('ingresos');
	}

}
