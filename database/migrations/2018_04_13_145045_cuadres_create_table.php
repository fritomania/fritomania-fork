<?php

use App\Models\Tienda;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CuadresCreateTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cuadres', function (Blueprint $table) {
            $table->increments('id');
            $table->date('fecha');
            $table->decimal('total_sistema',10,2);
            $table->decimal('cash',10,2);
            $table->integer('tienda_id')->default(Tienda::BODEGA_MP);
            $table->integer('user_id')->unsigned()->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('tienda_id')->references('id')->on('tiendas');
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cuadres');
    }
}
