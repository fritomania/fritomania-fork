<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEquivalenciasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('equivalencias', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('item_origen')->index('fk_equivalencias_items1_idx');
			$table->integer('item_destino')->index('fk_equivalencias_items2_idx');
			$table->decimal('cantidad');
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('equivalencias');
	}

}
