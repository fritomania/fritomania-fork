<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEgresoProduccionTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('egreso_produccion', function(Blueprint $table)
		{
			$table->integer('producto_consumido_id')->index('fk_producto_consumidos_has_stocks_producto_consumidos1_idx');
			$table->integer('stock_id')->index('fk_producto_consumidos_has_stocks_stocks1_idx');
			$table->decimal('cantidad', 12,4);
			$table->primary(['producto_consumido_id','stock_id']);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('egreso_produccion');
	}

}
