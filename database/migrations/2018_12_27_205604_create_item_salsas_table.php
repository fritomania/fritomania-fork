<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateItemSalsasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('item_salsas', function(Blueprint $table)
		{
			$table->integer('item')->index('fk_items_has_items_items3_idx');
			$table->integer('salsa')->index('fk_items_has_items_items4_idx');
			$table->primary(['item','salsa']);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('item_salsas');
	}

}
