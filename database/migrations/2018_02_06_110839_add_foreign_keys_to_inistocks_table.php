<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToInistocksTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('inistocks', function(Blueprint $table)
		{
			$table->foreign('item_id', 'fk_inistocks_items1')->references('id')->on('items')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('inistocks', function(Blueprint $table)
		{
			$table->dropForeign('fk_inistocks_items1');
		});
	}

}
