<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFnTotalCompra extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared("
            CREATE FUNCTION totalCompra(compraId int) 
            RETURNS decimal(10,2)
                SQL SECURITY DEFINER NOT DETERMINISTIC CONTAINS SQL
            BEGIN
                RETURN (select IFNULL(SUM(cantidad*precio),0) from compra_detalles where compra_id = compraId and deleted_at is null);
            END
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared("DROP FUNCTION totalCompra");
    }
}
