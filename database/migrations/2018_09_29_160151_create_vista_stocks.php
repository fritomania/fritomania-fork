<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVistaStocks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
            create or replace view vista_stocks as
            select 
                i.id,
                i.icategoria_id categoria_id,
                c.nombre categoria,
                i.codigo,
                i.nombre as nombre,
                t.id as tienda_id,
                t.nombre as tienda,
                i.precio_venta as precio,
                sum(s.cantidad) as stock
            from 
                stocks s inner join tiendas t on s.tienda_id=t.id
                inner join items i on s.item_id=i.id
                left join icategorias c on i.icategoria_id = c.id
            where
                s.deleted_at is null
            group by
                1,2,3,4,5,6,7,8
            order by
                5,6
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("drop view if exists vista_stocks ");
    }
}
