<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCompraPedidosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('compra_pedidos', function(Blueprint $table)
		{
			$table->integer('compra_id')->index('fk_compras_has_pedidos_compras1_idx');
			$table->integer('pedido_id')->index('fk_compras_has_pedidos_pedidos1_idx');
			$table->primary(['compra_id','pedido_id']);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('compra_pedidos');
	}

}
