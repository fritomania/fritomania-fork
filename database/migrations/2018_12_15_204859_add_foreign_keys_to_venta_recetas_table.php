<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToVentaRecetasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('venta_recetas', function(Blueprint $table)
		{
			$table->foreign('item_id', 'fk_venta_recetas_items1')->references('id')->on('items')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('venta_recetas', function(Blueprint $table)
		{
			$table->dropForeign('fk_venta_recetas_items1');
		});
	}

}
