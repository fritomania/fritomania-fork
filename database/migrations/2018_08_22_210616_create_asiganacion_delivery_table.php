<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAsiganacionDeliveryTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('asiganacion_delivery', function(Blueprint $table)
		{
			$table->integer('user_id')->unsigned()->index('fk_users_has_ventas_users1_idx');
			$table->integer('venta_id')->index('fk_users_has_ventas_ventas1_idx');
			$table->primary(['user_id','venta_id']);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('asiganacion_delivery');
	}

}
