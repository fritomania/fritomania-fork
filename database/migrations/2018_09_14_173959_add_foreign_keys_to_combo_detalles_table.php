<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToComboDetallesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('combo_detalles', function(Blueprint $table)
		{
			$table->foreign('combo_id', 'fk_combo_detalles_combos1')->references('id')->on('combos')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('item_id', 'fk_combo_detalles_items1')->references('id')->on('items')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('combo_detalles', function(Blueprint $table)
		{
			$table->dropForeign('fk_combo_detalles_combos1');
			$table->dropForeign('fk_combo_detalles_items1');
		});
	}

}
