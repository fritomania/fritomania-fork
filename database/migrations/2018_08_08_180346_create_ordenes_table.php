<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOrdenesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('ordenes', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->decimal('total', 10,10,0)->nullable();
			$table->text('comentarios', 65535)->nullable();
			$table->string('numero', 10)->nullable();
			$table->boolean('delivery')->nullable()->default(0);
			$table->integer('user_id')->unsigned()->index('fk_ordenes_users1_idx');
			$table->integer('receptor')->unsigned()->index('fk_ordenes_users2_idx');
			$table->integer('cocinero')->unsigned()->index('fk_ordenes_users3_idx');
			$table->integer('orden_estado_id')->default(1)->index('fk_ordenes_orden_estados1_idx');
			$table->dateTime('crearted_at')->nullable();
			$table->dateTime('updated_at')->nullable();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('ordenes');
	}

}
