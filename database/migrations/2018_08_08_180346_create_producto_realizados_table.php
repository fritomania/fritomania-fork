<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProductoRealizadosTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('producto_realizados', function(Blueprint $table)
        {
            $table->integer('id', true);
            $table->integer('produccione_id')->index('fk_producto_realizados_producciones1_idx');
            $table->integer('item_id')->index('fk_producto_realizados_items1_idx');
            $table->decimal('cantidad', 12,4);
            $table->integer('user_id')->unsigned()->index('fk_producto_realizados_users1_idx');
            $table->timestamps();
            $table->softDeletes();
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('producto_realizados');
    }

}
