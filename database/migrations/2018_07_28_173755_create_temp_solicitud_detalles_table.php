<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTempSolicitudDetallesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('temp_solicitud_detalles', function(Blueprint $table)
        {
            $table->integer('id', true);
            $table->integer('temp_solicitude_id')->index('fk_temp_solicitud_detalles_temp_solicitudes1_idx');
            $table->integer('item_id')->index('fk_temp_solicitud_detalles_items1_idx');
            $table->decimal('cantidad', 12,4)->nullable();
            $table->decimal('precio', 12,4)->nullable();
            $table->timestamps();
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('temp_solicitud_detalles');
    }

}
