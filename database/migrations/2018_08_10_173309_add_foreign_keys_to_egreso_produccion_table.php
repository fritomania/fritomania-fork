<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToEgresoProduccionTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('egreso_produccion', function(Blueprint $table)
		{
			$table->foreign('producto_consumido_id', 'fk_producto_consumidos_has_stocks_producto_consumidos1')->references('id')->on('producto_consumidos')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('stock_id', 'fk_producto_consumidos_has_stocks_stocks1')->references('id')->on('stocks')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('egreso_produccion', function(Blueprint $table)
		{
			$table->dropForeign('fk_producto_consumidos_has_stocks_producto_consumidos1');
			$table->dropForeign('fk_producto_consumidos_has_stocks_stocks1');
		});
	}

}
