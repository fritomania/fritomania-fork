<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProductoConsumidosTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('producto_consumidos', function(Blueprint $table)
        {
            $table->integer('id', true);
            $table->integer('produccione_id')->index('fk_producto_consumidos_producciones1_idx');
            $table->integer('item_id')->index('fk_producto_consumidos_items1_idx');
            $table->decimal('cantidad_receta', 12,4);
            $table->decimal('cantidad_ingresada', 12,4);
            $table->integer('user_id')->unsigned()->index('fk_producto_consumido_users1_idx');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('user_id')->references('id')->on('users');
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('producto_consumidos');
    }

}
