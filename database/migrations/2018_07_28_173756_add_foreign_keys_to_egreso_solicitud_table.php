<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToEgresoSolicitudTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('egreso_solicitud', function(Blueprint $table)
		{
			$table->foreign('solicitud_detalle_id', 'fk_solicitud_detalles_has_stocks_solicitud_detalles1')->references('id')->on('solicitud_detalles')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('stock_id', 'fk_solicitud_detalles_has_stocks_stocks1')->references('id')->on('stocks')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('egreso_solicitud', function(Blueprint $table)
		{
			$table->dropForeign('fk_solicitud_detalles_has_stocks_solicitud_detalles1');
			$table->dropForeign('fk_solicitud_detalles_has_stocks_stocks1');
		});
	}

}
