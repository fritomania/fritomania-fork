<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateIcategoriasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('icategorias', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('nombre', 45)->unique('nombre_UNIQUE');
			$table->text('descripcion')->nullable();
            $table->boolean('show_web')->default(0);
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('icategorias');
	}

}
