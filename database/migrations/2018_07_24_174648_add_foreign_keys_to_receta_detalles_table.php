<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToRecetaDetallesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('receta_detalles', function(Blueprint $table)
		{
			$table->foreign('item_id', 'fk_receta_detalles_item')->references('id')->on('items')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('receta_id', 'fk_receta_detalles_receta')->references('id')->on('recetas')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('receta_detalles', function(Blueprint $table)
		{
			$table->dropForeign('fk_receta_detalles_item');
			$table->dropForeign('fk_receta_detalles_receta');
		});
	}

}
