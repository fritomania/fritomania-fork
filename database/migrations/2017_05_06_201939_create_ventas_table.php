<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateVentasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('ventas', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('cliente_id')->index('fk_venta_cliente1_idx')->nullable();
			$table->integer('correlativo')->nullable();
			$table->date('fecha');
			$table->date('fecha_entrega')->nullable();
			$table->time('hora_entrega')->nullable();
			$table->string('serie', 45)->nullable();
			$table->string('numero', 45)->nullable();
			$table->decimal('recibido',10,0)->default(0);
            $table->decimal('monto_delivery',10,0)->default(0);
            $table->boolean('delivery')->default(0);
            $table->boolean('credito')->default(0);
            $table->boolean('pagada')->default(0);
            $table->text('direccion')->nullable();
            $table->text('observaciones')->nullable();
            $table->string('nombre_entrega')->nullable();
            $table->char('telefono',20)->nullable();
            $table->string('correo')->nullable();
            $table->boolean('retiro_en_local')->default(0);
            $table->boolean('web')->default(0);
            $table->integer('cod_web_pay')->nullable();
            $table->integer('vestado_id')->index('fk_ventas_estado_venta1_idx')->default(1);
            $table->integer('caja_id')->nullable()->index('fk_venta_caja1_idx');
            $table->integer('tienda_id')->nullable()->index('fk_venta_tienda1_idx');
            $table->integer('tipo_pago_id')->nullable()->index('fk_venta_tipo_pago1_idx');
            $table->decimal('descuento',10,0)->default(0)->nullable();
            $table->integer('user_id')->nullable();
            $table->dateTime('fecha_anula')->nullable();
            $table->dateTime('fecha_cancela')->nullable();
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('ventas');
	}

}
