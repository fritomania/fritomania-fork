<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateModificacionesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('modificaciones', function(Blueprint $table)
        {
            $table->integer('id')->primary();
            $table->integer('venta_detalle_id')->index('fk_modificaciones_venta_detalles1_idx');
            $table->integer('numero_item')->nullable();
            $table->string('ingredientes')->nullable();
            $table->timestamps();
            $table->softDeletes();

		});
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('modificaciones');
    }

}
