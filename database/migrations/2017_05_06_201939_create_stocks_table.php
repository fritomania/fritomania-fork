<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateStocksTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('stocks', function(Blueprint $table)
		{
			$table->integer('id',true);
			$table->integer('tienda_id')->index('fk_stocks_tiendas1_idx')->default(1);
			$table->integer('item_id')->index('fk_igresos_items1_idx');
			$table->string('lote', 25)->nullable();
			$table->timestamp('fecha_ing')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->date('fecha_ven')->nullable();
			$table->decimal('cantidad',12,4);
			$table->decimal('cnt_ini',12,4);
			$table->tinyInteger('orden_salida',false,true)->default(0);
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('stocks');
	}

}
