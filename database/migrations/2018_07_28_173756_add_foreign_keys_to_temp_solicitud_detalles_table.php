<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToTempSolicitudDetallesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('temp_solicitud_detalles', function(Blueprint $table)
        {
            $table->foreign('item_id', 'fk_temp_solicitud_detalles_items1')->references('id')->on('items')->onUpdate('NO ACTION')->onDelete('NO ACTION');
            $table->foreign('temp_solicitude_id', 'fk_temp_solicitud_detalles_temp_solicitudes1')->references('id')->on('temp_solicitudes')->onUpdate('NO ACTION')->onDelete('NO ACTION');
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('temp_solicitud_detalles', function(Blueprint $table)
        {
            $table->dropForeign('fk_temp_solicitud_detalles_items1');
            $table->dropForeign('fk_temp_solicitud_detalles_temp_solicitudes1');
        });
    }

}
