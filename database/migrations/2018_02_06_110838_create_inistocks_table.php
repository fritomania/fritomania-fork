<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateInistocksTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('inistocks', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('item_id')->index('fk_inistocks_items1_idx');
			$table->integer('tienda_id')->index('fk_inistocks_tiendas1_idx');
			$table->decimal('cantidad',12,4);
            $table->integer('user_id')->unsigned()->index('fk_inistocks_users1_idx');
			$table->timestamps();
			$table->softDeletes();


			$table->foreign('tienda_id')->references('id')->on('tiendas');
            $table->foreign('user_id')->references('id')->on('users');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('inistocks');
	}

}
