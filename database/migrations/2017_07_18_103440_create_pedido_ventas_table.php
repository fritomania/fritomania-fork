<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePedidoVentasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('pedido_ventas', function(Blueprint $table)
		{
			$table->integer('pedido_id')->index('fk_pedidos_has_ventas_pedidos1_idx');
			$table->integer('venta_id')->index('fk_pedidos_has_ventas_ventas1_idx');
			$table->primary(['pedido_id','venta_id']);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('pedido_ventas');
	}

}
