<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToCretirosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('cretiros', function(Blueprint $table)
		{
			$table->foreign('caja_id', 'fk_cretiros_cajas1')->references('id')->on('cajas')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('cretiros', function(Blueprint $table)
		{
			$table->dropForeign('fk_cretiros_cajas1');
		});
	}

}
