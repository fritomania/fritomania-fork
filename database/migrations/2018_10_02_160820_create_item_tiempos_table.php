<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateItemTiemposTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('item_tiempos', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('item_id')->index('fk_item_tiempos_items1_idx');
			$table->integer('tienda_id')->index('fk_item_tiempos_tiendas1_idx');
			$table->integer('preparacion')->default(0);
			$table->integer('entrega')->nullable()->default(0);
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('item_tiempos');
	}

}
