<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePedidosVista extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
            create or replace view vista_pedidos as
            select 
                p.id
                ,p.numero
                ,concat(c.nit,' ',c.nombres,' ',c.apellidos) as cliente
                ,DATE_FORMAT(p.created_at,'%d/%m/%Y') as fecha
                ,time(p.created_at) as hora
                ,p.fecha_ingreso
                ,p.fecha_entrega
                ,s.descripcion as estado
                ,u.name as usuario
            from 
                pedidos p inner join pestados s on p.pestado_id=s.id
                inner join users u on p.user_id= u.id
                inner join clientes c on p.cliente_id= c.id
            order by
                p.created_at desc
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('DROP VIEW IF EXISTS vista_pedidos');
    }
}
