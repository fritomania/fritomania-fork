<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTiendaDespachaTiendaTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tienda_despacha_tienda', function(Blueprint $table)
		{
			$table->integer('tiendas_solicita')->index('fk_tiendas_has_tiendas_tiendas2_idx');
			$table->integer('tienda_despacha')->index('fk_tiendas_has_tiendas_tiendas1_idx');
			$table->primary(['tiendas_solicita','tienda_despacha']);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tienda_despacha_tienda');
	}

}
