<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateVentaRecetasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('venta_recetas', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('item_id')->index('fk_venta_recetas_items1_idx');
			$table->boolean('activa')->default(1);
			$table->string('nombre', 45)->nullable()->default('default');
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('venta_recetas');
	}

}
