<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToIngredientesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('ingredientes', function(Blueprint $table)
		{
			$table->foreign('ingrediente_categoria_id', 'fk_ingredientes_ingrediente_tipos1')->references('id')->on('ingrediente_categorias')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('ingredientes', function(Blueprint $table)
		{
			$table->dropForeign('fk_ingredientes_ingrediente_tipos1');
		});
	}

}
