<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToModificacionesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('modificaciones', function(Blueprint $table)
		{
			$table->foreign('venta_detalle_id', 'fk_modificaciones_venta_detalles1')->references('id')->on('venta_detalles')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('modificaciones', function(Blueprint $table)
		{
			$table->dropForeign('fk_modificaciones_venta_detalles1');
		});
	}

}
