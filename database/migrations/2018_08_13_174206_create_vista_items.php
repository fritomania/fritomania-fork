<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVistaItems extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
            create or replace view vista_items as
            select 
                i.*
                ,m.nombre as marca
                ,st.descripcion as estado
                ,u.nombre as unidad_medida
                ,stockArticulo(i.id,null) as stock_fn
            from 
                items i left join iestados st on i.iestado_id=st.id
                left join unimeds u on i.unimed_id=u.id
                left join marcas m on i.marca_id=m.id
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
//        DB::statement("")
    }
}
