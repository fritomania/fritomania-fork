<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateItemsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('items', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('nombre', 100);
			$table->text('descripcion', 65535)->nullable();
			$table->string('codigo', 25)->nullable()->unique('codigo_UNIQUE');
			$table->decimal('precio_venta',10,0);
			$table->decimal('precio_compra',10,0)->default(0.00);
			$table->decimal('precio_mayoreo',10,0)->default(0.00);
			$table->tinyInteger('cantidad_mayoreo',false,true)->default(0);
			$table->decimal('precio_promedio',10,0)->default(0.00);
			$table->decimal('stock')->default(0.00);
			$table->string('imagen', 100)->nullable();
			$table->string('ubicacion', 45)->nullable();
			$table->boolean('inventariable')->nullable()->default(1);
			$table->boolean('perecedero')->nullable()->default(0);
            $table->boolean('materia_prima')->nullable()->default(1);
            $table->boolean('web')->nullable()->default(0);
			$table->integer('marca_id')->nullable()->index('fk_items_marcas1_idx');
			$table->integer('unimed_id')->nullable()->index('fk_items_unimeds1_idx');
			$table->integer('icategoria_id')->nullable()->index('fk_items_icategorias1_idx');
			$table->integer('iestado_id')->nullable()->default(1)->index('fk_items_iestados1_idx');
			$table->integer('orden')->nullable()->default(0);
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('items');
	}

}
