<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCretirosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('cretiros', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('caja_id')->index('fk_cretiros_cajas1_idx');
			$table->string('motivo');
			$table->decimal('monto');
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('cretiros');
	}

}
