<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCompraVentaTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('compra_venta', function(Blueprint $table)
		{
			$table->integer('compra_id')->index('fk_compras_has_ventas_compras1_idx');
			$table->integer('venta_id')->index('fk_compras_has_ventas_ventas1_idx');
			$table->primary(['compra_id','venta_id']);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('compra_venta');
	}

}
