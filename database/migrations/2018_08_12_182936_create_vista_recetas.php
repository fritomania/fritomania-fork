<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVistaRecetas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
            create or replace view vista_recetas as
            select 
                r.*,
                i.nombre as item,
                um.nombre as unimed,
                u.name as usuario
            from 
                recetas r inner join items i on r.item_id=i.id
                left join unimeds um on i.unimed_id=um.id
                inner join users u on r.user_id=u.id
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("drop view if exists vista_recetas");
    }
}
