<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVistaSolicitudes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
            Create or replace view vista_solicitudes as
            select 
                s.id,
                concat('SOL-INV-',date_format(s.created_at,'%Y%m%d'),s.numero) as numero, 
                s.observaciones,
                us.name as usuario_solicita,
                ud.name as usuario_despacha,
                date_format(s.created_at,'%d/%m/%Y') as fecha_solicita,
                date_format(s.fecha_despacha,'%d/%m/%Y') as fecha_despacha,
                st.nombre as estado,
                s.deleted_at,
                s.user_id,
                s.user_despacha,
                s.solicitud_estado_id,
                s.tienda_solicita,
                s.tienda_entrega
            from 
                solicitudes s inner join solicitud_estados st on s.solicitud_estado_id=st.id
                inner join users us on s.user_id=us.id
                left join users ud on s.user_despacha=ud.id
            order by
                id desc
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('DROP VIEW IF EXISTS vista_solicitudes');
    }
}
