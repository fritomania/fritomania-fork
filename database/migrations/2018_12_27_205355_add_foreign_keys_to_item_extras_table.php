<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToItemExtrasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('item_extras', function(Blueprint $table)
		{
			$table->foreign('item', 'fk_items_has_items_items1')->references('id')->on('items')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('extra', 'fk_items_has_items_items2')->references('id')->on('items')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('item_extras', function(Blueprint $table)
		{
			$table->dropForeign('fk_items_has_items_items1');
			$table->dropForeign('fk_items_has_items_items2');
		});
	}

}
