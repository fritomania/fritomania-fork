<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToIngredientesOpcionalesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('ingredientes_opcionales', function(Blueprint $table)
		{
			$table->foreign('venta_receta_detalle_id', 'fk_ingredientes_opcionales_detalles1')->references('id')->on('venta_receta_detalles')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('ingrediente_id', 'fk_ingredientes_opcionales_ingredientes1')->references('id')->on('ingredientes')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('ingredientes_opcionales', function(Blueprint $table)
		{
			$table->dropForeign('fk_ingredientes_opcionales_detalles1');
			$table->dropForeign('fk_ingredientes_opcionales_ingredientes1');
		});
	}

}
