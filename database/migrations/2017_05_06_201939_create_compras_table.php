<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateComprasTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('compras', function(Blueprint $table)
        {
            $table->integer('id', true);
            $table->integer('proveedor_id')->nullable()->index('fk_compra_proveedores1_idx');
            $table->integer('tcomprobante_id')->index('fk_compras_tcomprobantes1_idx');
            $table->date('fecha')->nullable()->comment('Fecha del docuemnto de  la Factura');
            $table->date('fecha_ingreso')->nullable()->comment('Fecha de ingreso al sistema');
            $table->date('fecha_ingreso_plan')->comment('Fecha Promesa del Proveedor');
            $table->string('serie', 45)->nullable();
            $table->string('numero', 20)->nullable();
            $table->boolean('credito')->default(0);
            $table->boolean('pagada')->default(0);
            $table->integer('user_id')->unsigned()->index('user_id');
            $table->integer('tienda_id')->index('tienda_id');
            $table->integer('cestado_id')->default(1)->index('fk_compras_cestados1_idx');
            $table->date('fecha_limite_credito')->nullable();
            $table->dateTime('fecha_anula')->nullable();
            $table->dateTime('fecha_cancela')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('compras');
    }

}
