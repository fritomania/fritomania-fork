<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToCpagosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('cpagos', function(Blueprint $table)
		{
			$table->foreign('compra_id', 'fk_cpagos_compras1')->references('id')->on('compras')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('user_id', 'fk_cpagos_users1')->references('id')->on('users')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('cpagos', function(Blueprint $table)
		{
			$table->dropForeign('fk_cpagos_compras1');
			$table->dropForeign('fk_cpagos_users1');
		});
	}

}
