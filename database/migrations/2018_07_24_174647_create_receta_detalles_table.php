<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRecetaDetallesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('receta_detalles', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('item_id')->default(0)->index('item_id');
			$table->integer('receta_id')->unsigned()->default(0)->index('receta_id');
			$table->decimal('cantidad', 12,4)->default(0);
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('receta_detalles');
	}

}
