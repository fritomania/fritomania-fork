<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToIngresoProduccionTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('ingreso_produccion', function(Blueprint $table)
		{
			$table->foreign('producto_realizado_id', 'fk_producto_realizados_has_stocks_producto_realizados1')->references('id')->on('producto_realizados')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('stock_id', 'fk_producto_realizados_has_stocks_stocks1')->references('id')->on('stocks')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('ingreso_produccion', function(Blueprint $table)
		{
			$table->dropForeign('fk_producto_realizados_has_stocks_producto_realizados1');
			$table->dropForeign('fk_producto_realizados_has_stocks_stocks1');
		});
	}

}
