<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateVentaVestadoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('venta_vestado', function(Blueprint $table)
		{
			$table->integer('venta_id')->index('fk_ventas_has_vestados_ventas1_idx');
			$table->integer('vestado_id')->index('fk_ventas_has_vestados_vestados1_idx');
			$table->timestamp('tiempo')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->primary(['venta_id','vestado_id']);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('venta_vestado');
	}

}
