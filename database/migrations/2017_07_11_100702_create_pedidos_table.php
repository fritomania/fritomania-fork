<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePedidosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('pedidos', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('numero')->nullable();
			$table->integer('cliente_id')->index('fk_pedidos_clientes1_idx');
			$table->timestamp('fecha_ingreso')->nullable();
			$table->timestamp('fecha_entrega')->nullable();
			$table->integer('user_id');
			$table->integer('pestado_id')->index('fk_pedidos_pestados1_idx');
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('pedidos');
	}

}
