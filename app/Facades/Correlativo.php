<?php
/**
 * Created by PhpStorm.
 * User: Usuario
 * Date: 14/09/2018
 * Time: 15:00
 */

namespace App\Facades;
use App\Models\Correlativo as CorrelativoModel;

class Correlativo
{

    /**
     * Devuelve una isntancia del modelo correlativos del registro correspondiente al nombre de la tabla
     * @param null $tabla
     * @return bool
     */
    public static function siguiente($tabla=null)
    {
        if (!$tabla) return false;

        $correlativo = CorrelativoModel::where('tabla',$tabla)
            ->where('anio',anioActual())
            ->first();

        //si no éxiste correlativo para la tabla o el año es diferente
        if(!$correlativo){
            $correlativo = CorrelativoModel::create([
                'tabla' => $tabla,
                'anio' => anioActual(),
                'max' => 0
            ]);
        }

        $correlativo->max = $correlativo->max +1;

        return $correlativo;
    }
}