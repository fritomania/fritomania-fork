<?php

namespace App\Http\Controllers;

use App\Models\Cestado;
use App\Models\Item;
use App\Models\ProduccionEstado;
use App\Models\Vestado;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class RptGenController extends Controller{


    /**
     * RptGenController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function kardex(Request $request)
    {
        $items = Item::select(DB::raw("concat(coalesce(codigo,''),'-',nombre) nombre"),'id')->pluck('nombre','id');
//        dd($items);
        $item = $request->item ? $request->item : null;

        $estadoProduccion = ProduccionEstado::COMPLETADA;
        $compraEstado = Cestado::RECIBIDA;
        $ventaEstados = Vestado::ANULADA;


        $detalles = DB::select("
            select 
                si.created_at fecha,1 tipo,'' numero,'Stock inicial' cliente_prov,si.cantidad ingresos,0 salidas,u.name usuario,'' estado 
            from 
                inistocks si left join users u on si.user_id=u.id
            where 
                si.item_id='$item'
                and si.cantidad!=0
                and si.deleted_at is null
            union
            select * from 
            (
            select 
                pr.created_at fecha,1 tipo,numero,'PRODUCCION' cliente_prov,pr.cantidad ingresos,0 salidas,u.name,'' estado 
            from 
                producto_realizados pr inner join producciones p on pr.produccione_id=p.id
                inner join users u on pr.user_id=u.id
            where 
                pr.item_id = '$item'
                and p.produccion_estado_id='$estadoProduccion'
            
            union 
                
            select 
                c.created_at fecha,1 tipo,c.numero,p.nombre,d.cantidad ingresos,0 salidas,'' usuario,cs.descripcion
            from 
                compras c inner join compra_detalles d on c.id = d.compra_id
                inner join items i on d.item_id= i.id
                inner join cestados cs on cs.id=c.cestado_id
                inner join proveedores p on p.id = c.proveedor_id
            where 
                c.cestado_id='$compraEstado'
                and i.id='$item'
                and d.deleted_at is null
                
            union
            
            select 
                pc.created_at fecha,2 tipo,numero,'PRODUCCION/CONSUMO' cliente_prov,0 ingresos,pc.cantidad_ingresada salidas,u.name,'' estado 
            from 
                producto_consumidos pc inner join producciones p on pc.produccione_id=p.id
                left join users u on pc.user_id=u.id
            where 
                pc.item_id = '$item'
                and p.produccion_estado_id='$estadoProduccion'

            union
            
            select 
                v.created_at fecha,2 tipo,v.numero,concat(cl.nombres,' ',cl.apellidos),0 ingresos,d.cantidad,nombre_usuario(v.user_id),vs.descripcion
            from 
                ventas v inner join venta_detalles d on v.id=d.venta_id 
                inner join items i on d.item_id=i.id
                inner join vestados vs on vs.id = v.vestado_id
                inner join clientes cl on cl.id = v.cliente_id
            where 
                v.vestado_id not in ('$ventaEstados')
                and i.id='$item'
                and d.deleted_at is null
            ) q1
            order by
                1,2
            

        ");

        $buscar = $request->buscar;
        $saldo = 0;

        return view('reportes.rpt_kardex_por_item',compact('detalles','items','item','buscar','saldo'));
    }

}
