<?php

namespace App\Http\Controllers;

use App\DataTables\DenominacioneDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateDenominacioneRequest;
use App\Http\Requests\UpdateDenominacioneRequest;
use App\Models\Denominacione;
use App\Repositories\DenominacioneRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Response;

class DenominacioneController extends AppBaseController
{
    /** @var  DenominacioneRepository */
    private $denominacioneRepository;

    public function __construct(DenominacioneRepository $denominacioneRepo)
    {
        $this->denominacioneRepository = $denominacioneRepo;
        $this->middleware('auth');
    }

    /**
     * Display a listing of the Denominacione.
     *
     * @param DenominacioneDataTable $denominacioneDataTable
     * @return Response
     */
    public function index(DenominacioneDataTable $denominacioneDataTable)
    {
        return $denominacioneDataTable->render('denominaciones.index');
    }

    /**
     * Show the form for creating a new Denominacione.
     *
     * @return Response
     */
    public function create()
    {
        return view('denominaciones.create');
    }

    /**
     * Store a newly created Denominacione in storage.
     *
     * @param CreateDenominacioneRequest $request
     *
     * @return Response
     */
    public function store(CreateDenominacioneRequest $request)
    {
        $input = $request->all();

        $denominacione = $this->denominacioneRepository->create($input);

        Flash::success('Denominación guardada exitosamente.');

        return redirect(route('denominaciones.index'));
    }

    /**
     * Display the specified Denominacione.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $denominacione = $this->denominacioneRepository->findWithoutFail($id);

        if (empty($denominacione)) {
            Flash::error('Denominación no encontrada');

            return redirect(route('denominaciones.index'));
        }

        return view('denominaciones.show',compact('denominacione'));
    }

    /**
     * Show the form for editing the specified Denominacione.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $denominacione = $this->denominacioneRepository->findWithoutFail($id);

        if (empty($denominacione)) {
            Flash::error('Denominación no encontrada');

            return redirect(route('denominaciones.index'));
        }

        return view('denominaciones.edit',compact('denominacione'));
    }

    /**
     * Update the specified Denominacione in storage.
     *
     * @param  int              $id
     * @param UpdateDenominacioneRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateDenominacioneRequest $request)
    {
        $denominacione = $this->denominacioneRepository->findWithoutFail($id);

        if (empty($denominacione)) {
            Flash::error('Denominación no encontrada');

            return redirect(route('denominaciones.index'));
        }

        $denominacione = $this->denominacioneRepository->update($request->all(), $id);

        Flash::success('Denominación actualizada exitosamente.');

        return redirect(route('denominaciones.index'));
    }

    /**
     * Remove the specified Denominacione from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $denominacione = $this->denominacioneRepository->findWithoutFail($id);

        if (empty($denominacione)) {
            Flash::error('Denominación no encontrada');

            return redirect(route('denominaciones.index'));
        }

        $this->denominacioneRepository->delete($id);

        Flash::success('Denominación eliminada exitosamente.');

        return redirect(route('denominaciones.index'));
    }

    public function getAjax(Request $request)
    {
        if(!$request->ajax()){
            abort(403, 'Acción no autorizada!!');
        }

        $denominaciones = Denominacione::all();

        return $this->sendResponse($denominaciones,'Denomincaiones');

    }
}
