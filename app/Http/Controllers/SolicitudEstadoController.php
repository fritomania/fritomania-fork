<?php

namespace App\Http\Controllers;

use App\DataTables\SolicitudEstadoDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateSolicitudEstadoRequest;
use App\Http\Requests\UpdateSolicitudEstadoRequest;
use App\Repositories\SolicitudEstadoRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class SolicitudEstadoController extends AppBaseController
{
    /** @var  SolicitudEstadoRepository */
    private $solicitudEstadoRepository;

    public function __construct(SolicitudEstadoRepository $solicitudEstadoRepo)
    {
        $this->solicitudEstadoRepository = $solicitudEstadoRepo;
        $this->middleware('auth');
    }

    /**
     * Display a listing of the SolicitudEstado.
     *
     * @param SolicitudEstadoDataTable $solicitudEstadoDataTable
     * @return Response
     */
    public function index(SolicitudEstadoDataTable $solicitudEstadoDataTable)
    {
        return $solicitudEstadoDataTable->render('solicitud_estados.index');
    }

    /**
     * Show the form for creating a new SolicitudEstado.
     *
     * @return Response
     */
    public function create()
    {
        return view('solicitud_estados.create');
    }

    /**
     * Store a newly created SolicitudEstado in storage.
     *
     * @param CreateSolicitudEstadoRequest $request
     *
     * @return Response
     */
    public function store(CreateSolicitudEstadoRequest $request)
    {
        $input = $request->all();

        $solicitudEstado = $this->solicitudEstadoRepository->create($input);

        Flash::success('Solicitud Estado guardado exitosamente.');

        return redirect(route('solicitudEstados.index'));
    }

    /**
     * Display the specified SolicitudEstado.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $solicitudEstado = $this->solicitudEstadoRepository->findWithoutFail($id);

        if (empty($solicitudEstado)) {
            Flash::error('Solicitud Estado no encontrado');

            return redirect(route('solicitudEstados.index'));
        }

        return view('solicitud_estados.show',compact('solicitudEstado'));
    }

    /**
     * Show the form for editing the specified SolicitudEstado.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $solicitudEstado = $this->solicitudEstadoRepository->findWithoutFail($id);

        if (empty($solicitudEstado)) {
            Flash::error('Solicitud Estado no encontrado');

            return redirect(route('solicitudEstados.index'));
        }

        return view('solicitud_estados.edit',compact('solicitudEstado'));
    }

    /**
     * Update the specified SolicitudEstado in storage.
     *
     * @param  int              $id
     * @param UpdateSolicitudEstadoRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSolicitudEstadoRequest $request)
    {
        $solicitudEstado = $this->solicitudEstadoRepository->findWithoutFail($id);

        if (empty($solicitudEstado)) {
            Flash::error('Solicitud Estado no encontrado');

            return redirect(route('solicitudEstados.index'));
        }

        $solicitudEstado = $this->solicitudEstadoRepository->update($request->all(), $id);

        Flash::success('Solicitud Estado actualizado exitosamente.');

        return redirect(route('solicitudEstados.index'));
    }

    /**
     * Remove the specified SolicitudEstado from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $solicitudEstado = $this->solicitudEstadoRepository->findWithoutFail($id);

        if (empty($solicitudEstado)) {
            Flash::error('Solicitud Estado no encontrado');

            return redirect(route('solicitudEstados.index'));
        }

        $this->solicitudEstadoRepository->delete($id);

        Flash::success('Solicitud Estado eliminado exitosamente.');

        return redirect(route('solicitudEstados.index'));
    }
}
