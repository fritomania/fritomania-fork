<?php

namespace App\Http\Controllers;

use App\DataTables\NotificacionTipoDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateNotificacionTipoRequest;
use App\Http\Requests\UpdateNotificacionTipoRequest;
use App\Repositories\NotificacionTipoRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class NotificacionTipoController extends AppBaseController
{
    /** @var  NotificacionTipoRepository */
    private $notificacionTipoRepository;

    public function __construct(NotificacionTipoRepository $notificacionTipoRepo)
    {
        $this->notificacionTipoRepository = $notificacionTipoRepo;
        $this->middleware('auth');
    }

    /**
     * Display a listing of the NotificacionTipo.
     *
     * @param NotificacionTipoDataTable $notificacionTipoDataTable
     * @return Response
     */
    public function index(NotificacionTipoDataTable $notificacionTipoDataTable)
    {
        return $notificacionTipoDataTable->render('notificacion_tipos.index');
    }

    /**
     * Show the form for creating a new NotificacionTipo.
     *
     * @return Response
     */
    public function create()
    {
        return view('notificacion_tipos.create');
    }

    /**
     * Store a newly created NotificacionTipo in storage.
     *
     * @param CreateNotificacionTipoRequest $request
     *
     * @return Response
     */
    public function store(CreateNotificacionTipoRequest $request)
    {
        $input = $request->all();

        $notificacionTipo = $this->notificacionTipoRepository->create($input);

        Flash::success('Notificacion Tipo guardado exitosamente.');

        return redirect(route('notificacionTipos.index'));
    }

    /**
     * Display the specified NotificacionTipo.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $notificacionTipo = $this->notificacionTipoRepository->findWithoutFail($id);

        if (empty($notificacionTipo)) {
            Flash::error('Notificacion Tipo no encontrado');

            return redirect(route('notificacionTipos.index'));
        }

        return view('notificacion_tipos.show',compact('notificacionTipo'));
    }

    /**
     * Show the form for editing the specified NotificacionTipo.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $notificacionTipo = $this->notificacionTipoRepository->findWithoutFail($id);

        if (empty($notificacionTipo)) {
            Flash::error('Notificacion Tipo no encontrado');

            return redirect(route('notificacionTipos.index'));
        }

        return view('notificacion_tipos.edit',compact('notificacionTipo'));
    }

    /**
     * Update the specified NotificacionTipo in storage.
     *
     * @param  int              $id
     * @param UpdateNotificacionTipoRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateNotificacionTipoRequest $request)
    {
        $notificacionTipo = $this->notificacionTipoRepository->findWithoutFail($id);

        if (empty($notificacionTipo)) {
            Flash::error('Notificacion Tipo no encontrado');

            return redirect(route('notificacionTipos.index'));
        }

        $notificacionTipo = $this->notificacionTipoRepository->update($request->all(), $id);

        Flash::success('Notificacion Tipo actualizado exitosamente.');

        return redirect(route('notificacionTipos.index'));
    }

    /**
     * Remove the specified NotificacionTipo from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $notificacionTipo = $this->notificacionTipoRepository->findWithoutFail($id);

        if (empty($notificacionTipo)) {
            Flash::error('Notificacion Tipo no encontrado');

            return redirect(route('notificacionTipos.index'));
        }

        $this->notificacionTipoRepository->delete($id);

        Flash::success('Notificacion Tipo eliminado exitosamente.');

        return redirect(route('notificacionTipos.index'));
    }
}
