<?php


namespace App\Http\Controllers;

use App\DataTables\TiendaDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateTiendaRequest;
use App\Http\Requests\UpdateTiendaRequest;
use App\Models\Empleado;
use App\Models\Tienda;
use App\Repositories\TiendaRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Response;

class TiendaController extends AppBaseController
{
    /** @var  TiendaRepository */
    private $tiendaRepository;

    public function __construct(TiendaRepository $tiendaRepo)
    {
        $this->middleware("auth")->except(['getPorIdMap']);
        $this->tiendaRepository = $tiendaRepo;
    }

    /**
     * Display a listing of the Tienda.
     *
     * @param TiendaDataTable $tiendaDataTable
     * @return Response
     */
    public function index(TiendaDataTable $tiendaDataTable)
    {
        return $tiendaDataTable->render('tiendas.index');
    }

    /**
     * Show the form for creating a new Tienda.
     *
     * @return Response
     */
    public function create()
    {
        $empleados = Empleado::all()->toArray();

        $empleados = array_pluck($empleados,'full_name','id');

//        dd($empleados);
        return view('tiendas.create',compact('empleados'));
    }

    /**
     * Store a newly created Tienda in storage.
     *
     * @param CreateTiendaRequest $request
     *
     * @return Response
     */
    public function store(CreateTiendaRequest $request)
    {
        $input = $request->all();

        $tienda = $this->tiendaRepository->create($input);

        Flash::success('Tienda guardado exitosamente.');

        return redirect(route('tiendas.index'));
    }

    /**
     * Display the specified Tienda.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $tienda = $this->tiendaRepository->findWithoutFail($id);

        if (empty($tienda)) {
            Flash::error('Tienda no existe');

            return redirect(route('tiendas.index'));
        }

        return view('tiendas.show')->with('tienda', $tienda);
    }

    /**
     * Show the form for editing the specified Tienda.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $tienda = $this->tiendaRepository->findWithoutFail($id);

        if (empty($tienda)) {
            Flash::error('Tienda no existe');

            return redirect(route('tiendas.index'));
        }

        $empleados = Empleado::all()->toArray();

        $empleados = array_pluck($empleados,'full_name','id');

        return view('tiendas.edit',compact('tienda','empleados'));
    }

    /**
     * Update the specified Tienda in storage.
     *
     * @param  int              $id
     * @param UpdateTiendaRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateTiendaRequest $request)
    {
        $tienda = $this->tiendaRepository->findWithoutFail($id);

        if (empty($tienda)) {
            Flash::error('Tienda no existe');

            return redirect(route('tiendas.index'));
        }

        $tienda = $this->tiendaRepository->update($request->all(), $id);

        Flash::success('Tienda actualizado exitosamente.');

        return redirect(route('tiendas.index'));
    }

    /**
     * Remove the specified Tienda from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $tienda = $this->tiendaRepository->findWithoutFail($id);

        if (empty($tienda)) {
            Flash::error('Tienda no existe');

            return redirect(route('tiendas.index'));
        }

        $this->tiendaRepository->delete($id);

        Flash::success('Tienda eliminado exitosamente.');

        return redirect(route('tiendas.index'));
    }

    public function getPorIdMap(Request $request)
    {
//        if(!$request->ajax()){
//            abort(403, 'Acción no autorizada!!');
//        }

        $tienda = Tienda::where('id_map',$request->id_map)->first();

        return $this->sendResponse($tienda,'Tienda');

    }

    public function cambiar(Tienda $lugar)
    {
        session()->put('tienda', $lugar->id);
        session()->save();

        flash('Se cambió con éxito al lugar: '.$lugar->nombre)->success()->important();

        return redirect()->back();

    }
}
