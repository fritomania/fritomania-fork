<?php

namespace App\Http\Controllers;

use App\DataTables\SaccionesDataTable;
use App\DataTables\SbitacoraDataTable;
use App\DataTables\Scopes\ServicioBitacoraScopeDt;
use App\DataTables\ServicioAtenderDataTable;
use App\DataTables\ServicioDataTable;
use App\DataTables\ServicioEntregarDataTable;
use App\DataTables\ServicioNoAsignadosDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateServicioRequest;
use App\Http\Requests\UpdateServicioRequest;
use App\Models\Cliente;
use App\Models\Correlativo;
use App\Models\Emarca;
use App\Models\Equipo;
use App\Models\Etipo;
use App\Models\Servicio;
use App\Models\ServicioArchivo;
use App\Models\Sestado;
use App\Models\Stipo;
use App\Models\TempFile;
use App\Repositories\ServicioRepository;
use App\User;
use Flash;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Response;

class ServicioController extends AppBaseController
{
    /** @var  ServicioRepository */
    private $servicioRepository;
    private $clientes;
    /**@var Tipos de servicio*/
    private $stipos;
    /**@var Tipos de equipos*/
    private $etipos;
    private $equipos;
    private $marcas;
    private $modelos;


    public function __construct(ServicioRepository $servicioRepo)
    {
        $this->servicioRepository = $servicioRepo;
        $this->middleware('auth');

        $clientes = Cliente::all()->toArray();
        $this->clientes = array_pluck($clientes,'full_name','id');

        $equipos = Equipo::all()->toArray();
        $this->equipos = array_pluck($equipos,'full_name','id');

        $this->stipos = Stipo::pluck('nombre','id')->toArray();
        $this->etipos = Etipo::pluck('nombre','id')->toArray();
        $this->marcas = Emarca::pluck('nombre','id')->toArray();

    }

    /**
     * Display a listing of the Servicio.
     *
     * @param ServicioDataTable $servicioDataTable
     * @return Response
     */
    public function index(ServicioDataTable $servicioDataTable)
    {
        return $servicioDataTable->render('servicios.index');
    }

    /**
     * Muestra formulario con tabla de los servicios sin asignar.
     *
     * @param ServicioDataTable $servicioDataTable
     * @return Response
     */
    public function asignarView(ServicioNoAsignadosDataTable $servicioDataTable)
    {
        return $servicioDataTable->render('servicios.asignar');
    }

    /**
     * Muestra formulario con tabla de los servicios sin asignar.
     *
     * @param ServicioDataTable $servicioDataTable
     * @return Response
     */
    public function pendientes(ServicioAtenderDataTable $servicioDataTable)
    {
        return $servicioDataTable->render('servicios.pendientes');
    }

    public function finalizados(ServicioEntregarDataTable $servicioDataTable)
    {
        return $servicioDataTable->render('servicios.finalizados');
    }

    public function asignar(Request $request){

        $servicios = $request->servicios;

        foreach ($servicios as $serviId => $usuId){

            if($usuId){
                $servicio = Servicio::find($serviId);
                $usuario = User::find($usuId);

//                dump('El servicio # '.$servicio->numero.' de: '.$servicio->cliente->full_name.' se asignara a: '.$usuario->name);

                $servicio->usuarioAsigna()->associate($usuario);
                $servicio->sestado_id=2;
                $servicio->fecha_asigna=fechaHoraActualDb();
                $servicio->save();

            }

        }

        Flash::success('Servicio asignados.');

        return redirect(route('servicios.asignar.view'));
    }

    /**
     * Show the form for creating a new Servicio.
     *
     * @return Response
     */
    public function create()
    {
        //elimina los archivos temporales
        $this->deleteTempFiles('servicios');

        $stipos= $this->stipos; $equipos =$this->equipos; $clientes = $this->clientes; $tipos = $this->etipos; $marcas = $this->marcas;
        $modelos = [];
        return view('servicios.create',compact('stipos','equipos','clientes','tipos','marcas','modelos'));
    }

    /**
     * Store a newly created Servicio in storage.
     *
     * @param CreateServicioRequest $request
     *
     * @return Response
     */
    public function store(CreateServicioRequest $request)
    {
        $correlativo = Correlativo::OfTabla('servicios')->get();
        $correlativo = $correlativo[0];
        $correlativo->max ++;

        $input = $request->all();

        $input= array_add($input,'sestado_id',1);
        $input= array_add($input,'usuario_recibe',1);
//        $input= array_add($input,'numero',$correlativo->max);

        $servicio = $this->servicioRepository->create($input);

        //Si se gravo el servicio actualiza su numero en base al mes año y correlativo
        if($servicio){
            list($fecha,$hora)=explode(' ',$servicio->created_at);
            list($anio,$mes,$dia)=explode('-',$fecha);

            $servicio->numero = $mes.substr($anio,-2,2).$correlativo->max;
            $servicio->save();
            $correlativo->save();

            //Vincula el equipo al cliente del servicio
            $equipo = Equipo::find($servicio->equipo->id);
            $equipo->clientes()->syncWithoutDetaching([$servicio->cliente->id]);

            $this->saveTempFiles($servicio,'servicios');
        }

        Flash::success('Servicio saved successfully.');

        return redirect(route('servicios.view.pdf.recepcion',$servicio->id));
    }

    /**
     * Display the specified Servicio.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id,SbitacoraDataTable $sbitacoraDataTable)
    {
        $servicio = $this->servicioRepository->findWithoutFail($id);

        if (empty($servicio)) {
            Flash::error('Servicio not found');

            return redirect(route('servicios.index'));
        }

        return $sbitacoraDataTable->addScope(new ServicioBitacoraScopeDt($servicio))
            ->render('servicios.show',compact('servicio'));
    }

    public function atender(Servicio $servicio,SbitacoraDataTable $sbitacoraDataTable)
    {

        if (empty($servicio)) {
            Flash::error('Servicio not found');

            return redirect(route('servicios.index'));
        }

        $estado=$servicio->sestado_id;

        //Si el estado del servicio es igual a recibido o asignado
        if($estado == 1 || $estado == 2){
            //Cambia a estado de visto
            $servicio->sestado_id = 3;
            $servicio->save();
        }

        return $sbitacoraDataTable->addScope(new ServicioBitacoraScopeDt($servicio))
            ->render('servicios.atender',compact('servicio'));
    }

    /**
     * Show the form for editing the specified Servicio.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $servicio = $this->servicioRepository->findWithoutFail($id);

        if (empty($servicio)) {
            Flash::error('Servicio not found');

            return redirect(route('servicios.index'));
        }

        return view('servicios.edit')->with('servicio', $servicio);
    }

    /**
     * Update the specified Servicio in storage.
     *
     * @param  int              $id
     * @param UpdateServicioRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateServicioRequest $request)
    {
        $servicio = $this->servicioRepository->findWithoutFail($id);


        if (empty($servicio)) {
            Flash::error('Servicio not found');

            return redirect(route('servicios.index'));
        }

        $servicio = $this->servicioRepository->update($request->all(), $id);

        Flash::success('Servicio updated successfully.');

        return redirect(route('servicios.index'));
    }

    /**
     * Actualiza los valores ingresados en la vista atención
     * @param Servicio $servicio
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function atenderUpdate(Servicio $servicio,Request $request)
    {

        if (empty($servicio)) {
            Flash::error('Servicio not found');

            return redirect(route('servicios.index'));
        }

        //Si el servicio no tiene diagnostico y se envía uno
        if (!$servicio->diagnostico && $request->diagnostico){
            $servicio->diagnostico = $request->diagnostico;
            $servicio->sestado_id = 4;
            $servicio->fecha_inicio = fechaHoraActualDb();
        }

        //Si el servicio no tiene diagnostico y se envía uno
        if (!$servicio->reparacion && $request->reparacion){
            $servicio->reparacion = $request->reparacion;
            $servicio->sestado_id = 6;
            $servicio->fecha_fin = fechaHoraActualDb();

        }

        $servicio->save();

        Flash::success('Servicio actualizado exitosamente.');

        return redirect(route('servicios.atender',$servicio->id));
    }
    /**
     * Remove the specified Servicio from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $servicio = $this->servicioRepository->findWithoutFail($id);

        if (empty($servicio)) {
            Flash::error('Servicio not found');

            return redirect(route('servicios.index'));
        }

        $this->servicioRepository->delete($id);

        Flash::success('Servicio deleted successfully.');

        return redirect(route('servicios.index'));
    }

    public function viewPdfRecepcion(Servicio $servicio)
    {
        return view('servicios.view_pdf_recepcion',compact('servicio'));
    }

    public function pdfRecepcion(Servicio $servicio)
    {
        return view('servicios.pdf_recepcion',compact('servicio'));
    }

    public function sigNumero(Servicio $servicio)
    {
        return view('servicios.pdf_recepcion',compact('servicio'));
    }

    public function cambioEstado(Servicio $servicio,Sestado $estado)
    {
//        dd('El servicio :'.$servicio->numero.' del cliente: '.$servicio->cliente->full_name.' cambiara a estado: '.$estado->nombre);

        $servicio->sestado()->associate($estado);


        Flash::success('Servicio actualizado exitosamente.');

        switch ($estado->id){
            case 5:
                $servicio->save();
                return redirect(route('servicios.atender',$servicio->id));
                break;
            case 7:
                $servicio->fecha_entrega = fechaHoraActualDb();
                $servicio->save();
                return redirect(route('servicios.finalizados'));
                break;
        }

    }

    public function saveTempFiles(Servicio $servicio,$opcion)
    {
        $tempFiles = TempFile::where('user_id',Auth::user()->id)->where('opcion',$opcion)->get();

        $servicioArchivos = collect();
        foreach ($tempFiles as $file){

            $datos= [
                'servicio_id' => $servicio->id,
                'data' => $file->data,
                'nombre' => $file->nombre,
                'type' => $file->type,
                'size' => $file->size,
                'extension' => $file->extension
            ];

            $servicioArchivos->push(New ServicioArchivo($datos));

        }

        $servicio->servicioArchivos()->saveMany($servicioArchivos);
        $this->deleteTempFiles($opcion);
    }

    public function deleteTempFiles($opcion)
    {
        TempFile::where('user_id',Auth::user()->id)->where('opcion',$opcion)->delete();
    }
}
