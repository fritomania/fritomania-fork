<?php

namespace App\Http\Controllers;

use App\Models\Caja;
use App\Models\Gasto;
use App\Models\Vestado;
use App\Models\Vpago;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class RptResumenController extends AppBaseController
{


    /**
     * RptResumenController constructor.
     */
    public function __construct(){
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $fecha = $request->fecha ? $request->fecha : hoyDb();

        $vpagos = Vpago::hoy()->get();

        $estadoAnulada = Vestado::ANULADA;
        $tienda = session('tienda');

        $ventasPorCat =DB::select("
            select 
                ic.nombre,sum(d.cantidad*d.precio) monto
            from 
                ventas v inner join venta_detalles d on v.id=d.venta_id
                inner join items i on d.item_id=i.id
                left join icategorias ic on i.icategoria_id=ic.id
            where
                v.fecha=CURDATE()
                and v.vestado_id not in ({$estadoAnulada})
                and v.cliente_id!=2
                and v.credito=0
                and v.tienda_id='$tienda'
            group by
                1
            order by
                1 desc 
        ");

        $consumos = DB::select("
            select 
                i.nombre,sum(d.cantidad*d.precio) monto
            from 
                ventas v inner join venta_detalles d on v.id=d.venta_id
                inner join items i on d.item_id=i.id
            where 
                date(v.created_at)=CURDATE()
                and v.vestado_id not in ({$estadoAnulada})
                and v.cliente_id=2
                and d.precio!=0
                and v.tienda_id='$tienda'
            group by 
                1
        ");

        $gastos = Gasto::hoy()->get();

        $cajas = Caja::deTienda()->hoy()->get();

//        dd($cajas->toArray());
        $totalGastos = array_sum(array_pluck($gastos,'monto'));
        $totalVpagos = array_sum(array_pluck($vpagos ,'monto'));
        $totalVentas = array_sum(array_pluck($ventasPorCat,'monto')) + $totalVpagos;
        $totalCajas = array_sum(array_pluck($cajas,'monto_cierre'));
        $totalRetiros = array_sum(array_pluck($cajas,'total_retiros'));



        $diaSemana=Carbon::now()->dayOfWeek;
        $dia = Carbon::now()->day;
        $mes = Carbon::now()->month;
        $anio = Carbon::now()->year;

        return view(
            'reportes.view_rpt_resumen',
            compact(
                'ventasPorCat',
                'totalVentas',
                'totalCajas',
                'totalGastos',
                'totalRetiros',
                'consumos',
                'gastos',
                'diaSemana',
                'dia',
                'mes',
                'anio',
                'cajas',
                'vpagos')
        );
    }

    public function getTotalAjax(Request $request)
    {
        $total = Caja::deTienda()->abiertas()->hoy()->get()->sum('monto_cierre');


        return $this->sendResponse($total,'total');

    }
}
