<?php

namespace App\Http\Controllers;

use App\DataTables\SaccionesDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateSaccionesRequest;
use App\Http\Requests\UpdateSaccionesRequest;
use App\Repositories\SaccionesRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class SaccionesController extends AppBaseController
{
    /** @var  SaccionesRepository */
    private $saccionesRepository;

    public function __construct(SaccionesRepository $saccionesRepo)
    {
        $this->saccionesRepository = $saccionesRepo;
    }

    /**
     * Display a listing of the Sacciones.
     *
     * @param SaccionesDataTable $saccionesDataTable
     * @return Response
     */
    public function index(SaccionesDataTable $saccionesDataTable)
    {
        return $saccionesDataTable->render('sacciones.index');
    }

    /**
     * Show the form for creating a new Sacciones.
     *
     * @return Response
     */
    public function create()
    {
        return view('sacciones.create');
    }

    /**
     * Store a newly created Sacciones in storage.
     *
     * @param CreateSaccionesRequest $request
     *
     * @return Response
     */
    public function store(CreateSaccionesRequest $request)
    {
        $input = $request->all();

        $sacciones = $this->saccionesRepository->create($input);

        Flash::success('Sacciones saved successfully.');

        return redirect(route('sacciones.index'));
    }

    /**
     * Display the specified Sacciones.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $sacciones = $this->saccionesRepository->findWithoutFail($id);

        if (empty($sacciones)) {
            Flash::error('Sacciones not found');

            return redirect(route('sacciones.index'));
        }

        return view('sacciones.show')->with('sacciones', $sacciones);
    }

    /**
     * Show the form for editing the specified Sacciones.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $sacciones = $this->saccionesRepository->findWithoutFail($id);

        if (empty($sacciones)) {
            Flash::error('Sacciones not found');

            return redirect(route('sacciones.index'));
        }

        return view('sacciones.edit')->with('sacciones', $sacciones);
    }

    /**
     * Update the specified Sacciones in storage.
     *
     * @param  int              $id
     * @param UpdateSaccionesRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSaccionesRequest $request)
    {
        $sacciones = $this->saccionesRepository->findWithoutFail($id);

        if (empty($sacciones)) {
            Flash::error('Sacciones not found');

            return redirect(route('sacciones.index'));
        }

        $sacciones = $this->saccionesRepository->update($request->all(), $id);

        Flash::success('Sacciones updated successfully.');

        return redirect(route('sacciones.index'));
    }

    /**
     * Remove the specified Sacciones from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $sacciones = $this->saccionesRepository->findWithoutFail($id);

        if (empty($sacciones)) {
            Flash::error('Sacciones not found');

            return redirect(route('sacciones.index'));
        }

        $this->saccionesRepository->delete($id);

        Flash::success('Sacciones deleted successfully.');

        return redirect(route('sacciones.index'));
    }
}
