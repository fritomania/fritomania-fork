<?php

namespace App\Http\Controllers;

use App\DataTables\IestadoDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateIestadoRequest;
use App\Http\Requests\UpdateIestadoRequest;
use App\Repositories\IestadoRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class IestadoController extends AppBaseController
{
    /** @var  IestadoRepository */
    private $iestadoRepository;

    public function __construct(IestadoRepository $iestadoRepo)
    {
        $this->iestadoRepository = $iestadoRepo;
        $this->middleware('auth');
    }

    /**
     * Display a listing of the Iestado.
     *
     * @param IestadoDataTable $iestadoDataTable
     * @return Response
     */
    public function index(IestadoDataTable $iestadoDataTable)
    {
        return $iestadoDataTable->render('iestados.index');
    }

    /**
     * Show the form for creating a new Iestado.
     *
     * @return Response
     */
    public function create()
    {
        return view('iestados.create');
    }

    /**
     * Store a newly created Iestado in storage.
     *
     * @param CreateIestadoRequest $request
     *
     * @return Response
     */
    public function store(CreateIestadoRequest $request)
    {
        $input = $request->all();

        $iestado = $this->iestadoRepository->create($input);

        Flash::success('Estado de Articulo guardado exitosamente.');

        return redirect(route('iestados.index'));
    }

    /**
     * Display the specified Iestado.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $iestado = $this->iestadoRepository->findWithoutFail($id);

        if (empty($iestado)) {
            Flash::error('Estado de Articulo no encontrado');

            return redirect(route('iestados.index'));
        }

        return view('iestados.show',compact('iestado'));
    }

    /**
     * Show the form for editing the specified Iestado.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $iestado = $this->iestadoRepository->findWithoutFail($id);

        if (empty($iestado)) {
            Flash::error('Estado de Articulo no encontrado');

            return redirect(route('iestados.index'));
        }

        return view('iestados.edit',compact('iestado'));
    }

    /**
     * Update the specified Iestado in storage.
     *
     * @param  int              $id
     * @param UpdateIestadoRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateIestadoRequest $request)
    {
        $iestado = $this->iestadoRepository->findWithoutFail($id);

        if (empty($iestado)) {
            Flash::error('Estado de Articulo no encontrado');

            return redirect(route('iestados.index'));
        }

        $iestado = $this->iestadoRepository->update($request->all(), $id);

        Flash::success('Estado de Articulo actualizado exitosamente.');

        return redirect(route('iestados.index'));
    }

    /**
     * Remove the specified Iestado from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $iestado = $this->iestadoRepository->findWithoutFail($id);

        if (empty($iestado)) {
            Flash::error('Estado de Articulo no encontrado');

            return redirect(route('iestados.index'));
        }

        $this->iestadoRepository->delete($id);

        Flash::success('Estado de Articulo eliminado exitosamente.');

        return redirect(route('iestados.index'));
    }
}
