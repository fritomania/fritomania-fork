<?php

namespace App\Http\Controllers;

use App\DataTables\ItemStockTiendaDataTable;
use App\DataTables\Scopes\TipoItemDatatableScope;
use App\Models\Item;
use App\Models\Tienda;
use Illuminate\Http\Request;

class InventarioController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(ItemStockTiendaDataTable $dataTable){

        return $dataTable->render('inventario.stock');
    }

    public function stockMp(ItemStockTiendaDataTable $dataTable){

        $tipo = 'Materia Prima';
        $dataTable->addScope(new TipoItemDatatableScope('mp'));
        return $dataTable->render('inventario.stock',compact('tipo'));
    }

    public function stockPf(ItemStockTiendaDataTable $dataTable){

        $tipo = 'Producto Final';
        $dataTable->addScope(new TipoItemDatatableScope('pf'));
        return $dataTable->render('inventario.stock',compact('tipo'));
    }

    public function stockCriticoView(Request $request)
    {
        $tienda_id = isset($request->tienda_id) ? $request->tienda_id : session('tienda');

        $tienda = Tienda::find($tienda_id);

        $items  = Item::enTienda($tienda_id)->NoCombo()->get();

        $stocks = $tienda->stockCriticos()->pluck('cantidad','item_id')->toArray();

        return view('inventario.stock_critico',compact('items','tienda','tienda_id','stocks'));
    }

    public function stockCriticoStore(Request $request)
    {

//        dd($request->items);
        $tienda = Tienda::find($request->tienda_id);

        $tienda->stockCriticos()->sync($request->items);

        flash('Datos guardados!!')->success()->important();

        return redirect(route('stock.critico').'?tienda_id='.$tienda->id);
    }
}
