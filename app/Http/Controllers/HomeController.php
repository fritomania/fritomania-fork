<?php

/*
 * Taken from
 * https://github.com/laravel/framework/blob/5.3/src/Illuminate/Auth/Console/stubs/make/controllers/HomeController.stub
 */

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Models\Caja;
use App\Models\Venta;
use Carbon\Carbon;
use Faker\Test\Provider\LocalizationTest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

/**
 * Class HomeController
 * @package App\Http\Controllers
 */
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth',['except' => ['index']]);

    }

    /**
     * Show the application dashboard.
     *
     * @return Response
     */
    public function indexAdmin(){
        return view('home');

    }

    /**
     * Show the application dashboard.
     *
     * @return Response
     */
    public function index(){


        return view('home_cliente');
//        return view('delivery.index');

    }
}