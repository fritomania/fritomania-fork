<?php

namespace App\Http\Controllers;

use App\DataTables\TpagoDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateTpagoRequest;
use App\Http\Requests\UpdateTpagoRequest;
use App\Repositories\TpagoRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class TpagoController extends AppBaseController
{
    /** @var  TpagoRepository */
    private $tpagoRepository;

    public function __construct(TpagoRepository $tpagoRepo)
    {
        $this->tpagoRepository = $tpagoRepo;
    }

    /**
     * Display a listing of the Tipo de pago.
     *
     * @param TpagoDataTable $tpagoDataTable
     * @return Response
     */
    public function index(TpagoDataTable $tpagoDataTable)
    {
        return $tpagoDataTable->render('tpagos.index');
    }

    /**
     * Show the form for creating a new Tipo de pago.
     *
     * @return Response
     */
    public function create()
    {
        return view('tpagos.create');
    }

    /**
     * Store a newly created Tipo de pago in storage.
     *
     * @param CreateTpagoRequest $request
     *
     * @return Response
     */
    public function store(CreateTpagoRequest $request)
    {
        $input = $request->all();

        $tpago = $this->tpagoRepository->create($input);

        Flash::success('Tipo de pago guardado exitosamente.');

        return redirect(route('tpagos.index'));
    }

    /**
     * Display the specified Tipo de pago.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $tpago = $this->tpagoRepository->findWithoutFail($id);

        if (empty($tpago)) {
            Flash::error('Tipo de pago no encontrado');

            return redirect(route('tpagos.index'));
        }

        return view('tpagos.show')->with('tpago', $tpago);
    }

    /**
     * Show the form for editing the specified Tipo de pago.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $tpago = $this->tpagoRepository->findWithoutFail($id);

        if (empty($tpago)) {
            Flash::error('Tipo de pago no encontrado');

            return redirect(route('tpagos.index'));
        }

        return view('tpagos.edit')->with('tpago', $tpago);
    }

    /**
     * Update the specified Tipo de pago in storage.
     *
     * @param  int              $id
     * @param UpdateTpagoRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateTpagoRequest $request)
    {
        $tpago = $this->tpagoRepository->findWithoutFail($id);

        if (empty($tpago)) {
            Flash::error('Tipo de pago no encontrado');

            return redirect(route('tpagos.index'));
        }

        $tpago = $this->tpagoRepository->update($request->all(), $id);

        Flash::success('Tipo de pago actualizado exitosamente.');

        return redirect(route('tpagos.index'));
    }

    /**
     * Remove the specified Tipo de pago from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $tpago = $this->tpagoRepository->findWithoutFail($id);

        if (empty($tpago)) {
            Flash::error('Tipo de pago no encontrado');

            return redirect(route('tpagos.index'));
        }

        $this->tpagoRepository->delete($id);

        Flash::success('Tipo de pago eliminado exitosamente.');

        return redirect(route('tpagos.index'));
    }
}
