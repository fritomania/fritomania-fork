<?php

namespace App\Http\Controllers;

use App\DataTables\CuadreDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateCuadreRequest;
use App\Http\Requests\UpdateCuadreRequest;
use App\Repositories\CuadreRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class CuadreController extends AppBaseController
{
    /** @var  CuadreRepository */
    private $cuadreRepository;

    public function __construct(CuadreRepository $cuadreRepo)
    {
        $this->cuadreRepository = $cuadreRepo;
        $this->middleware('auth');
    }

    /**
     * Display a listing of the Cuadre.
     *
     * @param CuadreDataTable $cuadreDataTable
     * @return Response
     */
    public function index(CuadreDataTable $cuadreDataTable)
    {
        return $cuadreDataTable->render('cuadres.index');
    }

    /**
     * Show the form for creating a new Cuadre.
     *
     * @return Response
     */
    public function create()
    {
        return view('cuadres.create');
    }

    /**
     * Store a newly created Cuadre in storage.
     *
     * @param CreateCuadreRequest $request
     *
     * @return Response
     */
    public function store(CreateCuadreRequest $request)
    {
        $input = $request->all();

        $cuadre = $this->cuadreRepository->create($input);

        Flash::success('Cuadre guardado exitosamente.');

        return redirect(route('cuadres.index'));
    }

    /**
     * Display the specified Cuadre.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $cuadre = $this->cuadreRepository->findWithoutFail($id);

        if (empty($cuadre)) {
            Flash::error('Cuadre no encontrado');

            return redirect(route('cuadres.index'));
        }

        return view('cuadres.show',compact('cuadre'));
    }

    /**
     * Show the form for editing the specified Cuadre.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $cuadre = $this->cuadreRepository->findWithoutFail($id);

        if (empty($cuadre)) {
            Flash::error('Cuadre no encontrado');

            return redirect(route('cuadres.index'));
        }

        return view('cuadres.edit',compact('cuadre'));
    }

    /**
     * Update the specified Cuadre in storage.
     *
     * @param  int              $id
     * @param UpdateCuadreRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCuadreRequest $request)
    {
        $cuadre = $this->cuadreRepository->findWithoutFail($id);

        if (empty($cuadre)) {
            Flash::error('Cuadre no encontrado');

            return redirect(route('cuadres.index'));
        }

        $cuadre = $this->cuadreRepository->update($request->all(), $id);

        Flash::success('Cuadre actualizado exitosamente.');

        return redirect(route('cuadres.index'));
    }

    /**
     * Remove the specified Cuadre from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $cuadre = $this->cuadreRepository->findWithoutFail($id);

        if (empty($cuadre)) {
            Flash::error('Cuadre no encontrado');

            return redirect(route('cuadres.index'));
        }

        $this->cuadreRepository->delete($id);

        Flash::success('Cuadre eliminado exitosamente.');

        return redirect(route('cuadres.index'));
    }
}
