<?php

namespace App\Http\Controllers;

use App\DataTables\TipoMarcajeDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateTipoMarcajeRequest;
use App\Http\Requests\UpdateTipoMarcajeRequest;
use App\Repositories\TipoMarcajeRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class TipoMarcajeController extends AppBaseController
{
    /** @var  TipoMarcajeRepository */
    private $tipoMarcajeRepository;

    public function __construct(TipoMarcajeRepository $tipoMarcajeRepo)
    {
        $this->tipoMarcajeRepository = $tipoMarcajeRepo;
        $this->middleware('auth');
    }

    /**
     * Display a listing of the TipoMarcaje.
     *
     * @param TipoMarcajeDataTable $tipoMarcajeDataTable
     * @return Response
     */
    public function index(TipoMarcajeDataTable $tipoMarcajeDataTable)
    {
        return $tipoMarcajeDataTable->render('tipo_marcajes.index');
    }

    /**
     * Show the form for creating a new TipoMarcaje.
     *
     * @return Response
     */
    public function create()
    {
        return view('tipo_marcajes.create');
    }

    /**
     * Store a newly created TipoMarcaje in storage.
     *
     * @param CreateTipoMarcajeRequest $request
     *
     * @return Response
     */
    public function store(CreateTipoMarcajeRequest $request)
    {
        $input = $request->all();

        $tipoMarcaje = $this->tipoMarcajeRepository->create($input);

        Flash::success('Tipo Marcaje guardado exitosamente.');

        return redirect(route('tipoMarcajes.index'));
    }

    /**
     * Display the specified TipoMarcaje.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $tipoMarcaje = $this->tipoMarcajeRepository->findWithoutFail($id);

        if (empty($tipoMarcaje)) {
            Flash::error('Tipo Marcaje no encontrado');

            return redirect(route('tipoMarcajes.index'));
        }

        return view('tipo_marcajes.show',compact('tipoMarcaje'));
    }

    /**
     * Show the form for editing the specified TipoMarcaje.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $tipoMarcaje = $this->tipoMarcajeRepository->findWithoutFail($id);

        if (empty($tipoMarcaje)) {
            Flash::error('Tipo Marcaje no encontrado');

            return redirect(route('tipoMarcajes.index'));
        }

        return view('tipo_marcajes.edit',compact('tipoMarcaje'));
    }

    /**
     * Update the specified TipoMarcaje in storage.
     *
     * @param  int              $id
     * @param UpdateTipoMarcajeRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateTipoMarcajeRequest $request)
    {
        $tipoMarcaje = $this->tipoMarcajeRepository->findWithoutFail($id);

        if (empty($tipoMarcaje)) {
            Flash::error('Tipo Marcaje no encontrado');

            return redirect(route('tipoMarcajes.index'));
        }

        $tipoMarcaje = $this->tipoMarcajeRepository->update($request->all(), $id);

        Flash::success('Tipo Marcaje actualizado exitosamente.');

        return redirect(route('tipoMarcajes.index'));
    }

    /**
     * Remove the specified TipoMarcaje from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $tipoMarcaje = $this->tipoMarcajeRepository->findWithoutFail($id);

        if (empty($tipoMarcaje)) {
            Flash::error('Tipo Marcaje no encontrado');

            return redirect(route('tipoMarcajes.index'));
        }

        $this->tipoMarcajeRepository->delete($id);

        Flash::success('Tipo Marcaje eliminado exitosamente.');

        return redirect(route('tipoMarcajes.index'));
    }
}
