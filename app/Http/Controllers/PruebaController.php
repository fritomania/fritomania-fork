<?php

namespace App\Http\Controllers;

use App\Mail\AtenderProduccion;
use App\Mail\DespacharSolicitud;
use App\Mail\OrdenCompraMail;
use App\Mail\PedidoWebMail;
use App\Mail\ProduccionSolicitud;
use App\Mail\RecepcionCompra;
use App\Mail\SolicitudStock;
use App\Mail\StockCriticoPorSolicitudMail;
use App\Mail\StockCriticoPorVentaMail;
use App\Models\Compra;
use App\Models\Item;
use App\Models\Produccione;
use App\Models\Solicitude;
use App\Models\Venta;
use App\Models\Vestado;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Swagger\Annotations\Items;

class PruebaController extends AppBaseController
{
    public function index(Request $request)
    {
//        if (App::environment('production')) {
//            abort(403);
//        }

        $datos= null;

        if ($request->mail){
            switch ($request->mail){
                case 'atender_produccion':
                    $produccion = Produccione::find(1);
                    if(!$produccion){
                        flash('No existe ninguna produccion')->warning()->important();
                        return redirect(route('producciones.create'));
                    }

                    if($request->enviar){
                        \Mail::send(new AtenderProduccion($produccion));
                    }

                    return new AtenderProduccion($produccion);
                    break;
                case 'despachar_solicitud':
                    $sol = Solicitude::find(1);
                    if(!$sol){
                        flash('No existe ninguna solicitud')->warning()->important();
                        return redirect(route('solicitudes.create'));
                    }

                    if($request->enviar){
                        \Mail::send(new DespacharSolicitud($sol));
                    }

                    return new DespacharSolicitud($sol);
                    break;
                case 'orden_compra':
                    $orden = Compra::find(1);
                    if(!$orden){
                        flash('No existe ninguna orden de compra')->warning()->important();
                        return redirect(route('compras.create'));
                    }

                    if($request->enviar){
                        \Mail::send(new OrdenCompraMail($orden));
                    }

                    return new OrdenCompraMail($orden);
                    break;
                case 'produccion_solicitud':
                    $produccion = Produccione::find(1);
                    if(!$produccion){
                        flash('No existe ninguna solicitud')->warning()->important();
                        return redirect(route('producciones.create'));
                    }

                    if($request->enviar){
                        \Mail::send(new ProduccionSolicitud($produccion));
                    }

                    return new ProduccionSolicitud($produccion);
                    break;
                case 'recepcion_compra':
                    $orden = Compra::find(1);
                    if(!$orden){
                        flash('No existe ninguna orden de compra')->warning()->important();
                        return redirect(route('compras.create'));
                    }

                    if($request->enviar){
                        \Mail::send(new RecepcionCompra($orden));
                    }

                    return new RecepcionCompra($orden);
                    break;
                case 'solicitud_stock':
                    $sol = Solicitude::find(1);
                    if(!$sol){
                        flash('No existe ninguna solicitud')->warning()->important();
                        return redirect(route('solicitudes.create'));
                    }

                    if($request->enviar){
                        \Mail::send(new  SolicitudStock($sol));
                    }


                    return new SolicitudStock($sol);
                    break;
                case 'stock_critico_por_venta_mail':
                    $items = Item::all()->random(5);
                    $venta = Venta::find(1);

                    if($request->enviar){
                        \Mail::send(new StockCriticoPorVentaMail($items,$venta));
                    }
                    return new StockCriticoPorVentaMail($items,$venta);
                    break;

                case 'stock_critico_por_solicitud_mail':
                    $items = Item::all()->random(5);
                    $solicitud = Solicitude::find(1);

                    if($request->enviar){
                        \Mail::send(new StockCriticoPorSolicitudMail($items,$solicitud));
                    }
                    return new StockCriticoPorSolicitudMail($items,$solicitud);
                    break;
                case 'pedido_web':
                    $pedido = Venta::find(1);

                    $msg ="Su pedido N° ".$pedido->id." esta en camino";

                    if($pedido->estado->id==Vestado::PAGADA){
                        $msg ="Su pedido N° ".$pedido->id." ha sido recibido con éxito";
                    }

                    if($request->enviar){
                        \Mail::send(new PedidoWebMail($pedido,$msg));
                    }

                    return new PedidoWebMail($pedido,$msg);
                    break;
            }
        }

        if ($request->promedio){
            $datos = Item::find($request->promedio);

            if ($datos){
                $datos= $datos->promedioPreparacion();
            }

            dd($datos);
        }

        //Código de pruebas
        return view('pruebas',compact('datos'));

    }
}
