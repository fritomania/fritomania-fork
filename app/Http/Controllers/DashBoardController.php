<?php

namespace App\Http\Controllers;


use App\Models\Caja;
use App\Models\Gasto;
use App\Models\Horario;
use App\Models\Venta;
use App\Models\Vestado;
use App\Models\Vpago;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Session;

class DashBoardController extends Controller
{

    /**
     * DashBoardController constructor.
     */
    public function __construct(){
        $this->middleware('auth');
    }

    public function index(){

        $tienda = session('tienda');

        $user = Auth::user();
        $cajaUser= $user->cajaAbierta();

        $ventasHoy = Venta::tienda($tienda)->hoy()->procesadas()->noCredito()->noNegocio()->get();
        $vpagos = Vpago::hoy()->get();

        $gastos = Gasto::hoy()->get();

        $cajas = Caja::deTienda()->hoy()->get();

        $totalDia = $cajas->sum('monto_apertura') + $ventasHoy->sum('total');

        $totalVpagos = array_sum(array_pluck($vpagos ,'monto'));


        $totalVentasHoyUser = $user->totalVentasHoy() + $user->totalCobrosHoy();
        $totalVentasHoy = array_sum(array_pluck($ventasHoy ,'total')) + $totalVpagos;

        $totalGastos = array_sum(array_pluck($gastos,'monto'));


        return view('admin.dashboard',compact('totalVentasHoy','totalVentasHoyUser','cajaUser','totalGastos','totalDia'));

    }

    public function graficaVentasDia(){

        $diaSemana=Carbon::now()->dayOfWeek;
        $diaSemana = $diaSemana==0 ? 7 : $diaSemana;
        $estadoAnulada = Vestado::ANULADA;

        $horarios= Horario::where('dia','=',$diaSemana)->get();

        $horaini = $horarios[0]->hora_ini;
        $horafin = $horarios[0]->hora_fin;

        $tienda = session('tienda');

        $results = DB::select( DB::raw("
            select 
                hour(v.created_at) hora,sum((d.cantidad* d.precio)) monto
            from 
                ventas v inner join venta_detalles d on v.id= d.venta_id
            where
                v.fecha=curdate()
                and v.vestado_id not in($estadoAnulada)
                and cliente_id != 2
                and v.tienda_id = $tienda
            group by
                1
        ") );

        $horas = array_pluck($results,'monto','hora');

        $horaActual= Carbon::now()->hour;

        $datos=[];
        for($i=$horaini;$i<=$horafin;$i++){

            if($i<=$horaActual){
                $datos[$i]= isset($horas[$i]) ? $horas[$i] : 0 ;
            }else{
                $datos[$i]= isset($horas[$i]) ? $horas[$i] : NULL ;
            }

        }


        return response()->json([
            'horaini' => $horaini,
            'horafin' => $horafin,
            'datos' => $datos
        ]);


    }

    public function graficaVentasMes(){

        $diasMes=diasMesActual();
        $estadoAnulada = Vestado::ANULADA;
        $tienda = session('tienda');

        $results = DB::select( DB::raw("
            select 
                day(v.created_at) dia,sum((d.cantidad* d.precio)) monto
            from 
                ventas v inner join venta_detalles d on v.id= d.venta_id
            where
                month(v.fecha)=MONTH(CURDATE())
                and v.vestado_id not in($estadoAnulada)
                and v.cliente_id!=2
                and (v.credito=0 or v.pagada=1)
                and v.deleted_at IS NULL
                and v.tienda_id = $tienda
            group by
                1
        ") );

        $dias = array_pluck($results,'monto','dia');

        $diaActual=Carbon::now()->day;

        $datos=[];
        for($i=1;$i<=$diasMes;$i++){

            if($i<=$diaActual){
                $datos[$i]= isset($dias[$i]) ? $dias[$i] : 0 ;
            }else{
                $datos[$i]= isset($dias[$i]) ? $dias[$i] : NULL ;
            }

        }

        return response()->json([
            'diasmes' => $diasMes,
            'datos' => $datos
        ]);
    }

    public function graficaVentasAnio(){
        $estadoAnulada = Vestado::ANULADA;
        $tienda = session('tienda');

        $results = DB::select( DB::raw("
            select 
                month(v.created_at) mes,sum((d.cantidad* d.precio)) monto
            from 
                ventas v inner join venta_detalles d on v.id= d.venta_id
            where
                year(v.fecha)=year(CURDATE())
                and v.vestado_id not in($estadoAnulada)
                and v.cliente_id!=2
                and (v.credito=0 or v.pagada=1)
                and v.deleted_at IS NULL
                and v.tienda_id = $tienda
            group by
                1
        ") );

        $meses = array_pluck($results,'monto','mes');

        $mesActual=Carbon::now()->month;

        $datos=[];
        for($i=1;$i<=12;$i++){

            if($i<=$mesActual){
                $datos[$i]= isset($meses[$i]) ? $meses[$i] : 0 ;
            }else{
                $datos[$i]= isset($meses[$i]) ? $meses[$i] : NULL ;
            }
        }

        return response()->json(['datos' => $datos]);
    }
}
