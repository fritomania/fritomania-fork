<?php

namespace App\Http\Controllers;

use App\DataTables\CretiroDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateCretiroRequest;
use App\Http\Requests\UpdateCretiroRequest;
use App\Repositories\CretiroRepository;
use App\Repositories\GastoRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Illuminate\Support\Facades\Auth;
use Response;

class CretiroController extends AppBaseController
{
    /** @var  CretiroRepository */
    private $cretiroRepository;
    private $gastoRepository;

    public function __construct(CretiroRepository $cretiroRepo,GastoRepository $gastoRepo)
    {
        $this->cretiroRepository = $cretiroRepo;
        $this->gastoRepository = $gastoRepo;
        $this->middleware('auth');
    }

    /**
     * Display a listing of the Cretiro.
     *
     * @param CretiroDataTable $cretiroDataTable
     * @return Response
     */
    public function index(CretiroDataTable $cretiroDataTable)
    {
        return $cretiroDataTable->render('cretiros.index');
    }

    /**
     * Show the form for creating a new Cretiro.
     *
     * @return Response
     */
    public function create()
    {

        //evita que guarde la misma url
        if(url()->previous()!=route('cretiros.create')){
            //Guarda la url anterior en variable de session flash
            request()->session()->put('backUrl',url()->previous());
        }

        $cajaUser = Auth::user()->cajaAbierta();

        //si el usuario no tiene caja abierta
        if(!$cajaUser){
            flash('Debe abrir primero una caja antes de poder realizar un retiro')->error()->important();
            return redirect(route('cajas.create'));
        }

        return view('cretiros.create');
    }

    /**
     * Store a newly created Retiro de Caja in storage.
     *
     * @param CreateCretiroRequest $request
     *
     * @return Response
     */
    public function store(CreateCretiroRequest $request)
    {
        $cajaUser = Auth::user()->cajaAbierta();

        if($cajaUser->monto_cierre< $request->monto){
            flash('El monto superoa el total de su caja')->error()->important();
            return redirect(route('cretiros.create'));
        }
        $input = $request->all();

        $cretiro = $this->cretiroRepository->create($input);

        if($cretiro && $request->tambien_es_gasto){
            $input['descripcion'] = $input['motivo'];
            $input['user_id'] = Auth::user()->id;
            $input['tienda_id'] = session('tienda');
            $gasto = $this->gastoRepository->create($input);
        }

        Flash::success('Retiro de Caja guardado exitosamente.');


        $url = session('backUrl') == route('cretiros.index') ? route('cretiros.index') : session('backUrl');

        session()->forget('backUrl');

        return redirect()->to($url);
    }

    /**
     * Display the specified Cretiro.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $cretiro = $this->cretiroRepository->findWithoutFail($id);

        if (empty($cretiro)) {
            Flash::error('Retiro de Caja no encontrado');

            return redirect(route('cretiros.index'));
        }

        return view('cretiros.show')->with('cretiro', $cretiro);
    }

    /**
     * Show the form for editing the specified Cretiro.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $cretiro = $this->cretiroRepository->findWithoutFail($id);

        if (empty($cretiro)) {
            Flash::error('Retiro de Caja no encontrado');

            return redirect(route('cretiros.index'));
        }

        return view('cretiros.edit')->with('cretiro', $cretiro);
    }

    /**
     * Update the specified Retiro de Caja in storage.
     *
     * @param  int              $id
     * @param UpdateCretiroRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCretiroRequest $request)
    {
        $cretiro = $this->cretiroRepository->findWithoutFail($id);

        if (empty($cretiro)) {
            Flash::error('Retiro de Caja no encontrado');

            return redirect(route('cretiros.index'));
        }

        $cretiro = $this->cretiroRepository->update($request->all(), $id);

        Flash::success('Retiro de Caja actualizado exitosamente.');

        return redirect(route('cretiros.index'));
    }

    /**
     * Remove the specified Retiro de Caja from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $cretiro = $this->cretiroRepository->findWithoutFail($id);

        if (empty($cretiro)) {
            Flash::error('Retiro de Caja no encontrado');

            return redirect(route('cretiros.index'));
        }

        $this->cretiroRepository->delete($id);

        Flash::success('Retiro de Caja eliminado exitosamente.');

        return redirect(route('cretiros.index'));
    }
}
