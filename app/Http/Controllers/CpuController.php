<?php

namespace App\Http\Controllers;

use App\DataTables\CpuDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateCpuRequest;
use App\Http\Requests\UpdateCpuRequest;
use App\Models\Etipo;
use App\Models\Unimed;
use App\Repositories\CpuRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class CpuController extends AppBaseController
{
    /** @var  CpuRepository */
    private $cpuRepository;

    public function __construct(CpuRepository $cpuRepo)
    {
        $this->middleware('auth');
        $this->cpuRepository = $cpuRepo;
    }

    /**
     * Display a listing of the Cpu.
     *
     * @param CpuDataTable $cpuDataTable
     * @return Response
     */
    public function index(CpuDataTable $cpuDataTable)
    {
        return $cpuDataTable->render('cpus.index');
    }

    /**
     * Show the form for creating a new Cpu.
     *
     * @return Response
     */
    public function create()
    {
        $tipos = Etipo::pluck('nombre','id')->toArray();
        $unimeds = Unimed::where('magnitude_id','8')->pluck('nombre','id')->toArray();

        return view('cpus.create',compact('tipos','unimeds'));
    }

    /**
     * Store a newly created Cpu in storage.
     *
     * @param CreateCpuRequest $request
     *
     * @return Response
     */
    public function store(CreateCpuRequest $request)
    {
        $input = $request->all();

        $cpu = $this->cpuRepository->create($input);

        Flash::success('Cpu saved successfully.');

        return redirect(route('cpus.index'));
    }

    /**
     * Display the specified Cpu.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $cpu = $this->cpuRepository->findWithoutFail($id);

        if (empty($cpu)) {
            Flash::error('Cpu not found');

            return redirect(route('cpus.index'));
        }

        return view('cpus.show')->with('cpu', $cpu);
    }

    /**
     * Show the form for editing the specified Cpu.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $cpu = $this->cpuRepository->findWithoutFail($id);

        if (empty($cpu)) {
            Flash::error('Cpu not found');

            return redirect(route('cpus.index'));
        }

        $tipos = Etipo::pluck('nombre','id')->toArray();
        $unimeds = Unimed::where('magnitude_id','8')->pluck('nombre','id')->toArray();

        return view('cpus.edit',compact('tipos','unimeds','cpu'));
    }

    /**
     * Update the specified Cpu in storage.
     *
     * @param  int              $id
     * @param UpdateCpuRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCpuRequest $request)
    {
        $cpu = $this->cpuRepository->findWithoutFail($id);

        if (empty($cpu)) {
            Flash::error('Cpu not found');

            return redirect(route('cpus.index'));
        }

        $cpu = $this->cpuRepository->update($request->all(), $id);

        Flash::success('Cpu updated successfully.');

        return redirect(route('cpus.index'));
    }

    /**
     * Remove the specified Cpu from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $cpu = $this->cpuRepository->findWithoutFail($id);

        if (empty($cpu)) {
            Flash::error('Cpu not found');

            return redirect(route('cpus.index'));
        }

        $this->cpuRepository->delete($id);

        Flash::success('Cpu deleted successfully.');

        return redirect(route('cpus.index'));
    }
}
