<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class BannerClienteController extends AppBaseController
{

    /**
     * BannerClienteController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('banner');
    }
}
