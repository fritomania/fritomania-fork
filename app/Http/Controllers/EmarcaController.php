<?php

namespace App\Http\Controllers;

use App\DataTables\EmarcaDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateEmarcaRequest;
use App\Http\Requests\UpdateEmarcaRequest;
use App\Repositories\EmarcaRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class EmarcaController extends AppBaseController
{
    /** @var  EmarcaRepository */
    private $emarcaRepository;

    public function __construct(EmarcaRepository $emarcaRepo)
    {
        $this->middleware('auth');
        $this->emarcaRepository = $emarcaRepo;
    }

    /**
     * Display a listing of the Emarca.
     *
     * @param EmarcaDataTable $emarcaDataTable
     * @return Response
     */
    public function index(EmarcaDataTable $emarcaDataTable)
    {
        return $emarcaDataTable->render('emarcas.index');
    }

    /**
     * Show the form for creating a new Emarca.
     *
     * @return Response
     */
    public function create()
    {
        return view('emarcas.create');
    }

    /**
     * Store a newly created Emarca in storage.
     *
     * @param CreateEmarcaRequest $request
     *
     * @return Response
     */
    public function store(CreateEmarcaRequest $request)
    {
        $input = $request->all();

        $emarca = $this->emarcaRepository->create($input);

        Flash::success('Emarca saved successfully.');

        return redirect(route('emarcas.index'));
    }

    /**
     * Display the specified Emarca.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $emarca = $this->emarcaRepository->findWithoutFail($id);

        if (empty($emarca)) {
            Flash::error('Emarca not found');

            return redirect(route('emarcas.index'));
        }

        return view('emarcas.show')->with('emarca', $emarca);
    }

    /**
     * Show the form for editing the specified Emarca.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $emarca = $this->emarcaRepository->findWithoutFail($id);

        if (empty($emarca)) {
            Flash::error('Emarca not found');

            return redirect(route('emarcas.index'));
        }

        return view('emarcas.edit')->with('emarca', $emarca);
    }

    /**
     * Update the specified Emarca in storage.
     *
     * @param  int              $id
     * @param UpdateEmarcaRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateEmarcaRequest $request)
    {
        $emarca = $this->emarcaRepository->findWithoutFail($id);

        if (empty($emarca)) {
            Flash::error('Emarca not found');

            return redirect(route('emarcas.index'));
        }

        $emarca = $this->emarcaRepository->update($request->all(), $id);

        Flash::success('Emarca updated successfully.');

        return redirect(route('emarcas.index'));
    }

    /**
     * Remove the specified Emarca from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $emarca = $this->emarcaRepository->findWithoutFail($id);

        if (empty($emarca)) {
            Flash::error('Emarca not found');

            return redirect(route('emarcas.index'));
        }

        $this->emarcaRepository->delete($id);

        Flash::success('Emarca deleted successfully.');

        return redirect(route('emarcas.index'));
    }
}
