<?php

namespace App\Http\Controllers;

use App\Models\Icategoria;
use App\Models\Vestado;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class RptVentasController extends Controller
{

    /**
     * RptVentasController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {

        //dd($inputs);
        $cats = Icategoria::pluck('nombre','id');
        $cats = $cats->prepend('Todas','T');
        $cats = $cats->put('S','Sin Categoría');
        $cat = $request->icategoria_id ? $request->icategoria_id : null;
        $del = $request->del ? fechaDb($request->del) : iniMesDb();
        $al = $request->al ? fechaDb($request->al) : hoyDb();


        $whereCatagoria='';

        if($cat!='T'){
            $whereCatagoria="and i.icategoria_id='$cat'";
        }
        if($cat=='S'){
            $whereCatagoria="and i.icategoria_id is null";
        }

        $estadoAnulada = Vestado::ANULADA;

        $query ="
            select 
                i.codigo,i.nombre,sum(d.cantidad) cant,sum(if(v.credito,0,d.precio)*d.cantidad) sub
            from 
                ventas v inner join venta_detalles d on v.id=d.venta_id
                inner join items i on d.item_id=i.id
            where 
                v.vestado_id not in($estadoAnulada)
                $whereCatagoria
                and v.fecha BETWEEN '$del' and '$al'
                and v.deleted_at is NULL 
            group by
                1,2
            order by
                1,2,3 desc
        ";
//        dd($query);
        $detalles = DB::select($query);

        $buscar = $request->buscar;

        return view('reportes.ventas.rpt_ventas_por_cat',compact('cats','del','al','cat','detalles','buscar'));
    }

    /**
     * Reporte de ventas por categoría resumen
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function porCatRes(Request $request)
    {

        $del = $request->del ? fechaDb($request->del) : hoyDb();
        $al = $request->al ? fechaDb($request->al) : hoyDb();
        $estadoAnulada = Vestado::ANULADA;

        $q = "
            select 
                IFNULL(ic.nombre,'Sin categoría') as nombre,sum(d.cantidad) cant,sum(d.cantidad*d.precio) monto,sum(d.cantidad * if(precio_promedio,precio_promedio,precio_compra)) costo
                ,sum(d.cantidad*d.precio) - sum(d.cantidad * if(precio_promedio,precio_promedio,precio_compra)) utilidad
            from 
                ventas v inner join venta_detalles d on v.id=d.venta_id
                inner join items i on d.item_id=i.id
                left join icategorias ic on i.icategoria_id=ic.id
            where
                v.fecha BETWEEN '$del' and '$al'
                and v.vestado_id not in($estadoAnulada)
                and v.cliente_id!=2
                and (v.credito=0 or v.pagada=1)
                and v.deleted_at IS NULL 
            group by
                1
            order by
                1 desc 
        ";

        $detalles =DB::select($q);

        return view('reportes.ventas.rpt_ventas_por_cat_res',compact('detalles','del','al'));
    }
}
