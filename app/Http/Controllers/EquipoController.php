<?php

namespace App\Http\Controllers;

use App\DataTables\EquipoDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateEquipoRequest;
use App\Http\Requests\UpdateEquipoRequest;
use App\Models\Eatributo;
use App\Models\Emarca;
use App\Models\Equipo;
use App\Models\EquipoArchivo;
use App\Models\Etipo;
use App\Models\Marca;
use App\Models\Modelo;
use App\Repositories\EquipoRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class EquipoController extends AppBaseController
{
    /** @var  EquipoRepository */
    private $equipoRepository;
    private $tipos;
    private $marcas;
    private $atributos;

    public function __construct(EquipoRepository $equipoRepo)
    {
        $this->middleware('auth');
        $this->equipoRepository = $equipoRepo;
        $this->tipos = Etipo::pluck('nombre','id')->toArray();
        $this->marcas = Emarca::pluck('nombre','id')->toArray();
        $this->atributos = Eatributo::pluck('nombre','id')->toArray();

    }

    /**
     * Display a listing of the Equipo.
     *
     * @param EquipoDataTable $equipoDataTable
     * @return Response
     */
    public function index(EquipoDataTable $equipoDataTable)
    {
        return $equipoDataTable->render('equipos.index');
    }

    /**
     * Show the form for creating a new Equipo.
     *
     * @return Response
     */
    public function create()
    {

        $tipos = $this->tipos; $marcas = $this->marcas; $atributos = $this->atributos;
        $modelos = [];

        return view('equipos.create',compact('tipos','marcas','modelos','atributos'));
    }

    /**
     * Store a newly created Equipo in storage.
     *
     * @param CreateEquipoRequest $request
     *
     * @return Response
     */
    public function store(CreateEquipoRequest $request)
    {
        $input = $request->all();

        $equipo = $this->equipoRepository->create($input);

        if(is_null($input['numero_serie'])){
            $equipo->numero_serie = $equipo->id.$equipo->etipo_id.$equipo->emarca_id.$equipo->modelo_id;
            $equipo->save();
        }

        Flash::success('Equipo guardado exitosamente.');

        return redirect(route('equipos.edit',$equipo->id));

    }

    /**
     * Display the specified Equipo.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $equipo = $this->equipoRepository->findWithoutFail($id);

        if (empty($equipo)) {
            Flash::error('Equipo not found');

            return redirect(route('equipos.index'));
        }

        return view('equipos.show')->with('equipo', $equipo);
    }

    /**
     * Show the form for editing the specified Equipo.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $equipo = $this->equipoRepository->findWithoutFail($id);

        if (empty($equipo)) {
            Flash::error('Equipo not found');

            return redirect(route('equipos.index'));
        }

        $tipos = $this->tipos; $marcas = $this->marcas; $atributos = $this->atributos;
        $modelos = Modelo::where('emarca_id','=',$equipo->emarca_id)->pluck('nombre','id')->toArray();

        return view('equipos.edit',compact('equipo','tipos','marcas','modelos','atributos'));
    }

    /**
     * Update the specified Equipo in storage.
     *
     * @param  int              $id
     * @param UpdateEquipoRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateEquipoRequest $request)
    {
        $equipo = $this->equipoRepository->findWithoutFail($id);

        if (empty($equipo)) {
            Flash::error('Equipo not found');

            return redirect(route('equipos.index'));
        }

        $equipo = $this->equipoRepository->update($request->all(), $id);

        Flash::success('Equipo updated successfully.');

        return redirect(route('equipos.edit',$equipo->id));
    }

    /**
     * Remove the specified Equipo from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $equipo = $this->equipoRepository->findWithoutFail($id);

        if (empty($equipo)) {
            Flash::error('Equipo not found');

            return redirect(route('equipos.index'));
        }

        $this->equipoRepository->delete($id);

        Flash::success('Equipo deleted successfully.');

        return redirect(route('equipos.index'));
    }

    /**
     * Guard de forma ajax los archivos subidos en la tabla equipo_archivos
     * @param Equipo $equipo
     * @param CreateEquipoRequest $requests
     * @return mixed
     */
    public function storeFile(Equipo $equipo,CreateEquipoRequest $requests)
    {

        $files = $requests->file('files');

        $equipoArchivos = collect();
        foreach ($files as $file){

            $datos= [
                'equipo_id' => $equipo->id,
                'data' => fileToBinary($file),
                'nombre' => $file->getClientOriginalName(),
                'type' => $file->getMimeType(),
                'size' => $file->getSize(),
                'extension' => $file->extension()
            ];

            $equipoArchivos->push(New EquipoArchivo($datos));

        }

        $equipo->equipoArchivos()->saveMany($equipoArchivos);

        return Response::json([]);

    }

    public function showFile($id)
    {
        $file = EquipoArchivo::find($id);

//        dd($file->toArray());

        return view('equipos.archivos',['file' => $file]);
    }
}
