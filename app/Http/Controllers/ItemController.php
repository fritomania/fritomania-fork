<?php

namespace App\Http\Controllers;

use App\DataTables\ItemDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateItemRequest;
use App\Http\Requests\UpdateItemRequest;
use App\Models\Icategoria;
use App\Models\Item;
use App\Models\Marca;
use App\Models\Stock;
use App\Models\Unimed;
use App\Models\VentaReceta;
use App\Models\VentaRecetaDetalle;
use App\Repositories\ItemRepository;
use Auth;
use DB;
use Flash;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Response;

class ItemController extends AppBaseController
{
    /** @var  ItemRepository */
    private $itemRepository;

    public function __construct(ItemRepository $itemRepo)
    {
        $this->middleware("auth")->except(['getIngredientes','getAgregados']);
        $this->itemRepository = $itemRepo;
    }

    /**
     * Display a listing of the Item.
     *
     * @param ItemDataTable $itemDataTable
     * @return Response
     */
    public function index(ItemDataTable $itemDataTable)
    {
        return $itemDataTable->render('items.index');
    }

    /**
     * Show the form for creating a new Item.
     *
     * @return Response
     */
    public function create()
    {
        $categorias = Icategoria::pluck("nombre","id")->toArray();
        $marcas = Marca::pluck("nombre","id")->toArray();
        $categoriasItem= 1;

        return view('items.form',compact('unimeds','categorias','categoriasItem','marcas'));
    }

    /**
     * Show the form for clon a new Item.
     *
     * @return Response
     */
    public function clonar(Item $item){

        $categorias = Icategoria::pluck("nombre","id")->toArray();
        $marcas = Marca::pluck("nombre","id")->toArray();
        $categoriasItem= array_pluck($item->icategorias->toArray(),"id");

        return view('items.clone',compact('unimeds','categorias','categoriasItem','marcas','item'));
    }

    /**
     * Store a newly created Item in storage.
     *
     * @param CreateItemRequest $request
     *
     * @return Response
     */
    public function store(CreateItemRequest $request)
    {

        try {
            DB::beginTransaction();

            $this->processStore($request);

        } catch (\Exception $exception) {
            DB::rollBack();

            if (Auth::user()->isSuperAdmin()){
                dd($exception,$exception->getLine());
            }

            $msg = Auth::user()->isAdmin() ? $exception->getMessage() : 'Hubo un error intente de nuevo';

            flash('Error: '.$msg)->error()->important();
            return redirect()->back();
        }

        DB::commit();

        Flash::success('Item guardado exitosamente.');

        return redirect(route('items.index'));
    }

    /**
     * Display the specified Item.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $item = $this->itemRepository->findWithoutFail($id);

        if (empty($item)) {
            Flash::error('Item no existe');

            return redirect(route('items.index'));
        }

        return view('items.show')->with('item', $item);
    }

    /**
     * Show the form for editing the specified Item.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $item = $this->itemRepository->findWithoutFail($id);

        if (empty($item)) {
            Flash::error('Item no existe');

            return redirect(route('items.index'));
        }

        $categorias = Icategoria::pluck("nombre","id")->toArray();
        $categoriasItem= array_pluck($item->icategorias->toArray(),"id");
        $marcas = Marca::pluck("nombre","id")->toArray();
        $edit=1;

        return view('items.form',compact('item','unimeds','categorias','categoriasItem','marcas','edit'));

    }

    /**
     * Update the specified Item in storage.
     *
     * @param  int              $id
     * @param UpdateItemRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateItemRequest $request)
    {

        try {
            DB::beginTransaction();

            $this->processUpdate($id,$request);

        } catch (\Exception $exception) {
            DB::rollBack();

            if (Auth::user()->isSuperAdmin()){
                dd($exception,$exception->getLine());
            }

            $msg = Auth::user()->isAdmin() ? $exception->getMessage() : 'Hubo un error intente de nuevo';

            flash('Error: '.$msg)->error()->important();
            return redirect()->back();
        }


        DB::commit();

        Flash::success('Item actualizado exitosamente.');

        return redirect(route('items.index'));
    }

    /**
     * Remove the specified Item from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $item = $this->itemRepository->findWithoutFail($id);

        if (empty($item)) {
            Flash::error('Item no existe');

            return redirect(route('items.index'));
        }

        $this->itemRepository->delete($id);

        Flash::success('Item eliminado exitosamente.');

        return redirect(route('items.index'));
    }

    public function saveImg($file,Item $item){

        $nameImg= $item->id.'.'.$file->extension();

        $item->imagen = 'img/items/'.$nameImg;
        $item->save();

        $file->move(public_path().'/img/items/',$nameImg);
    }

    public function precioCostoCeroView()
    {
        $items = Item::where('precio_compra',0)->orderBy('icategoria_id')->get();
        return view('items.precio_compra_cero',compact('items'));
    }
    public function precioCostoCeroStore(Request $request)
    {
        $precios = $request->precios;
        foreach ($request->items as $i => $itemId){

            $item = Item::find($itemId);
            if($precios[$i]){
//                dump('Editar precio compra de '.$item->nombre.': '.$item->precio_compra.' => '.$precios[$i]);
                $item->precio_compra = $precios[$i];
                $item->save();
            }
        }

//        dd();

        $items = Item::where('precio_compra',0)->orderBy('icategoria_id')->get();
        return redirect(route('items.precio.compra.cero'));
    }

    public function addIngrediente(Request $request)
    {

//        return $this->sendResponse($request->all(),'request');

        $itemId = $request->item_id;
        $ingredientes = $request->ingrediente;

        $item = Item::find($itemId);

        $receta = $item->ventaReceta();


        try {
            DB::beginTransaction();

            //Si el articulo no tiene receta
            if(!$receta){

                $receta = VentaReceta::create([
                    'item_id' => $item->id,
                ]);

            }

            //detalle que se agrega se forma en base al primer indice del array de ingredientes
            $detalle = new VentaRecetaDetalle([
                'venta_receta_id' => $receta->id,
                'ingrediente_id' => $ingredientes[0],
                'cantidad' => 1
            ]);

            $ingredientePrincipal = $receta->detalles()->save($detalle);

            //si los ingredientes son mas de uno
            if(count($ingredientes)>1){
                $ingredientePrincipal->opcionales()->sync($ingredientes);
            }


        } catch (\Exception $exception) {
            DB::rollBack();
            $msg = $exception->getMessage();

            return $this->sendError($msg);
        }

        DB::commit();


        return $this->sendResponse($receta->detalles,'Ingrediente asociado correctamente');

    }

    public function getIngredientes(Request $request)
    {
        $item = Item::find($request->id);

        if(!$item){
            return $this->sendError('Item no encontrado',400);
        }

        $receta = $item->ventaReceta();

        if ($receta){

            $ingredients = $receta->detalles;

            return $this->sendResponse($ingredients,'Ingredientes del item');
        }

        return $this->sendError('El item no tiene ingredientes asociados');

    }

    public function removeIngrediente(Request $request)
    {

        $detalleReceta = VentaRecetaDetalle::find($request->id);

        if (!$detalleReceta){

            return $this->sendError('Ingrediente no encontrado');
        }

        try {
            DB::beginTransaction();

            $detalleReceta->opcionales()->sync([]);

            $detalleReceta->delete();


        } catch (\Exception $exception) {
            DB::rollBack();
            $msg = $exception->getMessage();

            return $this->sendError($msg);
        }

        DB::commit();

        return $this->sendResponse($detalleReceta,'Ingrediente eliminado correctamente!');

    }

    public function getAgregados(Request $request)
    {

        if ($request->item_id){
            $item = Item::find($request->item_id);
            $adicionales = $item->adicionales;
            $salsas = $item->salsas;
        }else{

            $items = Item::agregados()->get();


            $adicionales = $items->filter(function ($item){
                return $item->categorias->contains('id',Icategoria::ADICIONALES);
            });

            $salsas = $items->filter(function ($item){
                return $item->categorias->contains('id',Icategoria::SALSAS);
            });
        }


        return $this->sendResponse([
            'adicionales' => $adicionales,
            'salsas' => $salsas,
        ],'Agregados');
    }

    public function attachAgregados(Request $request)
    {
//        return $this->sendResponse($request->all(),'request');

        $tipo = $request->tipo;
        $id_agregado = $request->id_agregado;
        $item = Item::find($request->item_id);

        if($tipo=='adicional'){
            if($item->adicionales->contains('id',$id_agregado)){
                return $this->sendError('El adicional ya esta asociado');
            }

            try {
                DB::beginTransaction();

                $result = $item->adicionales()->attach([$id_agregado]);


            } catch (\Exception $exception) {
                DB::rollBack();

                if (Auth::user()->isAdmin()){

                    $msg = $exception->getMessage();
                }else{
                    $msg = 'Hubo un error intente de nuevo';
                }

                return $this->sendError($msg);
            }


            DB::commit();


        }

        if($tipo=='salsa'){
            if($item->salsas->contains('id',$id_agregado)){
                return $this->sendError('La salsa ya esta asociada');
            }

            try {
                DB::beginTransaction();

                $result = $item->salsas()->attach([$id_agregado]);


            } catch (\Exception $exception) {
                DB::rollBack();
                if (Auth::user()->isAdmin()){

                    $msg = $exception->getMessage();
                }else{
                    $msg = 'Hubo un error intente de nuevo';
                }

                return $this->sendError($msg);
            }


            DB::commit();
        }

        return $this->sendResponse($result,'Listo!');
    }

    public function detachAgregados(Request $request)
    {
//        return $this->sendResponse($request->all(),'request');

        $tipo = $request->tipo;
        $id_agregado = $request->id_agregado;
        $item = Item::find($request->item_id);

        if($tipo=='adicional'){

            try {
                DB::beginTransaction();

                $result = $item->adicionales()->detach([$id_agregado]);


            } catch (\Exception $exception) {
                DB::rollBack();
                if (Auth::user()->isAdmin()){

                    $msg = $exception->getMessage();
                }else{
                    $msg = 'Hubo un error intente de nuevo';
                }

                return $this->sendError($msg);
            }


            DB::commit();


        }

        if($tipo=='salsa'){

            try {
                DB::beginTransaction();

                $result = $item->salsas()->detach([$id_agregado]);


            } catch (\Exception $exception) {
                DB::rollBack();
                if (Auth::user()->isAdmin()){

                    $msg = $exception->getMessage();
                }else{
                    $msg = 'Hubo un error intente de nuevo';
                }

                return $this->sendError($msg);
            }


            DB::commit();
        }

        return $this->sendResponse($result,'Listo!');
    }

    public function processUpdate($id,UpdateItemRequest $request)
    {

        $item = $this->itemRepository->findWithoutFail($id);

        if (empty($item)) {
            Flash::error('Item no existe');

            return redirect(route('items.index'));
        }

        $input = $request->all();

//        dd($input);

        $input['materia_prima']=isset($input['materia_prima']) ? $input['materia_prima'] : 0;
        $input['inventariable']=isset($input['inventariable']) ? $input['inventariable'] : 0;
        $input['perecedero']=isset($input['perecedero']) ? $input['perecedero'] : 0;
        $input['web']=isset($input['web']) ? $input['web'] : 0;

        //Categorías multiples para el item
        $categorias = $request->categorias ? $request->categorias : [];
        //Categoría principal (si se selecciona al menos una categoría; la categoría principal es la primera del array)
        $categoria = $categorias ? $categorias[0] : null;

        $input = array_add($input,'icategoria_id',$categoria);

        $item = $this->itemRepository->update($input, $id);

        //Actualiza y guarda imagen
        if ($request->hasFile('imagen')) {
            $this->saveImg($request->file('imagen'),$item);
        }

        //Sincroniza las categorías
        $item->icategorias()->sync($categorias);

        if (!$item->estaEnUnaCompra() && !$item->estaEnUnaVenta()){

            $stock = $item->stocks()->where('tienda_id',session('tienda'))->first();

            if ($stock){
                $stock->cantidad = $request->stock;
                $stock->cnt_ini = $request->stock;
                $stock->save();
            }else{
                $stock = new Stock();
                $stock->ingreso($item->id,$item->stock,null,null,null,session('tienda'));
            }


            $iniStock = $item->inistocks()->delItem($id)->deTienda()->first();

            if($iniStock){
                $iniStock->cantidad = $request->stock;
                $iniStock->save();
            }else{
                $item->inistocks()->create([
                    'tienda_id' => session('tienda'),
                    'cantidad' => $item->stock
                ]);
            }

        }


    }

    public function processStore(CreateItemRequest $request)
    {
        $input = $request->all();

        $input['materia_prima']=isset($input['materia_prima']) ? $input['materia_prima'] : 0;
        $input['inventariable']=isset($input['inventariable']) ? $input['inventariable'] : 0;
        $input['perecedero']=isset($input['perecedero']) ? $input['perecedero'] : 0;
        $input['web']=isset($input['web']) ? $input['web'] : 0;

        //Categorías multiples para el item
        $categorias = $request->categorias ? $request->categorias : [];
        //Categoría principal (si se selecciona al menos una categoría; la categoría principal es la primera del array)
        $categoria = $categorias ? $categorias[0] : null;

        $input = array_add($input,'icategoria_id',$categoria);

        $item = $this->itemRepository->create($input);


        $item->icategorias()->sync($categorias);

        //Actualiza y guarda imagen
        if ($request->hasFile('imagen')) {
            $this->saveImg($request->file('imagen'),$item);
        }

        if ($item->stock > 0){
            $stock = new Stock();
            $stock->ingreso($item->id,$item->stock,null,null,null,session('tienda'));

            $item->inistocks()->create([
                'tienda_id' => session('tienda'),
                'cantidad' => $item->stock
            ]);
        }
    }
}
