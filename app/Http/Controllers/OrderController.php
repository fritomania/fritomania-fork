<?php

namespace App\Http\Controllers;

use App\Facades\Correlativo;
use App\Mail\PedidoWebMail;
use App\Models\Cliente;
use App\Models\Ingrediente;
use App\Models\Item;
use App\Models\Stock;
use App\Models\Tienda;
use App\Models\TipoPago;
use App\Models\Venta;
use App\Models\VentaDetalle;
use App\Models\VentaReceta;
use App\Models\Vestado;
use App\User;
use Auth;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use League\Flysystem\Adapter\Local;


use Freshwork\Transbank\CertificationBagFactory;
use Freshwork\Transbank\TransbankServiceFactory;
use Freshwork\Transbank\RedirectorHelper;
use Mail;

use Khipu;

class OrderController extends AppBaseController
{

    /**
     * OrderController constructor.
     */
    public function __construct()
    {
//        $this->middleware('guest');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        //Se crea la variable de sesión para el carro de compras de no existir
        if ( ! session()->has('cart')) {
            session()->put('cart', new Collection());
            session()->save();
        }

//        if ( ! session()->has('orden')) {
//            flash('Ingrese sus datos para la búsqueda del local mas cercano y generar una nueva orden')->warning()->important();
//            return redirect(route('locales'));
//        }

        $tienda = session()->get('local');
        $orden = session()->get('orden');

        $query = Item::web()->productoFinal();

//        if($local= session()->get('local')){
//            $query->enTienda($local->id);
//        }

        if ($request->category){
            $query->deCategoria($request->category);
        }

        $items = $query->get();

        $articulos = $items->chunk(4);

        $muestraDatosCliente = 1;

        return view('delivery.productos',compact('articulos','tienda','orden','muestraDatosCliente'));
    }

    public function locales()
    {
        return view('delivery.locales');
    }

    public function pedir(Request $request)
    {

        //dd($request->all());



        if($request->OrderMode==1){
            $direccion = $request->calle.', '.$request->numero_domicilio;
            $direccion .= $request->departamento ? ', Depto/Casa '.$request->departamento : '';
            $direccion .= ', '.$request->comuna;

            list($nombres,$apellidos) = separaNombreApellido($request->name);
            $telefono = $request->telefono;
            $correo = $request->correo;
            $observaciones = $request->observaciones;
        }else{
            $direccion = $request->direccion_retiro_local;
            list($nombres,$apellidos) = separaNombreApellido($request->name_rl);
            $telefono = $request->telefono_rl;
            $correo = $request->correo_rl;
            $observaciones = $request->observaciones_rl;
        }


        $success = true;

        try {
            \DB::beginTransaction();



            $cliente = Cliente::where('telefono',$telefono)->first();


            if($cliente){
                //Actualizar los datos del cliente
                $cliente->email= $correo;
                $cliente->direccion= $direccion;
                $cliente->save();

            }else{

                //Crea el cliente
                $cliente = Cliente::create([
                    'nombres' => $nombres,
                    'apellidos' => $apellidos,
                    'telefono' => $telefono,
                    'email' => $correo,
                    'direccion' => $direccion
                ]);
            }


            //Si hay cliente, ya sea que fue creado o ya estaba registrado
            if($cliente){

                //Siguiente correlativo para tabla ventas
                $correlativo = Correlativo::siguiente('ventas');
                //Crea venta temporal (orden)
                $orden = Venta::create([
                    'fecha' => hoyDb(),
                    'correlativo' => $correlativo->max,
                    'vestado_id' => Vestado::TEMPORAL,
                    'cliente_id' => $cliente->id,
                    'tienda_id' => $request->tienda_id,
                    'direccion' => $direccion,
                    'delivery' => $request->OrderMode==2 ? 0 : 1,
                    'monto_delivery' => $this->costoDelivery($request->distancia),
                    'tipo_pago_id' => TipoPago::WEBPAY,
                    'observaciones' => $observaciones,
                    'nombre_entrega' => $nombres.' '.$apellidos,
                    'telefono' => $telefono,
                    'correo' => $correo,
                    'web' => 1,
                    'retiro_en_local' => $request->OrderMode==2 ? 1: 0,
                ]);

                $correlativo->save();
            }


        } catch (\Exception $exception) {
//                dd($exception);
            \DB::rollBack();
            $success = $exception->getMessage();
        }

        if ($success === true) {
            \DB::commit();
            //guarda la nueva orden temporal en sesión
            session()->put('orden', $orden);
        }else{

            flash('Hubo un erro al guardar los datos, por favor intente de nuevo')->warning()->important();
            return redirect(route('locales'));
        }

        if(!$orden){
            flash('Hubo un erro al guardar los datos, por favor intente de nuevo')->warning()->important();
            return redirect(route('locales'));
        }

        //Busca la tienda que se envió en la petición del botón pedir
        $tienda = Tienda::find($request->tienda_id);

        //Olvida la variable de session de local (tienda) para volverla a setear
        session()->forget('local');
        session()->put('local', $tienda);
        session()->save();

        if (session('cart')){
            return redirect(route('carro'));
        }else{

            return redirect(route('productos'));
        }
    }

    public function vistaCarro(Request  $request)
    {
//        session()->forget('modificaciones');
        if ( ! session()->has('orden') && config('app.orden_auto')) {

            session()->put('orden',$this->newOrdenPrueba());
        }

        if ( ! session()->has('orden')) {
            flash('Ingrese sus datos para la búsqueda del local mas cercano y generar una nueva orden')->warning()->important();
            return redirect(route('locales'));
        }



        $carro = session('cart');
        $tienda = session('local');
        $orden = session('orden');

//        dd($carro->toArray());

        $this->actualizaCantidades($request->cantidades);

        $muestraDatosCliente = 1;

        return view('delivery.cart', compact('carro','tienda','orden','muestraDatosCliente'));

    }

    public function pagar(Request $request){


        $this->actualizaCantidades($request->cantidades);

        switch ($request->forma_pago){
            case TipoPago::WEBPAY:
                return $this->pagarWebPay($request);
                break;
            case TipoPago::KHIPU:
                return $this->pagarKhipu($request);
                break;
            case TipoPago::EFECTIVO:
            case TipoPago::TARJETA_DEBITO:
            case TipoPago::TARJETA_CREDITO:
                return $this->pagarEfectivoOTarjeta($request);
                break;
            default:
                return $this->pagarWebPay($request);
                break;
        }

    }

    public function pagarWebPay(Request $request)
    {
//        dd($request->all());

        session()->put('detalles_combo',$request->detalles_combo);

        $orden = session()->get('orden');
        $orden->direccion = $request->direccion;
        $orden->observaciones = $request->observaciones;
        $orden->tipo_pago_id = $request->forma_pago;
        $orden->save();

        session()->put('orden',$orden);

        $this->actualizaCantidades($request->cantidades);

        $bag = CertificationBagFactory::integrationWebpayNormal();

        $plus = TransbankServiceFactory::normal($bag);

        //Para transacciones normales, solo se puede añadir una linea de detalle de transacción.
        $plus->addTransactionDetail($this->totalCarro(), $orden->id); //Amount and BuyOrder

        $response = $plus->initTransaction( route('carro.procesar.webpay'), route('orden.exito',$orden->id) );

        echo RedirectorHelper::redirectHTML($response->url, $response->token);
    }

    public function pagarKhipu(Request $request)
    {
//        dump('pagar con khipu',$request->all());

        session()->put('detalles_combo',$request->detalles_combo);

        $orden = session()->get('orden');
        $orden->direccion = $request->direccion;
        $orden->observaciones = $request->observaciones;
        $orden->tipo_pago_id = $request->forma_pago;
        $orden->save();

        session()->put('orden',$orden);

        $this->actualizaCantidades($request->cantidades);

        $receiverId = 204700;
        $secretKey = '11d5e4e834414da3cd34ff5059332e01dad4a8ff';

        $configuration = new Khipu\Configuration();
        $configuration->setReceiverId($receiverId);
        $configuration->setSecret($secretKey);
// $configuration->setDebug(true);

        $client = new Khipu\ApiClient($configuration);
        $payments = new Khipu\Client\PaymentsApi($client);

        try {
            $opts = array(
                "transaction_id" => $orden->id,
                "return_url" => route('carro.procesar.khipu'),
                "cancel_url" => route('carro'),
                "picture_url" => asset('cliente/images/logo_nuevo.png'),
//                "notify_url" => "http://mi-ecomerce.com/backend/notify",
//                "notify_api_version" => "1.3"
            );

            $response = $payments->paymentsPost(
                "Pedido web ".config('app.name'),
                 "CLP",
                $this->totalCarro(),
                $opts
            );

//            dd($response);

            return redirect()->to($response->getPaymentUrl());

        } catch (\Khipu\ApiException $e) {
            dd($e->getResponseBody(), TRUE);
        }

    }

    public function pagarEfectivoOTarjeta(Request $request)
    {

//        dd($request->all(),session('modificaciones'),$this->adicionalesYSalsas(Item::find(72),1,session('orden'))->toArray());

        try {
            DB::beginTransaction();


            $orden = $this->procesarOrden($request);

        } catch (\Exception $exception) {
            DB::rollBack();


            if (config('app.debug')){
                dd($exception);
            }

            flash('Hubo un error, intente de nuevo por favor')->error()->important();

            return redirect()->back();
        }


        DB::commit();


        return redirect(route('orden.exito',$orden));

    }

    public function procesarWebPay(Request $request)
    {

        $bag = CertificationBagFactory::integrationWebpayNormal();

        $plus = TransbankServiceFactory::normal($bag);

        $response = $plus->getTransactionResult();


        //$msj = msjResponseCode($response->detailOutput->responseCode);
        $msj = "No se pudo completar tu pedido, intenta nuevamente o si no, con otra tarjeta";

        //si la Transacción NO es aprobada
        if($response->detailOutput->responseCode!=0) {
            flash($msj.'.!')->warning()->important();
            return redirect(route('carro'));
        }

        $plus->acknowledgeTransaction();//Realiza el cobro al tarjetahabiente.


        try {
            DB::beginTransaction();


            $orden = $this->procesarOrden($request,$response->detailOutput->authorizationCode);

        } catch (\Exception $exception) {
            DB::rollBack();

            if (config('app.debug')){
                dd($exception->getMessage());
            }

            flash('Hubo un error, intente de nuevo por favor')->error()->important();

            return redirect()->back();
        }


        DB::commit();


        //Redirect back to Webpay Flow and then to the thanks page
        return RedirectorHelper::redirectBackNormal($response->urlRedirection);

    }

    public function procesarKhipu(Request $request)
    {

        try {
            DB::beginTransaction();


            $orden = $this->procesarOrden($request);

        } catch (\Exception $exception) {
            DB::rollBack();

            if (config('app.debug')){
                dd($exception->getMessage());
            }

            flash('Hubo un error, intente de nuevo por favor')->error()->important();

            return redirect()->back();
        }


        DB::commit();

        return redirect(route('orden.exito',$orden->id));
    }

    public function totalCarro()
    {
        $orden = session()->get('orden');
        $carro = session('cart');
        $total=0;
        foreach($carro as $det){
            $total += ($det->qty*$det->precio_venta);
        }

        if($orden->delivery){
            $total+= $orden->monto_delivery;
        }

        return $total;
    }

    public function actualizaCantidades($cantidades=null)
    {
        $carro = session('cart');

        if ($cantidades){
            $carro = $carro->map(function ($product) use ($cantidades) {
                $product->qty = $cantidades[$product->id];
                return $product;
            });

            session()->put('cart', $carro);
            session()->save();
        }

    }

    public function exito(Venta $orden){

        if ($orden->estado->id == Vestado::TEMPORAL){
            return redirect(route('carro'));
        }

        session()->forget('orden');
        session()->forget('local');
        session()->forget('cart');
        session()->forget('modificaciones');

//        flash('Su pedido N° '.$orden->id.' ha finalizado con éxito')->success()->important();

        return view('delivery.exito',compact('orden'));

    }

    public function seguirComprando(Venta $anterior)
    {

        if(!$anterior){
            flash('Hubo un error, por favor ingrese los datos nuevamente')->warning();
            return redirect(route('locales'));
        }

        $success = true;

        try {


            //Siguiente correlativo para tabla ventas
            $correlativo = Correlativo::siguiente('ventas');
            //Crea venta temporal (orden)
            $orden = Venta::create([
                'fecha' => hoyDb(),
                'correlativo' => $correlativo->max,
                'vestado_id' => Vestado::TEMPORAL,
                'cliente_id' => $anterior->cliente->id,
                'tienda_id' => $anterior->tienda_id,
                'direccion' => $anterior->direccion,
                'delivery' => $anterior->delivery,
                'monto_delivery' => $anterior->monto_delivery,
                'tipo_pago_id' => $anterior->tipo_pago_id,
                'observaciones' => $anterior->observaciones,
                'nombre_entrega' => $anterior->nombre_entrega,
                'telefono' => $anterior->telefono,
                'correo' => $anterior->correo,
                'web' => 1,
            ]);

            $correlativo->save();


        } catch (\Exception $exception) {
            \DB::rollBack();
            $success = $exception->getMessage();
        }

        if ($success){
            session()->put('orden',$orden);
            session()->put('local',Tienda::find($orden->tienda_id));
            return redirect(route('productos'));
        }

        flash('Hubo un error, por favor ingrese los datos nuevamente')->warning();
        return redirect(route('locales'));

    }

    public function actualizaDetalles($cantidades=null)
    {
        $carro = session('cart');

        if ($cantidades){
            $carro = $carro->map(function ($product) use ($cantidades) {
                $product->qty = $cantidades[$product->id];
                return $product;
            });

            session()->put('cart', $carro);
            session()->save();
        }

    }

    public function cambiaTipoPedido($tipo)
    {

        $orden = session()->get('orden');

        if ($tipo==1){
            $orden->retiro_en_local = 0;
            $orden->delivery = 1;
            $orden->save();
        }

        if ($tipo==2){
            $orden->delivery = 0;
            $orden->retiro_en_local = 1;
            $orden->save();
        }

        flash('Listo se cambio con éxito el tipo de pedido')->error();

        return redirect()->back();

    }

    public function costoDelivery($km)
    {

        return config('app.costo_delivery_base',1500) + ($km*config('app.costo_delivery_km',1000));
    }

    public function detalleLocal(Tienda $local){

        if(!$local){
            flash('Error! local no encontrado')->error();
            return redirect()->back();
        }

        return view('delivery.verdetalles',compact('local'));
    }

    public function procesarOrden(Request $request,$codWebPay=null)
    {
        $orden = session()->get('orden');
        $carro = session('cart');
        $detallesCombo = $request->detalles_combo;

        $orden->recibido =$this->totalCarro();
        $orden->vestado_id = Vestado::PAGADA;
        $orden->tipo_pago_id = $request->forma_pago;
        $orden->cod_web_pay = $codWebPay;


        if($orden){
            $detalles = collect();
            foreach ($carro as $det){


                //Articulo no modificable, no tiene receta y no es combo
                if(!$det->tieneReceta() && !$det->esCombo()){

                    //Agregar el detalle, fila o combo como tal
                    $detalles = $this->detCartToDetVenta($det,$detalles);
                }

                //Articulo con receta o con salsas o con adicionales que no es combo
                if (($det->tieneReceta() || $det->tieneSalsas() || $det->tieneExtras()) && !$det->esCombo() ){

                    for ($numItem=1;$numItem<=$det->qty;$numItem++){

                        $detalle = new VentaDetalle([
                            'item_id' => $det->id,
                            'cantidad' => 1,
                            'precio' => $det->precio_venta,
                            'ingredientes' => $this->getIngredientes($det,$numItem),
                            'num_item' => $numItem,
                        ]);

                        $detalles->push($detalle);

                        $adicionalesYsalsas = $this->adicionalesYSalsas($det->id,$numItem);

                        if ($adicionalesYsalsas->count() > 0){

                            $detalles = $detalles->merge($adicionalesYsalsas);
                        }


                    }
                }


                //Si es combo unitario
                if ($det->esComboUnitario()){

                    //Agregar el detalle, fila o combo como tal
                    $detalles = $this->detCartToDetVenta($det,$detalles);

                    $cantidad = $det->combo->piezas * $det->qty;
                    $articuloCombo = $det->combo->detalles->first()->item;

                    //si el unico articulo del combo tiene receta
                    if ($articuloCombo->tieneReceta() || $articuloCombo->tieneExtras() || $articuloCombo->tieneSalsas()){


                        for ($numItem=1;$numItem<=$cantidad;$numItem++){

                            $detalle = new VentaDetalle([
                                'item_id' => $articuloCombo->id,
                                'cantidad' => 1,
                                'precio' => 0,
                                'ingredientes' => $this->getIngredientes($det,$numItem,$articuloCombo),
                                'num_item' => $numItem,
                                'es_detalle_combo' => 1,
                            ]);

                            $detalles->push($detalle);

                            $adicionalesYsalsas = $this->adicionalesYSalsas($det->id,$numItem);

                            if ($adicionalesYsalsas->count() > 0){

                                $detalles = $detalles->merge($adicionalesYsalsas);
                            }

                        }
                    }else{
                        $detalle = new VentaDetalle([
                            'item_id' => $articuloCombo->id,
                            'cantidad' => $cantidad,
                            'precio' => 0,
                        ]);

                        $detalles->push($detalle);
                    }


                    if($det->tieneSalsas() || $det->tieneExtras()){
                        $adicionalesYsalsas = $this->adicionalesYSalsas($det->id,1);

                        if ($adicionalesYsalsas->count() > 0){

                            $detalles = $detalles->merge($adicionalesYsalsas);
                        }
                    }


                }


                //articulo combo no unitario de 2 o mas articulaos sin posibilidad de editar
                if($det->esCombo() && !$det->esComboUnitario()){

                    //Agregar el detalle, fila o combo como tal
                    $detalles = $this->detCartToDetVenta($det,$detalles);

                    //y existen detalles del combo
                    if(isset($detallesCombo[$det->id])){

                        foreach ($detallesCombo[$det->id] as $itemId => $cantidad){

                            $articuloCombo = Item::find($itemId);

                            if ($articuloCombo->tieneReceta()){


                                for ($numItem=1;$numItem<=$cantidad;$numItem++){

                                    $detalle = new VentaDetalle([
                                        'item_id' => $articuloCombo->id,
                                        'cantidad' => 1,
                                        'precio' => 0,
                                        'ingredientes' => $this->getIngredientes($det,$numItem,$articuloCombo),
                                        'num_item' => $numItem,
                                        'es_detalle_combo' => 1,
                                    ]);

                                    $detalles->push($detalle);

                                    $adicionalesYsalsas = $this->adicionalesYSalsas($det->id,$numItem);

                                    if ($adicionalesYsalsas->count() > 0){

                                        $detalles = $detalles->merge($adicionalesYsalsas);
                                    }

                                }
                            }else{
                                $detalle = new VentaDetalle([
                                    'item_id' => $itemId,
                                    'cantidad' => $cantidad,
                                    'precio' => 0,
                                    'es_detalle_combo' => 1,
                                ]);

                                $detalles->push($detalle);
                            }
                        }

                    }

                }




            }

            //Se guardan los detalles de la venta
            $orden->ventaDetalles()->saveMany($detalles);


            $orden->save();
            $msg ="Su pedido N° ".$orden->id." ha sido recibido con éxito";
            Mail::send(new PedidoWebMail($orden,$msg));


        }

        return $orden;
    }

    public function newOrdenPrueba()
    {
        $correlativo = Correlativo::siguiente('ventas');
        //Crea venta temporal (orden)
        $orden = Venta::create([
            'fecha' => hoyDb(),
            'correlativo' => $correlativo->max,
            'vestado_id' => Vestado::TEMPORAL,
            'cliente_id' => 1,
            'tienda_id' => Tienda::LOCAL_1,
            'direccion' => 'pruebas',
            'delivery' => 1,
            'monto_delivery' => 500,
            'tipo_pago_id' => TipoPago::WEBPAY,
            'observaciones' => 'sin hielo',
            'nombre_entrega' => 'Esdras Altamirano',
            'telefono' => '5555555555',
            'correo' => 'altamiranoesdras@gmail.com',
            'web' => 1,
            'retiro_en_local' => 0,
        ]);

        $correlativo->save();

        return $orden;
    }

    /**
     * Guarda en session las modificaciones de los ingredientes, adicionales y salsas para cada item
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function storeModifyItem(Request $request)
    {

        $itemId = $request->item_id;
        $detId = $request->det_id;

        $detEdit = Item::find($detId);
        $itemEdit = Item::find($itemId);

        if($itemEdit->tieneReceta()){

            $recetaOriginal = VentaReceta::where('item_id',$itemId)->with('detalles')->first()->detalles;
            $recetaOriginalArray = $recetaOriginal->pluck('ingrediente_id','id')->toArray();
        }

        //dd($recetaOriginal->toArray(),$recetaOriginalArray,$request->modificaciones );

        $modificaciones = [];
        $modificaciones['total'] = 0;


        if($request->modificaciones){

            //ciclo por cada item
            foreach ($request->modificaciones as $itemNum => $recetaModificada) {

                //si la re
                if ($recetaOriginalArray != $recetaModificada){

                    foreach ($recetaOriginal as $det){

                        $id = $det->id;

                        $ingredienteElegido = isset($recetaModificada[$id]) ? Ingrediente::find($recetaModificada[$id]) : null;

                        //si hay ingrediente
                        if ($ingredienteElegido){

                            //y el ingrediente elegido es diferente del de la receta original
                            if ($ingredienteElegido->id != $det->ingrediente->id){

                                $modificaciones[$itemNum]['ingredientes'][] = $ingredienteElegido->nombre;
                            }else{
                                $modificaciones[$itemNum]['ingredientes'][] = $det->ingrediente->nombre;
                            }

                        }else{

                            if ($det->es_opcional){

                                $modificaciones[$itemNum]['ingredientes'][] = 'Sin '.$det->categoria;

                            }else{

                                $modificaciones[$itemNum]['ingredientes'][] = 'Sin '.$det->ingrediente->nombre;
                            }

                        }

                    }
                }else{

                }
            }
        }

        if($adicionales = $request->adicionales){

            foreach ($adicionales as $itemNum => $adicionalesItem) {

                $modificaciones[$itemNum]['adicionales'] = $adicionalesItem;
                $modificaciones['total'] += $this->getTotalItems($adicionalesItem);
            }

        }

        if($salsas = $request->salsas){

            foreach ($salsas as $itemNum => $salsasItem) {
                $modificaciones[$itemNum]['salsas'] = $salsasItem;
                $modificaciones['total'] += $this->getTotalItems($salsasItem);
            }

        }


        $session = session('modificaciones');

        if ($session){

            if (isset($session[$detId])){
                $session[$detId] = $modificaciones;
            }else{
                $session = array_add($session,$detId,$modificaciones);
            }

            session()->put('modificaciones',$session);
        }else{

            $session = session()->put('modificaciones',[$detId => $modificaciones]);
        }

        session()->save();

        $session = session('modificaciones');

        return $this->sendResponse($session[$detId],'Modificaciones');
    }

    public function getIngredientes($detalle,$numItem,$articuloCombo=null)
    {

        $modificaciones = session('modificaciones');

        if (isset($modificaciones[$detalle->id][$numItem]['ingredientes'])){
            $ingredientes = implode(', ',$modificaciones[$detalle->id][$numItem]['ingredientes']);
        }else{

            if ($detalle->combo){

                $ingredientes = implode(', ',$articuloCombo->nombresIngredientes());

            }else{

                $ingredientes = implode(', ',$detalle->nombresIngredientes());

            }

        }

        return $ingredientes;

    }

    public function adicionalesYSalsas($detId,$numItem)
    {

        $modificaciones = session('modificaciones');

//        dd($detId,$numItem,$modificaciones);
        $adicionalesYsalsas = collect();

        $adicionales = $modificaciones[$detId][$numItem]['adicionales'] ?? false;


        if ($adicionales){

            foreach ($adicionales as $index => $id) {
                $adicional = Item::find($id);

                $detalle = new VentaDetalle([
                    'item_id' => $adicional->id,
                    'cantidad' => 1,
                    'precio' => $adicional->precio_venta,
                ]);

                $adicionalesYsalsas->push($detalle);
            }

        }

        $salsas = $modificaciones[$detId][$numItem]['salsas'] ?? false;

        if ($salsas){

            foreach ($salsas as $index => $id) {
                $salsa = Item::find($id);

                $detalle = new VentaDetalle([
                    'item_id' => $salsa->id,
                    'cantidad' => 1,
                    'precio' => $salsa->precio_venta,
                ]);

                $adicionalesYsalsas->push($detalle);
            }

        }


        return $adicionalesYsalsas;

    }

    public function getModificaciones()
    {
        return $this->sendResponse(session('modificaciones'),'Modificaciones');
    }

    public function getTotalItems($arrayIds)
    {
        $items = Item::whereIn('id',$arrayIds)->get();

        return $items->sum('precio_venta');
    }

    public function detCartToDetVenta($detCart,$detalles)
    {
        $detalle = new VentaDetalle([
            'item_id' => $detCart->id,
            'cantidad' => $detCart->qty,
            'precio' => $detCart->precio_venta,
        ]);

        $detalles->push($detalle);

        return $detalles;
    }
}
