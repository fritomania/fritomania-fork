<?php

namespace App\Http\Controllers;

use App\DataTables\MagnitudeDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateMagnitudeRequest;
use App\Http\Requests\UpdateMagnitudeRequest;
use App\Repositories\MagnitudeRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class MagnitudeController extends AppBaseController
{
    /** @var  MagnitudeRepository */
    private $magnitudeRepository;

    public function __construct(MagnitudeRepository $magnitudeRepo)
    {
        $this->magnitudeRepository = $magnitudeRepo;
        $this->middleware('auth');
    }

    /**
     * Display a listing of the Magnitude.
     *
     * @param MagnitudeDataTable $magnitudeDataTable
     * @return Response
     */
    public function index(MagnitudeDataTable $magnitudeDataTable)
    {
        return $magnitudeDataTable->render('magnitudes.index');
    }

    /**
     * Show the form for creating a new Magnitude.
     *
     * @return Response
     */
    public function create()
    {
        return view('magnitudes.create');
    }

    /**
     * Store a newly created Magnitude in storage.
     *
     * @param CreateMagnitudeRequest $request
     *
     * @return Response
     */
    public function store(CreateMagnitudeRequest $request)
    {
        $input = $request->all();

        $magnitude = $this->magnitudeRepository->create($input);

        Flash::success('Magnitud guardada exitosamente.');

        return redirect(route('magnitudes.index'));
    }

    /**
     * Display the specified Magnitude.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $magnitude = $this->magnitudeRepository->findWithoutFail($id);

        if (empty($magnitude)) {
            Flash::error('Magnitud no encontrado');

            return redirect(route('magnitudes.index'));
        }

        return view('magnitudes.show',compact('magnitude'));
    }

    /**
     * Show the form for editing the specified Magnitude.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $magnitude = $this->magnitudeRepository->findWithoutFail($id);

        if (empty($magnitude)) {
            Flash::error('Magnitud no encontrado');

            return redirect(route('magnitudes.index'));
        }

        return view('magnitudes.edit',compact('magnitude'));
    }

    /**
     * Update the specified Magnitude in storage.
     *
     * @param  int              $id
     * @param UpdateMagnitudeRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateMagnitudeRequest $request)
    {
        $magnitude = $this->magnitudeRepository->findWithoutFail($id);

        if (empty($magnitude)) {
            Flash::error('Magnitud no encontrado');

            return redirect(route('magnitudes.index'));
        }

        $magnitude = $this->magnitudeRepository->update($request->all(), $id);

        Flash::success('Magnitud actualizado exitosamente.');

        return redirect(route('magnitudes.index'));
    }

    /**
     * Remove the specified Magnitude from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $magnitude = $this->magnitudeRepository->findWithoutFail($id);

        if (empty($magnitude)) {
            Flash::error('Magnitud no encontrado');

            return redirect(route('magnitudes.index'));
        }

        $this->magnitudeRepository->delete($id);

        Flash::success('Magnitud eliminado exitosamente.');

        return redirect(route('magnitudes.index'));
    }
}
