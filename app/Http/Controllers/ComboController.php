<?php

namespace App\Http\Controllers;

use App\DataTables\ComboDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateComboRequest;
use App\Http\Requests\UpdateComboRequest;
use App\Models\Combo;
use App\Models\ComboDetalle;
use App\Models\Item;
use App\Repositories\ComboRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Response;

class ComboController extends AppBaseController
{
    /** @var  ComboRepository */
    private $comboRepository;

    public function __construct(ComboRepository $comboRepo)
    {
        $this->comboRepository = $comboRepo;
        $this->middleware('auth');
    }

    /**
     * Display a listing of the Combo.
     *
     * @param ComboDataTable $comboDataTable
     * @return Response
     */
    public function index(ComboDataTable $comboDataTable)
    {
        return $comboDataTable->render('combos.index');
    }

    /**
     * Show the form for creating a new Combo.
     *
     * @return Response
     */
    public function create()
    {
        if ( ! session()->has('combo')) {
            session()->put('combo', new Collection());
            session()->save();
        }

        return view('combos.create');
    }

    /**
     * Store a newly created Combo in storage.
     *
     * @param CreateComboRequest $request
     *
     * @return Response
     */
    public function store(Request $request)
    {

        $comboSession = session('combo');

        if(!$comboSession){
            flash('No hay combo en sessión')->error()->important();

            return redirect(route('combos.create'));
        }

        $item = $comboSession->get('item');

        $existe = Combo::where('item_id',$request->item_id)->first();

        if($existe){
            flash('Ya existe un definición para este combo')->error()->important();

            return redirect(route('combos.create'));
        }

        $detallesSesion = $comboSession->get('detalles');


        $datos = $this->validate($request,[
            'item_id' => 'required',
            'piezas' => 'required',
            'bebidas' => 'required',
        ]);

        $combo = $this->comboRepository->create($datos);

        $detalles = $detallesSesion->map(function ($item) use ($combo){
            return new ComboDetalle([
                'item_id' => $item->id,
                'combo_id' => $combo->id,
                'cantidad' => $item->cantidad,
                'tipo' => $item->esBebida() ? ComboDetalle::BEBIDA : ComboDetalle::PIEZA,
            ]);
        });

        $combo->comboDetalles()->saveMany($detalles);

        Flash::success('Definición de combo guardada exitosamente.');

        session()->forget('combo');

        return redirect(route('combos.index'));
    }

    /**
     * Display the specified Combo.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $combo = $this->comboRepository->findWithoutFail($id);

        if (empty($combo)) {
            Flash::error('Combo no encontrado');

            return redirect(route('combos.index'));
        }

        return view('combos.show',compact('combo'));
    }

    /**
     * Show the form for editing the specified Combo.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $combo = $this->comboRepository->findWithoutFail($id);

        if (empty($combo)) {
            Flash::error('Combo no encontrado');

            return redirect(route('combos.index'));
        }

        $detalles = $combo->detalles->map(function ($det){
            $item = $det->item;
            $item->setAttribute('cantidad', $det->cantidad);
            return $item;
        });

        session()->put('combo',collect());

        session('combo')->put('item',$combo->item);
        session('combo')->put('piezas',$combo->piezas);
        session('combo')->put('bebidas',$combo->bebidas);
        session('combo')->put('detalles',$detalles);

        session()->save();

        return view('combos.create',compact('combo'));
    }

    /**
     * Update the specified Combo in storage.
     *
     * @param  int              $id
     * @param UpdateComboRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateComboRequest $request)
    {
        $combo = $this->comboRepository->findWithoutFail($id);

        if (empty($combo)) {
            Flash::error('Combo no encontrado');

            return redirect(route('combos.index'));
        }

        $datos = $this->validate($request,[
            'item_id' => 'required',
            'piezas' => 'required',
            'bebidas' => 'required',
        ]);

        $combo = $this->comboRepository->update($datos, $id);

        $detallesSesion = session()->get('combo')->get('detalles');

        $detalles = $detallesSesion->map(function ($item) use ($combo){
            return new ComboDetalle([
                'item_id' => $item->id,
                'combo_id' => $combo->id,
                'cantidad' => $item->cantidad,
                'tipo' => $item->esBebida() ? ComboDetalle::BEBIDA : ComboDetalle::PIEZA,
            ]);
        });


        $combo->detalles()->delete();
        $combo->detalles()->saveMany($detalles);

        Flash::success('Definición de combo actualizado exitosamente.');

        return redirect(route('combos.index'));
    }

    /**
     * Remove the specified Combo from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $combo = $this->comboRepository->findWithoutFail($id);

        if (empty($combo)) {
            Flash::error('Combo no encontrado');

            return redirect(route('combos.index'));
        }

        $this->comboRepository->delete($id);

        Flash::success('Combo eliminado exitosamente.');

        return redirect(route('combos.index'));
    }

    public function add(Request $request)
    {

        $combo = session('combo');
        $itemId = $request->item_select;
        $ingrediente = $request->ingrediente;
        $cantidad = $request->cantidad;

        if(! $combo->has('item')){
            $product = Item::find($itemId);

            $combo->put('item',$product->toArray());
        }

        if(! $combo->has('detalles')){

            $combo->put('detalles',collect());

        }

        $detalles = $combo->get('detalles');

        if ( ! $detalles->contains('id', $ingrediente)) {
            $product = Item::find($ingrediente);

            $product->setAttribute('cantidad', $cantidad);
            $detalles->push($product);
        } else {
            return $this->sendError('Ingrediente ya agregado!');
        }

        session()->save();

        return $this->sendResponse($detalles,'Ingrediente agregado!');
    }

    public function remove (Request $request) {

        $newCol = collect();
        foreach (session()->get('combo')->get('detalles') as $det){
            if(!($det->id==$request->item_id)){
                $newCol->push($det);
            }
        }

        session()->get('combo')->put('detalles',$newCol);

        return $this->sendResponse($newCol,"Articulo removido!");

    }

    public function getCombo(){
        $combo = session()->get('combo');

        if(!$combo){
            return $this->sendError('No existen combo en sesion');
        }
        return $this->sendResponse($combo,'Combo');
    }

    public function cancelar()
    {

        session()->forget('combo');
        flash('Definición de combo cancelada!!')->important()->success();
        return redirect(route('combos.index'));
    }
}
