<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class WebhookController extends AppBaseController
{


    public function index(Request $request)
    {

//        dump($request->all());
        //return $this->sendResponse($request->all(),'Request');

        if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {

            echo 'Ejecutar webhook en SO Windows';
            $out = shell_exec("git pull origin master 2>&1");
        } else {

            echo 'Ejecutar webhook en SO Linux';
            $out = shell_exec(" echo A!b2C#d4 | /usr/bin/git pull origin master 2>&1");
        }



        echo'<pre>'. $out. '</pre>';
    }

    public function shellView(Request $request)
    {

        $comando = $request->comando;
        $out = '';

        if($comando){

            if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {

                $out = shell_exec($comando." 2>&1");
            } else {

                $out = shell_exec($comando." 2>&1");
            }
        }

        return view('shell',compact('out','comando'));
    }
}
