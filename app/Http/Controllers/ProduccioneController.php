<?php

namespace App\Http\Controllers;

use App\DataTables\ProduccioneDataTable;
use App\DataTables\ProduccionEnCocinaDataTable;
use App\DataTables\Scopes\ScopeProduccionDatatable;
use Carbon\Carbon;
use DB;
use Facades\App\Facades\Correlativo;
use App\Http\Requests;
use App\Http\Requests\CreateProduccioneRequest;
use App\Http\Requests\UpdateProduccioneRequest;
use App\Mail\AtenderProduccion;
use App\Mail\ProduccionSolicitud;
use App\Models\Item;
use App\Models\Produccione;
use App\Models\ProduccionEstado;
use App\Models\ProduccionTipo;
use App\Models\ProductoConsumido;
use App\Models\ProductoRealizado;
use App\Models\ProductoSolicitado;
use App\Models\Receta;
use App\Models\RecetaDetalle;
use App\Models\Stock;
use App\Models\Tienda;
use App\Models\TrasladoMateriaPrima;
use App\Repositories\ProduccioneRepository;
use Auth;
use Flash;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Mail;
use Response;

class ProduccioneController extends AppBaseController
{
    /** @var  ProduccioneRepository */
    private $produccioneRepository;
    private $msjErrorValidaConsumidos;

    public function __construct(ProduccioneRepository $produccioneRepo)
    {
        $this->produccioneRepository = $produccioneRepo;
        $this->middleware('auth');
        $this->msjErrorValidaConsumidos='';
    }

    /**
     * Display a listing of the Produccione.
     *
     * @param ProduccioneDataTable $produccioneDataTable
     * @return Response
     */
    public function index(ProduccioneDataTable $produccioneDataTable)
    {
        return $produccioneDataTable->render('producciones.index');
    }

    public function cocinaList(ProduccionEnCocinaDataTable $produccioneDataTable)
    {
        $produccioneDataTable->addScope(new ScopeProduccionDatatable([ProduccionEstado::SOLICITADA,ProduccionEstado::INCOMPLETA]));

        return $produccioneDataTable->render('producciones.cocina_produccion');
    }

    public function cocinaAtender($id)
    {
        $produccione = $this->produccioneRepository->findWithoutFail($id);

        if (empty($produccione)) {
            Flash::error('Produccione no encontrado');

            return redirect(route('producciones.index'));
        }

        return view('producciones.atender')->with('produccione', $produccione);
    }

    /**
     * Show the form for creating a new Produccione.
     *
     * @return Response
     */
    public function create()
    {
                session()->forget('producidos');
        if ( ! session()->has('solicitados')) {
            session()->put('solicitados', new Collection());
            session()->save();
        }

        return view('producciones.create');
    }

    /**
     * Store a newly created Produccione in storage.
     *
     * @param CreateProduccioneRequest $request
     *
     * @return Response
     */
    public function store(CreateProduccioneRequest $request)
    {

        try {
            DB::beginTransaction();

            $this->procesarSolicitud($request);

        } catch (\Exception $exception) {
            DB::rollBack();

            if (Auth::user()->isSuperAdmin()){
                dd($exception->getMessage(),$exception->getLine());
            }

            $msg = Auth::user()->isAdmin() ? $exception->getMessage() : 'Hubo un error intente de nuevo';

            flash('Error: '.$msg)->error()->important();
            return redirect()->back();
        }


        DB::commit();

        session()->forget('solicitados');

        flash('Solicitud de producción guardada con exito!')->success()->important();

        return redirect(route('producciones.index'));

    }

    public function storeCocina(Produccione $produccion,Request $request)
    {
        //dd($request->all());
        if (!$produccion) {
            Flash::error('Producción no encontrada');

            return redirect(route('produccion.cocina.listado'));
        }

        $recetas = $request->recetas;

        $realizados = $this->validaRealizados($produccion,$request->realizado);

        if(!$realizados){
            flash('No ha ingresado ninguna cantidad en Producto Terminado!!')->warning()->important();
            return redirect(route('produccion.cocina.atender',$produccion->id));
        }

        $consumidos = $this->validaConsumidos($produccion,$request->consumidos,$realizados);

        if (!$consumidos){
            flash($this->msjErrorValidaConsumidos)->warning()->important();
            return redirect(route('produccion.cocina.atender',$produccion->id));
        }

        $produccion->productoRealizados()->saveMany($realizados);
        $produccion->productoConsumidos()->saveMany($consumidos);


        $stock = new Stock();
        foreach ($produccion->productoRealizados as $detalle){
            $stock->ingresoProduccion($detalle->item_id,$detalle->cantidad,$detalle->id);
        }

        foreach ($produccion->productoConsumidos as $detalle){
            $stock->egresoProduccion($detalle->item_id,$detalle->cantidad_ingresada,$detalle->id);
        }

        $nuevoEstado = $this->cambioEstado($produccion);

        $msg = $nuevoEstado == ProduccionEstado::COMPLETADA ? 'Producción completada!!': 'Datos guardados!!';

        flash($msg)->success()->important();

        if($produccion){
            Mail::send(new AtenderProduccion($produccion));
        }

        return redirect(route('produccion.cocina.atender',$produccion->id));
    }

    /**
     * Display the specified Produccione.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $produccione = $this->produccioneRepository->findWithoutFail($id);

        if (empty($produccione)) {
            Flash::error('Produccione no encontrado');

            return redirect(route('producciones.index'));
        }

        return view('producciones.show')->with('produccione', $produccione);
    }

    /**
     * Show the form for editing the specified Produccione.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $produccione = $this->produccioneRepository->findWithoutFail($id);

        if (empty($produccione)) {
            Flash::error('Produccione no encontrado');

            return redirect(route('producciones.index'));
        }

        return view('producciones.edit',compact('produccione'));
    }

    /**
     * Update the specified Produccione in storage.
     *
     * @param  int              $id
     * @param UpdateProduccioneRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateProduccioneRequest $request)
    {
        $produccione = $this->produccioneRepository->findWithoutFail($id);

        if (empty($produccione)) {
            Flash::error('Produccione no encontrado');

            return redirect(route('producciones.index'));
        }

        $produccione = $this->produccioneRepository->update($request->all(), $id);

        Flash::success('Produccione actualizado exitosamente.');

        return redirect(route('producciones.index'));
    }

    /**
     * Remove the specified Produccione from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $produccione = $this->produccioneRepository->findWithoutFail($id);

        if (empty($produccione)) {
            Flash::error('Produccione no encontrado');

            return redirect(route('producciones.index'));
        }

        $this->produccioneRepository->delete($id);

        Flash::success('Produccione eliminado exitosamente.');

        return redirect(route('producciones.index'));
    }

    public function add(Request $request)
    {

        $solicitados = session('solicitados');
        $itemId = $request->item_select;
        $cantidad = $request->cantidad;

        if (!$itemId){
            return $this->sendError('Seleccione un articulo!');
        }

        if (!$cantidad){
            return $this->sendError('Ingrese una cantidad!');
        }

        if ( ! $solicitados->contains('id', $itemId)) {
            $product = Item::find($itemId);

            $product->setAttribute('qty', $cantidad);
            $solicitados->push($product);
        } else {

            return $this->sendError('Producto ya agregado');
        }

        session()->save();

        return $this->sendResponse($solicitados,"Articulo agrgado!");
    }

    public function remove () {

        $solicitados = session('solicitados');
        $itemId = request('item_id');

        foreach ($solicitados as $key => $item) {
            if ($item->id == $itemId) {
                $solicitados->forget($key);
            }
        }

        return $this->sendResponse($solicitados,"Articulo removido!");

    }

    public function getSolicitadosSesion(){
        $solicitados = session()->get('solicitados');

        if(!$solicitados){
            return $this->sendError('No existe ningún artículo solicitado');
        }

        return $this->sendResponse($solicitados,'Solicitados');
    }

    public function getSolicitados(Produccione $produccion){

        $solicitados = $produccion->productoSolicitados;

        if(!$solicitados){
            return $this->sendError('No existe ningún artículo solicitado');
        }

        return $this->sendResponse($solicitados,'Solicitados');
    }

    public function getMateriaSesion(){
        $solicitados = session()->get('solicitados');

        if(!$solicitados){
            $this->sendError('No hay artículos producidos en sesión');
        }

        $materias = $this->materias($solicitados);
        return $this->sendResponse($materias,'Materia prima sesión');
    }

    public function getMateria(Produccione $produccion){
        $solicitados = $produccion->productoSolicitados;

        if(!$solicitados){
            $this->sendError('No hay artículos solicitados');
        }

        $items= collect();

        foreach ($solicitados as $index => $solicitado) {
            if ($solicitado->estado!=ProductoSolicitado::COMPLETADO){
                $item = $solicitado->item;
                $item->setAttribute('qty', $solicitado->pendientes);
                $items->push($item);
            }
        }

        $materias = $this->materias($items);

        $materias = $materias->map(function ($m){
            $stock = $m->item->stockTienda(Tienda::COCINA_PRODUCCION);
            $m->item->setAttribute('stock_tienda', $stock);

            return $m;
        });

        return $this->sendResponse($materias,'Materia pirma');
    }

    /**
     * Devuelve los artículos que se debieran consumir según la receta de los artículos que se van a producir
     * @param Collection $solicitados
     * @return Collection|static
     */
    public function materias(Collection $producidos)
    {
        $consumidos = collect();

        //Ciclo de los articulos que se pretenden producir
        foreach ($producidos as $index => $item) {

            //Receta del articulo
            $receta = Receta::delItem($item->id)->first();

            //Ciclo de los artículos de la receta
            foreach ($receta->recetaDetalles as $det) {

                //Si el artículo ya existe en la colección de los consumidos,
                // esto pasa cuando un articulo de materia prima se utiliza para producir uno o mas productos finales
                if ($consumidos->contains('item_id',$det->item_id)){

                    $itemId= $det->item_id;
                    $cantidad = ($det->cantidad*$item->qty);

                    //Se hace un map de los consumidos para incrementar la cantidad del articulo que ya existe
                    $consumidos = $consumidos->map(function ($subDet) use($itemId,$cantidad) {
                        if ($subDet->item_id == $itemId) {
                            $subDet->cantidad += $cantidad;
                        }
                        return $subDet;
                    });

                }
                //De lo contrario se agrega el articulo a la colección
                else{
                    $det->cantidad = ($det->cantidad*$item->qty);
                    $det->stock = $det->item->stockTienda();
                    $consumidos->push($det);
                }

            }
        }

        return $consumidos;
    }

    /**
     * Cambia el estado de los artículos solicitados según los artículos realizados de una producción
     * @param Produccione $produccion
     */
    public function cambioEstado(Produccione $produccion)
    {

        $solicitados = $produccion->productoSolicitados;

        //Cambia de estado a completados los detalles de la produccion solicitados
        foreach ($solicitados as $index => $solicitado) {

            //Si hay algo realizado
            if($solicitado->pendientes!=$solicitado->cantidad){

                $nuevoEstado = ($solicitado->pendientes<=0) ? ProductoSolicitado::COMPLETADO : ProductoSolicitado::INCOMPLETO;

                $solicitado->estado=$nuevoEstado;
                $solicitado->save();
            }


        }

        $cntSol= $solicitados->count();
        $incompletos = $solicitados->filter(function ($sol){
            return $sol->estado == ProductoSolicitado::INCOMPLETO;
        })->count();
        $completados = $solicitados->filter(function ($sol){
            return $sol->estado == ProductoSolicitado::COMPLETADO;
        })->count();

        if($incompletos || $completados){
            $nuevoEstadoProduccion = $cntSol==$completados ? ProduccionEstado::COMPLETADA : ProduccionEstado::INCOMPLETA;
            $produccion->produccion_estado_id = $nuevoEstadoProduccion;
            $produccion->save();
        }

        return $nuevoEstadoProduccion;

    }

    public function validaRealizados(Produccione $produccion,$realizados=[])
    {
        if (array_sum($realizados)==0){
            return false;
        }

        $result = collect();
        foreach ($realizados as $idItem => $cantidad) {
            if($cantidad>0) {
                $result->push(
                    new ProductoRealizado([
                        'produccione_id' => $produccion->id,
                        'item_id' => $idItem,
                        'cantidad' => $cantidad,
                        'user_id' => Auth::user()->id
                    ])
                );
            }
        }

        return $result;
    }

    public function validaConsumidos(Produccione $produccion,$consumidos=[],$realizados)
    {

        $items= collect();
        $cantRealizados =[];

        foreach ($realizados as $index => $det) {
            $item = $det->item;
            $item->setAttribute('qty', $det->cantidad);
            $items->push($item);
            $cantRealizados[$item->id]= $det->cantidad;
        }

        //materias nesesarias para la produccion de los realizados
        $materias = $this->materias($items);

        $recetas = [];
        $items = collect();

        foreach ($materias as $index => $receta){
            $item = $receta->item;

            if($consumidos[$item->id]==0){
                $items->push($item);
            }

            $recetas[$item->id]= $receta->cantidad;
        }



        if ($items->count()>0){

            foreach ($items as $item){
                $this->msjErrorValidaConsumidos.="<strong>";
                $this->msjErrorValidaConsumidos.="No ingreso cantidad consumida para el articulo ".$item->nombre;
                $this->msjErrorValidaConsumidos.="</strong><br>";
            }

            return false;
        }


        $result = collect();
        foreach ($consumidos as $idItem => $cantidad) {
            if($cantidad>0){
                $result->push(
                    new ProductoConsumido([
                        'produccione_id' => $produccion->id,
                        'item_id' => $idItem,
                        'cantidad_receta' => $recetas[$idItem],
                        'cantidad_ingresada' => $cantidad,
                        'user_id' => Auth::user()->id
                    ])
                );
            }
        }

        return $result;

    }

    public function procesarSolicitud(Request $request)
    {
        $solicitados = session()->get('solicitados');
        $materias = $this->materias($solicitados);

        if(!$solicitados){
            flash('No hay artículos solicitados')->error()->important();
            return redirect(route('producciones.create'));
        }

        $produccione = $this->produccioneRepository->create([
            'produccion_tipo_id' => ProduccionTipo::MateriaPrimaAProductoFinal,
            'produccion_estado_id' => ProduccionEstado::SOLICITADA,
            'user_id' => Auth::user()->id
        ]);

        if($produccione){

            list($fecha,$hora) = explode(' ',$produccione->created_at);

            $correlativo=  Correlativo::siguiente('producciones');

            $numero=  'PROD-'.str_replace("-","",$fecha).$correlativo->max;
            $produccione->numero = $numero;
            $produccione->save();
            $correlativo->save();

            $solicitados = $solicitados->map(function ($item) use ($produccione){
                return new ProductoSolicitado([
                    'produccione_id' => $produccione->id,
                    'item_id' => $item->id,
                    'cantidad' => $item->qty
                ]);
            });

            $traslados = $materias->map(function ($m) use ($produccione){
                return new TrasladoMateriaPrima([
                    'produccione_id' => $produccione->id,
                    'item_id' => $m->item_id,
                    'cantidad' => $m->cantidad
                ]);
            });

            $produccione->productoSolicitados()->saveMany($solicitados);
            $produccione->trasladoMateriaPrimas()->saveMany($traslados);

            $stock = new Stock();
            foreach ($traslados as $detalle){
                //Realiza el egreso de la bodega de producción
                $stock->egresoProduccionMp($detalle->item_id,$detalle->cantidad,$detalle->id);
                //Realiza el ingreso en cocina de producción
                $stock->ingresoProduccionMp($detalle->item_id,$detalle->cantidad,$detalle->id);
            }

        }

        if($produccione){
            Mail::send(new ProduccionSolicitud($produccione));
        }
    }

    public function cancelar(Produccione $produccion)
    {
        if (is_null($produccion)){
            flash('Producción no encontrada')->warning()->important();
            return redirect()->back();
        }


        try {
            DB::beginTransaction();

            $this->procesarCancelar($produccion);

        } catch (\Exception $exception) {
            DB::rollBack();

            if (Auth::user()->isSuperAdmin()){
                dd($exception->getMessage(),$exception->getLine());
            }

            $msg = Auth::user()->isAdmin() ? $exception->getMessage() : 'Hubo un error intente de nuevo';

            flash('Error: '.$msg)->error()->important();
            return redirect()->back();
        }


        DB::commit();

        flash('Producción cancelada correctamente')->success()->important();

        return redirect(route('producciones.index'));

    }

    public function anular()
    {

    }

    public function procesarCancelar(Produccione $produccion)
    {

        if ($produccion->enProceso()){
            flash('La producción ya esta en proceso')->important()->error();
            return redirect()->back();
        }

        $produccion->produccion_estado_id = ProduccionEstado::CANCELADA;
        $produccion->fecha_cancela = Carbon::now();
        $produccion->save();

        $stock = new Stock();

        foreach ($produccion->trasladoMateriaPrimas as $detalle){
            //Se egresa la materia prima de cocina
            $stock->egresoProduccionMp($detalle->item->id,$detalle->cantidad,$detalle->id,Tienda::COCINA_PRODUCCION);
            //se ingresa la materia prima a bodega de produccion
            $stock->ingresoProduccionMp($detalle->item->id,$detalle->cantidad,$detalle->id,null,null,Tienda::BODEGA_PRODUCCION);
        }

    }

}
