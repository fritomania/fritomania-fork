<?php

namespace App\Http\Controllers;

use App\DataTables\EatributoDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateEatributoRequest;
use App\Http\Requests\UpdateEatributoRequest;
use App\Repositories\EatributoRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class EatributoController extends AppBaseController
{
    /** @var  EatributoRepository */
    private $eatributoRepository;

    public function __construct(EatributoRepository $eatributoRepo)
    {
        $this->middleware('auth');
        $this->eatributoRepository = $eatributoRepo;
    }

    /**
     * Display a listing of the Eatributo.
     *
     * @param EatributoDataTable $eatributoDataTable
     * @return Response
     */
    public function index(EatributoDataTable $eatributoDataTable)
    {
        return $eatributoDataTable->render('eatributos.index');
    }

    /**
     * Show the form for creating a new Eatributo.
     *
     * @return Response
     */
    public function create()
    {
        return view('eatributos.create');
    }

    /**
     * Store a newly created Eatributo in storage.
     *
     * @param CreateEatributoRequest $request
     *
     * @return Response
     */
    public function store(CreateEatributoRequest $request)
    {
        $input = $request->all();

        $eatributo = $this->eatributoRepository->create($input);

        Flash::success('Eatributo saved successfully.');

        return redirect(route('eatributos.index'));
    }

    /**
     * Display the specified Eatributo.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $eatributo = $this->eatributoRepository->findWithoutFail($id);

        if (empty($eatributo)) {
            Flash::error('Eatributo not found');

            return redirect(route('eatributos.index'));
        }

        return view('eatributos.show')->with('eatributo', $eatributo);
    }

    /**
     * Show the form for editing the specified Eatributo.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $eatributo = $this->eatributoRepository->findWithoutFail($id);

        if (empty($eatributo)) {
            Flash::error('Eatributo not found');

            return redirect(route('eatributos.index'));
        }

        return view('eatributos.edit')->with('eatributo', $eatributo);
    }

    /**
     * Update the specified Eatributo in storage.
     *
     * @param  int              $id
     * @param UpdateEatributoRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateEatributoRequest $request)
    {
        $eatributo = $this->eatributoRepository->findWithoutFail($id);

        if (empty($eatributo)) {
            Flash::error('Eatributo not found');

            return redirect(route('eatributos.index'));
        }

        $eatributo = $this->eatributoRepository->update($request->all(), $id);

        Flash::success('Eatributo updated successfully.');

        return redirect(route('eatributos.index'));
    }

    /**
     * Remove the specified Eatributo from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $eatributo = $this->eatributoRepository->findWithoutFail($id);

        if (empty($eatributo)) {
            Flash::error('Eatributo not found');

            return redirect(route('eatributos.index'));
        }

        $this->eatributoRepository->delete($id);

        Flash::success('Eatributo deleted successfully.');

        return redirect(route('eatributos.index'));
    }
}
