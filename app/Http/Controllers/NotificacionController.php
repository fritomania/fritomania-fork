<?php

namespace App\Http\Controllers;

use App\Models\Notificacione;
use App\Models\NotificacionTipo;
use Auth;
use Illuminate\Http\Request;

class NotificacionController extends Controller
{

    /**
     * NotificacionController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function ver(NotificacionTipo $tipo)
    {
        $user = Auth::user();

        $nots = Notificacione::delUser($user->id)->deTipo($tipo->id)->noVistas();

//        dd('Cambiar el estados de las notificaciones de tipo: '.$tipo->nombre,$nots->get()->toArray());

        $nots->update(['vista' => 1]);




        switch ($tipo->id){

            case NotificacionTipo::SOLICITUD:

                return redirect(route('mis.solicitudes'));

                break;

            case NotificacionTipo::DELIVERY:

                return redirect(route('delivery.index'));

                break;

            case NotificacionTipo::STOCK_CRITICO:

                return redirect(route('inventario.stock.tienda'));

                break;
            default:
                return redirect(route('admin.home'));
                break;
        }
    }
}
