<?php

namespace App\Http\Controllers;

use App\DataTables\CajaDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateCajaRequest;
use App\Http\Requests\UpdateCajaRequest;
use App\Repositories\CajaRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\URL;
use Response;

class CajaController extends AppBaseController
{
    /** @var  CajaRepository */
    private $cajaRepository;

    public function __construct(CajaRepository $cajaRepo)
    {
        $this->middleware("auth");
        $this->cajaRepository = $cajaRepo;
    }

    /**
     * Display a listing of the Caja.
     *
     * @param CajaDataTable $cajaDataTable
     * @return Response
     */
    public function index(CajaDataTable $cajaDataTable)
    {
        return $cajaDataTable->render('cajas.index');
    }

    /**
     * Show the form for creating a new Caja.
     *
     * @return Response
     */
    public function create()
    {
        return view('cajas.create');
    }

    /**
     * Store a newly created Caja in storage.
     *
     * @param CreateCajaRequest $request
     *
     * @return Response
     */
    public function store(CreateCajaRequest $request)
    {
        $user = Auth::user();

        $input = $request->all();

        $input['user_id']= $user->id;
        $input['tienda_id']= session('tienda');

        $caja = $this->cajaRepository->create($input);

        Flash::success('Listo! caja abierta.');

        return redirect(route('ventas.create'));
    }

    /**
     * Display the specified Caja.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $caja = $this->cajaRepository->findWithoutFail($id);

        if (empty($caja)) {
            Flash::error('Caja no existe');

            return redirect(route('cajas.index'));
        }

        if(!Auth::user()->cajaAbierta()){
            flash('Debes abrir una caja')->error()->important();
            return redirect(route('cajas.create'));
        }

        return view('cajas.show')->with('caja', $caja);
    }

    /**
     * Show the form for editing the specified Caja.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $caja = $this->cajaRepository->findWithoutFail($id);

        if (empty($caja)) {
            Flash::error('Caja no existe');

            return redirect(route('cajas.index'));
        }

        return view('cajas.edit')->with('caja', $caja);
    }

    /**
     * Update the specified Caja in storage.
     *
     * @param  int              $id
     * @param UpdateCajaRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCajaRequest $request)
    {

//        dd($request->all());
        $caja = $this->cajaRepository->findWithoutFail($id);

        if (empty($caja)) {
            Flash::error('Caja no existe');

            return redirect(route('cajas.index'));
        }

        $datos= $request->all();
        $datos['fecha_cierre']=fechaHoraActualDb();

        $caja = $this->cajaRepository->update($datos, $id);

        Flash::success('Listo! caja cerrada.');

//        return redirect($request->url_prev);
        return redirect(route('dashboard'));
    }

    /**
     * Remove the specified Caja from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $caja = $this->cajaRepository->findWithoutFail($id);

        if (empty($caja)) {
            Flash::error('Caja no existe');

            return redirect(route('cajas.index'));
        }

        $this->cajaRepository->delete($id);

        Flash::success('Caja eliminado exitosamente.');

        return redirect(route('cajas.index'));
    }

}
