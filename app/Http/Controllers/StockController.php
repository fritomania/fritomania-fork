<?php

namespace App\Http\Controllers;

use App\Models\Icategoria;
use App\Models\Item;
use App\VistaStock;
use Auth;
use Illuminate\Http\Request;

class StockController extends Controller
{
    public function rpt(Request $request)
    {
        $categoria_id = isset($request->categoria_id) ? $request->categoria_id : null;

        if (Auth::user()->isSuperAdmin() || Auth::user()->isAdmin()){

            $lugar = $request->lugar ?? null ;
        }
        else{
            $lugar = session('tienda') ;
        }

        $buscar = isset($request->buscar) ? $request->buscar : null;

        $stocks = VistaStock::orderBy('nombre');


        if(isset($categoria_id)){

            if($categoria_id=='S'){
                $stocks = $stocks->whereNull('categoria_id');
            }else{
                $stocks = $stocks->where('categoria_id',$categoria_id);
            }
        }

        if ($lugar){
            $stocks = $stocks->deLugar($lugar);
        }

        $stocks = $stocks->get();


        return view('reportes.stock',compact('stocks','categoria_id','lugar','buscar'));


    }
}
