<?php

namespace App\Http\Controllers;

use App\DataTables\Scopes\ScopeClienteDataTable;
use App\DataTables\Scopes\ScopeEntreFechasCompraDataTable;
use App\DataTables\Scopes\ScopeItemVentaDataTable;
use App\DataTables\Scopes\ScopeVentaTiendaDt;
use App\DataTables\Scopes\VentaEstadoScoopeDt;
use App\DataTables\VentaCreditoPendientesDataTable;
use App\DataTables\VentaDataTable;
use App\extensiones\PDF_AutoPrint;
use App\Facades\Correlativo;
use App\Http\Requests;
use App\Http\Requests\CreateVentaRequest;
use App\Http\Requests\UpdateVentaRequest;
use App\Mail\StockCriticoPorVentaMail;
use App\Models\Caja;
use App\Models\Cliente;
use App\Models\Ingrediente;
use App\Models\Item;
use App\Models\Notificacione;
use App\Models\NotificacionTipo;
use App\Models\Stock;
use App\Models\TempVenta;
use App\Models\Tienda;
use App\Models\TipoPago;
use App\Models\Venta;
use App\Models\VentaDetalle;
use App\Models\VentaReceta;
use App\Models\Vestado;
use App\Repositories\TempVentaRepository;
use App\Repositories\VentaRepository;
use App\User;
use Carbon\Carbon;
use Codedge\Fpdf\Fpdf\Fpdf;
use DB;
use Dompdf\Dompdf;
use Dompdf\Options;
use Flash;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Mail;
use NumeroALetras;
use Response;

class VentaController extends AppBaseController
{
    /** @var  VentaRepository */
    private $ventaRepository;
    private $tempVentaRepository;

    public function __construct(VentaRepository $ventaRepo,TempVentaRepository $tempVentaRepository)
    {
        $this->middleware('auth');
        $this->ventaRepository = $ventaRepo;
        $this->tempVentaRepository = $tempVentaRepository;
    }

    public function verificaCajaAjax(){

        $user = Auth::user();

        if (empty($user)) {
            return $this->sendError('Usuario no encontrado');
        }

        if (!$user->cajaAbierta()){
            return $this->sendError('Caja no abierta');
        }

        return $this->sendResponse($user->cajaAbierta(),'Caja devuelta');
    }

    public function newCaja(User $user,$monot=0)
    {
        return  Caja::create([
            'monto_apertura' => $monot,
            'user_id' => $user->id,
            'tienda_id' => session('tienda')
        ]);
    }

    public function getCajaUser(User $user)
    {
        $cajaUser = $user->cajaAbierta();

        //si en las configuraciones esta abrir_caja_auto en 1
        if(config('app.abrir_caja_auto')){

            //Y el usuario no tiene caja abierta
            if(!$cajaUser){

                //Apertura nueva caja
                $cajaUser = $this->newCaja($user);
            }else{

//                dd($cajaUser->toArray());
                list($fechaCaja,$horaCaja) = explode(' ',$cajaUser->created_at);

                //si la fecha de apertura de la caja es diferente de la fecha actual
                if($fechaCaja != hoyDb()){

                    //Cierra la caja actual
                    $cajaUser->fecha_cierre=fechaHoraActualDb();
                    $cajaUser->monto_cierre=$cajaUser->monto_cierre;
                    $cajaUser->save();

                    //Apertura nueva caja
                    $cajaUser = $this->newCaja($user);
                };
            }
        }else{
            if($cajaUser){

                list($fechaCaja,$horaCaja) = explode(' ',$cajaUser->created_at);

                //si la fecha de apertura de la caja es diferente de la fecha actual
                if($fechaCaja != hoyDb()){

                    //Cierra la caja actual
                    $cajaUser->fecha_cierre=fechaHoraActualDb();
                    $cajaUser->monto_cierre=$cajaUser->monto_cierre;
                    $cajaUser->save();

                };
            }
        }

        return $cajaUser;
    }

    /**
     * Display a listing of the Venta.
     *
     * @param VentaDataTable $ventaDataTable
     * @return Response
     */
    public function index(VentaDataTable $ventaDataTable, Request $request)
    {
        $del = $request->del ? $request->del : fechaDb(iniMes());
        $al = $request->al ? $request->al : hoyDb();
        $cliente_id = $request->cliente_id ? $request->cliente_id : null;
        $item_id = $request->item_id ? $request->item_id : null;
        $estado = $request->estado ?? null;

        $tienda = $request->tienda ?? session('tienda');

        if($cliente_id){
            $ventaDataTable->addScope(new ScopeClienteDataTable($cliente_id));
        }

        if($item_id){
            $ventaDataTable->addScope(new ScopeItemVentaDataTable($item_id));
        }

        if($del && $al){
            $ventaDataTable->addScope(new ScopeEntreFechasCompraDataTable($del,$al));
        }

        if ($estado){
            $ventaDataTable->addScope(new VentaEstadoScoopeDt($estado));
        }

        if($tienda!='T'){
            $ventaDataTable->addScope(new ScopeVentaTiendaDt($tienda));
        }

        $ventaDataTable->setTitulo('Reporte de ventas');
        $ventaDataTable->setSubtitulo(Carbon::now());

        return $ventaDataTable->render('ventas.index',compact ('del','al','cliente_id','item_id','estado','tienda'));
    }

    public function cobrar(VentaCreditoPendientesDataTable $ventaDataTable)
    {
        return $ventaDataTable->render('ventas.cobrar');
    }

    /**
     * Show the form for creating a new Venta.
     *
     * @return Response
     */
    public function create()
    {
        //session()->forget('modificaciones');

        if(!session()->has('tienda')){
            session()->put('tienda',\Auth::user()->tienda->id);
            session()->save();
        }

        $user=Auth::user();
        $cajaUser = $this->getCajaUser($user);

        $tempVenta = TempVenta::where('procesada',0)
            ->where('user_id',$user->id)
            ->get();

        ///si el usuario no tiene ninguna venta creada
        if($tempVenta->count()>1) {
            dd('el usuario tiene '.$tempVenta->count().' Ventas temporales');
        }

        if($tempVenta->count()==0){

            $tempVenta = TempVenta::create([
                'user_id' => $user->id
            ]);

        }else{
            $tempVenta = $tempVenta[0];
        }


        $clientes = Cliente::all()->toArray();

        $clientes = array_pluck($clientes,'full_name','id');


        return view('ventas.create',compact('clientes','tempVenta'));

    }

    /**
     * Store a newly created Venta in storage.
     *
     * @param CreateVentaRequest $request
     *
     * @return Response
     */
    public function store(TempVenta $tempVenta,CreateVentaRequest $request)
    {

        //dd($request->all(),$tempVenta->detalles->toArray());

        if (empty($tempVenta)) {
            Flash::error('Temp Venta no existe');

            return redirect(route('ventas.index'));
        }

        if ($tempVenta->procesada){
            Flash::error('Venta ya procesada');

            return redirect(route('ventas.create'));
        }


        $itemsStockInsuficiente=$this->validaStock($tempVenta->tempVentaDetalles,$request->detalles_combo);

        $detGruop=$this->detGroup($tempVenta->tempVentaDetalles,$request->detalles_combo);

        if(count($itemsStockInsuficiente)>0 && !$request->fecha_entrega){
//            $msj="<ul>";
            $msj="";
            foreach ($itemsStockInsuficiente as $item){
                $msj.="<strong>";
                $msj.="El articulo ".$item->nombre.", tiene ".nf($item->stockTienda(),0)." existencias e intenta vender ".nf($detGruop[$item->id],0);
                $msj.="</strong><br>";
            }
//            $msj.="</ul>";

//            flash()->overlay($msj,'Ups!','danger');
            flash($msj)->error()->important();

            return redirect(route('ventas.create',['tempVenta' => $tempVenta]))->withInput($request->all());
        }else{

            try {
                DB::beginTransaction();

                $venta= $this->procesar($tempVenta,$request);

            } catch (\Exception $exception) {
                DB::rollBack();

                if (Auth::user()->isSuperAdmin()){
                    dd($exception);
                }

                $msg = Auth::user()->isAdmin() ? $exception->getMessage() : 'Hubo un error intente de nuevo';

                flash('Error: '.$msg)->error()->important();
                return redirect()->back();
            }

            session()->forget('modificaciones');
            DB::commit();

            $this->verificaStockCritico($venta);

            Flash::success('Venta procesada!.');

            return redirect(route('ventas.ticket.view',$venta->id));

        }
    }

    /**
     * Display the specified Venta.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $venta = $this->ventaRepository->findWithoutFail($id);

        if (empty($venta)) {
            Flash::error('Venta no existe');

            return redirect(route('ventas.index'));
        }

        return view('ventas.show')->with('venta', $venta);
    }

    /**
     * Show the form for editing the specified Venta.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $venta = $this->ventaRepository->findWithoutFail($id);

        if (empty($venta)) {
            Flash::error('Venta no existe');

            return redirect()->back();
        }

        $clientes = Cliente::all()->toArray();

        $clientes = array_pluck($clientes,'full_name','id');

        return view('ventas.edit',compact('venta','clientes'));
    }

    /**
     * Update the specified Venta in storage.
     *
     * @param  int              $id
     * @param UpdateVentaRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateVentaRequest $request)
    {
        $venta = $this->ventaRepository->findWithoutFail($id);

        //dd($request->all(),$venta->detalles->toArray());

        if (empty($venta)) {
            Flash::error('Venta no existe');

            return redirect(route('ventas.index'));
        }

        $venta = $this->ventaRepository->update($request->all(), $id);

        flash('Venta Actualizada con éxito.')->success()->important();

        return redirect()->back();

    }

    /**
     * Remove the specified Venta from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $venta = $this->ventaRepository->findWithoutFail($id);

        if (empty($venta)) {
            Flash::error('Venta no existe');

            return redirect(route('ventas.index'));
        }

        $this->ventaRepository->delete($id);

        Flash::success('Venta eliminada exitosamente.');

        return redirect(route('ventas.index'));
    }

    public function procesar(TempVenta $tempVenta,CreateVentaRequest $request){
        $user=Auth::user();
        $userCajaAbierta = $user->cajaAbierta();
        $detallesCombo = $request->detalles_combo;


        $detalles = collect();
        foreach ($tempVenta->tempVentaDetalles as $det){
            $detId = $det->id;
            $articulo = $det->item;


            //Articulo no modificable, no tiene receta y no es combo
            if(!$articulo->tieneReceta() && !$articulo->esCombo()){

                $detalle = new VentaDetalle($det->toArray());

                $detalles->push($detalle);
            }

            //Articulo con receta que no es combo
            if (($articulo->tieneReceta() || $articulo->tieneSalsas() || $articulo->tieneExtras()) && !$articulo->esCombo()){


                for ($numItem=1;$numItem<=$det->cantidad;$numItem++){

                    $detalle = new VentaDetalle([
                        'item_id' => $articulo->id,
                        'cantidad' => 1,
                        'precio' => $articulo->precio_venta,
                        'ingredientes' => $this->getIngredientes($det,$numItem),
                        'num_item' => $numItem,
                    ]);

                    $detalles->push($detalle);

                    $adicionalesYsalsas = $this->adicionalesYSalsas($det->id,$numItem);

                    if ($adicionalesYsalsas->count() > 0){

                        $detalles = $detalles->merge($adicionalesYsalsas);
                    }


                }
            }


            //Si es combo unitario
            if ($articulo->esComboUnitario()){

                //este detalle es el combo como tal
                $detalle = new VentaDetalle($det->toArray());

                $detalles->push($detalle);

                $cantidad = $articulo->combo->piezas * $det->cantidad;
                $articuloCombo = $articulo->combo->detalles->first()->item;

                //si el unico articulo del combo tiene receta
                if ($articuloCombo->tieneReceta()){


                    for ($numItem=1;$numItem<=$cantidad;$numItem++){

                        $detalle = new VentaDetalle([
                            'item_id' => $articuloCombo->id,
                            'cantidad' => 1,
                            'precio' => 0,
                            'ingredientes' => $this->getIngredientes($det,$numItem,$articuloCombo),
                            'num_item' => $numItem,
                            'es_detalle_combo' => 1,
                        ]);

                        $detalles->push($detalle);

                        $adicionalesYsalsas = $this->adicionalesYSalsas($det->id,$numItem);

                        if ($adicionalesYsalsas->count() > 0){

                            $detalles = $detalles->merge($adicionalesYsalsas);
                        }

                    }
                }else{
                    $detalle = new VentaDetalle([
                        'item_id' => $articuloCombo->id,
                        'cantidad' => $cantidad,
                        'precio' => 0,
                    ]);

                    $detalles->push($detalle);
                }

                if($articulo->tieneSalsas() || $articulo->tieneExtras()){
                    $adicionalesYsalsas = $this->adicionalesYSalsas($det->id,1);

                    if ($adicionalesYsalsas->count() > 0){

                        $detalles = $detalles->merge($adicionalesYsalsas);
                    }
                }


            }


            //articulo combo no unitario de 2 o mas articulaos sin posibilidad de editar
            if($articulo->esCombo() && !$articulo->esComboUnitario()){

                //este detalle es el combo como tal
                $detalle = new VentaDetalle($det->toArray());

                $detalles->push($detalle);

                //y existen detalles del combo
                if(isset($detallesCombo[$detId])){

                    foreach ($detallesCombo[$detId] as $itemId => $cantidad){

                        $articuloCombo = Item::find($itemId);

                        if ($articuloCombo->tieneReceta()){


                            for ($numItem=1;$numItem<=$cantidad;$numItem++){

                                $detalle = new VentaDetalle([
                                    'item_id' => $articuloCombo->id,
                                    'cantidad' => 1,
                                    'precio' => 0,
                                    'ingredientes' => $this->getIngredientes($det,$numItem,$articuloCombo),
                                    'num_item' => $numItem,
                                    'es_detalle_combo' => 1,
                                ]);

                                $detalles->push($detalle);

                                $adicionalesYsalsas = $this->adicionalesYSalsas($det->id,$numItem);

                                if ($adicionalesYsalsas->count() > 0){

                                    $detalles = $detalles->merge($adicionalesYsalsas);
                                }

                            }
                        }else{
                            $detalle = new VentaDetalle([
                                'item_id' => $itemId,
                                'cantidad' => $cantidad,
                                'precio' => 0,
                                'es_detalle_combo' => 1,
                            ]);

                            $detalles->push($detalle);
                        }
                    }

                }

            }


        }

        $cliente = Cliente::find($request->cliente_id);

        //Siguiente correlativo para tabla ventas
        $correlativo = Correlativo::siguiente('ventas');

        $datos = [
            'fecha' => hoyDb(),
            'fecha_entrega' => $request->fecha_entrega,
            'hora_entrega' => $request->hora_entrega,
            'correlativo' => $correlativo->max,
            'vestado_id' => Vestado::PAGADA,
            'cliente_id' => $cliente->id,
            'tienda_id' => session()->get('tienda'),
            'serie' => $request->serie,
            'numero' => $request->numero,
            'user_id' => $user->id,
            'recibido' => $request->recibido ? $request->recibido : 0,
            'credito' => $request->credito ? 1 : 0,
            'caja_id' => $userCajaAbierta->id,
            'delivery' => $request->delivery,
            'monto_delivery' => $request->monto_delivery,
            'tipo_pago_id' => $request->tipo_pago_id,
            'direccion' => $request->direccion,
            'observaciones' => $request->observaciones,
            'nombre_entrega' => $cliente->full_name,
            'telefono' => $cliente->telefono,
            'correo' => $cliente->correo,
            'web' => 0,
        ];

        $venta = $this->ventaRepository->create($datos);

        if ($venta){
            $correlativo->save();
        }

        $venta->ventaDetalles()->saveMany($detalles);

        if (!$request->fecha_entrega){
            $stock = new Stock();
            foreach ($detalles as $detalle){

                //Rebajar solo si el item es inventariable
                if($detalle->item->inventariable){
                    $stock->egreso($detalle->item_id,$detalle->cantidad,$detalle->id);
                }
            }
        }

        $itemStockCritico= collect();
        foreach ($detalles as $detalle){

            //Agregar solo los items que no sean inventariables y que su stock sea menor o igual al stock critico definido
            if($detalle->item->inventariable && $detalle->item->stockTienda()<=$detalle->item->stockCriticoTienda()){
                $itemStockCritico->push($detalle->item);
            }
        }

        if ($itemStockCritico->count()>0){
            $tienda = Tienda::find(session('tienda'));


            if ($tienda->admin){
                //Crear notificación para el admin de la tienda
                Notificacione::create([
                    'mensaje' => '',
                    'user_id' => $tienda->admin,
                    'notificacion_tipo_id' => NotificacionTipo::STOCK_CRITICO,
                ]);
            }


            Mail::send(new StockCriticoPorVentaMail($itemStockCritico,$venta));
        }


        $tempVenta->procesada=1;
        $tempVenta->save();

        $this->abono($venta,$request->monto_ini);

        return $venta;
    }

    public function cancelar(TempVenta $tempVenta){


        $tempVenta->delete();

        Flash::success('Listo! venta cancelada.');

        return redirect(route('ventas.create'));
    }

    public function anular(Venta $venta){

        try {
            DB::beginTransaction();

            $this->procesarAnular($venta);

        } catch (\Exception $exception) {
            DB::rollBack();

            if (Auth::user()->isSuperAdmin()){
                dd($exception->getMessage(),$exception->getLine());
            }

            $msg = Auth::user()->isAdmin() ? $exception->getMessage() : 'Hubo un error intente de nuevo';

            flash('Error: '.$msg)->error()->important();
            return redirect()->back();
        }


        DB::commit();

        Flash::success('Listo! venta anulada.');

        return redirect(route('ventas.index'));
    }

    public function pdfFactura(Venta $venta){



        if(config('app.pdf_render')=='snappy'){
            $pdf = App::make('snappy.pdf.wrapper');

            $view = \View::make('ventas.factura_pdf2', compact('venta'))->render();
            $footer = \View::make('ventas.factura_pdf_footer')->render();

            $pdf->loadHTML($view)
                ->setPaper('letter')
                ->setOrientation('portrait')
                ->setOption('footer-html',utf8_decode($footer))
                ->setOption('margin-top',2)
                ->setOption('margin-bottom',10)
                ->setOption('margin-left',2)
                ->setOption('margin-right',2)
                ->stream('report.pdf');

            return $pdf->inline();

        }
        if(config('app.pdf_render')=='dompdf') {

            $options = new Options();
            $options->set('isRemoteEnabled', TRUE);

            $dompdf = new Dompdf($options);

            $view = \View::make('ventas.factura_pdf', compact('venta'))->render();
            $dompdf->loadHtml($view);
            $dompdf->setPaper('letter', 'portrait');
            $dompdf->render();
            $dompdf->stream('factura_' . $venta->id . '_' . $venta->fecha, ['Attachment' => 0]);
        }

    }

    public function facturaView(Venta $venta)
    {
        return view('ventas.factura',compact('venta'));
    }

    public function comprobanteView(Venta $venta)
    {
        return view('ventas.comprobante',compact('venta'));
    }

    /**
     * Agrupa los detalles sumando la cantidad según el item_id
     * @param $detalles
     * @return array
     */
    public function detGroup($detalles,$detallesCombo)
    {
        $detGroup = array();

        foreach ($detalles as $det) {

            //agrega los detalles del combo
            if($det->item->esCombo()){
                foreach ($detallesCombo[$det->id] as $id => $cant){

                    $detGroup[$id]= isset($detGroup[$id]) ? $detGroup[$id]+$cant : $cant;
                }
            }else{

                $id=$det->item_id;
                $cant=$det->cantidad;

                $detGroup[$id]= isset($detGroup[$id]) ? $detGroup[$id]+$cant : $cant;

            }



        }

        return $detGroup;
    }

    /**
     * Devuelve un array con los items los cuales no alcanza el stock según la suma de las cantidades de los detalles
     * (No valida los artículos que son combos)
     * @param array $detalles
     * @return array
     */
    public function validaStock($detalles=array(),$detallesCombo){

        $itemStockInsuficiente=array();

        foreach ($this->detGroup($detalles,$detallesCombo) as $itemId => $cant){
            $item = Item::find($itemId);

            //si el item es inventariable y el stock de la tienda es menor a la cantidad
            if($item->esInventariable() && $item->stockTienda()<$cant){
                $itemStockInsuficiente[]=$item;
            }
        }

        return $itemStockInsuficiente;
    }

    public function factura(Venta $venta){

        return view('fpdf.facturas.'.config('app.nombre_factura'),compact('venta'));

    }

    /**
     * Inserta pago en la tabla vpagos
     * @param Venta $venta
     * @param $monto
     */
    public function abono(Venta $venta,$monto)
    {
        //Si la venta no es al crédito o el monto es null
        if(!$venta->credito || is_null($monto)) return;

        $venta->vpagos()->create([
            'venta_id' => $venta->id,
            'monto' => $monto,
            'user_id' => Auth::user()->id
        ]);
    }

    /**
     * Display the specified Venta.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function abonarView($id)
    {
        $venta = $this->ventaRepository->findWithoutFail($id);

        if (empty($venta)) {
            Flash::error('Venta no existe');

            return redirect(route('ventas.cobrar'));
        }

        return view('ventas.por_cobrar_abonar')->with('venta', $venta);
    }


    /**
     * Genera un ticket de la venta mediante librería fpdf
     * @param Venta $venta
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function ticket(Venta $venta){

//        $pdf= new Fpdf();
        $pdf = new PDF_AutoPrint();

        $bordes=0;

        /**
         * Ancho detalles
         */
        $wd = [
            'producto' =>54, //Producto
            'cantidad' =>7, //Cantidad
//            'precio' =>12, //Precio
            'sub' =>13, //Sub Total
        ];

        /**
         * Ancho total 76 + 6 d margenes
         */
        $wt=array_sum($wd);

        //alto por defecto mm
        $altoTicket = 150;
        //alto por defecto mm
        $altoDetalle = 5;
        $cntDets = $venta->detalles->count();
        $cntDetsConIngredientes = $venta->detalles()->whereNotNull('ingredientes')->get()->count();
        $altoIgredientes = 3.5;

        $margenIz=3;
        $margenDe=3;

        //Si los detalles son mas de 10 multiplica la diferencia por el alto definido para los detalles y lo suma al alto del ticket
        $altoTicket += $cntDets>10 ? ($cntDets-10)*$altoDetalle : 0;

        $altoTicket += $cntDetsConIngredientes * ($altoIgredientes*2);

        $pdf->AddPage('P',[80,$altoTicket]);
        $pdf->SetAutoPageBreak(false,5);//margen inferior
        $pdf->SetMargins($margenIz,0,$margenDe);

        /* Datos del negocio
        -----------------------------------------------------------------------------------------------------------------*/
        $pdf->SetFont('Arial', '', 10);
        $pdf->Cell(0, 1, '',$bordes,1,'C');
        $pdf->Cell(0, 5, config('app.name'),$bordes,1,'C');
        $pdf->MultiCell(0, 5, config('app.slogan'),$bordes,'J');
        $pdf->MultiCell(0, 5, $venta->local->direccion,$bordes,'J');

        $pdf->Cell(24, 5, 'TELÉFONO: ',$bordes,0);
        $pdf->Cell(50, 5, $venta->local->telefono,0,1,'L');
        $pdf->ln(5);


        $pdf->SetFont('Arial', '', 8);
        $pdf->Cell(12, 4, 'Cliente: ',$bordes,0);
        $pdf->Cell(62, 4, $venta->cliente->full_name,$bordes,1);
        $pdf->Cell(12, 4, 'Teléfono: ',$bordes,0);
        $pdf->Cell(62, 4, $venta->cliente->telefono,$bordes,1);
        $pdf->MultiCell(0, 4,'Dirección: '.$venta->direccion,0,"J");
        $pdf->ln(2);

        list($fecha,$hora)=explode(' ',$venta->created_at);
        $pdf->Cell(12, 5, 'Fecha: ',$bordes,0,'C');
        $pdf->Cell(38, 5, fecha($fecha),$bordes,0,'L');
        $pdf->Cell(10, 5, 'Hora: ',$bordes,0,'R');
        $pdf->Cell(14, 5, $hora,$bordes,1,'L');
        $pdf->ln(4);


        /* detalles
        -----------------------------------------------------------------------------------------------------------------*/
        $altoDet=4;

        $pdf->Cell($wd['producto'], $altoDet, 'Producto', $bordes, 0,'L');
        $pdf->Cell($wd['cantidad'], $altoDet, 'Cantidad', $bordes, 0,'C');
//        $pdf->Cell($wd['precio'], $altoDet, 'P/U', $bordes, 0,'R');
        $pdf->Cell($wd['sub'], $altoDet, 'Total', $bordes, 1,'R');

        foreach ($venta->ventaDetalles as $det) {

            $subTotal=dvs().nfp($det->cantidad*$det->precio);

            $tabulacion = $numItem ='';
            $cantidad = nf($det->cantidad,0).' ';

            if ($det->item->es_extra){

                $tabulacion .= '       ';

            }

            if($det->es_detalle_combo){
                $cantidad = '';
                $tabulacion .= '   ';
            }

            if($det->num_item){
                $numItem = ' ('.$det->num_item.')';
            }

            $texto = $tabulacion.$det->item->nombre.$numItem;

            $pdf->Cell($wd['producto'], $altoDet, $texto, $bordes, 0,'L');
            $pdf->Cell($wd['cantidad'], $altoDet, (int) $det->cantidad, $bordes, 0,'L');
//            $pdf->Cell($wd['precio'], $altoDet, dvs().$det->precio, $bordes, 0,'R');
            $pdf->Cell($wd['sub'], $altoDet, $subTotal, $bordes, 1,'R');

            if($det->ingredientes){
                $pdf->setX(8);
                $pdf->SetFont('Arial', '', 8);
                $pdf->MultiCell($wd['producto'] - 8, $altoIgredientes, $det->ingredientes,0,'J');
                $pdf->setX($margenIz);
            }

        }



        $pdf->Cell($wd['producto']+$wd['cantidad'], 5, 'SubTotal: ',$bordes,0,'R');
        $pdf->Cell($wd['sub'], 5, dvs().nfp($venta->sub_total),$bordes,1,'R');


        $totalVenta = $venta->total;


        if ($venta->descuento){
            $totalVenta-=$venta->descuento;
            $pdf->Cell($wd['producto']+$wd['cantidad'], 5, 'Descuento: ',$bordes,0,'R');
            $pdf->Cell($wd['sub'], 5, dvs().nfp($venta->descuento),$bordes,1,"R");
        }


        if ($venta->delivery){
            $pdf->Cell($wd['producto']+$wd['cantidad'], 5, 'Monto Delivery: ',$bordes,0,'R');
            $pdf->Cell($wd['sub'], 5, dvs().nfp($venta->monto_delivery),$bordes,1,"R");
        }

        $pdf->SetFont('Arial', 'B', 8);
        $pdf->Cell($wd['producto']+$wd['cantidad'], 5, 'TOTAL: ',$bordes,0,'R');
        $pdf->Cell($wd['sub'], 5, dvs().nfp($totalVenta),$bordes,1,"R");
        $pdf->SetFont('Arial', '', 8);

        $pdf->ln(2);
        $pdf->Cell(20, 5, 'Forma Pago: ',$bordes,0);
        $pdf->Cell(54, 5, $venta->tipoPago->nombre,$bordes,1);
        $pdf->ln(2);

        if($venta->tipoPago->id==TipoPago::EFECTIVO){
            $pdf->Cell(0, 5, 'Efectivo: '.dvs().nfp($venta->recibido),$bordes,1,'R');
            $pdf->Cell(0, 5, 'Vuelto: '.dvs().nfp($venta->recibido-$totalVenta),$bordes,1,'R');
        }

        if($venta->tipoPago->id==TipoPago::WEBPAY){
            $pdf->Cell(20, 5, 'Cod WebPay: ',$bordes,0);
            $pdf->Cell(54, 5, $venta->cod_web_pay,$bordes,1);
        }


        $tipoVenta="";

        if (!$venta->web && !$venta->delivery){
            $tipoVenta="Consumo Local";
        }

        if ($venta->web && $venta->delivery){
            $tipoVenta="Web Delivery";
        }

        if ($venta->web && $venta->retiro_en_local){
            $tipoVenta="Web Retiro Local";
        }

        if (!$venta->web && $venta->delivery){
            $tipoVenta="Delivery Local";
        }

        $pdf->ln(2);
//        $pdf->Cell(20, 5, 'Tipo Venta: ',$bordes,0);
        $pdf->Cell(0, 5, $tipoVenta,$bordes,1);


        $pdf->ln(3);
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Cell($wt/2, 5, 'PEDIDO NÚMERO: ',$bordes,0);
        $pdf->Cell($wt/2, 5, $venta->id,$bordes,1);

        $pdf->AutoPrint();
        $pdf->Output('I','ticket '.$venta->id.' '.$venta->fecha.'.pdf');
        exit();

    }

    public function viewTicket(Venta $venta)
    {
        return view('ventas.ticket_view',compact('venta'));
    }

    /**
     * Verifica si hay algún item con stock crítico, de existir alguno envía un email a los administradores
     * @param Venta $venta
     */
    public function verificaStockCritico(Venta $venta)
    {

        $itemStockCritico = collect();

        foreach ($venta->detalles as $detalle){

            //Agregar solo los items que no sean inventariables y que su stock sea menor o igual al stock critico definido
            if($detalle->item->inventariable && $detalle->item->stockTienda()<=$detalle->item->stockCriticoTienda()){
                $itemStockCritico->push($detalle->item);
            }
        }

        if ($itemStockCritico->count() > 0){

            $tienda = Tienda::find(session('tienda'));


            if ($tienda->admin){
                //Crear notificación para el admin de la tienda
                Notificacione::create([
                    'mensaje' => '',
                    'user_id' => $tienda->admin,
                    'notificacion_tipo_id' => NotificacionTipo::STOCK_CRITICO,
                ]);
            }


            Mail::send(new StockCriticoPorVentaMail($itemStockCritico,$venta));
        }

    }

    /**
     * Guarda en session las modificaciones de los ingredientes, adicionales y salsas para cada item
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function storeModifyDet(Request $request)
    {

        $itemId = $request->item_id;
        $detId = $request->det_id;

        $detEdit = Item::find($detId);
        $itemEdit = Item::find($itemId);

        if($itemEdit->tieneReceta()){

            $recetaOriginal = VentaReceta::where('item_id',$itemId)->with('detalles')->first()->detalles;
            $recetaOriginalArray = $recetaOriginal->pluck('ingrediente_id','id')->toArray();
        }

        //dd($recetaOriginal->toArray(),$recetaOriginalArray,$request->modificaciones );

        $modificaciones = [];
        $modificaciones['total'] = 0;


        if($request->modificaciones){

            //ciclo por cada item
            foreach ($request->modificaciones as $itemNum => $recetaModificada) {

                //si la re
                if ($recetaOriginalArray != $recetaModificada){

                    foreach ($recetaOriginal as $det){

                        $id = $det->id;

                        $ingredienteElegido = isset($recetaModificada[$id]) ? Ingrediente::find($recetaModificada[$id]) : null;

                        //si hay ingrediente
                        if ($ingredienteElegido){

                            //y el ingrediente elegido es diferente del de la receta original
                            if ($ingredienteElegido->id != $det->ingrediente->id){

                                $modificaciones[$itemNum]['ingredientes'][] = $ingredienteElegido->nombre;
                            }else{
                                $modificaciones[$itemNum]['ingredientes'][] = $det->ingrediente->nombre;
                            }

                        }else{

                            if ($det->es_opcional){

                                $modificaciones[$itemNum]['ingredientes'][] = 'Sin '.$det->categoria;

                            }else{

                                $modificaciones[$itemNum]['ingredientes'][] = 'Sin '.$det->ingrediente->nombre;
                            }

                        }

                    }
                }else{

                }
            }
        }

        if($adicionales = $request->adicionales){

            foreach ($adicionales as $itemNum => $adicionalesItem) {

                $modificaciones[$itemNum]['adicionales'] = $adicionalesItem;
                $modificaciones['total'] += $this->getTotalItems($adicionalesItem);
            }

        }

        if($salsas = $request->salsas){

            foreach ($salsas as $itemNum => $salsasItem) {
                $modificaciones[$itemNum]['salsas'] = $salsasItem;
                $modificaciones['total'] += $this->getTotalItems($salsasItem);
            }

        }


        $session = session('modificaciones');

        if ($session){

            if (isset($session[$detId])){
                $session[$detId] = $modificaciones;
            }else{
                $session = array_add($session,$detId,$modificaciones);
            }

            session()->put('modificaciones',$session);
        }else{

            $session = session()->put('modificaciones',[$detId => $modificaciones]);
        }

        session()->save();

        $session = session('modificaciones');

        return $this->sendResponse($session[$detId],'Modificaciones');
    }

    public function getIngredientes($detalle,$numItem,$articuloCombo=null)
    {

        $modificaciones = session('modificaciones');

        if (isset($modificaciones[$detalle->id][$numItem]['ingredientes'])){
            $ingredientes = implode(', ',$modificaciones[$detalle->id][$numItem]['ingredientes']);
        }else{

            if ($detalle->item->combo){

                $ingredientes = implode(', ',$articuloCombo->nombresIngredientes());

            }else{

                $ingredientes = implode(', ',$detalle->item->nombresIngredientes());

            }

        }

        return $ingredientes;

    }

    public function adicionalesYSalsas($detId,$numItem)
    {

        $modificaciones = session('modificaciones');

//        dd($detId,$numItem,$modificaciones);
        $adicionalesYsalsas = collect();

        $adicionales = $modificaciones[$detId][$numItem]['adicionales'] ?? false;


        if ($adicionales){

            foreach ($adicionales as $index => $id) {
                $adicional = Item::find($id);

                $detalle = new VentaDetalle([
                    'item_id' => $adicional->id,
                    'cantidad' => 1,
                    'precio' => $adicional->precio_venta,
                ]);

                $adicionalesYsalsas->push($detalle);
            }

        }

        $salsas = $modificaciones[$detId][$numItem]['salsas'] ?? false;

        if ($salsas){

            foreach ($salsas as $index => $id) {
                $salsa = Item::find($id);

                $detalle = new VentaDetalle([
                    'item_id' => $salsa->id,
                    'cantidad' => 1,
                    'precio' => $salsa->precio_venta,
                ]);

                $adicionalesYsalsas->push($detalle);
            }

        }


        return $adicionalesYsalsas;

    }

    public function getModificaciones()
    {
        return $this->sendResponse(session('modificaciones'),'Modificaciones');
    }

    public function getDetalles($id)
    {
        $tempVenta = $this->tempVentaRepository->findWithoutFail($id);

        if (empty($tempVenta)) {
            return $this->sendError('No se encuentra la venta temporal');
        }

        $detalles = $tempVenta->tempVentaDetalles->load('item');

        return $this->sendResponse($detalles, 'Detalles de venta temporal');
    }

    public function getTotalItems($arrayIds)
    {
        $items = Item::whereIn('id',$arrayIds)->get();

        return $items->sum('precio_venta');
    }

    public function procesarAnular(Venta $venta)
    {
        $venta->vestado_id = Vestado::ANULADA;
        $venta->fecha_anula = Carbon::now();
        $venta->save();

        $venta->pagos()->delete();

        //Regresa las cantidades de los lotes en tabla stocks
        foreach ($venta->detalles as $det){
            foreach ($det->stocks as $stock){
                $stock->cantidad = $stock->cantidad + $stock->pivot->cantidad;
                $stock->save();
            }
        }

        $venta->historicoEstados()->attach([ Vestado::ANULADA ]);
    }
}
