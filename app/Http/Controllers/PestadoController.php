<?php

namespace App\Http\Controllers;

use App\DataTables\PestadoDataTable;
use App\Http\Requests;
use App\Http\Requests\CreatePestadoRequest;
use App\Http\Requests\UpdatePestadoRequest;
use App\Repositories\PestadoRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class PestadoController extends AppBaseController
{
    /** @var  PestadoRepository */
    private $pestadoRepository;

    public function __construct(PestadoRepository $pestadoRepo)
    {
        $this->middleware("auth");
        $this->pestadoRepository = $pestadoRepo;
    }

    /**
     * Display a listing of the Pestado.
     *
     * @param PestadoDataTable $pestadoDataTable
     * @return Response
     */
    public function index(PestadoDataTable $pestadoDataTable)
    {
        return $pestadoDataTable->render('pestados.index');
    }

    /**
     * Show the form for creating a new Pestado.
     *
     * @return Response
     */
    public function create()
    {
        return view('pestados.create');
    }

    /**
     * Store a newly created Pestado in storage.
     *
     * @param CreatePestadoRequest $request
     *
     * @return Response
     */
    public function store(CreatePestadoRequest $request)
    {
        $input = $request->all();

        $pestado = $this->pestadoRepository->create($input);

        Flash::success('Pestado saved successfully.');

        return redirect(route('pestados.index'));
    }

    /**
     * Display the specified Pestado.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $pestado = $this->pestadoRepository->findWithoutFail($id);

        if (empty($pestado)) {
            Flash::error('Pestado not found');

            return redirect(route('pestados.index'));
        }

        return view('pestados.show')->with('pestado', $pestado);
    }

    /**
     * Show the form for editing the specified Pestado.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $pestado = $this->pestadoRepository->findWithoutFail($id);

        if (empty($pestado)) {
            Flash::error('Pestado not found');

            return redirect(route('pestados.index'));
        }

        return view('pestados.edit')->with('pestado', $pestado);
    }

    /**
     * Update the specified Pestado in storage.
     *
     * @param  int              $id
     * @param UpdatePestadoRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePestadoRequest $request)
    {
        $pestado = $this->pestadoRepository->findWithoutFail($id);

        if (empty($pestado)) {
            Flash::error('Pestado not found');

            return redirect(route('pestados.index'));
        }

        $pestado = $this->pestadoRepository->update($request->all(), $id);

        Flash::success('Pestado updated successfully.');

        return redirect(route('pestados.index'));
    }

    /**
     * Remove the specified Pestado from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $pestado = $this->pestadoRepository->findWithoutFail($id);

        if (empty($pestado)) {
            Flash::error('Pestado not found');

            return redirect(route('pestados.index'));
        }

        $this->pestadoRepository->delete($id);

        Flash::success('Pestado deleted successfully.');

        return redirect(route('pestados.index'));
    }
}
