<?php

namespace App\Http\Controllers;

use App\DataTables\EtipoDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateEtipoRequest;
use App\Http\Requests\UpdateEtipoRequest;
use App\Repositories\EtipoRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class EtipoController extends AppBaseController
{
    /** @var  EtipoRepository */
    private $etipoRepository;

    public function __construct(EtipoRepository $etipoRepo)
    {
        $this->middleware('auth');
        $this->etipoRepository = $etipoRepo;
    }

    /**
     * Display a listing of the Etipo.
     *
     * @param EtipoDataTable $etipoDataTable
     * @return Response
     */
    public function index(EtipoDataTable $etipoDataTable)
    {
        return $etipoDataTable->render('etipos.index');
    }

    /**
     * Show the form for creating a new Etipo.
     *
     * @return Response
     */
    public function create()
    {
        return view('etipos.create');
    }

    /**
     * Store a newly created Etipo in storage.
     *
     * @param CreateEtipoRequest $request
     *
     * @return Response
     */
    public function store(CreateEtipoRequest $request)
    {
        $input = $request->all();

        $etipo = $this->etipoRepository->create($input);

        Flash::success('Etipo saved successfully.');

        return redirect(route('etipos.index'));
    }

    /**
     * Display the specified Etipo.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $etipo = $this->etipoRepository->findWithoutFail($id);

        if (empty($etipo)) {
            Flash::error('Etipo not found');

            return redirect(route('etipos.index'));
        }

        return view('etipos.show')->with('etipo', $etipo);
    }

    /**
     * Show the form for editing the specified Etipo.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $etipo = $this->etipoRepository->findWithoutFail($id);

        if (empty($etipo)) {
            Flash::error('Etipo not found');

            return redirect(route('etipos.index'));
        }

        return view('etipos.edit')->with('etipo', $etipo);
    }

    /**
     * Update the specified Etipo in storage.
     *
     * @param  int              $id
     * @param UpdateEtipoRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateEtipoRequest $request)
    {
        $etipo = $this->etipoRepository->findWithoutFail($id);

        if (empty($etipo)) {
            Flash::error('Etipo not found');

            return redirect(route('etipos.index'));
        }

        $etipo = $this->etipoRepository->update($request->all(), $id);

        Flash::success('Etipo updated successfully.');

        return redirect(route('etipos.index'));
    }

    /**
     * Remove the specified Etipo from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $etipo = $this->etipoRepository->findWithoutFail($id);

        if (empty($etipo)) {
            Flash::error('Etipo not found');

            return redirect(route('etipos.index'));
        }

        $this->etipoRepository->delete($id);

        Flash::success('Etipo deleted successfully.');

        return redirect(route('etipos.index'));
    }
}
