<?php

namespace App\Http\Controllers;

use App\DataTables\SolicitudeDataTable;
use App\DataTables\SolicitudeDespachaDataTable;
use App\DataTables\SolicitudeUserDataTable;
use App\Events\EventSolicitudCreate;
use App\Http\Requests;
use App\Http\Requests\CreateSolicitudeRequest;
use App\Http\Requests\UpdateSolicitudeRequest;
use App\Mail\DespacharSolicitud;
use App\Mail\SolicitudStock;
use App\Mail\StockCriticoPorSolicitudMail;
use App\Models\Correlativo;
use App\Models\Item;
use App\Models\SolicitudDetalle;
use App\Models\Solicitude;
use App\Models\SolicitudEstado;
use App\Models\Stock;
use App\Models\TempSolicitude;
use App\Repositories\SolicitudeRepository;
use App\Repositories\TempSolicitudeRepository;
use App\User;
use Carbon\Carbon;
use Exception;
use Flash;
use App\Http\Controllers\AppBaseController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Mail;
use Response;

class SolicitudeController extends AppBaseController
{
    /** @var  SolicitudeRepository */
    private $solicitudeRepository;
    private $tempSolicitudeRepository;

    public function __construct(SolicitudeRepository $solicitudeRepo,TempSolicitudeRepository $tempSolicitudeRepository)
    {
        $this->solicitudeRepository = $solicitudeRepo;
        $this->tempSolicitudeRepository = $tempSolicitudeRepository;
        $this->middleware('auth');
    }

    /**
     * Display a listing of the Solicitude.
     *
     * @param SolicitudeDataTable $solicitudeDataTable
     * @return Response
     */
    public function index(SolicitudeDataTable $solicitudeDataTable)
    {
        return $solicitudeDataTable->render('solicitudes.index');
    }

    public function user(SolicitudeUserDataTable $solicitudeDataTable)
    {
        return $solicitudeDataTable->render('solicitudes.index_user');
    }

    public function despacharList(SolicitudeDespachaDataTable $solicitudeDataTable)
    {
        return $solicitudeDataTable->render('solicitudes.despachar');
    }


    /**
     * Show the form for creating a new Solicitude.
     *
     * @return Response
     */
    public function create()
    {
        $user = Auth::user();

        $tempSolicitude = TempSolicitude::where('user_id',$user->id)->first();

        ///si el usuario no tiene ninguna Solicitud creada
        if(!$tempSolicitude){

            $tempSolicitude = TempSolicitude::create(['user_id' => $user->id]);

        };
//            dd($tempSolicitude);
        return view('solicitudes.create', compact('tempSolicitude'));
    }

    /**
     * Store a newly created Solicitude in storage.
     *
     * @param CreateSolicitudeRequest $request
     *
     * @return Response
     */
    public function store(CreateSolicitudeRequest $request)
    {
        $input = $request->all();

        $solicitude = $this->solicitudeRepository->create($input);

        Flash::success('Solicitude guardado exitosamente.');

        return redirect(route('solicitudes.index'));
    }

    /**
     * Display the specified Solicitude.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $solicitude = $this->solicitudeRepository->findWithoutFail($id);

        if (empty($solicitude)) {
            Flash::error('Solicitude no encontrado');

            return redirect(route('solicitudes.index'));
        }

        return view('solicitudes.show',compact('solicitude'));
    }

    /**
     * Show the form for editing the specified Solicitude.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $solicitude = $this->solicitudeRepository->findWithoutFail($id);

        if (empty($solicitude)) {
            Flash::error('Solicitude no encontrado');

            return redirect(route('solicitudes.index'));
        }

        return view('solicitudes.edit',compact('solicitude'));
    }

    /**
     * Update the specified Solicitude in storage.
     *
     * @param  int              $id
     * @param UpdateSolicitudeRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSolicitudeRequest $request)
    {
//        dd($request->all());
        $tempSolicitud = $this->tempSolicitudeRepository->findWithoutFail($id);


        if (empty($tempSolicitud)) {
            Flash::error('Solicitud temporal no existe');

            return redirect(route('solicitudes.index'));
        }

        $solicitud= $this->procesar($tempSolicitud,$request);

        flash('Solicitud procesada')->success()->important();

        return redirect(route('mis.solicitudes'));

    }

    /**
     * Remove the specified Solicitude from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $solicitude = $this->solicitudeRepository->findWithoutFail($id);

        if (empty($solicitude)) {
            Flash::error('Solicitude no encontrado');

            return redirect(route('solicitudes.index'));
        }

        $this->solicitudeRepository->delete($id);

        Flash::success('Solicitude eliminado exitosamente.');

        return redirect(route('solicitudes.index'));
    }

    public function cancelar(TempSolicitude $tempSolicitude){

        $tempSolicitude->tempSolicitudDetalles()->delete();
        $tempSolicitude->delete();

        Flash::success('Listo! solicitud cancelada.');

        return redirect(route('solicitudes.create'));
    }

    public function procesar(TempSolicitude $tempSolicitude,UpdateSolicitudeRequest $request){

        $datos = $request->all();

        $correlativo = Correlativo::OfTabla('solicitudes')->get();
        $correlativo = $correlativo[0];
        $correlativo->max ++;

        $datos = array_add($datos,'numero',$correlativo->max);
        $datos=  array_add($datos,'tienda_solicita', session('tienda'));

        //Crea colección de objetos en base a detalles temporales
        $detalles = $tempSolicitude->tempSolicitudDetalles->map(function ($item) {
            return new SolicitudDetalle($item->toArray());
        });

        //Guarda el encabezado de la Solicitud
        $solicitud = $this->solicitudeRepository->create($datos);

        //Guarda los detalles de la Solicitud
        $solicitud->solicitudDetalles()->saveMany($detalles);


        //Cambia el estado de la Solicitud temporal
        $tempSolicitude->tempSolicitudDetalles()->delete();
        $tempSolicitude->delete();
        $correlativo->save();


//        event(new EventSolicitudCreate($solicitud));

        if($solicitud){
            Mail::send(new SolicitudStock($solicitud));
        }

        return $solicitud;
    }

    /**
     * Agrupa los detalles sumando la cantidad según el item_id
     * @param $detalles
     * @return array
     */
    public function detGroup($detalles)
    {
        $detGroup = array();

        foreach ($detalles as $det) {

            $id=$det->item_id;
            $cant=$det->cantidad;

            $detGroup[$id]= isset($detGroup[$id]) ? number_format($detGroup[$id]+$cant,2) : number_format($cant,2);

        }

        return $detGroup;
    }

    /**
     * Devuelve un array con los items los cuales no alcanza el stock según la suma de las cantidades de los detalles
     * @param array $detalles
     * @return array
     */
    public function validaStock($detalles=array()){

        $itemStockInsuficiente=array();

        foreach ($this->detGroup($detalles) as $itemId => $cant){
            $item = Item::find($itemId);

//            dump('stock: '.$item->stock.' cant: '.$cant);
            if($item->stocks->sum('cantidad')<$cant){
                $itemStockInsuficiente[]=$item;
            }
        }

        return $itemStockInsuficiente;
    }

    public function despachar(Solicitude $solicitud)
    {


        try {
            DB::beginTransaction();


            $this->procesaStock($solicitud);

            $solicitud->solicitud_estado_id = SolicitudEstado::DESPACHADA;
            $solicitud->user_despacha = Auth::user()->id;
            $solicitud->fecha_despacha = fechaHoraActualDb();
            $solicitud->tienda_entrega = session('tienda');
            $solicitud->save();

            $this->verificaStock($solicitud);


            Mail::send(new DespacharSolicitud($solicitud));


        } catch (Exception $exception) {
            DB::rollBack();
            $success = $exception->getMessage();

            flash('Error al procesar intente de nuevo!')->error()->important();

            return redirect(route('solicitudes.despachar'));
        }


        DB::commit();

        flash('Solicitud despachada correctamanete')->success()->important();

        return redirect(route('solicitudes.despachar'));
    }

    public function procesaStock(Solicitude $solicitud)
    {
        $tiendaDespacha = Auth::user()->tienda->id;
        $tiendaSolicita = User::find($solicitud->user_id)->tienda->id;

        $stock = new Stock();
        foreach ($solicitud->solicitudDetalles as $detalle){
            $stock->egresoSolicitud($detalle->item_id,$detalle->cantidad,$detalle->id,$tiendaDespacha);
        }

        foreach ($solicitud->solicitudDetalles as $detalle){
            $stock->ingresoSolicitud($detalle->item_id,$detalle->cantidad,$detalle->id,null,null,$tiendaSolicita);
        }
    }

    public function verificaStock(Solicitude $solicitud)
    {
        $itemStockCritico= collect();
        foreach ($solicitud->solicitudDetalles()->with('item')->get() as $detalle){
            if($detalle->item->stockTienda()<=$detalle->item->stockCriticoTienda()){
                $itemStockCritico->push($detalle->item);
            }
        }

        if ($itemStockCritico->count()>0){
            Mail::send(new StockCriticoPorSolicitudMail($itemStockCritico,$solicitud));
        }
    }

    public function cancelarSolicitud(Solicitude $solicitud)
    {

        if ($solicitud->solicitud_estado_id==SolicitudEstado::DESPACHADA){
            flash('La solicitud ya fue despachada')->important()->warning();
            return redirect()->back();
        }

        try {
            DB::beginTransaction();

            $this->procesaCanelarSolicitu($solicitud);

        } catch (\Exception $exception) {
            DB::rollBack();

            if (Auth::user()->isSuperAdmin()){
                dd($exception->getMessage(),$exception->getLine());
            }

            $msg = Auth::user()->isAdmin() ? $exception->getMessage() : 'Hubo un error intente de nuevo';

            flash('Error: '.$msg)->error()->important();
            return redirect()->back();
        }


        DB::commit();

        flash('Solicitud de stock cancelada correctamente!')->important()->success();

        return redirect()->back();
    }

    public function procesaCanelarSolicitu(Solicitude $solicitud)
    {

        $solicitud->solicitud_estado_id = SolicitudEstado::CANCELADA;
        $solicitud->fecha_cancela = Carbon::now();
        $solicitud->save();

    }

    public function anular(Solicitude $solicitud)
    {

        try {
            DB::beginTransaction();

            $this->procesaAnular($solicitud);

        } catch (\Exception $exception) {
            DB::rollBack();

            if (Auth::user()->isSuperAdmin()){
                dd($exception->getMessage(),$exception->getLine());
            }

            $msg = Auth::user()->isAdmin() ? $exception->getMessage() : 'Hubo un error intente de nuevo';

            flash('Error: '.$msg)->error()->important();
            return redirect()->back();
        }


        DB::commit();

        flash('Solicitud de stock anulada correctamente!')->important()->success();

        return redirect()->back();
    }

    public function procesaAnular(Solicitude $solicitud)
    {

        $solicitud->solicitud_estado_id = SolicitudEstado::ANULADA;
        $solicitud->fecha_anula = Carbon::now();
        $solicitud->save();

        //Regresa el stock que se ingreso a la bodega solicitante
        foreach ($solicitud->detalles as $det){
            foreach ($det->ingresos as $stock){

                $stock->cantidad = $stock->cantidad - $stock->pivot->cantidad;
                $stock->save();
            }
        }

        //Regresa el stock que se egreso de la bodega que despacho
        foreach ($solicitud->detalles as $det){
            foreach ($det->egresos as $stock){

                $stock->cantidad = $stock->cantidad + $stock->pivot->cantidad;
                $stock->save();
            }
        }

    }
}
