<?php

namespace App\Http\Controllers;

use App\extensiones\PDF_AutoPrint;
use App\Mail\PedidoWebMail;
use App\Models\Item;
use App\Models\ItemTiempo;
use App\Models\Rol;
use App\Models\Stock;
use App\Models\Tienda;
use App\Models\Venta;
use App\Models\Vestado;
use Auth;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class OrdenesController extends AppBaseController
{
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index()
    {
        $lugar = Auth::user()->isAdminTienda() ? 'admin' : 'caja';

        return view('ordene.listado',compact('lugar'));
    }

    public function cocina()
    {
        $lugar = 'cocina';

        return view('ordene.listado',compact('lugar'));
    }

    public function delivery()
    {
        $lugar = 'delivery';

        return view('ordene.listado',compact('lugar'));
    }

    public function deliveryCaja()
    {
        $lugar = 'delivery';

        return view('ordene.listado_delivery_caja',compact('lugar'));
    }

    public function datos(Request $request)
    {
        $query = Venta::orden()->with('ventaDetalles')->tienda( session('tienda'))->orderBy('vestado_id')->orderBy('id','desc');

        if($request->lugar=='cocina'){
            $query->enCocina();
        }

        if($request->lugar=='delivery_caja'){
            $query = Venta::delivery()->deEstados([Vestado::EN_RUTA,Vestado::ENTREGADA])->hoy()->orderBy('vestado_id')->orderBy('id','desc');
        }

        $ordenes = $query->with('repartidores')->get();

        if($request->lugar=='delivery'){
            $ordenes = \Auth::user()->deliverys()->orden()->with('ventaDetalles')->with('repartidores')->tienda( session('tienda'))->orderBy('vestado_id')->orderBy('id','desc')->get();
        }

        $ordenes = $ordenes->map(function ($orden){

            $orden->repartidor = $orden->repartidor();
            return $orden;
        });

        if($request->lugar=='banner'){
            $ordenes = Venta::tienda( session('tienda'))->locales()->listas()->orderBy('updated_at','desc')->get();
        }

        return $this->sendResponse($ordenes,'Ordenes');
    }

    public function cambioEstado(Request $request)
    {
        $newEstado = 0;
        switch ($request->estado){
            case Vestado::PAGADA :
                $newEstado= Vestado::ENVIADA_COCINA;
                break;
            case Vestado::ENVIADA_COCINA :
                $newEstado= Vestado::COCINANDO;
                break;
            case Vestado::COCINANDO :
                $newEstado= Vestado::LISTA;
                break;
            case Vestado::LISTA :
                $newEstado= $request->delivery ? Vestado::EN_RUTA : Vestado::ENTREGADA;
                break;
            case Vestado::EN_RUTA:
                $newEstado= Vestado::ENTREGADA;
                break;
            default:
                $newEstado = $request->estado;
        }

        $newEstado = Vestado::find($newEstado);

        $orden = Venta::find($request->orden);
        $orden->vestado_id = $newEstado->id;


        //Si el nuevo estado es ENVIADA_COCINA
        if($orden->vestado_id == Vestado::ENVIADA_COCINA){

            //Si la orden no tiene asociada una caja
            if(!$orden->caja_id){

                //Si el usuario no tiene caja abierta
                if (!Auth::user()->cajaAbierta()){
                    return $this->sendError('Debe abrir caja para poder asociar la orden a una caja');
                }

                $orden->caja_id = Auth::user()->cajaAbierta()->id;
                $orden->user_id = Auth::user()->id;
            }

            //si la orden o pedido fue realizado en web cliente o tiene un fecha de entrega
            if($orden->web || !is_null($orden->fecha_entrega)){
                $itemsStockInsuficiente=$this->validaStock($orden->detalles);
                $detGruop=$this->detGroup($orden->detalles);

                //si hay articulos con stock insuficiente
                if(count($itemsStockInsuficiente)>0){
                    $msj="";
                    foreach ($itemsStockInsuficiente as $item){
                        $msj.="<strong>";
                        $msj.="El articulo ".$item->nombre.", tiene ".nf($item->stockTienda(),0)." existencias e intenta vender ".nf($detGruop[$item->id],0);
                        $msj.="</strong><br>";
                    }

                    return $this->sendError($msj);
                }else{
                    $stock = new Stock();
                    foreach ($orden->detalles as $detalle){
                        $stock->egreso($detalle->item_id,$detalle->cantidad,$detalle->id,$orden->tienda_id);
                    }
                }
            }

            if($orden->fecha_entrega && $orden->fecha_entrega!=hoyDb()){
                return $this->sendError('Aun no es el día de la entrega');
            }
        }

        $orden->historicoEstados()->syncWithoutDetaching([ $newEstado->id ]);
        $orden->save();


        //Para que actualize la relacion estado (Pendiente averiguar como actualizar relacion)
        $orden = Venta::find($request->orden);

        //si el nuevo estado de la orden es lista
        if ($orden->estado->id == Vestado::LISTA) {

            $transcurrido = $orden->transcurido(Vestado::COCINANDO,Vestado::LISTA);

            if ($transcurrido>0){
                foreach ($orden->detalles as $detalle){
                    ItemTiempo::create([
                        'item_id' => $detalle->item->id,
                        'tienda_id' => session('tienda'),
                        'preparacion' => $transcurrido,
                    ]);
                }
            }
        }

        return $this->sendResponse($orden->toArray(),$newEstado->descripcion);
    }

    public function repartidores(Request $request){

        $repartidores = Rol::find(Rol::REPARTIDOR)->users()->presentes()->deTienda(session('tienda'))->get();

        return $this->sendResponse($repartidores, 'Repartidores');
    }

    public function attach(Request $request)
    {
        $orden = Venta::find($request->orden);


        if ($orden->repartidor()){
            return $this->sendError('Orden ya asignada');
        }

        $orden->repartidores()->attach([$request->repartidor]);

        if (!$orden->repartidor()){
            return $this->sendError('Hubo un error intente de nuevo');
        }

        $msg ="Su pedido N° ".$orden->id." esta en camino";


        if($orden->web && $orden->delivery && $orden->correo){

            //Mandar correo que ingresó el cliente
            Mail::to($orden->correo)->send(new PedidoWebMail($orden,$msg));

            $address = ['info@itsb.cl'];

            if(env('APP_DEBUG')){

                $msg ="Su pedido N° ".$orden->id." esta en camino (admin)";
                $address[] = config('app.mail_pruebas');
            }

            Mail::to($address)->send(new PedidoWebMail($orden,$msg));
        }

        return $this->sendResponse($orden,'La orden fue asignada!!');

    }

//    public function imprimeCocina(Venta $orden){
//
//        return view('ventas.print_cocina',compact('orden'));
//    }

    public function imprimeCocina(Venta $orden){

        //alto por defecto mm
        $altoTicket = 110;
        //alto por defecto mm
        $altoDetalle = 10;
        $cntDets = $orden->detalles->count();

        //Si los detalles son mas de 10 multiplica la diferencia por el alto definido para los detalles y lo suma al alto del ticket
        $altoTicket += $cntDets>10 ? ($cntDets-10)*$altoDetalle : 0;

//        dd($cntDets,$altoTicket);
        $pdf = new PDF_AutoPrint();
        $pdf->AddPage('P',[80,$altoTicket]);
        $pdf->SetAutoPageBreak(false,5);//margen inferior
        $pdf->SetMargins(5,0,5);

        $pdf->SetFont('Arial', '', 18);
        $pdf->cell(0, 10, 'Pedido # '.$orden->id,0,1,'R');
        $pdf->ln(5);


        list($fecha,$hora)=explode(' ',$orden->created_at);

        $pdf->SetFont('Arial', '', 10);
        $pdf->cell(0, 10, 'Caja Local: '.$orden->local->nombre,0,1,'L');
        $pdf->cell(0, 10, 'Fecha: '.fecha($fecha).' '.$hora.'hrs',0,1,'L');

//        $pdf->cell(20, 5, 'Cantidad','LTB',0,'C');
//        $pdf->cell(50, 5, 'Articulo','RTB',1,'C');

        $pdf->SetFont('Arial', '', 8);
        foreach ($orden->detalles as $index => $det) {

            $tabulacion = $numItem ='';
            $cantidad = nf($det->cantidad,0).' ';

            if ($det->item->es_extra){

                $tabulacion .= '       ';
                $pdf->SetFont('Arial', '', 8);
            }else{
                $pdf->SetFont('Arial', 'B', 8);
            }

            if($det->es_detalle_combo){
               $cantidad = '';
                $tabulacion .= '   ';
            }

            if($det->num_item){
                $numItem = ' ('.$det->num_item.')';
            }

            $texto = $tabulacion.$cantidad.$det->item->nombre.$numItem;

            $pdf->cell(70, 5, $texto,0,1,'L');
            if($det->ingredientes){
                $pdf->setX(10);
                $pdf->SetFont('Arial', '', 8);
                $pdf->MultiCell(65, 3.5, $det->ingredientes,0,'J');
                $pdf->setX(5);
            }

        }


        list($fecha,$hora)=explode(' ',$orden->updated_at);
        $pdf->ln(5);
        $pdf->SetFont('Arial', '', 10);
        $pdf->cell(0, 10, 'Solicitado: '.fecha($fecha).' '.$hora.'hrs',0,1,'L');

        $pdf->AutoPrint();
        $pdf->Output();
        exit();
    }

    public function anular(Request $request){

        $venta = Venta::find($request->id);

        try {
            DB::beginTransaction();

            $this->procesarAnular($venta);

        } catch (\Exception $exception) {
            DB::rollBack();

            if (Auth::user()->isSuperAdmin()){
                dd($exception->getMessage(),$exception->getLine());
            }

            $msg = Auth::user()->isAdmin() ? $exception->getMessage() : 'Hubo un error intente de nuevo';

            return $this->sendError($msg);
        }


        DB::commit();

        return $this->sendResponse($venta, 'Se anulo correctamente');
    }

    /**
     * Agrupa los detalles sumando la cantidad según el item_id
     * @param $detalles
     * @return array
     */
    public function detGroup($detalles)
    {
        $detGroup = array();

        foreach ($detalles as $det) {

            $id=$det->item_id;
            $cant=$det->cantidad;

            $detGroup[$id]= isset($detGroup[$id]) ? number_format($detGroup[$id]+$cant,2) : number_format($cant,2);

        }

        return $detGroup;
    }

    /**
     * Devuelve un array con los items los cuales no alcanza el stock según la suma de las cantidades de los detalles
     * (No valida los artículos que son combos)
     * @param array $detalles
     * @return array
     */
    public function validaStock($detalles=array()){

        $itemStockInsuficiente=array();

        foreach ($this->detGroup($detalles) as $itemId => $cant){
            $item = Item::find($itemId);

            //si el item es inventariable y el stock de la tienda es menor a la cantidad
            if(!$item->esInventariable() && $item->stockTienda()<$cant){
                $itemStockInsuficiente[]=$item;
            }
        }

        return $itemStockInsuficiente;
    }

    public function procesarAnular(Venta $venta)
    {
        $venta->vestado_id = Vestado::ANULADA;
        $venta->fecha_anula = Carbon::now();
        $venta->save();

        $venta->pagos()->delete();

        //Regresa las cantidades de los lotes en tabla stocks
        foreach ($venta->detalles as $det){
            foreach ($det->stocks as $stock){
                $stock->cantidad = $stock->cantidad + $stock->pivot->cantidad;
                $stock->save();
            }
        }

        $venta->historicoEstados()->attach([ Vestado::ANULADA ]);
    }
}
