<?php

namespace App\Http\Controllers;

use App\Events\NewOrder;
use App\Events\UserCart;
use App\Models\Item;
use App\Order;
use App\OrderLine;
use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use PhpParser\ErrorHandler\Collecting;

class CartController extends AppBaseController
{
    public function index (Request $request) {
//        if ( ! request()->ajax()) {
//            dd('Acceso denegado');
//        }


        $items = session('cart');

        $items = $items->map(function ($item){
           $item->load('combo');
           return $item;
        });

        session()->put('cart',$items);

        return response()->json(session('cart'));
    }

    public function add (Request $request) {
//        if ( ! request()->ajax()) {
//            abort(401, 'Acceso denegado');
//        }


        $this->actualizaCantidades($request->items);

        $cart = session('cart');
        $productId = $request->id;


        if ( ! $cart->contains('id', $productId)) {
            $product = Item::with('combo')->find($productId);

            $product->setAttribute('qty', 1);
            $cart->push($product);
        } else {
            $cart->map(function ($product) use ($productId) {
                if ($product->id === $productId) {
                    $product->qty += 1;
                }
            });
        }
        session()->save();

        return $this->sendResponse($cart,'Agregado con éxito,');
    }

    public function remove($productId) {
//        if ( ! request()->ajax()) {
//            abort(401, 'Acceso denegado');
//        }

        $cart = session('cart');

        $filtered = $cart->filter(function ($product) use ($productId) {
            if($product->id != $productId){
                return $product;
            }
        });

        session()->put('cart', $filtered);
        session()->save();

        return $this->sendResponse($cart,'Removido con éxito');
    }

    public function process () {
        if ( ! request()->ajax()) {
            abort(401, 'Acceso denegado');
        }
        $cart = session('cart');
        $success = true;
        $order = null;

        try {
            \DB::beginTransaction();
            $order = Order::create([
                'user_id' => auth()->id(),
                'total' => $cart->sum(function ($product) {
                    return $product->price * $product->qty;
                })
            ]);

            $orderLines = [];
            $cart->map(function ($product) use (&$orderLines, $order) {
                $orderLines[] = [
                    "order_id" => $order->id,
                    "product_id" => $product->id,
                    "product_price" => $product->price,
                    "qty" => $product->qty
                ];
            });
            OrderLine::insert($orderLines);

        } catch (\Exception $exception) {
            \DB::rollBack();
            $success = $exception->getMessage();
        }

        if ($success === true) {
            \DB::commit();
            session()->put('cart', new Collection);
            broadcast(new UserCart(session('cart')));
            broadcast(new NewOrder($order))->toOthers();
        }
        return response()->json($success);
    }

    public function actualizaCantidades($items=null)
    {
        $cantidades = [];

        foreach ($items as $index => $item) {
            $cantidades[$item['id']] = $item['qty'];
        }

        $carro = session('cart');

        if ($cantidades){
            $carro = $carro->map(function ($product) use ($cantidades) {
                $product->qty = $cantidades[$product->id];
                return $product;
            });

            session()->put('cart', $carro);
            session()->save();
        }

        return $cantidades;

    }
}
