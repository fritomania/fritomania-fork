<?php

namespace App\Http\Controllers;

use App\DataTables\GastoDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateGastoRequest;
use App\Http\Requests\UpdateGastoRequest;
use App\Models\Gasto;
use App\Repositories\GastoRepository;
use App\User;
use Flash;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class GastoController extends AppBaseController
{
    /** @var  GastoRepository */
    private $gastoRepository;

    public function __construct(GastoRepository $gastoRepo)
    {
        $this->middleware('auth');
        $this->gastoRepository = $gastoRepo;
    }

    /**
     * Display a listing of the Gasto.
     *
     * @param GastoDataTable $gastoDataTable
     * @return Response
     */
    public function index(GastoDataTable $gastoDataTable)
    {
        //si el usuario logeado no es admin
        if(! (Auth::user()->isAdmin()) ){
            flash('No tiene permiso para ingresar a esta opción')->error()->important();
            return redirect('dashboard');
        }

        return $gastoDataTable->render('gastos.index');
    }

    /**
     * Show the form for creating a new Gasto.
     *
     * @return Response
     */
    public function create()
    {
        //evita que guarde la misma url
        if(url()->previous()!=route('gastos.create')){
            //Guarda la url anterior en variable de session flash
            request()->session()->flash('backUrl',url()->previous());
        }

        return view('gastos.create');
    }

    /**
     * Store a newly created Gasto in storage.
     *
     * @param CreateGastoRequest $request
     *
     * @return Response
     */
    public function store(CreateGastoRequest $request)
    {

        //Si existe la variable de session flash mantenerla
        if($request->session()->has('backUrl')){
            $request->session()->keep('backUrl');
        }

        $input = $request->all();

        $gasto = $this->gastoRepository->create($input);

        Flash::success('Gasto guardado exitosamente.');

        $url = session('backUrl') == route('gastos.index') ? route('gastos.index') : session('backUrl');

        return redirect()->to($url);
    }

    /**
     * Display the specified Gasto.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $gasto = $this->gastoRepository->findWithoutFail($id);

        if (empty($gasto)) {
            Flash::error('Gasto no encontrado');

            return redirect(route('gastos.index'));
        }

        return view('gastos.show')->with('gasto', $gasto);
    }

    /**
     * Show the form for editing the specified Gasto.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $gasto = $this->gastoRepository->findWithoutFail($id);

        if (empty($gasto)) {
            Flash::error('Gasto no encontrado');

            return redirect(route('gastos.index'));
        }

        return view('gastos.edit')->with('gasto', $gasto);
    }

    /**
     * Update the specified Gasto in storage.
     *
     * @param  int              $id
     * @param UpdateGastoRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateGastoRequest $request)
    {
        $gasto = $this->gastoRepository->findWithoutFail($id);

        if (empty($gasto)) {
            Flash::error('Gasto no encontrado');

            return redirect(route('gastos.index'));
        }

        $gasto = $this->gastoRepository->update($request->all(), $id);

        Flash::success('Gasto actualizado exitosamente.');

        return redirect(route('gastos.index'));
    }

    /**
     * Remove the specified Gasto from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $gasto = $this->gastoRepository->findWithoutFail($id);

        if (empty($gasto)) {
            Flash::error('Gasto no encontrado');

            return redirect(route('gastos.index'));
        }

        $this->gastoRepository->delete($id);

        Flash::success('Gasto eliminado exitosamente.');

        return redirect(route('gastos.index'));
    }

    public function rpt(Request $request)
    {
        $del = $request->del;
        $al = $request->al;
        $user_id = $request->user_id;
        $descripcion = $request->descripcion;
        $eliminados = $request->eliminados;

        $between = $del && $al ? delAlBetween($del,$al) : mesActualBetween();

        $queryGastos = Gasto::whereBetween('created_at',$between)->orderBy('created_at','desc');

        if($user_id){
            $queryGastos->where('user_id',[$user_id]);
        }

        if($descripcion){
            $queryGastos->where('descripcion', 'like', "%{$descripcion}%");
        }

        if($eliminados){
            $queryGastos->onlyTrashed();
        }

        $gastos = $queryGastos->get();

        $users = User::pluck('name','id');

        $users = array_prepend( $users->toArray(),'Todos','');

        $total= array_sum(array_pluck($gastos,'monto'));

        return view('gastos.rpt',compact('gastos','total','users','user_id'));
    }
}
