<?php

namespace App\Http\Controllers;

use App\Models\Cliente;
use Illuminate\Http\Request;

class AccountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cliente = Cliente::find(3);

        return view('delivery.account',['cliente' => $cliente]);
    }

    public function ejemplo()
    {
        return view('ejemplo');
    }
}
