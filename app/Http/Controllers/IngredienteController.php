<?php

namespace App\Http\Controllers;

use App\DataTables\IngredienteDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateIngredienteRequest;
use App\Http\Requests\UpdateIngredienteRequest;
use App\Repositories\IngredienteRepository;
use DB;
use Exception;
use File;
use Flash;
use App\Http\Controllers\AppBaseController;
use Illuminate\Support\Facades\Storage;
use Response;

class IngredienteController extends AppBaseController
{
    /** @var  IngredienteRepository */
    private $ingredienteRepository;

    public function __construct(IngredienteRepository $ingredienteRepo)
    {
        $this->ingredienteRepository = $ingredienteRepo;
        $this->middleware('auth');
    }

    /**
     * Display a listing of the Ingrediente.
     *
     * @param IngredienteDataTable $ingredienteDataTable
     * @return Response
     */
    public function index(IngredienteDataTable $ingredienteDataTable)
    {
        return $ingredienteDataTable->render('ingredientes.index');
    }

    /**
     * Show the form for creating a new Ingrediente.
     *
     * @return Response
     */
    public function create()
    {
        return view('ingredientes.create');
    }

    /**
     * Store a newly created Ingrediente in storage.
     *
     * @param CreateIngredienteRequest $request
     *
     * @return Response
     */
    public function store(CreateIngredienteRequest $request)
    {


        try {
            DB::beginTransaction();

            if ($request->has('file')){
                $file = $request->file('file');

                $file->store('ingredientes');

                $request->merge(['imagen' => $file->hashName()]);
            }

            $input = $request->all();

            $ingrediente = $this->ingredienteRepository->create($input);


        } catch (\Exception $exception) {
            DB::rollBack();
            $msg = $exception->getMessage();

            flash('Erro: '.$msg)->error()->important();

            return redirect()->back();

        }

        DB::commit();


        Flash::success('Ingrediente guardado exitosamente.');

        return redirect(route('ingredientes.index'));
    }

    /**
     * Display the specified Ingrediente.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $ingrediente = $this->ingredienteRepository->findWithoutFail($id);

        if (empty($ingrediente)) {
            Flash::error('Ingrediente no encontrado');

            return redirect(route('ingredientes.index'));
        }

        return view('ingredientes.show',compact('ingrediente'));
    }

    /**
     * Show the form for editing the specified Ingrediente.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $ingrediente = $this->ingredienteRepository->findWithoutFail($id);

        if (empty($ingrediente)) {
            Flash::error('Ingrediente no encontrado');

            return redirect(route('ingredientes.index'));
        }

        return view('ingredientes.edit',compact('ingrediente'));
    }

    /**
     * Update the specified Ingrediente in storage.
     *
     * @param  int              $id
     * @param UpdateIngredienteRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateIngredienteRequest $request)
    {
        $ingrediente = $this->ingredienteRepository->findWithoutFail($id);

        if (empty($ingrediente)) {
            Flash::error('Ingrediente no encontrado');

            return redirect(route('ingredientes.index'));
        }



        try {
            DB::beginTransaction();

            if ($request->has('file')){
                $file = $request->file('file');

                $file->store('ingredientes');

                $request->merge(['imagen' => $file->hashName()]);

                Storage::delete('ingredientes/'.$ingrediente->imagen);

            }

            $input = $request->all();

            $ingrediente = $this->ingredienteRepository->update($input, $id);


        } catch (\Exception $exception) {
            DB::rollBack();
            $msg = $exception->getMessage();

            flash('Erro: '.$msg)->error()->important();

            return redirect()->back();

        }

        DB::commit();


        Flash::success('Ingrediente guardado exitosamente.');

        return redirect(route('ingredientes.index'));

        Flash::success('Ingrediente actualizado exitosamente.');

        return redirect(route('ingredientes.index'));
    }

    /**
     * Remove the specified Ingrediente from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $ingrediente = $this->ingredienteRepository->findWithoutFail($id);


//        dd(Storage::delete('ingredientes/'.$ingrediente->imagen));

        if (empty($ingrediente)) {
            Flash::error('Ingrediente no encontrado');

            return redirect(route('ingredientes.index'));
        }


        try {
            DB::beginTransaction();

            if(!Storage::delete('ingredientes/'.$ingrediente->imagen)){
                throw new Exception('Error al eliminar el archivo');
            };

            $this->ingredienteRepository->delete($id);

        } catch (\Exception $exception) {
            DB::rollBack();
            $msg = $exception->getMessage();

            flash('Erro: '.$msg)->error()->important();

            return redirect()->back();
        }

        DB::commit();

        Flash::success('Ingrediente eliminado exitosamente.');

        return redirect(route('ingredientes.index'));
    }
}
