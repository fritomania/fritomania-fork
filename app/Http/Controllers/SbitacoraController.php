<?php

namespace App\Http\Controllers;

use App\DataTables\SbitacoraDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateSbitacoraRequest;
use App\Http\Requests\UpdateSbitacoraRequest;
use App\Repositories\SbitacoraRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class SbitacoraController extends AppBaseController
{
    /** @var  SbitacoraRepository */
    private $sbitacoraRepository;

    public function __construct(SbitacoraRepository $sbitacoraRepo)
    {
        $this->sbitacoraRepository = $sbitacoraRepo;
        $this->middleware('auth');
    }

    /**
     * Display a listing of the Sbitacora.
     *
     * @param SbitacoraDataTable $sbitacoraDataTable
     * @return Response
     */
    public function index(SbitacoraDataTable $sbitacoraDataTable)
    {
        return $sbitacoraDataTable->render('sbitacoras.index');
    }

    /**
     * Show the form for creating a new Sbitacora.
     *
     * @return Response
     */
    public function create()
    {
        return view('sbitacoras.create');
    }

    /**
     * Store a newly created Sbitacora in storage.
     *
     * @param CreateSbitacoraRequest $request
     *
     * @return Response
     */
    public function store(CreateSbitacoraRequest $request)
    {
        $input = $request->all();

        $sbitacora = $this->sbitacoraRepository->create($input);

        Flash::success('Sbitacora guardado exitosamente.');

        return redirect(route('sbitacoras.index'));
    }

    /**
     * Display the specified Sbitacora.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $sbitacora = $this->sbitacoraRepository->findWithoutFail($id);

        if (empty($sbitacora)) {
            Flash::error('Sbitacora no encontrado');

            return redirect(route('sbitacoras.index'));
        }

        return view('sbitacoras.show')->with('sbitacora', $sbitacora);
    }

    /**
     * Show the form for editing the specified Sbitacora.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $sbitacora = $this->sbitacoraRepository->findWithoutFail($id);

        if (empty($sbitacora)) {
            Flash::error('Sbitacora no encontrado');

            return redirect(route('sbitacoras.index'));
        }

        return view('sbitacoras.edit')->with('sbitacora', $sbitacora);
    }

    /**
     * Update the specified Sbitacora in storage.
     *
     * @param  int              $id
     * @param UpdateSbitacoraRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSbitacoraRequest $request)
    {
        $sbitacora = $this->sbitacoraRepository->findWithoutFail($id);

        if (empty($sbitacora)) {
            Flash::error('Sbitacora no encontrado');

            return redirect(route('sbitacoras.index'));
        }

        $sbitacora = $this->sbitacoraRepository->update($request->all(), $id);

        Flash::success('Sbitacora actualizado exitosamente.');

        return redirect(route('sbitacoras.index'));
    }

    /**
     * Remove the specified Sbitacora from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $sbitacora = $this->sbitacoraRepository->findWithoutFail($id);

        if (empty($sbitacora)) {
            Flash::error('Sbitacora no encontrado');

            return redirect(route('sbitacoras.index'));
        }

        $this->sbitacoraRepository->delete($id);

        Flash::success('Sbitacora eliminado exitosamente.');

        return redirect(route('sbitacoras.index'));
    }
}
