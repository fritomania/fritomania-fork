<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateStipoAPIRequest;
use App\Http\Requests\API\UpdateStipoAPIRequest;
use App\Models\Stipo;
use App\Repositories\StipoRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class StipoController
 * @package App\Http\Controllers\API
 */

class StipoAPIController extends AppBaseController
{
    /** @var  StipoRepository */
    private $stipoRepository;

    public function __construct(StipoRepository $stipoRepo)
    {
        $this->stipoRepository = $stipoRepo;
    }

    /**
     * Display a listing of the Stipo.
     * GET|HEAD /stipos
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->stipoRepository->pushCriteria(new RequestCriteria($request));
        $this->stipoRepository->pushCriteria(new LimitOffsetCriteria($request));
        $stipos = $this->stipoRepository->all();

        return $this->sendResponse($stipos->toArray(), 'Stipos retrieved successfully');
    }

    /**
     * Store a newly created Stipo in storage.
     * POST /stipos
     *
     * @param CreateStipoAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateStipoAPIRequest $request)
    {
        $input = $request->all();

        $stipos = $this->stipoRepository->create($input);

        return $this->sendResponse($stipos->toArray(), 'Stipo saved successfully');
    }

    /**
     * Display the specified Stipo.
     * GET|HEAD /stipos/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Stipo $stipo */
        $stipo = $this->stipoRepository->findWithoutFail($id);

        if (empty($stipo)) {
            return $this->sendError('Stipo not found');
        }

        return $this->sendResponse($stipo->toArray(), 'Stipo retrieved successfully');
    }

    /**
     * Update the specified Stipo in storage.
     * PUT/PATCH /stipos/{id}
     *
     * @param  int $id
     * @param UpdateStipoAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateStipoAPIRequest $request)
    {
        $input = $request->all();

        /** @var Stipo $stipo */
        $stipo = $this->stipoRepository->findWithoutFail($id);

        if (empty($stipo)) {
            return $this->sendError('Stipo not found');
        }

        $stipo = $this->stipoRepository->update($input, $id);

        return $this->sendResponse($stipo->toArray(), 'Stipo updated successfully');
    }

    /**
     * Remove the specified Stipo from storage.
     * DELETE /stipos/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Stipo $stipo */
        $stipo = $this->stipoRepository->findWithoutFail($id);

        if (empty($stipo)) {
            return $this->sendError('Stipo not found');
        }

        $stipo->delete();

        return $this->sendResponse($id, 'Stipo deleted successfully');
    }
}
