<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateIngredienteAPIRequest;
use App\Http\Requests\API\UpdateIngredienteAPIRequest;
use App\Models\Ingrediente;
use App\Repositories\IngredienteRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class IngredienteController
 * @package App\Http\Controllers\API
 */

class IngredienteAPIController extends AppBaseController
{
    /** @var  IngredienteRepository */
    private $ingredienteRepository;

    public function __construct(IngredienteRepository $ingredienteRepo)
    {
        $this->ingredienteRepository = $ingredienteRepo;
    }

    /**
     * Display a listing of the Ingrediente.
     * GET|HEAD /ingredientes
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ingredienteRepository->pushCriteria(new RequestCriteria($request));
        $this->ingredienteRepository->pushCriteria(new LimitOffsetCriteria($request));
        $ingredientes = $this->ingredienteRepository->all();

        return $this->sendResponse($ingredientes->toArray(), 'Ingredientes recuperado con éxito');
    }

    /**
     * Store a newly created Ingrediente in storage.
     * POST /ingredientes
     *
     * @param CreateIngredienteAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateIngredienteAPIRequest $request)
    {
        $input = $request->all();

        $ingredientes = $this->ingredienteRepository->create($input);

        return $this->sendResponse($ingredientes->toArray(), 'Ingrediente guardado exitosamente');
    }

    /**
     * Display the specified Ingrediente.
     * GET|HEAD /ingredientes/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Ingrediente $ingrediente */
        $ingrediente = $this->ingredienteRepository->findWithoutFail($id);

        if (empty($ingrediente)) {
            return $this->sendError('Ingrediente no encontrado');
        }

        return $this->sendResponse($ingrediente->toArray(), 'Ingrediente recuperado con éxito');
    }

    /**
     * Update the specified Ingrediente in storage.
     * PUT/PATCH /ingredientes/{id}
     *
     * @param  int $id
     * @param UpdateIngredienteAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateIngredienteAPIRequest $request)
    {
        $input = $request->all();

        /** @var Ingrediente $ingrediente */
        $ingrediente = $this->ingredienteRepository->findWithoutFail($id);

        if (empty($ingrediente)) {
            return $this->sendError('Ingrediente no encontrado');
        }

        $ingrediente = $this->ingredienteRepository->update($input, $id);

        return $this->sendResponse($ingrediente->toArray(), 'Ingrediente actualizado exitosamente');
    }

    /**
     * Remove the specified Ingrediente from storage.
     * DELETE /ingredientes/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Ingrediente $ingrediente */
        $ingrediente = $this->ingredienteRepository->findWithoutFail($id);

        if (empty($ingrediente)) {
            return $this->sendError('Ingrediente no encontrado');
        }

        $ingrediente->delete();

        return $this->sendResponse($id, 'Ingrediente eliminado exitosamente');
    }
}
