<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateCajaAPIRequest;
use App\Http\Requests\API\UpdateCajaAPIRequest;
use App\Models\Caja;
use App\Repositories\CajaRepository;
use App\Repositories\UserRepository;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class CajaController
 * @package App\Http\Controllers\API
 */

class CajaAPIController extends AppBaseController
{
    /** @var  CajaRepository */
    private $cajaRepository;
    private $userRepository;

    public function __construct(CajaRepository $cajaRepo,UserRepository $userRepo)
    {
        $this->cajaRepository = $cajaRepo;
        $this->userRepository = $userRepo;
    }

    /**
     * Display a listing of the Caja.
     * GET|HEAD /cajas
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        if(isset($request->fecha)){
            $cajas = Caja::deFecha($request->fecha)->get();
        }else{
            $this->cajaRepository->pushCriteria(new RequestCriteria($request));
            $this->cajaRepository->pushCriteria(new LimitOffsetCriteria($request));
            $cajas = $this->cajaRepository->all();
        }

        return $this->sendResponse($cajas->toArray(), 'Cajas recuperado exitosamente');
    }

    /**
     * Store a newly created Caja in storage.
     * POST /cajas
     *
     * @param CreateCajaAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateCajaAPIRequest $request)
    {
        $input = $request->all();

        $cajas = $this->cajaRepository->create($input);

        return $this->sendResponse($cajas->toArray(), 'Caja abierta exitosamente');
    }

    /**
     * Display the specified Caja.
     * GET|HEAD /cajas/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Caja $caja */
        $caja = $this->cajaRepository->findWithoutFail($id);

        if (empty($caja)) {
            return $this->sendError('Caja no existe');
        }

        return $this->sendResponse($caja->toArray(), 'Caja recuperado exitosamente');
    }

    /**
     * Update the specified Caja in storage.
     * PUT/PATCH /cajas/{id}
     *
     * @param  int $id
     * @param UpdateCajaAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCajaAPIRequest $request)
    {
        $input = $request->all();

        /** @var Caja $caja */
        $caja = $this->cajaRepository->findWithoutFail($id);

        if (empty($caja)) {
            return $this->sendError('Caja no existe');
        }

        $caja = $this->cajaRepository->update($input, $id);

        return $this->sendResponse($caja->toArray(), 'Caja actualizado exitosamente');
    }

    /**
     * Remove the specified Caja from storage.
     * DELETE /cajas/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Caja $caja */
        $caja = $this->cajaRepository->findWithoutFail($id);

        if (empty($caja)) {
            return $this->sendError('Caja no existe');
        }

        $caja->delete();

        return $this->sendResponse($id, 'Caja eliminado exitosamente');
    }

    public function cajaAbierta($id){

        $user = $this->userRepository->findWithoutFail($id);

        if (empty($user)) {
            return $this->sendError('Usuario no encontrado');
        }

        if (!$user->cajaAbierta()){
            return $this->sendError('Caja no abierta');
        }

        return $this->sendResponse($user->cajaAbierta(),'Caja devuelta');
    }

}
