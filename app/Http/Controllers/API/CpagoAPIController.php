<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateCpagoAPIRequest;
use App\Http\Requests\API\UpdateCpagoAPIRequest;
use App\Models\Compra;
use App\Models\Cpago;
use App\Repositories\CompraRepository;
use App\Repositories\CpagoRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class CpagoController
 * @package App\Http\Controllers\API
 */

class CpagoAPIController extends AppBaseController
{
    /** @var  CpagoRepository */
    private $cpagoRepository;
    private $compraRepository;

    public function __construct(CpagoRepository $cpagoRepo,CompraRepository $compraRepo)
    {
        $this->cpagoRepository = $cpagoRepo;
        $this->compraRepository = $compraRepo;
    }

    /**
     * Display a listing of the Cpago.
     * GET|HEAD /cpagos
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        if(isset($request->compra_id)){
            $compra = Compra::find($request->compra_id);
            $cpagos = $compra->cpagos;
            $cpagos = $cpagos->map(function ($item){
                $item->username = $item->user->name;
                unset($item->user);
                return $item;
            });

        }else {

            $this->cpagoRepository->pushCriteria(new RequestCriteria($request));
            $this->cpagoRepository->pushCriteria(new LimitOffsetCriteria($request));
            $cpagos = $this->cpagoRepository->all();
        }

        return $this->sendResponse($cpagos->toArray(), 'Pago de compras recuperado exitosamente');
    }

    /**
     * Store a newly created Cpago in storage.
     * POST /cpagos
     *
     * @param CreateCpagoAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateCpagoAPIRequest $request)
    {
        $compra = $this->compraRepository->findWithoutFail($request->compra_id);

        if (empty($compra)) {
            return $this->sendError('Compra no encontrada');
        }

        $totalPagos = array_sum(array_pluck($compra->cpagos->toArray(),'monto')) ;
        $saldo = $compra->total - $totalPagos;

        if($request->monto > $saldo){
            return $this->sendError('El monto supera el saldo pendiente');
        }

        $input = $request->all();

        $cpago = $this->cpagoRepository->create($input);

        if ($cpago->monto==$saldo){
            $compra->pagada =1;
            $compra->save();
            return $this->sendResponse($cpago->toArray(), 'Compra pagada');
        }

        return $this->sendResponse($cpago->toArray(), 'Pago de compra guardado exitosamente');
    }

    /**
     * Display the specified Cpago.
     * GET|HEAD /cpagos/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Cpago $cpago */
        $cpago = $this->cpagoRepository->findWithoutFail($id);

        if (empty($cpago)) {
            return $this->sendError('Pago de compra no encontrado');
        }

        return $this->sendResponse($cpago->toArray(), 'Pago de compra recuperado exitosamente');
    }

    /**
     * Update the specified Cpago in storage.
     * PUT/PATCH /cpagos/{id}
     *
     * @param  int $id
     * @param UpdateCpagoAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCpagoAPIRequest $request)
    {
        $input = $request->all();

        /** @var Cpago $cpago */
        $cpago = $this->cpagoRepository->findWithoutFail($id);

        if (empty($cpago)) {
            return $this->sendError('Pago de compra no encontrado');
        }

        $cpago = $this->cpagoRepository->update($input, $id);

        return $this->sendResponse($cpago->toArray(), 'Pago de compra actualizado exitosamente');
    }

    /**
     * Remove the specified Cpago from storage.
     * DELETE /cpagos/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Cpago $cpago */
        $cpago = $this->cpagoRepository->findWithoutFail($id);

        if (empty($cpago)) {
            return $this->sendError('Pago de compra no encontrado');
        }

        $cpago->delete();

        return $this->sendResponse($id, 'Pago de compra eliminado exitosamente');
    }
}
