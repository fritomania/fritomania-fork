<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateTempCompraDetalleAPIRequest;
use App\Http\Requests\API\UpdateTempCompraDetalleAPIRequest;
use App\Models\TempCompraDetalle;
use App\Repositories\TempCompraDetalleRepository;
use App\Repositories\TempCompraRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class TempCompraDetalleController
 * @package App\Http\Controllers\API
 */

class TempCompraDetalleAPIController extends AppBaseController
{
    /** @var  TempCompraDetalleRepository */
    private $tempCompraDetalleRepository;
    private $tempCompraRepository;

    public function __construct(TempCompraDetalleRepository $tempCompraDetalleRepo,TempCompraRepository $tempCompraRepo)
    {
        $this->tempCompraDetalleRepository = $tempCompraDetalleRepo;
        $this->tempCompraRepository = $tempCompraRepo;
    }

    /**
     * Display a listing of the TempCompraDetalle.
     * GET|HEAD /tempCompraDetalles
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->tempCompraDetalleRepository->pushCriteria(new RequestCriteria($request));
        $this->tempCompraDetalleRepository->pushCriteria(new LimitOffsetCriteria($request));
        $tempCompraDetalles = $this->tempCompraDetalleRepository->all();

        return $this->sendResponse($tempCompraDetalles->toArray(), 'Detalle de compras recuperado exitosamente');
    }

    /**
     * Store a newly created TempCompraDetalle in storage.
     * POST /tempCompraDetalles
     *
     * @param CreateTempCompraDetalleAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateTempCompraDetalleAPIRequest $request)
    {
        $input = $request->all();

        $tempCompraDetalles = $this->tempCompraDetalleRepository->create($input);

        $tempCompraDetalles->item;
        $tempCompraDetalles->item->marca;


        return $this->sendResponse($tempCompraDetalles->toArray(), 'Detalle de compra guardado exitosamente');
    }

    /**
     * Display the specified TempCompraDetalle.
     * GET|HEAD /tempCompraDetalles/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var TempCompraDetalle $tempCompraDetalle */
        $tempCompraDetalle = $this->tempCompraDetalleRepository->findWithoutFail($id);

        if (empty($tempCompraDetalle)) {
            return $this->sendError('Detalle de compra no existe');
        }

        return $this->sendResponse($tempCompraDetalle->toArray(), 'Detalle de compra recuperado exitosamente');
    }

    /**
     * Update the specified TempCompraDetalle in storage.
     * PUT/PATCH /tempCompraDetalles/{id}
     *
     * @param  int $id
     * @param UpdateTempCompraDetalleAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateTempCompraDetalleAPIRequest $request)
    {
        $input = $request->all();

        /** @var TempCompraDetalle $tempCompraDetalle */
        $tempCompraDetalle = $this->tempCompraDetalleRepository->findWithoutFail($id);

        if (empty($tempCompraDetalle)) {
            return $this->sendError('Detalle de compra no existe');
        }

        $tempCompraDetalle = $this->tempCompraDetalleRepository->update($input, $id);

        return $this->sendResponse($tempCompraDetalle->toArray(), 'TempCompraDetalle actualizado exitosamente');
    }

    /**
     * Remove the specified TempCompraDetalle from storage.
     * DELETE /tempCompraDetalles/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var TempCompraDetalle $tempCompraDetalle */
        $tempCompraDetalle = $this->tempCompraDetalleRepository->findWithoutFail($id);

        if (empty($tempCompraDetalle)) {
            return $this->sendError('Detalle de compra no existe');
        }

        $tempCompraDetalle->delete();

        return $this->sendResponse($id, 'Detalle de compra eliminado exitosamente');
    }

    public function detallesCompra($id){
        /** @var TempCompraDetalle $tempCompraDetalle */
        $tempCompra = $this->tempCompraRepository->findWithoutFail($id);

        if (empty($tempCompra)) {
            return $this->sendError('No se encuentra la compra temporal');
        }

        $detalles = $tempCompra->tempCompraDetalles->map(function ($det){
            $det->item;
            $det->fecha_ven = fecha($det->fecha_ven);
            return $det;
        });


        return $this->sendResponse($tempCompra->tempCompraDetalles->toArray(), 'Detalles de compra temporal');
    }
}
