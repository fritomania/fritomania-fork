<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateGastoAPIRequest;
use App\Http\Requests\API\UpdateGastoAPIRequest;
use App\Models\Gasto;
use App\Repositories\GastoRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class GastoController
 * @package App\Http\Controllers\API
 */

class GastoAPIController extends AppBaseController
{
    /** @var  GastoRepository */
    private $gastoRepository;

    public function __construct(GastoRepository $gastoRepo)
    {
        $this->gastoRepository = $gastoRepo;
    }

    /**
     * Display a listing of the Gasto.
     * GET|HEAD /gastos
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->gastoRepository->pushCriteria(new RequestCriteria($request));
        $this->gastoRepository->pushCriteria(new LimitOffsetCriteria($request));
        $gastos = $this->gastoRepository->all();

        return $this->sendResponse($gastos->toArray(), 'Gastos retrieved successfully');
    }

    /**
     * Store a newly created Gasto in storage.
     * POST /gastos
     *
     * @param CreateGastoAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateGastoAPIRequest $request)
    {
        $input = $request->all();

        $gastos = $this->gastoRepository->create($input);

        return $this->sendResponse($gastos->toArray(), 'Gasto saved successfully');
    }

    /**
     * Display the specified Gasto.
     * GET|HEAD /gastos/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Gasto $gasto */
        $gasto = $this->gastoRepository->findWithoutFail($id);

        if (empty($gasto)) {
            return $this->sendError('Gasto not found');
        }

        return $this->sendResponse($gasto->toArray(), 'Gasto retrieved successfully');
    }

    /**
     * Update the specified Gasto in storage.
     * PUT/PATCH /gastos/{id}
     *
     * @param  int $id
     * @param UpdateGastoAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateGastoAPIRequest $request)
    {
        $input = $request->all();

        /** @var Gasto $gasto */
        $gasto = $this->gastoRepository->findWithoutFail($id);

        if (empty($gasto)) {
            return $this->sendError('Gasto not found');
        }

        $gasto = $this->gastoRepository->update($input, $id);

        return $this->sendResponse($gasto->toArray(), 'Gasto updated successfully');
    }

    /**
     * Remove the specified Gasto from storage.
     * DELETE /gastos/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Gasto $gasto */
        $gasto = $this->gastoRepository->findWithoutFail($id);

        if (empty($gasto)) {
            return $this->sendError('Gasto not found');
        }

        $gasto->delete();

        return $this->sendResponse($id, 'Gasto deleted successfully');
    }
}
