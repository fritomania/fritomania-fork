<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateTempSolicitudDetalleAPIRequest;
use App\Http\Requests\API\UpdateTempSolicitudDetalleAPIRequest;
use App\Models\TempSolicitudDetalle;
use App\Repositories\TempSolicitudDetalleRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class TempSolicitudDetalleController
 * @package App\Http\Controllers\API
 */

class TempSolicitudDetalleAPIController extends AppBaseController
{
    /** @var  TempSolicitudDetalleRepository */
    private $tempSolicitudDetalleRepository;

    public function __construct(TempSolicitudDetalleRepository $tempSolicitudDetalleRepo)
    {
        $this->tempSolicitudDetalleRepository = $tempSolicitudDetalleRepo;
    }

    /**
     * Display a listing of the TempSolicitudDetalle.
     * GET|HEAD /tempSolicitudDetalles
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        if($request->temp_solicitude_id){
            $tempSolicitudDetalles = TempSolicitudDetalle::where('temp_solicitude_id',$request->temp_solicitude_id)->get();
        }else{
            $this->tempSolicitudDetalleRepository->pushCriteria(new RequestCriteria($request));
            $this->tempSolicitudDetalleRepository->pushCriteria(new LimitOffsetCriteria($request));
            $tempSolicitudDetalles = $this->tempSolicitudDetalleRepository->all();
        }

        return $this->sendResponse($tempSolicitudDetalles->toArray(), 'Detalles recuperado con éxito');
    }

    /**
     * Store a newly created TempSolicitudDetalle in storage.
     * POST /tempSolicitudDetalles
     *
     * @param CreateTempSolicitudDetalleAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateTempSolicitudDetalleAPIRequest $request)
    {
        $input = $request->all();

        $tempSolicitudDetalles = $this->tempSolicitudDetalleRepository->create($input);

        return $this->sendResponse($tempSolicitudDetalles->toArray(), 'Detalle guardado exitosamente');
    }

    /**
     * Display the specified TempSolicitudDetalle.
     * GET|HEAD /tempSolicitudDetalles/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var TempSolicitudDetalle $tempSolicitudDetalle */
        $tempSolicitudDetalle = $this->tempSolicitudDetalleRepository->findWithoutFail($id);

        if (empty($tempSolicitudDetalle)) {
            return $this->sendError('Detalle no encontrado');
        }

        return $this->sendResponse($tempSolicitudDetalle->toArray(), 'Detalle recuperado con éxito');
    }

    /**
     * Update the specified TempSolicitudDetalle in storage.
     * PUT/PATCH /tempSolicitudDetalles/{id}
     *
     * @param  int $id
     * @param UpdateTempSolicitudDetalleAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateTempSolicitudDetalleAPIRequest $request)
    {
        $input = $request->all();

        /** @var TempSolicitudDetalle $tempSolicitudDetalle */
        $tempSolicitudDetalle = $this->tempSolicitudDetalleRepository->findWithoutFail($id);

        if (empty($tempSolicitudDetalle)) {
            return $this->sendError('Detalle no encontrado');
        }

        $tempSolicitudDetalle = $this->tempSolicitudDetalleRepository->update($input, $id);

        return $this->sendResponse($tempSolicitudDetalle->toArray(), 'Detalle actualizado exitosamente');
    }

    /**
     * Remove the specified TempSolicitudDetalle from storage.
     * DELETE /tempSolicitudDetalles/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var TempSolicitudDetalle $tempSolicitudDetalle */
        $tempSolicitudDetalle = $this->tempSolicitudDetalleRepository->findWithoutFail($id);

        if (empty($tempSolicitudDetalle)) {
            return $this->sendError('Detalle no encontrado');
        }

        $tempSolicitudDetalle->delete();

        return $this->sendResponse($id, 'Detalle eliminado exitosamente');
    }

}
