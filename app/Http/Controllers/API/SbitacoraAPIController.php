<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateSbitacoraAPIRequest;
use App\Http\Requests\API\UpdateSbitacoraAPIRequest;
use App\Models\Sbitacora;
use App\Repositories\SbitacoraRepository;
use App\Repositories\ServicioRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class SbitacoraController
 * @package App\Http\Controllers\API
 */

class SbitacoraAPIController extends AppBaseController
{
    /** @var  SbitacoraRepository */
    private $sbitacoraRepository;
    private $servicioRepository;

    public function __construct(SbitacoraRepository $sbitacoraRepo,ServicioRepository $servicioRepo)
    {
        $this->sbitacoraRepository = $sbitacoraRepo;
        $this->servicioRepository = $servicioRepo;
    }

    /**
     * Display a listing of the Sbitacora.
     * GET|HEAD /sbitacoras
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->sbitacoraRepository->pushCriteria(new RequestCriteria($request));
        $this->sbitacoraRepository->pushCriteria(new LimitOffsetCriteria($request));
        $sbitacoras = $this->sbitacoraRepository->all();

        return $this->sendResponse($sbitacoras->toArray(), 'Anotaciones recuperadas exitosamente');
    }

    /**
     * Store a newly created Sbitacora in storage.
     * POST /sbitacoras
     *
     * @param CreateSbitacoraAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateSbitacoraAPIRequest $request)
    {
        $input = $request->all();

        $sbitacoras = $this->sbitacoraRepository->create($input);

        return $this->sendResponse($sbitacoras->toArray(), 'Anotacíon guardada exitosamente!');
    }

    /**
     * Display the specified Sbitacora.
     * GET|HEAD /sbitacoras/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Sbitacora $sbitacora */
        $sbitacora = $this->sbitacoraRepository->findWithoutFail($id);

        if (empty($sbitacora)) {
            return $this->sendError('Anotación no encontrada');
        }

        return $this->sendResponse($sbitacora->toArray(), 'Anotacíon recuperada exitosamente');
    }

    /**
     * Update the specified Sbitacora in storage.
     * PUT/PATCH /sbitacoras/{id}
     *
     * @param  int $id
     * @param UpdateSbitacoraAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSbitacoraAPIRequest $request)
    {
        $input = $request->all();

        /** @var Sbitacora $sbitacora */
        $sbitacora = $this->sbitacoraRepository->findWithoutFail($id);

        if (empty($sbitacora)) {
            return $this->sendError('Anotación no encontrada');
        }

        $sbitacora = $this->sbitacoraRepository->update($input, $id);

        return $this->sendResponse($sbitacora->toArray(), 'Anotacíon actualizada exitosamente!');
    }

    /**
     * Remove the specified Sbitacora from storage.
     * DELETE /sbitacoras/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Sbitacora $sbitacora */
        $sbitacora = $this->sbitacoraRepository->findWithoutFail($id);

        if (empty($sbitacora)) {
            return $this->sendError('Anotación no encontrada');
        }

        $sbitacora->delete();

        return $this->sendResponse($id, 'Anotacíon eliminada exitosamente!');
    }

    public function bitacoraServicio($id)
    {
        $servicio = $this->servicioRepository->findWithoutFail($id);

        if (empty($servicio)) {
            return $this->sendError('Servicio no encontrado');
        }

        $notas = $servicio->bitacora->map(function ($item){
            //$item->nombre_marca = $item->marca ? $item->marca->nombre : null;
            $item->sestado;
            $item->user;
            return $item;
        });




        return $this->sendResponse($notas, 'Notas de servicio');
    }
}
