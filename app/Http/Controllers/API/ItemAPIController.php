<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateItemAPIRequest;
use App\Http\Requests\API\UpdateItemAPIRequest;
use App\Models\Item;
use App\Models\Tienda;
use App\Repositories\ItemRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class ItemController
 * @package App\Http\Controllers\API
 */

class ItemAPIController extends AppBaseController
{
    /** @var  ItemRepository */
    private $itemRepository;

    public function __construct(ItemRepository $itemRepo)
    {
        $this->itemRepository = $itemRepo;
    }

    /**
     * Display a listing of the Item.
     * GET|HEAD /items
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
//        return $this->sendResponse($request->all(),'request');
        $this->itemRepository->pushCriteria(new RequestCriteria($request));
        $this->itemRepository->pushCriteria(new LimitOffsetCriteria($request));
        $items = $this->itemRepository->all();

        $items = $items->map(function ($item){
            $item->nombre_marca = $item->marca ? $item->marca->nombre : null;
            if(request()->tienda){
                $item->stock_tienda = $item->stockTienda(request()->tienda);
            }
            unset($item->marca );
            return $item;
        });

        if ($request->tienda){
            if ($request->tienda==Tienda::BODEGA_PRODUCCION || $request->tienda==Tienda::BODEGA_MP){
                $items = $items->filter->materia_prima;
            }else{
                $items = $items->filter->catProducto();
            }
        }


        $result=[
            'success' => true,
            'rows' => count($items),
            'data'    => $items,
            'message' => 'Items recuperado exitosamente',
        ];

        return Response::json($result);
    }

    /**
     * Store a newly created Item in storage.
     * POST /items
     *
     * @param CreateItemAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateItemAPIRequest $request)
    {
        $input = $request->all();

        $items = $this->itemRepository->create($input);

        return $this->sendResponse($items->toArray(), 'Item guardado exitosamente');
    }

    /**
     * Display the specified Item.
     * GET|HEAD /items/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Item $item */
        $item = $this->itemRepository->findWithoutFail($id);

        if (empty($item)) {
            return $this->sendError('Item no existe');
        }

        return $this->sendResponse($item->toArray(), 'Item recuperado exitosamente');
    }

    /**
     * Update the specified Item in storage.
     * PUT/PATCH /items/{id}
     *
     * @param  int $id
     * @param UpdateItemAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateItemAPIRequest $request)
    {
        $input = $request->all();

        /** @var Item $item */
        $item = $this->itemRepository->findWithoutFail($id);

        if (empty($item)) {
            return $this->sendError('Item no existe');
        }

        $item = $this->itemRepository->update($input, $id);

        return $this->sendResponse($item->toArray(), 'Item actualizado exitosamente');
    }

    /**
     * Remove the specified Item from storage.
     * DELETE /items/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Item $item */
        $item = $this->itemRepository->findWithoutFail($id);

        if (empty($item)) {
            return $this->sendError('Item no existe');
        }

        $item->delete();

        return $this->sendResponse($id, 'Item eliminado exitosamente');
    }

    public function showByCod(Request $request)
    {

        /** @var Item $item */
        $item = $this->itemRepository->findWhere(['codigo'=>$request->codigo]);

        if ($item->count()==0) {
            return $this->sendError('Item no existe');
        }

        $item = $item->toArray();

        return $this->sendResponse($item[0], 'Item recuperado exitosamente');
    }
}
