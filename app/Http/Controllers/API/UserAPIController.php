<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateUserAPIRequest;
use App\Http\Requests\API\UpdateUserAPIRequest;
use App\User;
use App\Repositories\UserRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class UserController
 * @package App\Http\Controllers\API
 */

class UserAPIController extends AppBaseController
{
    /** @var  UserRepository */
    private $userRepository;

    public function __construct(UserRepository $userRepo)
    {
        $this->userRepository = $userRepo;
    }

    /**
     * Display a listing of the User.
     * GET|HEAD /users
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        if($request->flag == 'user_despacha_user'){
            $user = \App\User::find($request->user_id);
            $asignados = $user->usersDespacha;
            $notIn = $asignados->pluck('id')->toArray();


            $isSuper = User::find($request->user_id)->isSuperAdmin();

            if(!$isSuper){

                $notIn[] = $request->user_id;
            }

            $noasig = User::whereNotIn('id',$notIn)->get();


            $datos = [
                'asignados' => $asignados->toArray(),
                'no_asignados' => $noasig->toArray()
            ];

            return $this->sendResponse($datos, 'Users asignados');
        }else{

            $this->userRepository->pushCriteria(new RequestCriteria($request));
            $this->userRepository->pushCriteria(new LimitOffsetCriteria($request));
            $users = $this->userRepository->all();

            return $this->sendResponse($users->toArray(), 'Users recuperado con éxito');
        }
    }

    /**
     * Store a newly created User in storage.
     * POST /users
     *
     * @param CreateUserAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateUserAPIRequest $request)
    {
        $input = $request->all();

        $users = $this->userRepository->create($input);

        return $this->sendResponse($users->toArray(), 'User guardado exitosamente');
    }

    /**
     * Display the specified User.
     * GET|HEAD /users/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var User $user */
        $user = $this->userRepository->findWithoutFail($id);

        if (empty($user)) {
            return $this->sendError('User no encontrado');
        }

        return $this->sendResponse($user->toArray(), 'User recuperado con éxito');
    }

    /**
     * Update the specified User in storage.
     * PUT/PATCH /users/{id}
     *
     * @param  int $id
     * @param UpdateUserAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateUserAPIRequest $request)
    {
        $input = $request->all();

        /** @var User $user */
        $user = $this->userRepository->findWithoutFail($id);

        if (empty($user)) {
            return $this->sendError('User no encontrado');
        }

        $user = $this->userRepository->update($input, $id);

        return $this->sendResponse($user->toArray(), 'User actualizado exitosamente');
    }

    /**
     * Remove the specified User from storage.
     * DELETE /users/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var User $user */
        $user = $this->userRepository->findWithoutFail($id);

        if (empty($user)) {
            return $this->sendError('User no encontrado');
        }

        $user->delete();

        return $this->sendResponse($id, 'User eliminado exitosamente');
    }

    public function attache(Request $request)
    {
        $user = $this->userRepository->findWithoutFail($request->user_id);

        if (empty($user)) {
            return $this->sendError('User no encontrado');
        }

        $user->usersDespacha()->attach($request->user_atach);


        return $this->sendResponse($user->toArray(), 'User Asignado exitosamente');
    }

    public function detach(Request $request)
    {
        $user = $this->userRepository->findWithoutFail($request->user_id);

        if (empty($user)) {
            return $this->sendError('User no encontrado');
        }

        $user->usersDespacha()->detach($request->user_detach);


        return $this->sendResponse($user->toArray(), 'User Des-asignado exitosamente');
    }
}
