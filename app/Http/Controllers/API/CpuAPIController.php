<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateCpuAPIRequest;
use App\Http\Requests\API\UpdateCpuAPIRequest;
use App\Models\Cpu;
use App\Repositories\CpuRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class CpuController
 * @package App\Http\Controllers\API
 */

class CpuAPIController extends AppBaseController
{
    /** @var  CpuRepository */
    private $cpuRepository;

    public function __construct(CpuRepository $cpuRepo)
    {
        $this->cpuRepository = $cpuRepo;
    }

    /**
     * Display a listing of the Cpu.
     * GET|HEAD /cpus
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->cpuRepository->pushCriteria(new RequestCriteria($request));
        $this->cpuRepository->pushCriteria(new LimitOffsetCriteria($request));
        $cpus = $this->cpuRepository->all();

        return $this->sendResponse($cpus->toArray(), 'Cpus retrieved successfully');
    }

    /**
     * Store a newly created Cpu in storage.
     * POST /cpus
     *
     * @param CreateCpuAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateCpuAPIRequest $request)
    {
        $input = $request->all();

        $cpus = $this->cpuRepository->create($input);

        return $this->sendResponse($cpus->toArray(), 'Cpu saved successfully');
    }

    /**
     * Display the specified Cpu.
     * GET|HEAD /cpus/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Cpu $cpu */
        $cpu = $this->cpuRepository->findWithoutFail($id);

        if (empty($cpu)) {
            return $this->sendError('Cpu not found');
        }

        return $this->sendResponse($cpu->toArray(), 'Cpu retrieved successfully');
    }

    /**
     * Update the specified Cpu in storage.
     * PUT/PATCH /cpus/{id}
     *
     * @param  int $id
     * @param UpdateCpuAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCpuAPIRequest $request)
    {
        $input = $request->all();

        /** @var Cpu $cpu */
        $cpu = $this->cpuRepository->findWithoutFail($id);

        if (empty($cpu)) {
            return $this->sendError('Cpu not found');
        }

        $cpu = $this->cpuRepository->update($input, $id);

        return $this->sendResponse($cpu->toArray(), 'Cpu updated successfully');
    }

    /**
     * Remove the specified Cpu from storage.
     * DELETE /cpus/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Cpu $cpu */
        $cpu = $this->cpuRepository->findWithoutFail($id);

        if (empty($cpu)) {
            return $this->sendError('Cpu not found');
        }

        $cpu->delete();

        return $this->sendResponse($id, 'Cpu deleted successfully');
    }
}
