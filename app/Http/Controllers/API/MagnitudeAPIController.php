<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateMagnitudeAPIRequest;
use App\Http\Requests\API\UpdateMagnitudeAPIRequest;
use App\Models\Magnitude;
use App\Repositories\MagnitudeRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class MagnitudeController
 * @package App\Http\Controllers\API
 */

class MagnitudeAPIController extends AppBaseController
{
    /** @var  MagnitudeRepository */
    private $magnitudeRepository;

    public function __construct(MagnitudeRepository $magnitudeRepo)
    {
        $this->magnitudeRepository = $magnitudeRepo;
    }

    /**
     * Display a listing of the Magnitude.
     * GET|HEAD /magnitudes
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->magnitudeRepository->pushCriteria(new RequestCriteria($request));
        $this->magnitudeRepository->pushCriteria(new LimitOffsetCriteria($request));
        $magnitudes = $this->magnitudeRepository->all();

        return $this->sendResponse($magnitudes->toArray(), 'Magnitudes retrieved successfully');
    }

    /**
     * Store a newly created Magnitude in storage.
     * POST /magnitudes
     *
     * @param CreateMagnitudeAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateMagnitudeAPIRequest $request)
    {
        $input = $request->all();

        $magnitudes = $this->magnitudeRepository->create($input);

        return $this->sendResponse($magnitudes->toArray(), 'Magnitude saved successfully');
    }

    /**
     * Display the specified Magnitude.
     * GET|HEAD /magnitudes/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Magnitude $magnitude */
        $magnitude = $this->magnitudeRepository->findWithoutFail($id);

        if (empty($magnitude)) {
            return $this->sendError('Magnitude not found');
        }

        return $this->sendResponse($magnitude->toArray(), 'Magnitude retrieved successfully');
    }

    /**
     * Update the specified Magnitude in storage.
     * PUT/PATCH /magnitudes/{id}
     *
     * @param  int $id
     * @param UpdateMagnitudeAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateMagnitudeAPIRequest $request)
    {
        $input = $request->all();

        /** @var Magnitude $magnitude */
        $magnitude = $this->magnitudeRepository->findWithoutFail($id);

        if (empty($magnitude)) {
            return $this->sendError('Magnitude not found');
        }

        $magnitude = $this->magnitudeRepository->update($input, $id);

        return $this->sendResponse($magnitude->toArray(), 'Magnitude updated successfully');
    }

    /**
     * Remove the specified Magnitude from storage.
     * DELETE /magnitudes/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Magnitude $magnitude */
        $magnitude = $this->magnitudeRepository->findWithoutFail($id);

        if (empty($magnitude)) {
            return $this->sendError('Magnitude not found');
        }

        $magnitude->delete();

        return $this->sendResponse($id, 'Magnitude deleted successfully');
    }
}
