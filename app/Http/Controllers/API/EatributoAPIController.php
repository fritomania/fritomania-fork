<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateEatributoAPIRequest;
use App\Http\Requests\API\UpdateEatributoAPIRequest;
use App\Models\Eatributo;
use App\Repositories\EatributoRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class EatributoController
 * @package App\Http\Controllers\API
 */

class EatributoAPIController extends AppBaseController
{
    /** @var  EatributoRepository */
    private $eatributoRepository;

    public function __construct(EatributoRepository $eatributoRepo)
    {
        $this->eatributoRepository = $eatributoRepo;
    }

    /**
     * Display a listing of the Eatributo.
     * GET|HEAD /eatributos
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->eatributoRepository->pushCriteria(new RequestCriteria($request));
        $this->eatributoRepository->pushCriteria(new LimitOffsetCriteria($request));
        $eatributos = $this->eatributoRepository->all();

        return $this->sendResponse($eatributos->toArray(), 'Eatributos retrieved successfully');
    }

    /**
     * Store a newly created Eatributo in storage.
     * POST /eatributos
     *
     * @param CreateEatributoAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateEatributoAPIRequest $request)
    {
        $input = $request->all();

        $eatributos = $this->eatributoRepository->create($input);

        return $this->sendResponse($eatributos->toArray(), 'Eatributo saved successfully');
    }

    /**
     * Display the specified Eatributo.
     * GET|HEAD /eatributos/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Eatributo $eatributo */
        $eatributo = $this->eatributoRepository->findWithoutFail($id);

        if (empty($eatributo)) {
            return $this->sendError('Eatributo not found');
        }

        return $this->sendResponse($eatributo->toArray(), 'Eatributo retrieved successfully');
    }

    /**
     * Update the specified Eatributo in storage.
     * PUT/PATCH /eatributos/{id}
     *
     * @param  int $id
     * @param UpdateEatributoAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateEatributoAPIRequest $request)
    {
        $input = $request->all();

        /** @var Eatributo $eatributo */
        $eatributo = $this->eatributoRepository->findWithoutFail($id);

        if (empty($eatributo)) {
            return $this->sendError('Eatributo not found');
        }

        $eatributo = $this->eatributoRepository->update($input, $id);

        return $this->sendResponse($eatributo->toArray(), 'Eatributo updated successfully');
    }

    /**
     * Remove the specified Eatributo from storage.
     * DELETE /eatributos/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Eatributo $eatributo */
        $eatributo = $this->eatributoRepository->findWithoutFail($id);

        if (empty($eatributo)) {
            return $this->sendError('Eatributo not found');
        }

        $eatributo->delete();

        return $this->sendResponse($id, 'Eatributo deleted successfully');
    }
}
