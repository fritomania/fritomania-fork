<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateCretiroAPIRequest;
use App\Http\Requests\API\UpdateCretiroAPIRequest;
use App\Models\Cretiro;
use App\Repositories\CretiroRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class CretiroController
 * @package App\Http\Controllers\API
 */

class CretiroAPIController extends AppBaseController
{
    /** @var  CretiroRepository */
    private $cretiroRepository;

    public function __construct(CretiroRepository $cretiroRepo)
    {
        $this->cretiroRepository = $cretiroRepo;
    }

    /**
     * Display a listing of the Cretiro.
     * GET|HEAD /cretiros
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->cretiroRepository->pushCriteria(new RequestCriteria($request));
        $this->cretiroRepository->pushCriteria(new LimitOffsetCriteria($request));
        $cretiros = $this->cretiroRepository->all();

        return $this->sendResponse($cretiros->toArray(), 'Cretiros recuperado exitosamente');
    }

    /**
     * Store a newly created Retiro de Caja in storage.
     * POST /cretiros
     *
     * @param CreateCretiroAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateCretiroAPIRequest $request)
    {
        $input = $request->all();

        $cretiros = $this->cretiroRepository->create($input);

        return $this->sendResponse($cretiros->toArray(), 'Retiro de Caja guardado exitosamente');
    }

    /**
     * Display the specified Cretiro.
     * GET|HEAD /cretiros/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Retiro de Caja $cretiro */
        $cretiro = $this->cretiroRepository->findWithoutFail($id);

        if (empty($cretiro)) {
            return $this->sendError('Retiro de Caja no encontrado');
        }

        return $this->sendResponse($cretiro->toArray(), 'Retiro de Caja recuperado exitosamente');
    }

    /**
     * Update the specified Retiro de Caja in storage.
     * PUT/PATCH /cretiros/{id}
     *
     * @param  int $id
     * @param UpdateCretiroAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCretiroAPIRequest $request)
    {
        $input = $request->all();

        /** @var Retiro de Caja $cretiro */
        $cretiro = $this->cretiroRepository->findWithoutFail($id);

        if (empty($cretiro)) {
            return $this->sendError('Retiro de Caja no encontrado');
        }

        $cretiro = $this->cretiroRepository->update($input, $id);

        return $this->sendResponse($cretiro->toArray(), 'Retiro de Caja actualizado exitosamente');
    }

    /**
     * Remove the specified Retiro de Caja from storage.
     * DELETE /cretiros/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Retiro de Caja $cretiro */
        $cretiro = $this->cretiroRepository->findWithoutFail($id);

        if (empty($cretiro)) {
            return $this->sendError('Retiro de Caja no encontrado');
        }

        $cretiro->delete();

        return $this->sendResponse($id, 'Retiro de Caja eliminado exitosamente');
    }
}
