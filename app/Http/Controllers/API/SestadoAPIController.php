<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateSestadoAPIRequest;
use App\Http\Requests\API\UpdateSestadoAPIRequest;
use App\Models\Sestado;
use App\Repositories\SestadoRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class SestadoController
 * @package App\Http\Controllers\API
 */

class SestadoAPIController extends AppBaseController
{
    /** @var  SestadoRepository */
    private $sestadoRepository;

    public function __construct(SestadoRepository $sestadoRepo)
    {
        $this->sestadoRepository = $sestadoRepo;
    }

    /**
     * Display a listing of the Sestado.
     * GET|HEAD /sestados
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->sestadoRepository->pushCriteria(new RequestCriteria($request));
        $this->sestadoRepository->pushCriteria(new LimitOffsetCriteria($request));
        $sestados = $this->sestadoRepository->all();

        return $this->sendResponse($sestados->toArray(), 'Sestados retrieved successfully');
    }

    /**
     * Store a newly created Sestado in storage.
     * POST /sestados
     *
     * @param CreateSestadoAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateSestadoAPIRequest $request)
    {
        $input = $request->all();

        $sestados = $this->sestadoRepository->create($input);

        return $this->sendResponse($sestados->toArray(), 'Sestado saved successfully');
    }

    /**
     * Display the specified Sestado.
     * GET|HEAD /sestados/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Sestado $sestado */
        $sestado = $this->sestadoRepository->findWithoutFail($id);

        if (empty($sestado)) {
            return $this->sendError('Sestado not found');
        }

        return $this->sendResponse($sestado->toArray(), 'Sestado retrieved successfully');
    }

    /**
     * Update the specified Sestado in storage.
     * PUT/PATCH /sestados/{id}
     *
     * @param  int $id
     * @param UpdateSestadoAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSestadoAPIRequest $request)
    {
        $input = $request->all();

        /** @var Sestado $sestado */
        $sestado = $this->sestadoRepository->findWithoutFail($id);

        if (empty($sestado)) {
            return $this->sendError('Sestado not found');
        }

        $sestado = $this->sestadoRepository->update($input, $id);

        return $this->sendResponse($sestado->toArray(), 'Sestado updated successfully');
    }

    /**
     * Remove the specified Sestado from storage.
     * DELETE /sestados/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Sestado $sestado */
        $sestado = $this->sestadoRepository->findWithoutFail($id);

        if (empty($sestado)) {
            return $this->sendError('Sestado not found');
        }

        $sestado->delete();

        return $this->sendResponse($id, 'Sestado deleted successfully');
    }
}
