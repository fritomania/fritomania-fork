<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateTempPedidoDetalleAPIRequest;
use App\Http\Requests\API\UpdateTempPedidoDetalleAPIRequest;
use App\Models\TempPedidoDetalle;
use App\Repositories\TempPedidoDetalleRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class TempPedidoDetalleController
 * @package App\Http\Controllers\API
 */

class TempPedidoDetalleAPIController extends AppBaseController
{
    /** @var  TempPedidoDetalleRepository */
    private $tempPedidoDetalleRepository;

    public function __construct(TempPedidoDetalleRepository $tempPedidoDetalleRepo)
    {
        $this->tempPedidoDetalleRepository = $tempPedidoDetalleRepo;
    }

    /**
     * Display a listing of the TempPedidoDetalle.
     * GET|HEAD /tempPedidoDetalles
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->tempPedidoDetalleRepository->pushCriteria(new RequestCriteria($request));
        $this->tempPedidoDetalleRepository->pushCriteria(new LimitOffsetCriteria($request));
        $tempPedidoDetalles = $this->tempPedidoDetalleRepository->all();

        return $this->sendResponse($tempPedidoDetalles->toArray(), 'Temp Pedido Detalles retrieved successfully');
    }

    /**
     * Store a newly created TempPedidoDetalle in storage.
     * POST /tempPedidoDetalles
     *
     * @param CreateTempPedidoDetalleAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateTempPedidoDetalleAPIRequest $request)
    {
        $input = $request->all();

        $tempPedidoDetalles = $this->tempPedidoDetalleRepository->create($input);

        $tempPedidoDetalles->item;
        $tempPedidoDetalles->item->marca;


        return $this->sendResponse($tempPedidoDetalles->toArray(), 'Temp Pedido Detalle saved successfully');
    }

    /**
     * Display the specified TempPedidoDetalle.
     * GET|HEAD /tempPedidoDetalles/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var TempPedidoDetalle $tempPedidoDetalle */
        $tempPedidoDetalle = $this->tempPedidoDetalleRepository->findWithoutFail($id);

        if (empty($tempPedidoDetalle)) {
            return $this->sendError('Temp Pedido Detalle not found');
        }

        return $this->sendResponse($tempPedidoDetalle->toArray(), 'Temp Pedido Detalle retrieved successfully');
    }

    /**
     * Update the specified TempPedidoDetalle in storage.
     * PUT/PATCH /tempPedidoDetalles/{id}
     *
     * @param  int $id
     * @param UpdateTempPedidoDetalleAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateTempPedidoDetalleAPIRequest $request)
    {
        $input = $request->all();

        /** @var TempPedidoDetalle $tempPedidoDetalle */
        $tempPedidoDetalle = $this->tempPedidoDetalleRepository->findWithoutFail($id);

        if (empty($tempPedidoDetalle)) {
            return $this->sendError('Temp Pedido Detalle not found');
        }

        $tempPedidoDetalle = $this->tempPedidoDetalleRepository->update($input, $id);

        return $this->sendResponse($tempPedidoDetalle->toArray(), 'TempPedidoDetalle updated successfully');
    }

    /**
     * Remove the specified TempPedidoDetalle from storage.
     * DELETE /tempPedidoDetalles/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var TempPedidoDetalle $tempPedidoDetalle */
        $tempPedidoDetalle = $this->tempPedidoDetalleRepository->findWithoutFail($id);

        if (empty($tempPedidoDetalle)) {
            return $this->sendError('Temp Pedido Detalle not found');
        }

        $tempPedidoDetalle->delete();

        return $this->sendResponse($id, 'Temp Pedido Detalle deleted successfully');
    }
}
