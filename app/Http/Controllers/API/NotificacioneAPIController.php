<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateNotificacioneAPIRequest;
use App\Http\Requests\API\UpdateNotificacioneAPIRequest;
use App\Models\Notificacione;
use App\Repositories\NotificacioneRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class NotificacioneController
 * @package App\Http\Controllers\API
 */

class NotificacioneAPIController extends AppBaseController
{
    /** @var  NotificacioneRepository */
    private $notificacioneRepository;

    public function __construct(NotificacioneRepository $notificacioneRepo)
    {
        $this->notificacioneRepository = $notificacioneRepo;
    }

    /**
     * Display a listing of the Notificacione.
     * GET|HEAD /notificaciones
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        if($request->user){
            $notificaciones = Notificacione::delUser($request->user)->noVistas()->get();
        }else{
            $this->notificacioneRepository->pushCriteria(new RequestCriteria($request));
            $this->notificacioneRepository->pushCriteria(new LimitOffsetCriteria($request));
            $notificaciones = $this->notificacioneRepository->all();
        }


        return $this->sendResponse($notificaciones->toArray(), 'Notificaciones recuperado con éxito');
    }

    /**
     * Store a newly created Notificacione in storage.
     * POST /notificaciones
     *
     * @param CreateNotificacioneAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateNotificacioneAPIRequest $request)
    {
        $input = $request->all();

        $notificaciones = $this->notificacioneRepository->create($input);

        return $this->sendResponse($notificaciones->toArray(), 'Notificacione guardado exitosamente');
    }

    /**
     * Display the specified Notificacione.
     * GET|HEAD /notificaciones/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Notificacione $notificacione */
        $notificacione = $this->notificacioneRepository->findWithoutFail($id);

        if (empty($notificacione)) {
            return $this->sendError('Notificacione no encontrado');
        }

        return $this->sendResponse($notificacione->toArray(), 'Notificacione recuperado con éxito');
    }

    /**
     * Update the specified Notificacione in storage.
     * PUT/PATCH /notificaciones/{id}
     *
     * @param  int $id
     * @param UpdateNotificacioneAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateNotificacioneAPIRequest $request)
    {
        $input = $request->all();

        /** @var Notificacione $notificacione */
        $notificacione = $this->notificacioneRepository->findWithoutFail($id);

        if (empty($notificacione)) {
            return $this->sendError('Notificacione no encontrado');
        }

        $notificacione = $this->notificacioneRepository->update($input, $id);

        return $this->sendResponse($notificacione->toArray(), 'Notificacione actualizado exitosamente');
    }

    /**
     * Remove the specified Notificacione from storage.
     * DELETE /notificaciones/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Notificacione $notificacione */
        $notificacione = $this->notificacioneRepository->findWithoutFail($id);

        if (empty($notificacione)) {
            return $this->sendError('Notificacione no encontrado');
        }

        $notificacione->delete();

        return $this->sendResponse($id, 'Notificacione eliminado exitosamente');
    }
}
