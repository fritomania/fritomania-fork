<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateSgastoAPIRequest;
use App\Http\Requests\API\UpdateSgastoAPIRequest;
use App\Models\Servicio;
use App\Models\Sgasto;
use App\Repositories\SgastoRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class SgastoController
 * @package App\Http\Controllers\API
 */

class SgastoAPIController extends AppBaseController
{
    /** @var  SgastoRepository */
    private $sgastoRepository;

    public function __construct(SgastoRepository $sgastoRepo)
    {
        $this->sgastoRepository = $sgastoRepo;
    }

    /**
     * Display a listing of the Sgasto.
     * GET|HEAD /sgastos
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {

        if(isset($request->servicio_id)){
            $servicio = Servicio::find($request->servicio_id);
            $sgastos = $servicio->sgastos;

        }else{
            $this->sgastoRepository->pushCriteria(new RequestCriteria($request));
            $this->sgastoRepository->pushCriteria(new LimitOffsetCriteria($request));
            $sgastos = $this->sgastoRepository->all();
        }


        return $this->sendResponse($sgastos->toArray(), 'Sgastos retrieved successfully');
    }

    /**
     * Store a newly created Sgasto in storage.
     * POST /sgastos
     *
     * @param CreateSgastoAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateSgastoAPIRequest $request)
    {
        $input = $request->all();

        $sgastos = $this->sgastoRepository->create($input);

        return $this->sendResponse($sgastos->toArray(), 'Gasto agregado!');
    }

    /**
     * Display the specified Sgasto.
     * GET|HEAD /sgastos/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Sgasto $sgasto */
        $sgasto = $this->sgastoRepository->findWithoutFail($id);

        if (empty($sgasto)) {
            return $this->sendError('Gasto no encotrado');
        }

        return $this->sendResponse($sgasto->toArray(), 'Gasto obtenido');
    }

    /**
     * Update the specified Sgasto in storage.
     * PUT/PATCH /sgastos/{id}
     *
     * @param  int $id
     * @param UpdateSgastoAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSgastoAPIRequest $request)
    {
        $input = $request->all();

        /** @var Sgasto $sgasto */
        $sgasto = $this->sgastoRepository->findWithoutFail($id);

        if (empty($sgasto)) {
            return $this->sendError('Gasto no encotrado');
        }

        $sgasto = $this->sgastoRepository->update($input, $id);

        return $this->sendResponse($sgasto->toArray(), 'Gasto actualizado!');
    }

    /**
     * Remove the specified Sgasto from storage.
     * DELETE /sgastos/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Sgasto $sgasto */
        $sgasto = $this->sgastoRepository->findWithoutFail($id);

        if (empty($sgasto)) {
            return $this->sendError('Gasto no encotrado');
        }

        $sgasto->delete();

        return $this->sendResponse($id, 'Gasto eliminado!');
    }
}
