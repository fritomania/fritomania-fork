<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateEquipoAPIRequest;
use App\Http\Requests\API\UpdateEquipoAPIRequest;
use App\Models\Equipo;
use App\Repositories\EquipoRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class EquipoController
 * @package App\Http\Controllers\API
 */

class EquipoAPIController extends AppBaseController
{
    /** @var  EquipoRepository */
    private $equipoRepository;

    public function __construct(EquipoRepository $equipoRepo)
    {
        $this->equipoRepository = $equipoRepo;
    }

    /**
     * Display a listing of the Equipo.
     * GET|HEAD /equipos
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->equipoRepository->pushCriteria(new RequestCriteria($request));
        $this->equipoRepository->pushCriteria(new LimitOffsetCriteria($request));
        $equipos = $this->equipoRepository->all();

        return $this->sendResponse($equipos->toArray(), 'Equipos retrieved successfully');
    }

    /**
     * Store a newly created Equipo in storage.
     * POST /equipos
     *
     * @param CreateEquipoAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateEquipoAPIRequest $request)
    {
        $input = $request->all();

        $equipo = $this->equipoRepository->create($input);

        if(is_null($input['numero_serie'])){
            $equipo->numero_serie = $equipo->id.$equipo->etipo_id.$equipo->emarca_id.$equipo->modelo_id;
            $equipo->save();
        }

        $equipo->etipo;
        $equipo->emarca;
        $equipo->modelo;

        return $this->sendResponse($equipo->toArray(), 'Equipo saved successfully');
    }

    /**
     * Display the specified Equipo.
     * GET|HEAD /equipos/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Equipo $equipo */
        $equipo = $this->equipoRepository->findWithoutFail($id);

        if (empty($equipo)) {
            return $this->sendError('Equipo not found');
        }

        return $this->sendResponse($equipo->toArray(), 'Equipo retrieved successfully');
    }

    /**
     * Update the specified Equipo in storage.
     * PUT/PATCH /equipos/{id}
     *
     * @param  int $id
     * @param UpdateEquipoAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateEquipoAPIRequest $request)
    {
        $input = $request->all();

        /** @var Equipo $equipo */
        $equipo = $this->equipoRepository->findWithoutFail($id);

        if (empty($equipo)) {
            return $this->sendError('Equipo not found');
        }

        $equipo = $this->equipoRepository->update($input, $id);

        return $this->sendResponse($equipo->toArray(), 'Equipo updated successfully');
    }

    /**
     * Remove the specified Equipo from storage.
     * DELETE /equipos/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Equipo $equipo */
        $equipo = $this->equipoRepository->findWithoutFail($id);

        if (empty($equipo)) {
            return $this->sendError('Equipo not found');
        }

        $equipo->delete();

        return $this->sendResponse($id, 'Equipo deleted successfully');
    }
}
