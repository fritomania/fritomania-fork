<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateEquivalenciaAPIRequest;
use App\Http\Requests\API\UpdateEquivalenciaAPIRequest;
use App\Models\Equivalencia;
use App\Repositories\EquivalenciaRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class EquivalenciaController
 * @package App\Http\Controllers\API
 */

class EquivalenciaAPIController extends AppBaseController
{
    /** @var  EquivalenciaRepository */
    private $equivalenciaRepository;

    public function __construct(EquivalenciaRepository $equivalenciaRepo)
    {
        $this->equivalenciaRepository = $equivalenciaRepo;
    }

    /**
     * Display a listing of the Equivalencia.
     * GET|HEAD /equivalencias
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->equivalenciaRepository->pushCriteria(new RequestCriteria($request));
        $this->equivalenciaRepository->pushCriteria(new LimitOffsetCriteria($request));
        $equivalencias = $this->equivalenciaRepository->all();

        return $this->sendResponse($equivalencias->toArray(), 'Equivalencias recuperado exitosamente');
    }

    /**
     * Store a newly created Equivalencia in storage.
     * POST /equivalencias
     *
     * @param CreateEquivalenciaAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateEquivalenciaAPIRequest $request)
    {
        $input = $request->all();

        $equivalencias = $this->equivalenciaRepository->create($input);

        return $this->sendResponse($equivalencias->toArray(), 'Equivalencia guardado exitosamente');
    }

    /**
     * Display the specified Equivalencia.
     * GET|HEAD /equivalencias/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Equivalencia $equivalencia */
        $equivalencia = $this->equivalenciaRepository->findWithoutFail($id);

        if (empty($equivalencia)) {
            return $this->sendError('Equivalencia no encontrado');
        }

        return $this->sendResponse($equivalencia->toArray(), 'Equivalencia recuperado exitosamente');
    }

    /**
     * Update the specified Equivalencia in storage.
     * PUT/PATCH /equivalencias/{id}
     *
     * @param  int $id
     * @param UpdateEquivalenciaAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateEquivalenciaAPIRequest $request)
    {
        $input = $request->all();

        /** @var Equivalencia $equivalencia */
        $equivalencia = $this->equivalenciaRepository->findWithoutFail($id);

        if (empty($equivalencia)) {
            return $this->sendError('Equivalencia no encontrado');
        }

        $equivalencia = $this->equivalenciaRepository->update($input, $id);

        return $this->sendResponse($equivalencia->toArray(), 'Equivalencia actualizado exitosamente');
    }

    /**
     * Remove the specified Equivalencia from storage.
     * DELETE /equivalencias/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Equivalencia $equivalencia */
        $equivalencia = $this->equivalenciaRepository->findWithoutFail($id);

        if (empty($equivalencia)) {
            return $this->sendError('Equivalencia no encontrado');
        }

        $equivalencia->delete();

        return $this->sendResponse($id, 'Equivalencia eliminado exitosamente');
    }

    public function item($id)
    {
        /** @var Equivalencia $equivalencia */
        $equivalencia = Equivalencia::where('item_origen',$id)->first();

        if (empty($equivalencia)) {
            return $this->sendError('El articulo no tiene registrada equivalencia');
        }

        $equivalencia->itemOrigen;
        $equivalencia->itemDestino;

        return $this->sendResponse($equivalencia->toArray(), 'Equivalencia: 1 '.$equivalencia->itemOrigen->nombre.'= '.$equivalencia->cantidad.' '.$equivalencia->itemDestino->nombre);
    }
}
