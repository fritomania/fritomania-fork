<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateUnimedAPIRequest;
use App\Http\Requests\API\UpdateUnimedAPIRequest;
use App\Models\Unimed;
use App\Repositories\UnimedRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class UnimedController
 * @package App\Http\Controllers\API
 */

class UnimedAPIController extends AppBaseController
{
    /** @var  UnimedRepository */
    private $unimedRepository;

    public function __construct(UnimedRepository $unimedRepo)
    {
        $this->unimedRepository = $unimedRepo;
    }

    /**
     * Display a listing of the Unimed.
     * GET|HEAD /unimeds
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->unimedRepository->pushCriteria(new RequestCriteria($request));
        $this->unimedRepository->pushCriteria(new LimitOffsetCriteria($request));
        $unimeds = $this->unimedRepository->all();

        return $this->sendResponse($unimeds->toArray(), 'Unimeds retrieved successfully');
    }

    /**
     * Store a newly created Unimed in storage.
     * POST /unimeds
     *
     * @param CreateUnimedAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateUnimedAPIRequest $request)
    {
        $input = $request->all();

        $unimeds = $this->unimedRepository->create($input);

        return $this->sendResponse($unimeds->toArray(), 'Unimed saved successfully');
    }

    /**
     * Display the specified Unimed.
     * GET|HEAD /unimeds/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Unimed $unimed */
        $unimed = $this->unimedRepository->findWithoutFail($id);

        if (empty($unimed)) {
            return $this->sendError('Unimed not found');
        }

        return $this->sendResponse($unimed->toArray(), 'Unimed retrieved successfully');
    }

    /**
     * Update the specified Unimed in storage.
     * PUT/PATCH /unimeds/{id}
     *
     * @param  int $id
     * @param UpdateUnimedAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateUnimedAPIRequest $request)
    {
        $input = $request->all();

        /** @var Unimed $unimed */
        $unimed = $this->unimedRepository->findWithoutFail($id);

        if (empty($unimed)) {
            return $this->sendError('Unimed not found');
        }

        $unimed = $this->unimedRepository->update($input, $id);

        return $this->sendResponse($unimed->toArray(), 'Unimed updated successfully');
    }

    /**
     * Remove the specified Unimed from storage.
     * DELETE /unimeds/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Unimed $unimed */
        $unimed = $this->unimedRepository->findWithoutFail($id);

        if (empty($unimed)) {
            return $this->sendError('Unimed not found');
        }

        $unimed->delete();

        return $this->sendResponse($id, 'Unimed deleted successfully');
    }
}
