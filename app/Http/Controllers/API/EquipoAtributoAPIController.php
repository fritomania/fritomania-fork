<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateEquipoAtributoAPIRequest;
use App\Http\Requests\API\UpdateEquipoAtributoAPIRequest;
use App\Models\EquipoAtributo;
use App\Repositories\EquipoAtributoRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class EquipoAtributoController
 * @package App\Http\Controllers\API
 */

class EquipoAtributoAPIController extends AppBaseController
{
    /** @var  EquipoAtributoRepository */
    private $equipoAtributoRepository;

    public function __construct(EquipoAtributoRepository $equipoAtributoRepo)
    {
        $this->equipoAtributoRepository = $equipoAtributoRepo;
    }

    /**
     * Display a listing of the EquipoAtributo.
     * GET|HEAD /equipoAtributos
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->equipoAtributoRepository->pushCriteria(new RequestCriteria($request));
        $this->equipoAtributoRepository->pushCriteria(new LimitOffsetCriteria($request));
        $equipoAtributos = $this->equipoAtributoRepository->all();

        return $this->sendResponse($equipoAtributos->toArray(), 'Equipo Atributos retrieved successfully');
    }

    /**
     * Store a newly created EquipoAtributo in storage.
     * POST /equipoAtributos
     *
     * @param CreateEquipoAtributoAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateEquipoAtributoAPIRequest $request)
    {
        $input = $request->all();

        $equipoAtributos = $this->equipoAtributoRepository->create($input);

        $equipoAtributos->eatributo;

        return $this->sendResponse($equipoAtributos->toArray(), 'Equipo Atributo saved successfully');
    }

    /**
     * Display the specified EquipoAtributo.
     * GET|HEAD /equipoAtributos/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var EquipoAtributo $equipoAtributo */
        $equipoAtributo = $this->equipoAtributoRepository->findWithoutFail($id);

        if (empty($equipoAtributo)) {
            return $this->sendError('Equipo Atributo not found');
        }

        return $this->sendResponse($equipoAtributo->toArray(), 'Equipo Atributo retrieved successfully');
    }

    /**
     * Update the specified EquipoAtributo in storage.
     * PUT/PATCH /equipoAtributos/{id}
     *
     * @param  int $id
     * @param UpdateEquipoAtributoAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateEquipoAtributoAPIRequest $request)
    {
        $input = $request->all();

        /** @var EquipoAtributo $equipoAtributo */
        $equipoAtributo = $this->equipoAtributoRepository->findWithoutFail($id);

        if (empty($equipoAtributo)) {
            return $this->sendError('Equipo Atributo not found');
        }

        $equipoAtributo = $this->equipoAtributoRepository->update($input, $id);

        return $this->sendResponse($equipoAtributo->toArray(), 'EquipoAtributo updated successfully');
    }

    /**
     * Remove the specified EquipoAtributo from storage.
     * DELETE /equipoAtributos/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var EquipoAtributo $equipoAtributo */
        $equipoAtributo = $this->equipoAtributoRepository->findWithoutFail($id);

        if (empty($equipoAtributo)) {
            return $this->sendError('Equipo Atributo not found');
        }

        $equipoAtributo->delete();

        return $this->sendResponse($id, 'Equipo Atributo deleted successfully');
    }
}
