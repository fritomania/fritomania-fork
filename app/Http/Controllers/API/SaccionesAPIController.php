<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateSaccionesAPIRequest;
use App\Http\Requests\API\UpdateSaccionesAPIRequest;
use App\Models\Sacciones;
use App\Repositories\SaccionesRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class SaccionesController
 * @package App\Http\Controllers\API
 */

class SaccionesAPIController extends AppBaseController
{
    /** @var  SaccionesRepository */
    private $saccionesRepository;

    public function __construct(SaccionesRepository $saccionesRepo)
    {
        $this->saccionesRepository = $saccionesRepo;
    }

    /**
     * Display a listing of the Sacciones.
     * GET|HEAD /sacciones
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->saccionesRepository->pushCriteria(new RequestCriteria($request));
        $this->saccionesRepository->pushCriteria(new LimitOffsetCriteria($request));
        $sacciones = $this->saccionesRepository->all();

        return $this->sendResponse($sacciones->toArray(), 'Sacciones retrieved successfully');
    }

    /**
     * Store a newly created Sacciones in storage.
     * POST /sacciones
     *
     * @param CreateSaccionesAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateSaccionesAPIRequest $request)
    {
        $input = $request->all();

        $sacciones = $this->saccionesRepository->create($input);

        return $this->sendResponse($sacciones->toArray(), 'Sacciones saved successfully');
    }

    /**
     * Display the specified Sacciones.
     * GET|HEAD /sacciones/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Sacciones $sacciones */
        $sacciones = $this->saccionesRepository->findWithoutFail($id);

        if (empty($sacciones)) {
            return $this->sendError('Sacciones not found');
        }

        return $this->sendResponse($sacciones->toArray(), 'Sacciones retrieved successfully');
    }

    /**
     * Update the specified Sacciones in storage.
     * PUT/PATCH /sacciones/{id}
     *
     * @param  int $id
     * @param UpdateSaccionesAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSaccionesAPIRequest $request)
    {
        $input = $request->all();

        /** @var Sacciones $sacciones */
        $sacciones = $this->saccionesRepository->findWithoutFail($id);

        if (empty($sacciones)) {
            return $this->sendError('Sacciones not found');
        }

        $sacciones = $this->saccionesRepository->update($input, $id);

        return $this->sendResponse($sacciones->toArray(), 'Sacciones updated successfully');
    }

    /**
     * Remove the specified Sacciones from storage.
     * DELETE /sacciones/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Sacciones $sacciones */
        $sacciones = $this->saccionesRepository->findWithoutFail($id);

        if (empty($sacciones)) {
            return $this->sendError('Sacciones not found');
        }

        $sacciones->delete();

        return $this->sendResponse($id, 'Sacciones deleted successfully');
    }
}
