<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateCorrelativoAPIRequest;
use App\Http\Requests\API\UpdateCorrelativoAPIRequest;
use App\Models\Correlativo;
use App\Repositories\CorrelativoRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class CorrelativoController
 * @package App\Http\Controllers\API
 */

class CorrelativoAPIController extends AppBaseController
{
    /** @var  CorrelativoRepository */
    private $correlativoRepository;

    public function __construct(CorrelativoRepository $correlativoRepo)
    {
        $this->correlativoRepository = $correlativoRepo;
    }

    /**
     * Display a listing of the Correlativo.
     * GET|HEAD /correlativos
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->correlativoRepository->pushCriteria(new RequestCriteria($request));
        $this->correlativoRepository->pushCriteria(new LimitOffsetCriteria($request));
        $correlativos = $this->correlativoRepository->all();

        return $this->sendResponse($correlativos->toArray(), 'Correlativos retrieved successfully');
    }

    /**
     * Store a newly created Correlativo in storage.
     * POST /correlativos
     *
     * @param CreateCorrelativoAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateCorrelativoAPIRequest $request)
    {
        $input = $request->all();

        $correlativos = $this->correlativoRepository->create($input);

        return $this->sendResponse($correlativos->toArray(), 'Correlativo saved successfully');
    }

    /**
     * Display the specified Correlativo.
     * GET|HEAD /correlativos/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Correlativo $correlativo */
        $correlativo = $this->correlativoRepository->findWithoutFail($id);

        if (empty($correlativo)) {
            return $this->sendError('Correlativo not found');
        }

        return $this->sendResponse($correlativo->toArray(), 'Correlativo retrieved successfully');
    }

    /**
     * Update the specified Correlativo in storage.
     * PUT/PATCH /correlativos/{id}
     *
     * @param  int $id
     * @param UpdateCorrelativoAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCorrelativoAPIRequest $request)
    {
        $input = $request->all();

        /** @var Correlativo $correlativo */
        $correlativo = $this->correlativoRepository->findWithoutFail($id);

        if (empty($correlativo)) {
            return $this->sendError('Correlativo not found');
        }

        $correlativo = $this->correlativoRepository->update($input, $id);

        return $this->sendResponse($correlativo->toArray(), 'Correlativo updated successfully');
    }

    /**
     * Remove the specified Correlativo from storage.
     * DELETE /correlativos/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Correlativo $correlativo */
        $correlativo = $this->correlativoRepository->findWithoutFail($id);

        if (empty($correlativo)) {
            return $this->sendError('Correlativo not found');
        }

        $correlativo->delete();

        return $this->sendResponse($id, 'Correlativo deleted successfully');
    }
}
