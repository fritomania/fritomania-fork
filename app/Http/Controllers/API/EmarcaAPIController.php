<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateEmarcaAPIRequest;
use App\Http\Requests\API\UpdateEmarcaAPIRequest;
use App\Models\Emarca;
use App\Repositories\EmarcaRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class EmarcaController
 * @package App\Http\Controllers\API
 */

class EmarcaAPIController extends AppBaseController
{
    /** @var  EmarcaRepository */
    private $emarcaRepository;

    public function __construct(EmarcaRepository $emarcaRepo)
    {
        $this->emarcaRepository = $emarcaRepo;
    }

    /**
     * Display a listing of the Emarca.
     * GET|HEAD /emarcas
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->emarcaRepository->pushCriteria(new RequestCriteria($request));
        $this->emarcaRepository->pushCriteria(new LimitOffsetCriteria($request));
        $emarcas = $this->emarcaRepository->all();

        return $this->sendResponse($emarcas->toArray(), 'Emarcas retrieved successfully');
    }

    /**
     * Store a newly created Emarca in storage.
     * POST /emarcas
     *
     * @param CreateEmarcaAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateEmarcaAPIRequest $request)
    {
        $input = $request->all();

        $emarcas = $this->emarcaRepository->create($input);

        return $this->sendResponse($emarcas->toArray(), 'Emarca saved successfully');
    }

    /**
     * Display the specified Emarca.
     * GET|HEAD /emarcas/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Emarca $emarca */
        $emarca = $this->emarcaRepository->findWithoutFail($id);

        if (empty($emarca)) {
            return $this->sendError('Emarca not found');
        }

        return $this->sendResponse($emarca->toArray(), 'Emarca retrieved successfully');
    }

    /**
     * Update the specified Emarca in storage.
     * PUT/PATCH /emarcas/{id}
     *
     * @param  int $id
     * @param UpdateEmarcaAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateEmarcaAPIRequest $request)
    {
        $input = $request->all();

        /** @var Emarca $emarca */
        $emarca = $this->emarcaRepository->findWithoutFail($id);

        if (empty($emarca)) {
            return $this->sendError('Emarca not found');
        }

        $emarca = $this->emarcaRepository->update($input, $id);

        return $this->sendResponse($emarca->toArray(), 'Emarca updated successfully');
    }

    /**
     * Remove the specified Emarca from storage.
     * DELETE /emarcas/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Emarca $emarca */
        $emarca = $this->emarcaRepository->findWithoutFail($id);

        if (empty($emarca)) {
            return $this->sendError('Emarca not found');
        }

        $emarca->delete();

        return $this->sendResponse($id, 'Emarca deleted successfully');
    }
}
