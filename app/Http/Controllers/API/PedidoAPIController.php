<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreatePedidoAPIRequest;
use App\Http\Requests\API\UpdatePedidoAPIRequest;
use App\Models\Pedido;
use App\Models\Stock;
use App\Repositories\PedidoRepository;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class PedidoController
 * @package App\Http\Controllers\API
 */

class PedidoAPIController extends AppBaseController
{
    /** @var  PedidoRepository */
    private $pedidoRepository;

    public function __construct(PedidoRepository $pedidoRepo)
    {
        $this->pedidoRepository = $pedidoRepo;
    }

    /**
     * Display a listing of the Pedido.
     * GET|HEAD /pedidos
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->pedidoRepository->pushCriteria(new RequestCriteria($request));
        $this->pedidoRepository->pushCriteria(new LimitOffsetCriteria($request));
        $pedidos = $this->pedidoRepository->all();

        return $this->sendResponse($pedidos->toArray(), 'Pedidos retrieved successfully');
    }

    /**
     * Store a newly created Pedido in storage.
     * POST /pedidos
     *
     * @param CreatePedidoAPIRequest $request
     *
     * @return Response
     */
    public function store(CreatePedidoAPIRequest $request)
    {
        $input = $request->all();

        $pedidos = $this->pedidoRepository->create($input);

        return $this->sendResponse($pedidos->toArray(), 'Pedido saved successfully');
    }

    /**
     * Display the specified Pedido.
     * GET|HEAD /pedidos/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Pedido $pedido */
        $pedido = $this->pedidoRepository->findWithoutFail($id);

        if (empty($pedido)) {
            return $this->sendError('Pedido not found');
        }

        return $this->sendResponse($pedido->toArray(), 'Pedido retrieved successfully');
    }

    /**
     * Update the specified Pedido in storage.
     * PUT/PATCH /pedidos/{id}
     *
     * @param  int $id
     * @param UpdatePedidoAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePedidoAPIRequest $request)
    {
        $input = $request->all();

        /** @var Pedido $pedido */
        $pedido = $this->pedidoRepository->findWithoutFail($id);

        if (empty($pedido)) {
            return $this->sendError('Pedido not found');
        }

        $pedido = $this->pedidoRepository->update($input, $id);

        return $this->sendResponse($pedido->toArray(), 'Pedido updated successfully');
    }

    /**
     * Remove the specified Pedido from storage.
     * DELETE /pedidos/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Pedido $pedido */
        $pedido = $this->pedidoRepository->findWithoutFail($id);

        if (empty($pedido)) {
            return $this->sendError('Pedido not found');
        }

        $pedido->delete();

        return $this->sendResponse($id, 'Pedido deleted successfully');
    }

    public function ingreso($id){

        /** @var Pedido $pedido */
        $pedido = $this->pedidoRepository->findWithoutFail($id);

        if (empty($pedido)) {
            return $this->sendError('Pedido no encontrado');
        }

        if ($pedido->pestado_id==2){
            return $this->sendError('Pedido ya ingresado');
        }

        $user = User::find($pedido->user_id);
        $tiendaUser= $user->empleado->tienda->id;

        $stock = new Stock();
        foreach ($pedido->pedidoDetalles as $detalle){
            $stock->ingreso($detalle->item_id,$detalle->cantidad,null,null,null,$tiendaUser);
        }

        $pedido->pestado_id=2;
        $pedido->save();

        return $this->sendResponse($pedido->toArray(), 'Pedido ingresado correctamente');

    }
}
