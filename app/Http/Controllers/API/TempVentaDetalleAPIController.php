<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateTempVentaDetalleAPIRequest;
use App\Http\Requests\API\UpdateTempVentaDetalleAPIRequest;
use App\Models\Item;
use App\Models\TempVenta;
use App\Models\TempVentaDetalle;
use App\Repositories\TempVentaDetalleRepository;
use App\Repositories\TempVentaRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class TempVentaDetalleController
 * @package App\Http\Controllers\API
 */

class TempVentaDetalleAPIController extends AppBaseController
{
    /** @var  TempVentaDetalleRepository */
    private $tempVentaDetalleRepository;
    private $tempVentaRepository;

    public function __construct(TempVentaDetalleRepository $tempVentaDetalleRepo,TempVentaRepository $tempVentaRepo)
    {
        $this->tempVentaDetalleRepository = $tempVentaDetalleRepo;
        $this->tempVentaRepository = $tempVentaRepo;
    }

    /**
     * Display a listing of the TempVentaDetalle.
     * GET|HEAD /tempVentaDetalles
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->tempVentaDetalleRepository->pushCriteria(new RequestCriteria($request));
        $this->tempVentaDetalleRepository->pushCriteria(new LimitOffsetCriteria($request));
        $tempVentaDetalles = $this->tempVentaDetalleRepository->all();

        return $this->sendResponse($tempVentaDetalles->toArray(), 'Temp Venta Detalles recuperado exitosamente');
    }

    /**
     * Store a newly created TempVentaDetalle in storage.
     * POST /tempVentaDetalles
     *
     * @param CreateTempVentaDetalleAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateTempVentaDetalleAPIRequest $request)
    {
        $input = $request->all();

        $tempVenta = TempVenta::find($input['temp_venta_id']);

        if ($tempVenta->procesada){
            return $this->sendError('Esta venta ya ha sido procesada');
        }

        $item= Item::find($request->item_id);

        if($item->esInventariable() && $item->stockTienda($request->tienda)<$request->cantidad){
            return $this->sendError('El stock del articulo no alcanza');
        }

        $tempVentaDetalles = $this->tempVentaDetalleRepository->create($input);

        $tempVentaDetalles->item;
        $tempVentaDetalles->item->marca;

        return $this->sendResponse($tempVentaDetalles->toArray(), 'Artículo agregado!');
    }

    /**
     * Display the specified TempVentaDetalle.
     * GET|HEAD /tempVentaDetalles/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var TempVentaDetalle $tempVentaDetalle */
        $tempVentaDetalle = $this->tempVentaDetalleRepository->findWithoutFail($id);

        if (empty($tempVentaDetalle)) {
            return $this->sendError('Temp Venta Detalle no existe');
        }

        return $this->sendResponse($tempVentaDetalle->toArray(), 'Temp Venta Detalle recuperado exitosamente');
    }

    /**
     * Update the specified TempVentaDetalle in storage.
     * PUT/PATCH /tempVentaDetalles/{id}
     *
     * @param  int $id
     * @param UpdateTempVentaDetalleAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateTempVentaDetalleAPIRequest $request)
    {
        $input = $request->all();

        /** @var TempVentaDetalle $tempVentaDetalle */
        $tempVentaDetalle = $this->tempVentaDetalleRepository->findWithoutFail($id);

        if (empty($tempVentaDetalle)) {
            return $this->sendError('Temp Venta Detalle no existe');
        }

        $tempVentaDetalle = $this->tempVentaDetalleRepository->update($input, $id);

        return $this->sendResponse($tempVentaDetalle->toArray(), 'Detalle de venta actualizado exitosamente');
    }

    /**
     * Remove the specified TempVentaDetalle from storage.
     * DELETE /tempVentaDetalles/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var TempVentaDetalle $tempVentaDetalle */
        $tempVentaDetalle = $this->tempVentaDetalleRepository->findWithoutFail($id);

        if (empty($tempVentaDetalle)) {
            return $this->sendError('Detalle de venta no existe');
        }

        $tempVentaDetalle->forceDelete();

        return $this->sendResponse($tempVentaDetalle, 'Detalle de venta eliminado exitosamente');
    }

    public function detallesVenta($id){
        /** @var TempVentaDetalle $tempVentaDetalle */
        $tempVenta = $this->tempVentaRepository->findWithoutFail($id);

        if (empty($tempVenta)) {
            return $this->sendError('No se encuentra la venta temporal');
        }

        $detalles = $tempVenta->tempVentaDetalles->map(function ($det){
            $det->item;
            return $det;
        });


        return $this->sendResponse($tempVenta->tempVentaDetalles->toArray(), 'Detalles de venta temporal');
    }
}
