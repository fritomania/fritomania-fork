<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateEtipoAPIRequest;
use App\Http\Requests\API\UpdateEtipoAPIRequest;
use App\Models\Etipo;
use App\Repositories\EtipoRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class EtipoController
 * @package App\Http\Controllers\API
 */

class EtipoAPIController extends AppBaseController
{
    /** @var  EtipoRepository */
    private $etipoRepository;

    public function __construct(EtipoRepository $etipoRepo)
    {
        $this->etipoRepository = $etipoRepo;
    }

    /**
     * Display a listing of the Etipo.
     * GET|HEAD /etipos
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->etipoRepository->pushCriteria(new RequestCriteria($request));
        $this->etipoRepository->pushCriteria(new LimitOffsetCriteria($request));
        $etipos = $this->etipoRepository->all();

        return $this->sendResponse($etipos->toArray(), 'Etipos retrieved successfully');
    }

    /**
     * Store a newly created Etipo in storage.
     * POST /etipos
     *
     * @param CreateEtipoAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateEtipoAPIRequest $request)
    {
        $input = $request->all();

        $etipos = $this->etipoRepository->create($input);

        return $this->sendResponse($etipos->toArray(), 'Etipo saved successfully');
    }

    /**
     * Display the specified Etipo.
     * GET|HEAD /etipos/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Etipo $etipo */
        $etipo = $this->etipoRepository->findWithoutFail($id);

        if (empty($etipo)) {
            return $this->sendError('Etipo not found');
        }

        return $this->sendResponse($etipo->toArray(), 'Etipo retrieved successfully');
    }

    /**
     * Update the specified Etipo in storage.
     * PUT/PATCH /etipos/{id}
     *
     * @param  int $id
     * @param UpdateEtipoAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateEtipoAPIRequest $request)
    {
        $input = $request->all();

        /** @var Etipo $etipo */
        $etipo = $this->etipoRepository->findWithoutFail($id);

        if (empty($etipo)) {
            return $this->sendError('Etipo not found');
        }

        $etipo = $this->etipoRepository->update($input, $id);

        return $this->sendResponse($etipo->toArray(), 'Etipo updated successfully');
    }

    /**
     * Remove the specified Etipo from storage.
     * DELETE /etipos/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Etipo $etipo */
        $etipo = $this->etipoRepository->findWithoutFail($id);

        if (empty($etipo)) {
            return $this->sendError('Etipo not found');
        }

        $etipo->delete();

        return $this->sendResponse($id, 'Etipo deleted successfully');
    }
}
