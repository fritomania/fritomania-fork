<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateVpagoAPIRequest;
use App\Http\Requests\API\UpdateVpagoAPIRequest;
use App\Models\Venta;
use App\Models\Vpago;
use App\Repositories\VentaRepository;
use App\Repositories\VpagoRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class VpagoController
 * @package App\Http\Controllers\API
 */

class VpagoAPIController extends AppBaseController
{
    /** @var  VpagoRepository */
    private $vpagoRepository;
    private $ventaReository;

    public function __construct(VpagoRepository $vpagoRepo,VentaRepository $ventaRepo)
    {
        $this->vpagoRepository = $vpagoRepo;
        $this->ventaReository = $ventaRepo;
    }

    /**
     * Display a listing of the Pago de venta.
     * GET|HEAD /vpagos
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        if(isset($request->venta_id)){
            $venta = Venta::find($request->venta_id);
            $vpagos = $venta->vpagos;
            $vpagos = $vpagos->map(function ($item){
               $item->username = $item->user->name;
               unset($item->user);
               return $item;
            });

        }else {
            $this->vpagoRepository->pushCriteria(new RequestCriteria($request));
            $this->vpagoRepository->pushCriteria(new LimitOffsetCriteria($request));
            $vpagos = $this->vpagoRepository->all();
        }

        return $this->sendResponse($vpagos->toArray(), 'Pagos de ventas recuperado exitosamente');
    }

    /**
     * Store a newly created Pago de venta in storage.
     * POST /vpagos
     *
     * @param CreateVpagoAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateVpagoAPIRequest $request)
    {
        $venta = $this->ventaReository->findWithoutFail($request->venta_id);

        if (empty($venta)) {
            return $this->sendError('Venta no encontrada');
        }

        $totalPagos = array_sum(array_pluck($venta->vpagos->toArray(),'monto')) ;
        $saldo = $venta->total - $totalPagos;

        if($request->monto>$saldo){
            return $this->sendError('El monto supera el saldo pendiente');
        }

        $input = $request->all();

        $vpago = $this->vpagoRepository->create($input);

        if ($vpago->monto==$saldo){
            $venta->pagada =1;
            $venta->save();
            return $this->sendResponse($vpago->toArray(), 'Venta pagada');
        }

        return $this->sendResponse($vpago->toArray(), 'Pago de venta guardado exitosamente');
    }

    /**
     * Display the specified Pago de venta.
     * GET|HEAD /vpagos/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Pago de venta $vpago */
        $vpago = $this->vpagoRepository->findWithoutFail($id);

        if (empty($vpago)) {
            return $this->sendError('Pago de venta no encontrado');
        }

        return $this->sendResponse($vpago->toArray(), 'Pago de venta recuperado exitosamente');
    }

    /**
     * Update the specified Pago de venta in storage.
     * PUT/PATCH /vpagos/{id}
     *
     * @param  int $id
     * @param UpdateVpagoAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateVpagoAPIRequest $request)
    {
        $input = $request->all();

        /** @var Pago de venta $vpago */
        $vpago = $this->vpagoRepository->findWithoutFail($id);

        if (empty($vpago)) {
            return $this->sendError('Pago de venta no encontrado');
        }

        $vpago = $this->vpagoRepository->update($input, $id);

        return $this->sendResponse($vpago->toArray(), 'Pago de venta actualizado exitosamente');
    }

    /**
     * Remove the specified Pago de venta from storage.
     * DELETE /vpagos/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Pago de venta $vpago */
        $vpago = $this->vpagoRepository->findWithoutFail($id);

        if (empty($vpago)) {
            return $this->sendError('Vpago no encontrado');
        }

        $vpago->delete();

        return $this->sendResponse($id, 'Vpago eliminado exitosamente');
    }
}
