<?php

namespace App\Http\Controllers;

use App\DataTables\RepartidoreDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateRepartidoreRequest;
use App\Http\Requests\UpdateRepartidoreRequest;
use App\Repositories\RepartidoreRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class RepartidoreController extends AppBaseController
{
    /** @var  RepartidoreRepository */
    private $repartidoreRepository;

    public function __construct(RepartidoreRepository $repartidoreRepo)
    {
        $this->repartidoreRepository = $repartidoreRepo;
        $this->middleware('auth');
    }

    /**
     * Display a listing of the Repartidore.
     *
     * @param RepartidoreDataTable $repartidoreDataTable
     * @return Response
     */
    public function index(RepartidoreDataTable $repartidoreDataTable)
    {
        return $repartidoreDataTable->render('repartidores.index');
    }

    /**
     * Show the form for creating a new Repartidore.
     *
     * @return Response
     */
    public function create()
    {
        return view('repartidores.create');
    }

    /**
     * Store a newly created Repartidore in storage.
     *
     * @param CreateRepartidoreRequest $request
     *
     * @return Response
     */
    public function store(CreateRepartidoreRequest $request)
    {
        $input = $request->all();

        $repartidore = $this->repartidoreRepository->create($input);

        Flash::success('Repartidore guardado exitosamente.');

        return redirect(route('repartidores.index'));
    }

    /**
     * Display the specified Repartidore.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $repartidore = $this->repartidoreRepository->findWithoutFail($id);

        if (empty($repartidore)) {
            Flash::error('Repartidore no encontrado');

            return redirect(route('repartidores.index'));
        }

        return view('repartidores.show',compact('repartidore'));
    }

    /**
     * Show the form for editing the specified Repartidore.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $repartidore = $this->repartidoreRepository->findWithoutFail($id);

        if (empty($repartidore)) {
            Flash::error('Repartidore no encontrado');

            return redirect(route('repartidores.index'));
        }

        return view('repartidores.edit',compact('repartidore'));
    }

    /**
     * Update the specified Repartidore in storage.
     *
     * @param  int              $id
     * @param UpdateRepartidoreRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateRepartidoreRequest $request)
    {
        $repartidore = $this->repartidoreRepository->findWithoutFail($id);

        if (empty($repartidore)) {
            Flash::error('Repartidore no encontrado');

            return redirect(route('repartidores.index'));
        }

        $repartidore = $this->repartidoreRepository->update($request->all(), $id);

        Flash::success('Repartidore actualizado exitosamente.');

        return redirect(route('repartidores.index'));
    }

    /**
     * Remove the specified Repartidore from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $repartidore = $this->repartidoreRepository->findWithoutFail($id);

        if (empty($repartidore)) {
            Flash::error('Repartidore no encontrado');

            return redirect(route('repartidores.index'));
        }

        $this->repartidoreRepository->delete($id);

        Flash::success('Repartidore eliminado exitosamente.');

        return redirect(route('repartidores.index'));
    }
}
