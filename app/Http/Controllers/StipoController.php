<?php

namespace App\Http\Controllers;

use App\DataTables\StipoDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateStipoRequest;
use App\Http\Requests\UpdateStipoRequest;
use App\Repositories\StipoRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class StipoController extends AppBaseController
{
    /** @var  StipoRepository */
    private $stipoRepository;

    public function __construct(StipoRepository $stipoRepo)
    {
        $this->middleware('auth');
        $this->stipoRepository = $stipoRepo;
    }

    /**
     * Display a listing of the Stipo.
     *
     * @param StipoDataTable $stipoDataTable
     * @return Response
     */
    public function index(StipoDataTable $stipoDataTable)
    {
        return $stipoDataTable->render('stipos.index');
    }

    /**
     * Show the form for creating a new Stipo.
     *
     * @return Response
     */
    public function create()
    {
        return view('stipos.create');
    }

    /**
     * Store a newly created Stipo in storage.
     *
     * @param CreateStipoRequest $request
     *
     * @return Response
     */
    public function store(CreateStipoRequest $request)
    {
        $input = $request->all();

        $stipo = $this->stipoRepository->create($input);

        Flash::success('Stipo saved successfully.');

        return redirect(route('stipos.index'));
    }

    /**
     * Display the specified Stipo.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $stipo = $this->stipoRepository->findWithoutFail($id);

        if (empty($stipo)) {
            Flash::error('Stipo not found');

            return redirect(route('stipos.index'));
        }

        return view('stipos.show')->with('stipo', $stipo);
    }

    /**
     * Show the form for editing the specified Stipo.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $stipo = $this->stipoRepository->findWithoutFail($id);

        if (empty($stipo)) {
            Flash::error('Stipo not found');

            return redirect(route('stipos.index'));
        }

        return view('stipos.edit')->with('stipo', $stipo);
    }

    /**
     * Update the specified Stipo in storage.
     *
     * @param  int              $id
     * @param UpdateStipoRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateStipoRequest $request)
    {
        $stipo = $this->stipoRepository->findWithoutFail($id);

        if (empty($stipo)) {
            Flash::error('Stipo not found');

            return redirect(route('stipos.index'));
        }

        $stipo = $this->stipoRepository->update($request->all(), $id);

        Flash::success('Stipo updated successfully.');

        return redirect(route('stipos.index'));
    }

    /**
     * Remove the specified Stipo from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $stipo = $this->stipoRepository->findWithoutFail($id);

        if (empty($stipo)) {
            Flash::error('Stipo not found');

            return redirect(route('stipos.index'));
        }

        $this->stipoRepository->delete($id);

        Flash::success('Stipo deleted successfully.');

        return redirect(route('stipos.index'));
    }
}
