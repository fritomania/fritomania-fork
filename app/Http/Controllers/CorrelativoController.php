<?php

namespace App\Http\Controllers;

use App\DataTables\CorrelativoDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateCorrelativoRequest;
use App\Http\Requests\UpdateCorrelativoRequest;
use App\Repositories\CorrelativoRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class CorrelativoController extends AppBaseController
{
    /** @var  CorrelativoRepository */
    private $correlativoRepository;

    public function __construct(CorrelativoRepository $correlativoRepo)
    {
        $this->correlativoRepository = $correlativoRepo;
        $this->middleware('auth');
    }

    /**
     * Display a listing of the Correlativo.
     *
     * @param CorrelativoDataTable $correlativoDataTable
     * @return Response
     */
    public function index(CorrelativoDataTable $correlativoDataTable)
    {
        return $correlativoDataTable->render('correlativos.index');
    }

    /**
     * Show the form for creating a new Correlativo.
     *
     * @return Response
     */
    public function create()
    {
        return view('correlativos.create');
    }

    /**
     * Store a newly created Correlativo in storage.
     *
     * @param CreateCorrelativoRequest $request
     *
     * @return Response
     */
    public function store(CreateCorrelativoRequest $request)
    {
        $input = $request->all();

        $correlativo = $this->correlativoRepository->create($input);

        Flash::success('Correlativo guardado exitosamente.');

        return redirect(route('correlativos.index'));
    }

    /**
     * Display the specified Correlativo.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $correlativo = $this->correlativoRepository->findWithoutFail($id);

        if (empty($correlativo)) {
            Flash::error('Correlativo no encontrado');

            return redirect(route('correlativos.index'));
        }

        return view('correlativos.show',compact('correlativo'));
    }

    /**
     * Show the form for editing the specified Correlativo.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $correlativo = $this->correlativoRepository->findWithoutFail($id);

        if (empty($correlativo)) {
            Flash::error('Correlativo no encontrado');

            return redirect(route('correlativos.index'));
        }

        return view('correlativos.edit',compact('correlativo'));
    }

    /**
     * Update the specified Correlativo in storage.
     *
     * @param  int              $id
     * @param UpdateCorrelativoRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCorrelativoRequest $request)
    {
        $correlativo = $this->correlativoRepository->findWithoutFail($id);

        if (empty($correlativo)) {
            Flash::error('Correlativo no encontrado');

            return redirect(route('correlativos.index'));
        }

        $correlativo = $this->correlativoRepository->update($request->all(), $id);

        Flash::success('Correlativo actualizado exitosamente.');

        return redirect(route('correlativos.index'));
    }

    /**
     * Remove the specified Correlativo from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $correlativo = $this->correlativoRepository->findWithoutFail($id);

        if (empty($correlativo)) {
            Flash::error('Correlativo no encontrado');

            return redirect(route('correlativos.index'));
        }

        $this->correlativoRepository->delete($id);

        Flash::success('Correlativo eliminado exitosamente.');

        return redirect(route('correlativos.index'));
    }
}
