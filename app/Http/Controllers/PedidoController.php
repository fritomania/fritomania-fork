<?php

namespace App\Http\Controllers;

use App\DataTables\PedidoDataTable;
use App\Http\Requests;
use App\Http\Requests\CreatePedidoRequest;
use App\Http\Requests\UpdatePedidoRequest;
use App\Models\Cliente;
use App\Models\Compra;
use App\Models\CompraDetalle;
use App\Models\Pedido;
use App\Models\PedidoDetalle;
use App\Models\Stock;
use App\Models\TempPedido;
use App\Models\Venta;
use App\Models\VentaDetalle;
use App\Repositories\PedidoRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Illuminate\Support\Facades\Auth;
use Response;

class PedidoController extends AppBaseController
{
    /** @var  PedidoRepository */
    private $pedidoRepository;

    public function __construct(PedidoRepository $pedidoRepo)
    {
        $this->middleware('auth');
        $this->pedidoRepository = $pedidoRepo;
    }

    /**
     * Display a listing of the Pedido.
     *
     * @param PedidoDataTable $pedidoDataTable
     * @return Response
     */
    public function index(PedidoDataTable $pedidoDataTable)
    {
        return $pedidoDataTable->render('pedidos.index');
    }

    /**
     * Show the form for creating a new Pedido.
     *
     * @return Response
     */
    public function create()
    {
        $user=Auth::user();

        $tempPedido = TempPedido::where('procesado',0)
            ->where('user_id',$user->id)
            ->get();

        ///si el usuario no tiene ninguna venta creada
        if($tempPedido->count()>1) {
            dd('el usuario tiene '.$tempPedido->count().' Ventas temporales');
        }

        if($tempPedido->count()==0){

            $tempPedido = TempPedido::create([
                'user_id' => $user->id
            ]);

        }else{
            $tempPedido = $tempPedido[0];
        }


        $clientes = Cliente::all()->toArray();
        $clientes = array_pluck($clientes,'full_name','id');


        return view('pedidos.create',compact('clientes','tempPedido'));

    }

    /**
     * Store a newly created Pedido in storage.
     *
     * @param CreatePedidoRequest $request
     *
     * @return Response
     */
    public function store(CreatePedidoRequest $request)
    {
        $input = $request->all();

        $pedido = $this->pedidoRepository->create($input);

        Flash::success('Pedido saved successfully.');

        return redirect(route('pedidos.index'));
    }

    /**
     * Display the specified Pedido.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $pedido = $this->pedidoRepository->findWithoutFail($id);

        if (empty($pedido)) {
            Flash::error('Pedido not found');

            return redirect(route('pedidos.index'));
        }

        return view('pedidos.show')->with('pedido', $pedido);
    }

    /**
     * Show the form for editing the specified Pedido.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $pedido = $this->pedidoRepository->findWithoutFail($id);

        if (empty($pedido)) {
            Flash::error('Pedido not found');

            return redirect(route('pedidos.index'));
        }

        return view('pedidos.edit')->with('pedido', $pedido);
    }

    /**
     * Update the specified Pedido in storage.
     *
     * @param  int              $id
     * @param UpdatePedidoRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePedidoRequest $request)
    {
        $pedido = $this->pedidoRepository->findWithoutFail($id);

        if (empty($pedido)) {
            Flash::error('Pedido not found');

            return redirect(route('pedidos.index'));
        }

        $pedido = $this->pedidoRepository->update($request->all(), $id);

        Flash::success('Pedido updated successfully.');

        return redirect(route('pedidos.index'));
    }

    /**
     * Remove the specified Pedido from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $pedido = $this->pedidoRepository->findWithoutFail($id);

        if (empty($pedido)) {
            Flash::error('Pedido not found');

            return redirect(route('pedidos.index'));
        }

        $this->pedidoRepository->delete($id);

        Flash::success('Pedido deleted successfully.');

        return redirect(route('pedidos.index'));
    }

    public function procesar(TempPedido $tempPedido,CreatePedidoRequest $request){

//        dd('Procesar pedido',$request->all());

        //Crea colección de objetos en base a detalles temporales
        $detalles = $tempPedido->tempPedidoDetalles->map(function ($item) {
            return new PedidoDetalle($item->toArray());
        });

        //Guarda el encabezado del pedido
        $pedido = $this->pedidoRepository->create($request->all());

        //Guarda los detalles del pedido
        $pedido->pedidoDetalles()->saveMany($detalles);

        //Cambia el estado del pedido temporal
        $tempPedido->procesado=1;
        $tempPedido->save();

        Flash::success('Pedido procesado exitosamente!');

        return redirect(route('pedidos.index'));
    }

    /**
     * Genera un ingreso o compra y lo asocia al pedido
     * @param $id
     * @return mixed
     */
    public function ingreso($id){

        /** @var Pedido $pedido */
        $pedido = $this->pedidoRepository->findWithoutFail($id);

        $tiendaUser= Auth::user()->empleado->tienda->id;

        if (empty($pedido)) {
            return $this->sendError('Pedido no encontrado');
        }

        if ($pedido->pestado_id==2){
            return $this->sendError('Pedido ya ingresado');
        }

        //Crea colección de objetos en base a detalles temporales
        $detalles = $pedido->pedidoDetalles->map(function ($item) {
            return new CompraDetalle($item->toArray());
        });

        //Guarda el encabezado de la compra
        $compra = Compra::create([
            'tcomprobante_id' => 2,
            'fecha' => hoyDb(),
        ]);

        //Guarda los detalles de la compra
        $compra->compraDetalles()->saveMany($detalles);

        $stock = new Stock();
        foreach ($detalles as $detalle){
            $stock->ingreso($detalle->item_id,$detalle->cantidad,$detalle->id,null,null,$tiendaUser);
        }

        //Asocia compra al pedido
        $pedido->compras()->syncWithoutDetaching([$compra->id]);

        $pedido->pestado_id=2;
        $pedido->fecha_ingreso=fechaHoraActualDb();
        $pedido->save();

        return $this->sendResponse($pedido->toArray(), 'Pedido ingresado correctamente');

    }

    /**
     * Genera un ingreso o compra y lo asocia al pedido
     * @param $id
     * @return mixed
     */
    public function entrega($id){

        /** @var Pedido $pedido */
        $pedido = $this->pedidoRepository->findWithoutFail($id);

        if (empty($pedido)) {
            return $this->sendError('Pedido no encontrado');
        }

        if ($pedido->pestado_id==3){
            return $this->sendError('Pedido ya entregado');
        }

        $user=Auth::user();
        $userCajaAbierta = $user->cajaAbierta();
        $tiendaUser= $user->empleado->tienda->id;

        //si el usuario no tiene caja abierta
        if(!$userCajaAbierta){
            return $this->sendError('La entrega del pedido genera una venta, por lo que es necesario la apertura de una caja primero');
        }

        //Crea colección de objetos en base a detalles temporales
        $detalles = $pedido->pedidoDetalles->map(function ($item) {
            return new VentaDetalle($item->toArray());
        });

        $datos = [
            'cliente_id' => $pedido->cliente_id,
            'fecha' => hoyDb(),
            'serie' => null,
            'numero' => null,
            'user_id' => $user->id,
            'recibido' => 0,
            'caja_id' => $userCajaAbierta->id
        ];

        //Guarda el encabezado de la compra
        $venta = Venta::create($datos);

        //Guarda los detalles de la compra
        $venta->ventaDetalles()->saveMany($detalles);

        $stock = new Stock();
        foreach ($detalles as $detalle){
            $stock->egreso($detalle->item_id,$detalle->cantidad,$detalle->id,$tiendaUser);
        }

        //Asocia compra al pedido
        $pedido->ventas()->syncWithoutDetaching([$venta->id]);

        $pedido->pestado_id=3;
        $pedido->fecha_entrega=fechaHoraActualDb();
        $pedido->save();

        return $this->sendResponse($pedido->toArray(), 'Pedido entregado correctamente');

    }

    public function fpdf(Pedido $pedido){

        return view('fpdf.pedidos.'.config('app.fpdf_pedido'),compact('pedido'));

    }
}
