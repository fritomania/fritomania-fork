<?php

namespace App\Http\Controllers;

use App\DataTables\ProduccionTipoDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateProduccionTipoRequest;
use App\Http\Requests\UpdateProduccionTipoRequest;
use App\Repositories\ProduccionTipoRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class ProduccionTipoController extends AppBaseController
{
    /** @var  ProduccionTipoRepository */
    private $produccionTipoRepository;

    public function __construct(ProduccionTipoRepository $produccionTipoRepo)
    {
        $this->produccionTipoRepository = $produccionTipoRepo;
        $this->middleware('auth');
    }

    /**
     * Display a listing of the ProduccionTipo.
     *
     * @param ProduccionTipoDataTable $produccionTipoDataTable
     * @return Response
     */
    public function index(ProduccionTipoDataTable $produccionTipoDataTable)
    {
        return $produccionTipoDataTable->render('produccion_tipos.index');
    }

    /**
     * Show the form for creating a new ProduccionTipo.
     *
     * @return Response
     */
    public function create()
    {
        return view('produccion_tipos.create');
    }

    /**
     * Store a newly created ProduccionTipo in storage.
     *
     * @param CreateProduccionTipoRequest $request
     *
     * @return Response
     */
    public function store(CreateProduccionTipoRequest $request)
    {
        $input = $request->all();

        $produccionTipo = $this->produccionTipoRepository->create($input);

        Flash::success('Produccion Tipo guardado exitosamente.');

        return redirect(route('produccionTipos.index'));
    }

    /**
     * Display the specified ProduccionTipo.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $produccionTipo = $this->produccionTipoRepository->findWithoutFail($id);

        if (empty($produccionTipo)) {
            Flash::error('Produccion Tipo no encontrado');

            return redirect(route('produccionTipos.index'));
        }

        return view('produccion_tipos.show',compact('produccionTipo'));
    }

    /**
     * Show the form for editing the specified ProduccionTipo.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $produccionTipo = $this->produccionTipoRepository->findWithoutFail($id);

        if (empty($produccionTipo)) {
            Flash::error('Produccion Tipo no encontrado');

            return redirect(route('produccionTipos.index'));
        }

        return view('produccion_tipos.edit',compact('produccionTipo'));
    }

    /**
     * Update the specified ProduccionTipo in storage.
     *
     * @param  int              $id
     * @param UpdateProduccionTipoRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateProduccionTipoRequest $request)
    {
        $produccionTipo = $this->produccionTipoRepository->findWithoutFail($id);

        if (empty($produccionTipo)) {
            Flash::error('Produccion Tipo no encontrado');

            return redirect(route('produccionTipos.index'));
        }

        $produccionTipo = $this->produccionTipoRepository->update($request->all(), $id);

        Flash::success('Produccion Tipo actualizado exitosamente.');

        return redirect(route('produccionTipos.index'));
    }

    /**
     * Remove the specified ProduccionTipo from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $produccionTipo = $this->produccionTipoRepository->findWithoutFail($id);

        if (empty($produccionTipo)) {
            Flash::error('Produccion Tipo no encontrado');

            return redirect(route('produccionTipos.index'));
        }

        $this->produccionTipoRepository->delete($id);

        Flash::success('Produccion Tipo eliminado exitosamente.');

        return redirect(route('produccionTipos.index'));
    }
}
