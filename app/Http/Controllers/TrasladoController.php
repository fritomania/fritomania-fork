<?php

namespace App\Http\Controllers;

use App\Http\Requests\TrasladoFormRequest;
use App\Models\Cestado;
use App\Models\Cliente;
use App\Models\CompraDetalle;
use App\Models\Equivalencia;
use App\Models\Item;
use App\Models\Proveedor;
use App\Models\Stock;
use App\Models\Tcomprobante;
use App\Models\TipoPago;
use App\Models\VentaDetalle;
use App\Models\Vestado;
use App\Repositories\CompraRepository;
use App\Repositories\EquivalenciaRepository;
use App\Repositories\VentaRepository;
use App\Venta;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Laracasts\Flash\Flash;

class TrasladoController extends Controller
{
    private $equivalenciaRepository;
    private $ventaRepo;
    private $compraRepo;

    /**
     * TrasladoController constructor.
     * @param $quivalenciaRepository
     */
    public function __construct(EquivalenciaRepository $equivalenciaRepository,VentaRepository $ventaRepository,CompraRepository $compraRepository)
    {
        $this->equivalenciaRepository = $equivalenciaRepository;
        $this->ventaRepo = $ventaRepository;
        $this->compraRepo = $compraRepository;
        $this->middleware('auth');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('traslados.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TrasladoFormRequest $request)
    {

        $user=Auth::user();
        $cajaUser = $user->cajaAbierta();

        //si el usuario no tiene caja abierta
        if(!$cajaUser){
            flash('Debe abrir primero una caja antes de poder realizar un traslado')->error()->important();
            return redirect(route('cajas.create'));
        }

        $item_origen = Item::find($request->item_origen);
        $cant_origen = $request->cantidad_origen;


        if ($item_origen->stockTienda() < $cant_origen){

            $unimed = $item_origen->unimed->nombre;
            $plural = $cant_origen > 1 ? '(s)' : '';

            flash('Trata de trasladar '.$request->cantidad_origen.' '.$unimed.$plural.' '.$item_origen->nombre.' pero solo cuenta con '.$item_origen->stockTienda())->error()->important();
            return redirect()->back();
        }


        try {
            DB::beginTransaction();

            $this->procesar($request);

        } catch (\Exception $exception) {
            DB::rollBack();
            if($user->isDev()){
                dd($exception->getMessage(),'linea: '.$exception->getLine());
            }

            $msg = $user->isAdmin() ? $exception->getMessage() : 'Hubo un error intente de nuevo';

            flash('Error: '.$msg)->error()->important();
            return redirect()->back();
        }


        DB::commit();



        Flash::success('Traslado realizado con éxito.');

        return redirect(route('traslados.index'));

    }


    public function procesar(Request $request)
    {

        $stock = new Stock();
        $tienda = session('tienda');

        $user=Auth::user();
        $cajaUser = $user->cajaAbierta();

        $item_origen = $request->item_origen;
        $item_destino = $request->item_destino;
        $cant_origen = $request->cantidad_origen;
        $cant_destino = $request->cantidad_destino;

        $equivalente = $request->cantidad_destino/$request->cantidad_origen;

        $equivalencia = Equivalencia::where('item_origen',$item_origen)->where('item_destino',$item_destino)->first();

        //si no existe equivalencia registrada en la db
        if (empty($equivalencia)){
            //registra la equivalencia
            $equivalencia = $this->equivalenciaRepository->create([
                'item_origen' => $item_origen,
                'item_destino' => $item_destino,
                'cantidad' => $equivalente
            ]);
        }
        //de lo contrario modifica la equivalencia existente
        else{
            $equivalencia->item_origen = $item_origen;
            $equivalencia->item_destino = $item_destino;
            $equivalencia->cantidad = $equivalente;
            $equivalencia->save();
        }

        //Registra el encabezado de la venta
        $venta = $this->ventaRepo->create([
            'cliente_id' => Cliente::NEGOCIO_MISMO,
            'fecha' => hoyDb(),
            'vestado_id' => Vestado::PAGADA,
            'vestado_id' => Vestado::PAGADA,
            'observaciones' => 'Traslado de stock',
            'tipo_pago_id' => TipoPago::EFECTIVO,
            'caja_id' => $cajaUser->id,
            'tienda_id' => session('tienda'),
            'user_id' => $user->id
        ]);


        if($venta){

            $detalleVenta = new VentaDetalle([
                'item_id' => $item_origen,
                'cantidad' => $cant_origen,
                'precio' => 0
            ]);

            //registra el detalle de la venta
            $venta->ventaDetalles()->saveMany([$detalleVenta]);

            //rebaja stock articulo origen
            $egreso = $stock->egreso($detalleVenta->item->id,$detalleVenta->cantidad,$detalleVenta->id,$tienda);

            if (!$egreso){
                throw new Exception('No se realizó el egreso de stock');
            }

            //registra el encabezado de la compra
            $compra = $this->compraRepo->create([
                'proveedor_id' => Proveedor::TRASLADOS_Y_OTROS,
                'fecha' => hoyDb(),
                'fecha_ingreso' => hoyDb(),
                'fecha_ingreso_plan' => hoyDb(),
                'tcomprobante_id' => Tcomprobante::TRASLADO,
                'user_id' => $user->id,
                'tienda_id' => session('tienda'),
                'cestado_id' => Cestado::RECIBIDA,
            ]);

            if($compra){

                $detalleCompra = new CompraDetalle([
                    'item_id' => $item_destino,
                    'cantidad' => $cant_destino,
                    'precio' => 0
                ]);

                //registra el detalle de la compra
                $compra->compraDetalles()->saveMany([$detalleCompra]);

                //asocia la venta a la compra
                $venta->compras()->syncWithoutDetaching([$compra->id]);

                //incrementa stock articulo destino
                $ingreso = $stock->ingreso($detalleCompra->item->id,$detalleCompra->cantidad,$detalleCompra->id,null,null,$tienda );

                if (!$ingreso){
                    throw new Exception('No se realizó el ingreso de stock');
                }
            }
        }
    }


}
