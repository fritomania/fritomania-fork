<?php

namespace App\Http\Controllers;

use App\DataTables\ProduccionEstadoDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateProduccionEstadoRequest;
use App\Http\Requests\UpdateProduccionEstadoRequest;
use App\Repositories\ProduccionEstadoRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class ProduccionEstadoController extends AppBaseController
{
    /** @var  ProduccionEstadoRepository */
    private $produccionEstadoRepository;

    public function __construct(ProduccionEstadoRepository $produccionEstadoRepo)
    {
        $this->produccionEstadoRepository = $produccionEstadoRepo;
        $this->middleware('auth');
    }

    /**
     * Display a listing of the ProduccionEstado.
     *
     * @param ProduccionEstadoDataTable $produccionEstadoDataTable
     * @return Response
     */
    public function index(ProduccionEstadoDataTable $produccionEstadoDataTable)
    {
        return $produccionEstadoDataTable->render('produccion_estados.index');
    }

    /**
     * Show the form for creating a new ProduccionEstado.
     *
     * @return Response
     */
    public function create()
    {
        return view('produccion_estados.create');
    }

    /**
     * Store a newly created ProduccionEstado in storage.
     *
     * @param CreateProduccionEstadoRequest $request
     *
     * @return Response
     */
    public function store(CreateProduccionEstadoRequest $request)
    {
        $input = $request->all();

        $produccionEstado = $this->produccionEstadoRepository->create($input);

        Flash::success('Produccion Estado guardado exitosamente.');

        return redirect(route('produccionEstados.index'));
    }

    /**
     * Display the specified ProduccionEstado.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $produccionEstado = $this->produccionEstadoRepository->findWithoutFail($id);

        if (empty($produccionEstado)) {
            Flash::error('Produccion Estado no encontrado');

            return redirect(route('produccionEstados.index'));
        }

        return view('produccion_estados.show',compact('produccionEstado'));
    }

    /**
     * Show the form for editing the specified ProduccionEstado.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $produccionEstado = $this->produccionEstadoRepository->findWithoutFail($id);

        if (empty($produccionEstado)) {
            Flash::error('Produccion Estado no encontrado');

            return redirect(route('produccionEstados.index'));
        }

        return view('produccion_estados.edit',compact('produccionEstado'));
    }

    /**
     * Update the specified ProduccionEstado in storage.
     *
     * @param  int              $id
     * @param UpdateProduccionEstadoRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateProduccionEstadoRequest $request)
    {
        $produccionEstado = $this->produccionEstadoRepository->findWithoutFail($id);

        if (empty($produccionEstado)) {
            Flash::error('Produccion Estado no encontrado');

            return redirect(route('produccionEstados.index'));
        }

        $produccionEstado = $this->produccionEstadoRepository->update($request->all(), $id);

        Flash::success('Produccion Estado actualizado exitosamente.');

        return redirect(route('produccionEstados.index'));
    }

    /**
     * Remove the specified ProduccionEstado from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $produccionEstado = $this->produccionEstadoRepository->findWithoutFail($id);

        if (empty($produccionEstado)) {
            Flash::error('Produccion Estado no encontrado');

            return redirect(route('produccionEstados.index'));
        }

        $this->produccionEstadoRepository->delete($id);

        Flash::success('Produccion Estado eliminado exitosamente.');

        return redirect(route('produccionEstados.index'));
    }
}
