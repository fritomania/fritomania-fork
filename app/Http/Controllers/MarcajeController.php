<?php

namespace App\Http\Controllers;

use App\DataTables\MarcajeDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateMarcajeRequest;
use App\Http\Requests\UpdateMarcajeRequest;
use App\Models\Marcaje;
use App\Repositories\MarcajeRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class MarcajeController extends AppBaseController
{
    /** @var  MarcajeRepository */
    private $marcajeRepository;

    public function __construct(MarcajeRepository $marcajeRepo)
    {
        $this->marcajeRepository = $marcajeRepo;
        $this->middleware('auth');
    }

    /**
     * Display a listing of the Marcaje.
     *
     * @param MarcajeDataTable $marcajeDataTable
     * @return Response
     */
    public function index(MarcajeDataTable $marcajeDataTable)
    {
        return $marcajeDataTable->render('marcajes.index');
    }

    /**
     * Show the form for creating a new Marcaje.
     *
     * @return Response
     */
    public function create()
    {
        return view('marcajes.create');
    }

    /**
     * Store a newly created Marcaje in storage.
     *
     * @param CreateMarcajeRequest $request
     *
     * @return Response
     */
    public function store(CreateMarcajeRequest $request)
    {
        $input = $request->all();

        $valida = Marcaje::where('fecha',$request->fecha)
            ->where('tipo_marcaje_id',$request->tipo_marcaje_id)
            ->where('user_id', $request->user_id)->first();

//        dd($valida->toArray());

        if(!$valida){
            $marcaje = $this->marcajeRepository->create($input);


            Flash::success('Marcaje guardado exitosamente.');
        }else{
            Flash::warning('Ya existe este marcaje.');
        }


        return redirect(route('marcajes.index'));
    }

    /**
     * Display the specified Marcaje.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $marcaje = $this->marcajeRepository->findWithoutFail($id);

        if (empty($marcaje)) {
            Flash::error('Marcaje no encontrado');

            return redirect(route('marcajes.index'));
        }

        return view('marcajes.show',compact('marcaje'));
    }

    /**
     * Show the form for editing the specified Marcaje.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $marcaje = $this->marcajeRepository->findWithoutFail($id);

        if (empty($marcaje)) {
            Flash::error('Marcaje no encontrado');

            return redirect(route('marcajes.index'));
        }

        return view('marcajes.edit',compact('marcaje'));
    }

    /**
     * Update the specified Marcaje in storage.
     *
     * @param  int              $id
     * @param UpdateMarcajeRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateMarcajeRequest $request)
    {
        $marcaje = $this->marcajeRepository->findWithoutFail($id);

        if (empty($marcaje)) {
            Flash::error('Marcaje no encontrado');

            return redirect(route('marcajes.index'));
        }

        $marcaje = $this->marcajeRepository->update($request->all(), $id);

        Flash::success('Marcaje actualizado exitosamente.');

        return redirect(route('marcajes.index'));
    }

    /**
     * Remove the specified Marcaje from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $marcaje = $this->marcajeRepository->findWithoutFail($id);

        if (empty($marcaje)) {
            Flash::error('Marcaje no encontrado');

            return redirect(route('marcajes.index'));
        }

        $this->marcajeRepository->delete($id);

        Flash::success('Marcaje eliminado exitosamente.');

        return redirect(route('marcajes.index'));
    }
}
