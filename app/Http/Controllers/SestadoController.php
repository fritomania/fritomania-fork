<?php

namespace App\Http\Controllers;

use App\DataTables\SestadoDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateSestadoRequest;
use App\Http\Requests\UpdateSestadoRequest;
use App\Repositories\SestadoRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class SestadoController extends AppBaseController
{
    /** @var  SestadoRepository */
    private $sestadoRepository;

    public function __construct(SestadoRepository $sestadoRepo)
    {
        $this->middleware('auth');
        $this->sestadoRepository = $sestadoRepo;
    }

    /**
     * Display a listing of the Sestado.
     *
     * @param SestadoDataTable $sestadoDataTable
     * @return Response
     */
    public function index(SestadoDataTable $sestadoDataTable)
    {
        return $sestadoDataTable->render('sestados.index');
    }

    /**
     * Show the form for creating a new Sestado.
     *
     * @return Response
     */
    public function create()
    {
        return view('sestados.create');
    }

    /**
     * Store a newly created Sestado in storage.
     *
     * @param CreateSestadoRequest $request
     *
     * @return Response
     */
    public function store(CreateSestadoRequest $request)
    {
        $input = $request->all();

        $sestado = $this->sestadoRepository->create($input);

        Flash::success('Sestado saved successfully.');

        return redirect(route('sestados.index'));
    }

    /**
     * Display the specified Sestado.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $sestado = $this->sestadoRepository->findWithoutFail($id);

        if (empty($sestado)) {
            Flash::error('Sestado not found');

            return redirect(route('sestados.index'));
        }

        return view('sestados.show')->with('sestado', $sestado);
    }

    /**
     * Show the form for editing the specified Sestado.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $sestado = $this->sestadoRepository->findWithoutFail($id);

        if (empty($sestado)) {
            Flash::error('Sestado not found');

            return redirect(route('sestados.index'));
        }

        return view('sestados.edit')->with('sestado', $sestado);
    }

    /**
     * Update the specified Sestado in storage.
     *
     * @param  int              $id
     * @param UpdateSestadoRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSestadoRequest $request)
    {
        $sestado = $this->sestadoRepository->findWithoutFail($id);

        if (empty($sestado)) {
            Flash::error('Sestado not found');

            return redirect(route('sestados.index'));
        }

        $sestado = $this->sestadoRepository->update($request->all(), $id);

        Flash::success('Sestado updated successfully.');

        return redirect(route('sestados.index'));
    }

    /**
     * Remove the specified Sestado from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $sestado = $this->sestadoRepository->findWithoutFail($id);

        if (empty($sestado)) {
            Flash::error('Sestado not found');

            return redirect(route('sestados.index'));
        }

        $this->sestadoRepository->delete($id);

        Flash::success('Sestado deleted successfully.');

        return redirect(route('sestados.index'));
    }
}
