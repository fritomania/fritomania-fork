<?php

namespace App\Http\Controllers;

use App;
use App\DataTables\CompraPagarDataTable;
use App\DataTables\CompraDataTable;
use App\DataTables\Scopes\CompraEstadoScopeDt;
use App\DataTables\Scopes\ScopeEntreFechasCompraDataTable;
use App\DataTables\Scopes\ScopeItemCompraDataTable;
use App\DataTables\Scopes\ScopeProveedorCompraDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateCompraRequest;
use App\Http\Requests\UpdateCompraRequest;
use App\Mail\OrdenCompraMail;
use App\Mail\RecepcionCompra;
use App\Models\Cestado;
use App\Models\Compra;
use App\Models\CompraDetalle;
use App\Models\Proveedor;
use App\Models\Stock;
use App\Models\Tcomprobante;
use App\Models\TempCompra;
use App\Repositories\CompraRepository;
use App\Repositories\TempCompraRepository;
use Carbon\Carbon;
use DB;
use Flash;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Mail;
use Response;

class CompraController extends AppBaseController
{
    /** @var  CompraRepository */
    private $compraRepository;
    private $tempCompraRepository;

    public function __construct(CompraRepository $compraRepo, TempCompraRepository $tempCompraRepoRepo)
    {
        $this->middleware('auth');
        $this->compraRepository = $compraRepo;
        $this->tempCompraRepository = $tempCompraRepoRepo;
    }

    /**
     * Display a listing of the Compra.
     *
     * @param CompraDataTable $compraDataTable
     * @return Response
     */
    public function index(CompraDataTable $compraDataTable,Request $request)
    {

        $del = $request->del ? $request->del : fechaDb(iniMes());
        $al = $request->al ? $request->al : hoyDb();
        $proveedor_id = $request->proveedor_id ? $request->proveedor_id : null;
        $item_id = $request->item_id ? $request->item_id : null;
        $estado = $request->estado ?? null;

        if($proveedor_id){
            $compraDataTable->addScope(new ScopeProveedorCompraDataTable($proveedor_id));
        }

        if($item_id){
            $compraDataTable->addScope(new ScopeItemCompraDataTable($item_id));
        }

        if($del && $al){
            $compraDataTable->addScope(new ScopeEntreFechasCompraDataTable($del,$al));
        }

        if ($estado){
            $compraDataTable->addScope(new CompraEstadoScopeDt($estado));
        }

        return $compraDataTable->render('compras.index',compact('del','al','proveedor_id','item_id','estado'));
    }

    /**
     * Muestra el listado del las compras por pagar
     * @param CompraPagarDataTable $compraCobrarDataTable
     * @return \Illuminate\Http\JsonResponse|\Illuminate\View\View
     */
    public function pagar(CompraPagarDataTable $compraCobrarDataTable)
    {
        return $compraCobrarDataTable->render('compras.por_pagar_lista');

    }

    /**
     * Show the form for creating a new Compra.
     *
     * @return Response
     */
    public function create()
    {

        $user=Auth::user();

        $tempCompra = TempCompra::where('procesada',0)
            ->where('user_id',$user->id)
            ->get();

        ///si el usuario no tiene ninguna compra creada
        if($tempCompra->count()>1) {
            dd('el usuario tiene '.$tempCompra->count().' compras temporales');
        }

        if($tempCompra->count()==0){

            $tempCompra = TempCompra::create(['user_id' => $user->id]);

        }else{
            $tempCompra = $tempCompra[0];
        }

        $proveedores = Proveedor::pluck('nombre','id')->toArray();
        $tiposComprobantes = Tcomprobante::pluck('nombre','id')->toArray();

        return view('compras.create',compact('proveedores','tiposComprobantes','tempCompra'));

    }

    /**
     * Store a newly created Compra in storage.
     *
     * @param CreateCompraRequest $request
     *
     * @return Response
     */
    public function store(CreateCompraRequest $request)
    {

    }

    /**
     * Display the specified Compra.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $compra = $this->compraRepository->findWithoutFail($id);

        if (empty($compra)) {
            Flash::error('Compra no existe');

            return redirect(route('compras.index'));
        }

        return view('compras.show')->with('compra', $compra);
    }

    /**
     * Show the form for editing the specified Compra.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $compra = $this->compraRepository->findWithoutFail($id);

        if (empty($compra)) {
            Flash::error('Compra no existe');

            return redirect(route('compras.index'));
        }

        return view('compras.edit')->with('compra', $compra);
    }

    /**
     * Update the specified Compra in storage.
     *
     * @param  int              $id
     * @param UpdateCompraRequest $request
     *
     * @return Response
     */
    public function update($id,UpdateCompraRequest $request)
    {
        $tempCompra = $this->tempCompraRepository->findWithoutFail($id);


        if (empty($tempCompra)) {
            Flash::error('Compra no existe');

            return redirect(route('compras.index'));
        }

        try {
            DB::beginTransaction();

            $this->procesar($tempCompra,$request);

        } catch (\Exception $exception) {
            DB::rollBack();

            if (Auth::user()->isSuperAdmin()){
                dd($exception);
            }

            $msg = Auth::user()->isAdmin() ? $exception->getMessage() : 'Hubo un error intente de nuevo';

            flash('Error: '.$msg)->error()->important();
            return redirect()->back();
        }

        DB::commit();

        Flash::success('Listo! compra procesada.');

        return redirect(route('compras.index'));

    }

    /**
     * Remove the specified Compra from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $compra = $this->compraRepository->findWithoutFail($id);

        if (empty($compra)) {
            Flash::error('Compra no existe');

            return redirect(route('compras.index'));
        }

        $this->compraRepository->delete($id);

        Flash::success('Compra eliminado exitosamente.');

        return redirect(route('compras.index'));
    }

    public function procesar(TempCompra $tempCompra,UpdateCompraRequest $request){

//        dd($request->all());

        //Crea colección de objetos en base a detalles temporales
        $detalles = $tempCompra->tempCompraDetalles->map(function ($item) {
            return new CompraDetalle($item->toArray());
        });

        //Guarda el encabezado de la compra
        $compra = $this->compraRepository->create($request->all());

        //Guarda los detalles de la compra
        $compra->compraDetalles()->saveMany($detalles);

        //Cambia el estado de la compra temporal
        $tempCompra->procesada=1;
        $tempCompra->save();

        //si hay un abono inicial lo guarda
        $this->abono($compra,$request->monto_ini);

        if($compra){
            Mail::send(new OrdenCompraMail($compra));
        }

        return $compra;
    }

    public function cancelar(TempCompra $tempCompra){

        $tempCompra->delete();

        Flash::success('Listo! compra cancelada.');

        return redirect(route('compras.create'));
    }

    public function anular(Compra $compra){


        $lotesAfectados = $this->lotesAfectados($compra)->pluck('id')->toArray();

        $solicitudes = App\Models\Solicitude::deLotes($lotesAfectados)->entregadasDespuesDe($compra->fecha_ingreso)->get();

        if ($solicitudes->count() > 0){

            $linksSolicitudes = "<ul>";

            foreach ($solicitudes as $solicitude){
                $linksSolicitudes .= "<li> <a href='".route("solicitudes.show",$solicitude->id)."' target='_blank'> ".$solicitude->numero. " </a> </li>";
            }

            $linksSolicitudes.="</ul>";


            flash("
                    <b>No se puede anulara esta compra</b><br>   
                    Existes solicitudes de stock que se realizaron posterior a su ingreso y con artículos que contiene la misma
                    
                    <p>$linksSolicitudes</p>
            ")->important()->warning();

            return redirect()->back();
        }



        try {
            DB::beginTransaction();

            $this->procesarAnular($compra);

        } catch (\Exception $exception) {
            DB::rollBack();

            if (Auth::user()->isSuperAdmin()){
                dd($exception,$exception->getLine());
            }

            $msg = Auth::user()->isAdmin() ? $exception->getMessage() : 'Hubo un error intente de nuevo';

            flash('Error: '.$msg)->error()->important();
            return redirect()->back();
        }


        DB::commit();

        Flash::success('Listo! compra anulada.');

        return redirect(route('compras.index'));
    }

    public function cancelarSolicictud(Compra $compra){

//        dd('Cancelar solicitud',$compra);
        $compra->cestado_id = Cestado::CANCELADA;
        $compra->fecha_cancela = Carbon::now();
        $compra->save();

        Flash::success('Listo! Solicitud Cancelada.');

        return redirect(route('compras.index'));
    }

    /**
     * Inserta pago en la tabla cpagos
     * @param Compra $compra
     * @param $monto
     */
    public function abono(Compra $compra,$monto)
    {
        //Si la compra no es al crédito o el monto es null
        if(!$compra->credito || is_null($monto)) return;

        $compra->cpagos()->create([
            'compra_id' => $compra->id,
            'monto' => $monto,
            'user_id' => Auth::user()->id
        ]);
    }

    /**
     * Display the specified Compra.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function abonarView($id)
    {
        $compra = $this->compraRepository->findWithoutFail($id);

        if (empty($compra)) {
            Flash::error('Compra no existe');

            return redirect(route('compras.pagar'));
        }

        return view('compras.por_pagar_abonar')->with('compra', $compra);
    }

    public function ingreso($id){

        $compra = Compra::find($id);

        $detalles = $compra->compraDetalles;

        $stock = new Stock();
        foreach ($detalles as $detalle){
            $stock->ingreso($detalle->item_id,$detalle->cantidad,$detalle->id,'',$detalle->fecha_ven);
        }

        $compra->cestado_id = Cestado::RECIBIDA;
        $compra->fecha_ingreso = hoyDb();

        $compra->save();

        flash('Ingreso Realizado')->success();

        if($compra){
            Mail::send(new RecepcionCompra($compra));
        }

        return redirect(route('compras.index'));

    }

    public function pdf(Compra $compra){

//        dd('este es el metodo que genera el pdf', $compra->toArray());

        $pdf = App::make('snappy.pdf.wrapper');

        $view = \View::make('compras.pdf', compact('compra'))->render();
        $footer = \View::make('compras.pdf_footer')->render();

        $pdf->loadHTML($view)
            ->setPaper('letter')
            ->setOrientation('portrait')
            ->setOption('footer-html',utf8_decode($footer))
            ->setOption('margin-top',2)
            ->setOption('margin-bottom',10)
            ->setOption('margin-left',2)
            ->setOption('margin-right',2)
            ->stream('report.pdf');
        return $pdf->inline();

    }

    public function rptComprasDiarias(){

        $diasMes=diasMesActual();
        $recibidas = Cestado::RECIBIDA;

        $results = DB::select( DB::raw("
            select 
                date(c.fecha_ingreso) dia,sum((d.cantidad* d.precio)) monto
            from 
                compras c inner join compra_detalles d on c.id= d.compra_id
            where
                month(c.fecha_ingreso)=MONTH(CURDATE())
                and c.cestado_id  in($recibidas)
                and c.deleted_at IS NULL
            group by
                1
        ") );

        return view('reportes.compras.rpt_compras_dia', compact('results'));
    }

    public function procesarAnular(Compra $compra)
    {
        $compra->cestado_id = Cestado::ANULADA;
        $compra->fecha_anula = Carbon::now();
        $compra->save();

        $compra->pagos()->delete();

        //Regresa las cantidades de los lotes en tabla stocks
        foreach ($compra->detalles as $det){
            foreach ($det->stocks as $stock){
                $stock->cantidad = $stock->cantidad - $stock->pivot->cantidad;
                $stock->save();
            }
        }
    }

    public function lotesAfectados(Compra $compra)
    {
        $afectados = collect();

        foreach ($compra->detalles as $detalle){
            foreach ($detalle->stocks as $lote){
                $afectados->push($lote) ;
            }
        }

        return $afectados;

    }
}
