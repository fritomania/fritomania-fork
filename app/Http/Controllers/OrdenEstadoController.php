<?php

namespace App\Http\Controllers;

use App\DataTables\OrdenEstadoDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateOrdenEstadoRequest;
use App\Http\Requests\UpdateOrdenEstadoRequest;
use App\Repositories\OrdenEstadoRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class OrdenEstadoController extends AppBaseController
{
    /** @var  OrdenEstadoRepository */
    private $ordenEstadoRepository;

    public function __construct(OrdenEstadoRepository $ordenEstadoRepo)
    {
        $this->ordenEstadoRepository = $ordenEstadoRepo;
        $this->middleware('auth');
    }

    /**
     * Display a listing of the OrdenEstado.
     *
     * @param OrdenEstadoDataTable $ordenEstadoDataTable
     * @return Response
     */
    public function index(OrdenEstadoDataTable $ordenEstadoDataTable)
    {
        return $ordenEstadoDataTable->render('orden_estados.index');
    }

    /**
     * Show the form for creating a new OrdenEstado.
     *
     * @return Response
     */
    public function create()
    {
        return view('orden_estados.create');
    }

    /**
     * Store a newly created OrdenEstado in storage.
     *
     * @param CreateOrdenEstadoRequest $request
     *
     * @return Response
     */
    public function store(CreateOrdenEstadoRequest $request)
    {
        $input = $request->all();

        $ordenEstado = $this->ordenEstadoRepository->create($input);

        Flash::success('Orden Estado guardado exitosamente.');

        return redirect(route('ordenEstados.index'));
    }

    /**
     * Display the specified OrdenEstado.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $ordenEstado = $this->ordenEstadoRepository->findWithoutFail($id);

        if (empty($ordenEstado)) {
            Flash::error('Orden Estado no encontrado');

            return redirect(route('ordenEstados.index'));
        }

        return view('orden_estados.show',compact('ordenEstado'));
    }

    /**
     * Show the form for editing the specified OrdenEstado.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $ordenEstado = $this->ordenEstadoRepository->findWithoutFail($id);

        if (empty($ordenEstado)) {
            Flash::error('Orden Estado no encontrado');

            return redirect(route('ordenEstados.index'));
        }

        return view('orden_estados.edit',compact('ordenEstado'));
    }

    /**
     * Update the specified OrdenEstado in storage.
     *
     * @param  int              $id
     * @param UpdateOrdenEstadoRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateOrdenEstadoRequest $request)
    {
        $ordenEstado = $this->ordenEstadoRepository->findWithoutFail($id);

        if (empty($ordenEstado)) {
            Flash::error('Orden Estado no encontrado');

            return redirect(route('ordenEstados.index'));
        }

        $ordenEstado = $this->ordenEstadoRepository->update($request->all(), $id);

        Flash::success('Orden Estado actualizado exitosamente.');

        return redirect(route('ordenEstados.index'));
    }

    /**
     * Remove the specified OrdenEstado from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $ordenEstado = $this->ordenEstadoRepository->findWithoutFail($id);

        if (empty($ordenEstado)) {
            Flash::error('Orden Estado no encontrado');

            return redirect(route('ordenEstados.index'));
        }

        $this->ordenEstadoRepository->delete($id);

        Flash::success('Orden Estado eliminado exitosamente.');

        return redirect(route('ordenEstados.index'));
    }
}
