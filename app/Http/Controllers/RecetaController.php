<?php

namespace App\Http\Controllers;

use App\DataTables\RecetaDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateRecetaRequest;
use App\Http\Requests\UpdateRecetaRequest;
use App\Models\Item;
use App\Models\RecetaDetalle;
use App\Repositories\RecetaRepository;
use Auth;
use Flash;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Response;

class RecetaController extends AppBaseController
{
    /** @var  RecetaRepository */
    private $recetaRepository;

    public function __construct(RecetaRepository $recetaRepo)
    {
        $this->recetaRepository = $recetaRepo;
        $this->middleware('auth');
        $this->middleware('admin',['only' => ['create', 'destroy']]);
    }

    /**
     * Display a listing of the Receta.
     *
     * @param RecetaDataTable $recetaDataTable
     * @return Response
     */
    public function index(RecetaDataTable $recetaDataTable)
    {
        return $recetaDataTable->render('recetas.index');
    }

    /**
     * Show the form for creating a new Receta.
     *
     * @return Response
     */
    public function create(Request $request)
    {
//        session()->forget('receta');
        if ( ! session()->has('receta')) {
            session()->put('receta', new Collection());
            session()->save();
        }

        return view('recetas.create');
    }

    /**
     * Store a newly created Receta in storage.
     *
     * @param CreateRecetaRequest $request
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $recetaSession = session('receta');

        if(!$recetaSession){
            flash('No hay receta en sessión')->error()->important();

            return redirect(route('recetas.create'));
        }

        $item = $recetaSession->get('item');

        $ingredientes = $recetaSession->get('detalles');

        $receta = $this->recetaRepository->create([
            'item_id' => $item['id'],
            'cantidad' => $request->cantidad,
            'user_id' => Auth::user()->id
        ]);

        $detalles = $ingredientes->map(function ($item) use ($receta){
            return new RecetaDetalle([
                'item_id' => $item->id,
                'receta_id' => $receta->id,
                'cantidad' => $item->cantidad
            ]);
        });

        $receta->recetaDetalles()->saveMany($detalles);

        Flash::success('Receta guardado exitosamente.');

        session()->forget('receta');


        return redirect(route('recetas.index'));
    }

    /**
     * Display the specified Receta.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $receta = $this->recetaRepository->findWithoutFail($id);

        if (empty($receta)) {
            Flash::error('Receta no encontrado');

            return redirect(route('recetas.index'));
        }

        return view('recetas.show',compact('receta'));
    }

    /**
     * Show the form for editing the specified Receta.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $receta = $this->recetaRepository->findWithoutFail($id);

        if (empty($receta)) {
            Flash::error('Receta no encontrado');

            return redirect(route('recetas.index'));
        }

        $detalles = $receta->detalles->map(function ($det){
            $item = $det->item;
            $item->setAttribute('cantidad', $det->cantidad);
            return $item;
        });

        session()->put('receta',collect());

        session('receta')->put('item',$receta->item);
        session('receta')->put('detalles',$detalles);

        session()->save();

        return view('recetas.create',compact('receta'));
    }

    /**
     * Update the specified Receta in storage.
     *
     * @param  int              $id
     * @param UpdateRecetaRequest $request
     *
     * @return Response
     */
    public function update($id, Request $request)
    {
        $receta = $this->recetaRepository->findWithoutFail($id);

        if (empty($receta)) {
            Flash::error('Receta no encontrado');

            return redirect(route('recetas.index'));
        }

        $datos = $this->validate($request,[
            'item_id' => 'required',
        ]);

        $receta = $this->recetaRepository->update($datos, $id);

        $detallesSesion = session()->get('receta')->get('detalles');

        $detalles = $detallesSesion->map(function ($item) use ($receta){
            return new RecetaDetalle([
                'item_id' => $item->id,
                'receta_id' => $receta->id,
                'cantidad' => $item->cantidad,
            ]);
        });


        $receta->detalles()->delete();
        $receta->detalles()->saveMany($detalles);

        Flash::success('Definición de receta actualizada exitosamente.');

        session()->forget('receta');

        return redirect(route('recetas.index'));
    }

    /**
     * Remove the specified Receta from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $receta = $this->recetaRepository->findWithoutFail($id);

        if (empty($receta)) {
            Flash::error('Receta no encontrado');

            return redirect(route('recetas.index'));
        }

        $this->recetaRepository->delete($id);

        Flash::success('Receta eliminado exitosamente.');

        return redirect(route('recetas.index'));
    }

    public function add(Request $request)
    {

        $receta = session('receta');
        $itemId = $request->item_select;
        $ingrediente = $request->ingrediente;
        $cantidad = $request->cantidad;

        if(! $receta->has('item')){
            $product = Item::find($itemId);

            $receta->put('item',$product->toArray());
        }

        if(! $receta->has('detalles')){

            $receta->put('detalles',collect());

        }

        $detalles = $receta->get('detalles');

        if ( ! $detalles->contains('id', $ingrediente)) {
            $product = Item::find($ingrediente);

            $product->setAttribute('cantidad', $cantidad);
            $detalles->push($product);
        } else {
            return $this->sendError('Ingrediente ya agregado!');
        }

        session()->save();

        return $this->sendResponse($detalles,'Ingrediente agregado!');
    }

    public function remove (Request $request) {

//         return $this->sendResponse($request->all(), 'request');
            $receta = session('receta');
//        $itemId = request('item_id');

        $newCol = collect();
        foreach (session()->get('receta')->get('detalles') as $det){
            if(!($det->id==$request->item_id)){
                $newCol->push($det);
            }
        }

        session()->get('receta')->put('detalles',$newCol);

        return $this->sendResponse($newCol,"Articulo removido!");

    }

    public function getReceta(){
        $receta = session()->get('receta');

        if(!$receta){
            return $this->sendError('No existen receta en sesion');
        }
        return $this->sendResponse($receta,'Receta');
    }

    public function cancelar()
    {
        session()->forget('receta');
        flash('Definición de receta cancelada!!')->important()->success();
        return redirect(route('recetas.index'));
    }
}
