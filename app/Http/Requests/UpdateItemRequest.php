<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\Item;

class UpdateItemRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id= $this->item;
        $rules = Item::$rules;
        $request= $this->request->all();

        $rules['codigo'] = $rules['codigo'] . ',codigo,' . $id;

        //si el código es null elimina las reglas para código
        if(is_null($request['codigo'])){
            unset($rules['codigo']);
//            dd($rules);
        }
//        dd($rules);

        return $rules;
    }

    public function all($keys = null)
    {
        $input = parent::all();

        return $input;
    }
}
