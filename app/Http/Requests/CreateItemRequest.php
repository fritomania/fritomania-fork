<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\Item;

class CreateItemRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $request= $this->request->all();
        $rules = Item::$rules;

        //si el código es null elimina las reglas para código
        if(is_null($request['codigo'])){
            unset($rules['codigo']);
//            dd($rules);
        }

//        dd($rules);

        return $rules;

    }
}
