<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\Descuento;

class UpdateDescuentoRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return Descuento::$rules;
    }

    public function all($keys = null){
        $input = parent::all();

//        dd($input);

        $input['fecha_inicio'] = fechaDb($input['fecha_inicio']);
        $input['fecha_expira'] = fechaDb($input['fecha_expira']);

        return $input;
    }
}
