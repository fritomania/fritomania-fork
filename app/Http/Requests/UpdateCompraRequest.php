<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\Compra;

class UpdateCompraRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(){
        $id = $this->compra;
        $rules = Compra::$rules;
        //$rules['campo'] = $rules['campo'] . ',campo,' . $id;

        return $rules;
    }

    public function all($keys = null){
        $input = parent::all();

        //cambia el formato de la fecha enviada
//        $input['fecha'] = fechaDb($input['fecha']);
//        $input['fecha_limite_credito'] = fechaDb($input['fecha_limite_credito']);
//        $input['fecha_ingreso_plan'] = fechaDb($input['fecha_ingreso_plan']);

        return $input;
    }


}
