<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\ProduccionEstado;

class UpdateProduccionEstadoRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(){
        $id = $this->produccionEstado;
        $rules = ProduccionEstado::$rules;
        $rules['nombre'] = $rules['nombre'] . ',nombre,' . $id;

        return $rules;
    }
}
