<?php

namespace App\Http\Requests\API;

use App\Models\Eatributo;
use InfyOm\Generator\Request\APIRequest;

class UpdateEatributoAPIRequest extends APIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id= $this->eatributo;

        $rules = Eatributo::$rules;
        $rules['nombre'] = $rules['nombre'] . ',nombre,' . $id;

        return $rules;
    }
}
