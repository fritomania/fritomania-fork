<?php

namespace App\Http\Requests\API;

use App\Models\Stipo;
use InfyOm\Generator\Request\APIRequest;

class UpdateStipoAPIRequest extends APIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(){
        $id = $this->stipo;
        $rules = Stipo::$rules;
        $rules['nombre'] = $rules['nombre'] . ',nombre,' . $id;

        return $rules;
    }
}
