<?php

namespace App\Http\Requests\API;

use App\Models\Cliente;
use InfyOm\Generator\Request\APIRequest;

class CreateClienteAPIRequest extends APIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return Cliente::$rules;
    }

    public function all($keys = null){
        $input = parent::all();

        $input['fecha_nacimiento'] = fechaDb($input['fecha_nacimiento']);

        return $input;
    }
}
