<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class SuperAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if (!Auth::user()->isSuperAdmin()) {
            flash('No tiene acceso a esta opción')->warning();
            return redirect(route('dashboard'));
        }

        return $next($request);
    }
}
