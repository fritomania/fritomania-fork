<?php

namespace App\Http\Middleware;

use Auth;
use Closure;

class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!Auth::user()->isAdmin() && !Auth::user()->isAdminTienda()) {
            $previus = session('_previous');
            flash('Solo un administrador puede realizar esta acción')->error()->important();
            return redirect()->to($previus['url']);
        }

        return $next($request);
    }
}
