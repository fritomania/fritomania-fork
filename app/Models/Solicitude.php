<?php

namespace App\Models;

use App\Events\EventSolicitudCreate;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\Solicitude
 *
 * @property int $id
 * @property int|null $tienda_solicita
 * @property int|null $tienda_entrega
 * @property int $numero
 * @property string $observaciones
 * @property int $user_id
 * @property int $user_despacha
 * @property \Carbon\Carbon|null $fecha_despacha
 * @property string|null $fecha_anula
 * @property string|null $fecha_cancela
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property \Carbon\Carbon|null $deleted_at
 * @property int $solicitud_estado_id
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\SolicitudDetalle[] $detalles
 * @property-read \App\Models\SolicitudEstado $estado
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\SolicitudDetalle[] $solicitudDetalles
 * @property-read \App\Models\SolicitudEstado $solicitudEstado
 * @property-read \App\User $user
 * @property-read \App\User|null $userDespacha
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Solicitude onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Solicitude whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Solicitude whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Solicitude whereFechaAnula($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Solicitude whereFechaDespacha($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Solicitude whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Solicitude whereNumero($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Solicitude whereObservaciones($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Solicitude whereSolicitudEstadoId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Solicitude whereTiendaEntrega($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Solicitude whereTiendaSolicita($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Solicitude whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Solicitude whereUserDespacha($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Solicitude whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Solicitude withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Solicitude withoutTrashed()
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Solicitude deLotes($lotes = array())
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Solicitude entregadasDespuesDe($fecha)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Solicitude whereFechaCancela($value)
 */
class Solicitude extends Model
{
    use SoftDeletes;

    public $table = 'solicitudes';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at','fecha_despacha'];

//    protected $dispatchesEvents =[
//        'created' => EventSolicitudCreate::class
//    ];

    public $fillable = [
        'tienda_solicita',
        'tienda_entrega',
        'numero',
        'observaciones',
        'user_id',
        'user_despacha',
        'fecha_despacha',
        'fecha_anula',
        'fecha_cancela',
        'solicitud_estado_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'numero' => 'integer',
        'observaciones' => 'string',
        'user_id' => 'integer',
        'user_despacha' => 'integer',
        'solicitud_estado_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function solicitudEstado()
    {
        return $this->belongsTo(\App\Models\SolicitudEstado::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function user()
    {
        return $this->belongsTo(\App\User::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function userDespacha()
    {
        return $this->belongsTo(\App\User::class, 'user_despacha','id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function solicitudDetalles()
    {
        return $this->hasMany(\App\Models\SolicitudDetalle::class);
    }

    public function getNumeroAttribute($num)
    {
        list($fecha,$hora) = explode(' ',$this->created_at);

        return 'SOL-INV-'.str_replace("-","",$fecha).$num;
    }

    public function tieneStock()
    {
        $result = true;
        $detalles = $this->solicitudDetalles;

        foreach ($detalles as $index => $det) {
            if ($det->cantidad> $det->item->stockTienda()){
                $result = false;
                break;
            }
        }

        return $result;
    }

    public function estado()
    {
        return $this->belongsTo(\App\Models\SolicitudEstado::class, 'solicitud_estado_id', 'id');
    }

    public function detalles()
    {
        return $this->hasMany(SolicitudDetalle::class,'solicitude_id','id');
    }

    public function scopeDeLotes($query,$lotes=array())
    {
        $query->whereIn('id',function ($query) use ($lotes){

            $query->select('solicitude_id')->from('solicitud_detalles')
                ->whereIn('id',function ($query) use ($lotes){

                    $query->select('solicitud_detalle_id')->from('egreso_solicitud')
                        ->whereIn('stock_id',$lotes);

                });
        });
    }

    public function scopeEntregadasDespuesDe($query,$fecha)
    {
        $query->whereBetween('fecha_despacha',[$fecha,Carbon::now()]);
    }
}
