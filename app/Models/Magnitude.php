<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Magnitude
 *
 * @package App\Models
 * @version July 28, 2017, 9:42 am CST
 * @property int $id
 * @property string $nombre
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property \Carbon\Carbon|null $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Unimed[] $unimeds
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Magnitude onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Magnitude whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Magnitude whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Magnitude whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Magnitude whereNombre($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Magnitude whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Magnitude withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Magnitude withoutTrashed()
 * @mixin \Eloquent
 */
class Magnitude extends Model
{
    use SoftDeletes;

    public $table = 'magnitudes';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'nombre'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'nombre' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'nombre' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function unimeds()
    {
        return $this->hasMany(\App\Models\Unimed::class);
    }
}
