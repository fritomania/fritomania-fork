<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Cliente
 *
 * @package App\Models
 * @version October 3, 2017, 7:59 pm CST
 * @property-read mixed $correo
 * @property-read mixed $fecha_nacimiento
 * @property-read mixed $full_name
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Pedido[] $pedidos
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Venta[] $ventas
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Cliente onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Cliente withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Cliente withoutTrashed()
 * @mixin \Eloquent
 * @property int $id
 * @property string $nit
 * @property string $dpi
 * @property string $nombres
 * @property string $apellidos
 * @property string $telefono
 * @property string|null $telefono2
 * @property string $email
 * @property string $genero
 * @property string $direccion
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property \Carbon\Carbon|null $deleted_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cliente whereApellidos($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cliente whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cliente whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cliente whereDireccion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cliente whereDpi($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cliente whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cliente whereFechaNacimiento($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cliente whereGenero($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cliente whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cliente whereNit($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cliente whereNombres($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cliente whereTelefono($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cliente whereTelefono2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cliente whereUpdatedAt($value)
 */
class Cliente extends Model
{
    use SoftDeletes;

    public $table = 'clientes';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    const CF = 1;
    const NEGOCIO_MISMO = 2;

    protected $dates = ['deleted_at'];
    protected $appends = ['full_name','correo'];


    public $fillable = [
        'nit',
        'dpi',
        'nombres',
        'apellidos',
        'telefono',
        'email',
        'genero',
        'fecha_nacimiento',
        'direccion'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'nit' => 'string',
        'dpi' => 'string',
        'nombres' => 'string',
        'apellidos' => 'string',
        'telefono' => 'string',
        'email' => 'string',
        'genero' => 'string',
        //'fecha_nacimiento' => 'date',
        'direccion' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'nit'=>'nullable|unique:clientes',
        'dpi'=>'nullable|numeric|unique:clientes',
        'nombres'=>'required',
        'apellidos'=>'required',
        'telefono'=>'nullable|digits_between:8,12',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function pedidos()
    {
        return $this->hasMany(\App\Models\Pedido::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function ventas()
    {
        return $this->hasMany(\App\Models\Venta::class,'cliente_id','id');
    }

    public function getFullNameAttribute(){
        return $this->attributes['nombres'].' '.$this->attributes['apellidos'];
    }

    public function getFechaNacimientoAttribute($d)
    {
        return fecha($d);
    }

    public function getCorreoAttribute()
    {
        return $this->email;
    }
}
