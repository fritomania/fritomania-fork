<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Unimed
 *
 * @package App\Models
 * @version July 28, 2017, 9:43 am CST
 * @property int $id
 * @property int $magnitude_id
 * @property string $simbolo
 * @property string $nombre
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property \Carbon\Carbon|null $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Item[] $items
 * @property-read \App\Models\Magnitude $magnitude
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Unimed onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Unimed whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Unimed whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Unimed whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Unimed whereMagnitudeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Unimed whereNombre($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Unimed whereSimbolo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Unimed whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Unimed withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Unimed withoutTrashed()
 * @mixin \Eloquent
 */
class Unimed extends Model
{
    use SoftDeletes;

    public $table = 'unimeds';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'magnitude_id',
        'simbolo',
        'nombre'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'magnitude_id' => 'integer',
        'simbolo' => 'string',
        'nombre' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'magnitude_id'=>'required',
        'simbolo'=>'required | max:10',
        'nombre'=>'required | max:45',
    ];

    public static $msg_error = [
        'magnitude_id.required' => 'El campo Magnitud es obligatorio',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function magnitude()
    {
        return $this->belongsTo(\App\Models\Magnitude::class);
    }



    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function items()
    {
        return $this->hasMany(\App\Models\Item::class);
    }


}
