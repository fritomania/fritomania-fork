<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Proveedor
 * @package App\Models
 * @version July 26, 2018, 3:56 pm CST
 *
 * @property \Illuminate\Database\Eloquent\Collection clienteEquipo
 * @property \Illuminate\Database\Eloquent\Collection compraDetalles
 * @property \Illuminate\Database\Eloquent\Collection compraPedidos
 * @property \Illuminate\Database\Eloquent\Collection compraVenta
 * @property \Illuminate\Database\Eloquent\Collection Compra
 * @property \Illuminate\Database\Eloquent\Collection cpagos
 * @property \Illuminate\Database\Eloquent\Collection cpuEquipo
 * @property \Illuminate\Database\Eloquent\Collection cpus
 * @property \Illuminate\Database\Eloquent\Collection egresos
 * @property \Illuminate\Database\Eloquent\Collection equipoAtributos
 * @property \Illuminate\Database\Eloquent\Collection equipoRmodulo
 * @property \Illuminate\Database\Eloquent\Collection equivalencias
 * @property \Illuminate\Database\Eloquent\Collection icategoriaItem
 * @property \Illuminate\Database\Eloquent\Collection ingresos
 * @property \Illuminate\Database\Eloquent\Collection optionUser
 * @property \Illuminate\Database\Eloquent\Collection pedidoDetalles
 * @property \Illuminate\Database\Eloquent\Collection pedidoVentas
 * @property \Illuminate\Database\Eloquent\Collection pedidos
 * @property \Illuminate\Database\Eloquent\Collection programaServicio
 * @property \Illuminate\Database\Eloquent\Collection recetaDetalles
 * @property \Illuminate\Database\Eloquent\Collection recetas
 * @property \Illuminate\Database\Eloquent\Collection rmodulos
 * @property \Illuminate\Database\Eloquent\Collection rolUser
 * @property \Illuminate\Database\Eloquent\Collection servicioArchivos
 * @property \Illuminate\Database\Eloquent\Collection sgastos
 * @property \Illuminate\Database\Eloquent\Collection stocks
 * @property \Illuminate\Database\Eloquent\Collection tempCompraDetalles
 * @property \Illuminate\Database\Eloquent\Collection tempPedidoDetalles
 * @property \Illuminate\Database\Eloquent\Collection tempVentaDetalles
 * @property \Illuminate\Database\Eloquent\Collection ventaDetalles
 * @property \Illuminate\Database\Eloquent\Collection vpagos
 * @property string nit
 * @property string nombre
 * @property string razon_social
 * @property string correo
 * @property string telefono_movil
 * @property string telefono_oficina
 * @property string direccion
 * @property string observaciones
 */
class Proveedor extends Model
{
    use SoftDeletes;

    public $table = 'proveedores';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';
    const TRASLADOS_Y_OTROS = 1;

    protected $dates = ['deleted_at'];


    public $fillable = [
        'nit',
        'nombre',
        'razon_social',
        'correo',
        'telefono_movil',
        'telefono_oficina',
        'direccion',
        'observaciones'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'nit' => 'string',
        'nombre' => 'string',
        'razon_social' => 'string',
        'correo' => 'string',
        'telefono_movil' => 'string',
        'telefono_oficina' => 'string',
        'direccion' => 'string',
        'observaciones' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'telefono' => 'max:8',
        'nombre' => 'required',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function compras()
    {
        return $this->hasMany(\App\Models\Compra::class);
    }
}
