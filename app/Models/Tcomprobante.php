<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Tcomprobante
 *
 * @package App\Models
 * @version March 26, 2017, 3:28 pm CST
 * @property int $id
 * @property string $nombre
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property \Carbon\Carbon|null $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Compra[] $compras
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Tcomprobante onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Tcomprobante whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Tcomprobante whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Tcomprobante whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Tcomprobante whereNombre($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Tcomprobante whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Tcomprobante withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Tcomprobante withoutTrashed()
 * @mixin \Eloquent
 */
class Tcomprobante extends Model
{
    use SoftDeletes;

    public $table = 'tcomprobantes';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    const INGRESO = 1;
    const FACTURA = 2;
    const REGALO = 3;
    const OTROS = 4;
    const TRASLADO = 5;


    protected $dates = ['deleted_at'];


    public $fillable = [
        'nombre'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'nombre' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'nombre' => 'required | max:45'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function compras()
    {
        return $this->hasMany(\App\Models\Compra::class);
    }
}
