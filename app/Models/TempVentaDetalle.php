<?php

namespace App\Models;

use App\Models\Item;
use App\Models\TempVenta;
use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class TempVentaDetalle
 *
 * @package App\Models
 * @version April 15, 2017, 4:59 pm CST
 * @property int $id
 * @property int $temp_venta_id
 * @property int $item_id
 * @property float $cantidad
 * @property float $precio
 * @property float $descuento
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property \Carbon\Carbon|null $deleted_at
 * @property-read mixed $sub_total
 * @property-read mixed $total_modificaciones
 * @property-read \App\Models\Item $item
 * @property-read \App\Models\TempVenta $tempVenta
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\TempVentaDetalle onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TempVentaDetalle whereCantidad($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TempVentaDetalle whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TempVentaDetalle whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TempVentaDetalle whereDescuento($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TempVentaDetalle whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TempVentaDetalle whereItemId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TempVentaDetalle wherePrecio($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TempVentaDetalle whereTempVentaId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TempVentaDetalle whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\TempVentaDetalle withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\TempVentaDetalle withoutTrashed()
 * @mixin \Eloquent
 */
class TempVentaDetalle extends Model
{
    use SoftDeletes;

    public $table = 'temp_venta_detalles';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];

    protected $with = ['item'];

    protected $appends = ['total_modificaciones','sub_total'];

    public $fillable = [
        'temp_venta_id',
        'item_id',
        'cantidad',
        'precio',
        'descuento'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'temp_venta_id' => 'integer',
        'item_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'temp_venta_id' => 'required',
        'item_id' => 'required',
        'cantidad' => 'required|numeric|min:1',
        'precio' => 'required|numeric',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function item()
    {
        return $this->belongsTo(Item::class)->with('combo');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function tempVenta()
    {
        return $this->belongsTo(TempVenta::class);
    }

    public function totalModificaciones()
    {
        $modificaciones = session('modificaciones');

        if (isset($modificaciones[$this->id])){
            return $modificaciones[$this->id]['total'];
        }

        return 0;
    }

    public function getTotalModificacionesAttribute()
    {
        return $this->totalModificaciones();
    }

    public function getSubTotalAttribute()
    {
        return ($this->cantidad* $this->precio);
    }
}
