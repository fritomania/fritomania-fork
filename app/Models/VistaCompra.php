<?php

namespace App\Models;

use Eloquent as Model;

/**
 * App\Models\VistaCompra
 *
 * @property int $id
 * @property string|null $creada
 * @property string|null $fecha_documento
 * @property string|null $fecha_ingreso
 * @property string|null $fecha_ingreso_plan
 * @property string|null $fecha_credito
 * @property string|null $ns
 * @property string|null $proveedor
 * @property string $tipo
 * @property string $estado
 * @property string|null $hora
 * @property string|null $usuario
 * @property int $credito
 * @property int $pagada
 * @property int $cestado_id
 * @property int $tienda_id
 * @property int $user_id
 * @property int|null $proveedor_id
 * @property float|null $saldo
 * @property \Carbon\Carbon|null $created_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VistaCompra anuladas()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VistaCompra credito()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VistaCompra deTienda($tienda = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VistaCompra delUser($user)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VistaCompra hoy()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VistaCompra noCredito()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VistaCompra porCobrar()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VistaCompra procesadas()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VistaCompra whereCestadoId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VistaCompra whereCreada($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VistaCompra whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VistaCompra whereCredito($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VistaCompra whereEstado($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VistaCompra whereFechaCredito($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VistaCompra whereFechaDocumento($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VistaCompra whereFechaIngreso($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VistaCompra whereFechaIngresoPlan($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VistaCompra whereHora($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VistaCompra whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VistaCompra whereNs($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VistaCompra wherePagada($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VistaCompra whereProveedor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VistaCompra whereProveedorId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VistaCompra whereSaldo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VistaCompra whereTiendaId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VistaCompra whereTipo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VistaCompra whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VistaCompra whereUsuario($value)
 * @mixin \Eloquent
 */
class VistaCompra extends Model
{
    public function scopeProcesadas($query)
    {
        return $query->where('cestado_id','=','1');
    }

    public function scopeNoCredito($query)
    {
        return $query->where('credito',0);
    }

    public function scopeCredito($query)
    {
        return $query->where('credito',1);
    }

    public function scopeHoy($query)
    {
        return $query->where('fecha','=',hoyDb());
    }

    public function scopeDeTienda($query,$tienda=null)
    {
        $tienda = isset($tienda) ? $tienda : session('tienda');
        return $query->where('tienda_id',$tienda);
    }

    public function scopeDelUser($query,$user)
    {
        return $query->where('user_id',$user);
    }

    public function scopePorCobrar($query)
    {
        return $query->where('credito',1)->where('pagada',0);
    }

    public function scopeAnuladas($query)
    {
        return $query->where('cestado_id','=','2');
    }

    public $table = 'vista_compras';
}
