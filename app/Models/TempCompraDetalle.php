<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class TempCompraDetalle
 *
 * @package App\Models
 * @version March 30, 2017, 5:31 pm CST
 * @property int $id
 * @property int $temp_compra_id
 * @property int $item_id
 * @property float $cantidad
 * @property float $precio
 * @property float $descuento
 * @property string|null $fecha_ven
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property \Carbon\Carbon|null $deleted_at
 * @property-read \App\Models\Item $item
 * @property-read \App\Models\TempCompra $tempCompra
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TempCompraDetalle whereCantidad($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TempCompraDetalle whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TempCompraDetalle whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TempCompraDetalle whereDescuento($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TempCompraDetalle whereFechaVen($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TempCompraDetalle whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TempCompraDetalle whereItemId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TempCompraDetalle wherePrecio($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TempCompraDetalle whereTempCompraId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TempCompraDetalle whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class TempCompraDetalle extends Model
{

    public $table = 'temp_compra_detalles';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'temp_compra_id',
        'item_id',
        'cantidad',
        'precio',
        'descuento',
        'fecha_ven'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'temp_compra_id' => 'integer',
        'item_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'temp_compra_id' => 'required',
        'item_id' => 'required',
        'cantidad' => 'required',
        'precio' => 'required',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function item()
    {
        return $this->belongsTo(\App\Models\Item::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function tempCompra()
    {
        return $this->belongsTo(\App\Models\TempCompra::class);
    }
}
