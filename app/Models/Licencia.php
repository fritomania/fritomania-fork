<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Licencia
 *
 * @package App\Models
 * @version September 17, 2017, 12:23 pm CST
 * @property int $id
 * @property string $fecha_expira
 * @property bool $prueba
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Licencia whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Licencia whereFechaExpira($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Licencia whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Licencia wherePrueba($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Licencia whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Licencia extends Model
{
//    use SoftDeletes;

    public $table = 'licencias';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'fecha_expira',
        'prueba'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
//        'fecha_expira' => 'date',
        'prueba' => 'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
