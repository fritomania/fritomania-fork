<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class TempPedido
 *
 * @package App\Models
 * @version July 13, 2017, 8:52 am CST
 * @property int $id
 * @property int $user_id
 * @property bool $procesado
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property \Carbon\Carbon|null $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\TempPedidoDetalle[] $tempPedidoDetalles
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\TempPedido onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TempPedido whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TempPedido whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TempPedido whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TempPedido whereProcesado($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TempPedido whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TempPedido whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\TempPedido withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\TempPedido withoutTrashed()
 * @mixin \Eloquent
 */
class TempPedido extends Model
{
    use SoftDeletes;

    public $table = 'temp_pedidos';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'user_id',
        'procesado'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'user_id' => 'integer',
        'procesado' => 'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function tempPedidoDetalles()
    {
        return $this->hasMany(\App\Models\TempPedidoDetalle::class);
    }
}
