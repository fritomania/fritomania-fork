<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Empleado
 * @package App\Models
 * @version July 27, 2018, 4:14 pm CST
 *
 * @property \App\Models\Tienda tienda
 * @property \Illuminate\Database\Eloquent\Collection clienteEquipo
 * @property \Illuminate\Database\Eloquent\Collection compraDetalles
 * @property \Illuminate\Database\Eloquent\Collection compraPedidos
 * @property \Illuminate\Database\Eloquent\Collection compraVenta
 * @property \Illuminate\Database\Eloquent\Collection cpagos
 * @property \Illuminate\Database\Eloquent\Collection cpuEquipo
 * @property \Illuminate\Database\Eloquent\Collection cpus
 * @property \Illuminate\Database\Eloquent\Collection egresos
 * @property \Illuminate\Database\Eloquent\Collection equipoAtributos
 * @property \Illuminate\Database\Eloquent\Collection equipoRmodulo
 * @property \Illuminate\Database\Eloquent\Collection equivalencias
 * @property \Illuminate\Database\Eloquent\Collection icategoriaItem
 * @property \Illuminate\Database\Eloquent\Collection ingresos
 * @property \Illuminate\Database\Eloquent\Collection optionUser
 * @property \Illuminate\Database\Eloquent\Collection pedidoDetalles
 * @property \Illuminate\Database\Eloquent\Collection pedidoVentas
 * @property \Illuminate\Database\Eloquent\Collection pedidos
 * @property \Illuminate\Database\Eloquent\Collection programaServicio
 * @property \Illuminate\Database\Eloquent\Collection recetaDetalles
 * @property \Illuminate\Database\Eloquent\Collection recetas
 * @property \Illuminate\Database\Eloquent\Collection rmodulos
 * @property \Illuminate\Database\Eloquent\Collection rolUser
 * @property \Illuminate\Database\Eloquent\Collection servicioArchivos
 * @property \Illuminate\Database\Eloquent\Collection sgastos
 * @property \Illuminate\Database\Eloquent\Collection stocks
 * @property \Illuminate\Database\Eloquent\Collection tempCompraDetalles
 * @property \Illuminate\Database\Eloquent\Collection tempPedidoDetalles
 * @property \Illuminate\Database\Eloquent\Collection tempVentaDetalles
 * @property \Illuminate\Database\Eloquent\Collection Tienda
 * @property \Illuminate\Database\Eloquent\Collection User
 * @property \Illuminate\Database\Eloquent\Collection ventaDetalles
 * @property \Illuminate\Database\Eloquent\Collection vpagos
 * @property integer tienda_id
 * @property string nombres
 * @property string apellidos
 * @property string telefono
 * @property string correo
 * @property decimal sueldo_diario
 * @property boolean porcentaje_comision
 * @property string genero
 * @property string puesto
 * @property date fecha_contratacion
 * @property string direccion
 * @property date fecha_nacimiento
 * @property int $id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property \Carbon\Carbon|null $deleted_at
 * @property-read mixed $full_name
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Tienda[] $tiendas
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\User[] $users
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Empleado onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Empleado whereApellidos($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Empleado whereCorreo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Empleado whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Empleado whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Empleado whereDireccion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Empleado whereFechaContratacion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Empleado whereFechaNacimiento($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Empleado whereGenero($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Empleado whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Empleado whereNombres($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Empleado wherePorcentajeComision($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Empleado wherePuesto($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Empleado whereSueldoDiario($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Empleado whereTelefono($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Empleado whereTiendaId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Empleado whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Empleado withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Empleado withoutTrashed()
 * @mixin \Eloquent
 */
class Empleado extends Model
{
    use SoftDeletes;

    public $table = 'empleados';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'tienda_id',
        'nombres',
        'apellidos',
        'telefono',
        'correo',
        'sueldo_diario',
        'porcentaje_comision',
        'genero',
        'puesto',
        'fecha_contratacion',
        'direccion',
        'fecha_nacimiento'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'tienda_id' => 'integer',
        'nombres' => 'string',
        'apellidos' => 'string',
        'telefono' => 'string',
        'correo' => 'string',
//        'porcentaje_comision' => 'boolean',
        'genero' => 'string',
        'puesto' => 'string',
//        'fecha_contratacion' => 'date',
        'direccion' => 'string',
//        'fecha_nacimiento' => 'date'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'nombres' => 'required',
        'apellidos' => 'required',
        'telefono' => 'required|numeric|digits_between:8,8',
        'correo' => 'required|email|max:255',
        'sueldo_diario' => 'required|numeric',
//        'genero' => 'string',
        'fecha_nacimiento' => 'nullable|date',
        'puesto' => 'nullable|max:255',
        'fecha_contratacion' => 'nullable|date',
        'porcentaje_comision' => 'nullable|numeric|digits_between:1,2|between:0,100',
        'direccion' => 'nullable'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function tienda()
    {
        return $this->belongsTo(\App\Models\Tienda::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function tiendas()
    {
        return $this->hasMany(\App\Models\Tienda::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function users()
    {
        return $this->hasMany(\App\User::class);
    }

    public function getFechaNacimientoAttribute($val)
    {
        return fecha($val);
    }

    public function getFechaContratacionAttribute($val)
    {
        return fecha($val);
    }

    public function getFullNameAttribute(){
        return $this->attributes['nombres'].' '.$this->attributes['apellidos'];
    }
}
