<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Cestado
 *
 * @package App\Models
 * @version March 21, 2017, 5:14 pm CST
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Compra[] $compras
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Cestado onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Cestado withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Cestado withoutTrashed()
 * @mixin \Eloquent
 * @property int $id
 * @property string $descripcion
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property \Carbon\Carbon|null $deleted_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cestado whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cestado whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cestado whereDescripcion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cestado whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cestado whereUpdatedAt($value)
 */
class Cestado extends Model
{
    const CREADA = 1;
    const ANULADA = 2;
    const RECIBIDA = 3;
    const CANCELADA = 4;

    use SoftDeletes;

    public $table = 'cestados';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'descripcion'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'descripcion' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'descripcion' => 'required|max:100|unique:cestados',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function compras()
    {
        return $this->hasMany(\App\Models\Compra::class);
    }
}
