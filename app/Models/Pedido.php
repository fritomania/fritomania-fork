<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Pedido
 *
 * @package App\Models
 * @version July 21, 2017, 9:05 am CST
 * @property int $id
 * @property int $numero
 * @property int $cliente_id
 * @property string|null $fecha_ingreso
 * @property string|null $fecha_entrega
 * @property int $user_id
 * @property int $pestado_id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property \Carbon\Carbon|null $deleted_at
 * @property-read \App\Models\Cliente $cliente
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Compra[] $compras
 * @property-read mixed $total
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\PedidoDetalle[] $pedidoDetalles
 * @property-read \App\Models\Pestado $pestado
 * @property-read \App\User $user
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Venta[] $ventas
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Pedido onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Pedido whereClienteId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Pedido whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Pedido whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Pedido whereFechaEntrega($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Pedido whereFechaIngreso($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Pedido whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Pedido whereNumero($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Pedido wherePestadoId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Pedido whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Pedido whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Pedido withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Pedido withoutTrashed()
 * @mixin \Eloquent
 */
class Pedido extends Model
{
    use SoftDeletes;

    public $table = 'pedidos';
    protected $appends = [
        'total'
    ];

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected $dates = ['deleted_at'];


    public $fillable = [
        'numero',
        'cliente_id',
        'fecha_ingreso',
        'fecha_entrega',
        'user_id',
        'pestado_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'numero' => 'integer',
        'cliente_id' => 'integer',
        'user_id' => 'integer',
        'pestado_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function cliente()
    {
        return $this->belongsTo(\App\Models\Cliente::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function pestado()
    {
        return $this->belongsTo(\App\Models\Pestado::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     **/
    public function compras()
    {
        return $this->belongsToMany(\App\Models\Compra::class, 'compra_pedidos');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function pedidoDetalles()
    {
        return $this->hasMany(\App\Models\PedidoDetalle::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     **/
    public function ventas()
    {
        return $this->belongsToMany(\App\Models\Venta::class, 'pedido_ventas');
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function getTotalAttribute()
    {
        $total=0;
        foreach ($this->pedidoDetalles as $detalle){
            $total+=($detalle->cantidad*$detalle->precio);
        }

        return $total;
    }
}
