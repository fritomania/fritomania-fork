<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class VentaReceta
 * @package App\Models
 * @version December 17, 2018, 8:22 pm -03
 *
 * @property \App\Models\Item item
 * @property \Illuminate\Database\Eloquent\Collection asiganacionDelivery
 * @property \Illuminate\Database\Eloquent\Collection comboDetalles
 * @property \Illuminate\Database\Eloquent\Collection compraDetalles
 * @property \Illuminate\Database\Eloquent\Collection compraPedidos
 * @property \Illuminate\Database\Eloquent\Collection compraVenta
 * @property \Illuminate\Database\Eloquent\Collection cpagos
 * @property \Illuminate\Database\Eloquent\Collection egresoMateriaPrima
 * @property \Illuminate\Database\Eloquent\Collection egresoProduccion
 * @property \Illuminate\Database\Eloquent\Collection egresoSolicitud
 * @property \Illuminate\Database\Eloquent\Collection egresos
 * @property \Illuminate\Database\Eloquent\Collection equivalencias
 * @property \Illuminate\Database\Eloquent\Collection gastos
 * @property \Illuminate\Database\Eloquent\Collection icategoriaItem
 * @property \Illuminate\Database\Eloquent\Collection ingredientesOpcionales
 * @property \Illuminate\Database\Eloquent\Collection ingresoMateriaPrima
 * @property \Illuminate\Database\Eloquent\Collection ingresoProduccion
 * @property \Illuminate\Database\Eloquent\Collection ingresoSolicitud
 * @property \Illuminate\Database\Eloquent\Collection ingresos
 * @property \Illuminate\Database\Eloquent\Collection itemTiempos
 * @property \Illuminate\Database\Eloquent\Collection marcajes
 * @property \Illuminate\Database\Eloquent\Collection notificaciones
 * @property \Illuminate\Database\Eloquent\Collection optionUser
 * @property \Illuminate\Database\Eloquent\Collection ordenDetalles
 * @property \Illuminate\Database\Eloquent\Collection pedidoDetalles
 * @property \Illuminate\Database\Eloquent\Collection pedidoVentas
 * @property \Illuminate\Database\Eloquent\Collection pedidos
 * @property \Illuminate\Database\Eloquent\Collection productoConsumidos
 * @property \Illuminate\Database\Eloquent\Collection productoSolicitados
 * @property \Illuminate\Database\Eloquent\Collection recetaDetalles
 * @property \Illuminate\Database\Eloquent\Collection recetas
 * @property \Illuminate\Database\Eloquent\Collection rolUser
 * @property \Illuminate\Database\Eloquent\Collection solicitudDetalles
 * @property \Illuminate\Database\Eloquent\Collection stockCriticos
 * @property \Illuminate\Database\Eloquent\Collection stocks
 * @property \Illuminate\Database\Eloquent\Collection tempCompraDetalles
 * @property \Illuminate\Database\Eloquent\Collection tempPedidoDetalles
 * @property \Illuminate\Database\Eloquent\Collection tempSolicitudDetalles
 * @property \Illuminate\Database\Eloquent\Collection tempVentaDetalles
 * @property \Illuminate\Database\Eloquent\Collection tiendaDespachaTienda
 * @property \Illuminate\Database\Eloquent\Collection trasladoMateriaPrimas
 * @property \Illuminate\Database\Eloquent\Collection userDespachaUser
 * @property \Illuminate\Database\Eloquent\Collection ventaDetalles
 * @property \Illuminate\Database\Eloquent\Collection VentaRecetaDetalle
 * @property \Illuminate\Database\Eloquent\Collection ventaVestado
 * @property \Illuminate\Database\Eloquent\Collection vpagos
 * @property integer item_id
 * @property boolean activa
 * @property string nombre
 */
class VentaReceta extends Model
{
    use SoftDeletes;

    public $table = 'venta_recetas';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'item_id',
        'activa',
        'nombre'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'item_id' => 'integer',
        'activa' => 'boolean',
        'nombre' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function item()
    {
        return $this->belongsTo(\App\Models\Item::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function ventaRecetaDetalles()
    {
        return $this->hasMany(\App\Models\VentaRecetaDetalle::class);
    }

    public function detalles()
    {
        return $this->hasMany(\App\Models\VentaRecetaDetalle::class,'venta_receta_id','id');
    }
}
