<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class CompraDetalle
 *
 * @package App\Models
 * @version June 11, 2018, 6:13 pm CST
 * @property int $id
 * @property int $compra_id
 * @property int $item_id
 * @property float $cantidad
 * @property float $precio
 * @property float $descuento
 * @property string|null $fecha_ven
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property \Carbon\Carbon|null $deleted_at
 * @property-read \App\Models\Compra $compra
 * @property-read mixed $sub_total
 * @property-read \App\Models\Item $item
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Stock[] $stocks
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CompraDetalle onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompraDetalle whereCantidad($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompraDetalle whereCompraId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompraDetalle whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompraDetalle whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompraDetalle whereDescuento($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompraDetalle whereFechaVen($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompraDetalle whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompraDetalle whereItemId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompraDetalle wherePrecio($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompraDetalle whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CompraDetalle withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CompraDetalle withoutTrashed()
 * @mixin \Eloquent
 */
class CompraDetalle extends Model
{
    use SoftDeletes;

    public $table = 'compra_detalles';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];

    protected $appends = ['sub_total'];

    public $fillable = [
        'compra_id',
        'item_id',
        'cantidad',
        'precio',
        'descuento',
        'fecha_ven'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'compra_id' => 'integer',
        'item_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function compra()
    {
        return $this->belongsTo(\App\Models\Compra::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function item()
    {
        return $this->belongsTo(\App\Models\Item::class)->withTrashed();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     **/
    public function stocks()
    {
        return $this->belongsToMany(\App\Models\Stock::class, 'ingresos')->withPivot('cantidad');
    }

    public function getSubTotalAttribute()
    {
        return ($this->cantidad* $this->precio);
    }
}
