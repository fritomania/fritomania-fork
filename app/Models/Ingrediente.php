<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Ingrediente
 * @package App\Models
 * @version December 16, 2018, 4:07 pm -03
 *
 * @property \App\Models\IngredienteCategoria ingredienteCategoria
 * @property \Illuminate\Database\Eloquent\Collection asiganacionDelivery
 * @property \Illuminate\Database\Eloquent\Collection comboDetalles
 * @property \Illuminate\Database\Eloquent\Collection compraDetalles
 * @property \Illuminate\Database\Eloquent\Collection compraPedidos
 * @property \Illuminate\Database\Eloquent\Collection compraVenta
 * @property \Illuminate\Database\Eloquent\Collection cpagos
 * @property \Illuminate\Database\Eloquent\Collection egresoMateriaPrima
 * @property \Illuminate\Database\Eloquent\Collection egresoProduccion
 * @property \Illuminate\Database\Eloquent\Collection egresoSolicitud
 * @property \Illuminate\Database\Eloquent\Collection egresos
 * @property \Illuminate\Database\Eloquent\Collection equivalencias
 * @property \Illuminate\Database\Eloquent\Collection gastos
 * @property \Illuminate\Database\Eloquent\Collection icategoriaItem
 * @property \Illuminate\Database\Eloquent\Collection ingredientesOpcionales
 * @property \Illuminate\Database\Eloquent\Collection ingresoMateriaPrima
 * @property \Illuminate\Database\Eloquent\Collection ingresoProduccion
 * @property \Illuminate\Database\Eloquent\Collection ingresoSolicitud
 * @property \Illuminate\Database\Eloquent\Collection ingresos
 * @property \Illuminate\Database\Eloquent\Collection itemTiempos
 * @property \Illuminate\Database\Eloquent\Collection marcajes
 * @property \Illuminate\Database\Eloquent\Collection notificaciones
 * @property \Illuminate\Database\Eloquent\Collection optionUser
 * @property \Illuminate\Database\Eloquent\Collection ordenDetalles
 * @property \Illuminate\Database\Eloquent\Collection pedidoDetalles
 * @property \Illuminate\Database\Eloquent\Collection pedidoVentas
 * @property \Illuminate\Database\Eloquent\Collection pedidos
 * @property \Illuminate\Database\Eloquent\Collection productoConsumidos
 * @property \Illuminate\Database\Eloquent\Collection productoSolicitados
 * @property \Illuminate\Database\Eloquent\Collection recetaDetalles
 * @property \Illuminate\Database\Eloquent\Collection recetas
 * @property \Illuminate\Database\Eloquent\Collection rolUser
 * @property \Illuminate\Database\Eloquent\Collection solicitudDetalles
 * @property \Illuminate\Database\Eloquent\Collection stockCriticos
 * @property \Illuminate\Database\Eloquent\Collection stocks
 * @property \Illuminate\Database\Eloquent\Collection tempCompraDetalles
 * @property \Illuminate\Database\Eloquent\Collection tempPedidoDetalles
 * @property \Illuminate\Database\Eloquent\Collection tempSolicitudDetalles
 * @property \Illuminate\Database\Eloquent\Collection tempVentaDetalles
 * @property \Illuminate\Database\Eloquent\Collection tiendaDespachaTienda
 * @property \Illuminate\Database\Eloquent\Collection trasladoMateriaPrimas
 * @property \Illuminate\Database\Eloquent\Collection userDespachaUser
 * @property \Illuminate\Database\Eloquent\Collection ventaDetalles
 * @property \Illuminate\Database\Eloquent\Collection VentaRecetaDetalle
 * @property \Illuminate\Database\Eloquent\Collection ventaVestado
 * @property \Illuminate\Database\Eloquent\Collection vpagos
 * @property integer ingrediente_categoria_id
 * @property string nombre
 * @property string imagen
 */
class Ingrediente extends Model
{
    use SoftDeletes;

    public $table = 'ingredientes';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];

    protected $appends = ['img','text'];

    protected $with = ['categoria'];

    public $fillable = [
        'ingrediente_categoria_id',
        'nombre',
        'imagen'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'ingrediente_categoria_id' => 'integer',
        'nombre' => 'string',
        'imagen' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'nombre' => 'required|max:255|unique:ingredientes',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function categoria()
    {
        return $this->belongsTo(\App\Models\IngredienteCategoria::class,'ingrediente_categoria_id','id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function ventaRecetaDetalles()
    {
        return $this->hasMany(\App\Models\VentaRecetaDetalle::class);
    }

    public function getImgAttribute()
    {
        return $this->imagen ? asset('storage/ingredientes/'.$this->imagen) : asset('img/avatar_none.png');
    }

    public function getTextAttribute()
    {
        return $this->nombre;
    }
}
