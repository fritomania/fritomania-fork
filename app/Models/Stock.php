<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

/**
 * Class Stock
 *
 * @package App\Models
 * @version July 2, 2017, 5:14 pm CST
 * @property int $id
 * @property int $tienda_id
 * @property int $item_id
 * @property string $lote
 * @property string $fecha_ing
 * @property string|null $fecha_ven
 * @property float $cantidad
 * @property float $cnt_ini
 * @property bool $orden_salida
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property \Carbon\Carbon|null $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\CompraDetalle[] $compraDetalles
 * @property-read \App\Models\Item $item
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ProductoConsumido[] $produccionEgresoDetalles
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\TrasladoMateriaPrima[] $produccionEgresoDetallesMp
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ProductoRealizado[] $produccionIngresoDetalles
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\TrasladoMateriaPrima[] $produccionIngresoDetallesMp
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\SolicitudDetalle[] $solicitudEgresoDetalles
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\SolicitudDetalle[] $solicitudIngresoDetalles
 * @property-read \App\Models\Tienda $tienda
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\VentaDetalle[] $ventaDetalles
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Stock deTienda($tienda)
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Stock onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Stock whereCantidad($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Stock whereCntIni($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Stock whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Stock whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Stock whereFechaIng($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Stock whereFechaVen($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Stock whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Stock whereItemId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Stock whereLote($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Stock whereOrdenSalida($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Stock whereTiendaId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Stock whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Stock withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Stock withoutTrashed()
 * @mixin \Eloquent
 */
class Stock extends Model
{
    use SoftDeletes;

    public function scopeDeTienda($query,$tienda){
        return $query->where('tienda_id',$tienda);
    }

    public $table = 'stocks';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'tienda_id',
        'item_id',
        'lote',
//        'fecha_ing',
        'fecha_ven',
        'cantidad',
        'cnt_ini',
        'orden_salida'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'tienda_id' => 'integer',
        'item_id' => 'integer',
        'lote' => 'string',
        'orden_salida' => 'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function item()
    {
        return $this->belongsTo(\App\Models\Item::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function tienda()
    {
        return $this->belongsTo(\App\Models\Tienda::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     **/
    public function ventaDetalles()
    {
        return $this->belongsToMany(\App\Models\VentaDetalle::class, 'egresos');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     **/
    public function compraDetalles()
    {
        return $this->belongsToMany(\App\Models\CompraDetalle::class, 'ingresos');
    }

    public function solicitudEgresoDetalles()
    {
        return $this->belongsToMany(SolicitudDetalle::class, 'egreso_solicitud');
    }

    public function solicitudIngresoDetalles()
    {
        return $this->belongsToMany(SolicitudDetalle::class, 'ingreso_solicitud');
    }

    public function produccionEgresoDetalles()
    {
        return $this->belongsToMany(ProductoConsumido::class, 'egreso_produccion');
    }

    public function produccionIngresoDetalles()
    {
        return $this->belongsToMany(ProductoRealizado::class, 'ingreso_produccion');
    }

    public function produccionEgresoDetallesMp()
    {
        return $this->belongsToMany(TrasladoMateriaPrima::class, 'egreso_materia_prima');
    }

    public function produccionIngresoDetallesMp()
    {
        return $this->belongsToMany(TrasladoMateriaPrima::class, 'ingreso_materia_prima');
    }


    /**
     * Realiza un egreso del stock en base al orden de salida definido, si no esta definido un orden de salida egresa
     * los de menor fecha de vencimiento
     * @param null $detalleVenta
     * @param null $item
     * @param int $cantida
     * @param null $tienda
     * @param null $lote
     * @param null $fechaVence
     * @return array|bool
     */
    function egreso($item=null,$cantida=0,$detalleVenta=null,$tienda=null){

        $cantida=$cantida*-1;

        //Sin no se envía la tienda se toma la de la sesión del usuario
//        $tienda = !$tienda ? Auth::user()->empleado->tienda->id : $tienda;
        $tienda = !$tienda ? session('tienda') : $tienda;

        $stocks = Stock::where('item_id',$item)
            ->where('tienda_id',$tienda)
            ->where('cantidad','>',0)
            ->orderBy('orden_salida')
            ->orderBy('fecha_ven')
            ->orderBy('created_at')
            ->orderBy('id')
            ->get();

        if($stocks){

            $egresos = collect();
            foreach ($stocks as $key => $stock) {

                $cantida=($cantida+$stock->cantidad);

                $nuevoStock = $cantida<0 ? 0 : $cantida;

                $rebajado= $cantida<=0 ? $stock->cantidad : ($cantida-$stock->cantidad)*-1;

//                    dump('cantida: '.$cantida.' nuevo stock: '.$nuevoStock.' rebajado:'.$rebajado);

                if($rebajado>0){
                    $egresos[]= [$detalleVenta => ['cantidad' => $rebajado]];
                    $stock->cantidad= $nuevoStock;

                    $stock->save();
                    if($detalleVenta){
                        $stock->ventaDetalles()->syncWithoutDetaching([$detalleVenta => ['cantidad' => $rebajado]]);
                    }
                }

                if($cantida>0)
                    break;
            }

            return $egresos;

        }else{

            return false;
        }


    }

    /**
     * Realiza un ingreso al stock, si existe un stock para el item en la tienda se suma a la cantidad existente si no,
     * se crea un nuevo registro
     * @param null $detalleCompra
     * @param null $item
     * @param int $cantida
     * @param null $lote
     * @param null $fechaVence
     * @param null $tienda
     * @return mixed
     */
    public function ingreso($item=null,$cantida=0,$detalleCompra=null,$lote=null,$fechaVence=null,$tienda=null){

        //Sin no se envía la tienda se toma la de la sesión del usuario
//        $tienda = !$tienda ? Auth::user()->empleado->tienda->id : $tienda;
        $tienda = !$tienda ? session('tienda') : $tienda;

        $stock = Stock::where('tienda_id',$tienda)
            ->where('item_id',$item)
//            ->where('lote',$lote)
            ->where('fecha_ven',$fechaVence)
            ->get();

        //Si hay un registro existente
        if($stock->count()==1){

            $stock= $stock[0];

            $stock->cantidad = $stock->cantidad + $cantida;
            $stock->save();
            if ($detalleCompra){
                $stock->compraDetalles()->syncWithoutDetaching([$detalleCompra => ['cantidad' => $cantida]]);
            }
        }

        //Si no hay ningún registro
        if($stock->count()==0){
            $datos = [
                'tienda_id' => $tienda,
                'item_id' => $item,
                'lote' =>  $lote,
                'fecha_ven' => $fechaVence,
                'cantidad' =>  $cantida,
                'cnt_ini' =>  $cantida,
                'orden_salida' => 0
            ];

            $stock= $this->create($datos);

            if ($detalleCompra){
                $stock->compraDetalles()->syncWithoutDetaching([$detalleCompra => ['cantidad' => $cantida]]);
            }
        }

        return $stock->compraDetalles();

    }


    function egresoSolicitud($item=null,$cantida=0,$detalleSolicitud=null,$tienda=null){

        $cantida=$cantida*-1;

        //Sin no se envía la tienda se toma la de la sesión del usuario
//        $tienda = !$tienda ? Auth::user()->empleado->tienda->id : $tienda;
        $tienda = !$tienda ? session('tienda') : $tienda;

        $stocks = Stock::where('item_id',$item)
            ->where('tienda_id',$tienda)
            ->where('cantidad','>',0)
            ->orderBy('orden_salida')
            ->orderBy('fecha_ven')
            ->orderBy('created_at')
            ->orderBy('id')
            ->get();

        if($stocks){

            $egresos = collect();
            foreach ($stocks as $key => $stock) {

                $cantida=($cantida+$stock->cantidad);

                $nuevoStock = $cantida<0 ? 0 : $cantida;

                $rebajado= $cantida<=0 ? $stock->cantidad : ($cantida-$stock->cantidad)*-1;

//                    dump('cantida: '.$cantida.' nuevo stock: '.$nuevoStock.' rebajado:'.$rebajado);

                if($rebajado>0){
                    $egresos[]= [$detalleSolicitud => ['cantidad' => $rebajado]];
                    $stock->cantidad= $nuevoStock;

                    $stock->save();
                    if($detalleSolicitud){
                        $stock->solicitudEgresoDetalles()->syncWithoutDetaching([$detalleSolicitud => ['cantidad' => $rebajado]]);
                    }
                }

                if($cantida>0)
                    break;
            }

            return $egresos;

        }else{

            return false;
        }


    }

    public function ingresoSolicitud($item=null,$cantida=0,$detalleSolicitud=null,$lote=null,$fechaVence=null,$tienda=null){

        //Sin no se envía la tienda se toma la de la sesión del usuario
//        $tienda = !$tienda ? Auth::user()->empleado->tienda->id : $tienda;
        $tienda = !$tienda ? session('tienda') : $tienda;

        $stock = Stock::where('tienda_id',$tienda)
            ->where('item_id',$item)
//            ->where('lote',$lote)
            ->where('fecha_ven',$fechaVence)
            ->get();

        //Si hay un registro existente
        if($stock->count()==1){

            $stock= $stock[0];

            $stock->cantidad = $stock->cantidad + $cantida;
            $stock->save();
            if ($detalleSolicitud){
                $stock->solicitudIngresoDetalles()->syncWithoutDetaching([$detalleSolicitud => ['cantidad' => $cantida]]);
            }
        }

        //Si no hay ningún registro
        if($stock->count()==0){
            $datos = [
                'tienda_id' => $tienda,
                'item_id' => $item,
                'lote' =>  $lote,
                'fecha_ven' => $fechaVence,
                'cantidad' =>  $cantida,
                'cnt_ini' =>  $cantida,
                'orden_salida' => 0
            ];

            $stock= $this->create($datos);

            if ($detalleSolicitud){
                $stock->solicitudIngresoDetalles()->syncWithoutDetaching([$detalleSolicitud => ['cantidad' => $cantida]]);
            }
        }

        return $stock->solicitudIngresoDetalles();

    }

    function egresoProduccion($item=null,$cantida=0,$detalleConsumido=null){

        $cantida=$cantida*-1;

        $stocks = Stock::where('item_id',$item)
            ->where('tienda_id',Tienda::COCINA_PRODUCCION)
            ->where('cantidad','>',0)
            ->orderBy('orden_salida')
            ->orderBy('fecha_ven')
            ->orderBy('created_at')
            ->orderBy('id')
            ->get();

        if($stocks){

            $egresos = collect();
            foreach ($stocks as $key => $stock) {

                $cantida=($cantida+$stock->cantidad);

                $nuevoStock = $cantida<0 ? 0 : $cantida;

                $rebajado= $cantida<=0 ? $stock->cantidad : ($cantida-$stock->cantidad)*-1;

//                    dump('cantida: '.$cantida.' nuevo stock: '.$nuevoStock.' rebajado:'.$rebajado);

                if($rebajado>0){
                    $egresos[]= [$detalleConsumido => ['cantidad' => $rebajado]];
                    $stock->cantidad= $nuevoStock;

                    $stock->save();
                    if($detalleConsumido){
                        $stock->produccionEgresoDetalles()->syncWithoutDetaching([$detalleConsumido => ['cantidad' => $rebajado]]);
                    }
                }

                if($cantida>0)
                    break;
            }

            return $egresos;

        }else{

            return false;
        }


    }

    public function ingresoProduccion($item=null,$cantida=0,$detalleProducido=null,$lote=null,$fechaVence=null,$tienda=null){

        $tienda = Tienda::BODEGA_PRODUCCION;

        $stock = Stock::where('tienda_id',$tienda)
            ->where('item_id',$item)
////            ->where('lote',$lote)
            ->where('fecha_ven',$fechaVence)
            ->get();

        //Si hay un registro existente
        if($stock->count()==1){

            $stock= $stock[0];

            $stock->cantidad = $stock->cantidad + $cantida;
            $stock->save();
            if ($detalleProducido){
                $stock->produccionIngresoDetalles()->syncWithoutDetaching([$detalleProducido => ['cantidad' => $cantida]]);
            }
        }

        //Si no hay ningún registro
        if($stock->count()==0){
            $datos = [
                'tienda_id' => $tienda,
                'item_id' => $item,
                'lote' =>  $lote,
                'fecha_ven' => $fechaVence,
                'cantidad' =>  $cantida,
                'cnt_ini' =>  $cantida,
                'orden_salida' => 0
            ];

            $stock= $this->create($datos);

            if ($detalleProducido){
                $stock->produccionIngresoDetalles()->syncWithoutDetaching([$detalleProducido => ['cantidad' => $cantida]]);
            }
        }

        return $stock->produccionIngresoDetalles();

    }

    /**
     * Egreso que se realiza cuando producción manda a cocina la solicitud de producción, sale materia prima de producción para luego ingresar a cocina
     * @param null $item
     * @param int $cantida
     * @param null $detalleTrasladoMp
     * @return array|bool|\Illuminate\Support\Collection
     */
    function egresoProduccionMp($item=null,$cantida=0,$detalleTrasladoMp=null,$tienda=Tienda::BODEGA_PRODUCCION){

        $cantida=$cantida*-1;

        $stocks = Stock::where('item_id',$item)
            ->where('tienda_id',$tienda)
            ->where('cantidad','>',0)
            ->orderBy('orden_salida')
            ->orderBy('fecha_ven')
            ->orderBy('created_at')
            ->orderBy('id')
            ->get();

        if($stocks){

            $egresos = collect();
            foreach ($stocks as $key => $stock) {

                $cantida=($cantida+$stock->cantidad);

                $nuevoStock = $cantida<0 ? 0 : $cantida;

                $rebajado= $cantida<=0 ? $stock->cantidad : ($cantida-$stock->cantidad)*-1;

//                    dump('cantida: '.$cantida.' nuevo stock: '.$nuevoStock.' rebajado:'.$rebajado);

                if($rebajado>0){
                    $egresos[]= [$detalleTrasladoMp => ['cantidad' => $rebajado]];
                    $stock->cantidad= $nuevoStock;

                    $stock->save();
                    if($detalleTrasladoMp){
                        $stock->produccionEgresoDetallesMp()->syncWithoutDetaching([$detalleTrasladoMp => ['cantidad' => $rebajado]]);
                    }
                }

                if($cantida>0)
                    break;
            }

            return $egresos;

        }else{

            return false;
        }


    }

    /**
     * Ingreso que se realiza cuando producción manda a cocina la solicitud de producción, ingresar materia prima a cocina
     * @param null $item
     * @param int $cantida
     * @param null $detalleProducido
     * @param null $lote
     * @param $fechaVence
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function ingresoProduccionMp($item=null,$cantida=0,$detalleTrasladoMp=null,$lote=null,$fechaVence=null,$tienda=Tienda::COCINA_PRODUCCION){

        $stock = Stock::where('tienda_id',$tienda)
            ->where('item_id',$item)
//            ->where('lote',$lote)
            ->where('fecha_ven',$fechaVence)
            ->get();

        //Si hay un registro existente
        if($stock->count()==1){
            $stock= $stock[0];

            $stock->cantidad = $stock->cantidad + $cantida;
            $stock->save();
            if ($detalleTrasladoMp){
                $stock->produccionIngresoDetallesMp()->syncWithoutDetaching([$detalleTrasladoMp => ['cantidad' => $cantida]]);
            }


        }

        //Si no hay ningún registro
        if($stock->count()==0){
            $datos = [
                'tienda_id' => $tienda,
                'item_id' => $item,
                'lote' =>  $lote,
                'fecha_ven' => $fechaVence,
                'cantidad' =>  $cantida,
                'cnt_ini' =>  $cantida,
                'orden_salida' => 0
            ];

            $stock= $this->create($datos);



            if ($detalleTrasladoMp){
                $stock->produccionIngresoDetallesMp()->syncWithoutDetaching([$detalleTrasladoMp => ['cantidad' => $cantida]]);
            }
        }

        return $stock->produccionIngresoDetallesMp();

    }
}
