<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Tienda
 * @package App\Models
 * @version September 7, 2018, 8:42 pm -03
 *
 * @property \App\User user
 * @property \Illuminate\Database\Eloquent\Collection asiganacionDelivery
 * @property \Illuminate\Database\Eloquent\Collection compraDetalles
 * @property \Illuminate\Database\Eloquent\Collection compraPedidos
 * @property \Illuminate\Database\Eloquent\Collection compraVenta
 * @property \Illuminate\Database\Eloquent\Collection cpagos
 * @property \Illuminate\Database\Eloquent\Collection egresoMateriaPrima
 * @property \Illuminate\Database\Eloquent\Collection egresoProduccion
 * @property \Illuminate\Database\Eloquent\Collection egresoSolicitud
 * @property \Illuminate\Database\Eloquent\Collection egresos
 * @property \Illuminate\Database\Eloquent\Collection Empleado
 * @property \Illuminate\Database\Eloquent\Collection equivalencias
 * @property \Illuminate\Database\Eloquent\Collection icategoriaItem
 * @property \Illuminate\Database\Eloquent\Collection ingresoMateriaPrima
 * @property \Illuminate\Database\Eloquent\Collection ingresoProduccion
 * @property \Illuminate\Database\Eloquent\Collection ingresoSolicitud
 * @property \Illuminate\Database\Eloquent\Collection ingresos
 * @property \Illuminate\Database\Eloquent\Collection marcajes
 * @property \Illuminate\Database\Eloquent\Collection notificaciones
 * @property \Illuminate\Database\Eloquent\Collection optionUser
 * @property \Illuminate\Database\Eloquent\Collection ordenDetalles
 * @property \Illuminate\Database\Eloquent\Collection pedidoDetalles
 * @property \Illuminate\Database\Eloquent\Collection pedidoVentas
 * @property \Illuminate\Database\Eloquent\Collection pedidos
 * @property \Illuminate\Database\Eloquent\Collection productoConsumidos
 * @property \Illuminate\Database\Eloquent\Collection productoSolicitados
 * @property \Illuminate\Database\Eloquent\Collection recetaDetalles
 * @property \Illuminate\Database\Eloquent\Collection recetas
 * @property \Illuminate\Database\Eloquent\Collection rolUser
 * @property \Illuminate\Database\Eloquent\Collection solicitudDetalles
 * @property \Illuminate\Database\Eloquent\Collection stockCriticos
 * @property \Illuminate\Database\Eloquent\Collection Stock
 * @property \Illuminate\Database\Eloquent\Collection tempCompraDetalles
 * @property \Illuminate\Database\Eloquent\Collection tempPedidoDetalles
 * @property \Illuminate\Database\Eloquent\Collection tempSolicitudDetalles
 * @property \Illuminate\Database\Eloquent\Collection tempVentaDetalles
 * @property \Illuminate\Database\Eloquent\Collection tiendaDespachaTienda
 * @property \Illuminate\Database\Eloquent\Collection trasladoMateriaPrimas
 * @property \Illuminate\Database\Eloquent\Collection userDespachaUser
 * @property \Illuminate\Database\Eloquent\Collection User
 * @property \Illuminate\Database\Eloquent\Collection ventaDetalles
 * @property \Illuminate\Database\Eloquent\Collection Venta
 * @property \Illuminate\Database\Eloquent\Collection vpagos
 * @property string id_map
 * @property integer admin
 * @property string nombre
 * @property string direccion
 * @property string telefono
 */
class Tienda extends Model
{
    use SoftDeletes;

    public $table = 'tiendas';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    const BODEGA_MP = 1;
    const BODEGA_PRODUCCION = 2;
    const COCINA_PRODUCCION = 3;
    const LOCAL_1 = 4;
    const LOCAL_2 = 5;
    const LOCAL_3 = 6;

    protected $dates = ['deleted_at'];


    public $fillable = [
        'admin',
        'nombre',
        'direccion',
        'telefono',
        'id_map'
    ];
    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'id_map' => 'string',
        'admin' => 'integer',
        'nombre' => 'string',
        'direccion' => 'string',
        'telefono' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'nombre' => 'required|max:100|unique:tiendas',
    ];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function empleados()
    {
        return $this->hasMany(\App\Models\Empleado::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     **/
    public function stockCriticos()
    {
        return $this->belongsToMany(\App\Models\Item::class, 'stock_criticos');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function stocks()
    {
        return $this->hasMany(\App\Models\Stock::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function users()
    {
        return $this->hasMany(\App\User::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function ventas()
    {
        return $this->hasMany(\App\Models\Venta::class);
    }

    public function userCaja()
    {
        return $this->users();
    }
}
