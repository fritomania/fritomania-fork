<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
//use OwenIt\Auditing\Auditable;
//use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

/**
 * Class Icategoria
 *
 * @package App\Models
 * @version March 19, 2017, 10:51 pm CST
 * @property int $id
 * @property string $nombre
 * @property string $descripcion
 * @property int $show_web
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property \Carbon\Carbon|null $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Item[] $items
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Icategoria onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Icategoria web()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Icategoria whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Icategoria whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Icategoria whereDescripcion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Icategoria whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Icategoria whereNombre($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Icategoria whereShowWeb($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Icategoria whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Icategoria withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Icategoria withoutTrashed()
 * @mixin \Eloquent
 */
class Icategoria extends Model //implements AuditableContract
{
    use SoftDeletes;
//    use Auditable;

    public $table = 'icategorias';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    const PRODUCTO = 1;
    const BEBIDA = 2;
    const MATERIA_PRIMA = 3;
    const COMBO = 4;
    const ADICIONALES = 5;
    const SALSAS = 6;
    const HAMBURGUESAS = 7;
    const POSTRES = 8;
    const COCINA = 9;

    protected $dates = ['deleted_at'];


    public $fillable = [
        'nombre',
        'descripcion',
        'show_web'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'nombre' => 'string',
        'descripcion' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'nombre' => 'required|max:100|unique:icategorias',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     **/
    public function items()
    {
        return $this->belongsToMany(\App\Models\Item::class, 'icategoria_item');
    }

    public function scopeWeb($query)
    {
        return $query->where('show_web',1);
    }
}
