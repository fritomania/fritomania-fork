<?php

namespace App\Models;

use Auth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Item
 * @package App\Models
 * @version September 15, 2018, 2:58 pm -03
 *
 * @property \App\Models\Icategoria icategoria
 * @property \App\Models\Iestado iestado
 * @property \App\Models\Marca marca
 * @property \App\Models\Unimed unimed
 * @property \Illuminate\Database\Eloquent\Collection asiganacionDelivery
 * @property \Illuminate\Database\Eloquent\Collection ComboDetalle
 * @property \Illuminate\Database\Eloquent\Collection Combo
 * @property \Illuminate\Database\Eloquent\Collection CompraDetalle
 * @property \Illuminate\Database\Eloquent\Collection compraPedidos
 * @property \Illuminate\Database\Eloquent\Collection compraVenta
 * @property \Illuminate\Database\Eloquent\Collection cpagos
 * @property \Illuminate\Database\Eloquent\Collection Descuento
 * @property \Illuminate\Database\Eloquent\Collection egresoMateriaPrima
 * @property \Illuminate\Database\Eloquent\Collection egresoProduccion
 * @property \Illuminate\Database\Eloquent\Collection egresoSolicitud
 * @property \Illuminate\Database\Eloquent\Collection egresos
 * @property \Illuminate\Database\Eloquent\Collection Equivalencia
 * @property \Illuminate\Database\Eloquent\Collection icategoriaItem
 * @property \Illuminate\Database\Eloquent\Collection ingresoMateriaPrima
 * @property \Illuminate\Database\Eloquent\Collection ingresoProduccion
 * @property \Illuminate\Database\Eloquent\Collection ingresoSolicitud
 * @property \Illuminate\Database\Eloquent\Collection ingresos
 * @property \Illuminate\Database\Eloquent\Collection Inistock
 * @property \Illuminate\Database\Eloquent\Collection marcajes
 * @property \Illuminate\Database\Eloquent\Collection notificaciones
 * @property \Illuminate\Database\Eloquent\Collection optionUser
 * @property \Illuminate\Database\Eloquent\Collection OrdenDetalle
 * @property \Illuminate\Database\Eloquent\Collection PedidoDetalle
 * @property \Illuminate\Database\Eloquent\Collection pedidoVentas
 * @property \Illuminate\Database\Eloquent\Collection pedidos
 * @property \Illuminate\Database\Eloquent\Collection ProductoConsumido
 * @property \Illuminate\Database\Eloquent\Collection ProductoRealizado
 * @property \Illuminate\Database\Eloquent\Collection ProductoSolicitado
 * @property \Illuminate\Database\Eloquent\Collection RecetaDetalle
 * @property \Illuminate\Database\Eloquent\Collection Receta
 * @property \Illuminate\Database\Eloquent\Collection rolUser
 * @property \Illuminate\Database\Eloquent\Collection SolicitudDetalle
 * @property \Illuminate\Database\Eloquent\Collection stockCriticos
 * @property \Illuminate\Database\Eloquent\Collection Stock
 * @property \Illuminate\Database\Eloquent\Collection TempCompraDetalle
 * @property \Illuminate\Database\Eloquent\Collection TempPedidoDetalle
 * @property \Illuminate\Database\Eloquent\Collection TempSolicitudDetalle
 * @property \Illuminate\Database\Eloquent\Collection TempVentaDetalle
 * @property \Illuminate\Database\Eloquent\Collection tiendaDespachaTienda
 * @property \Illuminate\Database\Eloquent\Collection TrasladoMateriaPrima
 * @property \Illuminate\Database\Eloquent\Collection userDespachaUser
 * @property \Illuminate\Database\Eloquent\Collection VentaDetalle
 * @property \Illuminate\Database\Eloquent\Collection vpagos
 * @property string nombre
 * @property string descripcion
 * @property string codigo
 * @property decimal precio_venta
 * @property decimal precio_compra
 * @property decimal precio_mayoreo
 * @property boolean cantidad_mayoreo
 * @property decimal precio_promedio
 * @property decimal stock
 * @property string imagen
 * @property string ubicacion
 * @property boolean inventariable
 * @property boolean perecedero
 * @property boolean materia_prima
 * @property boolean web
 * @property integer marca_id
 * @property integer unimed_id
 * @property integer icategoria_id
 * @property integer iestado_id
 */
class Item extends Model
{
    use SoftDeletes;

    public function scopeConReceta($query)
    {
        return $query->whereIn('id', function($q){
            $q->select('item_id')->from('recetas')->whereNull('deleted_at');
        });
    }

    public function scopeTopTen($query)
    {
        return $query->whereIn('id', function($q){
            $q->select('id')->from('top_ten_mes');
        });
    }

    public function scopeDeCategoria($query,$categoria)
    {
        return $query->whereIn('id', function($q) use ($categoria){
            $q->select('item_id')->from('icategoria_item')->where('icategoria_id',$categoria)->whereNull('deleted_at');
        });
    }

    //select * from items where id in(select item_id from icategoria_item where icategoria_id = 4)
    public function scopeTipoCombo($query)
    {
        return $query->whereIn('id', function($q){
            $q->select('item_id')->from('icategoria_item')->where('icategoria_id',Icategoria::COMBO)->whereNull('deleted_at');
        });
    }

    public function scopeNoCombo($query)
    {
        return $query->whereNotIn('id', function($q){
            $q->select('item_id')->from('icategoria_item')->where('icategoria_id',Icategoria::COMBO)->whereNull('deleted_at');
        });
    }

    public function scopeBebida($query)
    {
        return $query->whereIn('id', function($q){
            $q->select('item_id')->from('icategoria_item')->where('icategoria_id',Icategoria::BEBIDA)->whereNull('deleted_at');
        });
    }

    public function scopeCocina($query){
        return $query->whereIn('id', function($q){
            $q->select('item_id')->from('icategoria_item')->where('icategoria_id',Icategoria::COCINA)->whereNull('deleted_at');
        });
    }

    public function scopeSinReceta($query)
    {
        return $query->whereNotIn('id', function($q){
            $q->select('item_id')->from('recetas')->whereNull('deleted_at');
        });
    }

    public function scopeEnTienda($query,$tienda=null)
    {
        $tienda = !$tienda ? session('tienda') : $tienda;

        return $query->whereIn('id', function($q) use ($tienda){
            $q->select('item_id')->from('stocks')->where('tienda_id',$tienda)->whereNull('deleted_at');
        });

    }

    public function scopeMateriaPrima($query)
    {
        return $query->where('materia_prima',1);
    }

    public function scopeProductoFinal($query)
    {
        return $query->where('materia_prima',0);
    }

    public function esBebida(){
        return $this->categorias->contains('id',Icategoria::BEBIDA);
    }

    /**
     * Devuelve true si el item contiene la categoría combo y tiene una definición del combo
     * @return bool
     */
    public function esCombo(){
        return $this->categorias->contains('id',Icategoria::COMBO) || !is_null($this->combo);
    }


    /**
     * Valida si el articulo es combo pero el combo solo tiene el mismo articulo
     * @return bool
     */
    public function esComboUnitario(){
        return !is_null($this->combo) && $this->combo->detalles->count() == 1 ;
    }

    public function scopeWeb($query){
        return $query->where('web',1);
    }

    public function scopeAdicionales($query){
        return $query->whereIn('id', function($q){
            $q->select('item_id')->from('icategoria_item')->where('icategoria_id',Icategoria::ADICIONALES)->whereNull('deleted_at');
        });
    }

    public function scopeSalsas($query){
        return $query->whereIn('id', function($q){
            $q->select('item_id')->from('icategoria_item')->where('icategoria_id',Icategoria::ADICIONALES)->whereNull('deleted_at');
        });
    }

    public function scopeAgregados($query){
        return $query->whereIn('id', function($q){
            $q->select('item_id')->from('icategoria_item')->whereIn('icategoria_id',[Icategoria::ADICIONALES,Icategoria::SALSAS])->whereNull('deleted_at');
        });
    }

    public function scopeComboNoDefinido($query)
    {
        $query->whereNotIn('id',function ($query){
            $query->select('item_id')->from('combos')->whereNull('deleted_at');
        });
    }

    public $table = 'items';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];

    protected $appends= ['text','img_full_path','es_extra','total_modificaciones','receta','tiene_extras','tiene_salsas'];

    protected $with = ['unimed'];

    public $fillable = [
        'nombre',
        'descripcion',
        'codigo',
        'precio_venta',
        'precio_compra',
        'precio_mayoreo',
        'cantidad_mayoreo',
        'precio_promedio',
        'stock',
        'imagen',
        'ubicacion',
        'inventariable',
        'perecedero',
        'materia_prima',
        'web',
        'marca_id',
        'unimed_id',
        'icategoria_id',
        'iestado_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'nombre' => 'string',
        'descripcion' => 'string',
        'codigo' => 'string',
        'cantidad_mayoreo' => 'boolean',
        'imagen' => 'string',
        'ubicacion' => 'string',
        'inventariable' => 'boolean',
        'perecedero' => 'boolean',
        'materia_prima' => 'boolean',
        'web' => 'boolean',
        'marca_id' => 'integer',
        'unimed_id' => 'integer',
        'icategoria_id' => 'integer',
        'iestado_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        "nombre" => 'required|max:45',
        "precio_venta" => 'required|numeric',
        "precio_compra" => 'required|numeric',
        "stock" => 'required|numeric',
        'codigo'=>'unique:items'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function icategoria()
    {
        return $this->belongsTo(\App\Models\Icategoria::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function iestado()
    {
        return $this->belongsTo(\App\Models\Iestado::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function marca()
    {
        return $this->belongsTo(\App\Models\Marca::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function unimed()
    {
        return $this->belongsTo(\App\Models\Unimed::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function comboDetalles()
    {
        return $this->hasMany(\App\Models\ComboDetalle::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function combo()
    {
        return $this->hasOne(\App\Models\Combo::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function compraDetalles()
    {
        return $this->hasMany(\App\Models\CompraDetalle::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function descuentos()
    {
        return $this->hasMany(\App\Models\Descuento::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function equivalencias()
    {
        return $this->hasMany(\App\Models\Equivalencia::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     **/
    public function icategorias()
    {
        return $this->belongsToMany(\App\Models\Icategoria::class, 'icategoria_item');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function inistocks()
    {
        return $this->hasMany(\App\Models\Inistock::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function pedidoDetalles()
    {
        return $this->hasMany(\App\Models\PedidoDetalle::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function productoConsumidos()
    {
        return $this->hasMany(\App\Models\ProductoConsumido::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function productoRealizados()
    {
        return $this->hasMany(\App\Models\ProductoRealizado::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function productoSolicitados()
    {
        return $this->hasMany(\App\Models\ProductoSolicitado::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function recetaDetalles()
    {
        return $this->hasMany(\App\Models\RecetaDetalle::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function recetas()
    {
        return $this->hasMany(\App\Models\Receta::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function solicitudDetalles()
    {
        return $this->hasMany(\App\Models\SolicitudDetalle::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     **/
    public function tiendas()
    {
        return $this->belongsToMany(\App\Models\Tienda::class, 'stock_criticos');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function stocks()
    {
        return $this->hasMany(\App\Models\Stock::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function tempCompraDetalles()
    {
        return $this->hasMany(\App\Models\TempCompraDetalle::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function tempPedidoDetalles()
    {
        return $this->hasMany(\App\Models\TempPedidoDetalle::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function tempSolicitudDetalles()
    {
        return $this->hasMany(\App\Models\TempSolicitudDetalle::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function tempVentaDetalles()
    {
        return $this->hasMany(\App\Models\TempVentaDetalle::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function trasladoMateriaPrimas()
    {
        return $this->hasMany(\App\Models\TrasladoMateriaPrima::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function ventaDetalles()
    {
        return $this->hasMany(\App\Models\VentaDetalle::class);
    }

    public function getImagenAttribute($value){

        return $value ? $value : 'img/avatar_none.png';
    }

    /**
     * Verifica si el item esta en el detalle de una compra
     * @return bool
     */
    public function estaEnUnaCompra(){

        return $this->compraDetalles()->count() > 0 ? true : false;
    }

    /**
     * Verifica si el item esta en el detalle de una venta
     * @return bool
     */
    public function estaEnUnaVenta(){

        return $this->ventaDetalles()->count() > 0 ? true : false;
    }

    public function getTextAttribute()
    {
        $codigo = $this->codigo ? $this->codigo : 'sin codigo';
        return $codigo.' / '.$this->nombre;
    }

    public function getImgFullPathAttribute()
    {

        return asset($this->imagen);
    }

    public function stockTienda($tienda=null){

        $tienda = !$tienda ? session('tienda') : $tienda;

        return $this->stocks()->deTienda($tienda)->sum('cantidad');

    }

    public function stockCriticos()
    {
        return $this->belongsToMany(\App\Models\Tienda::class, 'stock_criticos')->withPivot('cantidad');
    }

    public function stockCriticoTienda($tienda=null)
    {
        $tienda = !$tienda ? session('tienda') : $tienda;

        $stock = $this->stockCriticos->filter(function ($item) use ($tienda){

            if($item->id==$tienda) {
                return $item;
            }

        });

        $stock = $stock->count()>0 ? $stock[0]->pivot->cantidad : 0;

        return $stock;
    }

    public function categorias()
    {
        return $this->belongsToMany(Icategoria::class,'icategoria_item','item_id','icategoria_id');
    }

    public function tiempos()
    {
        return $this->hasMany(ItemTiempo::class);
    }

    public function promedioPreparacion($tienda=null)
    {
        //si existen registros en la tabla item_tiempos para la tienda de session y el item
        if($this->tiempos()->deTienda($tienda)->count()>0){

            //promedio de todos los tiempos de la tienda del item
            $tiempo =  $this->tiempos()->deTienda($tienda)->avg('preparacion') ;
        }else{
            $tiempo =  0;
        }

        return $tiempo;
    }

    public function promedioEntrega($tienda=null)
    {
        //si existen registros en la tabla item_tiempos para la tienda de session y el item
        if($this->tiempos()->deTienda($tienda)->count()>0){

            //promedio de todos los tiempos de la tienda del item
            $tiempo =  $this->tiempos()->deTienda($tienda)->avg('entrega') ;
        }else{
            $tiempo =  0;
        }

        return $tiempo;
    }

    public function ventaRecetas()
    {
        return $this->hasMany(VentaReceta::class,'item_id','id');
    }

    public function ventaReceta()
    {
        return $this->ventaRecetas()->where('activa',1)->first();
    }

    public function esInventariable()
    {
        return ($this->inventariable && !$this->esCombo());
    }

    public function adicionales()
    {
        return $this->belongsToMany(Item::class,'item_extras','item','extra');
    }

    public function salsas()
    {
        return $this->belongsToMany(Item::class,'item_salsas','item','salsa');
    }

    public function receta()
    {
        return $this->tieneReceta() ? $this->ventaRecetas()->first() : null;
    }

    public function tieneReceta()
    {
        return $this->ventaRecetas()->count() > 0 ? true : false;
    }

    public function nombresIngredientes()
    {
        $ingredientes = [];
        foreach ($this->ventaReceta()->detalles as $detalle){
            $ingredientes[] = $detalle->ingrediente->nombre;
        }

        return $ingredientes;
    }

    public function esExtra()
    {
        return $this->categorias->contains('id',Icategoria::ADICIONALES) || $this->categorias->contains('id',Icategoria::SALSAS);
    }

    public function getEsExtraAttribute()
    {
        return $this->esExtra();
    }

    public function totalModificaciones()
    {
        $modificaciones = session('modificaciones');

        if (isset($modificaciones[$this->id])){
            return $modificaciones[$this->id]['total'];
        }

        return 0;
    }

    public function getTotalModificacionesAttribute()
    {
        return $this->totalModificaciones();
    }

    public function getRecetaAttribute()
    {
        return $this->receta();
    }

    public function tieneExtras()
    {
        return $this->adicionales->count() > 0 ? true : false;
    }

    public function tieneSalsas()
    {
        return $this->salsas->count() > 0 ? true : false;
    }

    public function getTieneSalsasAttribute()
    {
        return $this->tieneSalsas();
    }

    public function getTieneExtrasAttribute()
    {
        return $this->tieneExtras();
    }

    public function catProducto()
    {
        return $this->categorias->contains('id',Icategoria::PRODUCTO);
    }

    public function scopeTipoProducto($query)
    {
        return $query->whereIn('id', function($q){
            $q->select('item_id')->from('icategoria_item')->where('icategoria_id',Icategoria::PRODUCTO)->whereNull('deleted_at');
        });
    }
}
