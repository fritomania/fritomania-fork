<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Pestado
 *
 * @package App\Models
 * @version July 12, 2017, 10:48 am CST
 * @property int $id
 * @property string $descripcion
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property \Carbon\Carbon|null $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Pestado onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Pestado whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Pestado whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Pestado whereDescripcion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Pestado whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Pestado whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Pestado withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Pestado withoutTrashed()
 * @mixin \Eloquent
 */
class Pestado extends Model
{
    use SoftDeletes;

    public $table = 'pestados';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'descripcion'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'descripcion' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'descripcion' => 'required|max:45'
    ];

    
}
