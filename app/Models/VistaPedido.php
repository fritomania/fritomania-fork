<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Venta
 *
 * @package App\Models
 * @version April 17, 2017, 11:07 am CST
 * @property int $id
 * @property int|null $numero
 * @property string|null $cliente
 * @property string|null $fecha
 * @property string|null $hora
 * @property string|null $fecha_ingreso
 * @property string|null $fecha_entrega
 * @property string $estado
 * @property string $usuario
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VistaPedido whereCliente($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VistaPedido whereEstado($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VistaPedido whereFecha($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VistaPedido whereFechaEntrega($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VistaPedido whereFechaIngreso($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VistaPedido whereHora($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VistaPedido whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VistaPedido whereNumero($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VistaPedido whereUsuario($value)
 * @mixin \Eloquent
 */
class VistaPedido extends Model
{
//    use SoftDeletes;

    public $table = 'vista_pedidos';


}
