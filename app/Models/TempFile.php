<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class TempFile
 *
 * @package App\Models
 * @version September 23, 2017, 2:41 pm CST
 * @mixin \Eloquent
 */
class TempFile extends Model
{

    public $table = 'temp_files';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'user_id',
        'opcion',
        'data',
        'nombre',
        'type',
        'size',
        'extension'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'user_id' => 'integer',
        'opcion' => 'string',
        'data' => 'string',
        'nombre' => 'string',
        'type' => 'string',
        'size' => 'integer',
        'extension' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
