<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Cpago
 *
 * @package App\Models
 * @version February 13, 2018, 10:32 am CST
 * @property int $id
 * @property int $compra_id
 * @property float $monto
 * @property int $user_id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property \Carbon\Carbon|null $deleted_at
 * @property-read \App\Models\Compra $compra
 * @property-read \App\User $user
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Cpago onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cpago whereCompraId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cpago whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cpago whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cpago whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cpago whereMonto($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cpago whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cpago whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Cpago withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Cpago withoutTrashed()
 * @mixin \Eloquent
 */
class Cpago extends Model
{
    use SoftDeletes;

    public $table = 'cpagos';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'compra_id',
        'monto',
        'user_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'compra_id' => 'integer',
        'user_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function compra()
    {
        return $this->belongsTo(\App\Models\Compra::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function user()
    {
        return $this->belongsTo(\App\User::class);
    }
}
