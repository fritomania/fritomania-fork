<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;



/**
 * App\Models\Compra
 *
 * @property int $id
 * @property int $proveedor_id
 * @property int $tcomprobante_id
 * @property string|null $fecha Fecha del docuemnto de  la Factura
 * @property string|null $fecha_ingreso Fecha de ingreso al sistema
 * @property string $fecha_ingreso_plan Fecha Promesa del Proveedor
 * @property string|null $fecha_limite_credito
 * @property string|null $fecha_anula
 * @property string|null $fecha_cancela
 * @property string $serie
 * @property string $numero
 * @property bool $credito
 * @property bool $pagada
 * @property int $user_id
 * @property int $tienda_id
 * @property int $cestado_id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property \Carbon\Carbon|null $deleted_at
 * @property-read \App\Models\Cestado $cestado
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\CompraDetalle[] $compraDetalles
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Cpago[] $cpagos
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\CompraDetalle[] $detalles
 * @property-read \App\Models\Cestado $estado
 * @property-read mixed $total
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Cpago[] $pagos
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Pedido[] $pedidos
 * @property-read \App\Models\Proveedor|null $proveedor
 * @property-read \App\Models\Tcomprobante $tcomprobante
 * @property-read \App\Models\Tienda $tienda
 * @property-read \App\User $user
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Venta[] $ventas
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Compra delItem($item)
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Compra onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Compra whereCestadoId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Compra whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Compra whereCredito($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Compra whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Compra whereFecha($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Compra whereFechaAnula($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Compra whereFechaIngreso($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Compra whereFechaIngresoPlan($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Compra whereFechaLimiteCredito($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Compra whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Compra whereNumero($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Compra wherePagada($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Compra whereProveedorId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Compra whereSerie($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Compra whereTcomprobanteId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Compra whereTiendaId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Compra whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Compra whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Compra withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Compra withoutTrashed()
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Compra whereFechaCancela($value)
 */
class Compra extends Model
{

    public function scopeDelItem($query,$item)
    {
        return $query->whereIn('id', function($q) use ($item){
            $q->select('compra_id')->from('compra_detalles')->where('item_id',$item);
        });
    }

    use SoftDeletes;

    public $table = 'compras';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'proveedor_id',
        'tcomprobante_id',
        'fecha',
        'fecha_ingreso',
        'fecha_ingreso_plan',
        'serie',
        'numero',
        'credito',
        'pagada',
        'user_id',
        'tienda_id',
        'cestado_id',
        'fecha_limite_credito',
        'fecha_anula',
        'fecha_cancela'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'proveedor_id' => 'integer',
        'tcomprobante_id' => 'integer',
        'serie' => 'string',
        'numero' => 'string',
        'credito' => 'boolean',
        'pagada' => 'boolean',
        'user_id' => 'integer',
        'tienda_id' => 'integer',
        'cestado_id' => 'integer',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'proveedor_id' => 'required',
        'tcomprobante_id' => 'required',
        'fecha_ingreso_plan' => 'required',
//        'numero' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function proveedor()
    {
        return $this->belongsTo(\App\Models\Proveedor::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function cestado()
    {
        return $this->belongsTo(\App\Models\Cestado::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function tcomprobante()
    {
        return $this->belongsTo(\App\Models\Tcomprobante::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function tienda()
    {
        return $this->belongsTo(\App\Models\Tienda::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function user()
    {
        return $this->belongsTo(\App\User::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function compraDetalles()
    {
        return $this->hasMany(\App\Models\CompraDetalle::class)->withTrashed();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function detalles()
    {
        return $this->hasMany(\App\Models\CompraDetalle::class,'compra_id','id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     **/
    public function pedidos()
    {
        return $this->belongsToMany(\App\Models\Pedido::class, 'compra_pedidos');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     **/
    public function ventas()
    {
        return $this->belongsToMany(\App\Models\Venta::class, 'compra_venta');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function cpagos()
    {
        return $this->hasMany(\App\Models\Cpago::class);
    }

    public function pagos()
    {
        return $this->hasMany(\App\Models\Cpago::class,'compra_id','id');
    }

    public function getTotalAttribute()
    {
        $total=0;
        foreach ($this->compraDetalles as $detalle){
            $total+=($detalle->cantidad*$detalle->precio);
        }

        return $total;
    }

    public function estado()
    {
        return $this->belongsTo(\App\Models\Cestado::class);
    }
}
