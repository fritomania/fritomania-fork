<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Horario
 *
 * @package App\Models
 * @version April 20, 2017, 11:11 am CST
 * @property int $id
 * @property mixed $dia
 * @property int $hora_ini
 * @property int $hora_fin
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property \Carbon\Carbon|null $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Horario onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Horario whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Horario whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Horario whereDia($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Horario whereHoraFin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Horario whereHoraIni($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Horario whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Horario whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Horario withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Horario withoutTrashed()
 * @mixin \Eloquent
 */
class Horario extends Model
{
    use SoftDeletes;

    public $table = 'horarios';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'dia',
        'hora_ini',
        'hora_fin'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'dia' => 'days',
        'hora_ini' => 'integer',
        'hora_fin' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'dia' => 'required|max:1|unique:horarios',
        'hora_ini' => 'required|numeric|digits_between:1,2|between:0,24',
        'hora_fin' => 'required|numeric|digits_between:1,2|between:0,24'
    ];


}
