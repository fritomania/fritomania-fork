<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class PedidoDetalle
 *
 * @package App\Models
 * @version July 13, 2017, 10:06 am CST
 * @property int $id
 * @property int $pedido_id
 * @property int $item_id
 * @property float $cantidad
 * @property float $precio
 * @property float $descuento
 * @property int $ingreso
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property \Carbon\Carbon|null $deleted_at
 * @property-read \App\Models\Item $item
 * @property-read \App\Models\Pedido $pedido
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\PedidoDetalle onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PedidoDetalle whereCantidad($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PedidoDetalle whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PedidoDetalle whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PedidoDetalle whereDescuento($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PedidoDetalle whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PedidoDetalle whereIngreso($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PedidoDetalle whereItemId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PedidoDetalle wherePedidoId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PedidoDetalle wherePrecio($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PedidoDetalle whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\PedidoDetalle withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\PedidoDetalle withoutTrashed()
 * @mixin \Eloquent
 */
class PedidoDetalle extends Model
{
    use SoftDeletes;

    public $table = 'pedido_detalles';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'pedido_id',
        'item_id',
        'cantidad',
        'precio',
        'descuento'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'pedido_id' => 'integer',
        'item_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function item()
    {
        return $this->belongsTo(\App\Models\Item::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function pedido()
    {
        return $this->belongsTo(\App\Models\Pedido::class);
    }
}
