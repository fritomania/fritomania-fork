<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class TempVenta
 *
 * @package App\Models
 * @version April 15, 2017, 4:54 pm CST
 * @property int $id
 * @property int $user_id
 * @property bool $procesada
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property \Carbon\Carbon|null $deleted_at
 * @property-read \App\Models\Cliente $cliente
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\TempVentaDetalle[] $detalles
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\TempVentaDetalle[] $tempVentaDetalles
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\TempVenta onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TempVenta whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TempVenta whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TempVenta whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TempVenta whereProcesada($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TempVenta whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TempVenta whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\TempVenta withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\TempVenta withoutTrashed()
 * @mixin \Eloquent
 */
class TempVenta extends Model
{
    use SoftDeletes;

    public $table = 'temp_ventas';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'cliente_id',
        'fecha',
        'serie',
        'numero',
        'procesada',
        'user_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'cliente_id' => 'integer',
        'serie' => 'string',
        'numero' => 'string',
        'procesada' => 'boolean',
        'user_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function cliente()
    {
        return $this->belongsTo(\App\Models\Cliente::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function tempVentaDetalles()
    {
        return $this->hasMany(\App\Models\TempVentaDetalle::class);
    }

    public function detalles()
    {
        return $this->hasMany(\App\Models\TempVentaDetalle::class,'temp_venta_id','id');
    }
}
