<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Descuento
 *
 * @package App\Models
 * @version June 25, 2017, 6:20 pm CST
 * @property int $id
 * @property int $item_id
 * @property int $porcentaje
 * @property string|null $fecha_inicio
 * @property string|null $fecha_expira
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property \Carbon\Carbon|null $deleted_at
 * @property-read \App\Models\Item $item
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Descuento onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Descuento whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Descuento whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Descuento whereFechaExpira($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Descuento whereFechaInicio($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Descuento whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Descuento whereItemId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Descuento wherePorcentaje($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Descuento whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Descuento withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Descuento withoutTrashed()
 * @mixin \Eloquent
 */
class Descuento extends Model
{
    use SoftDeletes;

    public $table = 'descuentos';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'item_id',
        'porcentaje',
        'fecha_inicio',
        'fecha_expira'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'item_id' => 'integer',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'item_id' => 'required',
        'porcentaje' => 'required|numeric|digits_between:1,2|between:0,100',
        'fecha_inicio' => 'required|date',
        'fecha_expira' => 'required|date'

    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function item()
    {
        return $this->belongsTo(\App\Models\Item::class);
    }

    public function getFechaInicioAttribute($d)
    {
        return fecha($d);
    }

    public function getFechaExpiraAttribute($d)
    {
        return fecha($d);
    }
}
