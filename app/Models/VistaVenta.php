<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Venta
 *
 * @package App\Models
 * @version April 17, 2017, 11:07 am CST
 * @property int $id
 * @property string|null $cliente
 * @property string|null $ns
 * @property string|null $fecha
 * @property string|null $hora
 * @property string $estado
 * @property string $usuario
 * @property string|null $tipo_pago
 * @property int $user_id
 * @property int $credito
 * @property int $pagada
 * @property float|null $descuento
 * @property int $vestado_id
 * @property int|null $cliente_id
 * @property int|null $tipo_pago_id
 * @property int|null $tienda_id
 * @property float|null $saldo
 * @property \Carbon\Carbon|null $created_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VistaVenta anuladas()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VistaVenta credito()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VistaVenta deTienda($tienda = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VistaVenta delUser($user)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VistaVenta hoy()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VistaVenta noCredito()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VistaVenta noNegocio()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VistaVenta porCobrar()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VistaVenta procesadas()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VistaVenta whereCliente($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VistaVenta whereClienteId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VistaVenta whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VistaVenta whereCredito($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VistaVenta whereDescuento($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VistaVenta whereEstado($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VistaVenta whereFecha($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VistaVenta whereHora($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VistaVenta whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VistaVenta whereNs($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VistaVenta wherePagada($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VistaVenta whereSaldo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VistaVenta whereTiendaId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VistaVenta whereTipoPago($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VistaVenta whereTipoPagoId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VistaVenta whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VistaVenta whereUsuario($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VistaVenta whereVestadoId($value)
 * @mixin \Eloquent
 */
class VistaVenta extends Model
{
//    use SoftDeletes;

    public function scopeDeTienda($query,$tienda=null)
    {
        $tienda = isset($tienda) ? $tienda : session('tienda');
        return $query->where('tienda_id',$tienda);
    }

    public function scopeDelUser($query,$user)
    {
        return $query->where('user_id',$user);
    }

    public function scopeProcesadas($query)
    {
        return $query->where('vestado_id','=','1');
    }

    public function scopeNoNegocio($query)
    {
        return $query->where('cliente_id','!=','2');
    }

    public function scopeNoCredito($query)
    {
        return $query->where('credito',0);
    }

    public function scopeCredito($query)
    {
        return $query->where('credito',1);
    }

    public function scopeHoy($query)
    {
        return $query->where('fecha','=',hoyDb());
    }

    public function scopePorCobrar($query)
    {
        return $query->where('credito',1)->where('pagada',0);
    }

    public function scopeAnuladas($query)
    {
        return $query->where('vestado_id','=','2');
    }


    public $table = 'vista_ventas';

}
