<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Vpago
 *
 * @package App\Models
 * @version February 7, 2018, 4:59 pm CST
 * @property int $id
 * @property int $venta_id
 * @property float $monto
 * @property int $user_id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property \Carbon\Carbon|null $deleted_at
 * @property-read \App\User $user
 * @property-read \App\Models\Venta $venta
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Vpago hoy()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Vpago onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Vpago whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Vpago whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Vpago whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Vpago whereMonto($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Vpago whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Vpago whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Vpago whereVentaId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Vpago withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Vpago withoutTrashed()
 * @mixin \Eloquent
 */
class Vpago extends Model
{

    public function scopeHoy($query)
    {
        return $query->whereDate('created_at',hoyDb());
    }

    use SoftDeletes;

    public $table = 'vpagos';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'venta_id',
        'monto',
        'user_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'venta_id' => 'integer',
        'user_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'venta_id' => 'required',
        'monto' => 'required|numeric',
        'user_id' => 'required',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function user()
    {
        return $this->belongsTo(\App\User::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function venta()
    {
        return $this->belongsTo(\App\Models\Venta::class);
    }
}
