<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\Produccione
 *
 * @property int $id
 * @property string $numero
 * @property int $produccion_tipo_id
 * @property int $produccion_estado_id
 * @property int $user_id
 * @property string|null $fecha_anula
 * @property string|null $fecha_cancela
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property \Carbon\Carbon|null $deleted_at
 * @property-read \App\Models\ProduccionEstado $estado
 * @property-read \App\Models\ProduccionEstado $produccionEstado
 * @property-read \App\Models\ProduccionTipo $produccionTipo
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ProductoConsumido[] $productoConsumidos
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ProductoRealizado[] $productoRealizados
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ProductoSolicitado[] $productoSolicitados
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\TrasladoMateriaPrima[] $trasladoMateriaPrimas
 * @property-read \App\User $user
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Produccione onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Produccione whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Produccione whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Produccione whereFechaAnula($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Produccione whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Produccione whereNumero($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Produccione whereProduccionEstadoId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Produccione whereProduccionTipoId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Produccione whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Produccione whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Produccione withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Produccione withoutTrashed()
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Produccione whereFechaCancela($value)
 */
class Produccione extends Model
{
    use SoftDeletes;

    public $table = 'producciones';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'numero',
        'produccion_tipo_id',
        'produccion_estado_id',
        'user_id',
        'fecha_anula',
        'fecha_cancela'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'numero' => 'string',
        'produccion_tipo_id' => 'integer',
        'produccion_estado_id' => 'integer',
        'user_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function produccionEstado()
    {
        return $this->belongsTo(\App\Models\ProduccionEstado::class);
    }

    public function estado()
    {
        return $this->belongsTo(\App\Models\ProduccionEstado::class,'produccion_estado_id','id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function produccionTipo()
    {
        return $this->belongsTo(\App\Models\ProduccionTipo::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function user()
    {
        return $this->belongsTo(\App\User::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function productoConsumidos()
    {
        return $this->hasMany(\App\Models\ProductoConsumido::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function productoRealizados()
    {
        return $this->hasMany(\App\Models\ProductoRealizado::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function productoSolicitados()
    {
        return $this->hasMany(\App\Models\ProductoSolicitado::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function trasladoMateriaPrimas()
    {
        return $this->hasMany(\App\Models\TrasladoMateriaPrima::class);
    }

    public function enProceso()
    {
        return $this->productoRealizados->count() > 0 ? true : false;
    }
}
