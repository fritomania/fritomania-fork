<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class SolicitudDetalle
 * @package App\Models
 * @version July 31, 2018, 6:39 pm CST
 *
 * @property \App\Models\Item item
 * @property \App\Models\Solicitude solicitude
 * @property \Illuminate\Database\Eloquent\Collection compraDetalles
 * @property \Illuminate\Database\Eloquent\Collection compraPedidos
 * @property \Illuminate\Database\Eloquent\Collection compraVenta
 * @property \Illuminate\Database\Eloquent\Collection cpagos
 * @property \Illuminate\Database\Eloquent\Collection egresoSolicitud
 * @property \Illuminate\Database\Eloquent\Collection egresos
 * @property \Illuminate\Database\Eloquent\Collection equivalencias
 * @property \Illuminate\Database\Eloquent\Collection icategoriaItem
 * @property \Illuminate\Database\Eloquent\Collection ingresos
 * @property \Illuminate\Database\Eloquent\Collection optionUser
 * @property \Illuminate\Database\Eloquent\Collection pedidoDetalles
 * @property \Illuminate\Database\Eloquent\Collection pedidoVentas
 * @property \Illuminate\Database\Eloquent\Collection pedidos
 * @property \Illuminate\Database\Eloquent\Collection recetaDetalles
 * @property \Illuminate\Database\Eloquent\Collection recetas
 * @property \Illuminate\Database\Eloquent\Collection rolUser
 * @property \Illuminate\Database\Eloquent\Collection stocks
 * @property \Illuminate\Database\Eloquent\Collection tempCompraDetalles
 * @property \Illuminate\Database\Eloquent\Collection tempPedidoDetalles
 * @property \Illuminate\Database\Eloquent\Collection tempSolicitudDetalles
 * @property \Illuminate\Database\Eloquent\Collection tempVentaDetalles
 * @property \Illuminate\Database\Eloquent\Collection ventaDetalles
 * @property \Illuminate\Database\Eloquent\Collection vpagos
 * @property integer solicitude_id
 * @property integer item_id
 * @property decimal cantidad
 */
class SolicitudDetalle extends Model
{
    use SoftDeletes;

    public $table = 'solicitud_detalles';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];

    protected $appends = ['sub_total'];

    public $fillable = [
        'solicitude_id',
        'item_id',
        'cantidad',
        'precio'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'solicitude_id' => 'integer',
        'item_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function item()
    {
        return $this->belongsTo(\App\Models\Item::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function solicitude()
    {
        return $this->belongsTo(\App\Models\Solicitude::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     **/
    public function stocks()
    {
        return $this->belongsToMany(\App\Models\Stock::class, 'egreso_solicitud')->withPivot('cantidad');
    }

    public function egresos()
    {
        return $this->belongsToMany(\App\Models\Stock::class, 'egreso_solicitud')->withPivot('cantidad');
    }

    public function ingresos()
    {
        return $this->belongsToMany(\App\Models\Stock::class, 'ingreso_solicitud')->withPivot('cantidad');
    }

    public function getSubTotalAttribute()
    {
        return $this->precio * $this->cantidad;
    }
}
