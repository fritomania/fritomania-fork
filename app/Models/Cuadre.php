<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Cuadre
 *
 * @package App\Models
 * @version April 13, 2018, 4:06 pm CST
 * @property int $id
 * @property string $fecha
 * @property float $total_sistema
 * @property float $cash
 * @property int $tienda_id
 * @property int|null $user_id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property \Carbon\Carbon|null $deleted_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cuadre deFecha($fecha)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cuadre deTienda($tienda = null)
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Cuadre onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cuadre whereCash($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cuadre whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cuadre whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cuadre whereFecha($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cuadre whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cuadre whereTiendaId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cuadre whereTotalSistema($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cuadre whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cuadre whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Cuadre withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Cuadre withoutTrashed()
 * @mixin \Eloquent
 */
class Cuadre extends Model
{
    use SoftDeletes;

    public function scopeDeFecha($query,$fecha)
    {
        return $query->where('fecha',$fecha)->whereNull('deleted_at');
    }

    public $table = 'cuadres';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'fecha',
        'total_sistema',
        'cash',
        'tienda_id',
        'user_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'fecha' => 'required',
        'cash' => 'required|numeric'
    ];

    public function scopeDeTienda($q,$tienda=null)
    {
        $tienda = is_null($tienda) ? session('tienda') : $tienda;

        $q->where('tienda_id',$tienda);
    }


}
