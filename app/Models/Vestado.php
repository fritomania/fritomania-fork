<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Vestado
 *
 * @package App\Models
 * @version July 25, 2018, 2:19 pm CST
 * @property \Illuminate\Database\Eloquent\Collection clienteEquipo
 * @property \Illuminate\Database\Eloquent\Collection compraDetalles
 * @property \Illuminate\Database\Eloquent\Collection compraPedidos
 * @property \Illuminate\Database\Eloquent\Collection compraVenta
 * @property \Illuminate\Database\Eloquent\Collection cpagos
 * @property \Illuminate\Database\Eloquent\Collection cpuEquipo
 * @property \Illuminate\Database\Eloquent\Collection cpus
 * @property \Illuminate\Database\Eloquent\Collection egresos
 * @property \Illuminate\Database\Eloquent\Collection equipoAtributos
 * @property \Illuminate\Database\Eloquent\Collection equipoRmodulo
 * @property \Illuminate\Database\Eloquent\Collection equivalencias
 * @property \Illuminate\Database\Eloquent\Collection icategoriaItem
 * @property \Illuminate\Database\Eloquent\Collection ingresos
 * @property \Illuminate\Database\Eloquent\Collection optionUser
 * @property \Illuminate\Database\Eloquent\Collection pedidoDetalles
 * @property \Illuminate\Database\Eloquent\Collection pedidoVentas
 * @property \Illuminate\Database\Eloquent\Collection pedidos
 * @property \Illuminate\Database\Eloquent\Collection programaServicio
 * @property \Illuminate\Database\Eloquent\Collection rmodulos
 * @property \Illuminate\Database\Eloquent\Collection rolUser
 * @property \Illuminate\Database\Eloquent\Collection servicioArchivos
 * @property \Illuminate\Database\Eloquent\Collection sgastos
 * @property \Illuminate\Database\Eloquent\Collection stocks
 * @property \Illuminate\Database\Eloquent\Collection tempCompraDetalles
 * @property \Illuminate\Database\Eloquent\Collection tempPedidoDetalles
 * @property \Illuminate\Database\Eloquent\Collection tempVentaDetalles
 * @property \Illuminate\Database\Eloquent\Collection ventaDetalles
 * @property \Illuminate\Database\Eloquent\Collection Venta
 * @property \Illuminate\Database\Eloquent\Collection vpagos
 * @property string descripcion
 */
class Vestado extends Model
{
    use SoftDeletes;

    public $table = 'vestados';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    const PAGADA = 1;
    const ANULADA = 2;
    const ENVIADA_COCINA = 3;
    const COCINANDO = 4;
    const LISTA = 5;
    const EN_RUTA = 6;
    const ENTREGADA = 7;
    const TEMPORAL = 8;
    const CANCELADA = 9;


    protected $dates = ['deleted_at'];

    protected $appends =['nombre'];

    public $fillable = [
        'descripcion'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'descripcion' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function ventas()
    {
        return $this->hasMany(\App\Models\Venta::class);
    }

    public function getNombreAttribute()
    {
        return $this->descripcion;
    }
}
