<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Notificacione
 * @package App\Models
 * @version August 28, 2018, 8:33 pm -03
 *
 * @property \App\Models\NotificacionTipo notificacionTipo
 * @property \App\User user
 * @property \Illuminate\Database\Eloquent\Collection asiganacionDelivery
 * @property \Illuminate\Database\Eloquent\Collection compraDetalles
 * @property \Illuminate\Database\Eloquent\Collection compraPedidos
 * @property \Illuminate\Database\Eloquent\Collection compraVenta
 * @property \Illuminate\Database\Eloquent\Collection cpagos
 * @property \Illuminate\Database\Eloquent\Collection egresoProduccion
 * @property \Illuminate\Database\Eloquent\Collection egresoSolicitud
 * @property \Illuminate\Database\Eloquent\Collection egresos
 * @property \Illuminate\Database\Eloquent\Collection equivalencias
 * @property \Illuminate\Database\Eloquent\Collection icategoriaItem
 * @property \Illuminate\Database\Eloquent\Collection ingresoProduccion
 * @property \Illuminate\Database\Eloquent\Collection ingresoSolicitud
 * @property \Illuminate\Database\Eloquent\Collection ingresos
 * @property \Illuminate\Database\Eloquent\Collection optionUser
 * @property \Illuminate\Database\Eloquent\Collection ordenDetalles
 * @property \Illuminate\Database\Eloquent\Collection pedidoDetalles
 * @property \Illuminate\Database\Eloquent\Collection pedidoVentas
 * @property \Illuminate\Database\Eloquent\Collection pedidos
 * @property \Illuminate\Database\Eloquent\Collection productoConsumidos
 * @property \Illuminate\Database\Eloquent\Collection productoSolicitados
 * @property \Illuminate\Database\Eloquent\Collection recetaDetalles
 * @property \Illuminate\Database\Eloquent\Collection recetas
 * @property \Illuminate\Database\Eloquent\Collection rolUser
 * @property \Illuminate\Database\Eloquent\Collection solicitudDetalles
 * @property \Illuminate\Database\Eloquent\Collection stockCriticos
 * @property \Illuminate\Database\Eloquent\Collection stocks
 * @property \Illuminate\Database\Eloquent\Collection tempCompraDetalles
 * @property \Illuminate\Database\Eloquent\Collection tempPedidoDetalles
 * @property \Illuminate\Database\Eloquent\Collection tempSolicitudDetalles
 * @property \Illuminate\Database\Eloquent\Collection tempVentaDetalles
 * @property \Illuminate\Database\Eloquent\Collection tiendaDespachaTienda
 * @property \Illuminate\Database\Eloquent\Collection userDespachaUser
 * @property \Illuminate\Database\Eloquent\Collection ventaDetalles
 * @property \Illuminate\Database\Eloquent\Collection vpagos
 * @property string mensaje
 * @property integer user_id
 * @property integer notificacion_tipo_id
 * @property boolean vista
 */
class Notificacione extends Model
{
    use SoftDeletes;

    public $table = 'notificaciones';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public function scopeDelUser($query,$user)
    {
        return $query->where('user_id',$user);
    }

    public function scopeVista($query)
    {
        return $query->where('vista',1);
    }

    public function scopeNoVistas($query)
    {
        return $query->where('vista',0);
    }

    public function scopeDeTipo($query,$tipo)
    {
        return $query->where('notificacion_tipo_id',$tipo);
    }

    public $fillable = [
        'mensaje',
        'user_id',
        'notificacion_tipo_id',
        'vista'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'mensaje' => 'string',
        'user_id' => 'integer',
        'notificacion_tipo_id' => 'integer',
        'vista' => 'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'mensaje' => 'required|max:255',
        'user_id' => 'required',
        'notificacion_tipo_id' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function notificacionTipo()
    {
        return $this->belongsTo(\App\Models\NotificacionTipo::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function user()
    {
        return $this->belongsTo(\App\User::class);
    }
}
