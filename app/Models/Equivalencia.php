<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Equivalencia
 *
 * @package App\Models
 * @version March 19, 2018, 4:10 pm CST
 * @property int $id
 * @property int $item_origen
 * @property int $item_destino
 * @property float $cantidad
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property \Carbon\Carbon|null $deleted_at
 * @property-read \App\Models\Item $itemDestino
 * @property-read \App\Models\Item $itemOrigen
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Equivalencia onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Equivalencia whereCantidad($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Equivalencia whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Equivalencia whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Equivalencia whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Equivalencia whereItemDestino($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Equivalencia whereItemOrigen($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Equivalencia whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Equivalencia withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Equivalencia withoutTrashed()
 * @mixin \Eloquent
 */
class Equivalencia extends Model
{
    use SoftDeletes;

    public $table = 'equivalencias';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'item_origen',
        'item_destino',
        'cantidad'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'item_origen' => 'integer',
        'item_destino' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function itemOrigen()
    {
        return $this->belongsTo(\App\Models\Item::class,'item_origen');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function itemDestino()
    {
        return $this->belongsTo(\App\Models\Item::class,'item_destino');
    }
}
