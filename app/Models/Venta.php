<?php

namespace App\Models;

use App\User;
use Carbon\Carbon;
use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;


/**
 * App\Models\Venta
 *
 * @property-read \App\Models\Caja $caja
 * @property-read \App\Models\Cliente $cliente
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Compra[] $compras
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\VentaDetalle[] $detalles
 * @property-read \App\Models\Vestado $estado
 * @property-read mixed $fecha_entrega_f
 * @property-read mixed $sub_total
 * @property-read mixed $tiempo_despacho
 * @property-read array $tiempos
 * @property-read mixed $total
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Vestado[] $historicoEstados
 * @property-read \App\Models\Tienda $local
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Pedido[] $pedidos
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\User[] $repartidores
 * @property-read \App\Models\TipoPago $tipoPago
 * @property-read \App\User $user
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\VentaDetalle[] $ventaDetalles
 * @property-read \App\Models\Vestado $vestado
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Vpago[] $vpagos
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Vpago[] $pagos
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Venta credito()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Venta deEstados($estados = array())
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Venta deFecha($fecha)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Venta delivery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Venta efectivo()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Venta enCocina()
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Venta hoy()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Venta listas()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Venta locales()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Venta noCredito()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Venta noNegocio()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Venta onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Venta orden()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Venta pagadas()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Venta procesadas()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Venta tarjeta()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Venta tienda($tienda = null)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Venta withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Venta withoutTrashed()
 * @mixin \Eloquent
 * @property int $id
 * @property int $cliente_id
 * @property int|null $correlativo
 * @property string $fecha
 * @property string|null $fecha_entrega
 * @property string|null $hora_entrega
 * @property string $serie
 * @property string $numero
 * @property float $recibido
 * @property float $monto_delivery
 * @property int $delivery
 * @property bool $credito
 * @property bool $pagada
 * @property string|null $direccion
 * @property string|null $observaciones
 * @property string|null $nombre_entrega
 * @property string|null $telefono
 * @property string|null $correo
 * @property int $retiro_en_local
 * @property int $web
 * @property int|null $cod_web_pay
 * @property int $vestado_id
 * @property int $caja_id
 * @property int|null $tienda_id
 * @property int $tipo_pago_id
 * @property float|null $descuento
 * @property int $user_id
 * @property string|null $fecha_anula
 * @property string|null $fecha_cancela
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property \Carbon\Carbon|null $deleted_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Venta whereCajaId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Venta whereClienteId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Venta whereCodWebPay($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Venta whereCorrelativo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Venta whereCorreo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Venta whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Venta whereCredito($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Venta whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Venta whereDelivery($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Venta whereDescuento($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Venta whereDireccion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Venta whereFecha($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Venta whereFechaAnula($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Venta whereFechaCancela($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Venta whereFechaEntrega($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Venta whereHoraEntrega($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Venta whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Venta whereMontoDelivery($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Venta whereNombreEntrega($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Venta whereNumero($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Venta whereObservaciones($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Venta wherePagada($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Venta whereRecibido($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Venta whereRetiroEnLocal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Venta whereSerie($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Venta whereTelefono($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Venta whereTiendaId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Venta whereTipoPagoId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Venta whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Venta whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Venta whereVestadoId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Venta whereWeb($value)
 */
class Venta extends Model
{
    public function scopeEfectivo($query)
    {
        return $query->whereIn('tipo_pago_id',[TipoPago::EFECTIVO]);
    }

    public function scopeTarjeta($query)
    {
        return $query->whereIn('tipo_pago_id',[TipoPago::TARJETA_CREDITO,TipoPago::TARJETA_DEBITO]);
    }

    public function scopeProcesadas($query)
    {
        return $query->whereNotIn('vestado_id',[Vestado::ANULADA]);
    }

    public function scopePagadas($query)
    {
        return $query->where('vestado_id','=',Vestado::PAGADA);
    }

    public function scopeEnCocina($query)
    {
        return $query->whereIn('vestado_id',[Vestado::ENVIADA_COCINA,Vestado::COCINANDO]);
    }

    public function scopeListas($query)
    {
        return $query->whereIn('vestado_id',[Vestado::LISTA]);
    }

    public function scopeNoNegocio($query)
    {
        return $query->where('cliente_id','!=','2');
    }

    public function scopeNoCredito($query)
    {
        return $query->where('credito',0);
    }

    public function scopeCredito($query)
    {
        return $query->where('credito',1);
    }

    public function scopeHoy($query)
    {
        return $query->where('fecha','=',hoyDb());
    }

    public function scopeDeFecha($query,$fecha)
    {
        return $query->where('fecha','=',$fecha);
    }

    public function scopeOrden($query)
    {
        return $query->whereNotIn('vestado_id',[Vestado::ENTREGADA,Vestado::ANULADA,Vestado::TEMPORAL]);
    }
    public function scopeTienda($query,$tienda=null)
    {
        $tienda = isset($tienda) ? $tienda : session('tienda');
        return $query->where('tienda_id',$tienda);
    }

    public function scopeDeEstados($query,$estados=[])
    {
        if (count($estados)==0){
            return $query;
        }

        return $query->whereIn('vestado_id',$estados);
    }

    public function scopeDelivery($query)
    {
        return $query->where('delivery',1);
    }

    public function scopeLocales($query)
    {
        return $query->where('delivery',0);
    }

    use SoftDeletes;

    public $table = 'ventas';
    protected $appends = [
        'sub_total','total','tiempo_despacho','tiempos','fecha_entrega_f'
    ];
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];

    protected $with = ['estado'];

    public $fillable = [
        'cliente_id',
        'correlativo',
        'fecha',
        'fecha_entrega',
        'hora_entrega',
        'serie',
        'numero',
        'vestado_id',
        'delivery',
        'recibido',
        'monto_delivery',
        'observaciones',
        'direccion',
        'nombre_entrega',
        'telefono',
        'correo',
        'retiro_en_local',
        'web',
        'cod_web_pay',
        'user_id',
        'credito',
        'caja_id',
        'tienda_id',
        'tipo_pago_id',
        'fecha_anula',
        'fecha_cancela'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'cliente_id' => 'integer',
        'serie' => 'string',
        'numero' => 'string',
        'credito' => 'boolean',
        'pagada' => 'boolean',
        'vestado_id' => 'integer',
        'caja_id' => 'integer',
        'user_id' => 'integer',
        'tipo_pago_id' => 'integer',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'cliente_id' => 'required',
//        'monto_ini' => 'numeric',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function cliente()
    {
        return $this->belongsTo(\App\Models\Cliente::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function vestado()
    {
        return $this->belongsTo(\App\Models\Vestado::class);
    }

    public function estado()
    {
        return $this->belongsTo(\App\Models\Vestado::class,'vestado_id','id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function ventaDetalles()
    {
        return $this->hasMany(\App\Models\VentaDetalle::class)->withTrashed();
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function caja()
    {
        return $this->belongsTo(Caja::class);
    }

    public function getTotalAttribute()
    {
        $total= $this->sub_total;

        if ($this->delivery){
            $total+=$this->monto_delivery;
        }

        return $total;
    }

    public function getSubTotalAttribute()
    {
        $total=0;
        foreach ($this->ventaDetalles as $detalle){
            $total+=($detalle->cantidad*$detalle->precio);
        }

        return $total;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     **/
    public function pedidos()
    {
        return $this->belongsToMany(\App\Models\Pedido::class, 'pedido_ventas');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function vpagos()
    {
        return $this->hasMany(\App\Models\Vpago::class);
    }

    public function pagos()
    {
        return $this->hasMany(\App\Models\Vpago::class,'venta_id','id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     **/
    public function compras()
    {
        return $this->belongsToMany(\App\Models\Compra::class, 'compra_venta');
    }

    /**
     * Si la venta tiene egresos de dos o mas lotes diferentes devuelve true
     * @return bool
     */
    public function mustraEgreso()
    {
        $return = false;
        foreach ($this->ventaDetalles as $index => $ventaDetalle) {
            if($ventaDetalle->stocks->count()>1){
                $return = true;
                break;
            }
        }

        return $return;
    }

    public function repartidores()
    {
        return $this->belongsToMany(\App\User::class, 'asiganacion_delivery','venta_id','user_id');
    }

    public function repartidor()
    {
        return $this->repartidores()->first();
    }

    public function historicoEstados()
    {
        return $this->belongsToMany(Vestado::class)->withPivot('tiempo');
    }

    public function detalles()
    {
        return $this->hasMany(VentaDetalle::class,'venta_id','id');
    }

    public function local()
    {
        return $this->belongsTo(Tienda::class,'tienda_id','id');
    }

    //comentado porque da conflicto por el scope que se llama igual
//    public function tienda()
//    {
//        return $this->belongsTo(Tienda::class,'tienda_id','id');
//    }

    public function tipoPago()
    {
        return $this->belongsTo(TipoPago::class);
    }

    /**
     * Devuelve los minutos transcurridos entre dos estados de una vente
     * @param $estadoInicial
     * @param $estadoFinal
     * @return int
     */
    public function transcurido($estadoInicial,$estadoFinal)
    {
        $tiempoInicial = $this->historicoEstados()->where('vestado_id',$estadoInicial)->first()->pivot->tiempo;
        $tiempoFinal = $this->historicoEstados()->where('vestado_id',$estadoFinal)->first()->pivot->tiempo;

        $transcurrido = ceil((strtotime($tiempoFinal) - strtotime($tiempoInicial) ) / 60);

        return $transcurrido;
    }

    /**
     * Devuelve el maximo tiempo promedio de preparación de los artículos o detalles de la venta
     * @return mixed
     */
    public function tiempoDespacho()
    {
        $tiempo = $this->detalles->max(function ($det){
            return nf($det->item->promedioPreparacion($this->local->id),0);
        });

        return $tiempo+2;

    }

    public function getTiempoDespachoAttribute()
    {
        return $this->tiempoDespacho();
    }


    /**
     * Tiempos entre los estados de la venta
     * @return array
     */
    public function getTiemposAttribute()
    {
        $tiempos = [];

        foreach ($this->historicoEstados as $index => $estado) {
            $tiempos = array_add($tiempos,$estado->id,$estado->pivot->tiempo);
        }

        return $tiempos;
    }

    public function getFechaEntregaFAttribute()
    {
        return fecha($this->fecha_entrega);
    }

    public function noItemsCocina()
    {
        return $this->detalles()->cocina()->get()->count() > 0 ? false : true;
    }
}
