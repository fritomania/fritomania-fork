<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Caja
 *
 * @package App\Models
 * @version May 22, 2017, 11:17 am CST
 * @property int $id
 * @property float $monto_apertura
 * @property float $monto_cierre
 * @property string|null $fecha_cierre
 * @property int $user_id
 * @property int $tienda_id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property \Carbon\Carbon|null $deleted_at
 * @property-read mixed $total
 * @property-read mixed $total_efectivo
 * @property-read mixed $total_pagos
 * @property-read mixed $total_retiros
 * @property-read mixed $total_tarjeta
 * @property-read mixed $total_ventas
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Cretiro[] $retiros
 * @property-read \App\User $user
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Venta[] $ventas
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Caja abiertas()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Caja deFecha($fecha)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Caja deTienda($tienda = null)
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Caja hoy()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Caja onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Caja whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Caja whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Caja whereFechaCierre($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Caja whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Caja whereMontoApertura($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Caja whereMontoCierre($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Caja whereTiendaId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Caja whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Caja whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Caja withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Caja withoutTrashed()
 * @mixin \Eloquent
 */
class Caja extends Model
{


    public function scopeHoy($query)
    {
        return $query->whereDate('created_at',hoyDb());
    }

    public function scopeDeFecha($query,$fecha)
    {
        return $query->whereDate('created_at',$fecha);
    }

    public function scopeAbiertas($query)
    {
        return $query->whereNull('fecha_cierre');
    }

    public function scopeDeTienda($query,$tienda=null)
    {
        $tienda = isset($tienda) ? $tienda : session('tienda');

        return $query->where('tienda_id', $tienda);
    }

    use SoftDeletes;

    public $table = 'cajas';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];
    protected $appends = ['monto_cierre','total_pagos','total','total_efectivo','total_tarjeta','total_ventas','total_retiros'];


    public $fillable = [
        'monto_apertura',
        'fecha_cierre',
        'monto_cierre',
        'tienda_id',
        'user_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'user_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'monto_apertura' => 'required|numeric'
    ];

    public function totalVentasTarjeta()
    {
        $total=0;
        $ventas = $this->ventas()->tarjeta()->tienda()->hoy()->procesadas()->noCredito()->noNegocio()->get();

        $total = array_sum(array_pluck($ventas ,'total'));

        return $total;
    }

    public function totalVentasEfectivo()
    {
        $total=0;
        $ventas = $this->ventas()->efectivo()->tienda()->hoy()->procesadas()->noCredito()->noNegocio()->get();

        $total = array_sum(array_pluck($ventas ,'total'));

        return $total;
    }

    public function totalVentas()
    {
        $total=0;
        $ventas = $this->ventas()->tienda()->hoy()->procesadas()->noCredito()->noNegocio()->get();

        $total = array_sum(array_pluck($ventas ,'total'));

        return $total;
    }

    public function ventas()
    {
        return $this->hasMany(Venta::class);
    }

    public function getMontoCierreAttribute()
    {

        $montoCierre = ( $this->monto_apertura + $this->totalVentasEfectivo() ) - $this->totalRetiros();

        return $montoCierre;
    }

    public function getTotalVentasAttribute()
    {
        return $this->totalVentas();
    }

    public function getTotalEfectivoAttribute()
    {
        return $this->totalVentasEfectivo();
    }

    public function getTotalTarjetaAttribute()
    {
        return $this->totalVentasTarjeta();
    }

    public function getTotalAttribute()
    {
        return $this->monto_apertura+$this->totalVentas();
    }

    public function getTotalPagosAttribute()
    {
        return $this->user->totalCobrosHoy();
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function retiros()
    {
        return $this->hasMany(Cretiro::class);
    }

    public function totalRetiros()
    {
        return array_sum(array_pluck($this->retiros,'monto'));
    }

    public function getTotalRetirosAttribute()
    {
        return $this->totalRetiros();
    }
    
}
