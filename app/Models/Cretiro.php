<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Cretiro
 *
 * @package App\Models
 * @version February 28, 2018, 11:05 am CST
 * @property int $id
 * @property int $caja_id
 * @property string $motivo
 * @property string $monto
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property \Carbon\Carbon|null $deleted_at
 * @property-read \App\Models\Caja $caja
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Cretiro onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cretiro whereCajaId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cretiro whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cretiro whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cretiro whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cretiro whereMonto($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cretiro whereMotivo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cretiro whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Cretiro withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Cretiro withoutTrashed()
 * @mixin \Eloquent
 */
class Cretiro extends Model
{
    use SoftDeletes;

    public $table = 'cretiros';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'caja_id',
        'motivo',
        'monto'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'caja_id' => 'integer',
        'motivo' => 'string',
        'monto' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'motivo' => 'required|max:255',
        'monto' => 'required|numeric',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function caja()
    {
        return $this->belongsTo(\App\Models\Caja::class);
    }
}
