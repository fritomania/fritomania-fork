<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Inistock
 *
 * @package App\Models
 * @version February 6, 2018, 11:15 am CST
 * @property int $id
 * @property int $item_id
 * @property int $tienda_id
 * @property float $cantidad
 * @property int $user_id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property \Carbon\Carbon|null $deleted_at
 * @property-read \App\Models\Item $item
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Inistock deTienda($tienda = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Inistock delItem($item)
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Inistock onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Inistock whereCantidad($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Inistock whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Inistock whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Inistock whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Inistock whereItemId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Inistock whereTiendaId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Inistock whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Inistock whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Inistock withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Inistock withoutTrashed()
 * @mixin \Eloquent
 */
class Inistock extends Model
{
    use SoftDeletes;

    public $table = 'inistocks';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'item_id',
        'tienda_id',
        'cantidad',
        'user_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'item_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'item_id' => 'required',
        'tienda_id' => 'required',
        'cantidad' => 'required',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function item()
    {
        return $this->belongsTo(\App\Models\Item::class);
    }

    public function scopeDelItem($query,$item)
    {
        return $query->where('item_id',$item);
    }

    public function scopeDeTienda($query,$tienda=null)
    {
        $tienda = is_null($tienda) ?  session('tienda') : $tienda ;

        return $query->where('tienda_id',$tienda);
    }
}
