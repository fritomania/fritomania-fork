<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class VentaDetalle
 *
 * @package App\Models
 * @version March 27, 2017, 1:11 pm CST
 * @property-read \App\Models\Item $item
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Stock[] $stocks
 * @property-read \App\Models\Venta $venta
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\VentaDetalle onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\VentaDetalle withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\VentaDetalle withoutTrashed()
 * @mixin \Eloquent
 * @property int $id
 * @property int $venta_id
 * @property int $item_id
 * @property float $cantidad
 * @property float $precio
 * @property float $descuento
 * @property string|null $ingredientes
 * @property int|null $num_item
 * @property int $es_detalle_combo
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property \Carbon\Carbon|null $deleted_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VentaDetalle whereCantidad($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VentaDetalle whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VentaDetalle whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VentaDetalle whereDescuento($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VentaDetalle whereEsDetalleCombo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VentaDetalle whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VentaDetalle whereIngredientes($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VentaDetalle whereItemId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VentaDetalle whereNumItem($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VentaDetalle wherePrecio($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VentaDetalle whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VentaDetalle whereVentaId($value)
 */
class VentaDetalle extends Model
{
    use SoftDeletes;

    public $table = 'venta_detalles';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];

    protected $with = ['item'];

    public $fillable = [
        'venta_id',
        'item_id',
        'cantidad',
        'precio',
        'descuento',
        'ingredientes',
        'num_item',
        'es_detalle_combo'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'venta_id' => 'integer',
        'item_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function venta()
    {
        return $this->belongsTo(\App\Models\Venta::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function item()
    {
        return $this->belongsTo(\App\Models\Item::class)->withTrashed();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     **/
    public function stocks()
    {
        return $this->belongsToMany(\App\Models\Stock::class, 'egresos')->withPivot('cantidad');
    }

}
