<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class TempPedidoDetalle
 *
 * @package App\Models
 * @version July 13, 2017, 9:06 am CST
 * @property int $id
 * @property int $temp_pedido_id
 * @property int $item_id
 * @property float $cantidad
 * @property float $precio
 * @property float $descuento
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property \Carbon\Carbon|null $deleted_at
 * @property-read \App\Models\Item $item
 * @property-read \App\Models\TempPedido $tempPedido
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\TempPedidoDetalle onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TempPedidoDetalle whereCantidad($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TempPedidoDetalle whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TempPedidoDetalle whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TempPedidoDetalle whereDescuento($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TempPedidoDetalle whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TempPedidoDetalle whereItemId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TempPedidoDetalle wherePrecio($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TempPedidoDetalle whereTempPedidoId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TempPedidoDetalle whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\TempPedidoDetalle withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\TempPedidoDetalle withoutTrashed()
 * @mixin \Eloquent
 */
class TempPedidoDetalle extends Model
{
    use SoftDeletes;

    public $table = 'temp_pedido_detalles';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'temp_pedido_id',
        'item_id',
        'cantidad',
        'precio',
        'descuento'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'temp_pedido_id' => 'integer',
        'item_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function item()
    {
        return $this->belongsTo(\App\Models\Item::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function tempPedido()
    {
        return $this->belongsTo(\App\Models\TempPedido::class);
    }
}
