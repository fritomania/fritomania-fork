<?php

namespace App\Mail;

use App\Models\Compra;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class OrdenCompraMail extends Mailable
{
    use Queueable, SerializesModels;
    public $compra;
    public $proveedor;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Compra $compra)
    {
        //
        $this->compra = $compra;
        $this->proveedor = $compra->proveedor;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $adrress = mailsSuperAdmins();

        if(env('APP_DEBUG')){
            $adrress[] = config('app.mail_pruebas');
        }

//        if($this->proveedor->correo ){
//            $adrress[] = $this->proveedor->correo;
//        }

        return $this->view('emails.orden_compra')
            ->subject('Orden de Compra')
            ->from(config('app.mail_negocio'),config('app.name'))
            ->to($adrress);
    }
}
