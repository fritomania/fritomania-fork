<?php

namespace App\Mail;

use App\Models\Solicitude;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class DespacharSolicitud extends Mailable
{
    use Queueable, SerializesModels;
    public $solicitud;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Solicitude $solicitud)
    {
        //
        $this->solicitud = $solicitud;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $adrress = ['info@itsb.cl'];

        if(env('APP_DEBUG')){
            $adrress[] = config('app.mail_pruebas');
        }

        $this->view('emails.despachar_solicitud')
            ->subject('Despacho de Solicitud')
            ->from(config('app.mail_negocio'),config('app.name'))
            ->to($adrress);
    }
}
