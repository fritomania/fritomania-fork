<?php

namespace App\Mail;

use App\Models\Produccione;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class AtenderProduccion extends Mailable
{
    use Queueable, SerializesModels;
    public $produccion;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Produccione $produccion)
    {
        //
        $this->produccion = $produccion;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $adrress = ['info@itsb.cl'];

        if(env('APP_DEBUG')){
            $adrress[] = config('app.mail_pruebas');
        }

        $this->view('emails.atender_produccion')
            ->subject('Producción Atendida')
            ->from(config('app.mail_negocio'),config('app.name'))
            ->to($adrress);
    }
}
