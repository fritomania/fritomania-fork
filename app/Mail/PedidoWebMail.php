<?php

namespace App\Mail;

use App\Models\Venta;
use App\Models\Vestado;
use FontLib\Table\Type\name;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class PedidoWebMail extends Mailable
{
    use Queueable, SerializesModels;
    /**
     * @var Venta
     */
    public $pedido;
    /**
     * @var
     */
    public $msg;

    public $subject;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Venta $pedido,$msg)
    {
        //
        $this->pedido = $pedido;
        $this->msg = $msg;

        $this->subject = 'Pedido Web ' . config('app.name');
        if ($pedido->estado->id == Vestado::PAGADA){
            $this->subject .= ' Recibido';
        }

        if ($pedido->estado->id == Vestado::EN_RUTA){
            $this->subject .= ' En Camino';
        }

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {


        if(env('APP_DEBUG')){
            $adrress[] = config('app.mail_pruebas');
        }

        if($this->pedido->correo){
            $adrress[] = $this->pedido->correo;
        }

        return $this->view('emails.pedido_web')
            ->subject($this->subject)
            ->from(config('app.mail_negocio'),config('app.name'))
            ->to($adrress);
    }
}
