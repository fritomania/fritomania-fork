<?php

namespace App\DataTables;

use App\Models\Item;
use App\extensiones\DataTable;
use App\VistaItem;
use Yajra\DataTables\EloquentDataTable;

class ItemDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable
            ->addColumn('action', 'items.datatables_actions',false)
            ->editColumn('imagen',function ($data){
                if($data->imagen)
                    return asset($data->imagen);
                else
                    return asset('img/avatar_none.png');

            })
            ->editColumn('precio_venta',function ($data){
                return nfp($data->precio_venta);

            })
            ->editColumn('precio_compra',function ($data){
                return nfp($data->precio_compra);

            })
            ->editColumn('stock','items.datatable_colum_stock')
            ->rawColumns(['stock','action']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Post $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(VistaItem $model)
    {
        return $model->newQuery();
//            ->leftJoin('iestados', 'items.iestado_id', '=', 'iestados.id')
//            ->leftJoin('unimeds', 'items.unimed_id', '=', 'unimeds.id')
//            ->leftJoin('marcas', 'items.marca_id', '=', 'marcas.id')
//            ->select(
//                'items.*',
//                'marcas.nombre as marca',
//                'unimeds.nombre as um',
//                'iestados.descripcion as iestado'
//            )
//            ->whereNull('items.deleted_at')
//            ->orderBy('nombre');
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '15%','printable' => false, 'title' => 'Acción'])
            ->parameters([
                'dom'     => 'Bfrtip',
                'order'   => [[0, 'desc']],
                'language' => ['url' => asset('js/SpanishDataTables.json')],
                //'scrollX' => false,
                //'responsive' => true,
                'buttons' => [
                    ['extend' => 'create', 'text' => '<i class="fa fa-plus"></i> <span class="d-none d-sm-inline">Crear</span>'],
                    ['extend' => 'print', 'text' => '<i class="fa fa-print"></i> <span class="d-none d-sm-inline">Imprimir</span>'],
                    ['extend' => 'reload', 'text' => '<i class="fa fa-sync-alt"></i> <span class="d-none d-sm-inline">Recargar</span>'],
                    ['extend' => 'reset', 'text' => '<i class="fa fa-undo"></i> <span class="d-none d-sm-inline">Reiniciar</span>'],
                    ['extend' => 'export', 'text' => '<i class="fa fa-download"></i> <span class="d-none d-sm-inline">Exportar</span>'],
                ],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'imagen' => ['name' => 'imagen', 'data' => 'imagen','order' => 'false', 'render' => '"<img src=\""+data+"\" class=\"img-responsive\" alt=\"Image\" width=\"42px\" height=\"42px\"/>"'],
            'nombre' => ['name' => 'nombre', 'data' => 'nombre'],
//            'descripcion' => ['name' => 'descripcion', 'data' => 'descripcion'],
            'precio_v' => ['name' => 'precio_venta', 'data' => 'precio_venta'],
            'precio_c' => ['name' => 'precio_compra', 'data' => 'precio_compra'],
//            'precio_mayoreo' => ['name' => 'precio_mayoreo', 'data' => 'precio_mayoreo'],
            'marca',
            'unidad_medida',
            'stock' => ['name' => 'stock', 'data' => 'stock'],
            'código' => ['name' => 'codigo', 'data' => 'codigo'],
//            'ubicación' => ['name' => 'ubicacion', 'data' => 'ubicacion'],
//            'cantidad_mayoreo' => ['name' => 'cantidad_mayoreo', 'data' => 'cantidad_mayoreo'],
//            'precio_promedio' => ['name' => 'precio_promedio', 'data' => 'precio_promedio'],
//            'inventariable' => ['name' => 'inventariable', 'data' => 'inventariable'],
//            'Estado' => ['name' => 'iestados.descripcion', 'data' => 'iestado']
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'itemsdatatable_' . time();
    }
}