<?php

namespace App\DataTables;

use App\Models\Cestado;
use App\Models\Compra;
use App\Models\VistaCompra;
use App\extensiones\DataTable;
use Auth;
use Carbon\Carbon;
use Yajra\DataTables\EloquentDataTable;

class CompraDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable
            ->addColumn('action', 'compras.datatables_actions')
//            ->orderColumn('id', '-id $1')
            ->setRowClass(function (VistaCompra $compra) {

                $fechaIngresoPlan = Carbon::parse(fechaDb($compra->fecha_ingreso_plan));
                $fechaIngreso = Carbon::parse(fechaDb($compra->fecha_ingreso));

                $fechaCompara = $compra->cestado_id == Cestado::CREADA ? Carbon::now() :  $fechaIngreso;

                $diasRetraso = $fechaIngresoPlan->diffInDays($fechaCompara);


                $diasRetraso = $fechaIngresoPlan < $fechaCompara ? $diasRetraso : 0;

                $alert = '';

                if($diasRetraso > 0 && $compra->cestado_id == Cestado::CREADA ){
                    $alert = 'alert-danger';
                }
                elseif($diasRetraso == 0  && $compra->cestado_id == Cestado::CREADA ){
                    $alert = 'alert-warning';
                };

                return $alert;

            })
            ->editColumn('dias_retraso',function (VistaCompra $compra){

                $fechaIngresoPlan = Carbon::parse(fechaDb($compra->fecha_ingreso_plan));
                $fechaIngreso = Carbon::parse(fechaDb($compra->fecha_ingreso));

                $fechaCompara = $compra->cestado_id == Cestado::CREADA ? Carbon::now() :  $fechaIngreso;

                $diasRetraso = $fechaIngresoPlan->diffInDays($fechaCompara);

                $diasRetraso = $diasRetraso == 0 ? '' : $diasRetraso;

                $diasRetraso = $fechaIngresoPlan < $fechaCompara ? $diasRetraso : '';


                return $diasRetraso;
            });
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Post $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(VistaCompra $model)
    {
        $user = Auth::user();

        //si el usuario es administrador devuelve todos los registros
        if ($user->isAdmin()){
            return $model->newQuery();
        }

        //si el usuario es admin de tienda devuelve todas las compras de la tienda
        if ($user->isAdminTienda()){
            return $model->newQuery()->deTienda();
        }

        //Usuario normal o empleado solo las compras realizadas por el
        else{
            return $model->newQuery()->deTienda()->delUser($user->id);
        }


    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '15%','printable' => false, 'title' => 'Acción'])
            ->parameters([
                'dom'     => 'Bfrtip',
                'order'   => [[0, 'desc']],
                'language' => ['url' => asset('js/SpanishDataTables.json')],
                //'scrollX' => false,
                //'responsive' => true,
                'buttons' => [
                    ['extend' => 'create', 'text' => '<i class="fa fa-plus"></i> <span class="d-none d-sm-inline">Crear</span>'],
                    ['extend' => 'print', 'text' => '<i class="fa fa-print"></i> <span class="d-none d-sm-inline">Imprimir</span>'],
                    ['extend' => 'reload', 'text' => '<i class="fa fa-sync-alt"></i> <span class="d-none d-sm-inline">Recargar</span>'],
                    ['extend' => 'reset', 'text' => '<i class="fa fa-undo"></i> <span class="d-none d-sm-inline">Reiniciar</span>'],
                    ['extend' => 'export', 'text' => '<i class="fa fa-download"></i> <span class="d-none d-sm-inline">Exportar</span>'],
                ],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'id' => ['name' => 'id', 'data' => 'id'],
            'creada' => ['name' => 'creada', 'data' => 'creada'],
//            'hora' => ['name' => 'hora', 'data' => 'hora'],
//            'fecha_documento' => ['name' => 'fecha_documento', 'data' => 'fecha_documento'],
            'fecha_entrega_a_bodega' => ['name' => 'fecha_ingreso_plan', 'data' => 'fecha_ingreso_plan'],
            'dias_retraso',
            'fecha_recepción' => ['name' => 'fecha_ingreso', 'data' => 'fecha_ingreso'],
//            'fecha_pago' => ['name' => 'fecha_credito', 'data' => 'fecha_credito'],
            'tipo' => ['name' => 'tipo', 'data' => 'tipo'],
            'proveedor' => ['name' => 'proveedor', 'data' => 'proveedor'],
            'S/N' => ['name' => 'ns', 'data' => 'ns'],
            'estado' => ['name' => 'estado', 'data' => 'estado'],
//            'usuario' => ['name' => 'usuario', 'data' => 'usuario', ]
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'reporte_compras_' . time();
    }
}