<?php

namespace App\DataTables;

use App\Models\Venta;
use App\Models\VistaVenta;
use Illuminate\Support\Facades\Auth;
use App\extensiones\DataTable;
use Yajra\DataTables\EloquentDataTable;

class VentaDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable->addColumn('action', 'ventas.datatables_actions');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Post $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(VistaVenta $model)
    {
        $user = Auth::user();


        //Usuario normal o empleado solo las compras realizadas por el
        if (Auth::user()->esSoloEmpleado()){
            return $model->newQuery()->deTienda()->delUser($user->id);
        }

        return $model->newQuery();

    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '15%','printable' => false, 'title' => 'Acción'])
            ->parameters([
                'dom'     => 'Bfrtip',
                'order'   => [[0, 'desc']],
                'language' => ['url' => asset('js/SpanishDataTables.json')],
                //'scrollX' => false,
                //'responsive' => true,
                'buttons' => [
//                    ['extend' => 'create', 'text' => '<i class="fa fa-plus"></i> <span class="d-none d-sm-inline">Crear</span>'],
                    ['extend' => 'print', 'text' => '<i class="fa fa-print"></i> <span class="d-none d-sm-inline">Imprimir</span>'],
                    ['extend' => 'reload', 'text' => '<i class="fa fa-sync-alt"></i> <span class="d-none d-sm-inline">Recargar</span>'],
                    ['extend' => 'reset', 'text' => '<i class="fa fa-undo"></i> <span class="d-none d-sm-inline">Reiniciar</span>'],
                    ['extend' => 'export', 'text' => '<i class="fa fa-download"></i> <span class="d-none d-sm-inline">Exportar</span>'],
                ],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'id' => ['name' => 'id', 'data' => 'id'],
            'cliente' => ['name' => 'cliente', 'data' => 'cliente'],
            'fecha' => ['name' => 'fecha', 'data' => 'fecha'],
            'hora' => ['name' => 'hora', 'data' => 'hora'],
//            'S/N' => ['name' => 'ns', 'data' => 'ns'],
            'estado' => ['name' => 'estado', 'data' => 'estado'],
            'tipo_pago' => ['name' => 'tipo_pago', 'data' => 'tipo_pago'],
            'usuario' => ['name' => 'usuario', 'data' => 'usuario'],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'ventasdatatable_' . time();
    }
}