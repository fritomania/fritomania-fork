<?php

namespace App\DataTables;

use App\Models\Caja;
use App\extensiones\DataTable;
use Yajra\DataTables\EloquentDataTable;

class CajaDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable->addColumn('action', 'cajas.datatables_actions')
            ->orderColumn('cajas.id', '-cajas.id $1');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Post $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Caja $model)
    {
        return $model->newQuery()
            ->join('users','users.id','=','cajas.user_id')
            ->select(
                'cajas.*',
                'users.name as username'
            )->where('cajas.tienda_id',session('tienda'));
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '15%','printable' => false, 'title' => 'Acción'])
            ->parameters([
                'dom'     => 'Bfrtip',
                'order'   => [[0, 'desc']],
                'language' => ['url' => asset('js/SpanishDataTables.json')],
                //'scrollX' => false,
                //'responsive' => true,
                'buttons' => [
                    ['extend' => 'create', 'text' => '<i class="fa fa-plus"></i> <span class="d-none d-sm-inline">Crear</span>'],
                    ['extend' => 'print', 'text' => '<i class="fa fa-print"></i> <span class="d-none d-sm-inline">Imprimir</span>'],
                    ['extend' => 'reload', 'text' => '<i class="fa fa-sync-alt"></i> <span class="d-none d-sm-inline">Recargar</span>'],
                    ['extend' => 'reset', 'text' => '<i class="fa fa-undo"></i> <span class="d-none d-sm-inline">Reiniciar</span>'],
                    ['extend' => 'export', 'text' => '<i class="fa fa-download"></i> <span class="d-none d-sm-inline">Exportar</span>'],
                ],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'id' => ['name' => 'cajas.id', 'data' => 'id'],
            'usuario' => ['name' => 'users.name', 'data' => 'username'],
            'fecha_apertura' => ['name' => 'created_at', 'data' => 'created_at'],
            'monto_apertura' => ['name' => 'monto_apertura', 'data' => 'monto_apertura'],
            'fecha_cierre' => ['name' => 'fecha_cierre', 'data' => 'fecha_cierre'],
            'monto_cierre' => ['name' => 'monto_cierre', 'data' => 'monto_cierre'],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'cajasdatatable_' . time();
    }
}