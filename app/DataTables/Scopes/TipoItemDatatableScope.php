<?php

namespace App\DataTables\Scopes;

use Yajra\DataTables\Contracts\DataTableScope;

class TipoItemDatatableScope implements DataTableScope
{
    /**
     * @var
     */
    private $tipo;

    /**
     * TipoItemDatatableScope constructor.
     */
    public function __construct($tipo)
    {
        $this->tipo = $tipo;
    }

    /**
     * Apply a query scope.
     *
     * @param \Illuminate\Database\Query\Builder|\Illuminate\Database\Eloquent\Builder $query
     * @return mixed
     */
    public function apply($query)
    {

        if ($this->tipo=='mp'){
            $query->where('materia_prima',1);
        }
        if ($this->tipo=='pf'){
            $query->where('materia_prima',0);
        }

        return $query;

    }
}
