<?php

namespace App\DataTables\Scopes;

use Yajra\DataTables\Contracts\DataTableScope;

class ScopeItemVentaDataTable implements DataTableScope
{
    /**
     * @var
     */
    private $item;

    /**
     * ScopeItemVentaDataTable constructor.
     */
    public function __construct($item)
    {
        $this->item = $item;
    }

    /**
     * Apply a query scope.
     *
     * @param \Illuminate\Database\Query\Builder|\Illuminate\Database\Eloquent\Builder $query
     * @return mixed
     */
    public function apply($query)
    {
        return $query->whereIn('id', function($q) {
            $q->select('venta_id')->from('venta_detalles')->where('item_id',$this->item);
        });
    }
}