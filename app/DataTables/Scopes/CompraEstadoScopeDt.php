<?php

namespace App\DataTables\Scopes;

use Yajra\DataTables\Contracts\DataTableScope;

class CompraEstadoScopeDt implements DataTableScope
{
    private $estado;

    /**
     * CompraEstadoScopeDt constructor.
     */
    public function __construct($estado)
    {
        $this->estado = $estado;
    }


    /**
     * Apply a query scope.
     *
     * @param \Illuminate\Database\Query\Builder|\Illuminate\Database\Eloquent\Builder $query
     * @return mixed
     */
    public function apply($query)
    {
         return $query->where('cestado_id', $this->estado);
    }
}
