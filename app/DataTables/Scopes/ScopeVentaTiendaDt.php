<?php

namespace App\DataTables\Scopes;

use Yajra\DataTables\Contracts\DataTableScope;

class ScopeVentaTiendaDt implements DataTableScope
{

    private $tienda;

    /**
     * ScopeVentaTiendaDt constructor.
     * @param $tienda
     */
    public function __construct($tienda)
    {
        $this->tienda = $tienda;
    }


    /**
     * Apply a query scope.
     *
     * @param \Illuminate\Database\Query\Builder|\Illuminate\Database\Eloquent\Builder $query
     * @return mixed
     */
    public function apply($query)
    {
         return $query->where('tienda_id', $this->tienda);
    }
}
