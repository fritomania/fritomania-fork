<?php

namespace App\DataTables\Scopes;

use Yajra\DataTables\Contracts\DataTableScope;

class VentaEstadoScoopeDt implements DataTableScope
{
    private $estado;

    /**
     * VentaEstadoScoopeDt constructor.
     */
    public function __construct($estado)
    {
        $this->estado = $estado;
    }


    /**
     * Apply a query scope.
     *
     * @param \Illuminate\Database\Query\Builder|\Illuminate\Database\Eloquent\Builder $query
     * @return mixed
     */
    public function apply($query)
    {
         return $query->where('vestado_id', $this->estado);
    }
}
