<?php

namespace App\DataTables\Scopes;

use Yajra\DataTables\Contracts\DataTableScope;

class ScopeProveedorCompraDataTable implements DataTableScope
{
    /**
     * @var
     */
    private $proveedor;

    /**
     * ScopeProveedorCompraDataTable constructor.
     */
    public function __construct($proveedor)
    {

        $this->proveedor = $proveedor;
    }

    /**
     * Apply a query scope.
     *
     * @param \Illuminate\Database\Query\Builder|\Illuminate\Database\Eloquent\Builder $query
     * @return mixed
     */
    public function apply($query)
    {
        return $query->where('proveedor_id', $this->proveedor);
    }
}
