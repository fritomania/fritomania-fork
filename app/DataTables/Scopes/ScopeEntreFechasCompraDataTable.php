<?php

namespace App\DataTables\Scopes;

use Carbon\Carbon;
use Yajra\DataTables\Contracts\DataTableScope;

class ScopeEntreFechasCompraDataTable implements DataTableScope
{
    /**
     * @var
     */
    private $del;
    /**
     * @var
     */
    private $al;
    private $between;

    /**
     * ScopeEntreFechasCompraDataTable constructor.
     */
    public function __construct($del,$al)
    {
        $this->del = Carbon::parse($del);
        $this->al = Carbon::parse($al)->addHour(12);


        if($this->del && $this->al){

            $this->between =  [$this->del,$this->al];

        }
        else{
            $this->between = mesActualBetween();
        }

    }

    /**
     * Apply a query scope.
     *
     * @param \Illuminate\Database\Query\Builder|\Illuminate\Database\Eloquent\Builder $query
     * @return mixed
     */
    public function apply($query)
    {


        return $query->whereBetween('created_at',$this->between);
    }
}
