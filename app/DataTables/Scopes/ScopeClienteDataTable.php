<?php

namespace App\DataTables\Scopes;

use Yajra\DataTables\Contracts\DataTableScope;

class ScopeClienteDataTable implements DataTableScope
{
    /**
     * @var
     */
    private $cliente;


    /**
     * ScopeClienteDataTable constructor.
     */
    public function __construct($cliente)
    {
        $this->cliente = $cliente;
    }

    /**
     * Apply a query scope.
     *
     * @param \Illuminate\Database\Query\Builder|\Illuminate\Database\Eloquent\Builder $query
     * @return mixed
     */
    public function apply($query)
    {
        return $query->where('cliente_id', $this->cliente);
    }
}
