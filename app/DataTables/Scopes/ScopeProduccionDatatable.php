<?php

namespace App\DataTables\Scopes;

use Yajra\DataTables\Contracts\DataTableScope;

class ScopeProduccionDatatable implements DataTableScope
{
    /**
     * @var array
     */
    private $estados;

    /**
     * ScopeProduccionDatatable constructor.
     */
    public function __construct($estados=[])
    {

        $this->estados = $estados;
    }


    /**
     * Apply a query scope.
     *
     * @param \Illuminate\Database\Query\Builder|\Illuminate\Database\Eloquent\Builder $query
     * @return mixed
     */
    public function apply($query)
    {
         return $query->whereIn('produccion_estado_id', $this->estados);
    }
}
