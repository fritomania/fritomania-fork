<?php

namespace App\DataTables;

use App\Models\Cpu;
use Form;
use Yajra\Datatables\Services\DataTable;

class CpuDataTable extends DataTable
{

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajax()
    {
        return $this->datatables
            ->eloquent($this->query())
            ->addColumn('action', 'cpus.datatables_actions')
            ->make(true);
    }

    /**
     * Get the query object to be processed by datatables.
     *
     * @return \Illuminate\Database\Query\Builder|\Illuminate\Database\Eloquent\Builder
     */
    public function query()
    {
        $cpus = Cpu::query()
            ->join('unimeds', 'cpus.unimed_id', '=', 'unimeds.id')
            ->join('etipos', 'cpus.etipo_id', '=', 'etipos.id')
            ->select(
                'cpus.*',
                'etipos.nombre as tipo',
                'unimeds.nombre as unimed'
            );

        return $this->applyScopes($cpus);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->addAction(['width' => '10%', 'title' => 'Acción'])
            ->ajax('')
            ->parameters([
                'dom' => 'Bfrtip',
                'scrollX' => false,
                'buttons' => [
                    'print',
                    'reset',
                    'reload',
                    [
                         'extend'  => 'collection',
                         'text'    => '<i class="fa fa-download"></i> Export',
                         'buttons' => [
                             'csv',
                             'excel',
                             'pdf',
                         ],
                    ],
                    'colvis'
                ]
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    private function getColumns()
    {
        return [
            'tipo' => ['name' => 'etipos.nombre', 'data' => 'tipo'],
            'nombre' => ['name' => 'nombre', 'data' => 'nombre'],
            'cache' => ['name' => 'cache', 'data' => 'cache'],
            'nucleos' => ['name' => 'nucleos', 'data' => 'nucleos'],
            'subprocesos' => ['name' => 'subprocesos', 'data' => 'subprocesos'],
            'frecuencia_basica' => ['name' => 'frecuencia_basica', 'data' => 'frecuencia_basica'],
            'frecuencia_maxima' => ['name' => 'frecuencia_maxima', 'data' => 'frecuencia_maxima'],
            'unimed' => ['name' => 'unimeds.nombre', 'data' => 'unimed']
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'cpus';
    }
}
