<?php

namespace App\DataTables;

use App\Models\Empleado;
use App\extensiones\DataTable;
use Yajra\DataTables\EloquentDataTable;

class EmpleadoDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable->addColumn('action', 'empleados.datatables_actions');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Post $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Empleado $model)
    {
        return $model->newQuery()
            ->join('tiendas','empleados.tienda_id','=','tiendas.id')
            ->select(
                'empleados.*',
                'tiendas.nombre as tienda'
            );
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '15%','printable' => false, 'title' => 'Acción'])
            ->parameters([
                'dom'     => 'Bfrtip',
                'order'   => [[0, 'desc']],
                'language' => ['url' => asset('js/SpanishDataTables.json')],
                //'scrollX' => false,
                //'responsive' => true,
                'buttons' => [
                    ['extend' => 'create', 'text' => '<i class="fa fa-plus"></i> <span class="d-none d-sm-inline">Crear</span>'],
                    ['extend' => 'print', 'text' => '<i class="fa fa-print"></i> <span class="d-none d-sm-inline">Imprimir</span>'],
                    ['extend' => 'reload', 'text' => '<i class="fa fa-sync-alt"></i> <span class="d-none d-sm-inline">Recargar</span>'],
                    ['extend' => 'reset', 'text' => '<i class="fa fa-undo"></i> <span class="d-none d-sm-inline">Reiniciar</span>'],
                    ['extend' => 'export', 'text' => '<i class="fa fa-download"></i> <span class="d-none d-sm-inline">Exportar</span>'],
                ],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'Tienda' => ['name' => 'tiendas.nombre', 'data' => 'tienda'],
            'nombres' => ['name' => 'nombres', 'data' => 'nombres'],
            'apellidos' => ['name' => 'apellidos', 'data' => 'apellidos'],
            'telefono' => ['name' => 'telefono', 'data' => 'telefono'],
            'correo' => ['name' => 'correo', 'data' => 'correo'],
            'sueldo_diario' => ['name' => 'sueldo_diario', 'data' => 'sueldo_diario'],
            '% comision' => ['name' => 'porcentaje_comision', 'data' => 'porcentaje_comision'],
//            'genero' => ['name' => 'genero', 'data' => 'genero'],
            'puesto' => ['name' => 'puesto', 'data' => 'puesto'],
//            'fecha_contratacion' => ['name' => 'fecha_contratacion', 'data' => 'fecha_contratacion'],
//            'direccion' => ['name' => 'direccion', 'data' => 'direccion'],
//            'fecha_nacimiento' => ['name' => 'fecha_nacimiento', 'data' => 'fecha_nacimiento']
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'empleadosdatatable_' . time();
    }
}