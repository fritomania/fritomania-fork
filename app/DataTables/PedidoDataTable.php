<?php

namespace App\DataTables;

use App\Models\Pedido;
use App\Models\VistaPedido;
use Form;
use App\extensiones\DataTable;

class PedidoDataTable extends DataTable
{

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajax()
    {
        return $this->datatables
            ->eloquent($this->query())
            ->addColumn('action', 'pedidos.datatables_actions')
            ->orderColumn('id', '-id $1')
            ->make(true);
    }

    /**
     * Get the query object to be processed by datatables.
     *
     * @return \Illuminate\Database\Query\Builder|\Illuminate\Database\Eloquent\Builder
     */
    public function query()
    {
        $pedidos = VistaPedido::query();

        return $this->applyScopes($pedidos);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->addAction(['width' => '10%', 'title' => 'Acción'])
            ->ajax('')
            ->parameters([
                'dom' => 'Bfrtip',
                'scrollX' => false,
                'buttons' => [
                    'print',
                    'reset',
                    'reload',
                    [
                         'extend'  => 'collection',
                         'text'    => '<i class="fa fa-download"></i> Export',
                         'buttons' => [
                             'csv',
                             'excel',
                             'pdf',
                         ],
                    ],
                    'colvis'
                ]
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    private function getColumns()
    {
        return [
            'id' => ['name' => 'id', 'data' => 'id'],
            'numero' => ['name' => 'numero', 'data' => 'numero'],
            'cliente' => ['name' => 'cliente', 'data' => 'cliente'],
            'fecha_ingreso' => ['name' => 'fecha_ingreso', 'data' => 'fecha_ingreso'],
            'fecha_entrega' => ['name' => 'fecha_entrega', 'data' => 'fecha_entrega'],
            'usuario' => ['name' => 'usuario', 'data' => 'usuario'],
            'estado' => ['name' => 'estado', 'data' => 'estado']
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'pedidos';
    }
}
