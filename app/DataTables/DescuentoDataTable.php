<?php

namespace App\DataTables;

use App\Models\Descuento;
use Form;
use App\extensiones\DataTable;

class DescuentoDataTable extends DataTable
{

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajax()
    {
        return $this->datatables
            ->eloquent($this->query())
            ->addColumn('action', 'descuentos.datatables_actions')
            ->make(true);
    }

    /**
     * Get the query object to be processed by datatables.
     *
     * @return \Illuminate\Database\Query\Builder|\Illuminate\Database\Eloquent\Builder
     */
    public function query()
    {
        $descuentos = Descuento::query()
            ->join('items','items.id','=','descuentos.item_id')
            ->select(
                'items.nombre as item',
                'descuentos.*'
            );

        return $this->applyScopes($descuentos);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->addAction(['width' => '10%', 'title' => 'Acción'])
            ->ajax('')
            ->parameters([
                'dom' => 'Bfrtip',
                'scrollX' => false,
                'buttons' => [
                    'print',
                    'reset',
                    'reload',
                    [
                         'extend'  => 'collection',
                         'text'    => '<i class="fa fa-download"></i> Export',
                         'buttons' => [
                             'csv',
                             'excel',
                             'pdf',
                         ],
                    ],
                    'colvis'
                ]
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    private function getColumns()
    {
        return [
            'item' => ['name' => 'items.nombre', 'data' => 'item'],
            'porcentaje' => ['name' => 'porcentaje', 'data' => 'porcentaje'],
            'fecha_inicio' => ['name' => 'fecha_inicio', 'data' => 'fecha_inicio'],
            'fecha_expira' => ['name' => 'fecha_expira', 'data' => 'fecha_expira']
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'descuentos';
    }
}
