<?php

namespace App\DataTables;

use App\Models\Cuadre;
use App\extensiones\DataTable;
use App\VistaCuadre;
use Yajra\DataTables\EloquentDataTable;

class CuadreDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable
            ->addColumn('action', 'cuadres.datatables_actions')
            ->editColumn('total_sistema', function($data) {
                return dvs().' '.nfp($data->total_sistema);
            })
            ->editColumn('cash', function($data) {
                return dvs().' '.nfp($data->cash);
            })
            ->editColumn('diferencia', function($data) {
                return dvs().' '.nfp($data->cash-$data->total_sistema);
            });
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Post $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(VistaCuadre $model)
    {
        return $model->newQuery()->deTienda();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '15%','printable' => false, 'title' => 'Acción'])
            ->parameters([
                'dom'     => 'Bfrtip',
                'order'   => [[0, 'desc']],
                'language' => ['url' => asset('js/SpanishDataTables.json')],
                //'scrollX' => false,
                //'responsive' => true,
                'buttons' => [
//                    ['extend' => 'create', 'text' => '<i class="fa fa-plus"></i> <span class="d-none d-sm-inline">Crear</span>'],
                    ['extend' => 'print', 'text' => '<i class="fa fa-print"></i> <span class="d-none d-sm-inline">Imprimir</span>'],
                    ['extend' => 'reload', 'text' => '<i class="fa fa-sync-alt"></i> <span class="d-none d-sm-inline">Recargar</span>'],
                    ['extend' => 'reset', 'text' => '<i class="fa fa-undo"></i> <span class="d-none d-sm-inline">Reiniciar</span>'],
                    ['extend' => 'export', 'text' => '<i class="fa fa-download"></i> <span class="d-none d-sm-inline">Exportar</span>'],
                ],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'fecha' => ['name' => 'fecha_format', 'data' => 'fecha_format'],
            'total_sistema' => ['name' => 'total_sistema', 'data' => 'total_sistema'],
            'efectivo' => ['name' => 'cash', 'data' => 'cash'],
            'diferencia' => ['name' => 'diferencia', 'data' => 'diferencia'],
            'usuario' => ['name' => 'usuario', 'data' => 'usuario']
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'cuadresdatatable_' . time();
    }
}