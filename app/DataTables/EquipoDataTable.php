<?php

namespace App\DataTables;

use App\Models\Equipo;
use Form;
use Yajra\Datatables\Services\DataTable;

class EquipoDataTable extends DataTable
{

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajax()
    {
        return $this->datatables
            ->eloquent($this->query())
            ->addColumn('action', 'equipos.datatables_actions')
            ->make(true);
    }

    /**
     * Get the query object to be processed by datatables.
     *
     * @return \Illuminate\Database\Query\Builder|\Illuminate\Database\Eloquent\Builder
     */
    public function query()
    {
        $equipos = Equipo::query()
            ->join('emarcas', 'equipos.emarca_id', '=', 'emarcas.id')
            ->join('modelos', 'equipos.modelo_id', '=', 'modelos.id')
            ->join('etipos', 'equipos.etipo_id', '=', 'etipos.id')
            ->select(
                'equipos.*',
                'etipos.nombre as tipo',
                'modelos.nombre as model',
                'emarcas.nombre as marca'
            );

        return $this->applyScopes($equipos);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->addAction(['width' => '10%', 'title' => 'Acción'])
            ->ajax('')
            ->parameters([
                'dom' => 'Bfrtip',
                'scrollX' => false,
                'responsive' => true,
                'buttons' => [
                    'print',
                    'reset',
                    'reload',
                    [
                         'extend'  => 'collection',
                         'text'    => '<i class="fa fa-download"></i> Export',
                         'buttons' => [
                             'csv',
                             'excel',
                             'pdf',
                         ],
                    ],
                    'colvis'
                ]
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    private function getColumns()
    {
        return [
            'tipo' => ['name' => 'etipos.nombre', 'data' => 'tipo'],
            'marca' => ['name' => 'emarcas.nombre', 'data' => 'marca'],
            'modelo' => ['name' => 'modeloss.nombre', 'data' => 'model'],
            'numero_serie' => ['name' => 'numero_serie', 'data' => 'numero_serie'],
            'imei' => ['name' => 'imei', 'data' => 'imei']
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'equipos';
    }
}
