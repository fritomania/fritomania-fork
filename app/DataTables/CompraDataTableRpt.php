<?php

namespace App\DataTables;

use App\Models\Compra;
use App\Models\VistaCompra;
use App\extensiones\DataTable;
use Yajra\DataTables\EloquentDataTable;

class CompraDataTableRpt extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable
//            ->addColumn('action', 'compras.datatables_actions')
//            ->orderColumn('id', '-id $1')
            ->setRowClass(function ($data) {

                if(hoyDb()>fechaDb($data->fecha_ingreso_plan) && $data->estado == 'CREADA' ){
                    $alert = 'alert-danger';
                }
                elseif(hoyDb() == fechaDb($data->fecha_ingreso_plan ) && $data->estado == 'CREADA' ){
                    $alert = 'alert-warning';
                }else{
                    $alert = '';
                };

                return $alert;

            });
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Post $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(VistaCompra $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
//            ->addAction(['width' => '15%','printable' => false, 'title' => 'Acción'])
            ->parameters([
                'dom'     => 'Bfrtip',
                'order'   => [[0, 'desc']],
                'language' => ['url' => asset('js/SpanishDataTables.json')],
                //'scrollX' => false,
                //'responsive' => true,
                'buttons' => [
                    ['extend' => 'create', 'text' => '<i class="fa fa-plus"></i> <span class="d-none d-sm-inline">Crear</span>'],
                    ['extend' => 'print', 'text' => '<i class="fa fa-print"></i> <span class="d-none d-sm-inline">Imprimir</span>'],
                    ['extend' => 'reload', 'text' => '<i class="fa fa-sync-alt"></i> <span class="d-none d-sm-inline">Recargar</span>'],
                    ['extend' => 'reset', 'text' => '<i class="fa fa-undo"></i> <span class="d-none d-sm-inline">Reiniciar</span>'],
                    ['extend' => 'export', 'text' => '<i class="fa fa-download"></i> <span class="d-none d-sm-inline">Exportar</span>'],
                ],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'id' => ['name' => 'id', 'data' => 'id'],
            'creada' => ['name' => 'creada', 'data' => 'creada'],
//            'hora' => ['name' => 'hora', 'data' => 'hora'],
//            'fecha_documento' => ['name' => 'fecha_documento', 'data' => 'fecha_documento'],
            'fecha_entrega_a_bodega' => ['name' => 'fecha_ingreso_plan', 'data' => 'fecha_ingreso_plan'],
            'fecha_recepción' => ['name' => 'fecha_ingreso', 'data' => 'fecha_ingreso'],
//            'fecha_pago' => ['name' => 'fecha_credito', 'data' => 'fecha_credito'],
            'tipo' => ['name' => 'tipo', 'data' => 'tipo'],
            'proveedor' => ['name' => 'proveedor', 'data' => 'proveedor'],
            'S/N' => ['name' => 'ns', 'data' => 'ns'],
            'estado' => ['name' => 'estado', 'data' => 'estado'],
//            'usuario' => ['name' => 'usuario', 'data' => 'usuario', ]
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'reporte_compras_' . time();
    }
}