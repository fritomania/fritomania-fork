<?php

namespace App\DataTables;

use App\Models\Compra;
use App\Models\VistaCompra;
use App\extensiones\DataTable;
use Yajra\DataTables\EloquentDataTable;

class CompraPagarDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable
            ->addColumn('action', 'compras.datatables_actions_por_pagar');
//            ->orderColumn('id', '-id $1');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Post $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(VistaCompra $model)
    {
        return $model->newQuery()->porCobrar()->procesadas();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '15%','printable' => false, 'title' => 'Acción'])
            ->parameters([
                'dom'     => 'Bfrtip',
                'order'   => [[0, 'desc']],
                'language' => ['url' => asset('js/SpanishDataTables.json')],
                //'scrollX' => false,
                //'responsive' => true,
                'buttons' => [
//                    ['extend' => 'create', 'text' => '<i class="fa fa-plus"></i> <span class="d-none d-sm-inline">Crear</span>'],
                    ['extend' => 'print', 'text' => '<i class="fa fa-print"></i> <span class="d-none d-sm-inline">Imprimir</span>'],
                    ['extend' => 'reload', 'text' => '<i class="fa fa-sync-alt"></i> <span class="d-none d-sm-inline">Recargar</span>'],
                    ['extend' => 'reset', 'text' => '<i class="fa fa-undo"></i> <span class="d-none d-sm-inline">Reiniciar</span>'],
                    ['extend' => 'export', 'text' => '<i class="fa fa-download"></i> <span class="d-none d-sm-inline">Exportar</span>'],
                ],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'id' => ['name' => 'id', 'data' => 'id'],
            'fecha' => ['name' => 'creada', 'data' => 'creada'],
            'hora' => ['name' => 'hora', 'data' => 'hora'],
            'tipo' => ['name' => 'tipo', 'data' => 'tipo'],
            'fecha_limite_credito' => ['name' => 'fecha_credito', 'data' => 'fecha_credito'],
            'proveedor' => ['name' => 'proveedor', 'data' => 'proveedor'],
            'S/N' => ['name' => 'ns', 'data' => 'ns'],
//            'estado' => ['name' => 'estado', 'data' => 'estado'],
            'usuario' => ['name' => 'usuario', 'data' => 'usuario'],
            'saldo' => ['name' => 'saldo', 'data' => 'saldo']
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'comprasdatatable_' . time();
    }
}