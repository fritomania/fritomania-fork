<?php

namespace App\DataTables;

use App\Models\Item;
use App\Models\Tienda;
use App\VistaItem;
use Auth;
use App\extensiones\DataTable;
use Yajra\DataTables\EloquentDataTable;

class ItemStockTiendaDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);


        return $dataTable
//            ->addColumn('action', 'items.datatables_actions',false)
            ->editColumn('imagen',function ($data){
                if($data->imagen)
                    return asset($data->imagen);
                else
                    return asset('img/avatar_none.png');

            })
            ->editColumn('stock',function ($item){
                $tienda = Auth::user()->tienda->id ;
                return nf($item->stockTienda($tienda));
            })
            ->editColumn('stock_cocina',function ($item){
                return nf($item->stockTienda(Tienda::COCINA_PRODUCCION));
            });
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Post $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(VistaItem $model)
    {
        $tienda = Auth::user()->tienda->id ;
        return $model->newQuery()->enTienda($tienda);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
//            ->addAction(['width' => '15%','printable' => false, 'title' => 'Acción'])
            ->parameters([
                'dom'     => 'Bfrtip',
                'order'   => [[0, 'desc']],
                'language' => ['url' => asset('js/SpanishDataTables.json')],
                //'scrollX' => false,
                //'responsive' => true,
                'buttons' => [
//                    ['extend' => 'create', 'text' => '<i class="fa fa-plus"></i> <span class="d-none d-sm-inline">Crear</span>'],
                    ['extend' => 'print', 'text' => '<i class="fa fa-print"></i> <span class="d-none d-sm-inline">Imprimir</span>'],
                    ['extend' => 'reload', 'text' => '<i class="fa fa-sync-alt"></i> <span class="d-none d-sm-inline">Recargar</span>'],
                    ['extend' => 'reset', 'text' => '<i class="fa fa-undo"></i> <span class="d-none d-sm-inline">Reiniciar</span>'],
                    ['extend' => 'export', 'text' => '<i class="fa fa-download"></i> <span class="d-none d-sm-inline">Exportar</span>'],
                ],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        $colums= [
            'imagen' => ['name' => 'imagen', 'data' => 'imagen','order' => 'false', 'render' => '"<img src=\""+data+"\" class=\"img-responsive\" alt=\"Image\" width=\"42px\" height=\"42px\"/>"'],
            'código' => ['name' => 'codigo', 'data' => 'codigo'],
            'nombre' => ['name' => 'nombre', 'data' => 'nombre'],
            'marca' => ['name' => 'marca', 'data' => 'marca'],
            'U/M' => ['name' => 'unidad_medida', 'data' => 'unidad_medida'],
            'ubicación' => ['name' => 'ubicacion', 'data' => 'ubicacion'],
            'stock' => ['name' => 'stock', 'data' => 'stock'],
        ];

        if (session('tienda')==Tienda::BODEGA_PRODUCCION){
            $colums = array_add($colums,'stock_cocina',['name' => 'stock_cocina', 'data' => 'stock_cocina', 'orderable' => false, 'searchable' => false]);
        }

        return $colums;
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'itemsdatatable_' . time();
    }
}