<?php

namespace App\DataTables;

use App\Models\Marcaje;
use App\extensiones\DataTable;
use Yajra\DataTables\EloquentDataTable;

class MarcajeDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable->addColumn('action', 'marcajes.datatables_actions')
            ->editColumn('fecha',function ($row){
                return fecha($row->fecha);
            });
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Post $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Marcaje $model)
    {
        return $model->newQuery()
            ->join('tipo_marcajes','marcajes.tipo_marcaje_id','=','tipo_marcajes.id')
            ->join('users','marcajes.user_id','=','users.id')
            ->select('marcajes.*','tipo_marcajes.nombre as tipo_marcaje','users.name as usuario')
            ->orderBy('marcajes.fecha','desc')
            ->orderBy('marcajes.user_id');
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '15%','printable' => false])
            ->parameters([
                'dom'     => 'Bfrtip',
                'order'   => [[0, 'desc']],
                'language' => ['url' => asset('js/SpanishDataTables.json')],
                //'scrollX' => false,
                //'responsive' => true,
                'buttons' => [
                    ['extend' => 'create', 'text' => '<i class="fa fa-plus"></i> <span class="d-none d-sm-inline">Crear</span>'],
                    ['extend' => 'print', 'text' => '<i class="fa fa-print"></i> <span class="d-none d-sm-inline">Imprimir</span>'],
                    ['extend' => 'reload', 'text' => '<i class="fas fa-sync-alt"></i> <span class="d-none d-sm-inline">Recargar</span>'],
                    ['extend' => 'reset', 'text' => '<i class="fa fa-undo"></i> <span class="d-none d-sm-inline">Reiniciar</span>'],
                    ['extend' => 'export', 'text' => '<i class="fa fa-download"></i> <span class="d-none d-sm-inline">Exportar</span>'],
                ],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
//            'fecha_registro' => ['name' => 'created_at', 'data' => 'created_at'],
            'fecha',
            'hora',
            'tipo_marcaje' => ['data' => 'tipo_marcaje', 'name' => 'tipo_marcajes.nombre'],
            'usuario' => ['data' => 'usuario', 'name' => 'users.name'],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'marcajesdatatable_' . time();
    }
}