<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\VistaProduccione
 *
 * @property int $id
 * @property string|null $numero
 * @property int $produccion_tipo_id
 * @property int $produccion_estado_id
 * @property int $user_id
 * @property string|null $fecha_cancela
 * @property string|null $fecha_anula
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @property string $tipo
 * @property string $estado
 * @property string $usuario
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\VistaProduccione onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VistaProduccione whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VistaProduccione whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VistaProduccione whereEstado($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VistaProduccione whereFechaAnula($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VistaProduccione whereFechaCancela($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VistaProduccione whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VistaProduccione whereNumero($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VistaProduccione whereProduccionEstadoId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VistaProduccione whereProduccionTipoId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VistaProduccione whereTipo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VistaProduccione whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VistaProduccione whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VistaProduccione whereUsuario($value)
 * @method static \Illuminate\Database\Query\Builder|\App\VistaProduccione withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\VistaProduccione withoutTrashed()
 * @mixin \Eloquent
 */
class VistaProduccione extends Model
{
    use SoftDeletes;
}
