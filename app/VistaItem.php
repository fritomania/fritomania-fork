<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\VistaItem
 *
 * @property int $id
 * @property string $nombre
 * @property string|null $descripcion
 * @property string|null $codigo
 * @property float $precio_venta
 * @property float $precio_compra
 * @property float $precio_mayoreo
 * @property int $cantidad_mayoreo
 * @property float $precio_promedio
 * @property float $stock
 * @property string|null $imagen
 * @property string|null $ubicacion
 * @property int|null $inventariable
 * @property int|null $perecedero
 * @property int|null $materia_prima
 * @property int|null $web
 * @property int|null $marca_id
 * @property int|null $unimed_id
 * @property int|null $icategoria_id
 * @property int|null $iestado_id
 * @property int|null $orden
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @property string|null $marca
 * @property string|null $estado
 * @property string|null $unidad_medida
 * @property float|null $stock_fn
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Stock[] $stocks
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VistaItem enTienda($tienda)
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\VistaItem onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VistaItem whereCantidadMayoreo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VistaItem whereCodigo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VistaItem whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VistaItem whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VistaItem whereDescripcion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VistaItem whereEstado($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VistaItem whereIcategoriaId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VistaItem whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VistaItem whereIestadoId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VistaItem whereImagen($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VistaItem whereInventariable($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VistaItem whereMarca($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VistaItem whereMarcaId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VistaItem whereMateriaPrima($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VistaItem whereNombre($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VistaItem whereOrden($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VistaItem wherePerecedero($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VistaItem wherePrecioCompra($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VistaItem wherePrecioMayoreo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VistaItem wherePrecioPromedio($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VistaItem wherePrecioVenta($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VistaItem whereStock($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VistaItem whereStockFn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VistaItem whereUbicacion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VistaItem whereUnidadMedida($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VistaItem whereUnimedId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VistaItem whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VistaItem whereWeb($value)
 * @method static \Illuminate\Database\Query\Builder|\App\VistaItem withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\VistaItem withoutTrashed()
 * @mixin \Eloquent
 */
class VistaItem extends Model
{

    use SoftDeletes;

    public function scopeEnTienda($query,$tienda)
    {

        return $query->whereIn('id', function($q) use ($tienda){
            $q->select('item_id')->from('stocks')->where('tienda_id',$tienda)->whereNull('deleted_at');
        });

    }

    public function stocks()
    {
        return $this->hasMany(\App\Models\Stock::class,'item_id','id');
    }

    public function stockTienda($tienda){

        return $this->stocks()->deTienda($tienda)->sum('cantidad');

    }
}
