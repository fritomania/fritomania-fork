<?php

namespace App\Listeners;

use App\Events\EventSolicitudCreate;
use App\Mail\SolicitudEmail;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Mail;

class SolicitudSendMail
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  SolicitudCreate  $event
     * @return void
     */
    public function handle(EventSolicitudCreate $event)
    {
        $sol = $event->solicitude;

        $despachan = $sol->user->usersSolicita;

        foreach ($despachan as $user){
            Mail::to($user->email)->send(new SolicitudEmail($sol->user->name));
        }

    }
}
