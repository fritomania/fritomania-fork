<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\VistaCombo
 *
 * @property int $id
 * @property int $item_id
 * @property int $piezas
 * @property int $bebidas
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @property string $item
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\VistaCombo onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VistaCombo whereBebidas($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VistaCombo whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VistaCombo whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VistaCombo whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VistaCombo whereItem($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VistaCombo whereItemId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VistaCombo wherePiezas($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VistaCombo whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\VistaCombo withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\VistaCombo withoutTrashed()
 * @mixin \Eloquent
 */
class VistaCombo extends Model
{
    use SoftDeletes;
}
