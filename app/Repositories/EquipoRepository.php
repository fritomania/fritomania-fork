<?php

namespace App\Repositories;

use App\Models\Equipo;
use InfyOm\Generator\Common\BaseRepository;

class EquipoRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'etipo_id',
        'emarca_id',
        'modelo_id',
        'numero_serie',
        'imei'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Equipo::class;
    }
}
