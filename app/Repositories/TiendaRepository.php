<?php

namespace App\Repositories;

use App\Models\Tienda;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class TiendaRepository
 * @package App\Repositories
 * @version July 26, 2018, 4:46 pm CST
 *
 * @method Tienda findWithoutFail($id, $columns = ['*'])
 * @method Tienda find($id, $columns = ['*'])
 * @method Tienda first($columns = ['*'])
*/
class TiendaRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'admin',
        'nombre',
        'direccion',
        'telefono'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Tienda::class;
    }
}
