<?php

namespace App\Repositories;

use App\Models\Iestado;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class IestadoRepository
 * @package App\Repositories
 * @version July 26, 2018, 2:48 pm CST
 *
 * @method Iestado findWithoutFail($id, $columns = ['*'])
 * @method Iestado find($id, $columns = ['*'])
 * @method Iestado first($columns = ['*'])
*/
class IestadoRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'descripcion'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Iestado::class;
    }
}
