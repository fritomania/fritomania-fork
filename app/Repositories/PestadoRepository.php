<?php

namespace App\Repositories;

use App\Models\Pestado;
use InfyOm\Generator\Common\BaseRepository;

class PestadoRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'descripcion'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Pestado::class;
    }
}
