<?php

namespace App\Repositories;

use App\Models\Vestado;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class VestadoRepository
 * @package App\Repositories
 * @version July 25, 2018, 2:19 pm CST
 *
 * @method Vestado findWithoutFail($id, $columns = ['*'])
 * @method Vestado find($id, $columns = ['*'])
 * @method Vestado first($columns = ['*'])
*/
class VestadoRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'descripcion'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Vestado::class;
    }
}
