<?php

namespace App\Repositories;

use App\Models\Solicitude;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class SolicitudeRepository
 * @package App\Repositories
 * @version July 31, 2018, 2:55 pm CST
 *
 * @method Solicitude findWithoutFail($id, $columns = ['*'])
 * @method Solicitude find($id, $columns = ['*'])
 * @method Solicitude first($columns = ['*'])
*/
class SolicitudeRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'numero',
        'observaciones',
        'user_id',
        'user_despacha',
        'fecha_despacha',
        'solicitud_estado_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Solicitude::class;
    }
}
