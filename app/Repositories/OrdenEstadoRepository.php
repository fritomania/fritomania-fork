<?php

namespace App\Repositories;

use App\Models\OrdenEstado;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class OrdenEstadoRepository
 * @package App\Repositories
 * @version August 9, 2018, 12:08 pm CST
 *
 * @method OrdenEstado findWithoutFail($id, $columns = ['*'])
 * @method OrdenEstado find($id, $columns = ['*'])
 * @method OrdenEstado first($columns = ['*'])
*/
class OrdenEstadoRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nombre'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return OrdenEstado::class;
    }
}
