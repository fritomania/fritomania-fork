<?php

namespace App\Repositories;

use App\Models\ProduccionTipo;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class ProduccionTipoRepository
 * @package App\Repositories
 * @version August 9, 2018, 12:56 pm CST
 *
 * @method ProduccionTipo findWithoutFail($id, $columns = ['*'])
 * @method ProduccionTipo find($id, $columns = ['*'])
 * @method ProduccionTipo first($columns = ['*'])
*/
class ProduccionTipoRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nombre'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ProduccionTipo::class;
    }
}
