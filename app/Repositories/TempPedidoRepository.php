<?php

namespace App\Repositories;

use App\Models\TempPedido;
use InfyOm\Generator\Common\BaseRepository;

class TempPedidoRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'user_id',
        'procesado'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return TempPedido::class;
    }
}
