<?php

namespace App\Repositories;

use App\Models\Empleado;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class EmpleadoRepository
 * @package App\Repositories
 * @version July 27, 2018, 4:14 pm CST
 *
 * @method Empleado findWithoutFail($id, $columns = ['*'])
 * @method Empleado find($id, $columns = ['*'])
 * @method Empleado first($columns = ['*'])
*/
class EmpleadoRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'tienda_id',
        'nombres',
        'apellidos',
        'telefono',
        'correo',
        'sueldo_diario',
        'porcentaje_comision',
        'genero',
        'puesto',
        'fecha_contratacion',
        'direccion',
        'fecha_nacimiento'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Empleado::class;
    }
}
