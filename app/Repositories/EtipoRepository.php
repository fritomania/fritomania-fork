<?php

namespace App\Repositories;

use App\Models\Etipo;
use InfyOm\Generator\Common\BaseRepository;

class EtipoRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nombre'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Etipo::class;
    }
}
