<?php

namespace App\Repositories;

use App\Models\Cpago;
use InfyOm\Generator\Common\BaseRepository;

class CpagoRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'compra_id',
        'monto',
        'user_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Cpago::class;
    }
}
