<?php

namespace App\Repositories;

use App\Models\Venta;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class VentaRepository
 * @package App\Repositories
 * @version July 25, 2018, 2:43 pm CST
 *
 * @method Venta findWithoutFail($id, $columns = ['*'])
 * @method Venta find($id, $columns = ['*'])
 * @method Venta first($columns = ['*'])
*/
class VentaRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'cliente_id',
        'fecha',
        'serie',
        'numero',
        'recibido',
        'credito',
        'pagada',
        'vestado_id',
        'caja_id',
        'user_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Venta::class;
    }
}
