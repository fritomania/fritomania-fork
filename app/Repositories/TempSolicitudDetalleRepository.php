<?php

namespace App\Repositories;

use App\Models\TempSolicitudDetalle;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class TempSolicitudDetalleRepository
 * @package App\Repositories
 * @version July 31, 2018, 4:32 pm CST
 *
 * @method TempSolicitudDetalle findWithoutFail($id, $columns = ['*'])
 * @method TempSolicitudDetalle find($id, $columns = ['*'])
 * @method TempSolicitudDetalle first($columns = ['*'])
*/
class TempSolicitudDetalleRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'cantidad',
        'temp_solicitude_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return TempSolicitudDetalle::class;
    }
}
