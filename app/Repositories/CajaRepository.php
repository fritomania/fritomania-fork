<?php

namespace App\Repositories;

use App\Models\Caja;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class CajaRepository
 * @package App\Repositories
 * @version July 25, 2018, 8:09 pm CST
 *
 * @method Caja findWithoutFail($id, $columns = ['*'])
 * @method Caja find($id, $columns = ['*'])
 * @method Caja first($columns = ['*'])
*/
class CajaRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'monto_apertura',
        'monto_cierre',
        'fecha_cierre',
        'user_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Caja::class;
    }
}
