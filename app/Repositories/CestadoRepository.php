<?php

namespace App\Repositories;

use App\Models\Cestado;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class CestadoRepository
 * @package App\Repositories
 * @version July 26, 2018, 4:03 pm CST
 *
 * @method Cestado findWithoutFail($id, $columns = ['*'])
 * @method Cestado find($id, $columns = ['*'])
 * @method Cestado first($columns = ['*'])
*/
class CestadoRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'descripcion'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Cestado::class;
    }
}
