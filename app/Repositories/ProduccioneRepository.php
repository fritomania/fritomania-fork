<?php

namespace App\Repositories;

use App\Models\Produccione;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class ProduccioneRepository
 * @package App\Repositories
 * @version August 10, 2018, 5:39 pm CST
 *
 * @method Produccione findWithoutFail($id, $columns = ['*'])
 * @method Produccione find($id, $columns = ['*'])
 * @method Produccione first($columns = ['*'])
*/
class ProduccioneRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'numero',
        'produccion_tipo_id',
        'produccion_estado_id',
        'user_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Produccione::class;
    }
}
