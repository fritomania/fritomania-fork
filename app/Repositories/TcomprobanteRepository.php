<?php

namespace App\Repositories;

use App\Models\Tcomprobante;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class TcomprobanteRepository
 * @package App\Repositories
 * @version July 26, 2018, 4:10 pm CST
 *
 * @method Tcomprobante findWithoutFail($id, $columns = ['*'])
 * @method Tcomprobante find($id, $columns = ['*'])
 * @method Tcomprobante first($columns = ['*'])
*/
class TcomprobanteRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nombre'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Tcomprobante::class;
    }
}
