<?php

namespace App\Repositories;

use App\Models\Cuadre;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class CuadreRepository
 * @package App\Repositories
 * @version July 25, 2018, 7:20 pm CST
 *
 * @method Cuadre findWithoutFail($id, $columns = ['*'])
 * @method Cuadre find($id, $columns = ['*'])
 * @method Cuadre first($columns = ['*'])
*/
class CuadreRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'fecha',
        'total_sistema',
        'cash'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Cuadre::class;
    }
}
