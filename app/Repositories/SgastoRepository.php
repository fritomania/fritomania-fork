<?php

namespace App\Repositories;

use App\Models\Sgasto;
use InfyOm\Generator\Common\BaseRepository;

class SgastoRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'servicio_id',
        'descripcion',
        'cantidad',
        'precio',
        'user_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Sgasto::class;
    }
}
