<?php

namespace App\Repositories;

use App\Models\Cretiro;
use InfyOm\Generator\Common\BaseRepository;

class CretiroRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'caja_id',
        'motivo',
        'monto'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Cretiro::class;
    }
}
