<?php

namespace App\Repositories;

use App\Models\TempSolicitude;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class TempSolicitudeRepository
 * @package App\Repositories
 * @version July 31, 2018, 7:00 pm CST
 *
 * @method TempSolicitude findWithoutFail($id, $columns = ['*'])
 * @method TempSolicitude find($id, $columns = ['*'])
 * @method TempSolicitude first($columns = ['*'])
*/
class TempSolicitudeRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'user_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return TempSolicitude::class;
    }
}
