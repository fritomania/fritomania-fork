<?php

namespace App\Repositories;

use App\Models\Eatributo;
use InfyOm\Generator\Common\BaseRepository;

class EatributoRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nombre'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Eatributo::class;
    }
}
