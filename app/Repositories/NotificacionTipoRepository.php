<?php

namespace App\Repositories;

use App\Models\NotificacionTipo;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class NotificacionTipoRepository
 * @package App\Repositories
 * @version August 1, 2018, 2:17 pm CST
 *
 * @method NotificacionTipo findWithoutFail($id, $columns = ['*'])
 * @method NotificacionTipo find($id, $columns = ['*'])
 * @method NotificacionTipo first($columns = ['*'])
*/
class NotificacionTipoRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nombre'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return NotificacionTipo::class;
    }
}
