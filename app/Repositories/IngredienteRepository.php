<?php

namespace App\Repositories;

use App\Models\Ingrediente;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class IngredienteRepository
 * @package App\Repositories
 * @version December 16, 2018, 4:07 pm -03
 *
 * @method Ingrediente findWithoutFail($id, $columns = ['*'])
 * @method Ingrediente find($id, $columns = ['*'])
 * @method Ingrediente first($columns = ['*'])
*/
class IngredienteRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nombre' => 'like',
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Ingrediente::class;
    }
}
