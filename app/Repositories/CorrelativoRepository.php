<?php

namespace App\Repositories;

use App\Models\Correlativo;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class CorrelativoRepository
 * @package App\Repositories
 * @version July 27, 2018, 4:02 pm CST
 *
 * @method Correlativo findWithoutFail($id, $columns = ['*'])
 * @method Correlativo find($id, $columns = ['*'])
 * @method Correlativo first($columns = ['*'])
*/
class CorrelativoRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'tabla',
        'anio',
        'max'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Correlativo::class;
    }
}
