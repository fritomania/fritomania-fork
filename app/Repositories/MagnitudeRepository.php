<?php

namespace App\Repositories;

use App\Models\Magnitude;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class MagnitudeRepository
 * @package App\Repositories
 * @version July 26, 2018, 3:19 pm CST
 *
 * @method Magnitude findWithoutFail($id, $columns = ['*'])
 * @method Magnitude find($id, $columns = ['*'])
 * @method Magnitude first($columns = ['*'])
*/
class MagnitudeRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nombre'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Magnitude::class;
    }
}
