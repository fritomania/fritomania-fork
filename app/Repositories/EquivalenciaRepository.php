<?php

namespace App\Repositories;

use App\Models\Equivalencia;
use InfyOm\Generator\Common\BaseRepository;

class EquivalenciaRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'item_origen',
        'item_destino',
        'cantidad'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Equivalencia::class;
    }
}
