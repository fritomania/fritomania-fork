<?php

namespace App\Repositories;

use App\Models\Licencia;
use InfyOm\Generator\Common\BaseRepository;

class LicenciaRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'fecha_expira',
        'prueba'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Licencia::class;
    }
}
