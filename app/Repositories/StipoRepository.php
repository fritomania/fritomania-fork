<?php

namespace App\Repositories;

use App\Models\Stipo;
use InfyOm\Generator\Common\BaseRepository;

class StipoRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nombre',
        'descripcion',
        'precio',
        'dias',
        'prioridad'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Stipo::class;
    }
}
