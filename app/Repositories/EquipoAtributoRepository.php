<?php

namespace App\Repositories;

use App\Models\EquipoAtributo;
use InfyOm\Generator\Common\BaseRepository;

class EquipoAtributoRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'equipo_id',
        'eatributo_id',
        'valor'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return EquipoAtributo::class;
    }
}
