<?php

namespace App\Repositories;

use App\Models\Descuento;
use InfyOm\Generator\Common\BaseRepository;

class DescuentoRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'item_id',
        'porcentaje',
        'fecha_inicio',
        'fecha_expira'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Descuento::class;
    }
}
