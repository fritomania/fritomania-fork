<?php

namespace App\Repositories;

use App\Models\Emarca;
use InfyOm\Generator\Common\BaseRepository;

class EmarcaRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nombre'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Emarca::class;
    }
}
