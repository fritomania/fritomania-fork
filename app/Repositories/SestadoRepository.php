<?php

namespace App\Repositories;

use App\Models\Sestado;
use InfyOm\Generator\Common\BaseRepository;

class SestadoRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nombre'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Sestado::class;
    }
}
