<?php

namespace App\Repositories;

use App\Models\Sbitacora;
use InfyOm\Generator\Common\BaseRepository;

class SbitacoraRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'servicio_id',
        'sestado_id',
        'titulo',
        'descripcion',
        'user_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Sbitacora::class;
    }
}
