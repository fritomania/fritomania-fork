<?php

namespace App\Repositories;

use App\Models\Pedido;
use InfyOm\Generator\Common\BaseRepository;

class PedidoRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'cliente_id',
        'fecha_ingreso',
        'fecha_entrega',
        'user_id',
        'pestado_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Pedido::class;
    }
}
