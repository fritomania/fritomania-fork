<?php

namespace App\Repositories;

use App\Models\Servicio;
use InfyOm\Generator\Common\BaseRepository;

class ServicioRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'stipo_id',
        'equipo_id',
        'cliente_id',
        'sestado_id',
        'usuario_recibe',
        'numero',
        'observaciones',
        'diagnostico',
        'costo_cotizacion',
        'costo_reparacion',
        'fecha_estimada',
        'usuario_asigna',
        'fecha_asigna',
        'fecha_inicio',
        'fecha_fin',
        'reparacion',
        'usuario_entrega',
        'fecha_entrega'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Servicio::class;
    }
}
