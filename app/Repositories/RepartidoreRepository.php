<?php

namespace App\Repositories;

use App\Models\Repartidore;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class RepartidoreRepository
 * @package App\Repositories
 * @version August 2, 2018, 3:28 pm CST
 *
 * @method Repartidore findWithoutFail($id, $columns = ['*'])
 * @method Repartidore find($id, $columns = ['*'])
 * @method Repartidore first($columns = ['*'])
*/
class RepartidoreRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nombres',
        'apellidos',
        'hora_inicia',
        'hora_fin',
        'user_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Repartidore::class;
    }
}
