<?php

namespace App\Repositories;

use App\Models\TempPedidoDetalle;
use InfyOm\Generator\Common\BaseRepository;

class TempPedidoDetalleRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'temp_pedido_id',
        'item_id',
        'cantidad',
        'precio',
        'descuento'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return TempPedidoDetalle::class;
    }
}
