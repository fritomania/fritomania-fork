<?php

namespace App\Repositories;

use App\Models\Icategoria;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class IcategoriaRepository
 * @package App\Repositories
 * @version July 26, 2018, 2:59 pm CST
 *
 * @method Icategoria findWithoutFail($id, $columns = ['*'])
 * @method Icategoria find($id, $columns = ['*'])
 * @method Icategoria first($columns = ['*'])
*/
class IcategoriaRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nombre',
        'descripcion'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Icategoria::class;
    }
}
