<?php

namespace App\Repositories;

use App\Models\ProduccionEstado;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class ProduccionEstadoRepository
 * @package App\Repositories
 * @version August 10, 2018, 5:02 pm CST
 *
 * @method ProduccionEstado findWithoutFail($id, $columns = ['*'])
 * @method ProduccionEstado find($id, $columns = ['*'])
 * @method ProduccionEstado first($columns = ['*'])
*/
class ProduccionEstadoRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nombre'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ProduccionEstado::class;
    }
}
