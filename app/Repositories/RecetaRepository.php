<?php

namespace App\Repositories;

use App\Models\Receta;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class RecetaRepository
 * @package App\Repositories
 * @version August 10, 2018, 5:54 pm CST
 *
 * @method Receta findWithoutFail($id, $columns = ['*'])
 * @method Receta find($id, $columns = ['*'])
 * @method Receta first($columns = ['*'])
*/
class RecetaRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'item_id',
        'user_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Receta::class;
    }
}
