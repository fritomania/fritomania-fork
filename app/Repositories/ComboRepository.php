<?php

namespace App\Repositories;

use App\Models\Combo;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class ComboRepository
 * @package App\Repositories
 * @version September 14, 2018, 5:47 pm -03
 *
 * @method Combo findWithoutFail($id, $columns = ['*'])
 * @method Combo find($id, $columns = ['*'])
 * @method Combo first($columns = ['*'])
*/
class ComboRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'item_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Combo::class;
    }
}
