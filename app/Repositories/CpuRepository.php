<?php

namespace App\Repositories;

use App\Models\Cpu;
use InfyOm\Generator\Common\BaseRepository;

class CpuRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'etipo_id',
        'nombre',
        'cache',
        'nucleos',
        'subprocesos',
        'frecuencia_basica',
        'frecuencia_maxima',
        'unimed_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Cpu::class;
    }
}
