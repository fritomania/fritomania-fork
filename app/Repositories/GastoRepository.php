<?php

namespace App\Repositories;

use App\Models\Gasto;
use InfyOm\Generator\Common\BaseRepository;

class GastoRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'descripcion',
        'monto',
        'user_id',
        'tienda_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Gasto::class;
    }
}
