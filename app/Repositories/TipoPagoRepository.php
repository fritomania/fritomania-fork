<?php

namespace App\Repositories;

use App\Models\TipoPago;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class TipoPagoRepository
 * @package App\Repositories
 * @version August 26, 2018, 3:16 pm -03
 *
 * @method TipoPago findWithoutFail($id, $columns = ['*'])
 * @method TipoPago find($id, $columns = ['*'])
 * @method TipoPago first($columns = ['*'])
*/
class TipoPagoRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nombre'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return TipoPago::class;
    }
}
