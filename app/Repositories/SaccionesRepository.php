<?php

namespace App\Repositories;

use App\Models\Sacciones;
use InfyOm\Generator\Common\BaseRepository;

class SaccionesRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'servicio_id',
        'descripcion',
        'user_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Sacciones::class;
    }
}
