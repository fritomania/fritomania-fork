<?php

namespace App\Repositories;

use App\Models\Horario;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class HorarioRepository
 * @package App\Repositories
 * @version July 27, 2018, 3:54 pm CST
 *
 * @method Horario findWithoutFail($id, $columns = ['*'])
 * @method Horario find($id, $columns = ['*'])
 * @method Horario first($columns = ['*'])
*/
class HorarioRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'dia',
        'hora_ini',
        'hora_fin'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Horario::class;
    }
}
