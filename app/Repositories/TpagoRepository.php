<?php

namespace App\Repositories;

use App\Models\Tpago;
use InfyOm\Generator\Common\BaseRepository;

class TpagoRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nombre'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Tpago::class;
    }
}
