<?php

namespace App\Repositories;

use App\Models\SolicitudEstado;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class SolicitudEstadoRepository
 * @package App\Repositories
 * @version July 31, 2018, 2:40 pm CST
 *
 * @method SolicitudEstado findWithoutFail($id, $columns = ['*'])
 * @method SolicitudEstado find($id, $columns = ['*'])
 * @method SolicitudEstado first($columns = ['*'])
*/
class SolicitudEstadoRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nombre'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return SolicitudEstado::class;
    }
}
