<?php

namespace App\Repositories;

use App\Models\Vpago;
use InfyOm\Generator\Common\BaseRepository;

class VpagoRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'monto',
        'user_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Vpago::class;
    }
}
