<?php

namespace App\Repositories;

use App\Models\Notificacione;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class NotificacioneRepository
 * @package App\Repositories
 * @version August 28, 2018, 8:33 pm -03
 *
 * @method Notificacione findWithoutFail($id, $columns = ['*'])
 * @method Notificacione find($id, $columns = ['*'])
 * @method Notificacione first($columns = ['*'])
*/
class NotificacioneRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'mensaje',
        'user_id',
        'notificacion_tipo_id',
        'visto'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Notificacione::class;
    }
}
