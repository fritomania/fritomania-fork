<?php

namespace App\Repositories;

use App\Models\TipoMarcaje;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class TipoMarcajeRepository
 * @package App\Repositories
 * @version September 5, 2018, 10:55 pm -03
 *
 * @method TipoMarcaje findWithoutFail($id, $columns = ['*'])
 * @method TipoMarcaje find($id, $columns = ['*'])
 * @method TipoMarcaje first($columns = ['*'])
*/
class TipoMarcajeRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nombre'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return TipoMarcaje::class;
    }
}
