<?php

namespace App\Repositories;

use App\Models\TempFile;
use InfyOm\Generator\Common\BaseRepository;

class TempFileRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'user_id',
        'data',
        'nombre',
        'type',
        'size',
        'extension'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return TempFile::class;
    }
}
