<?php

namespace App\Repositories;

use App\Models\Marcaje;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class MarcajeRepository
 * @package App\Repositories
 * @version September 6, 2018, 1:38 am -03
 *
 * @method Marcaje findWithoutFail($id, $columns = ['*'])
 * @method Marcaje find($id, $columns = ['*'])
 * @method Marcaje first($columns = ['*'])
*/
class MarcajeRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'tipo_marcaje_id',
        'user_id',
        'fecha_hora'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Marcaje::class;
    }
}
