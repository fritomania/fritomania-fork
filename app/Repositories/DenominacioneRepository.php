<?php

namespace App\Repositories;

use App\Models\Denominacione;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class DenominacioneRepository
 * @package App\Repositories
 * @version September 5, 2018, 6:36 pm -03
 *
 * @method Denominacione findWithoutFail($id, $columns = ['*'])
 * @method Denominacione find($id, $columns = ['*'])
 * @method Denominacione first($columns = ['*'])
*/
class DenominacioneRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'monto'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Denominacione::class;
    }
}
