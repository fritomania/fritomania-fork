<?php

namespace App\Repositories;

use App\Models\Compra;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class CompraRepository
 * @package App\Repositories
 * @version July 26, 2018, 2:11 pm CST
 *
 * @method Compra findWithoutFail($id, $columns = ['*'])
 * @method Compra find($id, $columns = ['*'])
 * @method Compra first($columns = ['*'])
*/
class CompraRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'proveedor_id',
        'tcomprobante_id',
        'fecha',
        'serie',
        'numero',
        'credito',
        'pagada',
        'user_id',
        'cestado_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Compra::class;
    }
}
