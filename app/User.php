<?php

namespace App;

use App\Models\Empleado;
use App\Models\Notificacione;
use App\Models\Rol;
use App\Models\Tienda;
use App\Models\TipoMarcaje;
use App\Models\Venta;
use App\Models\Vpago;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Auth;
use Jenssegers\Date\Date;

/**
 * App\User
 *
 * @property int $id
 * @property int|null $tienda_id
 * @property string $name
 * @property string $username
 * @property string|null $email
 * @property string $password
 * @property string|null $remember_token
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Caja[] $cajas
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Venta[] $deliverys
 * @property-read \App\Models\Empleado $empleado
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Notificacione[] $notificaciones
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Option[] $opciones
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Rol[] $rols
 * @property-read \App\Models\Tienda|null $tienda
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Uimage[] $uimages
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\User[] $usersDespacha
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\User[] $usersSolicita
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Venta[] $ventas
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Vpago[] $vpagos
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User admins()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User caja()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User deTienda($tienda = null)
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\User onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User presentes()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User repartidor()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User superAdmins()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereTiendaId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereUsername($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\User withoutTrashed()
 * @mixin \Eloquent
 */
class User extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'tienda_id','name', 'username','email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function scopeDeTienda($query, $tienda = null){
        $tienda = isset($tienda) ? $tienda : session('tienda');
        return $query->where('tienda_id',$tienda);
    }

    public function scopeCaja($query){
        return $query->whereIn('id', function($q){
            $q->select('user_id')->from('rol_user')->where('rol_id',Rol::CAJA);
        });
    }

    public function scopeRepartidor($query){
        return $query->whereIn('id', function($q){
            $q->select('user_id')->from('rol_user')->where('rol_id',Rol::REPARTIDOR);
        });
    }

    public function scopePresentes($query){

        return $query->whereIn('id', function($q){
            $q->select('user_id')->from('marcajes')
                ->where('tipo_marcaje_id',TipoMarcaje::ENTRADA)
                ->where('fecha',hoyDb())
                ->whereNull('deleted_at');
        });
    }

    public function scopeAdmins($query){
        return $query->whereIn('id', function($q){
            $q->select('user_id')->from('rol_user')->where('rol_id',Rol::ADMIN);
        });
    }

    public function scopeSuperAdmins($query){
        return $query->whereIn('id', function($q){
            $q->select('user_id')->from('rol_user')->where('rol_id',Rol::SUPER);
        });
    }


    public function opciones(){
        return $this->belongsToMany(Option::class);
    }

    public function rols()
    {
      return $this->belongsToMany(Rol::class);
    }

    /**
     * Si el usuario contiene el rol de administrador
     * @return bool
     */
    public function isAdmin(){

        return $this->rols->contains('id',Rol::ADMIN);
    }

    /**
     * Si el usuario es el administra
     * @return bool
     */
    public function isAdminTienda(){

        return ($this->id == $this->tienda->admin);
    }

    public function isSuperAdmin(){

        return $this->rols->contains('id',Rol::SUPER);
    }

    public function esSoloEmpleado(){

        return $this->rols->contains('id',Rol::EMPLEADO) && !$this->isSuperAdmin() && !$this->isAdmin();
    }

    public function isCaja(){
        return $this->rols->contains('id',Rol::CAJA);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function cajas()
    {
        return $this->hasMany(\App\Models\Caja::class);
    }

    /**
     * Devuelve la ultima caja abierta por el usuario
     * @return bool|mixed
     */
    public function cajaAbierta(){

        $caja = $this->cajas()
            ->whereNull('fecha_cierre')
            ->where('user_id',$this->id)
            ->where('tienda_id',session('tienda'))
            ->orderBy('id','desc')
            ->first();

        return $caja ? $caja : false;
    }

    public function ventas()
    {
        return $this->hasMany(Venta::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function empleado()
    {
        return $this->belongsTo(Empleado::class);
    }

    public function ventasHoy()
    {
        $ventas = $this->ventas()->hoy()->procesadas()->noCredito()->noNegocio()->get();

        return $ventas;
    }

    public function totalVentasHoy()
    {
        return array_sum(array_pluck($this->ventasHoy()->toArray(),'total'));
    }

    public function totalCobrosHoy()
    {
        $cobros = $this->vpagos()->hoy()->get();

        return array_sum(array_pluck($cobros,'monto'));
    }

    public function vpagos()
    {
        return $this->hasMany(Vpago::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function uimages()
    {
        return $this->hasMany(\App\Models\Uimage::class);
    }

    public function imagen()
    {
        $imagen = $this->uimages()->first();

        $imagen = is_null($imagen) ? asset('img/avatar_none.png') : srcImgBynary($imagen);

        return $imagen;
    }

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name'     => 'required|max:255',
        'tienda_id'     => 'required',
        'username' => 'sometimes|required|max:255|unique:users',
        'email'    => 'required|email|max:255',
        'password' => 'required|min:6|confirmed',
    ];


    public function usersDespacha(){
        return $this->belongsToMany(User::class, 'user_despacha_user','user_des', 'user_sol');
    }

    public function usersSolicita(){
        return $this->belongsToMany(User::class, 'user_despacha_user','user_sol','user_des');
    }

    public function tienda()
    {
        return $this->belongsTo(Tienda::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     **/
    public function deliverys()
    {
        return $this->belongsToMany(\App\Models\Venta::class, 'asiganacion_delivery');
    }

    public function notificaciones()
    {
        return $this->hasMany(Notificacione::class,'user_id','id');
    }

    public function tiempoUltimaNotificacion($tipo=null)
    {
        $query = $this->notificaciones()
                ->orderBy('created_at','asc');

        if($tipo){
            $query->deTipo($tipo);
        }

        $notificacion = $query->first();

        if($notificacion){
            $fecha = new Date($notificacion->created_at);

            return $fecha->diffForHumans(Carbon::now(),1);
        }


        return '';
    }
}
