<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\VistaReceta
 *
 * @property int $id
 * @property int $item_id
 * @property float $cantidad
 * @property string|null $procedimiento
 * @property int $user_id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @property string $item
 * @property string|null $unimed
 * @property string $usuario
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\VistaReceta onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VistaReceta whereCantidad($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VistaReceta whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VistaReceta whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VistaReceta whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VistaReceta whereItem($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VistaReceta whereItemId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VistaReceta whereProcedimiento($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VistaReceta whereUnimed($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VistaReceta whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VistaReceta whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VistaReceta whereUsuario($value)
 * @method static \Illuminate\Database\Query\Builder|\App\VistaReceta withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\VistaReceta withoutTrashed()
 * @mixin \Eloquent
 */
class VistaReceta extends Model
{
    use SoftDeletes;
}
