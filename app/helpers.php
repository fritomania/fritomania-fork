<?php
/**
 * Created by PhpStorm.
 * User: Usuario
 * Date: 16/04/2017
 * Time: 2:57 PM
 */
use Carbon\Carbon;

/**
 * Cambia el formato de una fecha d/m/Y a Y-m-d
 * @param null $fecha
 * @param string $separador el de la fecha introducida
 * @param string $divisor el de la fecha a devolver
 * @return null|string
 */
function fechaDb($fecha=NULL, $separador='/', $divisor="-"){

    if(is_null($fecha))
        return NULL;

    $tmp=explode("$separador",$fecha);

    return $tmp[2].$divisor.$tmp[1].$divisor.$tmp[0];
}

/**
 * Cambia el formato de una fecha Y-m-d a d/m/Y
 * @param null $fecha
 * @param string $separador el de la fecha introducida
 * @param string $divisor el de la fecha a devolver
 * @return null|string
 */
function fecha($fecha=NULL, $separador='-', $divisor="/"){

    if(is_null($fecha))
        return NULL;

    $tmp=explode("$separador",$fecha);

    return $tmp[2].$divisor.$tmp[1].$divisor.$tmp[0];
}

/**
 * Devuelve la fecha de hoy en formato config('app.timezone' 'd/m/Y'
 * @return string
 */
function hoy(){

    return \Carbon\Carbon::now(config('app.timezone'))->format('d/m/Y');
}

function anioActual(){

    list($dia,$mes,$anio)= explode('/',hoy());
    return $anio;
}

/**
 * Devuelve la fecha de hoy en formato config('app.timezone' 'd/m/Y'
 * @return string
 */
function hoyYmas($dias){

    return \Carbon\Carbon::now(config('app.timezone'))->addDays($dias)->format('d/m/Y');
}

/**
 * Devuelve la fecha de hoy en formato config('app.timezone' 'Y-m-d' para guardar en la base de datos
 * @return string
 */
function hoyDb(){

    return \Carbon\Carbon::now(config('app.timezone'))->format('Y-m-d');
}


function diasMes($anio=0,$mes=0){

    if(!$mes && !$anio)
        return false;

    return cal_days_in_month ( CAL_GREGORIAN , $mes , $anio );

}

function diasMesActual(){

    $fechaActual= hoy();

    list($dia,$mes,$anio)=explode('/',$fechaActual);

    return diasMes($anio,$mes);

}

function mesLetras($mes=0){
    $mes=$mes*1;//pasar a entero

    if($mes<1 || $mes>12)
        return 'mes invalido';

    $meses=['mes invalido','enero','febrero','marzo','abril','mayo','junio','julio','agosto','septiembre','octubre','noviembre','diciembre'];

    return $meses[$mes];
}

function arrayDias(){
    $dias=['domingo','lunes','martes','miércoles','jueves','viernes','sábado','domingo',];

    return $dias;
}

function diaLetras($dia=NULL){
    if(is_null($dia))
        return 'día invalido';

    $dias=arrayDias();

    return $dias[$dia];
}


/**
 * Devuelve la fecha y hora actual en el formato necesario para guardar en la base de datos (Y-m-d h:m:s)
 * @return string
 */
function fechaHoraActualDb(){
    return Carbon::now()->toDateTimeString();
}

/**
 * Devuelve la fecha y hora actual en formato 'd/m/Y H:m:s'
 * @return string
 */
function fechaHoraActual(){

    return Carbon::now()->format('d/m/Y H:i:s');
}

/**
 * Elimina los ceros decimales de un numero
 * @return string
 */
function noCerosDecimales($numero){

    list($entero,$decimal)=explode('.',$numero);

    return ($decimal>0) ? $numero : $entero;
}

/**
 * Convierte un archivo a su valor binario
 * @param $file
 * @return mixed
 */
function fileToBinary($file){
    return $file->openFile()->fread($file->getSize());
}

/**
 * Devuelve el src para etiqueta <img> en base a imágenes binarias
 * @param $img
 * @return string
 */
function srcImgBynary($img){
    return "data:".$img->type.";base64,".base64_encode($img->data);
}

/**
 * Format bytes to kb, mb, gb, tb
 *
 * @param  integer $size
 * @param  integer $precision
 * @return integer
 */
function formatBytes($size, $precision = 2)
{
    if ($size > 0) {
        $size = (int) $size;
        $base = log($size) / log(1024);
        $suffixes = array(' bytes', ' KB', ' MB', ' GB', ' TB');

        return round(pow(1024, $base - floor($base)), $precision) . $suffixes[floor($base)];
    } else {
        return $size;
    }
}

function diasEntreFechas($fini,$ffin){
    $datetime1 = new DateTime($fini);
    $datetime2 = new DateTime($ffin);
    $interval = $datetime1->diff($datetime2);

//    return $interval->format('%R%a');
    return $interval->format('%a');
}

function fechaExpiraLicencia(){
    $licencia = \App\Models\Licencia::first();

    return is_null($licencia) ? null : $licencia->fecha_expira;
}

function diasRestanteLicencia(){

    return diasEntreFechas(hoyDb(),fechaExpiraLicencia());
}

function licenciaEsPrueba(){
    $licencia = \App\Models\Licencia::first();

    return is_null($licencia) ? false : $licencia->prueba;
}

function diaSemana($fecha){
    $f=Carbon::parse($fecha);

    return diaLetras($f->dayOfWeek);
}

function fechaHoraLtn($fecha){

    return date('d/m/Y H:i:s A',strtotime($fecha));
}

function mesActual(){

    $fechaActual= hoy();

    list($dia,$mes,$anio)=explode('/',$fechaActual);

    return $mes;

}

function mesActualLtr(){
    return mesLetras(mesActual());
}


function iniMes(){

    list($dia,$mes,$anio)=explode('/',hoy());

    return '01/'.$mes.'/'.$anio;
}


function iniMesDb(){

    list($dia,$mes,$anio)=explode('/',hoy());

    return $anio.'-'.$mes.'-01';
}

function mesActualBetween(){
    $del = Carbon::createFromFormat('Y-m-d H:i:s', iniMesDb().'00:00:00');
    $al = Carbon::createFromFormat('Y-m-d H:i:s', fechaHoraActualDb());

    return [$del,$al];
}

function delAlBetween($del,$al){
    $del = Carbon::createFromFormat('Y-m-d H:i:s', fechaDb($del).'00:00:00');
    $al = Carbon::createFromFormat('Y-m-d H:i:s', fechaDb($al).Carbon::now()->format('H:i:s'));

    return [$del,$al];
}

function formatVueSelect($collection,$label='name'){

    $collection = $collection->map(function ($item) use ($label){

        $item = $item->toArray();
        return [
            'label' => $item[$label],
            'id' => $item['id']
        ];
    });

    return json_encode($collection);
}

/**
 * Devuelve el símbolo de la moneda que esta guardada en las variables de configuración en la tabla configurations
 * @return \Illuminate\Config\Repository|mixed
 */
function dvs(){
    return config('app.divisa');
}

/**
 * Formatea los números de cantidades con separador de miles, separador decimales y cantidad de decimales mediante llaves de configuración
 */
function nf($numero,$cantidad_decimales=null,$separador_decimal=null,$separador_miles=null){

    $cantidad_decimales = is_null($cantidad_decimales) ? config('app.cantidad_decimales') : $cantidad_decimales;
    $separador_decimal = !$separador_decimal ? config('app.separador_decimal') : $separador_decimal;
    $separador_miles = !$separador_miles ? config('app.separador_miles') : $separador_miles;

    return number_format($numero,$cantidad_decimales,$separador_decimal,$separador_miles);
}

/**
 * Formatea los números de precios con separador de miles, separador decimales y cantidad de decimales mediante llaves de configuración
 */
function nfp($numero,$cantidad_decimales=null,$separador_decimal=null,$separador_miles=null){

    $cantidad_decimales = is_null($cantidad_decimales) ? config('app.cantidad_decimales_precio') : $cantidad_decimales;
    $separador_decimal = !$separador_decimal ? config('app.separador_decimal') : $separador_decimal;
    $separador_miles = !$separador_miles ? config('app.separador_miles') : $separador_miles;

    return number_format($numero,$cantidad_decimales,$separador_decimal,$separador_miles);
}

/**
 * Devuelve un array de dos posiciones [0] nombres y [1] apellidos, a partir de un nombre contiguo
 * (
 *    Implementación:
 *      list($nombres,$apellidos) = separaNombreApellido($request->name);
 * )
 * @param $nombre
 * @return array
 */
function separaNombreApellido($nombre){
    $temp = explode(' ',$nombre);

    switch (count($temp)){
        case 1:
            $nombres = $temp[0];
            $apellidos = '';
            break;
        case 2:
            $nombres = $temp[0];
            $apellidos = $temp[1];
            break;
        case 3:
            $nombres = $temp[0].' '.$temp[1];
            $apellidos = $temp[2];
            break;
        case 4:
            $nombres = $temp[0].' '.$temp[1];
            $apellidos = $temp[2].' '.$temp[3];
            break;
    }

    return [$nombres,$apellidos];
}


/**
 * Devuelve el mensaje correspondiente al responseCode del api de webpay
     0 Transacción aprobada.
    -1 Rechazo de transacción.
    -2 Transacción debe reintentarse.
    -3 Error en transacción.
    -4 Rechazo de transacción.
    -5 Rechazo por error de tasa.
    -6 Excede cupo máximo mensual.
    -7 Excede límite diario por transacción.
    -8  Rubro no autorizado.
 * @param $code
 */
function msjResponseCode($code){
    switch ($code){
        case 0:
            $msj = "Rechazo de transacción";
            break;
        case -1:
            $msj = "Rechazo de transacción";
            break;
        case -2:
            $msj = "Transacción debe reintentarse";
            break;
        case -3:
            $msj = "Error en transacción";
            break;
        case -4:
            $msj = "Rechazo de transacción";
            break;
        case -5:
            $msj = "Rechazo por error de tasa";
            break;
        case -6:
            $msj = "Excede cupo máximo mensual";
            break;
        case -7:
            $msj = "Excede límite diario por transacción";
            break;
        case -8:
            $msj = "Rubro no autorizado";
            break;
    }

    return $msj;
}

function slc($model,$defaultValue='Seleccione uno...',$defaultKey='',$label='nombre'){

    $options = $model::pluck($label,'id')->toArray();

    if (!is_null($defaultValue)){
        $options = array_prepend($options,$defaultValue,$defaultKey);
    }
    return $options;
}

function mailsAdmins(){

    $user =Auth::user();
    $mails = [];

    //recorrido de todos los usuarios admin
    foreach (\App\User::admins()->get() as $index => $u) {
        if($u->email){
            $mails[]= $u->email;
        }
    }

    if($user->isAdminTienda()){
        //si no existe el email en el array
        if(!in_array($user->email,$mails)){
            $mails[] = $user->email;
        }
    }

    return $mails;
}

function mailsSuperAdmins(){

    $user =Auth::user();
    $mails = [];

    //recorrido de todos los usuarios admin
    foreach (\App\User::superAdmins()->get() as $index => $u) {
        if($u->email){
            $mails[]= $u->email;
        }
    }

    return $mails;
}