<?php

namespace App\Providers;

use App\Models\Configuration;
use App\Models\Rol;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;
//use Laravel\Dusk\DuskServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $userAuth = Auth::user();

        setlocale(LC_ALL, config('app.locale'));

        if( !App::runningInConsole() ){
            $configurations = Configuration::pluck('value','key')->toArray();

            foreach ($configurations as $key => $value){
                config(['app.'.$key => $value]);
            }
        }

        //si usuario es admin
        Blade::if('super', function () {
            return Auth::user()->isSuperAdmin();
        });

        Blade::if('admin', function () {
            return Auth::user()->isAdmin();
        });

        Blade::if('dashboard', function () {
            $user = Auth::user();
            return ($user->isAdminTienda() || $user->isAdmin() || $user->isSuperAdmin() || $user->isCaja());
        });

        //si user no tiene opciones
        Blade::if('usernoops', function () {
            return Auth::user()->opciones->count()==0 ? true : false;
        });

        Blade::component('components.alert', 'alert');
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if ($this->app->environment() !== 'production') {
            $this->app->register(\Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider::class);
        }

        if ($this->app->environment() !== 'production') {
            $this->app->register(\Way\Generators\GeneratorsServiceProvider::class);
            $this->app->register(\Xethron\MigrationsGenerator\MigrationsGeneratorServiceProvider::class);
        }
    }
}
