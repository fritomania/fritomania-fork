<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\VistaCuadre
 *
 * @property int $id
 * @property string $fecha
 * @property float $total_sistema
 * @property float $cash
 * @property int $tienda_id
 * @property int|null $user_id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @property string|null $fecha_format
 * @property string|null $tienda
 * @property string|null $usuario
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VistaCuadre deTienda($tienda = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VistaCuadre whereCash($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VistaCuadre whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VistaCuadre whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VistaCuadre whereFecha($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VistaCuadre whereFechaFormat($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VistaCuadre whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VistaCuadre whereTienda($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VistaCuadre whereTiendaId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VistaCuadre whereTotalSistema($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VistaCuadre whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VistaCuadre whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VistaCuadre whereUsuario($value)
 * @mixin \Eloquent
 */
class VistaCuadre extends Model
{
    public function scopeDeTienda($q,$tienda=null)
    {
        $tienda = is_null($tienda) ? session('tienda') : $tienda;

        $q->where('tienda_id',$tienda);
    }
}
