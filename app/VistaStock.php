<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\VistaStock
 *
 * @property int $id
 * @property int|null $categoria_id
 * @property string|null $categoria
 * @property string|null $codigo
 * @property string $nombre
 * @property int $tienda_id
 * @property string $tienda
 * @property float $precio
 * @property float|null $stock
 * @property-read mixed $sub_total
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VistaStock deLugar($tienda = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VistaStock whereCategoria($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VistaStock whereCategoriaId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VistaStock whereCodigo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VistaStock whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VistaStock whereNombre($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VistaStock wherePrecio($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VistaStock whereStock($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VistaStock whereTienda($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VistaStock whereTiendaId($value)
 * @mixin \Eloquent
 */
class VistaStock extends Model
{
    protected $table= 'vista_stocks';

    protected $appends =['sub_total'];

    public function scopeDeLugar($q,$tienda=null)
    {
        $tienda = !$tienda ? session('tienda') : $tienda;

        return $q->where('tienda_id',$tienda);

    }

    public function getSubTotalAttribute()
    {
        return $this->precio*$this->stock;
    }
}
