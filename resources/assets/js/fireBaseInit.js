import firebase from 'firebase/app';
import 'firebase/firestore';
import firebaseConfig from './firebaseConfig';
const firebaseApp = firebase.initializeApp(firebaseConfig);
const firestore = firebase.firestore();
var settings = { timestampsInSnapshots: true};
firestore.settings(settings);
export default firebaseApp.firestore();