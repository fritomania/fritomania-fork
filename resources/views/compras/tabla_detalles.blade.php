<table class="table table-bordered table-hover table-xtra-condensed">
    <thead>
    <tr>
        <th>Producto</th>
        <th>Precio</th>
        <th>Cantidad</th>
        <th>Fecha V</th>
        <th>Subtotal</th>
    </tr>
    </thead>
    <tbody>
    @php
        $total=0;
    @endphp
    @foreach($compra->compraDetalles as $det)
        <tr>
            <td>{{$det->item->nombre}}</td>
            <td>{{dvs() . nfp($det->precio)}}</td>
            <td>{{nf($det->cantidad)}}</td>
            <td>{{fecha($det->fecha_ven)}}</td>
            <td>{{dvs().nfp($det->cantidad*$det->precio)}}</td>
        </tr>
        @php
            $total+=($det->cantidad*$det->precio);
        @endphp
    @endforeach

    </tbody>
    <tfoot>
    <tr>
        <th colspan="5"><span class="pull-right">Total  {{dvs().nfp($total)}}</span></th>
    </tr>
    </tfoot>
</table>
