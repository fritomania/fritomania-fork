@section('css')
    @include('layouts.datatables_css')
@endsection

<div class="table-responsive">
    {!! $dataTable->table(['width' => '100%']) !!}
</div>

<div class="row">
    <div class="col">
        <span class="badge badge-danger">No han ingresado</span>
        <span class="badge badge-warning">Hoy Ingresan</span>
    </div>
</div>

@section('scripts')
    @include('layouts.datatables_js')
    {!! $dataTable->scripts() !!}
    <script>
        $(function () {
            jQuery.fn.dataTable.Api.register( 'sum()', function ( ) {
                return this.flatten().reduce( function ( a, b ) {
                    if ( typeof a === 'string' ) {
                        a = a.replace(/[^\d.-]/g, '') * 1;
                    }
                    if ( typeof b === 'string' ) {
                        b = b.replace(/[^\d.-]/g, '') * 1;
                    }

                    return a + b;
                }, 0 );
            } );

            var dt = window.LaravelDataTables["dataTableBuilder"];

            //Cuando dibuja la tabla
            dt.on( 'draw.dt', function () {
                $(this).addClass('text-xs table-sm table-bordered table-hover');
                $(this).find('tbody,thead').addClass('text-sm');

                var td = dt.column( 8 ).data().sum();
                $("#total_deuda").text(addComas(td.toFixed(0)));
                $('[data-toggle="tooltip"]').tooltip();
            });


        })
    </script>
@endsection