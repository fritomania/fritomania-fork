@extends('layouts.app')

@include('layouts.xtra_condensed_css')
@include('layouts.vue')
@include('layouts.axios')
@include('compras.pagos.script')

@section('htmlheader_title')
    Compra crédito
@endsection

@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row">
                <div class="col">
                    <h1 class="m-0 text-dark">Compra crédito</h1>
                </div><!-- /.col -->
                <div class="col">
                    <a class="btn btn-outline-info float-right" href="{!! route('compras.pagar') !!}">
                        <i class="fa fa-list"></i>
                        <span class="d-none d-sm-inline">Listado</span>
                    </a>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content"  id="root">
        <div class="container-fluid">
            @include('flash::message')

            <div class="row">
                <div class="col-sm-6">
                    <div class="card card-default ">
                        <div class="card-header with-border">
                            <h3 class="card-title">Compra</h3>

                            <div class="card-tools pull-right">
                                <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                </button>
                            </div>
                            <!-- /.card-tools -->
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    @include('compras.show_fields')

                                </div>

                                <div class="col-sm-12">
                                    @include('compras.tabla_detalles')
                                </div>

                                {{--<div class="col-sm-12 pull-right">--}}
                                {{--<a href="{!! route('compras.pdf_factura', $venta->id) !!}" class="btn btn-primary" target="_blank" >--}}
                                {{--<i class="fa fa-print"></i> Comprobante--}}
                                {{--</a>--}}
                                {{--&nbsp;--}}
                                {{--<a href="{!! route('compras.pdf_factura2', $venta->id) !!}" class="btn btn-primary" target="_blank" >--}}
                                {{--<i class="fa fa-print"></i> factura--}}
                                {{--</a>--}}
                                {{--</div>--}}

                            </div>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <div class="col-sm-6">
                    <div class="card card-default">
                        <div class="card-header with-border">
                            <h3 class="card-title">
                                Abonos
                            </h3>

                            <div class="card-tools pull-right" >
                                <a class="btn btn-sm btn-success" data-toggle="modal" href="#modalCreateCpago" v-if="saldo>0">
                                    <i class="fas fa-hand-holding-usd"></i> Abonar
                                </a>
                                <span class="text-success" v-else>
                                <i class="fa fa-check" ></i> Pagada
                            </span>
                            </div>
                            <!-- /.card-tools -->
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="row">

                                <div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    @include('compras.pagos.tabla')
                                </div>
                            </div>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
        </div>
        <!-- /.container-fluid -->

        @include('compras.pagos.edit_modal')
        @include('compras.pagos.create_modal')
        @include('compras.pagos.confirm_delete_modal')
    </div>
    <!-- /.content -->
@endsection

