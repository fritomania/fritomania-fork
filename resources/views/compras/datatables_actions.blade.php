    @php
        $compra=\App\Models\Compra::find($id);
    @endphp

    {{--<a href="{{ route('compras.show', $id) }}" class='btn btn-info btn-xs' data-toggle="tooltip" title="Detalles">--}}
    <span data-toggle="tooltip" title="Ver detalles">
        <a href="#modal-detalles-{{$id}}" data-keyboard="true" data-toggle="modal" class='btn btn-info btn-xs' >
            <i class="fa fa-eye"></i>
        </a>
    </span>

    @if( ( Auth::user()->isAdmin()  || Auth::user()->isAdminTienda() ) && $compra->cestado_id != \App\Models\Cestado::ANULADA && $compra->cestado_id == \App\Models\Cestado::RECIBIDA )
        <span data-toggle="tooltip" title="Anular Ingreso de Compra">
        <a href="#modal-delete-{{$id}}" data-toggle="modal" class='btn btn-danger btn-xs'>
            <i class="fas fa-times"></i>
        </a>
        </span>
    @endif
    @if( ( Auth::user()->isAdmin() || Auth::user()->isAdminTienda() )&& $compra->cestado_id == \App\Models\Cestado::CREADA )
        {{--<a href="#modal-delete-{{$id}}" data-toggle="modal" class='btn btn-danger btn-xs'>--}}
            {{--<i class="far fa-trash-alt" data-toggle="tooltip" title="Eliminar Solicitud de Compra"></i>--}}
        {{--</a>--}}
        <span data-toggle="tooltip" title="Cancelar Solicitud de Compra">
        <a href="#modal-cancelar-{{$id}}" data-toggle="modal" class='btn btn-warning btn-xs'>
            <i class="fas fa-ban" ></i>
        </a>
        </span>
    @endif

    <a href="{{route('compra.pdf',$id)}}" target="_blank" class='btn btn-outline-success btn-xs' data-toggle="tooltip" title="Imprimir Orden de Compra">
        <i class="fas fa-print"></i>
    </a>

    <div class="modal fade modal-warning" id="modal-delete-{{$id}}">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Anular</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body">
                    Seguro que anular esta compra?
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                    <a href="{{route('compras.anular',['id' => $id])}}" class="btn btn-danger">
                        SI
                    </a>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    <div class="modal fade modal-warning" id="modal-cancelar-{{$id}}">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Cancelar Solicitud</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body">
                    Seguro que cancelar la solicitud de compra?
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                    <a href="{{route('compras.cancelar.solicitud',['id' => $id])}}" class="btn btn-warning">
                        SI
                    </a>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    <div class="modal fade" id="modal-detalles-{{$id}}" tabindex='-1'>
        <div class="modal-dialog modal-lg">
            <div class="modal-content " style="color: #0A0A0A">
                <div class="modal-header">
                    <h5 class="modal-title">Detalles del ingreso</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-sm">
                            @include('compras.show_fields',['compra'=>$compra])
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            @include('compras.tabla_detalles',['compra'=>$compra])
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    @if($compra->cestado_id == \App\Models\Cestado::CREADA)
                        <a href="{{route('compra.ingreso', $id)}}" ><div class="btn btn-outline-success" >Ingresar</div></a>
                    @else
                        <h4><span class="badge badge-info">{{ $compra->cestado->descripcion }}</span></h4>
                    @endif
                    
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->