{{--<a href="{{ route('compras.show', $id) }}" class='btn btn-info btn-xs' data-toggle="tooltip" title="Detalles">--}}
<a href="{{route('compras.abonar',$id)}}" class='btn btn-info btn-xs' data-toggle="tooltip" title="Ver detalles">
    <i class="fa fa-eye"></i>
</a>

<a href="#modal-delete-{{$id}}" data-toggle="modal" class='btn btn-danger btn-xs'>
    <i class="fa fa-trash-alt" data-toggle="tooltip" title="Anular compra"></i>
</a>

<div class="modal fade modal-warning" id="modal-delete-{{$id}}">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Anular</h5>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <div class="modal-body">
                Seguro que anular esta compra?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                <a href="{{route('compras.anular',['id' => $id])}}" class="btn btn-danger">
                    SI
                </a>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
