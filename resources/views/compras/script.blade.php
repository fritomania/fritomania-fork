@push('scripts')
<!--    Scripts compras
------------------------------------------------->
<script >
    vm = new Vue({
        el: '#root',
        created: function() {
            this.getDets();
        },
        data: {
            detalles: [],
            nuevoDetalle: {
                temp_compra_id: '{{$tempCompra->id}}',
                item_id: '',
                cantidad: '1',
                fecha_ven: '',
                precio: '',
            },
            detalleEdita: {
                id: '',
                temp_compra_id: '',
                item_id: '',
                cantidad: '',
                fecha_ven: '',
                precio: '',
            },
            loadingBtnUpdateDet: false,
            loadingBtnAdd: false,
            idEliminando: '',
            tipoComprobante: '2',
            recibido:0,
            picked:'F',
            credito: false,
            abono_ini: null,
        },
        methods: {
            fechaFuturo: function(diasFuturo){
                var f = new Date();
                var dias = diasFuturo;
                f.setDate(f.getDate() + dias);
                var fecha = ((f.getDate()) + "/" + (f.getMonth() +1) + "/" + f.getFullYear());
                return fecha;
            },
            numf: function(numero){
                return numf(numero)
            },
            getDets: function(page) {
                var urlKeeps = '{{route('api.temp.compra.detalles',$tempCompra->id)}}';
                axios.get(urlKeeps).then(response => {
                    this.detalles = response.data.data;
                    this.loadingBtnAdd= false;
                    this.idEliminando = '';
                });
            },
            createDet: function() {
                this.loadingBtnAdd= true;
                var url= '{{route("api.temp_compra_detalles.store")}}';

                axios.post(url, this.nuevoDetalle ).then(response => {
                    this.getDets();

                    this.nuevoDetalle.item_id = '';
                    this.nuevoDetalle.cantidad = '1';
                    this.nuevoDetalle.precio = '';

                    toastr.success(response.data.message); //mensaje
                    slc2item.empty().trigger('change');
                    slc2item.select2('open');

                }).catch(error => {
                    toastr.error(error.response.data.message);
                    this.loadingBtnAdd= false;
                });
            },
            editDet: function(det) {
                this.detalleEdita.id = det.id;
                this.detalleEdita.temp_compra_id = det.temp_compra_id;
                this.detalleEdita.item_id = det.item_id;
                this.detalleEdita.cantidad = det.cantidad;
                this.detalleEdita.precio = det.precio;

                $('#modalEditDetalle').modal('show');
            },
            updateDet: function(id) {
                this.loadingBtnUpdateDet= true;
                var url = '{{url('api/temp_compra_detalles')}}' + '/' + id;

                axios.put(url, this.detalleEdita).then(response => {
                    this.getDets();
                    $('#modalEditDetalle').modal('hide');
                    toastr.success(response.data.message);
                    this.loadingBtnUpdateDet= false;
                }).catch(error => {
                    toastr.error(error.response.data.message);
                    this.loadingBtnUpdateDet= false;
                });
            },
            deleteDet: function(det) {
                this.idEliminando = det.id;
                var url = '{{url('api/temp_compra_detalles')}}' + '/' + det.id;

                axios.delete(url).then(response => {
                    this.getDets();
                    toastr.success(response.data.message);
                });
            },
            procesar: function () {
                if(this.totalitems>=1){
                    $('#modal-confirma-procesar').modal('show');
                }else {
                    toastr.error('No hay ningún artículo en este ingreso')
                }
            }
        },
        computed: {
            total: function () {
                var t=0;
                $.each(this.detalles,function (i,det) {
                    t+=(det.cantidad*det.precio);
                });

                return t;
            },
            totalitems: function () {
                var t=0;
                $.each(this.detalles,function (i,det) {
                    t+=(det.cantidad*1);
                });

                return t;
            },
            vuelto: function () {
                var t=this.total, r=this.recibido, v=0;

                v=r-t;//Vuelto es igual a recibido menos total

                return v<1 ? 0 : v.toFixed(0);
            }
        }
    });

    $(function () {
        function formatStateItems (state) {

            var imagen= state.imagen==undefined ? 'img/avatar_none.png' : state.imagen;

            var color_stock_less = (state.stock<=0) ? 'color_stock_less' : '';

            var $state = $(
                "@component('components.format_slc2_item')"+
                "@slot('imagen')"+imagen+ "@endslot"+
                "@slot('nombre')"+state.nombre+ "@endslot"+
                "@slot('marca')"+state.nombre_marca+ "@endslot"+
                "@slot('descripcion')"+ state.descripcion+"@endslot"+
                "@slot('precio')"+numf(state.precio_compra)+"@endslot"+
                "@slot('ubicacion')"+state.ubicacion+"@endslot"+
                "@slot('stock')"+numf(state.stock_tienda)+"@endslot"+
                "@slot('codigo')"+state.codigo+"@endslot"+
                "@slot('color_stock_less')"+color_stock_less+"@endslot"+
                "@endcomponent"
            );

            return $state;
        };

        slc2item=$("#items").select2({
            language : 'es',
            maximumSelectionLength: 1,
            placeholder: "Ingrese código,nombre o descripción para la búsqueda",
            delay: 250,
            ajax: {
                url: "{{ route('api.items.index') }}",
                dataType: 'json',
                data: function (params) {
                    return {
                        search: params.term,
                        tienda: '{{session('tienda')}}'
                    };
                },
                processResults: function (data, params) {
                    //recorre todos los item q
                    var data = $.map(data.data, function (item) {

                        //recorre los atributos del item
                        $.each(item,function (index,valor) {
                            //Si no existe valor se asigan un '-' al attributo
                            item[index] = !valor ? '-' : valor;
                        });

                        return item;
                    });

                    return {
                        results: data,
                    };
                },
                cache: true
            },
            minimumInputLength: 1,
            templateResult: formatStateItems,
            templateSelection: function(data,contenedor) {

                $("#div-info-item").html(
                    "@component('components.items.show_bar')"+
                    "@slot('imagen')"+data.imagen+ "@endslot"+
                    "@slot('nombre')"+ data.nombre+"@endslot"+
                    "@slot('marca')"+ data.nombre_marca+"@endslot"+
                    "@slot('descripcion')"+ data.descripcion+"@endslot"+
                    "@slot('precio')"+numf(data.precio_venta)+"@endslot"+
                    "@slot('ubicacion')"+data.ubicacion+"@endslot"+
                    "@slot('stock')"+numf(data.stock_tienda)+"@endslot"+
                    "@slot('precio_mayoreo')"+ numf(data.precio_mayoreo)+"@endslot"+
                    "@slot('cantidad_mayoreo')"+ numf(data.cantidad_mayoreo)+"@endslot"+
                    "@slot('um')"+ data.um+"@endslot"+
                    "@endcomponent"
                );

                vm.nuevoDetalle.item_id = data.id;
                vm.nuevoDetalle.precio = data.precio_compra;

                if(data.stock<=0){
                    $('.select2-result__stock').css('color','#d5404b');
                }

                return data.nombre;
            }
        })
            .on('select2:select',function (e) {
            $('#cant-new-det').focus().select();
        }).on('select2:unselecting',function (e) {
            $("#div-info-item").html('');
        });

        $("#fv-new-det,#precio-new-det,#cant-new-det").keypress(function (e) {
            if (e.keyCode == 13) {
                e.preventDefault();
                $("#btn-agregar").focus();
            }
        }).focus(function () {
            $(this).select()
        });

        $("#recibido").keypress(function (e) {
            console.log(e);
            if (e.keyCode == 13) {
                e.preventDefault();
                $("#btn-procesar").focus();
            }
        });

        // $("#fecha").datetimepicker({
        //     format: 'DD/MM/YYYY',
        // });
        //
        // $("#fecha_limite_credito").datetimepicker({
        //     format: 'DD/MM/YYYY',
        // });

        $('#clientes').select2({
            language: 'es',
            maximumSelectionLength: 1,
            allowClear: true
        });

        $("#proveedores").select2({
            language: "es",
            maximumSelectionLength: 1,
            allowClear: true
        });

        $("#btn-confirma-procesar").click(function () {
            $(this).button('loading');
        });

    })
</script>
@endpush