<ul class="list-group">
    <li class="list-group-item pb-0 pl-2 pr-2">
        <div class="form-group col-sm-12">
            <label for="proveedores" class="control-label">
                Proveedor:
                <a  data-toggle="modal" href="#modal-form-proveedores" tabindex="1000">Nuevo</a>
            </label>
            {!! Form::select('proveedor_id', $proveedores , 1,['class' => 'form-control', 'id'=>'proveedores','multiple'=>"multiple",'style' => "width: 100%"]) !!}
        </div>
    </li>
    <li class="list-group-item pb-0 pl-2 pr-2">
        <div class="form-group col-sm-12">
            {!! Form::label('tcomprobante', 'Tipo:') !!}
            {!! Form::select('tcomprobante_id', $tiposComprobantes , 2,['class' => 'form-control', 'id'=>'tcomprobantes','v-model'=>'tipoComprobante']) !!}
        </div>
    </li>
    <li class="list-group-item pb-0 pl-2 pr-2" v-show="tipoComprobante=='2'">
        <div class="form-group col-sm-12">
            <div class="input-group ">
                {{--<div class="input-group-prepend">--}}
                {{--<span class="input-group-text">S</span>--}}
                {{--</div>--}}
                {{--{!! Form::text('serie', null, ['class' => 'form-control','placeholder'=>'Serie']) !!}--}}
                <div class="input-group-prepend">
                    <span class="input-group-text">F</span>
                </div>
                {!! Form::text('numero', null, ['class' => 'form-control','placeholder'=>'Folio']) !!}
            </div>

        </div>
    </li>
    <li class="list-group-item pb-0 pl-2 pr-2">

        <div class="form-group col-sm-12">
            {!! Form::label('fecha', 'Fecha Documento:') !!}
            {!! Form::date('fecha', null, ['class' => 'form-control']) !!}
        </div>
    </li>

    <li class="list-group-item pb-0 pl-2 pr-2">

        <div class="form-group col-sm-12">
            {!! Form::label('fecha_ingreso_plan', 'Fecha ingreso a bodega:') !!}
            {!! Form::date('fecha_ingreso_plan', null, ['class' => 'form-control']) !!}
        </div>
    </li>

    <li class="list-group-item pb-0 pl-2 pr-2 text-bold ">
        Credito
        <input type="hidden" name="credito" :value="credito ? 1 : 0">
        <span class="float-right">
             <toggle-button v-model="credito"
                            :sync="true"
                            :labels="{checked: 'SI', unchecked: 'NO'}"
                            :height="30"
                            :width="60"
                            :value="false"
             />
        </span>

    </li>
    <li class="list-group-item p-2 pb-0 pl-2 pr-2" v-show="credito">

        <div class="form-group col-sm-12">
            {!! Form::label('fecha_limite_credito', 'Fecha Pago:') !!}
            <input id="fecha_limite_credito" placeholder="Fecha limite de Crédito" name="fecha_limite_credito" type="date" value="{{ hoyDb() }}" class="form-control">
        </div>
    </li>
    <li class="list-group-item p-2 pb-0 pl-2 pr-2" v-show="credito">
        <input type="text" name="monto_ini" id="" class="form-control" title="Abono inicial" placeholder="Abono inicial (Opcional)" v-model="abono_ini">
    </li>
    <li class="list-group-item pb-0 pl-2 pr-2">
        <div class="form-group col-sm-12">
            <input type="hidden" name="user_id" value="{{Auth::user()->id}}">
            <input type="hidden" name="tienda_id" value="{{session('tienda')}}">
            <button type="button"  class="btn btn-outline-success" @click="procesar()">
                <span class="glyphicon glyphicon-ok"></span> Procesar
            </button>

            <a class="btn btn-outline-danger pull-right" data-toggle="modal" href="#modal-cancel-compra">
                <span data-toggle="tooltip" title="Cancelar compra">Cancelar</span>
            </a>
        </div>
    </li>
</ul>

<!-- Modal confirm -->
<div class="modal fade modal-info" id="modal-confirma-procesar">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
                <h4 class="modal-title">PROCESAR COMPRA!</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			</div>
			<div class="modal-body">
				Seguro que desea continuar?
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">NO</button>
				<button type="submit" class="btn btn-primary" name="procesar" value="1"  id="btn-confirma-procesar" data-loading-text="<i class='fa fa-cog fa-spin fa-1x fa-fw'></i> Procesando">SI</button>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- Modal cancel -->
<div class="modal fade modal-warning" id="modal-cancel-compra">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Cancelar compra!</h4>
            </div>
            <div class="modal-body">
                Seguro que desea cancelar la compra?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">NO</button>
                <a href="{{route('compras.cancelar',['id' => $tempCompra->id])}}" class="btn btn-danger">
                    SI
                </a>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->