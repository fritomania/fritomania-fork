
<!-- Tipo Field -->
{!! Form::label('tipo', 'Tipo:') !!}
{!! $compra->tcomprobante->nombre or '' !!}
<br>

<!-- Proveedore Id Field -->
{!! Form::label('proveedor', 'Proveedor :') !!}
{!! $compra->proveedor->nombre or '' !!}
<br>

<!-- Serie Field -->
{!! Form::label('serie', 'N/S:') !!}
{!! $compra->numero !!}-{!! $compra->serie !!}

<br>

<!-- Fecha ingreso Plan Field-->
{!! Form::label('fecha_ingreso_plan', 'Fecha entrega a bodega:') !!}
{!! fecha($compra->fecha_ingreso_plan) !!}

<br>

<!-- Fecha ingreso Field-->
{!! Form::label('fecha_ingreso', 'Fecha Recepción:') !!}
{!! fecha($compra->fecha_ingreso) !!}

<br>

<!-- Fecha del documento Field -->
{!! Form::label('fecha', 'Fecha del documento:') !!}
{!! fecha($compra->fecha) !!}

<br>

<!-- Fecha Credito Field-->
{!! Form::label('fecha_limite_credito', 'Fecha de pago:') !!}
{!! fecha($compra->fecha_limite_credito) !!}

<br>

<!-- Cestado Id Field -->
{!! Form::label('cestado', 'Estado:') !!}
{!! $compra->cestado->descripcion !!}
<br>

<!-- Created At Field -->
{!! Form::label('created_at', 'Creado el:') !!}
{!! $compra->created_at->format('Y/m/d H:i:s')!!}
<br>
<!-- Updated At Field -->
{!! Form::label('updated_at', 'Actualizado el:') !!}
{!! $compra->updated_at->format('Y/m/d H:i:s')!!}
<br>
<br>

