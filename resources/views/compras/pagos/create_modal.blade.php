<!-- Modal cpagos -->
<div class="modal fade" id="modalCreateCpago">
    <div class="modal-dialog ">
        <div class="modal-content">
            <form action="" @submit.prevent="createCpago" method="post" role="form" id="form-modal-cpagos">

                <div class="modal-header">
                    <h5 class="modal-title">Nuevo abono</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <!-- Cantidad Field -->
                        <div class="form-group col-sm-12">
                            <label for="cantidad">Monto</label>
                            <input v-model="nuevoCpago.monto" id="cantidad" class="form-control">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="reset" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    <button type="submit" data-loading-text="Loading..." class="btn btn-success" autocomplete="off" :disabled="loading">
                        <i  v-show="loading" class="fa fa-spinner fa-spin"></i> Guardar
                    </button>
                </div>

            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
<!-- /. Modal cpagos -->