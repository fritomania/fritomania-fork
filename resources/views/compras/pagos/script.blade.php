@push('scripts')
<!--    Scripts abonos compra credito
------------------------------------------------->
<script >

    var vmCpago = new Vue({
            el: '#root',
            created: function() {
                this.getCpagos();
            },
            data: {
                totalCompra: '{{number_format($compra->total,2,'.','')}}',
                cpagos: [],
                nuevoCpago: {
                    'compra_id': '{{$compra->id}}',
                    'monto': '',
                    'user_id': '{{Auth::user()->id}}'
                },
                cpagoEdit: {},
                cpagoElimina: {},
                loading: false,
            },
            methods: {
                numf (numero){
                   return numf(numero)
                },
                getCpagos: function() {
                    var parms = {
                        params:{
                            compra_id: '{{$compra->id}}'
                        }
                    };

                    var url = "{{route('api.cpagos.index')}}";

                    axios.get(url,parms).then(response => {
                        this.cpagos = response.data.data
                    });
                },
                createCpago: function() {
                    this.loading= true;
                    var url = "{{route('api.cpagos.store')}}";

                    axios.post(url, this.nuevoCpago ).then(response => {
                        this.getCpagos();

                        this.nuevoCpago.monto = '';
                        $('#modalCreateCpago').modal('hide');
                        toastr.success(response.data.message); //mensaje
                        this.loading= false;
                    }).catch(error => {
                        toastr.error(error.response.data.message);
                        this.loading= false;
                    });
                },
                editCpago: function(cpago) {
                    this.cpagoEdit = cpago;

                    $('#modalEditCpago').modal('show');
                },
                updateCpago: function(id) {
                    this.loading= true;
                    var url = "{{url('api/cpagos')}}" + "/" + id;

                    axios.put(url, this.cpagoEdit).then(response => {
                        this.getCpagos();

                        this.cpagoEdit.id = '';
                        this.cpagoEdit.monto = '';

                        $('#modalEditCpago').modal('hide');
                        toastr.success(response.data.message);
                        this.loading= false;
                    }).catch(error => {
                        toastr.error(error.response.data.message);
                        this.loading= false;
                    });
                },
                confirmDelete: function(cpago) {
                    this.cpagoElimina = cpago;
                    $('#modalDeleteCpago').modal('show');
                },
                deleteCpago: function(cpago) {
                    var url = "{{url('api/cpagos')}}" + "/" + cpago.id;

                    axios.delete(url).then(response => { //eliminamos
                        this.getCpagos(); //listamos
                        $('#modalDeleteCpago').modal('hide');
                        toastr.success(response.data.message); //mensaje
                    });
                }
            },
            computed: {
                totalPagos : function () {
                    var t=0;
                    $.each(this.cpagos,function (i,det) {
                        t+=parseFloat(det.monto);
                    });

                    return t.toFixed(0);
                },
                saldo: function () {
                    var tv = parseFloat(this.totalCompra);
                    var tp = parseFloat(this.totalPagos);

                    return (tv-tp).toFixed(0);
                },
                saldoTexto: function () {
                    return addComas(this.saldo);
                }
            }
        });
</script>
@endpush