<div class="table-responsive">
    <table class="table table-bordered table-hover table-xtra-condensed ">
        <thead>
        <tr>
            <th>Fecha</th>
            <th>Usuario</th>
            <th>Monto</th>
            {{--<th>Saldo</th>--}}
            <th v-if="saldo>0" width="17%">-</th>
        </tr>
        </thead>
        <tbody>

        <tr v-for="(cpago,i) in cpagos" class="text-sm">
            <td>@{{cpago.created_at}}</td>
            <td>@{{cpago.username}}</td>
            <td class="text-right">{{ dvs() }} @{{cpago.monto}}</td>
            {{--<td class="text-right" :class="{'text-success text-bold' : (i+1 == cpagos.length)}">{{ dvs() }} @{{saldo(cpago.monto)}}</td>--}}
            <td width="10%" v-if="saldo>0">
                @if(Auth::user()->isAdmin())
                    <a href="#" class="btn btn-info btn-xs" @click.prevent="editCpago(cpago)">
                        <i class="fa fa-edit"></i>
                    </a>
                    <a href="#" class='btn btn-danger btn-xs' @click.prevent="confirmDelete(cpago)">
                        <i class="fa fa-trash-alt"></i>
                    </a>
                @endif
            </td>
        </tr>
        <tr v-show="cpagos.length == 0">
            <td colspan="5" class="text-center">Ningún pago agregado</td>
        </tr>
        <tr>
            <th colspan="2">Total Pagos</th>
            <th class="text-right">{{ dvs() }} @{{ totalPagos }}</th>
            <th v-if="saldo>0"></th>
        </tr>
        </tbody>
    </table>
</div>

<h2  class="pull-right">
    <span class="badge badge-success" >Saldo {{ dvs() }} @{{ saldoTexto }}</span>
</h2>
