<!-- Id Field -->
{!! Form::label('id', 'Id:') !!}
{!! $repartidore->id !!}<br>


<!-- Nombres Field -->
{!! Form::label('nombres', 'Nombres:') !!}
{!! $repartidore->nombres !!}<br>


<!-- Apellidos Field -->
{!! Form::label('apellidos', 'Apellidos:') !!}
{!! $repartidore->apellidos !!}<br>


<!-- Hora Inicia Field -->
{!! Form::label('hora_inicia', 'Hora Inicia:') !!}
{!! $repartidore->hora_inicia !!}<br>


<!-- Hora Fin Field -->
{!! Form::label('hora_fin', 'Hora Fin:') !!}
{!! $repartidore->hora_fin !!}<br>


<!-- User Id Field -->
{!! Form::label('user_id', 'User Id:') !!}
{!! $repartidore->user_id !!}<br>


<!-- Created At Field -->
{!! Form::label('created_at', 'Creado el:') !!}
{!! $repartidore->created_at->format('Y/m/d H:i:s')!!}<br>


<!-- Updated At Field -->
{!! Form::label('updated_at', 'Actualizado el:') !!}
{!! $repartidore->updated_at->format('Y/m/d H:i:s')!!}<br>


<!-- Deleted At Field -->
{!! Form::label('deleted_at', 'Borrado el:') !!}
{!! $repartidore->deleted_at !!}<br>


