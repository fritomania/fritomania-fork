<!-- Nombres Field -->
<div class="form-group col-sm-6">
    {!! Form::label('nombres', 'Nombres:') !!}
    {!! Form::text('nombres', null, ['class' => 'form-control']) !!}
</div>

<!-- Apellidos Field -->
<div class="form-group col-sm-6">
    {!! Form::label('apellidos', 'Apellidos:') !!}
    {!! Form::text('apellidos', null, ['class' => 'form-control']) !!}
</div>

<!-- Hora Inicia Field -->
<div class="form-group col-sm-6">
    {!! Form::label('hora_inicia', 'Hora Inicia:') !!}
    {!! Form::number('hora_inicia', null, ['class' => 'form-control']) !!}
</div>

<!-- Hora Fin Field -->
<div class="form-group col-sm-6">
    {!! Form::label('hora_fin', 'Hora Fin:') !!}
    {!! Form::number('hora_fin', null, ['class' => 'form-control']) !!}
</div>

<!-- User Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('user_id','Usuario:') !!}
    {!!
        Form::select(
            'user_id',
            \App\User::pluck('name','id')->toArray()
            , null
            , ['id'=>'users','class' => 'form-control','style'=>'width: 100%']
        )
    !!}
</div>