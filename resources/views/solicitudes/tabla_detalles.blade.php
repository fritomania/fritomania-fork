<table class="table table-bordered table-hover table-xtra-condensed">
    <thead>
    <tr>
        <th>Producto</th>
        @if(isset($despachar))
        <th>Tu Stock</th>
        @endif
        <th>Cantidad</th>
        <th class="text-right">Precio</th>
        <th class="text-right">SubTotal</th>
    </tr>
    </thead>
    <tbody>
    @php
        $total=0;
    @endphp
    @foreach($solicitud->detalles as $det)
        <tr>
            <td>{{$det->item->nombre}}</td>
            @if(isset($despachar))
                <th>{{nf($det->item->stockTienda())}}</th>
            @endif
            <td>{{nf($det->cantidad)}}</td>
            <td class="text-right">{{dvs().nfp($det->precio) }}</td>
            <td class="text-right">{{dvs().nfp($det->sub_total)}}</td>
        </tr>
    @endforeach

    </tbody>
    <tfoot>
    <tr>
        <th>
            TOTAL
        </th>
        @if(isset($despachar))
            <th>{{nf( $solicitud->detalles->sum(function ($det){ return $det->item->stockTienda(); }))}}</th>
        @endif
        <th>{{nf( $solicitud->detalles->sum('cantidad'))}}</th>
        <th></th>
        <th class="text-right">{{dvs().nfp( $solicitud->detalles->sum('sub_total') ) }} </th>
    </tr>
    </tfoot>
</table>
