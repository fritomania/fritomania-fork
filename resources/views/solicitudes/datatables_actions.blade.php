@php
    $solicitud=\App\Models\Solicitude::find($id);
@endphp


{{--<a href="{{ route('compras.show', $id) }}" class='btn btn-info btn-xs' data-toggle="tooltip" title="Detalles">--}}
<a href="#modal-detalles-{{$id}}" data-keyboard="true" data-toggle="modal" class='btn btn-info btn-xs' data-toggle="tooltip" title="Ver detalles">
    <i class="fa fa-eye"></i>
</a>

{{--<a href="{{ route('solicitudes.edit', $id) }}" class='btn btn-info btn-xs' data-toggle="tooltip" title="Editar">--}}
    {{--<i class="fa fa-edit"></i>--}}
{{--</a>--}}

@if($solicitud->solicitud_estado_id == \App\Models\SolicitudEstado::SOLICITADA)
    <span data-toggle="tooltip" title="Cancelar">
        <a href="#modal-cancelar-soicitud-{{$id}}" data-toggle="modal" data-keyboard="true" class='btn btn-warning btn-xs'>
            <i class="fa fa-ban"></i>
        </a>
    </span>

    <!-- Modal-->
    <div class="modal fade" id="modal-cancelar-soicitud-{{$id}}" tabindex="-1" role="dialog" aria-labelledby="modalLogoutLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content ">
                <div class="modal-header">
                    <h5 class="modal-title" >
                        <i class="fa fa-warning text-warning fa-2x" aria-hidden="true"></i> &nbsp;¿Cancelar Solicitud?
                    </h5>
                    <buttoan class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </buttoan>
                </div>
                <div class="modal-body">Seleccione "SI" a continuación si está seguro de Cancelar la solicitud.</div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                    {!! Form::open(['route' => ['solicitud.cancelar.solicitud', $id], 'method' => 'delete']) !!}
                    <button type="submit" class="btn btn-danger">SI</button>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
    <!-- /.modal -->
@endif

@if($solicitud->solicitud_estado_id == \App\Models\SolicitudEstado::DESPACHADA)
    <span data-toggle="tooltip" title="Anular">
        <a href="#modal-anular-soicitud-{{$id}}" data-toggle="modal" data-keyboard="true" class='btn btn-danger btn-xs'>
            <i class="fa fa-times"></i>
        </a>
    </span>

    <!-- Modal-->
    <div class="modal fade" id="modal-anular-soicitud-{{$id}}" tabindex="-1" role="dialog" aria-labelledby="modalLogoutLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content ">
                <div class="modal-header">
                    <h5 class="modal-title" >
                        <i class="fa fa-warning text-warning fa-2x" aria-hidden="true"></i> &nbsp;¿Anular Solicitud?
                    </h5>
                    <buttoan class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </buttoan>
                </div>
                <div class="modal-body">Seleccione "SI" a continuación si está seguro de Anular la solicitud.</div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                    {!! Form::open(['route' => ['solicitud.anular', $id], 'method' => 'get']) !!}
                    <button type="submit" class="btn btn-danger">SI</button>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
    <!-- /.modal -->
@endif

<!-- /.modal -->
@include('solicitudes.modal_show')