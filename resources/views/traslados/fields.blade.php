@include('layouts.plugins.select2')
{{--@include('layouts.axios')--}}
@push('css')
<style>
    .select2-container--default .select2-selection--multiple .select2-selection__choice{
        color: #0A0A0A;
    }
</style>
@endpush
<!-- Item Id Field -->
<div class="form-group col-sm-5">
    {!! Form::label('item_origen', 'Artículo origen:') !!}
    {!! Form::select('item_origen', [] ,null,['class' => 'form-control', 'id'=>'item_origen','multiple'=>"multiple",'style' => "width: 100%"]) !!}
    <span class="help-block" id="msj"></span>
</div>

<div class="form-group col-sm-1">
    {!! Form::label('cantidad_origen', 'Cantidad: ') !!}
    {!! Form::text('cantidad_origen', null, ['class' => 'form-control','id' => 'cantidad_origen']) !!}
    <input type="hidden" id="equivalencia" value="0">
</div>

<!-- Item Id Field -->
<div class="form-group col-sm-5">
    {!! Form::label('item_destino', 'Artículo destino:') !!}
    {!! Form::select('item_destino', [] ,null,['class' => 'form-control', 'id'=>'item_destino','multiple'=>"multiple",'style' => "width: 100%"]) !!}
</div>

<div class="form-group col-sm-1">
    {!! Form::label('cantidad_destino', 'Cantidad: ') !!}
    {!! Form::text('cantidad_destino', null, ['class' => 'form-control']) !!}
</div>


<!-- Submit Field -->
<div class="form-group col-sm-12">
    <button type="submit" onClick="this.form.submit(); this.disabled=true;" class="btn btn-success">Guardar</button>
    <a href="{!! route('traslados.index') !!}" class="btn btn-default">Cancel</a>
</div>

@push('scripts')
<script>
    $(function () {
        function formatStateItems (state) {

            var imagen= state.imagen==undefined ? 'img/avatar_none.png' : state.imagen;

            var color_stock_less = (state.stock<=0) ? 'color_stock_less' : '';

            var $state = $(
                "@component('components.format_slc2_item')"+
                "@slot('imagen')"+imagen+ "@endslot"+
                "@slot('nombre')"+state.nombre+ "@endslot"+
                "@slot('marca')"+state.nombre_marca+ "@endslot"+
                "@slot('descripcion')"+ state.descripcion+"@endslot"+
                "@slot('precio')"+state.precio_venta+"@endslot"+
                "@slot('ubicacion')"+state.ubicacion+"@endslot"+
                "@slot('stock')"+state.stock_tienda+"@endslot"+
                "@slot('color_stock_less')"+color_stock_less+"@endslot"+
                "@endcomponent"
            );

            return $state;
        };

        $("#item_origen,#item_destino").select2({
            language : 'es',
            maximumSelectionLength: 1,
            placeholder: "Ingrese código,nombre o descripción para la búsqueda",
            delay: 250,
            ajax: {
                url: "{{ route('api.items.index') }}",
                dataType: 'json',
                data: function (params) {
                    return {
                        search: params.term,
                        tienda: '{{session('tienda')}}'
                    };
                },
                processResults: function (data, params) {
                    //recorre todos los item q
                    var data = $.map(data.data, function (item) {

                        //recorre los atributos del item
                        $.each(item,function (index,valor) {
                            //Si no existe valor se asigan un '-' al attributo
                            item[index] = !valor ? '-' : valor;
                        });

                        return item;
                    });

                    return {
                        results: data,
                    };
                },
                cache: true
            },
            minimumInputLength: 1,
            allowClear: true,
            templateResult: formatStateItems,
            templateSelection: function(data,contenedor) {

                //si se esta editando el descuento
                @if(isset($descuento->id))
                    return '{{$descuento->item->nombre}}';
                @else
                    return data.nombre;
                @endif

            }
        });

        $('#item_origen').on('select2:select', function (evt) {

            $('#msj').text('');

            var item_origen =  parseInt(evt.target.value);

            var url = "{{url('api/equivalencias/item')}}" + "/" + item_origen;

            axios.get(url).then(response => {
//                console.log(response.data.data);
                $('#msj').text(response.data.message);
                $("#item_destino").select2("trigger", "select", {
                    data: { id: response.data.data.item_destino.id, nombre: response.data.data.item_destino.nombre }
                });
                $("#cantidad_origen").focus();
                $("#equivalencia").val(response.data.data.cantidad);

            }).catch(errors=>{
                $('#msj').text(errors.response.data.message);
            });

        });

        $("#cantidad_origen").keyup(function (e) {
            var cantidad = parseFloat($(this).val());

            cantidad = isNaN(cantidad) ? 0 : cantidad;

            var equivalencia = parseFloat($('#equivalencia').val());
            var total = cantidad* equivalencia;


            $("#cantidad_destino").val(total);
            console.log(cantidad,equivalencia,total);
        });

    });

</script>
@endpush