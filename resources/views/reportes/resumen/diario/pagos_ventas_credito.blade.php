<div class="card card-default">
    <div class="card-header with-border">
        <h3 class="card-title">Pagos ventas credito</h3>

        <div class="card-tools pull-right">
            <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
        </div>
        <!-- /.card-tools -->
    </div>
    <!-- /.card-header -->
    <div class="card-body">
        <table class="table table-bordered table-hover">
            <thead>
            <tr>
                <th>ID V</th>
                <th>Cliente</th>
                <th>Monto</th>
            </tr>
            </thead>
            <tbody>
            @foreach($vpagos as $r)
                <tr>
                    <td>{{$r->venta->id}}</td>
                    <td>{{$r->venta->cliente->full_name}}</td>
                    <td>{{ dvs() }} {{nfp($r->monto,2)}}</td>
                </tr>
            @endforeach
            </tbody>
            <thead>
            <tr>
                <th></th>
                <th>Total</th>
                <th>{{ dvs() }} {{nfp(array_sum(array_pluck($vpagos,'monto')),2)}}</th>
            </tr>
            </thead>
        </table>
    </div>
    <!-- /.card-body -->
</div>
<!-- /.card -->