@extends('layouts.app')

@include('layouts.xtra_condensed_css')
@include('layouts.plugins.datatables_reportes')


@section('htmlheader_title')
    Reporte Stock
@endsection

@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row">
                <div class="col">
                    <h1 class="m-0 text-dark">Reporte Stock</h1>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            @include('flash::message')

            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            {!! Form::open(['route' => 'rpt.stock','method' => 'get']) !!}
                            <div class="form-row">


                                <div class="form-group col-sm-4">
                                    {!! Form::label('categoria_id','Categoría:') !!}
                                    {!! Form::select('categoria_id', slc(\App\Models\Icategoria::class,'Todas'), $categoria_id, ['id'=>'categorias','class' => 'form-control']) !!}
                                </div>

                                @if(Auth::user()->isAdmin() || Auth::user()->isSuperAdmin())
                                <div class="form-group col-sm-4">
                                    {!! Form::label('lugar','Local:') !!}
                                    {!! Form::select('lugar', slc(\App\Models\Tienda::class,'Todos'), $lugar, ['id'=>'categorias','class' => 'form-control']) !!}
                                </div>
                                @endif

                                <div class="form-group col-sm-4">
                                    {!! Form::label('buscar','&nbsp;') !!}
                                    <div>
                                        <button class="btn btn-success" id="buscar" type="submit" name="buscar"  value="1">
                                            <i class="fa fa-search"></i> Consultar
                                        </button>
                                    </div>
                                </div>

                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col-md-6 -->
            </div>
            <!-- /.row -->


            <!-- /.row -->
                @if($stocks )
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card">
                                <div class="card-body">
                                    <div class="table-responsive">
                                        <table class="table table-bordered table-hover table-striped table-xtra-condensed" id="tbl-dets">
                                            <thead>
                                            <tr class="text-sm">
                                                <th>id</th>
                                                <th>Categoría</th>
                                                <th>Articulo</th>
                                                @if(Auth::user()->isAdmin() || Auth::user()->isSuperAdmin())
                                                <th>Lugar</th>
                                                @endif
                                                <th>Stock</th>
                                                <th>Precio U</th>
                                                <th>SuboTotal</th>
                                            </tr>
                                            </thead>
                                            <tbody>

                                            @foreach($stocks as $det)
                                                <tr class="text-sm  ">
                                                    <td>{{$det->id}}</td>
                                                    <td>{{$det->categoria}}</td>
                                                    <td>{{$det->nombre}}</td>
                                                    @if(Auth::user()->isAdmin() || Auth::user()->isSuperAdmin())
                                                    <td>{{$det->tienda}}</td>
                                                    @endif
                                                    <td>{{nf($det->stock)}}</td>
                                                    <td>{{ dvs().nfp($det->precio)}}</td>
                                                    <td>{{ dvs().nfp($det->sub_total)}}</td>
                                                </tr>
                                            @endforeach
                                            <tr class="text-sm">
                                                <td >Total</td>
                                                <td></td>
                                                <td></td>
                                                @if(Auth::user()->isAdmin() || Auth::user()->isSuperAdmin())
                                                <td></td>
                                                @endif
                                                <td>{{nf($stocks->sum('stock'))}}</td>
                                                <td></td>
                                                <td>{{ dvs().nfp($stocks->sum('sub_total'))}}</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <!-- /.card -->
                        </div>
                        <!-- /.col-md-6 -->
                    </div>
                @else
                    <div class="alert alert-danger">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <strong>No hay resultados para su búsqueda</strong>
                    </div>
                @endif
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /.content -->
@endsection

@push('scripts')

<!--    Scripts reporte de stock datatable
------------------------------------------------->
<script>
    $(function () {

        $('#tbl-dets').DataTable( {
            dom: 'Brtip',
            paginate: false,
            ordering: false,
            language: {
                "url": "{{asset('js/SpanishDataTables.json')}}"
            },
            buttons: [
                {extend : 'copy', 'text' : '<i class="fa fa-copy"></i> <span class="d-none d-sm-inline">Copiar</span>'},
                {extend : 'csv', 'text' : '<i class="fa fa-file-excel"></i> <span class="d-none d-sm-inline">CSV</span>'},
                {extend : 'excel', 'text' : '<i class="fa fa-file-excel"></i> <span class="d-none d-sm-inline">Excel</span>'},
                {extend : 'pdf', 'text' : '<i class="fa fa-file-pdf"></i> <span class="d-none d-sm-inline">PDF</span>'},
                {extend : 'print', 'text' : '<i class="fa fa-print"></i> <span class="d-none d-sm-inline">Imprimir</span>'},
            ],
            "order": []
        } );

    })
</script>
@endpush
