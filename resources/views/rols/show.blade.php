@extends('layouts.app')

@section('htmlheader_title')
	Rol
@endsection

@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-12">
                    <h1 class="m-0 text-dark">Rol</h1>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->


    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            @include('flash::message')

            <div class="row">
                <div class="col-lg-5">
                    <div class="card">
                        <div class="card-body">
                           @include('rols.show_fields')
                        </div>
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col-md-6 -->
                <div class="col-lg-7">
                    <div class="card card-outline card-success">
                        <div class="card-header">
                            <h3 class="card-title">Usuarios con el rol asignado</h3>
                            <!-- /.card-tools -->
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>Usuario</th>
                                    <th>Nombre</th>
                                    <th>Desasignar</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($rol->users as $us)
                                    <tr>
                                        <td>{{$us->username}}</td>
                                        <td>{{$us->name}}</td>
                                        <td>
                                            <a href="{{route('rol.detach',['user' => $us->id,'rol' => $rol->id])}}" class="btn btn-danger btn-xs">
                                                <i class="fa fa-times" aria-hidden="true"></i>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                </div>
                <!-- /.col-md-6 -->
            </div>
            <!-- /.row -->

            <div class="row">
                <div class="form-group col-sm-12">
                <a href="{!! route('rols.index') !!}" class="btn btn-default">Regresar</a>
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /.content -->
@endsection
