<!-- Id Field -->
{!! Form::label('id', 'Id:') !!}
{!! $rol->id !!}<br>


<!-- Descripcion Field -->
{!! Form::label('descripcion', 'Descripcion:') !!}
{!! $rol->descripcion !!}<br>


<!-- Created At Field -->
{!! Form::label('created_at', 'Creado el:') !!}
{!! $rol->created_at->format('Y/m/d H:i:s')!!}<br>


<!-- Updated At Field -->
{!! Form::label('updated_at', 'Actualizado el:') !!}
{!! $rol->updated_at->format('Y/m/d H:i:s')!!}<br>


<!-- Deleted At Field -->
{!! Form::label('deleted_at', 'Borrado el:') !!}
{!! $rol->deleted_at !!}<br>


