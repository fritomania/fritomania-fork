@extends('adminlte::layouts.app')

@include('layouts.bootstrap_alert_float')
@include('layouts.xtra_condensed_css')

@section('content')
    <section class="content-header">
        <h1 class="pull-left">Pedidos</h1>
        <h1 class="pull-right">
           <a class="btn btn-success pull-right" style="margin-top: -10px;margin-bottom: 5px" href="{!! route('pedidos.create') !!}">Agregar Nuevo</a>
        </h1>
    </section>
    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body">
                    @include('pedidos.table')
            </div>
        </div>
    </div>
@endsection

@push('scripts')
<script>
    $(function () {

        /**
         * Var DataTable
         */
        var dt = window.LaravelDataTables["dataTableBuilder"];

        //Cuando termine de cargar o dibujara la tabla de datos
        dt.on( 'draw.dt', function () {

            //Agrega método al botón de cada registro para ingresar
            $(".btn-ingreso-pedido").click(function () {

                var  btn = $(this);
                var idPedido = $(this).attr('data-id');
                var InOen = $(this).closest('.modal-footer').find('.ingresado-o-entregado');
                console.log('Ingresar pedido',InOen.val());

                btn.button('loading');

                $.ajax({
                    method: 'GET',
                    url: '{{url('pedidos/ingreso')}}' + '/' + idPedido,
                    dataType: 'json',
                    success: function (res) {
                        console.log('respuesta ajax:',res)

                        if(res.success){
                            bootstrap_alert(res.message,'success',3000);
                        }

                        btn.button('reset');
                        InOen.val(1);

                    },
                    error: function (res) {
                        console.log('respuesta ajax:',res.responseJSON);

                        toastr.error('<strong>Error! </strong>'+res.responseJSON.message);
                        btn.button('reset');

                    }
                })
            });

            //Agrega método al botón de cada registro para entregar
            $(".btn-entrega-pedido").click(function () {

                var  btn = $(this);
                var idPedido = $(this).attr('data-id');
                var InOen = $(this).closest('.modal-footer').find('.ingresado-o-entregado');
                console.log('Entregar pedido',InOen.val());

                btn.button('loading');

                $.ajax({
                    method: 'GET',
                    url: '{{url('pedidos/entrega')}}' + '/' + idPedido,
                    dataType: 'json',
                    success: function (res) {
                        console.log('respuesta ajax:',res)

                        if(res.success){
                            bootstrap_alert(res.message,'success',3000);
                        }

                        btn.button('reset');
                        InOen.val(1);
                        //dt.draw();

                    },
                    error: function (res) {
                        console.log('respuesta ajax:',res.responseJSON);

                        toastr.error('<strong>Error! </strong>'+res.responseJSON.message);
                        btn.button('reset');

                    }
                })
            })

            //Agrega método cuando el modal de los detalles se alla ocultado
            $('.modal-detalles').on('hidden.bs.modal', function (e) {
                var modal = $(this)
                var InOen = modal.find('.ingresado-o-entregado').val();

                console.log('Ingresado o entregado: ',InOen);

                //Si se ha ingresado o entregado un pedido
                if(InOen==1){
                    dt.draw();//recargar datatables
                }
            })
        } );

    })
</script>
@endpush

