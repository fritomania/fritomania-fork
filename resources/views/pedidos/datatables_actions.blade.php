{{--<a href="{{ route('pedidos.show', $id) }}" class='btn btn-info btn-xs' data-toggle="tooltip" title="Detalles">--}}
<a href="#modal-detalles-{{$id}}" data-keyboard="true" data-toggle="modal" class='btn btn-info btn-xs' data-toggle="tooltip" title="Ver detalles">
    <i class="glyphicon glyphicon-eye-open"></i>
</a>

<a href="#modal-delete-{{$id}}" data-toggle="modal" class='btn btn-danger btn-xs'>
    <i class="glyphicon glyphicon-remove" data-toggle="tooltip" title="Anular pedido"></i>
</a>

<div class="modal fade modal-warning" id="modal-delete-{{$id}}">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Anular</h4>
            </div>
            <div class="modal-body">
                Seguro que anular esta pedido?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                <a href="{{route('pedidos.anular',['id' => $id])}}" class="btn btn-danger">
                    SI
                </a>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade modal-detalles" id="modal-detalles-{{$id}}" tabindex='-1'>
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Detalles del pedido</h4>
            </div>
            <div class="modal-body">
                <div class="row">

                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-sm">
                        @include('pedidos.show_fields',['pedido'=>$pedido=\App\Models\Pedido::find($id)])
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        @include('pedidos.tabla_detalles',['pedido'=>$pedido])
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                @if($pedido->pestado_id==2 || $pedido->pestado_id==3)
                    <span class="text-info">
                        <span class="glyphicon glyphicon-ok"></span> Ingresado
                    </span>
                @else
                    <button type="button" data-id="{{$id}}" class="btn btn-info btn-ingreso-pedido" data-loading-text="<i class='fa fa-cog fa-spin fa-1x fa-fw'></i> Procesando">
                        <span class="glyphicon glyphicon-download"></span> Ingreso
                    </button>
                @endif

                @if($pedido->pestado_id==3)
                    <span class="text-success">
                        <span class="glyphicon glyphicon-ok"></span> Entregado
                    </span>
                @else
                    <button type="button" data-id="{{$id}}" class="btn btn-success btn-entrega-pedido" data-loading-text="<i class='fa fa-cog fa-spin fa-1x fa-fw'></i> Procesando">
                        <span class="glyphicon glyphicon-upload"></span> Entrega
                    </button>
                @endif


                    <a href="{!! route('pedidos.fpdf', $pedido->id) !!}" class="btn btn-primary pull-right" target="_blank" >
                        <i class="fa fa-print"></i> Imprimir
                    </a>

                    <input type="hidden" value="0" class="ingresado-o-entregado">
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->