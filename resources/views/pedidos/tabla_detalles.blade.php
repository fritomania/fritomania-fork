<table class="table table-bordered table-hover table-xtra-condensed">
    <thead>
    <tr>
        <th class="text-center">Producto</th>
        <th class="text-center">Precio</th>
        <th class="text-center">Cantidad</th>
        <th class="text-center">Subtotal</th>
    </tr>
    </thead>
    <tbody>
    @foreach($pedido->pedidoDetalles as $det)
        <tr class="text-sm">
            <td width="61%">{{$det->item->nombre}}</td>
            <td width="13%" class="text-right">{{ dvs() }} {{$det->precio}}</td>
            <td width="13%" class="text-right">{{$det->cantidad}}</td>
            <td width="13%" class="text-right">{{ dvs() }} {{number_format($det->cantidad*$det->precio,2)}}</td>
        </tr>
    @endforeach
    <tr class="text-right text-sm">
        <td colspan="3">Total </td>
        <td ><b>{{ dvs() }} {{number_format($pedido->total,2)}}</b></td>
    </tr>
</table>

{{--<span class="label label-success">Recibido {{ dvs() }} {{$pedido->recibido}}</span>--}}
{{--<span class="label label-default">Vuelto {{ dvs() }} {{number_format($pedido->recibido-$venta->total,2)}}</span>--}}
