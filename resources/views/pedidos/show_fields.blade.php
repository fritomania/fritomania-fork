<!-- Id Field -->
    {!! Form::label('id', 'Id:') !!}
    {!! $pedido->id !!}
<br>

<!-- Cliente Id Field -->
    {!! Form::label('cliente_id', 'Cliente:') !!}
    {!! $pedido->cliente->full_name !!}
<br>

<!-- Fecha Ingreso Field -->
    {!! Form::label('fecha_ingreso', 'Fecha Ingreso:') !!}
    {!! $pedido->fecha_ingreso !!}
<br>

<!-- Fecha Entrega Field -->
    {!! Form::label('fecha_entrega', 'Fecha Entrega:') !!}
    {!! $pedido->fecha_entrega !!}
<br>

<!-- User Id Field -->
    {!! Form::label('user_id', 'Usuario:') !!}
    {!! $pedido->user->name !!}
<br>

<!-- pestado Id Field -->
    {!! Form::label('pestado_id', 'Estado:') !!}
    {!! $pedido->pestado->descripcion !!}
<br>

<!-- Created At Field -->
    {!! Form::label('created_at', 'Creado el:') !!}
    {!! $pedido->created_at->format('Y/m/d H:i:s')!!}
<br>

<!-- Updated At Field -->
    {!! Form::label('updated_at', 'Actualizado el:') !!}
    {!! $pedido->updated_at->format('Y/m/d H:i:s')!!}
<br>

{{--<!-- Deleted At Field -->--}}
    {{--{!! Form::label('deleted_at', 'Borrado el:') !!}--}}
    {{--{!! $pedido->deleted_at !!}--}}
{{--<br>--}}

