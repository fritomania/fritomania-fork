@extends('adminlte::layouts.app')

@push('css')
@include('layouts.select2_css')
@include('layouts.plugins.datepiker_css')
@endpush

@push('scripts')
@include('layouts.select2_js')
@include('layouts.plugins.datepiker_js')
@endpush


@include('layouts.plugins.fancy_box')
@include('layouts.xtra_condensed_css')
@include('layouts.bootstrap_alert_float')

@section('content')
    <section class="content-header">
        <h1>
            Pedido <small> iniciado: {{$tempPedido->created_at}}</small>
            <small class="pull-right" ><a href="{!! route('pedidos.index') !!}">Listado de pedidos</a></small>
        </h1>

    </section>
    <div class="content">
        @include('flash::message')
        @include('adminlte-templates::common.errors')

        {!! Form::model($tempPedido, ['route' => ['pedidos.procesar', $tempPedido->id], 'method' => 'post']) !!}
        <div class="row">
            <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
                <div class="box box-warning" id="datos-new-det">
                    <div class="box-header with-border">
                        <div class="form-group">
                            <select name="item_id" id="items" class="form-control" multiple="multiple" size="10" style="width: 100%;">
                                <option value=""> -- Select One -- </option>
                            </select>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div id="div-info-item">

                        </div>
                        <div class="row">
                            <div class="form-group col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                <div class="input-group">
                                    <span class="input-group-addon">Cant</span>
                                    <input type="text" name="cantidad" id="cant-new-det"  class="form-control"  value="1" data-toggle="tooltip" title="Doble Enter para agregar">
                                </div>
                            </div>
                            <div class="form-group  col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                <div class="input-group">
                                    <span class="input-group-addon">Q</span>
                                    <input type="text" name="precio" id="precio-new-det" class="form-control" placeholder="Precio venta" data-toggle="tooltip" title="Doble Enter para agregar">
                                    <span class="input-group-btn">
                                                <button type="button" id="btn-add-det" class="btn btn-success" data-loading-text="<i class='fa fa-cog fa-spin fa-1x fa-fw'></i> Agregando" >
                                                    <span class="glyphicon glyphicon-plus"></span> Agregar
                                                </button>
                                            </span>
                                </div><!-- /input-group -->

                                <input type="hidden" name="temp_pedido_id" value="{{$tempPedido->id}}">
                                <input type="hidden" id="precio-venta-new-det" >
                                <input type="hidden" id="precio-compra-new-det" >
                                <input type="hidden" id="precio-mayoreo-new-det" >
                                <input type="hidden" id="cant-mayoreo-new-det" >
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">

                <div class="box box-primary">

                    <!-- /.box-header -->
                    <div class="box-body" style="padding: 0px;">

                        @include('pedidos.fields')


                    </div>
                </div><!-- /.row -->
            </div>
        </div>

        <div class="row">

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="box box-danger">
                <div class="box-body" style="padding: 0px;">
                    @include('components.tabla_item_detalles', ['detalles' => $tempPedido->tempPedidoDetalles])
                </div>
            </div>
        </div>
        </div>

        {!! Form::close() !!}

    </div>

    @include('ventas.modal_cliente')

@endsection

@push('scripts')
<!--*********** Scripts create ventas ***********-->
<script >
    $(function() {

        $('#flash-overlay-modal').modal();

        function formatStateItems (state) {

            var imagen= state.imagen==undefined ? 'img/avatar_none.png' : state.imagen;

            var color_stock_less = (state.stock<=0) ? 'color_stock_less' : '';

            var $state = $(
                "@component('components.format_slc2_item')"+
                "@slot('imagen')"+imagen+ "@endslot"+
                "@slot('nombre')"+state.nombre+ "@endslot"+
                "@slot('marca')"+state.nombre_marca+ "@endslot"+
                "@slot('descripcion')"+ state.descripcion+"@endslot"+
                "@slot('precio')"+state.precio_venta+"@endslot"+
                "@slot('ubicacion')"+state.ubicacion+"@endslot"+
                "@slot('stock')"+state.stock+"@endslot"+
                "@slot('color_stock_less')"+color_stock_less+"@endslot"+
                "@endcomponent"
            );

            return $state;
        };

        var slc2item=$("#items").select2({
            language : 'es',
            maximumSelectionLength: 1,
            placeholder: "Ingrese código,nombre o descripción para la búsqueda",
            delay: 250,
            ajax: {
                url: "{{ route('api.items.index') }}",
                dataType: 'json',
                data: function (params) {
                    return {
                        search: params.term
                    };
                },
                processResults: function (data, params) {
                    //recorre todos los item q
                    var data = $.map(data.data, function (item) {

                        //recorre los atributos del item
                        $.each(item,function (index,valor) {
                            //Si no existe valor se asigan un '-' al attributo
                            item[index] = !valor ? '-' : valor;
                        });

                        return item;
                    });

                    return {
                        results: data,
                    };
                },
                cache: true
            },
            minimumInputLength: 1,
            templateResult: formatStateItems,
            templateSelection: function(data,contenedor) {

                $("#div-info-item").html(
                    "@component('components.items.show_bar')"+
                    "@slot('imagen')"+data.imagen+ "@endslot"+
                    "@slot('marca')"+ data.nombre_marca+"@endslot"+
                    "@slot('descripcion')"+ data.descripcion+"@endslot"+
                    "@slot('precio')"+data.precio_venta+"@endslot"+
                    "@slot('ubicacion')"+data.ubicacion+"@endslot"+
                    "@slot('stock')"+data.stock+"@endslot"+
                    "@slot('precio_mayoreo')"+ data.precio_mayoreo+"@endslot"+
                    "@slot('cantidad_mayoreo')"+ data.cantidad_mayoreo+"@endslot"+
                    "@slot('um')"+ data.um+"@endslot"+
                    "@endcomponent"
                );

                $("#precio-new-det").val(data.precio_venta);
                $("#cant-new-det").val(1);

                $("#precio-venta-new-det").val(data.precio_venta);
                $("#precio-compra-new-det").val(data.precio_compra);
                $("#precio-mayoreo-new-det").val(data.precio_mayoreo);
                $("#cant-mayoreo-new-det").val(data.cantidad_mayoreo);

                if(data.stock<=0){
                    $('.select2-result__stock').css('color','#d5404b');
                }

                return data.nombre;
            }
        }).on('select2:unselecting',function (e) {
            $("#div-info-item").html('');
        })


        $("#btn-add-det").click(function (e) {

            e.preventDefault();

            var $btn = $(this).button('loading');
            var data = $('#datos-new-det :input').serializeArray();
            console.log('Envío de valores detalle por ajax',data);

            $.ajax({
                method: 'POST',
                url: '{{route("api.temp_pedido_detalles.store")}}',
                data: data,
                dataType: 'json',
                success: function (res) {
                    console.log('respuesta ajax:',res)

                    var det= res.data;
                    //si es item tiene marca agrega a la descripción el nombre de la misma
                    var descrip = det.item.marca ? det.item.nombre+" / "+det.item.marca.nombre : det.item.nombre;

                    if(res.success){
                        addDet(det.id,det.item_id,det.cantidad,descrip,det.precio);
                        bootstrap_alert(res.message,'success',3000);
                    }

                    $btn.button('reset');
                    slc2item.empty().trigger('change');
                    slc2item.select2('open');
                    $("#div-info-item").html('');
                },
                error: function (res) {
                    console.log('respuesta ajax:',res.responseJSON);

                    toastr.error('<strong>Error! </strong>'+res.responseJSON.message);
                    $btn.button('reset');
                }
            })
        });

        $("#tablaDetalles").DataTable({
            responsive: true,
            searching: false,
            ordering:  false,
            paginate: false,
            info: false,
        });

        function addDet(idDet,item,cantidad,descripcion,precio) {

            cantidad= parseFloat(cantidad);
            precio= parseFloat(precio);

            var fila=$("#tablaDetalle tbody tr:last");
            var subTotal=(cantidad*precio).toFixed(2);
            var subTotalTexto="{{ dvs() }} "+addComas(subTotal);
            var precioTexto= "{{ dvs() }} "+addComas(precio.toFixed(2));

            //si el total es mayor a 0 esto dice que hay algun detalle existente
            if(total()>0){

                $("#tablaDetalle tbody tr:last").clone(true).appendTo($("#tablaDetalle tbody"));

                var fila=$("#tablaDetalle tbody tr:last");
            }

            fila.find(".celda-cantidad").text(cantidad)
            fila.find(".celda-descripcion").text(descripcion);
            fila.find(".celda-precio").text(precioTexto);
            fila.find(".celda-subt").text(subTotalTexto);

            fila.find(".h-item").val(item);
            fila.find(".h-cantidad").val(cantidad);
            fila.find(".h-precio").val(precio);
            fila.find(".h-subt").val(subTotal);
            fila.find(".btnEliminaDet").val(idDet);

            $("#cant-new-det").val(1);
            $("#precio-new-det").val(0);

            total();
            totalItems();
        }

        /*	Suma cada uno de los subtotales
         ------------------------------------------------*/
        function total(){

            var Total=0,totalTexto;

            $(".h-subt").each(function() {
                var cant=parseFloat($(this).val());
                //suma del valor de cada uno de los subtotales
                if(!isNaN(cant)){
                    Total+=cant;
                }
            });

            totalTexto="{{ dvs() }} "+addComas(Total.toFixed(2));
            $("#h-total").val(Total.toFixed(2));
            $("#totalTexto").text(totalTexto);
            $("#total-pedido").text(totalTexto);

            if(Total>0){
                $("#btn-procesar").attr('disabled',false)
            }else{
                $("#btn-procesar").attr('disabled',true)
            }

            return Total;
        }

        total();

        /*	Suma cada una de las cantidades
         ------------------------------------------------*/
        function totalItems(){

            var Total=0,totalTexto;

            $(".h-cantidad").each(function() {
                var cant=parseFloat($(this).val());
                //suma del valor de cada uno de los subtotales
                if(!isNaN(cant)){
                    Total+=cant;
                }
            });

            $("#num-items").text(Total.toFixed(2));

            return Total;
        }

        totalItems();

        /*	Remueve las filas detalle validando si solo queda una fila (no se remueve, solo se borran sus campos)
         --------------------------------------------------------------------------------------------------------*/
        $.fn.removerFila=function(){
            if($('#tablaDetalle >tbody >tr').length==1){
                $(this).find(".celda-cantidad").text(0)
                $(this).find(".celda-descripcion").text('-');
                $(this).find(".celda-precio").text(0);
                $(this).find(".celda-subt").text(0);

                $(this).find(".h-item").val('');
                $(this).find(".h-cantidad").val(0);
                $(this).find(".h-precio").val(0);
                $(this).find(".h-subt").val(0);
            }else{
                $(this).remove();
            }

            total();
            totalItems();
        };

        /* 						   REMOVER DETALLES
         --------------------------------------------------------------------------------------*/
        $(".btnEliminaDet").click(function(){

            var fila=$(this).closest('tr');
            var id= parseInt($(this).val());

            console.log('eliminar detalle: ' + id)

            if(id){
                var $btn= $(this).button('loading');

                $.ajax({
                    method: 'DELETE',
                    url: '{{url('api/temp_pedido_detalles')}}' + '/' + id,
                    dataType: 'json',
                    success: function (res) {
                        ///res = JSON.parse(res);
                        console.log('respuesta ajax:',res);

                        bootstrap_alert('<strong>Listo! </strong>'+res.message,'success',5000);
                        $btn.button('reset');
                        fila.removerFila();
                    },
                    error: function (res) {
                        console.log('respuesta ajax:',res.responseJSON);

                        toastr.error('<strong>Error! </strong>'+res.responseJSON.message);
                        $btn.button('reset');
                    }
                })
            }

        });

        $("#precio-new-det,#cant-new-det").keypress(function (e) {
            if (e.keyCode == 13) {
                e.preventDefault();
                $("#btn-add-det").focus();
            }
        }).focus(function () {
            $(this).select()
        });

        $("#cant-new-det").keyup(function (e) {

            var precioVenta  = parseFloat($("#precio-venta-new-det").val());

            //si exite un precio de venta
            if(!isNaN(precioVenta)){
                var cant  = parseFloat($(this).val());

                cant = isNaN(cant) ? 0 : cant;

                var precioMayoreo  = parseFloat($("#precio-mayoreo-new-det").val());
                var cantMayoreo  = parseFloat($("#cant-mayoreo-new-det").val());

                var fieldPrecio = $("#precio-new-det");

                if(cant>=cantMayoreo){
                    fieldPrecio.val(precioMayoreo.toFixed(2));
                }else{
                    fieldPrecio.val(precioVenta.toFixed(2));
                }
            }

        });

    })
</script>
@endpush
