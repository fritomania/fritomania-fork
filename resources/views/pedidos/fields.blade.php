

@push('css')
<style>
    .panel-body{
        padding-bottom: 0px;
        padding-left: 0px;
        padding-right: 0px;
    }
    .panel {
        margin-bottom: 0px;
        border-radius: 0px;

    }
</style>
@endpush
<!-- Cliente Id Field -->
<div class="panel panel-default">
    <div class="panel-body">
        <div class="form-group col-sm-12">
            <label for="clients" class="control-label">Cliente: <a class="success" data-toggle="modal" href="#modal-form-cliente" tabindex="1000">nuevo</a></label>
            {!! Form::select('cliente_id', $clientes ,1,['class' => 'form-control', 'id'=>'clientes','multiple'=>"multiple",'style' => "width: 100%"]) !!}
        </div>
    </div>
</div>

<div class="panel panel-default">
    <div class="panel-body">
        <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
            <h4>No. Productos:</h4>
        </div>
        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 pull-right" >
            <h4 class="text-right" id="num-items">0</h4>
        </div>
    </div>
</div>


<div class="panel panel-default">
    <div class="panel-body">
        <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
            <h4>Total</h4>
        </div>
        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 pull-right" >
            <h4 class="text-right" id="total-pedido">{{ dvs() }} 0.00</h4>
            <input type="hidden" id="h-total" value="0">
        </div>
    </div>
</div>

<div class="panel panel-default">
    <div class="panel-body">
        <div class="form-group col-sm-12 text-center">
            <button type="submit" id="btn-procesar" class="btn btn-success" disabled data-loading-text="<i class='fa fa-cog fa-spin fa-1x fa-fw'></i> Procesando">
                <span class="glyphicon glyphicon-ok" ></span> Procesar
            </button>
            <a class="btn btn-danger" data-toggle="modal" href="#modal-cancel-pedido">
                <span data-toggle="tooltip" title="Cancelar pedido">Cancelar</span>
            </a>
            <input type="hidden" name="user_id" value="{{Auth::user()->id}}">
            <input type="hidden" name="pestado_id" value="1">
        </div>
    </div>
</div>

<div class="modal fade modal-warning" id="modal-cancel-pedido">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Cancelar pedido</h4>
            </div>
            <div class="modal-body">
                Seguro que desea cancelar la pedido?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">NO</button>
                <a href="{{route('pedidos.cancelar',['id' => $tempPedido->id])}}" class="btn btn-danger">
                    SI
                </a>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


@push('scripts')
<script>

    $('#clientes').select2({
        language: 'es',
        maximumSelectionLength: 1,
        allowClear: true
    });

</script>
@endpush


