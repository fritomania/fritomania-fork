<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $cretiro->id !!}</p>
</div>

<!-- Caja Id Field -->
<div class="form-group">
    {!! Form::label('caja_id', 'Caja Id:') !!}
    <p>{!! $cretiro->caja_id !!}</p>
</div>

<!-- Motivo Field -->
<div class="form-group">
    {!! Form::label('motivo', 'Motivo:') !!}
    <p>{!! $cretiro->motivo !!}</p>
</div>

<!-- Monto Field -->
<div class="form-group">
    {!! Form::label('monto', 'Monto:') !!}
    <p>{!! $cretiro->monto !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Creado el:') !!}
    <p>{!! $cretiro->created_at->format('Y/m/d H:i:s')!!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Actualizado el:') !!}
    <p>{!! $cretiro->updated_at->format('Y/m/d H:i:s')!!}</p>
</div>

<!-- Deleted At Field -->
<div class="form-group">
    {!! Form::label('deleted_at', 'Borrado el:') !!}
    <p>{!! $cretiro->deleted_at !!}</p>
</div>

