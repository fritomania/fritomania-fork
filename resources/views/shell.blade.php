<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Shell</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css"
          integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
</head>
<body>

    <div class="container">

        <h1>Shell script SO: {{(strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') ? 'Windows' : 'Linux'}}</h1>

        <form action="{{route('shell')}}" method="get">

            <div class="row">
                <div class="form-group col-sm-6">
                    {!! Form::label('comando', 'Comand:') !!}
                    {!! Form::text('comando', null, ['class' => 'form-control']) !!}
                </div>

                <div class="offset-sm-2 col-sm-10">
                    <button type="submit" class="btn btn-primary">Ejecutar</button>
                </div>

                <br>
            </div>
        </form>

        <br>
        <div class="row">
          <div class="form-group  col-sm-6">
            <div class="card">
              <div class="card-body">
                <h4 class="card-title">Resultado:</h4>
                  <p class="card-text">
                      {{$comando or ''}}:
                  </p>
                  <p class="card-text">
                      <pre>
                        {{$out or ''}}
                      </pre>
                  </p>
              </div>
            </div>
          </div>
        </div>
    </div>

<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js"
        integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>
</body>
</html>