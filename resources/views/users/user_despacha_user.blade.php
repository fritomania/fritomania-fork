@extends('layouts.app')

@section('htmlheader_title')
    Asignar Usuario para despacho
@endsection

@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row">
                <div class="col">
                    <h1 class="m-0 text-dark">Asignar Usuario para despacho</h1>
                </div><!-- /.col -->
                <div class="col">
                    <a class="btn btn-outline-info float-right" href="{!! route('home') !!}">
                        <i class="fa fa-list"></i>
                        <span class="d-none d-sm-inline">Listado</span>
                    </a>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content" id="app">
        <div class="container-fluid">
            @include('flash::message')


            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="form-group col-sm-6">
                                <v-select v-model="usuario" :options="{{json_encode($usuarios->toArray())}}" placeholder="Seleccione un Usuario.." ></v-select>
                            </div>
                        </div>
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col-md-6 -->
            </div>
            <!-- /.row -->

            <div class="row" v-show="usuario">
                <div class="col ">
                    <div class="card card-outline card-success">
                        <div class="card-header">
                            <h3 class="card-title">Usuarios a los que puede despachar</h3>

                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                </button>
                            </div>
                            <!-- /.card-tools -->
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body p-0">
                            <table class="table table-sm table-striped table-hover">
                                <thead>
                                <tr>
                                    <th>Username</th>
                                    <th>Nombre</th>
                                    <th class="text-center">Accion</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr v-if="asignados.length == 0">
                                        <td colspan="5" class="text-center">Ningún Registro</td>
                                    </tr>
                                <tr v-for="usuario in asignados">
                                    <td v-text="usuario.username"></td>
                                    <td v-text="usuario.name"></td>
                                    <td align="center" >
                                        <button class="btn btn-outline-danger btn-xs" @click="detach(usuario)" :id="usuario.id" :disabled="(usuario.id===idRemoving)">
                                            <i v-show="(usuario.id===idRemoving)" class="fa fa-spinner fa-spin"></i>
                                            <i v-show="!(usuario.id===idRemoving)" class="fa fa-trash-alt" aria-hidden="true"></i>
                                        </button>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                </div>
                <div class="col">
                    <div class="card card-outline card-success">
                        <div class="card-header">
                            <h3 class="card-title">Usuarios para asignar</h3>

                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                </button>
                            </div>
                            <!-- /.card-tools -->
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body p-0">
                            <table class="table table-sm table-striped table-hover">
                                <thead>
                                <tr>
                                    <th>Username</th>
                                    <th>Nombre</th>
                                    <th class="text-center">Accion</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr v-if="no_asignados.length == 0">
                                    <td colspan="5" class="text-center">Ningún Registro</td>
                                </tr>
                                <tr v-for="usuario in no_asignados">
                                    <td v-text="usuario.username"></td>
                                    <td v-text="usuario.name"></td>
                                    <td align="center">
                                        <button class="btn btn-outline-success btn-xs" @click.prevent="attache(usuario)" :disabled="(usuario.id===idAdding)">
                                            <i v-show="(usuario.id===idAdding)" class="fa fa-spinner fa-spin"></i>
                                            <i v-show="!(usuario.id===idAdding)" class="fa fa-check" aria-hidden="true"></i>
                                        </button>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /.content -->
@endsection
@push('scripts')
    <script>

        new Vue({
            el: '#app',
            data: {
                usuario: null,
                asignados: [],
                no_asignados: [],
                url: '{{ route('api.users.index') }}',
                urlattache: '{{ route('api.user.despacha.user.attache') }}',
                urldetach: '{{ route('api.user.despacha.user.detach') }}',
                idAdding: false,
                idRemoving: false

            },
            mounted () {

            },
            methods: {

                attache(usuario){
                    this.idAdding= usuario.id;
                    console.log('attache')
                    axios.get(this.urlattache,{
                        params:{
                            'user_id': this.usuario.id,
                            'user_atach': usuario.id,
                        }}).then(respose=>{
                        this.getdatos();
                        // console.log(respose.data);
                        toastr.success(respose.data.message);

                    }).catch(error=>{
                        console.log(error.response)
                    })
                },
                detach(usuario){
                    this.idRemoving = usuario.id;
                    console.log('detach')
                    axios.get(this.urldetach,{
                        params:{
                            'user_id': this.usuario.id,
                            'user_detach': usuario.id,
                        }}).then(respose=>{
                        this.getdatos();
                        // console.log(respose.data);
                        toastr.success(respose.data.message);

                    }).catch(error=>{
                        // console.log(error.response)
                    })
                },
                getdatos(){

                    console.log('getdatos');
                    axios.get(this.url,{
                        params: {
                            'user_id': this.usuario.id,
                            'flag': 'user_despacha_user'
                        }}).then(response=>{
                                // console.log(response.data.data);
                                this.asignados = response.data.data.asignados;
                                this.no_asignados = response.data.data.no_asignados;
                                this.idAdding= false;
                                this.idRemoving = false;
                    }).catch(error=>{
                        // console.log(error)
                    })
                }

            },
            watch: {
                usuario: function(newVal,) {

                    this.getdatos()
                }
            }
        });
    </script>
@endpush