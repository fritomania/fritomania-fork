<!-- Id Field -->
{!! Form::label('id', 'Id:') !!}
{!! $user->id !!}<br>

<!-- Username Field -->
{!! Form::label('username', 'Username:') !!}
{!! $user->username !!}<br>

<!-- Name Field -->
{!! Form::label('name', 'Name:') !!}
{!! $user->name !!}<br>

<!-- Email Field -->
{!! Form::label('email', 'Email:') !!}
{!! $user->email !!}<br>

<!-- Created At Field -->
{!! Form::label('created_at', 'Creado el:') !!}
{!! $user->created_at->format('Y/m/d H:i:s')!!}<br>

<!-- Updated At Field -->
{!! Form::label('updated_at', 'Actualizado el:') !!}
{!! $user->updated_at->format('Y/m/d H:i:s')!!}<br>

{{--<!-- Deleted At Field -->--}}
{{--{!! Form::label('deleted_at', 'Borrado el:') !!}--}}
{{--{!! $user->deleted_at !!}<br>--}}