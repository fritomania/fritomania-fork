<!-- Id Field -->
{!! Form::label('id', 'Id:') !!}
{!! $horario->id !!}<br>


<!-- Dia Field -->
{!! Form::label('dia', 'Dia:') !!}
{!! $horario->dia !!}<br>


<!-- Hora Ini Field -->
{!! Form::label('hora_ini', 'Hora Ini:') !!}
{!! $horario->hora_ini !!}<br>


<!-- Hora Fin Field -->
{!! Form::label('hora_fin', 'Hora Fin:') !!}
{!! $horario->hora_fin !!}<br>


<!-- Created At Field -->
{!! Form::label('created_at', 'Creado el:') !!}
{!! $horario->created_at->format('Y/m/d H:i:s')!!}<br>


<!-- Updated At Field -->
{!! Form::label('updated_at', 'Actualizado el:') !!}
{!! $horario->updated_at->format('Y/m/d H:i:s')!!}<br>


<!-- Deleted At Field -->
{!! Form::label('deleted_at', 'Borrado el:') !!}
{!! $horario->deleted_at !!}<br>


