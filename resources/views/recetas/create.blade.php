@extends('layouts.app')

@include('layouts.plugins.select2')

@section('htmlheader_title')
    Crear Receta
@endsection

@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row">
                <div class="col">
                    <h1 class="m-0 text-dark">
                        {{isset($receta) ? "Editar " : "Crear "}}Receta
                    </h1>
                </div><!-- /.col -->
                <div class="col ">
                    {{--<a class="btn btn-outline-info float-right"--}}
                       {{--href="{{route('recetas.index')}}">--}}
                        {{--<i class="fa fa-list" aria-hidden="true"></i>&nbsp;<span class="d-none d-sm-inline">Listado</span>--}}
                    {{--</a>--}}
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content" id="root">
        <div class="container-fluid">
            @include('flash::message')
            @include('adminlte-templates::common.errors')

            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            @isset($receta)
                                {!! Form::model($receta, ['route' => ['recetas.update', $receta->id], 'method' => 'patch','id' => 'myForm']) !!}
                                @else
                                    {!! Form::open(['route' => 'recetas.store','id' => 'myForm']) !!}
                                @endif

                            <div class="row">

                                <!--            Imagen articulo
                                ------------------------------------------------------------------------>
                                <div class="col-sm-3 border-right">
                                    <img :src="img_item" class="img-fluid" alt="">
                                </div>

                                <!--            Receta
                                ------------------------------------------------------------------------>
                                <div class="col-sm-9">

                                    <!-- Articulo a producir -->
                                    <div class="row">
                                        <div class="form-group col-sm-8">
                                            {!! Form::label('item_master','Seleccione el articulo para el cual desea la receta:') !!}

                                            <v-select
                                                    v-model="item_master"
                                                    ref="ref_item_master"
                                                    :options="{{ formatVueSelect(\App\Models\Item::productoFinal()->sinReceta()->get(),'text') }}"
                                                    placeholder="Seleccione un articulo..">

                                            </v-select>
                                            <input type="hidden" name="item_id" v-model="item_master.id">
                                        </div>
                                        <div class="form-group col-sm-4">
                                            {!! Form::label('cantidad', 'Cantidad:') !!}
                                            <div class="input-group">
                                                <input type="number" class="form-control" v-model="cantidad" name="cantidad" readonly="">
                                                <div class="input-group-append">
                                                    <span class="input-group-text" id="unidad_medida" v-text="item.unimed.nombre">Unidad</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- Card ingredientes -->
                                    <div class="card">
                                        <div class="card-header">
                                            <h3 class="card-title">Ingredientes:</h3>
                                        </div>
                                        <!-- /.card-header -->
                                        <div class="card-body pb-1">
                                            <ul v-show="!item_master">
                                                <li></li>
                                            </ul>
                                            <div v-show="item_master">
                                                <ul>
                                                    <li v-for="item in ingredientes">
                                                        <span v-text="item.cantidad"></span>&nbsp;
                                                        <span v-text="item.unimed.nombre"></span>&nbsp;
                                                        <span v-text="item.nombre"></span>
                                                        &nbsp;
                                                        <a href="#"
                                                           class="text-danger btn-delete"
                                                           @click="remove(item)">
                                                                <span class="fa fa-trash-alt"
                                                                      data-toggle="tooltip"
                                                                      title="Eliminar">

                                                                </span>
                                                        </a>
                                                    </li>
                                                </ul>
                                                <div class="row border-top border-bottom mb-1">

                                                    <div class="form-group col-sm-8">
                                                        {!! Form::label('ingrediente_select','Ingrediente:') !!}

                                                        <v-select
                                                                v-model="ingrediente_select"
                                                                ref="refingrediente"
                                                                :options="{{formatVueSelect(\App\Models\Item::where('materia_prima',1)->get(),'text')}}"
                                                                placeholder="Seleccione un articulo.." ></v-select>
                                                    </div>
                                                    <div class="form-group col-sm-4">
                                                        {!! Form::label('cantidad', 'Cantidad:') !!}
                                                        <div class="input-group">
                                                            <input class="form-control"
                                                                   id="cantidad_ingrediente"
                                                                   type="number"
                                                                   v-model="cantidad_ingrediente"
                                                                   ref="cantidad"
                                                                    v-tooltip="'Doble enter para agregar'">
                                                            <div class="input-group-append">
                                                                <span class="input-group-text" id="unidad_medida" >
                                                                    <i class="fas fa-sync-alt fa-spin" aria-hidden="true" v-show="loading_ingrediente"></i>
                                                                    <span v-text="ingrediente.unimed.nombre" v-show="!loading_ingrediente"></span>
                                                                </span>
                                                                <button id="btn-add-ingrediente"
                                                                        class="btn btn-outline-secondary"
                                                                        type="button"
                                                                        v-tooltip="'Agregar Ingrediente'"
                                                                        @click="add()"
                                                                        :disabled="!ingrediente.id && cantidad_ingrediente<=0">

                                                                    <i class="fa fa-plus" aria-hidden="true" v-show="!loading_add"></i>
                                                                    <i class="fas fa-sync-alt fa-spin" v-show="loading_add"></i>
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- /.card-body -->
                                        <div class="overlay" v-show="loading">
                                            <i class="fas fa-sync-alt fa-spin"></i>
                                        </div>
                                    </div>

                                    <!-- Submit Field -->
                                    <div class="row mt-3">
                                        <div class="form-group col-sm-12">
                                            <button type="submit" @click.prevent="guardar()" class="btn btn-outline-success">
                                                {{isset($receta) ? "Actualizar" : "Guardar"}}
                                            </button>
                                            <a href="{!! route('receta.cancelar') !!}" class="btn btn-outline-default">Cancelar</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col-md-6 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /.content -->
@endsection
@push('scripts')
<script>
    new Vue({
        el: '#root',
        mounted() {
            this.getReceta();
            // console.log('Instancia vue montada');
        },
        created() {
            console.log('Instancia vue creada');
        },
        data: {
            'item_master' : '',
            'item' : {
                'unimed' : {}
            },
            'cantidad': 1,
            'img_item' : "{{asset('img/avatar_none.png')}}",
            'ingredientes' : [],
            'ingrediente_select': '',
            'ingrediente' : {
                'unimed' : {}
            },
            'cantidad_ingrediente':1,
            'loading': false,
            'loading_ingrediente': false,
            'loading_add': false,

        },
        methods: {
            getReceta(){
              var url = "{{route("get.receta.session")}}";

              axios.get(url).then(response => {
//                  console.log(response.data.data);
                  var receta = response.data.data

                  //Si no hay combo en sesión
                  if(receta.length==0){
                      console.log('no hay receta en sesion');
                      //Hace foco en el select de los items de tipo receta
                      this.$refs.ref_item_master.$refs.search.focus();
                  }else {
                      this.ingredientes = receta.detalles;
                      this.item_master = {
                          id: receta.item.id,
                          label: receta.item.text,
                      };
                  }


              })
              .catch(error => {
                  // console.log(error.response.data);
              });  
            },
            getInfoItem(){
                console.log('Get Datos Item Select');

                this.loading= true;

                var url = "{{route("api.items.index")}}"+'/'+this.item_master.id;

                axios.get(url).then(response => {
                    // console.log(response.data);
                    this.item = response.data.data;
                    this.img_item = this.item.img_full_path;

                    // toastr.success(response.data.message);
                    this.loading= false;
                })
                .catch(error => {
//                    console.log(error.response.data);
                    // toastr.error(error.response.data.message);
                    this.loading= false;
                });
            },
            getInfoIngrediente(){
                console.log('Get Datos Ingrediente');

                this.loading_ingrediente= true;

                var url = "{{route("api.items.index")}}"+'/'+this.ingrediente_select.id;

                axios.get(url).then(response => {
                    // console.log(response.data);
                    this.ingrediente = response.data.data;

                    // toastr.success(response.data.message);
                    this.loading_ingrediente= false;
                })
                    .catch(error => {
//                        console.log(error.response.data);
                        // toastr.error(error.response.data.message);
                        this.loading_ingrediente= false;
                    });
            },
            add(){
                this.loading_add = true;
                console.log('Agregar ingrediente');

                var url = "{{route("recetas.add.det")}}";

                var params = {
                    params:{
                        'item_select' : this.item.id,
                        'ingrediente' : this.ingrediente.id,
                        'cantidad' : this.cantidad_ingrediente,
                    }
                };
                console.log ('cantidad de ingredientes antes ' + this.cantidad_ingrediente);
                if(this.cantidad_ingrediente == '' | this.cantidad_ingrediente <= 0){
                    toastr.error('Revisar el valor de la cantidad');
                }else {
                    axios.get(url,params).then(response => {
//                        console.log(response.data);

                        this.ingredientes = response.data.data;
                        toastr.success(response.data.message);
                        this.loading_add = false;
                        this.$refs.refingrediente.$refs.search.focus();
                    })
                    .catch(error => {
//                        console.log(error.response);
                        toastr.error(error.response.data.message);
                        this.loading_add = false;
                        this.$refs.refingrediente.$refs.search.focus();
                    });
                }
            },
            remove(item){
                console.log('Eliminar ingrediente', item);

                var url = "{{route("recetas.remove.det")}}";

                var params = {
                    params:{
                        'item_id' : item.id
                    }
                };
                axios.get(url,params).then(response => {
//                    console.log(response.data);

                    this.ingredientes = response.data.data;
                    toastr.success(response.data.message);
                }).catch(error => {
//                        console.log(error.response);
                    });

            },
            guardar(){
                if(this.ingredientes.length==0){
                    toastr.error('No hay ingredientes en la receta');
                    return false;
                }else{
                    document.getElementById("myForm").submit();
                }
            }
        },
        watch: {
            item_master: function (val) {
                // console.log('Cambio item');
                if(val){
                    this.getInfoItem();
                }
            },
            ingrediente_select: function (val) {
                // console.log('Cambio item');
                if(val){
                    this.getInfoIngrediente();
                    this.cantidad_ingrediente = "";
                    this.$refs.cantidad.focus();
                }
            }
        }
    });

    $(function () {
        $("#cantidad_ingrediente").keypress(function (e) {
//            console.log(e);
            if (e.keyCode == 13) {
                e.preventDefault();
                $("#btn-add-ingrediente").focus();
            }
        });
    })
</script>
@endpush

