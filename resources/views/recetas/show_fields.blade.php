<div class="row">
    <div class="col-sm-4">
        <div class="card">
            <h5 class="card-header">Producto</h5>
            <div class="card-body">
                <!-- Id Field -->
                {!! Form::label('id', 'Código:') !!}
                {!! $receta->item->codigo !!}<br>


                <!-- Item Id Field -->
                {!! Form::label('item_id', 'Producto:') !!}
                {!! $receta->item->nombre !!}<br>


                <!-- User Id Field -->
                {!! Form::label('user_id', 'Creado por:') !!}
                {!! $receta->user->name !!}<br>


                <!-- Created At Field -->
                {!! Form::label('created_at', 'Creado el:') !!}
                {!! $receta->created_at->format('Y/m/d H:i:s')!!}<br>


                <!-- Updated At Field -->
                {!! Form::label('updated_at', 'Actualizado el:') !!}
                {!! $receta->updated_at->format('Y/m/d H:i:s')!!}<br>


                <!-- Deleted At Field -->
                {{--{!! Form::label('deleted_at', 'Borrado el:') !!}--}}
                {{--{!! $receta->deleted_at !!}<br>--}}
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        <div class="card">
            <h5 class="card-header">Ingredientes</h5>
            <div class="card-body">
                {{--Tabla de ingredientes--}}
                <table class="table table-striped">
                    <thead>
                    <tr class="text-center">
                        <th scope="col">Código</th>
                        <th scope="col">Nombre</th>
                        <th scope="col">Cantidad</th>
                        <th scope="col">Unidad de Medida</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($receta->recetaDetalles as $det)
                            <tr class="text-center">
                                <td>{{$det->item->codigo}}</td>
                                <td>{{$det->item->nombre}}</td>
                                <td>{{nf($det->cantidad)}}</td>
                                <td>{{$det->item->unimed->simbolo}}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>


