<section class="heading-content">
    <div class="heading-wrapper">
        <div class="container">
            <div class="row">
                <div class="page-heading-inner heading-group">
                    <div class="breadcrumb-group">
                        <h1 class="hidden">{{$titulo}}</h1>
                        <div class="breadcrumb clearfix">
                            <span>
                                <a href="{{route('home')}}" title="Fast Food" itemprop="url">
                                    <span itemprop="title"><i class="fa fa-home"></i></span>
                                </a>
                            </span>
                            <span class="arrow-space">
                            </span>
                            <span itemscope="">
                                <a href="{{$ruta}}">
                                    <span itemprop="title">{{$titulo}}</span>
                                </a>
                            </span>
                        </div>
                        <span class="pull-right">{{$tienda ? $tienda->nombre : ''}}</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>