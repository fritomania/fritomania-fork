<!-- Id Field -->
{!! Form::label('id', 'Id:') !!}
{!! $configuration->id !!}<br>


<!-- Key Field -->
{!! Form::label('key', 'Key:') !!}
{!! $configuration->key !!}<br>


<!-- Value Field -->
{!! Form::label('value', 'Value:') !!}
{!! $configuration->value !!}<br>


<!-- Created At Field -->
{!! Form::label('created_at', 'Creado el:') !!}
{!! $configuration->created_at->format('Y/m/d H:i:s')!!}<br>


<!-- Updated At Field -->
{!! Form::label('updated_at', 'Actualizado el:  ') !!}
{!! $configuration->updated_at->format('Y/m/d H:i:s')!!}<br>


<!-- Deleted At Field -->
{!! Form::label('deleted_at', 'Borrado el:') !!}
{!! $configuration->deleted_at !!}<br>


