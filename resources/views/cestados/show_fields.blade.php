<!-- Id Field -->
{!! Form::label('id', 'Id:') !!}
{!! $cestado->id !!}<br>


<!-- Descripcion Field -->
{!! Form::label('descripcion', 'Descripcion:') !!}
{!! $cestado->descripcion !!}<br>


<!-- Created At Field -->
{!! Form::label('created_at', 'Creado el:') !!}
{!! $cestado->created_at->format('Y/m/d H:i:s')!!}<br>


<!-- Updated At Field -->
{!! Form::label('updated_at', 'Actualizado el:') !!}
{!! $cestado->updated_at->format('Y/m/d H:i:s')!!}<br>


<!-- Deleted At Field -->
{!! Form::label('deleted_at', 'Borrado el:') !!}
{!! $cestado->deleted_at !!}<br>


