@extends('layouts.app')

@section('htmlheader_title')
    Cuadre
@endsection

@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row">
                <div class="col">
                    <h1 class="m-0 text-dark">Cuadre</h1>
                </div><!-- /.col -->
                <div class="col">
                    <a class="btn btn-outline-info float-right" href="{!! route('home') !!}">
                        <i class="fa fa-list"></i>
                        <span class="d-none d-sm-inline">Listado</span>
                    </a>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            @include('flash::message')

            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                                @include('cuadres.show_fields')
                        </div>
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col-md-6 -->
            </div>
            <!-- /.row -->

            <div class="row">
                <div class="col">

                    <a href="{!! route('cuadres.index') !!}" class="btn btn-outline-secondary">Regresar</a>
                    @if(Auth::user()->isSuperAdmin())
                        <span data-toggle="tooltip" title="Eliminar">
                                        <a href="#modal-delete-{{$cuadre->id}}" data-toggle="modal" data-keyboard="true" class='btn btn-danger'>
                                            <i class="glyphicon glyphicon-remove"></i> Eliminar
                                        </a>
                                    </span>
                    @endif
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /.content -->

    <div class="modal fade modal-warning" id="modal-delete-{{$cuadre->id}}" tabindex='-1'>
        <div class="modal-dialog">
            <div class="modal-content">
                {!! Form::open(['route' => ['cuadres.destroy', $cuadre->id], 'method' => 'delete']) !!}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Eliminar!</h4>
                </div>
                <div class="modal-body">
                    Seguro desea eliminar el registro?
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                    <button type="submit" class="btn btn-danger">SI</button>
                </div>
                {!! Form::close() !!}
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
@endsection