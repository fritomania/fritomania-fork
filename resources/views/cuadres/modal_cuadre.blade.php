<div class="modal fade" id="cuadre-cash">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Cuadrar caja</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    &times;
                </button>
            </div>
            <div class="modal-body">
                <table class="table table-bordered table-hover table-sm">
                    <thead>
                    <tr>
                        <th>Denominación</th>
                        <th>Cantidad</th>
                        <th>Total</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr v-for="(item, index) in denominaciones">
                        <td>{{ dvs() }} <span v-text="nf(item.name)"></span> </td>
                        <td>
                            <input type="text" name="200" class="form-control form-control-sm denominaciones" @focus="$event.target.select()" value="" v-model="item.val" title="" required="required">
                        </td>
                        <td v-text="item.subf" class="text-right"></td>
                    </tr>
                    <tr>
                        <td colspan="2">Total suma</td>
                        <td v-text="'{{ dvs() }} ' + total" class="text-right text-bold"></td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            Total sistema efectivo
                            <span class="pull-right">
                                <button type="button" class="btn btn-sm btn-default" @click="getTotal()" v-tooltip="'Actualizar'">
                                    <span v-show="!loadingGetTotal">
                                        <i class="fa fa-sync" ></i>
                                    </span>
                                    <span v-show="loadingGetTotal">
                                        <i class="fa fa-sync fa-spin" ></i>
                                    </span>
                                </button>
                            </span>
                        </td>
                        <td v-text="'{{ dvs() }} ' +totalSistemaFormat" class="text-right text-bold"></td>
                    </tr>
                    <tr :class="{'text-success': diferencia<=0,'text-danger': diferencia>0 }">
                        <td colspan="2">
                            Diferencia
                            <span class="pull-right" v-if="diferencia<0">(Sobra)</span>
                            <span class="pull-right" v-else-if="diferencia==0">(CUADRE EXACTO)</span>
                            <span class="pull-right" v-else>(Falta)</span>
                        </td>
                        <td v-text="'{{ dvs() }} ' +diferenciaFormat" class="text-right text-bold" ></td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                @if($cuadre=\App\Models\Cuadre::deTienda()->deFecha(hoyDb())->first())
                    {{--{{dump($cuadre)}}--}}
                    <div class="alert alert-warning text-left" role="alert">
                        <strong>Ya existe un conteo de efectivo para este día: {{ dvs() }} {{nfp($cuadre->cash,2)}}</strong><br>
                        puede verlo o eliminarlo <a href="{{route('cuadres.show',$cuadre->id)}}" class="alert-link">aquí</a>.
                    </div>
                @else
                    {!! Form::open(['route' => 'cuadres.store']) !!}
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn btn-primary">Guardar</button>
                    <input type="hidden" name="fecha" value="{{hoyDb()}}">
                    <input type="hidden" name="cash" v-model="totalCash">
                    <input type="hidden" name="total_sistema" v-model="totalSistema">
                    <input type="hidden" name="tienda_id" value="{{session('tienda')}}">
                    <input type="hidden" name="user_id" value="{{Auth::user()->id}}">
                    {!! Form::close() !!}
                @endif
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

@push('scripts')
<script>
    var vmModalCuadre = new Vue({
        el: '#cuadre-cash',
        mounted: function() {
            console.log('Instancia vue modal cuader montada');
//            this.getDenominaciones();
        },
        created: function() {
            console.log('Instancia vue modal cuader creada');
            this.getDenominaciones();
            this.getTotal();
        },
        data: {
            denominaciones: [],
            totalSistema : 0,
            totalCash : 0,
            diferencia: 0,
            cajas: [],
            hoy: "{{hoyDb()}}",
            loadingGetTotal : false
        },

        methods :{
            nf(cant){
                return numf(cant);
            },
            getDenominaciones(){
                console.log('get denominaciones');
                var url = "{{route("get.denominaciones")}}";

                axios.get(url).then(response => {
                   console.log(response.data);
                    var data = response.data.data;

                    const denominaciones = [];
                    $.each(data,function (index,dn) {
//                        console.log(dn);
                        denominaciones.push({
                            'name': dn.monto
                            ,'val':0
                            ,'sub':0
                            ,'subf':0
                        });
                    });

                    this.denominaciones = denominaciones;
                })
                    .catch(error => {
                        console.log(error.response);
                        //              toastr.error(error.response.data.message);
                    });
            },
            getTotal() {
                console.log('Get Total');
                this.loadingGetTotal = true;
                var url = "{{route("get.total.dia")}}";

                axios.get(url).then(response => {
//                    console.log(response.data);
                    var total= parseFloat(response.data.data);

                    this.totalSistema = total.toFixed(this.cantidadDecimalesPrecio);

                    this.loadingGetTotal = false;

                })
                    .catch(errors => {
                        console.log(errors);
                    });

            }

        },
        computed: {
            total: function () {
                var t=0;

                $.each(this.denominaciones,function (i,e) {
                    var monto = parseFloat(e.val);
                    monto = isNaN(monto) ? 0 : monto;
                    e.sub = parseFloat(e.name)* monto;
                    e.subf = '{{ dvs() }} '+addComas(e.sub);
                    t+= e.sub;
                });

                this.totalCash = t.toFixed(this.cantidadDecimalesPrecio);

                return addComas(t.toFixed(this.cantidadDecimalesPrecio));
            },
            diferenciaFormat: function () {
                var dif =0;
                dif = this.totalSistema - this.totalCash;
                dif = dif.toFixed(this.cantidadDecimalesPrecio);

                this.diferencia = dif;

                dif = dif<0 ? dif*-1 : dif;
                return addComas(dif);
            },
            totalSistemaFormat : function () {
                var total = parseFloat(this.totalSistema);

                return numf(total.toFixed(this.cantidadDecimalesPrecio));
            },
            cantidadDecimalesPrecio: function () {
                return '{{config('app.cantidad_decimales_precio')}}';
            }

        }
    });
</script>
@endpush