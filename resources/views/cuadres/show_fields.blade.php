<!-- Id Field -->
    {!! Form::label('id', 'Id:') !!}
    {!! $cuadre->id !!}<br>

<!-- Fecha Field -->
    {!! Form::label('fecha', 'Fecha:') !!}
    {!! $cuadre->fecha !!}<br>

<!-- Total Sistema Field -->
    {!! Form::label('total_sistema', 'Total Sistema:') !!}
    {!! dvs().' '.nfp($cuadre->total_sistema) !!}<br>

<!-- Cash Field -->
    {!! Form::label('efectivo', 'Cash:') !!}
    {!! dvs().' '.nfp($cuadre->cash) !!}<br>

<!-- Diferencia Field -->
    {!! Form::label('dif', 'Diferencia:') !!}
    {!! dvs().' '.nfp($cuadre->total_sistema - $cuadre->cash) !!}<br>

<!-- Created At Field -->
    {!! Form::label('created_at', 'Creado el:') !!}
    {!! $cuadre->created_at->format('Y/m/d H:i:s')!!}<br>

<!-- Updated At Field -->
    {!! Form::label('updated_at', 'Actualizado el:') !!}
    {!! $cuadre->updated_at->format('Y/m/d H:i:s')!!}<br>

    @if($cuadre->deleted_at)
    <!-- Deleted At Field -->
    {!! Form::label('deleted_at', 'Borrado el:') !!}
    {!! $cuadre->deleted_at !!}<br>
    @endif
