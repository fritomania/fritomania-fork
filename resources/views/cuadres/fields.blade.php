<!-- Fecha Field -->
<div class="form-group col-sm-6">
    {!! Form::label('fecha', 'Fecha:') !!}
    {!! Form::date('fecha', null, ['class' => 'form-control']) !!}
</div>

<!-- Total Sistema Field -->
<div class="form-group col-sm-6">
    {!! Form::label('total_sistema', 'Total Sistema:') !!}
    {!! Form::number('total_sistema', null, ['class' => 'form-control']) !!}
</div>

<!-- Cash Field -->
<div class="form-group col-sm-6">
    {!! Form::label('efectivo', 'Cash:') !!}
    {!! Form::number('cash', null, ['class' => 'form-control']) !!}
</div>