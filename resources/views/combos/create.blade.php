@extends('layouts.app')

@include('layouts.plugins.select2')

@section('htmlheader_title')
    Crear Combo
@endsection

@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row">
                <div class="col">
                    <h1 class="m-0 text-dark">
                        Crear Combos
                    </h1>
                </div><!-- /.col -->
                <div class="col ">
                    {{--<a class="btn btn-outline-info float-right"--}}
                       {{--href="{{route('combos.index')}}">--}}
                        {{--<i class="fa fa-list" aria-hidden="true"></i>&nbsp;<span class="d-none d-sm-inline">Listado</span>--}}
                    {{--</a>--}}
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content" id="root">
        <div class="container-fluid">
            @include('flash::message')
            @include('adminlte-templates::common.errors')

            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            @isset($combo)
                                {!! Form::model($combo, ['route' => ['combos.update', $combo->id], 'method' => 'patch','id' => 'myForm']) !!}
                            @else
                                {!! Form::open(['route' => 'combos.store','id' => 'myForm']) !!}
                            @endif
                            <div class="row">

                                <!--            Imagen articulo
                                ------------------------------------------------------------------------>
                                <div class="col-sm-3 border-right">
                                    <img :src="img_item" class="img-fluid" alt="">
                                </div>

                                <!--            Receta
                                ------------------------------------------------------------------------>
                                <div class="col-sm-9">

                                    <!-- Articulo a producir -->
                                    <div class="row">
                                        <div class="form-group col-sm-6">
                                            {!! Form::label('item_selcet','Seleccione el combo a crear:') !!}

                                            <v-select
                                                    v-model="item_selcet"
                                                    ref="refitemcombo"
                                                    :options="{{ formatVueSelect(\App\Models\Item::tipoCombo()->comboNoDefinido()->get(),'text') }}"
                                                    placeholder="Seleccione un articulo.." >
                                            </v-select>
                                            <input type="hidden" name="item_id" v-model="item_selcet.id">
                                        </div>

                                        <div class="form-group col-sm-3">
                                            {!! Form::label('piezas', 'Piezas:') !!}
                                            <div class="input-group">
                                                <input type="number" class="form-control" name="piezas" min="1" max="30" required value="{{isset($combo) ? $combo->piezas : old('piezas') }}">
                                            </div>
                                        </div>

                                        <div class="form-group col-sm-3">
                                            {!! Form::label('bebidas', 'Bebidas:') !!}
                                            <div class="input-group">
                                                <input type="number" class="form-control" name="bebidas" min="1" max="5" required value="{{isset($combo) ? $combo->bebidas : old('bebidas') }}">
                                            </div>
                                        </div>
                                    </div>

                                    <!-- Card ingredientes -->
                                    <div class="card">
                                        <div class="card-header">
                                            <h3 class="card-title">Productos:</h3>
                                        </div>
                                        <!-- /.card-header -->
                                        <div class="card-body pb-1">
                                            <ul v-show="!item_selcet">
                                                <li></li>
                                            </ul>
                                            <div v-show="item_selcet">
                                                <ul>
                                                    <li v-for="item in ingredientes">
                                                        <span v-text="item.cantidad"></span>&nbsp;
                                                        <span v-text="item.unimed.nombre"></span>&nbsp;
                                                        <span v-text="item.nombre"></span>
                                                        &nbsp;
                                                        <button type="button" class="btn btn-danger btn-xs"  @click="remove(item)">
                                                                <span v-show="(idEliminando===item.id)" >
                                                                    <i  class="fa fa-sync-alt fa-spin"></i>
                                                                </span>
                                                                <span v-show="!(idEliminando===item.id)" >
                                                                    <i class="fa fa-trash-alt"></i>
                                                                </span>
                                                        </button>
                                                        <hr class="mt-1 mb-1">
                                                    </li>
                                                </ul>
                                                <div class="row border-top border-bottom mb-1">

                                                    <div class="form-group col-sm-8">
                                                        {!! Form::label('ingrediente_selcet','Productos:') !!}

                                                        <v-select
                                                                v-model="ingrediente_selcet"
                                                                ref="refingrediente"
                                                                :options="{{formatVueSelect(\App\Models\Item::productoFinal()->noCombo()->get(),'text')}}"
                                                                placeholder="Seleccione un articulo.." ></v-select>
                                                    </div>
                                                    <div class="form-group col-sm-4">
                                                        {!! Form::label('cantidad', 'Cantidad:') !!}
                                                        <div class="input-group">
                                                            <input class="form-control"
                                                                   id="cantidad_ingrediente"
                                                                   type="number"
                                                                   v-model="cantidad_ingrediente"
                                                                   ref="cantidad"
                                                                   v-tooltip="'Doble enter para agregar'"
                                                                   readonly
                                                            >
                                                            <div class="input-group-append">
                                                                <button id="btn-add-ingrediente"
                                                                        class="btn btn-outline-secondary"
                                                                        type="button"
                                                                        v-tooltip="'Agregar Ingrediente'"
                                                                        @click="add()"
                                                                        ref="buttonadd"
                                                                        :disabled="!ingrediente.id && cantidad_ingrediente<=0">

                                                                    <span v-show="!loading_add">
                                                                        <i class="fa fa-plus" aria-hidden="true" ></i>
                                                                    </span>
                                                                    <span v-show="loading_add">
                                                                        <i class="fas fa-sync-alt fa-spin" ></i>
                                                                    </span>
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- /.card-body -->
                                        <div class="overlay" v-show="loading">
                                            <i class="fas fa-sync-alt fa-spin"></i>
                                        </div>
                                    </div>

                                    <!-- Submit Field -->
                                    <div class="row mt-3">
                                        <div class="form-group col-sm-12">
                                            <button type="submit" @click.prevent="guardar()" class="btn btn-outline-success">
                                                {{isset($combo) ? "Actualizar" : "Guardar"}}
                                            </button>
                                            <a href="{!! route('combos.cancelar') !!}" class="btn btn-outline-default">Cancelar</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col-md-6 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /.content -->
@endsection
@push('scripts')
    <script>
        new Vue({
            el: '#root',
            mounted() {
                this.getCombo();
                // console.log('Instancia vue montada');
            },
            created() {
                console.log('Instancia vue creada');
            },
            data: {
                'item_selcet' : '',
                'item' : {
                    'unimed' : {}
                },
                'cantidad': 1,
                'img_item' : "{{asset('img/avatar_none.png')}}",
                'ingredientes' : [],
                'ingrediente_selcet': '',
                'ingrediente' : {
                    'unimed' : {}
                },
                'cantidad_ingrediente':1,
                'loading': false,
                'loading_add': false,
                'idEliminando' : ''

            },
            methods: {
                getCombo(){
                    console.log('getCombos');
                    var url = "{{route("get.combo.session")}}";

                    axios.get(url).then(response => {
                        console.log(response.data.data);
                        var combo = response.data.data;

                        //Si no hay combo en sesión
                        if(combo.length==0){
                            console.log('no hay combo en sesion');
                            //Hace foco en el select de los items de tipo combo
                            this.$refs.refitemcombo.$refs.search.focus();
                        }else {
                            this.ingredientes = combo.detalles;
                            this.item_selcet = {
                                id: combo.item.id,
                                label: combo.item.text,
                            };
                        }

                    })
                        .catch(error => {
                            // console.log(error.response.data);
                        });
                },
                getInfoItem(){
                    console.log('Get Datos Item Select');

                    this.loading= true;

                    var url = "{{route("api.items.index")}}"+'/'+this.item_selcet.id;

                    axios.get(url).then(response => {
                        // console.log(response.data);
                        this.item = response.data.data;
                        this.img_item = this.item.img_full_path;

                        // toastr.success(response.data.message);
                        this.loading= false;
                    })
                        .catch(error => {
//                    console.log(error.response.data);
                            // toastr.error(error.response.data.message);
                            this.loading= false;
                        });
                },
                add(){
                    this.loading_add = true;
                    console.log('Agregar ingrediente');

                    var url = "{{route("combos.add.det")}}";

                    var params = {
                        params:{
                            'item_select' : this.item.id,
                            'ingrediente' : this.ingrediente_selcet.id,
                            'cantidad' : this.cantidad_ingrediente,
                        }
                    };
                    console.log ('cantidad de ingredientes antes ' ,params);
                    axios.get(url,params).then(response => {
                        // console.log(response.data);

                        this.ingredientes = response.data.data;
                        toastr.success(response.data.message);
                        this.loading_add = false;
                        this.$refs.refingrediente.$refs.search.focus();
                    })
                    .catch(error => {
//                        console.log(error.response);
                        toastr.error(error.response.data.message);
                        this.$refs.refingrediente.$refs.search.focus();
                        this.loading_add = false;
                    });
                },
                remove(item){
                    this.idEliminando = item.id;
                    console.log('Eliminar ingrediente', item);

                    var url = "{{route("combos.remove.det")}}";

                    var params = {
                        params:{
                            'item_id' : item.id
                        }
                    };
                    axios.get(url,params).then(response => {
                        // console.log(response.data);
                        this.ingredientes = response.data.data;
                        this.idEliminando = '';
                        toastr.success(response.data.message);
                    }).catch(error => {
                       console.log(error.response);
                    });

                },
                guardar(){
                    if(this.ingredientes.length==0){
                        toastr.error('No hay ingredientes en la receta');
                        return false;
                    }else{
                        document.getElementById("myForm").submit();
                    }
                }
            },
            watch: {
                item_selcet: function (val) {
                    // console.log('Cambio item');
                    if(val){
                        this.getInfoItem();
                    }
                },
                ingrediente_selcet: function (val) {
                    // console.log('Cambio item');
                    if(val){
                        this.$refs.buttonadd.focus();
                    }
                }
            }
        });

        $(function () {
            $("#cantidad_ingrediente").keypress(function (e) {
//            console.log(e);
                if (e.keyCode == 13) {
                    e.preventDefault();
                    $("#btn-add-ingrediente").focus();
                }
            });
        })
    </script>
@endpush

