<div class="row">
    <div class="col-sm-4">
        <div class="card">
            <h5 class="card-header">COMBO</h5>
            <div class="card-body">
                <!-- Id Field -->
                {!! Form::label('id', 'Código:') !!}
                {!! $combo->item->codigo !!}<br>


                <!-- Item Id Field -->
                {!! Form::label('item_id', 'Nombre Combo:') !!}
                {!! $combo->item->nombre !!}<br>


                <!-- Created At Field -->
                {!! Form::label('created_at', 'Creado el:') !!}
                {!! $combo->created_at->format('Y/m/d H:i:s')!!}<br>


                <!-- Updated At Field -->
                {!! Form::label('updated_at', 'Actualizado el:') !!}
                {!! $combo->updated_at->format('Y/m/d H:i:s')!!}<br>


                <!-- Deleted At Field -->
                {{--{!! Form::label('deleted_at', 'Borrado el:') !!}--}}
                {{--{!! $combo->deleted_at !!}<br>--}}
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        <div class="card">
            <h5 class="card-header">Productos</h5>
            <div class="card-body">
                {{--Tabla de ingredientes--}}
                <table class="table table-striped">
                    <thead>
                    <tr class="text-center">
                        <th scope="col">Código</th>
                        <th scope="col">Nombre</th>
                        <th scope="col">Cantidad</th>
                        <th scope="col">Unidad de Medida</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($combo->comboDetalles as $det)
                        <tr class="text-center">
                            <td>{{$det->item->codigo}}</td>
                            <td>{{$det->item->nombre}}</td>
                            <td>{{nf($det->cantidad)}}</td>
                            <td>{{$det->item->unimed->simbolo}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>




