<!-- Nombre Field -->
<div class="form-group col-sm-6">
    {!! Form::label('nombre', 'Nombre:') !!}
    {!! Form::text('nombre', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-3">
    {!! Form::label('show_web', 'Mostrar en Web Cliente:') !!}<br>
    <input type="checkbox" data-toggle="toggle" data-size="normal" data-on="Si" data-off="No" data-style="ios" name="show_web" id="show_web"
           value="1"
            {{( !isset($icategoria) || (isset($icategoria) && $icategoria->show_web) ) ? 'checked' : '' }}>
</div>

<!-- Descripcion Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('descripcion', 'Descripcion:') !!}
    {!! Form::textarea('descripcion', null, ['class' => 'form-control','rows'=>'2']) !!}
</div>

