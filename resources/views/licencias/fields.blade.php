<!-- Fecha Expira Field -->
<div class="form-group col-sm-6">
    {!! Form::label('fecha_expira', 'Fecha Expira:') !!}
    {!! Form::date('fecha_expira', null, ['class' => 'form-control']) !!}
</div>

<!-- Prueba Field -->
<div class="form-group col-sm-6">
    {!! Form::label('prueba', 'Prueba:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('prueba', false) !!}
        {!! Form::checkbox('prueba', '1', null) !!} 1
    </label>
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    <button type="submit" onClick="this.form.submit(); this.disabled=true;" class="btn btn-success">Guardar</button>
    <a href="{!! route('licencias.index') !!}" class="btn btn-default">Cancel</a>
</div>
