<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $licencia->id !!}</p>
</div>

<!-- Fecha Expira Field -->
<div class="form-group">
    {!! Form::label('fecha_expira', 'Fecha Expira:') !!}
    <p>{!! $licencia->fecha_expira !!}</p>
</div>

<!-- Prueba Field -->
<div class="form-group">
    {!! Form::label('prueba', 'Prueba:') !!}
    <p>{!! $licencia->prueba !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Creado el:') !!}
    <p>{!! $licencia->created_at->format('Y/m/d H:i:s')!!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Actualizado el:') !!}
    <p>{!! $licencia->updated_at->format('Y/m/d H:i:s')!!}</p>
</div>

