<!-- Id Field -->
{!! Form::label('id', 'Id:') !!}
{!! $tipoPago->id !!}<br>


<!-- Nombre Field -->
{!! Form::label('nombre', 'Nombre:') !!}
{!! $tipoPago->nombre !!}<br>


<!-- Created At Field -->
{!! Form::label('created_at', 'Created At:') !!}
{!! $tipoPago->created_at->format('Y/m/d H:i:s')!!}<br>


<!-- Updated At Field -->
{!! Form::label('updated_at', 'Updated At:') !!}
{!! $tipoPago->updated_at->format('Y/m/d H:i:s')!!}<br>


<!-- Deleted At Field -->
{!! Form::label('deleted_at', 'Deleted At:') !!}
{!! $tipoPago->deleted_at !!}<br>


