<!-- Id Field -->
{!! Form::label('id', 'Id:') !!}
{!! $denominacione->id !!}<br>


<!-- Monto Field -->
{!! Form::label('monto', 'Monto:') !!}
{!! $denominacione->monto !!}<br>


<!-- Created At Field -->
{!! Form::label('created_at', 'Created At:') !!}
{!! $denominacione->created_at->format('Y/m/d H:i:s')!!}<br>


<!-- Updated At Field -->
{!! Form::label('updated_at', 'Updated At:') !!}
{!! $denominacione->updated_at->format('Y/m/d H:i:s')!!}<br>


<!-- Deleted At Field -->
{!! Form::label('deleted_at', 'Deleted At:') !!}
{!! $denominacione->deleted_at !!}<br>


