<!-- Modal clientes -->
<div class="modal fade" id="modal-form-clientes">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form action="" method="post" role="form" id="form-modal-clientes">

                <div class="modal-header">
                    <h5 class="modal-title">Nuevo cliente</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body">
                    <div class="form-row">
                        @include('clientes.fields')
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="reset" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    <button type="submit"  data-loading-text="Loading..." class="btn btn-primary" autocomplete="off">
                        Guardar
                    </button>
                </div>

            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
<!-- /. Modal clientes -->

@push('scripts')
<!--    Scripts modal form clientes
------------------------------------------------->
<script>

    //Cuando el modal se abre
    $('#modal-form-clientes').on('shown.bs.modal', function (event) {
        var button = $(event.relatedTarget) // Boton que abrió el modal
        var select2target = button.closest('div').find('select');


        //Envío del formulario del modal
        $("#form-modal-clientes").submit(function (e) {
            e.preventDefault();

            console.log('enviar formulario modal marcas select2target: '+select2target.attr('id'));

            var data= $(this).serializeArray();

            var $btn = $(this).find(':submit').button('loading');

            $.ajax({
                method: 'POST',
                url: '{{route("api.clientes.store")}}',
                data: data,
                dataType: 'json',
                success: function (res) {
                    console.log('respuesta ajax:',res)
                    if(res.success){

                        //Ocultar el modal
                        $('#modal-form-clientes').modal('hide');

                        var option = new Option(res.data.full_name, res.data.id);
                        option.selected = true;

                        //quita la opción seleccionada del select objetivo
                        select2target.find('option:selected').attr("selected", false);
                        //Cambia la opción del select objetivo por la creada
                        select2target.append(option).trigger("change");

                        toastr.success(res.message);
                    }

                    $btn.button('reset');
                },
                error: function (res) {
                    console.log('respuesta ajax:',res.responseJSON);
                    toastr.error('<strong>Error! </strong>'+res.responseJSON.message);
                    $btn.button('reset');
                }
            })
        });
        //Envío del formulario del modal

    });

    //Cuando el modal se cierra
    $('#modal-form-clientes').on('hidden.bs.modal', function (event) {

        //Elimina los eventos del formulario
        $("#form-modal-clientes").unbind();

    });
</script>
@endpush