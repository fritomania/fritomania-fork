<!-- Id Field -->
{!! Form::label('id', 'Id:') !!}
{!! $gasto->id !!}<br>


<!-- Descripcion Field -->
{!! Form::label('descripcion', 'Descripcion:') !!}
{!! $gasto->descripcion !!}<br>


<!-- Monto Field -->
{!! Form::label('monto', 'Monto:') !!}
{!! $gasto->monto !!}<br>


<!-- User Id Field -->
{!! Form::label('user_id', 'User Id:') !!}
{!! $gasto->user_id !!}<br>


<!-- Created At Field -->
{!! Form::label('created_at', 'Creado el:') !!}
{!! $gasto->created_at->format('Y/m/d H:i:s')!!}<br>


<!-- Updated At Field -->
{!! Form::label('updated_at', 'Actualizado el:') !!}
{!! $gasto->updated_at->format('Y/m/d H:i:s')!!}<br>


<!-- Deleted At Field -->
{!! Form::label('deleted_at', 'Borrado el:') !!}
{!! $gasto->deleted_at !!}<br>


