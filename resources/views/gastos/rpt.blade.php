@extends('layouts.app')

@include('layouts.plugins.bootstrap_datetimepicker')
@include('layouts.plugins.datatables')

@section('htmlheader_title')
    Reporte de Gastos
@endsection

@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row">
                <div class="col">
                    <h1 class="m-0 text-dark">Reporte de Gastos</h1>
                </div><!-- /.col -->
                <div class="col">
                    <a class="btn btn-outline-info float-right" href="{!! route('gastos.create') !!}">
                        <i class="fa fa-plus"></i>
                        <span class="d-none d-sm-inline">Nuevo Gasto</span>
                    </a>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            @include('flash::message')

            <div class="row">
                <div class="col">
                    <div class="card card-primary">
                        <div class="card-body">
                            {!! Form::open(['route' => 'rpt.gastos','method' => 'get']) !!}
                            <div class="form-row">
                                <div class="form-group col-sm-2">
                                    {!! Form::label('user_id','Usuario:') !!}
                                    {!! Form::select('user_id', $users, $user_id, ['id'=>'users','class' => 'form-control']) !!}
                                </div>
                                <div class="form-group col-sm-2">
                                    {!! Form::label('del', 'Del:') !!}
                                    {!! Form::text('del', iniMes(), ['class' => 'form-control fecha']) !!}
                                </div>
                                <div class="form-group col-sm-2">
                                    {!! Form::label('al', 'Al:') !!}
                                    {!! Form::text('al', hoy(), ['class' => 'form-control fecha']) !!}
                                </div>

                                <div class="form-group col-sm-4">
                                    {!! Form::label('descripcion', 'Descripción:') !!}
                                    {!! Form::text('descripcion', null, ['class' => 'form-control']) !!}
                                </div>

                                <div class="form-group col-sm-1">
                                    {!! Form::label('eliminados', 'Eliminados:') !!}
                                    <div style="width: 100%">
                                        <input type="checkbox" data-toggle="toggle" data-size="normal" data-on="Si" data-off="No" data-style="ios" name="eliminados" value="1"
                                                {{isset($_GET['eliminados'])  ? 'checked' : ''}}>
                                    </div>
                                </div>

                                <!-- Submit Field -->
                                <div class="form-group col-sm-1">
                                    <br>
                                    {!! Form::submit('Buscar', ['class' => 'btn btn-success']) !!}
                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
                <!-- /.col-md-6 -->
            </div>
            <!-- /.row -->

            <div class="row">
                <div class="col">
                    <div class="card card-outline card-success">
                        <div class="card-header">
                            <h3 class="card-title">Reporte</h3>

                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-widget="remove"><i class="fa fa-times"></i>
                                </button>
                                <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                </button>
                            </div>
                            <!-- /.card-tools -->
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="row">
                                <div class="col">

                                    <div class="table-responsive">
                                        @include('gastos.table')
                                    </div>
                                </div>
                            </div>

                            <h2  class="pull-right">
                                <span class="badge badge-warning" >Total {{ dvs() }} {{ number_format($total ,2)}}</span>
                            </h2>
                        </div>
                        <!-- /.card-body -->
                    </div>
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /.content -->
@endsection

@push('scripts')
<script>
    $(function () {
        $(".fecha").datetimepicker({
            format: 'DD/MM/YYYY',
        });
    })
</script>
@endpush

