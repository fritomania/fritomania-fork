<table class="table table-bordered table-striped table-sm " id="gastos-table">
    <thead>
    <tr>
        <th>Fecha</th>
        <th>Usuario</th>
        <th>Descripción</th>
        <th>Monto</th>
        {{--<th >Action</th>--}}
    </tr>
    </thead>
    <tbody>
    @foreach($gastos as $gasto)
        <tr>
            <td>{{fechaHoraLtn($gasto->created_at)}} {!! diaSemana($gasto->created_at) !!}</td>
            <td>{!! $gasto->user->name !!}</td>
            <td>{!! $gasto->descripcion !!}</td>
            <td>{{ dvs() }} {!! number_format($gasto->monto,2) !!}</td>
        </tr>
    @endforeach
    <tr>
        <td>Total</td>
        <td></td>
        <td></td>
        <td>{{ dvs() }} {{number_format($total,2)}}</td>
    </tr>
    </tbody>
</table>

@push('scripts')
    <script>
        $(function () {
            $('#gastos-table').DataTable( {
                dom: 'Brtip',
                paginate: false,
                scrollY: "200px",
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ],
                "order": [],
                'ordering': false
            } );
        })
    </script>
@endpush