<!doctype html>
<html>
<head>
    <meta name="viewport" content="width=device-width" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Simple Transactional Email</title>
    @include('emails.css')

    <style>
        table > tfoot > tr > th {
            text-align: right;
            padding-right: 0px;
            padding-left: 0px;
        }
    </style>


</head>
<body>

    <!-- Main content -->
    <div class="content">
        <div class="container-fluid" >
            <div style="text-align: center;">
            <img src="https://fritomania.cl/cliente/images/logo_nuevo.png" alt="">
            </div>
            <p style="text-align: center; background-color: #9d1414; color: #ffffff; padding: 5%; font-weight: bold; font-size: 24px;">Solicitud de Stock</p>
            <div class="row">

                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <span class="float-right">
                                        {!! Form::label('solicitud_estado_id', 'Estado:') !!}
                                        <span class="badge badge-pill badge-info">
                                            {!! $solicitud->estado->nombre !!}
                                        </span>
                                    </span>
                                    <!-- Id Field -->
                                    {!! Form::label('id', 'Id:') !!}
                                    {!! $solicitud->id !!}<br>

                                    <!-- Numero Field -->
                                    {!! Form::label('numero', 'Numero:') !!}
                                    {!! $solicitud->numero !!}<br>

                                    <!-- Observaciones Field -->
                                    {!! Form::label('observaciones', 'Observaciones:') !!}
                                    {!! $solicitud->observaciones !!}<br>

                                    <!-- User Id Field -->
                                    {!! Form::label('user_id', 'Solicitante:') !!}
                                    {!! $solicitud->user->name !!}<br>

                                    <!-- Created At Field -->
                                    {!! Form::label('created_at', 'Creado el:') !!}
                                    {!! $solicitud->created_at->format('Y/m/d H:i:s')!!}<br>

                                    @if ($solicitud->estado->id == \App\Models\SolicitudEstado::DESPACHADA)

                                    <!-- User Despacha Field -->
                                        {!! Form::label('user_despacha', 'User Despacha:') !!}
                                        {!! $solicitud->userDespacha ? $solicitud->userDespacha->name : ''!!}<br>

                                        <!-- Fecha Despacha Field -->
                                        {!! Form::label('fecha_despacha', 'Despachada El:') !!}
                                        {!! $solicitud->fecha_despacha !!}<br>

                                    @endif

                                    <table border="1">
                                        <thead>
                                        <tr>
                                            <th>Producto</th>
                                            <th>Cantidad</th>
                                            <th>Precio</th>
                                            <th>SubTotal</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($solicitud->detalles as $det)
                                            <tr>
                                                <td>{{$det->item->nombre}}</td>
                                                <td>{{nf($det->cantidad)}}</td>
                                                <td>{{dvs().nfp($det->precio)}}</td>
                                                <td>{{dvs().nfp($det->sub_total)}}</td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                        <tfoot>
                                        <tr>
                                            <th>
                                                TOTAL
                                            </th>
                                            <th >
                                                {{nf($solicitud->detalles->sum('cantidad'))}}
                                            </th>
                                            <th></th>
                                            <th >
                                                {{dvs().nfp($solicitud->detalles->sum('sub_total'))}}
                                            </th>
                                        </tr>
                                        </tfoot>
                                    </table>

                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col-md-6 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /.content -->
@include('emails.footer')

