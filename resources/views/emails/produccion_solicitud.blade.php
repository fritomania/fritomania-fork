<!doctype html>
<html>
<head>
    <meta name="viewport" content="width=device-width" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Simple Transactional Email</title>
    @include('emails.css')
    <style>
        /* -------------------------------------
            GLOBAL RESETS
        ------------------------------------- */
        img {
            border: none;
            -ms-interpolation-mode: bicubic;
            max-width: 100%; }

        body {
            background-color: #f6f6f6;
            font-family: sans-serif;
            -webkit-font-smoothing: antialiased;
            font-size: 14px;
            line-height: 1.4;
            margin: 0;
            padding: 0;
            -ms-text-size-adjust: 100%;
            -webkit-text-size-adjust: 100%; }

        table {
            border-collapse: collapse;
            mso-table-lspace: 0pt;
            mso-table-rspace: 0pt;
            width: 100%; }
        table td {
            font-family: sans-serif;
            font-size: 14px;
            vertical-align: top; }

        /* -------------------------------------
            BODY & CONTAINER
        ------------------------------------- */

        .body {
            background-color: #f6f6f6;
            width: 100%; }

        /* Set a max-width, and make it display as block so it will automatically stretch to that width, but will also shrink down on a phone or something */
        .container {
            display: block;
            Margin: 0 auto !important;
            /* makes it centered */
            max-width: 580px;
            padding: 10px;
            width: 580px; }

        /* This should also be a block element, so that it will fill 100% of the .container */
        .content {
            box-sizing: border-box;
            display: block;
            Margin: 0 auto;
            max-width: 580px;
            padding: 10px; }

        /* -------------------------------------
            HEADER, FOOTER, MAIN
        ------------------------------------- */
        .main {
            background: #ffffff;
            border-radius: 3px;
            width: 100%; }

        .wrapper {
            box-sizing: border-box;
            padding: 20px; }

        .content-block {
            padding-bottom: 10px;
            padding-top: 10px;
        }

        .footer {
            clear: both;
            Margin-top: 10px;
            text-align: center;
            width: 100%; }
        .footer td,
        .footer p,
        .footer span,
        .footer a {
            color: #999999;
            font-size: 12px;
            text-align: center; }

        /* -------------------------------------
            TYPOGRAPHY
        ------------------------------------- */
        h1,
        h2,
        h3,
        h4 {
            color: #000000;
            font-family: sans-serif;
            font-weight: 400;
            line-height: 1.4;
            margin: 0;
            Margin-bottom: 30px; }

        h1 {
            font-size: 35px;
            font-weight: 300;
            text-align: center;
            text-transform: capitalize; }

        p,
        ul,
        ol {
            font-family: sans-serif;
            font-size: 14px;
            font-weight: normal;
            margin: 0;
            Margin-bottom: 15px; }
        p li,
        ul li,
        ol li {
            list-style-position: inside;
            margin-left: 5px; }

        a {
            color: #9d1414;
            text-decoration: underline; }

        /* -------------------------------------
            BUTTONS
        ------------------------------------- */
        .btn {
            box-sizing: border-box;
            width: 100%; }
        .btn > tbody > tr > td {
            padding-bottom: 15px; }
        .btn table {
            width: auto; }
        .btn table td {
            background-color: #ffffff;
            border-radius: 5px;
            text-align: center; }
        .btn a {
            background-color: #ffffff;
            border: solid 1px #9d1414;
            border-radius: 5px;
            box-sizing: border-box;
            color: #9d1414;
            cursor: pointer;
            display: inline-block;
            font-size: 14px;
            font-weight: bold;
            margin: 0;
            padding: 12px 25px;
            text-decoration: none;
            text-transform: capitalize; }

        .btn-primary table td {
            background-color: #fff; }

        .btn-primary a {
            background-color: #9d1414;
            border-color: #9d1414;
            color: #ffffff; }

        /* -------------------------------------
            OTHER STYLES THAT MIGHT BE USEFUL
        ------------------------------------- */
        .last {
            margin-bottom: 0; }

        .first {
            margin-top: 0; }

        .align-center {
            text-align: center; }

        .align-right {
            text-align: right; }

        .align-left {
            text-align: left; }

        .clear {
            clear: both; }

        .mt0 {
            margin-top: 0; }

        .mb0 {
            margin-bottom: 0; }

        .preheader {
            color: transparent;
            display: none;
            height: 0;
            max-height: 0;
            max-width: 0;
            opacity: 0;
            overflow: hidden;
            mso-hide: all;
            visibility: hidden;
            width: 0; }

        .powered-by a {
            text-decoration: none; }

        hr {
            border: 0;
            border-bottom: 1px solid #f6f6f6;
            Margin: 20px 0; }

        th {
            background-color: #9d1414;
            color: #fff;
            padding: 2%;
            border: none;
        }

        td:nth-child(2){
            text-align: right;
        }

        td:nth-child(3){
            text-align: right;
        }

        td:nth-child(4){
            text-align: right;
        }


        /* -------------------------------------
            RESPONSIVE AND MOBILE FRIENDLY STYLES
        ------------------------------------- */
        @media only screen and (max-width: 620px) {
            table[class=body] h1 {
                font-size: 28px !important;
                margin-bottom: 10px !important; }
            table[class=body] p,
            table[class=body] ul,
            table[class=body] ol,
            table[class=body] td,
            table[class=body] span,
            table[class=body] a {
                font-size: 16px !important; }
            table[class=body] .wrapper,
            table[class=body] .article {
                padding: 10px !important; }
            table[class=body] .content {
                padding: 0 !important; }
            table[class=body] .container {
                padding: 0 !important;
                width: 100% !important; }
            table[class=body] .main {
                border-left-width: 0 !important;
                border-radius: 0 !important;
                border-right-width: 0 !important; }
            table[class=body] .btn table {
                width: 100% !important; }
            table[class=body] .btn a {
                width: 100% !important; }
            table[class=body] .img-responsive {
                height: auto !important;
                max-width: 100% !important;
                width: auto !important; }}

        /* -------------------------------------
            PRESERVE THESE STYLES IN THE HEAD
        ------------------------------------- */
        @media all {
            .ExternalClass {
                width: 100%; }
            .ExternalClass,
            .ExternalClass p,
            .ExternalClass span,
            .ExternalClass font,
            .ExternalClass td,
            .ExternalClass div {
                line-height: 100%; }
            .apple-link a {
                color: inherit !important;
                font-family: inherit !important;
                font-size: inherit !important;
                font-weight: inherit !important;
                line-height: inherit !important;
                text-decoration: none !important; }
            .btn-primary table td:hover {
                background-color: #34495e !important; }
            .btn-primary a:hover {
                background-color: #34495e !important;
                border-color: #34495e !important; } }

    </style>
</head>
<body>
    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            <div style="text-align: center;">
                <img src="https://fritomania.cl/cliente/images/logo_nuevo.png" alt="">
            </div>
            <p style="text-align: center; background-color: #9d1414; color: #ffffff; padding: 5%; font-weight: bold; font-size: 14px;"><!-- Id Field -->
                {!! Form::label('id', 'Correlativo:') !!}
                {!! $produccion->id !!}</p>
            <div class="row">
                <div class="col-lg-12">


                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <br>


                                    <!-- Numero Field -->
                                    {!! Form::label('numero', 'Numero Producción:') !!}
                                    {!! $produccion->numero !!}<br>


                                    <!-- Produccion Tipo Id Field -->
                                    {!! Form::label('produccion_tipo_id', 'Tipo Producción:') !!}
                                    {!! $produccion->produccionTipo->nombre !!}<br>


                                    <!-- Produccion Estado Id Field -->
                                    {!! Form::label('produccion_estado_id', 'Estado:') !!}
                                    <span class="badge badge-info text-lg">
                                         {!! $produccion->produccionEstado->nombre !!}<br>
                                    </span>
                                </div>
                                <div class="col-sm-6">
                                    <!-- User Id Field -->
                                    {!! Form::label('user_id', 'Usuario:') !!}
                                    {!! $produccion->user->name !!}<br>


                                    <!-- Created At Field -->
                                    {!! Form::label('created_at', 'Creado el:') !!}
                                    {!! $produccion->created_at->format('Y/m/d H:i:s')!!}<br>


                                    <!-- Updated At Field -->
                                    {!! Form::label('updated_at', 'Actualizado el:') !!}
                                    {!! $produccion->updated_at->format('Y/m/d H:i:s')!!}<br>


                                    <!-- Deleted At Field -->
                                    {{--{!! Form::label('deleted_at', 'Borrado el:') !!}--}}
                                    {{--{!! $produccion->deleted_at !!}<br>--}}
                                </div>


                            </div>


                    <!-- /.card -->
                </div>
                <!-- /.col-md-6 -->
            </div>
            <div class="row">
                <table border="1">
                    <thead>
                    <tr>
                        <th>Cantidad</th>
                        <th>U/M</th>
                        <th>Nombre</th>
                        <th>Código</th>
                        <th>Pendientes</th>
                        <th>Producido</th>
                        <th>Estado</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($produccion->productoSolicitados as $det)
                        <tr>
                            <td>{{nf($det->cantidad)}}</td>
                            <td>{{$det->item->unimed->nombre}}</td>
                            <td>{{$det->item->nombre}}</td>
                            <td>{{$det->item->codigo}}</td>
                            <td>{{nf($det->pendientes)}}</td>
                            <td>{{nf($det->producido)}}</td>
                            <td>{{$det->estado}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /.content -->
@include('emails.footer')

