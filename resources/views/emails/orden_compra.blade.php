<!doctype html>
<html>
<head>
    <meta name="viewport" content="width=device-width" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Simple Transactional Email</title>
    @include('emails.css')

    <style>
        table > tfoot > tr > th {
            text-align: right;
            padding-right: 0px;
            padding-left: 0px;
        }
    </style>

</head>
<body>
    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            <div style="text-align: center;">
                <img src="https://fritomania.cl/cliente/images/logo_nuevo.png" alt="">
            </div>
            <p style="text-align: center; background-color: #9d1414; color: #ffffff; padding: 5%; font-weight: bold; font-size: 24px;">Nueva Orden de Compra</p>
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                {{--columnas de la compra--}}
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                                    <!-- Tipo Field -->
                                    {!! Form::label('tipo', 'Tipo:') !!}
                                    {!! $compra->tcomprobante->nombre or '' !!}
                                    <br>

                                    <!-- Proveedore Id Field -->
                                    {!! Form::label('proveedor', 'Proveedor :') !!}
                                    {!! $compra->proveedor->nombre or '' !!}
                                    <br>

                                    <!-- Serie Field -->
                                    {!! Form::label('serie', 'N/S:') !!}
                                    {!! $compra->numero !!}-{!! $compra->serie !!}

                                    <br>

                                    <!-- Fecha ingreso Plan Field-->
                                    {!! Form::label('fecha_ingreso_plan', 'Fecha entrega a bodega:') !!}
                                    {!! fecha($compra->fecha_ingreso_plan)!!}

                                    <br>

                                    <!-- Fecha ingreso Field-->
                                    {!! Form::label('fecha_ingreso', 'Fecha Recepción:') !!}
                                    {!! fecha($compra->fecha_ingreso)!!}

                                    <br>

                                    <!-- Fecha del documento Field -->
                                    {!! Form::label('fecha', 'Fecha del documento:') !!}
                                    {!! fecha($compra->fecha)!!}

                                    <br>

                                    <!-- Fecha Credito Field-->
                                    {!! Form::label('fecha_limite_credito', 'Fecha de pago:') !!}
                                    {!! fecha($compra->fecha_limite_credito)!!}

                                    <br>

                                    <!-- Cestado Id Field -->
                                    {{--{!! Form::label('cestado', 'Estado:') !!}--}}
                                    {{--{!! $compra->cestado->descripcion !!}--}}
                                    {{--<br>--}}

                                    <!-- Created At Field -->
                                    {!! Form::label('created_at', 'Creado el:') !!}
                                    {!! $compra->created_at->format('Y/m/d H:i:s') !!}
                                    <br>
                                    <!-- Updated At Field -->
                                    {!! Form::label('updated_at', 'Actualizado el:') !!}
                                    {!! $compra->updated_at->format('Y/m/d H:i:s') !!}
                                    <br>
                                    <br>
                                </div>

                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <table border="1">
                                        <thead>
                                        <tr>
                                            <th width="55%">Producto</th>
                                            <th width="15%">Cantidad</th>
                                            <th width="15%">Precio</th>
                                            <th width="15%">SubTotal</th>
                                        </tr>
                                        </thead>
                                        <tbody>

                                        @foreach($compra->detalles as $det)
                                            <tr>
                                                <td>{{$det->item->nombre}}</td>
                                                <td>{{nf($det->cantidad)}}</td>
                                                <td>{{dvs().nfp($det->precio)}}</td>
                                                <td>{{dvs().nfp($det->sub_total)}}</td>
                                            </tr>

                                        @endforeach

                                        </tbody>
                                        <tfoot>
                                        <tr>
                                            <th align="right">Total</th>
                                            <th align="right">{{nf( $compra->detalles->sum('cantidad') )}}</th>
                                            <th align="right"></th>
                                            <th align="right">{{dvs().nfp( $compra->detalles->sum('sub_total') )}}</th>
                                        </tr>
                                        </tfoot>
                                    </table>

                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col-md-6 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /.content -->
@include('emails.footer')

