<!doctype html>
<html>
<head>
    <meta name="viewport" content="width=device-width" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Simple Transactional Email</title>
    @include('emails.css')

</head>
<body>
<table border="0" cellpadding="0" cellspacing="0" class="body" style="text-align: center;">
    <tbody>
    <tr>
        <td>&nbsp;</td>
        <td class="container">

            <div class="content">

                <!-- START CENTERED WHITE CONTAINER -->
                <span class="preheader">Gracias por su compra.</span>
                <table class="table">

                    <!-- START MAIN CONTENT AREA -->
                    <tr>
                        <td class="wrapper">

                            <table  class="table" border="0" cellpadding="0" cellspacing="0">
                                <tr >
                                    <td style="text-align: center;">
                                        <img src="https://fritomania.cl/cliente/images/logo_nuevo.png" alt="">
                                        <p style="text-align: center; background-color: #9d1414; color: #ffffff; padding: 5%; font-weight: bold; font-size: 21px;">Gracias por su compra</p>
                                        <p>{{$msg}}</p>
                                        <table border="0" cellpadding="0" cellspacing="0" class="table">
                                            <tbody>
                                            <tr>
                                                <td align="center">
                                                    <section class="order-layout">
                                                        <div class="order-wrapper">
                                                            <div class="container">
                                                                <div class="row">
                                                                    <div class="order-inner">
                                                                        <div class="order-content">
                                                                            <div class="order-id">
                                                                                <h2>Pedido N° {{$pedido->id}}</h2>
                                                                                <span class="date">{{fechaHoraActual()}}</span>
                                                                                <div class="address-content">
                                                                                    <div class="address-item name">
                                                                                        <span class="title">Cliente: {{$pedido->nombre_entrega}}</span><br>
                                                                                        <span class="title">Teléfono: {{$pedido->telefono}}</span><br>
                                                                                        <span class="title">Dirección: {{$pedido->direccion}}</span><br>
                                                                                        <span class="title">Cod WebPay: {{$pedido->cod_web_pay}}</span><br>
                                                                                        <span class="title">Local: {{$pedido->local->nombre}}</span>
                                                                                    </div>
                                                                                    {{--<div class="address-item">--}}
                                                                                    {{--<span class="title">Zip / Código Postal:</span>--}}
                                                                                    {{--<span class="content">2340000</span>--}}
                                                                                    {{--</div>--}}
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="order-info">
                                                                            <div class="order-info-inner">
                                                                                <table id="order_details" border="1">
                                                                                    <thead>
                                                                                    <tr>
                                                                                        <th>Producto</th>
                                                                                        <th>Precio</th>
                                                                                        <th class="center">Cantidad</th>
                                                                                        <th class="total">Total</th>
                                                                                    </tr>
                                                                                    </thead>
                                                                                    <tbody>
                                                                                    @foreach($pedido->detalles as $det)
                                                                                        <tr>
                                                                                            <td class="td-product">
                                                                                                {{$det->item->nombre}}
                                                                                            </td>
                                                                                            <td class="money">
                                                                                            <span class="money">
                                                                                                {{dvs().nfp($det->precio)}}
                                                                                            </span>
                                                                                            </td>
                                                                                            <td class="quantity ">
                                                                                                {{nf($det->cantidad,0)}}
                                                                                            </td>
                                                                                            <td class="total">
                                                                                            <span class="money">
                                                                                                {{dvs().nfp($det->precio*$det->cantidad)}}
                                                                                            </span>
                                                                                            </td>
                                                                                        </tr>
                                                                                    @endforeach
                                                                                    </tbody>
                                                                                    <tfoot>
                                                                                    @if($pedido->delivery)
                                                                                        <tr class="order_summary note">
                                                                                            <td class="td-label" colspan="3">SubTotal</td>
                                                                                            <td class="total">
                                                                                                <span class="money" >{{dvs().nfp($pedido->total)}}</span>
                                                                                            </td>
                                                                                        </tr>

                                                                                        <tr class="order_summary note">
                                                                                            <td class="td-label" colspan="3">Delivery</td>
                                                                                            <td class="total">
                                                                                                <span class="money" >{{dvs().nfp($pedido->monto_delivery)}}</span>
                                                                                            </td>
                                                                                        </tr>
                                                                                    @endif
                                                                                    <tr class="order_summary order_total">
                                                                                        <td class="td-label" colspan="3" style="font-weight: bold;"git fetch upstream>TOTAL</td>
                                                                                        <td class="total">
                                                                                            <span class="money" style="font-weight: bold;">{{dvs().nfp($pedido->total)}}</span>
                                                                                        </td>
                                                                                    </tr>
                                                                                    </tfoot>
                                                                                </table>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </section>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                            {{--<table border="0" cellpadding="0" cellspacing="0">--}}
                            {{--<tbody>--}}
                            {{--<tr>--}}
                            {{--<td> <a href="https://fritomania.cl/locales" target="_blank">Seguir comprando</a> </td>--}}
                            {{--</tr>--}}
                            {{--</tbody>--}}
                            {{--</table>--}}
                        </td>
                    </tr>
                </table>

            </div>
        </td>
    </tr>
    </tbody>
</table>

@include('emails.footer')


</body>
</html>
