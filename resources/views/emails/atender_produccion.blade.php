<!doctype html>
<html>
<head>
    <meta name="viewport" content="width=device-width" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Simple Transactional Email</title>
    @include('emails.css')

</head>
<body>
    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            <div style="text-align: center;">
                <img src="https://fritomania.cl/cliente/images/logo_nuevo.png" alt="">
            </div>
            <p style="text-align: center; background-color: #9d1414; color: #ffffff; padding: 5%; font-weight: bold; font-size: 14px;">Despachar Solicitud</p>
            <div class="row">
                <div class="col-lg-12">


                            <div class="row">
                                <div>
                                    <div class="row">
                                        <div class="col">

                                                <h5>Datos de la Producción</h5>

                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <!-- Id Field -->
                                                            {!! Form::label('id', 'Correlativo:') !!}
                                                            {!! $produccion->id !!}<br>


                                                            <!-- Numero Field -->
                                                            {!! Form::label('numero', 'Numero Producción:') !!}
                                                            {!! $produccion->numero !!}<br>


                                                            <!-- Produccion Tipo Id Field -->
                                                            {!! Form::label('produccion_tipo_id', 'Tipo Producción:') !!}
                                                            {!! $produccion->produccionTipo->nombre !!}<br>


                                                            <!-- Produccion Estado Id Field -->
                                                            {!! Form::label('produccion_estado_id', 'Estado:') !!}
                                                            <span class="badge badge-info text-lg">
                            {!! $produccion->produccionEstado->nombre !!}<br>
                        </span>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <!-- User Id Field -->
                                                            {!! Form::label('user_id', 'Usuario:') !!}
                                                            {!! $produccion->user->name !!}<br>


                                                            <!-- Created At Field -->
                                                            {!! Form::label('created_at', 'Creado el:') !!}
                                                            {!! $produccion->created_at->format('Y/m/d H:i:s')!!}<br>


                                                            <!-- Updated At Field -->
                                                            {!! Form::label('updated_at', 'Actualizado el:') !!}
                                                            {!! $produccion->updated_at->format('Y/m/d H:i:s')!!}<br>


                                                            <!-- Deleted At Field -->
                                                            {{--{!! Form::label('deleted_at', 'Borrado el:') !!}--}}
                                                            {{--{!! $produccion->deleted_at !!}<br>--}}
                                                        </div>
                                                    </div>



                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12">

                                                <h5>Producto Solicitado</h5>

                                                    <table border="1">
                                                        <thead class="bg-secondary">
                                                        <tr>
                                                            <th scope="col">Cantidad</th>
                                                            <th scope="col">U/M</th>
                                                            <th scope="col">Nombre</th>
                                                            <th scope="col">Código</th>
                                                            <th scope="col">Pendientes</th>
                                                            <th scope="col">Producido</th>
                                                            <th scope="col">Estado</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        @foreach($produccion->productoSolicitados as $det)
                                                            <tr class="">
                                                                <td>{{nf($det->cantidad)}}</td>
                                                                <td>{{$det->item->unimed->nombre}}</td>
                                                                <td>{{$det->item->nombre}}</td>
                                                                <td>{{$det->item->codigo}}</td>
                                                                <td>{{nf($det->pendientes)}}</td>
                                                                <td>{{nf($det->producido)}}</td>
                                                                <td>{{$det->estado}}</td>
                                                            </tr>
                                                        @endforeach
                                                        </tbody>
                                                    </table>


                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12">

                                                <h5>Producto Terminado</h5>

                                                    {{--Tabla de ingredientes--}}
                                            <table border="1">
                                                        <thead class="bg-secondary">
                                                        <tr>
                                                            <th scope="col">Código</th>
                                                            <th scope="col">Nombre</th>
                                                            <th scope="col">Cantidad</th>
                                                            <th scope="col">Unidad de Medida</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        @foreach($produccion->productoRealizados as $det)
                                                            <tr class="">
                                                                <td>{{$det->item->codigo}}</td>
                                                                <td>{{$det->item->nombre}}</td>
                                                                <td>{{nf($det->cantidad)}}</td>
                                                                <td>{{$det->item->unimed->nombre}}</td>
                                                            </tr>
                                                        @endforeach
                                                        </tbody>
                                                    </table>


                                        </div>
                                        <div class="col-sm-12">

                                                <h5>Materia Prima</h5>

                                                    {{--Tabla de ingredientes--}}
                                                    <table border="1">
                                                        <thead class="bg-secondary">
                                                        <tr>
                                                            <th scope="col">Código</th>
                                                            <th scope="col">Nombre</th>
                                                            <th scope="col">Unidad de Medida</th>
                                                            <th scope="col">Cantidad Receta</th>
                                                            <th scope="col">Cantidad Usada</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody >
                                                        @foreach($produccion->productoConsumidos as $det)
                                                            <tr class="">
                                                                <td>{{$det->item->codigo}}</td>
                                                                <td>{{$det->item->nombre}}</td>
                                                                <td>{{$det->item->unimed->nombre}}</td>
                                                                <td>{{nf($det->cantidad_receta)}}</td>
                                                                <td>{{nf($det->cantidad_ingresada)}}</td>
                                                            </tr>
                                                        @endforeach
                                                        </tbody>
                                                    </table>


                                        </div>
                                    </div>
                                </div>
                            </div>


                    <!-- /.card -->
                </div>
                <!-- /.col-md-6 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /.content -->
    <div class="footer">


        <div class="content-block">
            <span class="apple-link">Av. Nueva Providencia 1881, oficina 1407, Providencia, Región Metropolitana, Chile</span>
            <br> ¿No quiere seguir recibiendo estos mails? <a href="http://i.imgur.com/CScmqnj.gif">Darme de alta</a>.
        </div>


        <div class="content-block powered-by">
            Copyright 2018 | Desarrollado por <a href="http://negociovirtual.cl/">Negocio Virtual</a>.
        </div>
        </tr>

    </div>
