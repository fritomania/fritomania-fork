<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" href="favicon.ico">

        <title>@yield('htmlheader_title', config('app.name'))</title>

        <!-- Bootstrap core CSS -->
        <link rel="stylesheet" href="{{asset('css/app.css')}}">

        <link rel="stylesheet" href="{{asset('css/mails.css')}}" >

        <!--            Css inyectado en la vista
        ------------------------------------------------------------------------>

        @stack('css')

        <!-- Fin Css inyectado -->

    </head>

    <body>

    <div class="container">
        @yield('content')
    </div><!-- /.container -->


  </body>
</html>
