@push('css')
<style>
    .select2-container--default .select2-selection--multiple .select2-selection__choice{
        color: #0A0A0A;
    }
</style>
@endpush
<!-- Item Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('item_id', 'Artículo:') !!}
    {!! Form::select('item_id', [] ,null,['class' => 'form-control', 'id'=>'items','multiple'=>"multiple",'style' => "width: 100%"]) !!}
</div>

<!-- Porcentaje Field -->
<div class="form-group col-sm-6">
    {!! Form::label('porcentaje', 'Porcentaje: ') !!}
    {!! Form::text('porcentaje', null, ['class' => 'form-control']) !!}
</div>

<!-- Fecha Inicio Field -->
<div class="form-group col-sm-6">
    {!! Form::label('fecha_inicio', 'Fecha Inicio:') !!}
    {!! Form::text('fecha_inicio', null, ['class' => 'form-control date']) !!}
</div>

<!-- Fecha Expira Field -->
<div class="form-group col-sm-6">
    {!! Form::label('fecha_expira', 'Fecha Expira:') !!}
    {!! Form::text('fecha_expira', null, ['class' => 'form-control date']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    <button type="submit" onClick="this.form.submit(); this.disabled=true;" class="btn btn-success">Guardar</button>
    <a href="{!! route('descuentos.index') !!}" class="btn btn-default">Cancel</a>
</div>
@push('scripts')
<script>
    $(function () {
        function formatStateItems (state) {

            var imagen= state.imagen==undefined ? 'img/avatar_none.png' : state.imagen;

            var color_stock_less = (state.stock<=0) ? 'color_stock_less' : '';

            var $state = $(
                "@component('components.format_slc2_item')"+
                "@slot('imagen')"+imagen+ "@endslot"+
                "@slot('nombre')"+state.nombre+ "@endslot"+
                "@slot('marca')"+state.nombre_marca+ "@endslot"+
                "@slot('descripcion')"+ state.descripcion+"@endslot"+
                "@slot('precio')"+state.precio_venta+"@endslot"+
                "@slot('ubicacion')"+state.ubicacion+"@endslot"+
                "@slot('stock')"+state.stock+"@endslot"+
                "@slot('color_stock_less')"+color_stock_less+"@endslot"+
                "@endcomponent"
            );

            return $state;
        };

        var slc2item=$("#items").select2({
            language : 'es',
            maximumSelectionLength: 1,
            placeholder: "Ingrese código,nombre o descripción para la búsqueda",
            delay: 250,
            ajax: {
                url: "{{ route('api.items.index') }}",
                dataType: 'json',
                data: function (params) {
                    return {
                        search: params.term
                    };
                },
                processResults: function (data, params) {
                    //recorre todos los item q
                    var data = $.map(data.data, function (item) {

                        //recorre los atributos del item
                        $.each(item,function (index,valor) {
                            //Si no existe valor se asigan un '-' al attributo
                            item[index] = !valor ? '-' : valor;
                        });

                        return item;
                    });

                    return {
                        results: data,
                    };
                },
                cache: true
            },
            minimumInputLength: 1,
            allowClear: true,
            templateResult: formatStateItems,
            templateSelection: function(data,contenedor) {

                //si se esta editando el descuento
                @if(isset($descuento->id))
                    return '{{$descuento->item->nombre}}';
                @else
                    return data.nombre;
                @endif

            }
        })

        var hoy=new Date();

        var manana=new Date(hoy.getTime() + 24*60*60*1000);

        $("#fecha_inicio").datetimepicker({
            format: 'DD/MM/YYYY',
            minDate: hoy
        })

        $("#fecha_expira").datetimepicker({
            format: 'DD/MM/YYYY',
            minDate: manana
        })

        //si se esta editando el descuento
        @if(isset($descuento->id))

            var option = new Option('{{$descuento->item->nombre}}', '{{$descuento->item->id}}');
            option.selected = true;

            slc2item.append(option).trigger("change");
        @endif

    })
</script>
@endpush