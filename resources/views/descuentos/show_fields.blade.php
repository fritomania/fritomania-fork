<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $descuento->id !!}</p>
</div>

<!-- Item Id Field -->
<div class="form-group">
    {!! Form::label('item_id', 'Item Id:') !!}
    <p>{!! $descuento->item_id !!}</p>
</div>

<!-- Porcentaje Field -->
<div class="form-group">
    {!! Form::label('porcentaje', 'Porcentaje:') !!}
    <p>{!! $descuento->porcentaje !!}</p>
</div>

<!-- Fecha Inicio Field -->
<div class="form-group">
    {!! Form::label('fecha_inicio', 'Fecha Inicio:') !!}
    <p>{!! $descuento->fecha_inicio !!}</p>
</div>

<!-- Fecha Expira Field -->
<div class="form-group">
    {!! Form::label('fecha_expira', 'Fecha Expira:') !!}
    <p>{!! $descuento->fecha_expira !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Creado el:') !!}
    <p>{!! $descuento->created_at->format('Y/m/d H:i:s')!!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Actualizado el:') !!}
    <p>{!! $descuento->updated_at->format('Y/m/d H:i:s')!!}</p>
</div>

<!-- Deleted At Field -->
<div class="form-group">
    {!! Form::label('deleted_at', 'Borrado el:') !!}
    <p>{!! $descuento->deleted_at !!}</p>
</div>

