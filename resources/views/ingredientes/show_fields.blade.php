<!-- Id Field -->
{!! Form::label('id', 'Id:') !!}
{!! $ingrediente->id !!}<br>


<!-- Ingrediente Categoria Id Field -->
{!! Form::label('ingrediente_categoria_id', 'Categoría:') !!}
{!! $ingrediente->categoria->nombre !!}<br>


<!-- Nombre Field -->
{!! Form::label('nombre', 'Nombre:') !!}
{!! $ingrediente->nombre !!}<br>



<!-- Created At Field -->
{!! Form::label('created_at', 'Creado EL:') !!}
{!! $ingrediente->created_at->format('Y/m/d H:i:s')!!}<br>


<!-- Updated At Field -->
{!! Form::label('updated_at', 'Actualizado El:') !!}
{!! $ingrediente->updated_at->format('Y/m/d H:i:s')!!}<br>


<!-- Deleted At Field -->
{{--{!! Form::label('deleted_at', 'Eliminado El:') !!}--}}
{{--{!! $ingrediente->deleted_at !!}<br>--}}


