

@include('layouts.plugins.bootstrap_fileinput')

<div class="col-sm-4">

    <!-- Imagen Field -->
    <div class="form-group col">
        @isset($ingrediente)
            <div class="card" >
                <img class="card-img-top" src="{{$ingrediente->img}}" alt="Card image cap" id="img-user">
                <div class="card-body" style="padding: 0px">
                    <!-- Imagen Field -->

                    <div class="form-row " id="field-img" style="display: none">
                        <input id="files" name="file" type="file" class="file">
                    </div>
                    <a class="btn btn-outline-info btn-sm btn-block" id="etidarImagen">Editar</a>
                </div>
            </div>
        @else
            <input id="files" name="file" type="file" class="file">
        @endisset

    </div>
</div>

<div class="col-sm-8">

    <!-- Ingrediente Categoria Id Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('ingrediente_categoria_id', 'Categoría:') !!}

        {!!
            Form::select(
                'ingrediente_categoria_id',
                slc(\App\Models\IngredienteCategoria::class)
                , null
                , ['id'=>'ingredientes_categorias','class' => 'form-control','style'=>'width: 100%']
            )
        !!}

    </div>

    <!-- Nombre Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('nombre', 'Nombre:') !!}
        {!! Form::text('nombre', null, ['class' => 'form-control']) !!}
    </div>
</div>


@push('scripts')
    <script>
        $("#etidarImagen").click(function () {
            $('#field-img').show();
            $('#img-user').hide();

        });
    </script>
@endpush