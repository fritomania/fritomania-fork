<?php
/**
 * Created by PhpStorm.
 * User: Usuario
 * Date: 23/06/2017
 * Time: 17:29
 */

use Codedge\Fpdf\Fpdf\Fpdf;

$pdf= new Fpdf();
/* Configuraciones
       -----------------------------------------------------------------------------------------------------------------*/
$size= [216,140];

$bordes=0;

/**
 * Ancho detalles
 */
$wd = [
    'c' =>20, //Cantidad
    'd' =>113, //Descripción
    'p' =>25.5, //Precio
    's' =>25.5, //Sub Total
];

$h=5;
/**
 * Ancho total 216 - margen izquierdo y derecho (216-16-16)=184
 */
$wt=array_sum($wd);

$pdf->SetAutoPageBreak(false,10);//margen inferior
$pdf->SetMargins(16,10,16);
$pdf->AddPage('P','Letter');

/* Fecha Nombre dirección y nit
-----------------------------------------------------------------------------------------------------------------*/

list($fecha,$hora)=explode(' ',$pedido->created_at);
list($anio,$mes,$dia)=explode('-',$fecha);

$fecha = fecha($fecha);
$pdf->SetFont('Arial', 'B', 14);
$pdf->Cell(0, $h, config('app.nombre_negocio'),$bordes,1,'C');
$pdf->SetFont('Arial', '', 10);
$pdf->Cell(0, $h, config('app.dire_negocio').' '.config('app.pais_negocio').' '.config('app.depto_negocio').' '.config('app.muni_negocio'),$bordes,1,'C');
$pdf->Cell(0, $h, 'Telefono: '.config('app.tel_negocio'),$bordes,1,'C');
$pdf->SetFont('Arial', 'B', 14);
$pdf->Cell(0, $h, 'PEDIDO',$bordes,1,'C');
$pdf->ln(5);

$pdf->SetFont('Arial', 'B', 12);
$pdf->Cell(0, $h, 'Numero: '.$pedido->numero,$bordes,1);
$pdf->Cell(0, $h, 'Fecha: '.$fecha,$bordes,1);
$pdf->Cell(0, $h, 'Cliente: '.$pedido->cliente->full_name,$bordes,1);
$pdf->Cell(0, $h, utf8_decode('Dirección: ').$pedido->cliente->direcion,$bordes,1);
$pdf->ln(5);


/* detalles
//-----------------------------------------------------------------------------------------------------------------*/
$bordes=1;
$altoMaxDets=63;
$altoDet=5;
$altoTotalDets= count($pedido->pedidoDetalles)*$altoDet;

$pdf->Cell($wd['c'], $h, 'Cantidad', $bordes, 0,'C');
$pdf->Cell($wd['d'], $h, utf8_decode('Descripción'), $bordes, 0,'L');
$pdf->Cell($wd['p'], $h, 'Precio', $bordes, 0,'R');
$pdf->Cell($wd['s'], $h, 'Subtotal', $bordes, 1,'R');

$pdf->SetFont('Arial', '', 10);

foreach ($pedido->pedidoDetalles as $det) {

    $subTotal=dvs().' '.number_format($det->cantidad*$det->precio,2);

    $pdf->Cell($wd['c'], $altoDet, noCerosDecimales($det->cantidad), $bordes, 0,'C');
    $pdf->Cell($wd['d'], $altoDet, $det->item->nombre, $bordes, 0,'L');
    $pdf->Cell($wd['p'], $altoDet, dvs().' '.$det->precio, $bordes, 0,'R');
    $pdf->Cell($wd['s'], $altoDet, $subTotal, $bordes, 1,'R');

}

$espacioRestante=$altoMaxDets-$altoTotalDets;

//si los detalles no alcanzan el maximo; dejar espacio con lo restante
//$altoTotalDets<$altoMaxDets ? $pdf->ln($espacioRestante) : null;

$y=$pdf->Gety();
$pdf->Cell($wd['c']+$wd['d'], 9, '', $bordes, 0);
$pdf->SetFont('Arial', 'B', 14);

$pdf->Cell($wd['p'], 9, 'TOTAL', $bordes, 0);
$pdf->Cell($wd['s'], 9, dvs().' '.number_format($pedido->total,2), $bordes, 1,'R');

$pdf->SetFont('Arial', 'B', 7);
$totalLetras = NumeroALetras::convertir(number_format($pedido->total,2), 'Quetzales', 'centavos');

$pdf->SetY($y);
$pdf->Cell($wd['c']+$wd['d'], 5, 'Total letras:', 'LRT', 0);
$pdf->SetY($y+4);
$pdf->Cell($wd['c']+$wd['d'], 5, $totalLetras, 'LRB', 0);
$pdf->Output('I',' pedido '.$pedido->id.' '.$pedido->created_at.'.pdf');
exit();