<?php
/**
 * Created by PhpStorm.
 * User: Usuario
 * Date: 23/06/2017
 * Time: 17:29
 */

use Codedge\Fpdf\Fpdf\Fpdf;

$pdf= new Fpdf();
/* Configuraciones
       -----------------------------------------------------------------------------------------------------------------*/
$size= [216,140];

$bordes=0;

/**
 * Ancho detalles
 */
$wd = [
    'c' =>18, //Cantidad
    'd' =>115, //Descripción
    'p' =>25.5, //Precio
    's' =>25.5, //Sub Total
];

/**
 * Ancho total 216 - margen izquierdo y derecho (216-16-16)=184
 */
$wt=array_sum($wd);

$pdf->SetAutoPageBreak(false,10);//margen inferior
$pdf->SetMargins(16,0,16);
$pdf->AddPage('P','Letter');

/* Fecha Nombre dirección y nit
-----------------------------------------------------------------------------------------------------------------*/

list($anio,$mes,$dia)=explode('-',$venta->fecha);

$pdf->ln(30.5);
$pdf->SetFont('Arial', 'B', 12);
$pdf->Cell(10, 8.5, $dia,$bordes,0,'C');
$pdf->Cell(20, 8.5, $mes,$bordes,0,'C');
$pdf->Cell(23, 8.5, $anio,$bordes,1,'C');
$pdf->ln(1);

$pdf->SetFont('Arial', 'B', 8);
$pdf->Cell(20, 5, '',$bordes,0);
$pdf->Cell(164, 5, $venta->cliente->full_name,$bordes,1);

$pdf->Cell(20, 4, '',$bordes,0);
$pdf->Cell(139, 4, $venta->cliente->direccion,$bordes,0);
$pdf->Cell(25, 4, $venta->cliente->nit,$bordes,1);
$pdf->Cell(0, 3, '',$bordes,1);
$pdf->ln(4);


/* detalles
-----------------------------------------------------------------------------------------------------------------*/
$altoMaxDets=63;
$altoDet=4;
$altoTotalDets= count($venta->ventaDetalles)*$altoDet;

$pdf->SetFont('Arial', '', 10);

foreach ($venta->ventaDetalles as $det) {

    $subTotal=dvs().' '.number_format($det->cantidad*$det->precio,2);

    $pdf->Cell($wd['c'], $altoDet, $det->cantidad, $bordes, 0,'C');
    $pdf->Cell($wd['d'], $altoDet, $det->item->nombre, $bordes, 0,'L');
    $pdf->Cell($wd['p'], $altoDet, dvs().' '.$det->precio, $bordes, 0,'R');
    $pdf->Cell($wd['s'], $altoDet, $subTotal, $bordes, 1,'R');

}

$espacioRestante=$altoMaxDets-$altoTotalDets;

//si los detalles no alcanzan el maximo; dejar espacio con lo restante
$altoTotalDets<$altoMaxDets ? $pdf->ln($espacioRestante) : null;

$y=$pdf->Gety();
$pdf->Cell($wd['c']+$wd['d'], 9, '', $bordes, 0);
$pdf->SetFont('Arial', 'B', 14);

$pdf->Cell($wd['p'], 9, '', $bordes, 0);
$pdf->Cell($wd['s'], 9, number_format($venta->total,2), $bordes, 1,'R');

$pdf->SetFont('Arial', 'B', 7);
$totalLetras = NumeroALetras::convertir(number_format($venta->total,2), 'Quetzales', 'centavos');

$pdf->SetY($y+4);
$pdf->Cell($wd['c']+$wd['d'], 5, $totalLetras, $bordes, 0);
$pdf->Output('I',' factura '.$venta->id.' '.$venta->fecha.'.pdf');
exit();