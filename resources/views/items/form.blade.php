@extends('layouts.app')

@include('layouts.plugins.select2')
@include('layouts.plugins.bootstrap_fileinput')

@section('htmlheader_title')
    {{ isset($item) ? 'Editar Artículo ' : 'Crear Artículo'   }}
@endsection

@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row">
                <div class="col">
                    <h1 class="m-0 text-dark">
                        {{ isset($item) ? 'Editar Artículo ' : 'Crear Artículo'   }}
                    </h1>
                </div><!-- /.col -->
                <div class="col">
                    <a class="btn btn-outline-info float-right"
                       href="{{route('items.index')}}">
                        <i class="fa fa-list" aria-hidden="true"></i>&nbsp;<span class="d-none d-sm-inline">Listado</span>
                    </a>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            @include('flash::message')
            @include('adminlte-templates::common.errors')

            <div class="row">
                <div class="col-lg-12">

                    <nav>
                        <div class="nav nav-tabs" id="nav-tab" role="tablist">
                            <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-info-gen" role="tab" aria-controls="nav-info-gen" aria-selected="true">
                                Info General
                            </a>
                            <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-ficha" role="tab" aria-controls="nav-ficha" aria-selected="false">
                                Ficha
                            </a>
                        </div>
                    </nav>
                    <div class="tab-content" id="nav-tabContent">
                        <div class="tab-pane fade show active" id="nav-info-gen" role="tabpanel" aria-labelledby="nav-home-tab">

                            <div class="card">
                                <div class="card-body">

                                    @isset($item)
                                        {!! Form::model($item, ['route' => ['items.update', $item->id], 'method' => 'patch',"enctype"=>"multipart/form-data","autocomplete"=>"off"]) !!}
                                    @else
                                        {!! Form::open(['route' => 'items.store',"enctype"=>"multipart/form-data","autocomplete"=>"off"]) !!}
                                    @endisset

                                    <div class="form-row">

                                        @include('items.fields')
                                        <!-- Submit Field -->
                                        <div class="form-group col-sm-12">
                                            <button type="submit" onClick="this.form.submit(); this.disabled=true;" class="btn btn-outline-success">Guardar</button>
                                            <a href="{!! route('items.index') !!}" class="btn btn-outline-default">Cancelar</a>
                                        </div>
                                    </div>

                                    {!! Form::close() !!}

                                </div>
                            </div>
                            <!-- /.card -->
                        </div>
                        <div class="tab-pane fade" id="nav-ficha" role="tabpanel" aria-labelledby="nav-profile-tab">
                            <div class="card">
                                <div class="card-body">
                                    @isset($item)
                                        {!! Form::model($item, ['route' => ['items.update', $item->id], 'method' => 'patch',"enctype"=>"multipart/form-data","autocomplete"=>"off"]) !!}
                                            <div class="form-row">
                                                @include('items.fields_ficha')
                                            </div>
                                        {!! Form::close() !!}
                                    @else
                                        <h5 class="text-info text-center">
                                            Guarde primero los datos del articulo para poder asociar ingredientes, extras o salsas
                                        </h5>
                                    @endisset
                                </div>
                            </div>
                        </div>
                    </div>


                </div>
                <!-- /.col-md-6 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /.content -->

    @include('items.modal_form_marcas')
    @include('unimeds.modal_create')
@endsection