<div class="form-group col-sm-12" id="root">

{{--    @if(!$item->esCombo())--}}
    <div class="card card-outline card-danger ">
        <div class="card-header">
            <h3 class="card-title">Ingredientes</h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body">

            @isset($item)

                <div class="row">
                    <div class="col">
                        <span class="text-muted">Seleccione 2 o mas para que el ingrediente se opcional</span>
                    </div>
                </div>
                <div class="row mb-4">
                    <div class="col-sm-11">
                        <select name="tags[]" class="custom-select" multiple="multiple" id="ingredientes" style="width: 100%"></select>
                    </div>
                    <div class="col-sm-1">
                        <button type="button" class="btn btn-outline-success btn-sm" @click="agregarIngrediente()">
                            Agregar
                        </button>
                    </div>
                </div>


                <div class="row mt-2">
                    <div class="mx-1" v-for="(detalle,index) in receta_detalles">
                        <div class="card" >
                            <div class="card-header py-1">
                                <h5 class="card-title"><span v-text="index+1"></span></h5>

                                <div class="card-tools" style="top: 1px !important;">
                                    <button type="button" class="btn btn-outline-danger btn-xs m-0" @click="removerIngrediente(detalle)">
                                        <i class="fa fa-trash"></i>
                                    </button>
                                </div>
                                <!-- /.card-tools -->
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body p-1">
                                <div class="row py-0">
                                    <template v-if="detalle.opcionales.length > 0">
                                        <template v-for="(ingrediente,subIndex) in detalle.opcionales">
                                            <div class="col py-0" >
                                                <div class="card mb-0" style="width: 7rem; margin-bottom: .2rem !important;" >

                                                    <img class="card-img-top" :src="ingrediente.img" alt="Card image cap">
                                                    <div class="card-body p-0">
                                                        <span v-text="ingrediente.nombre"></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- Si no es el ultimo -->
                                            <div class="col p-0 " v-if="(subIndex+1) < detalle.opcionales.length">
                                                <br>
                                                ó
                                            </div>
                                        </template>
                                    </template>
                                    <template v-else>
                                        <div class="col py-0" >
                                            <div class="card mb-0" style="width: 7rem; margin-bottom: .2rem !important;" >

                                                <img class="card-img-top" :src="detalle.ingrediente.img" alt="Card image cap">
                                                <div class="card-body p-0">
                                                    <span v-text="detalle.ingrediente.nombre"></span>
                                                </div>
                                            </div>
                                        </div>
                                    </template>


                                </div>
                            </div>
                            <!-- /.card-body -->
                        </div>
                    </div>
                </div>

                <p class="text-danger text-dark text-center" v-if="receta_detalles.length == 0">
                    NINGUN INGREDIENTE AGREGADO
                </p>
            @else

                <h5 class="text-info text-center">
                    Guarde primero los datos del articulo para poder asociar ingredientes
                </h5>
            @endisset


        </div>
        <!-- /.card-body -->
    </div>
    {{--@else--}}
        {{--<h2 class="text-center text-danger">Este articulo es combo por lo que solo puede asociarle salsas</h2>--}}
    {{--@endif--}}

    {{--@if(!$item->esCombo())--}}
        <div class="card card-outline card-danger ">
    <div class="card-header">
        <h3 class="card-title">Extras</h3>

        <div class="card-tools">
            <button type="button" class="btn btn-tool" data-widget="remove"><i class="fa fa-times"></i>
            </button>
            <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
        </div>
        <!-- /.card-tools -->
    </div>
    <!-- /.card-header -->
    <div class="card-body">

        <div class="row">
            <div class="col">
                <div class="card ">
                    <div class="card-header">
                        <h3 class="card-title">Disponibles</h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-sm table-striped">
                                <thead>
                                <tr>
                                    <th>Item</th>
                                    <th>Acción</th>
                                </tr>
                                </thead>
                                <tbody>
                                    <tr v-for="adicional in adicionales">
                                        <td v-text="adicional.nombre"></td>
                                        <td>
                                            <button type="button"class="btn btn-success btn-xs" @click="attachAgregado(adicional.id,'adicional')">
                                                Asociar
                                            </button>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- /.card-body -->
                </div>
            </div>
            <div class="col">
                <div class="card ">
                    <div class="card-header">
                        <h3 class="card-title">Asociados</h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-sm table-striped">
                                <thead>
                                <tr>
                                    <th>Item</th>
                                    <th>Acción</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr v-for="adicional in adicionales_item">
                                    <td v-text="adicional.nombre"></td>
                                    <td>
                                        <button type="button" class="btn btn-danger btn-xs" @click="detachAgregado(adicional.id,'adicional')">
                                            Quitar
                                        </button>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- /.card-body -->
                </div>
            </div>
        </div>

    </div>
    <!-- /.card-body -->
</div>
    {{--@endif--}}

    <div class="card card-outline card-danger ">
        <div class="card-header">
            <h3 class="card-title">Salsas</h3>

            <div class="card-tools">
                <button type="button" class="btn btn-tool" data-widget="remove"><i class="fa fa-times"></i>
                </button>
                <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
            </div>
            <!-- /.card-tools -->
        </div>
        <!-- /.card-header -->
        <div class="card-body">

            <div class="row">
                <div class="col">
                    <div class="card ">
                        <div class="card-header">
                            <h3 class="card-title">Disponibles</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-sm table-striped">
                                    <thead>
                                    <tr>
                                        <th>Item</th>
                                        <th>Acción</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr v-for="salsa in salsas">
                                        <td v-text="salsa.nombre"></td>
                                        <td>
                                            <button type="button"class="btn btn-success btn-xs" @click="attachAgregado(salsa.id,'salsa')">
                                                Asociar
                                            </button>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- /.card-body -->
                    </div>
                </div>
                <div class="col">
                    <div class="card ">
                        <div class="card-header">
                            <h3 class="card-title">Asociados</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-sm table-striped">
                                    <thead>
                                    <tr>
                                        <th>Item</th>
                                        <th>Acción</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr v-for="salsa in salsas_item">
                                        <td v-text="salsa.nombre"></td>
                                        <td>
                                            <button type="button" class="btn btn-danger btn-xs" @click="detachAgregado(salsa.id,'salsa')">
                                                Quitar
                                            </button>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- /.card-body -->
                    </div>
                </div>
            </div>

        </div>
        <!-- /.card-body -->
    </div>
</div>


@push('scripts')
    <script>
        $(function () {

            function formatIngrediente (state) {

                var $state = $(`
                <div class='row p-0'>
                    <div class='col-sm-2 p-0'>
                        <img src=' ` + state.img + `' class='img-responsive' alt='Image' width='80px'/>
                    </div>
                    <div class='col-sm-10'>
                        `+state.nombre+`
                    </div>
                </div>`
                );

                return $state;
            };

            slc2ing=$("#ingredientes").select2({
                language : 'es',
                // maximumSelectionLength: 1,
                placeholder: "Escriba para buscar ingredientes",
                delay: 250,
                ajax: {
                    url: "{{ route('api.ingredientes.index') }}",
                    dataType: 'json',
                    data: function (params) {
                        return {
                            search: params.term
                        };
                    },
                    processResults: function (data, params) {
                        return {
                            results: data.data,
                        };
                    },
                    cache: true
                },
                minimumInputLength: 1,
                templateResult: formatIngrediente,
                templateSelection: function(data,contenedor) {
                    return data.nombre;
                }
            }).on('select2:select',function (e) {

                // vm.ingredientes.push(e.params.data);
                vm.ingredientes = $("#ingredientes").val();
                slc2ing.select2('open');

            })

        });

        var vm = new Vue({
            el: '#root',
            mounted(){
                this.getIngredinetes();
                this.getAgregados();
                this.getAgregadosItem();
            },
            data: {
                item_id : '{{$item->id or ''}}',
                ingredientes: [],
                receta_detalles: [],
                adicionales: [],
                adicionales_item: [],
                salsas: [],
                salsas_item: [],
            },
            methods: {
                getIngredinetes(){

                    var url = "{{route("item.get.ingredientes")}}";
                    var params = {params: {id :this.item_id}};

                    axios.get(url,params).then(response => {
                        // console.log(response.data);
                        this.receta_detalles = response.data.data;
                    })
                        .catch(error => {
                            console.log(error.response.data);
                        });

                },
                agregarIngrediente(){
                    var ingrediente = this.ingredientes;
                    // console.log('agregarIngrediente',ingrediente);
                    if (ingrediente.length==0){
                        toastr.error('seleccione o agregue un ingrediente en el campo de selección');
                        return false;
                    }

                    var url = "{{route("items.add.ingrediente")}}";

                    var params= { params: {item_id: this.item_id, ingrediente: ingrediente} };

                    axios.get(url,params).then(response => {
                        console.log(response.data);

                        this.ingredientes = [];
                        this.getIngredinetes();

                        slc2ing.empty().trigger('change');
                        slc2ing.select2('open');

                        toastr.success(response.data.message);
                    })
                        .catch(error => {
                            console.log(error.response.data);
                            toastr.error(error.response.data.message);
                        });


                },
                removerIngrediente(detalle){

                    console.log('removerIngrediente(detalle)',detalle);

                    var url = "{{route("item.remover.ingredientes")}}";

                    var params= { params: {id: detalle.id} };

                    axios.get(url,params).then(response => {
                        console.log(response.data);
                        toastr.success(response.data.message);
                        this.getIngredinetes();
                    })
                        .catch(error => {
                            console.log(error.response.data);
                            toastr.error(error.response.data.message);
                        });


                },
                getAgregados(){

                    console.log('Get all agregados');
                    var url = "{{route("item.get.agregados")}}";

                    axios.get(url).then(response => {
                        console.log('respuesta getAgregados',response.data);
                        this.adicionales = response.data.data.adicionales;
                        this.salsas = response.data.data.salsas;
                    })
                    .catch(error => {
                        console.log(error.response.data);
                    });

                },
                getAgregadosItem(){

                    console.log('Get agregados item');
                    var url = "{{route("item.get.agregados")}}";
                    var params= { params: {item_id: this.item_id} };

                    axios.get(url,params).then(response => {
                        console.log('respuesta getAgregadosItem',response.data);
                        this.adicionales_item = response.data.data.adicionales;
                        this.salsas_item = response.data.data.salsas;
                    })
                    .catch(error => {
                        console.log(error.response.data);
                    });

                },
                attachAgregado(id,tipo){
                    var url = "{{route("item.attach.agregados")}}";

                    var params= { params: {item_id: this.item_id,id_agregado: id,tipo: tipo} };
                    axios.get(url,params).then(response => {
                        console.log(response.data);
                        toastr.success(response.data.message);
                        this.getAgregadosItem();
                        //this.models = response.data.data;
                    })
                    .catch(error => {
                        console.log(error.response);
                        toastr.error(error.response.data.message);
                    });
                },
                detachAgregado(id,tipo){
                    var url = "{{route("item.detach.agregados")}}";

                    var params= { params: {item_id: this.item_id,id_agregado: id,tipo: tipo} };

                    axios.get(url,params).then(response => {
                        console.log(response.data);
                        toastr.success(response.data.message);
                        this.getAgregadosItem();
                        //this.models = response.data.data;
                    })
                    .catch(error => {
                        console.log(error.response);
                        toastr.error(error.response.data.message);
                    });
                },
            }
        });


    </script>
@endpush