@extends('layouts.app')

@section('htmlheader_title')
    Artículo
@endsection

@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-12">
                    <h1 class="m-0 text-dark">Artículo</h1>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->


    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row" >
                                <div class="col-sm-6" align="center">
                                    @if($item->imagen)
                                        <img src="{{asset($item->imagen)}}" class="img-fluid" alt="Image">
                                    @else
                                        <img src="{{asset('img/avatar_none.png')}}" class="img-responsive" alt="Image" width="400px" height="400px">
                                    @endif
                                </div>
                                <div class="col-sm-6">

                                    @include('items.show_fields')
                                </div>

                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                                    <a href="{!! route('items.index') !!}" class="btn btn-default">Regresar</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col-md-6 -->
            </div>
            <!-- /.row -->

        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /.content -->
@endsection
