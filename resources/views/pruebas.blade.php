@extends('layouts.app')

@section('content')

    <h1>Vista de pruebas</h1>

    <h2>{{$datos}}</h2>

    <div class="container">
        <div class="row">
            <div class="mx-1">
                <div class="card">
                    <div class="card-header py-1">
                        <h5 class="card-title"><span>1</span></h5>
                        <div class="card-tools" style="top: 1px !important;">
                            <button type="button" class="btn btn-outline-danger btn-xs m-0">
                                <i class="fa fa-trash"></i>
                                <!-- <i class="fa fa-trash"></i> -->
                            </button>
                        </div>
                    </div>
                    <div class="card-body p-1">
                        <div class="row py-0">
                            <div class="col py-0">
                                <div class="card " style="width: 7rem; margin-bottom: .2rem !important;">
                                    <img src="http://fritomania.local/storage/ingredientes/c8d7c7b97faa90862f18eb988cf56140.jpg" alt="Card image cap" class="card-img-top">
                                    <div class="card-body p-0"><span>Pollo</span></div>
                                </div>
                            </div>
                            <div class="col p-0 ">
                                <br>
                                ó
                            </div>
                            <div class="col py-0">
                                <div class="card mb-0" style="width: 7rem; margin-bottom: .2rem !important;">
                                    <img src="http://fritomania.local/storage/ingredientes/3b52e1d8fd1d5b54824e48b74482e9f1.jpg" alt="Card image cap" class="card-img-top">
                                    <div class="card-body p-0"><span>Res</span></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection