@extends('layouts.app')

@section('htmlheader_title')
    Crear Venta
@endsection


@include('layouts.plugins.select2')
@include('layouts.xtra_condensed_css')
@include('layouts.plugins.bootstrap_datetimepicker')
@include('ventas.script')
@include('layouts.plugins.fancy_box')
@include('layouts.plugins.sweetalert2')

@push('css')
<style>
    .select2-dropdown {
        z-index: 1039;
    }
</style>
@endpush

@section('content')
    {{--<div class="content-header">--}}
    {{--<div class="container-fluid">--}}
    {{--<div class="row">--}}
    {{--<div class="col">--}}
    {{--<h1 class="m-0 text-dark">Nueva Venta</h1>--}}
    {{--</div><!-- /.col -->--}}
    {{--</div><!-- /.row -->--}}
    {{--</div><!-- /.container-fluid -->--}}
    {{--</div>--}}

    <!-- Main content -->
    <div class="content" id="root">
        <div class="container-fluid">
            <ul class="nav nav-tabs" id="myTab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">
                        Venta
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">
                        Ordenes
                        <span data-toggle="tooltip" title="En espera" class="badge badge-warning" v-text="noListas">15</span>
                        <span data-toggle="tooltip" title="Listas" class="badge badge-success" v-text="listas">15</span>
                    </a>
                </li>
            </ul>
            <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade show active p-2" id="home" role="tabpanel" aria-labelledby="home-tab">
                    @include('flash::message')
                    @include('layouts.errores')
                    {!! Form::model($tempVenta, ['route' => ['venta.store', $tempVenta->id], 'method' => 'post']) !!}
                    <div class="row">

                        <!--Artículos-->
                        <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
                            <div class="card card-info">
                                <div class="card-header with-border">
                                    <h3 class="card-title">
                                        <strong>Artículos</strong>
                                    </h3>
                                    <div class="card-tools pull-right">
                                        <button type="button" class="btn btn-tool" data-toggle="tooltip" title="Búsqueda avanzada" @click="tipoBusqueda = tipoBusqueda=='S' ? 'A' :'S'">
                                            <i class="fa fa-toggle-off" v-if="tipoBusqueda=='S'"></i>
                                            <i class="fa fa-toggle-on" v-else ></i>
                                        </button>
                                        <button class="btn btn-tool" data-widget="collapse" tabindex="1000"><i class="fa fa-minus"></i></button>
                                        {{--<button class="btn btn-tool" data-widget="remove"><i class="fa fa-times"></i></button>--}}
                                    </div>
                                </div>
                                <!-- /.card-header -->
                                <div class="card-body">
                                    <div v-show="tipoBusqueda == 'A'">
                                        <div class="form-group">
                                            <select  id="items" class="form-control" multiple="multiple" size="10" style="width: 100%;" >
                                                <option value=""> -- Select One -- </option>
                                            </select>
                                        </div>
                                        <div class="row mt-5">
                                            <div class="form-group col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">Cant</span>
                                                    </div>
                                                    <input v-model="nuevoDetalle.cantidad" type="text" name="cantidad" id="cant-new-det"  class="form-control"  value="1" data-toggle="tooltip" title="Doble Enter para agregar">
                                                </div>
                                            </div>
                                            <div class="form-group  col-xs-12 col-sm-6 col-md-6 col-lg-6 ">
                                                <div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">{{dvs()}}</span>
                                                    </div>
                                                    <input v-model="nuevoDetalle.precio" type="number" min="0" step="1" id="precio-new-det" class="form-control" placeholder="Precio venta" data-toggle="tooltip" title="Doble Enter para agregar">
                                                    <div class="input-group-append">
                                                        <button type="button" id="btn-agregar" class="btn btn-danger" v-on:click.prevent="createDet" :disabled="loadingBtnAdd" >
                                                            <span v-show="loadingBtnAdd" >
                                                                <i class="fa fa-sync-alt fa-spin"></i>
                                                            </span>
                                                            <span v-show="!loadingBtnAdd" class="glyphicon glyphicon-plus"></span>
                                                            Agregar
                                                        </button>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        <div id="div-info-item"></div>
                                    </div>
                                    <div v-show="tipoBusqueda == 'S'">

                                        <div class="form-group" >
                                            <div class="input-group">

                                                <input type="text" v-model="codigo"
                                                       @keypress.enter.prevent="addDetCod" ref="search"
                                                       class="form-control"  placeholder="Código"
                                                       @focus="$event.target.select()">
                                                <span class="input-group-btn">
                        <button type="button" id="btn-agregar" class="btn btn-danger" v-on:click.prevent="addDetCod" :disabled="loadingBtnAdd" >
                            <i  v-show="loadingBtnAdd" class="fa fa-spinner fa-spin"></i>
                            <span v-show="!loadingBtnAdd" class="glyphicon glyphicon-plus"></span>
                            Agregar
                        </button>
                    </span>
                                            </div>
                                        </div>
                                    </div>

                                    @include('ventas.tabla_det_vue')
                                </div>

                            </div>
                        </div>
                        <!--Artículos-->

                        <!--Resumen-->
                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">

                            <div class="card card-info card-solid">
                                <div class="card-header with-border">
                                    <h3 class="card-title">
                                        <strong>
                                            VENTA <small> iniciada: {{fechaHoraLtn($tempVenta->created_at)}}</small>
                                        </strong>
                                    </h3>
                                </div>
                                <!-- /.card-header -->
                                <div class="card-body" style="padding: 0px;">


                                    @include('ventas.fields')


                                </div>
                            </div><!-- /.row -->

                        </div>
                        <!--/Resumen-->

                    </div>
                    {!! Form::close() !!}
                </div>
                <div class="tab-pane fade p-2" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                    <ordenes-venta
                            api="{{route('ordenes.datos')}}"
                            url_cambio_estado="{{route('ordenes.cambio.estado')}}"
                            lugar="caja"
                            tienda="{{ session('tienda') }}"
                            url_repartidores ="{{ route('get.repartidores.ajax') }}"
                            url_attach="{{ route('orden.attach') }}"
                            url_save_notify="{{route('api.notificaciones.store')}}"
                            url_imprime="{{route('orden.print.cocina',0)}}"
                            url_anulacion="{{route('ventas.anular.ajax')}}"
                            url_edit="{{route('ventas.edit',0)}}"
                            tipo_notify="{{\App\Models\NotificacionTipo::DELIVERY}}"
                    ></ordenes-venta>
                </div>

            </div>

        </div>
        <!-- /.container-fluid -->

        @include('ventas.modal_edit_ficha')

        @include('cajas.modal_abrir')
    </div>
    <!-- /.content -->

    @include('clientes.modal_form')
    @include('ventas.edit_modal_detalle')



@endsection
