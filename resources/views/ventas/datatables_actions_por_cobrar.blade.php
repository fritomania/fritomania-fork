
    {{--<a href="{{ route('ventas.show', $id) }}" class='btn btn-info btn-xs' data-toggle="tooltip" title="Ver detalles">--}}
    <a href="{{route('ventas.abonar',$id)}}" class='btn btn-info btn-xs' data-toggle="tooltip" title="Ver detalles">
        <i class="fa fa-eye"></i>
    </a>

    @if(Auth::user()->isAdmin())
    <a href="#modal-delete-{{$id}}" data-toggle="modal" class='btn btn-danger btn-xs'>
        <i class="fa fa-trash-alt" data-toggle="tooltip" title="Anular venta"></i>
    </a>
    @endif

    <div class="modal fade modal-warning" id="modal-delete-{{$id}}">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Anular venta!</h4>
                </div>
                <div class="modal-body">
                    Seguro que desea anular la venta?
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                    <a href="{{route('ventas.anular',['id' => $id])}}" class="btn btn-danger">
                        SI
                    </a>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->