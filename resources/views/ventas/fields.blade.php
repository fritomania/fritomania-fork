<ul class="list-group list-group-flush">
    <!--            Clientes
    ------------------------------------------------------------------------>
    <li class="list-group-item p-1">
        <div class="form-group col-sm-12">
            <label for="clients" class="control-label">Cliente: <a class="success" data-toggle="modal" href="#modal-form-clientes" tabindex="1000">nuevo</a></label>
            {!! Form::select('cliente_id', $clientes ,1,['class' => 'form-control', 'id'=>'clientes','multiple'=>"multiple",'style' => "width: 100%"]) !!}
        </div>
    </li>

    <!--            Numero productos
    ------------------------------------------------------------------------>
    <li class="list-group-item pt-1 pb-1 text-bold">
        No. Productos:
        <span class="float-right" v-text="totalitems"></span>
    </li>

    <!--            Total
    ------------------------------------------------------------------------>
    <li class="list-group-item pt-1 pb-1 text-bold ">
        Total
        <span class="float-right" >
            {{dvs()}} <span v-text="numf(total.toFixed(0))"></span>
        </span>
    </li>

    <!--            Vuelto
    ------------------------------------------------------------------------>
    <li class="list-group-item pt-1 pb-1 text-bold ">
        Vuelto
        <span class="float-right">
            {{dvs()}} <span v-text="numf(vuelto)"></span>
        </span>
    </li>

    {{--<!--            Factura--}}
    {{-------------------------------------------------------------------------->--}}
    {{--<li class="list-group-item text-bold ">--}}
        {{--Factura--}}
        {{--<span class="float-right">--}}

             {{--<toggle-button v-model="picked"--}}
                            {{--:sync="true"--}}
                            {{--:labels="{checked: 'SI', unchecked: 'NO'}"--}}
                            {{--:height="30"--}}
                            {{--:width="60"--}}
                            {{--:value="false"/>--}}
        {{--</span>--}}
    {{--</li>--}}

    {{--<!--            Numero y serie--}}
    {{-------------------------------------------------------------------------->--}}
    {{--<li class="list-group-item text-bold p-1" v-show="picked">--}}
        {{--<!-- Serie Field -->--}}
        {{--<div class="form-group col-sm-12">--}}
            {{--<div class="input-group ">--}}
                {{--<div class="input-group-prepend">--}}
                    {{--<span class="input-group-text">S</span>--}}
                {{--</div>--}}
                {{--{!! Form::text('serie', null, ['class' => 'form-control','placeholder'=>'Serie']) !!}--}}
                {{--<div class="input-group-prepend">--}}
                    {{--<span class="input-group-text">N</span>--}}
                {{--</div>--}}
                {{--{!! Form::text('numero', null, ['class' => 'form-control','placeholder'=>'Número']) !!}--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</li>--}}

    {{--<!--            Crédito--}}
    {{-------------------------------------------------------------------------->--}}
    {{--<li class="list-group-item text-bold ">--}}
        {{--Credito--}}
        {{--<input type="hidden" name="credito" :value="credito ? 1 : 0">--}}
        {{--<span class="float-right">--}}
             {{--<toggle-button v-model="credito"--}}
                            {{--:sync="true"--}}
                            {{--:labels="{checked: 'SI', unchecked: 'NO'}"--}}
                            {{--:height="30"--}}
                            {{--:width="60"--}}
                            {{--:value="false"--}}
             {{--/>--}}
        {{--</span>--}}

    {{--</li>--}}

    {{--<!--            Monto inicial--}}
    {{-------------------------------------------------------------------------->--}}
    {{--<li class="list-group-item" v-show="credito">--}}
        {{--<input type="text" name="monto_ini" id="" class="form-control" title="Abono inicial" placeholder="Abono inicial (pocional)" v-model="abono_ini">--}}
    {{--</li>--}}

    <!--            Delivery
    ------------------------------------------------------------------------>
    <li class="list-group-item pt-2 pb-1 text-bold ">
        Delivery
        <input type="hidden" name="delivery" :value="delivery ? 1 : 0">
        <span class="float-right">
             <toggle-button v-model="delivery"
                            :sync="true"
                            :labels="{checked: 'SI', unchecked: 'NO'}"
                            :height="30"
                            :width="60"
                            :value="false"
             />
        </span>
    </li>

    <!--            Monto Delivery
    ------------------------------------------------------------------------>
    <li class="list-group-item p-1" v-show="delivery">
        <div class="input-group">
            <div class="input-group-prepend">
                <div class="input-group-text">
                    Monto
                </div>
            </div>
            <input type="text" name="monto_delivery" id="" class="form-control" title="Costo Delivery" placeholder="Costo Delivery" v-model="abono_delivery">
        </div>
    </li>

    <!--            Direccion Delivery
    ------------------------------------------------------------------------>
    <li class="list-group-item p-1" v-show="delivery" >
        <div class="input-group">
            <textarea type="text" name="direccion" id="" class="form-control" title="Dirección de entrega" placeholder="Dirección de entrega" rows="3" >{{old('direccion')}}</textarea>
        </div>
    </li>

    <!--            Programar entrega
    ------------------------------------------------------------------------>
    <li class="list-group-item pt-2 pb-1 text-bold ">
        Programar entrega
        <input type="hidden" name="programa_entrega" :value="programa_entrega ? 1 : 0">
        <span class="float-right">
             <toggle-button v-model="programa_entrega"
                            :sync="true"
                            :labels="{checked: 'SI', unchecked: 'NO'}"
                            :height="30"
                            :width="60"
                            :value="false"
             />
        </span>
    </li>

    <!--            Fecha Entrega
    ------------------------------------------------------------------------>
    <li class="list-group-item" v-show="programa_entrega">
        <div class="input-group">
            <div class="input-group-append">
                <span class="input-group-text">
                    <i class="fa fa-calendar" aria-hidden="true"></i>
                </span>
            </div>
            <input type="date" name="fecha_entrega" class="form-control" placeholder="Fecha Entrega" value="" :required="programa_entrega" >
        </div>
        <div class="input-group">
            <div class="input-group-append">
                <span class="input-group-text">
                    <i class="fa fa-clock" aria-hidden="true"></i>
                </span>
            </div>
            <input type="time" name="hora_entrega" class="form-control" placeholder="Hora Entrega" :required="programa_entrega">
        </div>
    </li>

    <!--            Tipo pago
    ------------------------------------------------------------------------>
    <li class="list-group-item pl-2 pr-2">
        <div class="input-group">
            <div class="input-group-prepend">
                <label class="input-group-text" for="tipo_pago_id">Tipo Pago</label>
            </div>
            {!!
                Form::select(
                    'tipo_pago_id',
                    \App\Models\TipoPago::whereNotIn('id',[\App\Models\TipoPago::WEBPAY])->pluck('nombre', 'id')->toArray()
                    , null
                    , ['id'=>'tipo_pago_id','class' => 'custom-select']
                )
            !!}
        </div>

    </li>

    <!--            Observaciones
    ------------------------------------------------------------------------>
    <li class="list-group-item p-1">
        <div class="input-group">
            <textarea
                   name="observaciones"
                   id="observaciones"
                   @focus="$event.target.select()"
                   class="form-control"
                   rows="2"
                   placeholder="Observaciones"
            >{{old('observaciones')}}</textarea>
        </div>
    </li>

    <!--            Recibido y procesar
    ------------------------------------------------------------------------>
    <li class="list-group-item p-0 pt-2">
        <div class="form-group col-sm-12">
            <label for="clients" class="control-label">Recibido: </label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text">{{dvs()}}</span>
                </div>
                <input type="text" name="recibido" id="recibido" v-model="recibido" @focus="$event.target.select()" class="form-control" placeholder="{{dvs()}} Recibido" data-toggle="tooltip" title="Doble Enter para procesar">
                <div class="input-group-append">
                    <button type="submit" id="btn-procesar" name="procesar" value="1" class="btn btn-danger" :disabled="!habilitaProcesar" data-loading-text="<i class='fa fa-cog fa-spin fa-1x fa-fw'></i> Procesando">
                        <span data-toggle="tooltip" title="Ingrese un monto mayor al total para procesar">

                        <span class="glyphicon glyphicon-ok" ></span> Procesar
                        </span>
                    </button>
                    <a class="btn btn-danger" data-toggle="modal" href="#modal-cancel-venta">
                        <span data-toggle="tooltip" title="Cancelar venta">X</span>
                    </a>
                </div>
            </div>
            <span v-show="faltaArmarCombos">
                <span class="text-danger">
                    *Faltan combos por armar
                </span>
            </span>
        </div>
    </li>
</ul>

<!-- Fecha Field -->
{{--<div class="form-group col-sm-12">--}}
{{--{!! Form::label('fecha', 'Fecha:') !!}--}}
{{--{!! Form::text('fecha', \Carbon\Carbon::today()->format('d/m/Y'), ['class' => 'form-control']) !!}--}}
{{--</div>--}}

<div class="modal fade modal-warning" id="modal-cancel-venta">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Cancelar venta</h5>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <div class="modal-body">
                Seguro que desea cancelar la venta?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">NO</button>
                <a href="{{route('ventas.cancelar',['id' => $tempVenta->id])}}" class="btn btn-danger">
                    SI
                </a>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

