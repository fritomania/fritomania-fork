<!-- Modal -->
<div class="modal fade" id="modalEditItem" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog modal-notify modal-warning modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Editar items y agregados</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <h1 class="text-center" v-if="loadingReceta || loadingAgregados">
                    <i class="fa fa-sync fa-spin" > </i>
                </h1>

                <div v-else>
                    <form id="datos-item-modifica">

                        <div class="row" >
                            <div class="col-sm-6" v-for="itemNumber in cantidadItemsModal" style="padding: 2px">
                                <div class="card card-danger">
                                    <div class="card-heading">
                                        <h3 class="card-title">
                                            <b>
                                                <span v-text="nombreItemDetEdit+' '+itemNumber"></span>
                                            </b>
                                        </h3>
                                    </div>
                                    <div class="card-body">
                                        <!--            Detalles receta (ingredientes)
                                        ------------------------------------------------------------------------>
                                        <ul>
                                            <li v-for="det in recetaDetalles">
                                                <template v-if="det.opcionales.length > 0">

                                                    <div class="form-check form-check-inline" v-for="(ingrediente,index) in det.opcionales">

                                                        <input
                                                            class="form-check-input"
                                                            type="radio"
                                                            :name="'modificaciones['+itemNumber+']['+det.id+']'"
                                                            :checked="index===0"
                                                            :value="ingrediente.id"
                                                        >

                                                        <label class="form-check-label">
                                                            <span v-text="ingrediente.nombre"></span>
                                                        </label>
                                                    </div>

                                                    {{--<div class="radio-inline">--}}
                                                        {{--<label>--}}

                                                            {{--<input--}}
                                                            {{--type="radio"--}}
                                                            {{--:name="'modificaciones['+itemNumber+']['+det.id+']'"--}}
                                                            {{--value="0">--}}
                                                            {{--Sin <span v-text="det.categoria"></span>--}}
                                                            {{--</label>--}}
                                                        {{--</div>--}}

                                                </template>
                                                <template v-else>

                                                    <label class="checkbox-inline">
                                                        <input
                                                            type="checkbox"
                                                            :name="'modificaciones['+itemNumber+']['+det.id+']'"
                                                            :value="det.ingrediente.id"
                                                            checked>
                                                        <span v-text="det.ingrediente.nombre"></span>
                                                    </label>
                                                </template>
                                            </li>
                                            <li v-if="recetaDetalles.length == 0">
                                                Sin ingredientes definidos
                                            </li>
                                        </ul>
                                        <hr>

                                        <!--            Adicionales
                                        ------------------------------------------------------------------------>
                                        <b>Adicionales</b>
                                        <ul>
                                            <li v-for="(adicional,aIndex) in adicionales">
                                                <label class="checkbox-inline">
                                                    <input
                                                        type="checkbox"
                                                        :id="'adicionales'+itemNumber.toString()+aIndex.toString()"
                                                        :name="'adicionales['+itemNumber+'][]'"
                                                        :value="adicional.id"
                                                    >
                                                    <span v-text="adicional.nombre"></span> /&nbsp;
                                                    {{dvs()}} <span v-text="nf(adicional.precio_venta)"></span>
                                                </label>

                                            </li>
                                            <li v-if="adicionales.length == 0">
                                                Sin adicionales asociados
                                            </li>
                                        </ul>
                                        <hr>

                                        <!--            Salsas
                                        ------------------------------------------------------------------------>
                                        <b>Salsas</b>
                                        <ul>
                                            <li v-for="(salsa,sIndex) in salsas">
                                                <label class="checkbox-inline">
                                                    <input type="checkbox"
                                                           :id="'salsas'+itemNumber.toString()+sIndex.toString()"
                                                           :name="'salsas['+itemNumber+'][]'"
                                                           :value="salsa.id"
                                                    >
                                                    <span v-text="salsa.nombre"></span> /&nbsp;
                                                    {{dvs()}}<span v-text="nf(salsa.precio_venta)"></span>
                                                </label>
                                            </li>
                                            <li v-if="salsas.length == 0">
                                                Sin salsas asociadas
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-primary" @click="guardarModificacion(detEdit)">
                    Guardar &nbsp;
                    <span v-show="loadingModificaciones">
                        <i class="fa fa-sync fa-spin" > </i>
                    </span>
                </button>
            </div>
        </div>
    </div>
</div>