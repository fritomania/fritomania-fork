@extends('layouts.app')

@section('htmlheader_title')
	LISTADO DE VENTAS
@endsection

@include('layouts.plugins.select2')

@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row">
                <div class="col">
                    <h1 class="m-0 text-dark">LISTADO DE VENTAS</h1>
                </div><!-- /.col -->
                <div class="col">
                    <ol class="breadcrumb float-right">
                        <li class="breadcrumb-item">
                            <a class="btn btn-outline-success"
                                href="{!! route('ventas.create') !!}">
                                <i class="fa fa-plus"></i>
                                <span class="d-none d-sm-inline">Nueva Venta</span>
                            </a>
                        </li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            @include('flash::message')

            <div class="row">
                <div class="col">
                    <div class="card card-outline card-success">
                        <div class="card-header p-1">
                            <h3 class="card-title">Filtros</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body p-2">
                            {!! Form::open(['rout' => 'ventas.index','method' => 'get']) !!}
                            <div class="form-row">
                                <div class="form-group col-sm-4">
                                    {!! Form::label('cliente_id','Cliente:') !!}
                                    {!!
                                        Form::select(
                                            'cliente_id',
                                            array_prepend(\App\Models\Cliente::pluck('nombres','id')->toArray(),'Todos','')
                                            , $cliente_id
                                            , ['id'=>'clientes','class' => 'form-control select2-simple','multiple','style'=>'width: 100%']
                                        )
                                    !!}
                                </div>
                                <div class="form-group col-sm-2">
                                    {!! Form::label('del', 'Del:') !!}
                                    {!! Form::date('del', $del, ['class' => 'form-control ']) !!}
                                </div>
                                <div class="form-group col-sm-2">
                                    {!! Form::label('al', 'Al:') !!}
                                    {!! Form::date('al', $al, ['class' => 'form-control ']) !!}
                                </div>
                                <div class="form-group col-sm-4">
                                    {!! Form::label('item_id','Artículo:') !!}
                                    {!!
                                        Form::select(
                                            'item_id',
                                            array_prepend(
                                                \App\Models\Item::tipoProducto()->pluck('nombre','id')->toArray()
                                                ,'Todos',''
                                            )
                                            , $item_id
                                            , ['id'=>'items','class' => 'form-control select2-simple','multiple','style'=>'width: 100%']
                                        )
                                    !!}
                                </div>

                                <div class="form-group col-sm-4">
                                    {!! Form::label('item_id','Estados:') !!}
                                    {!!
                                        Form::select(
                                            'estado',
                                            slc(\App\Models\Vestado::class,'Todos','','descripcion')
                                            , $estado
                                            , ['id'=>'estados','class' => 'form-control select2-simple','multiple','style'=>'width: 100%']
                                        )
                                    !!}
                                </div>

                                @if(Auth::user()->isAdmin() || Auth::user()->isSuperAdmin())
                                <div class="form-group col-sm-4">
                                    {!! Form::label('tienda','Lugar:') !!}
                                    {!!
                                        Form::select(
                                            'tienda',
                                            slc(\App\Models\Tienda::class,'Todos','T')
                                            , $tienda
                                            , ['id'=>'tiendas','class' => 'form-control select2-simple','multiple','style'=>'width: 100%']
                                        )
                                    !!}
                                </div>
                                @endif

                                <div class="form-group col-sm-6">
                                    {!! Form::label('boton','&nbsp;') !!}
                                    <div>
                                        <button type="submit" id="boton" class="btn btn-info">
                                            <i class="fa fa-sync"></i> Filtrar
                                        </button>
                                    </div>
                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>
                        <!-- /.card-body -->
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                           @include('ventas.table')
                        </div>
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col-md-6 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /.content -->
@endsection

