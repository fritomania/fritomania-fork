<div class="table-responsive">
    <table class="table table-bordered table-sm" id="tablaDetalle" style="margin-bottom: 2px">
        <thead>
        <tr align="center">
            <th width="50%">Producto</th>
            <th width="15%">Fecha Vence</th>
            <th width="10%">Cantidad</th>
        </tr>
        </thead>
        <tbody>
        @foreach($venta->ventaDetalles as $detalle)
            @foreach($detalle->stocks as $egreso)
                <tr class="{{$detalle->stocks->count()>1 ? 'table-danger' : ''}} ">
                    <td>{{$detalle->item->nombre }}</td>
                    <td>{{ fecha($egreso->fecha_ven )}}</td>
                    <td class="text-right">{{ $egreso->pivot->cantidad }}</td>
                </tr>
            @endforeach
        @endforeach
        <tr class="text-bold">
            <td >
                Total Articulos
            </td>
            <td></td>
            <td class="text-right" >
                {{number_format(
                    array_sum(array_pluck($venta->ventaDetalles,'cantidad'))
                    ,2
                  )}}
            </td>

        </tr>
        </tbody>
    </table>
</div>