    @php
        $venta=\App\Models\Venta::find($id);
    @endphp

    {{--<a href="{{ route('ventas.show', $id) }}" class='btn btn-info btn-xs' data-toggle="tooltip" title="Ver detalles">--}}
    <a href="#modal-detalles-{{$id}}" data-keyboard="true" data-toggle="modal" class='btn btn-info btn-xs' data-toggle="tooltip" title="Ver detalles">
        <i class="fa fa-eye"></i>
    </a>

    @if(Auth::user()->isAdmin() && $venta->vestado_id!=2)
        <a href="#modal-delete-{{$id}}" data-toggle="modal" class='btn btn-danger btn-xs'>
            <i class="fa fa-trash-alt" data-toggle="tooltip" title="Anular venta"></i>
        </a>
    @endif

    <div class="modal fade modal-warning" id="modal-delete-{{$id}}">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Anular venta!</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body">
                    Seguro que desea anular la venta?
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                    <a href="{{route('ventas.anular',['id' => $id])}}" class="btn btn-danger">
                        SI
                    </a>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    <div class="modal fade" id="modal-detalles-{{$id}}" tabindex='-1'>
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Detalles de la venta</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        @if($venta)
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="font-size: 14px">
                                @include('ventas.show_fields',['venta'=> $venta])
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                @include('ventas.tabla_detalles2',['venta'=> $venta])
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 float-right">

                                <a href="{!! route('orden.print.cocina', $venta->id) !!}" class="btn btn-primary" target="_blank" >
                                    <i class="fa fa-print"></i> Ticket Cocina
                                </a>
                                &nbsp;
                                &nbsp;
                                &nbsp;
                                <a href="{!! route('ventas.ticket.pdf', $venta->id) !!}" class="btn btn-primary" target="_blank" >
                                    <i class="fa fa-print"></i> Ticket Cliente
                                </a>
                            </div>
                        @else
                            <div class="alert alert-danger">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;
                                </button>
                                <strong>Venta no encontrada</strong>
                            </div>
                        @endif
                    </div>

                </div>
                {{--<div class="modal-footer">--}}
                {{--<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>--}}
                {{--<button type="button" class="btn btn-primary">Save changes</button>--}}
                {{--</div>--}}
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->