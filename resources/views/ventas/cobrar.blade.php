@extends('layouts.app')

@section('htmlheader_title')
    Ventas por cobrar
@endsection

@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row">
                <div class="col">
                    <h1 class="m-0 text-dark">Ventas por cobrar</h1>
                </div><!-- /.col -->
                {{--<div class="col">--}}
                    {{--<a class="btn btn-danger float-right"--}}
                       {{--href="{!! route('ventas.create') !!}">--}}
                        {{--<i class="fa fa-plus"></i>--}}
                        {{--<span class="d-none d-sm-inline">Nueva Venta</span>--}}
                    {{--</a>--}}
                {{--</div><!-- /.col -->--}}
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            @include('flash::message')

            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            @include('ventas.table')
                            <h3>
                                <span class="badge badge-success">Total cobrar $ <span id="total_deuda"></span></span>
                            </h3>
                        </div>
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col-md-6 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /.content -->
@endsection