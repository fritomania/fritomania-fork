@push('scripts')
<!--    Scripts ventas
------------------------------------------------->
<script >
    var EventBus = new Vue;

    vm = new Vue({
        el: '#root',
        mounted: function() {
            this.getDets();
            this.verificaCaja();
            this.cambioTipoBus();
        },
        created: function () {
            EventBus.$on('ordenes-ventas', function (data) {
                this.listas = data.listas;
                this.noListas = data.noListas;
            }.bind(this));
        },
        data: {
            listas: 0,
            noListas: 0,
            detalles: [],
            nuevoDetalle: {
                temp_venta_id: '{{$tempVenta->id}}',
                item_id: '',
                cantidad: '1',
                precio: '',
                tienda: '{{session('tienda')}}'
            },
            detalleEdita: {
                id: '',
                temp_venta_id: '',
                item_id: '',
                cantidad: '',
                precio: '',
            },
            loadingBtnUpdateDet: false,
            loadingBtnAdd: false,
            loadingBtnCaja: false,
            idEliminando: '',
            idEditando: '',
            recibido:0,
            picked: false,
            caja: {
                monto_apertura: '',
                user_id: '{{Auth::user()->id}}',
                tienda_id: '{{session('tienda')}}'
            },
            tipoBusqueda: '{{config('app.tipo_busqueda_venta','A')}}',
            codigo: '',
            disabledCodigo: false,
            clienteVenta: '',
            credito: false,
            abono_ini: null,
            delivery: JSON.parse("{{old('delivery') ?? 'false'}}"),
            abono_delivery: "{{old('monto_delivery') ?? 0}}",
            programa_entrega: false,

            piezaAdd : [],
            bebidaAdd : [],
            piezasCombo: [],
            bebidasCombo: [],
            cntCombos: false,
            combosCompletados: [],

            recetaDetalles: [],
            adicionales: [],
            salsas: [],
            cantidadItemsModal: 0,
            detEdit: {},
            itemEdit: {},
            loadingReceta : false,
            loadingAgregados : false,
            loadingModificaciones: false,
            modificaciones: {},

        },
        methods: {
            numf: function(numero){
              return numf(numero)
            },
            nf: function(numero){
                return numf(numero)
            },
            getDets: function(page) {
                var urlKeeps = '{{route('venta.temp.detalles',$tempVenta->id)}}';
                axios.get(urlKeeps).then(response => {


                    this.detalles = response.data.data;

                    // modifica las cantidades de los detalles de los combos
                    $.each(this.detalles, (index,det) => {
                        // console.log(det);
                        if(det.item.combo){
                            det.item.combo.piezas=det.cantidad*det.item.combo.piezas_val_o;
                            det.item.combo.bebidas=det.cantidad*det.item.combo.bebidas_val_o;
                        }

                    });

                    this.idEliminando = '';
                    this.loadingBtnAdd= false;
                    this.definePiezas();
                });
            },
            createDet: function() {
                this.loadingBtnAdd= true;
                this.disabledCodigo= true;
                var url= '{{route("api.temp_venta_detalles.store")}}';

                axios.post(url, this.nuevoDetalle ).then(response => {
                    var det = response.data.data;

                    if(det.item.combo) {
                        this.piezasCombo.push([]);
                        this.bebidasCombo.push([]);
                        this.piezaAdd.push({item: {},cantidad: 1});
                        this.bebidaAdd.push({item: {},cantidad: 1});

                        det.item.combo.piezas=det.cantidad*det.item.combo.piezas_val_o;
                        det.item.combo.bebidas=det.cantidad*det.item.combo.bebidas_val_o;

                    }

                    this.detalles.push(det);
                    this.loadingBtnAdd= false;

                    this.nuevoDetalle.item_id = '';
                    this.nuevoDetalle.cantidad = '1';
                    this.nuevoDetalle.precio = '';

                    toastr.success(response.data.message); //mensaje

                    this.disabledCodigo= false;

                    if(this.tipoBusqueda=='A'){
                        slc2item.empty().trigger('change');
                        slc2item.select2('open');
                        $("#div-info-item").html('');
                    }

                }).catch(error => {
                    //console.log(error);
                    toastr.error(error.response.data.message);
                    this.loadingBtnAdd= false;
                    this.disabledCodigo= false;
                    //descomentar sig linea para refrescar venta
                    {{--window.location.href ='{{route('ventas.create')}}';--}}
                });
            },
            editDet: function(det) {
                this.detalleEdita.id = det.id;
                this.detalleEdita.temp_venta_id = det.temp_venta_id;
                this.detalleEdita.item_id = det.item_id;
                this.detalleEdita.cantidad = det.cantidad;
                this.detalleEdita.precio = det.precio;

                $('#modalEditDetalle').modal('show');
            },
            updateDet: function(id) {
                this.loadingBtnUpdateDet= true;
                var url = '{{url('api/temp_venta_detalles')}}' + '/' + id;

                axios.put(url, this.detalleEdita).then(response => {
                    this.getDets();
                    $('#modalEditDetalle').modal('hide');
                    toastr.success(response.data.message);
                    this.loadingBtnUpdateDet= false;
                }).catch(error => {
                    toastr.error(error.response.data.message);
                    this.loadingBtnUpdateDet= false;
                });
            },
            deleteDet: function(det,index) {
                this.idEliminando = det.id;
                var url = '{{url('api/temp_venta_detalles')}}' + '/' + det.id;

                axios.delete(url).then(response => {
                    var det = response.data.data;

                    this.detalles.splice(index,1);
                    this.idEliminando = '';

                    if(det.item.combo) {
                        this.piezasCombo.splice(index, 1);
                        this.bebidasCombo.splice(index, 1);
                    }

                    toastr.success(response.data.message);
                });
            },
            verificaCaja: function () {

                var url = '{{route('venta.verifica.caja')}}';

                console.log('verifica caja',url);

                axios.get(url).then(function (res) {
                    if(!res.data.success){
                        console.log('abrir caja');
                        $('#modalAbrirCaja').modal('show');
                    }else {
                        console.log('Caja abierta');
                        $('#modalAbrirCaja').modal('hide');
                    }

                }).catch(error => {
                    $('#modalAbrirCaja').modal('show');
                    toastr.error(error.response.data.message);
                });


            },
            abrirCaja: function() {
                this.loadingBtnCaja= true;
                var url= '{{route("api.cajas.store")}}';

                axios.post(url, this.caja ).then(response => {

                    this.montoCaja= '';
                    toastr.success(response.data.message); //mensaje
                    this.loadingBtnCaja= false;
                    this.verificaCaja()

                }).catch(error => {
                    toastr.error(error.response.data.message);
                    this.loadingBtnCaja= false;
                    this.verificaCaja();
                });
            },
            cambioTipoBus: function (){
                $(document).keypress(function(e) {
                    if(e.which == 42 && vm.tipoBusqueda=='A') {
                        console.log('Cambio tipo búsqueda simple');
                        vm.tipoBusqueda='S';
                        slc2item.select2('close');
                    }
                    else if(e.which == 42 && vm.tipoBusqueda=='S'){
                        console.log('Cambio tipo búsqueda Avansada');
                        vm.tipoBusqueda='A';
                    }

                });
            },
            addDetCod: function () {
                var url = '{{route('api.item.codigo')}}';

                axios.get(url+'?codigo='+this.codigo).then(response => {

                    var item = response.data.data;

                    this.nuevoDetalle.item_id = item.id;
                    this.nuevoDetalle.cantidad = '1';
                    this.nuevoDetalle.precio = item.precio_venta;

                    this.createDet();
                    this.codigo ='';
                    this.$refs.search.focus();

                }).catch(error => {
                    toastr.error(error.response.data.message);
                    this.loadingBtnAdd= false;
                });

            },

            definePiezas(){
                //console.log('Define arrays para guardar piezas y bebidas agregadas');
                //por cada detalle agrega un array
                $.each(this.detalles,() => {
                    this.piezasCombo.push([]);
                    this.bebidasCombo.push([]);
                    this.piezaAdd.push({item: {},cantidad: 1});
                    this.bebidaAdd.push({item: {},cantidad: 1});
                });

            },
            piezasDisponibles(piezas,index){

                var piezasAgregadas = this.piezasCombo[index];
                var disponibles = [];
                var temps= [];

                $.each(piezasAgregadas,function (index,item) {
                    temps.push(item.item.id);
                });

                $.each(piezas ,function (index,item) {
                    if(!($.inArray(item.item.id,temps)>=0)){
                        disponibles.push(item);
                    }
                });


                return disponibles;
            },
            bebidasDisponibles(piezas,index){

                var bebidasAgregadas = this.bebidasCombo[index];
                var disponibles = [];
                var temps= [];

                $.each(bebidasAgregadas,function (index,item) {
                    temps.push(item.item.id);
                });

                $.each(piezas ,function (index,item) {
                    if(!($.inArray(item.item.id,temps)>=0)){
                        disponibles.push(item);
                    }
                });


                return disponibles;
            },
            addPieza(combo,tope) {
                var cantidad = parseInt(this.piezaAdd[combo].cantidad);
                var item = this.piezaAdd[combo].item;

                if(item.id == null){
                    toastr.error('Seleccionte una pieza');
                    return;
                }

                if(cantidad <= 0){
                    toastr.error('La cantidad debe ser mayor o igual a 1');
                    return;
                }

                console.log('agregar',cantidad,item.nombre,'a combo',combo,'Piezas:',this.cantidadPiezas(combo),'Bebidas:',this.cantidadBebidas(combo));

                if(cantidad+this.cantidadPiezas(combo)>tope){
                    swal({
                        title: 'Error!',
                        text: 'No puede agregar mas piezas de las indicadas',
                        type: 'error',
                        confirmButtonText: 'ok'
                    });
                    return false;
                }

                var piezasCombo = this.piezasCombo[combo];

                const pieza = {
                    item : item,
                    cantidad : cantidad,
                };

                piezasCombo.push(pieza);

                this.piezasCombo.splice( combo, 1, piezasCombo);

                this.piezaAdd[combo] = {
                    item : {},
                    cantidad: 1,
                }
            },
            addBebida(combo,tope) {
                var cantidad = parseInt(this.bebidaAdd[combo].cantidad);
                var item = this.bebidaAdd[combo].item;

                if(item.id == null){
                    toastr.error('Seleccionte una bebida');
                    return;
                }

                if(cantidad <= 0){
                    toastr.error('La cantidad debe ser mayor o igual a 1');
                    return;
                }

                console.log('agregar',cantidad,item.nombre,'a combo',combo,'Piezas:',this.cantidadPiezas(combo),'Bebidas:',this.cantidadBebidas(combo));

                if(cantidad+this.cantidadBebidas(combo)>tope){
                    swal({
                        title: 'Error!',
                        text: 'No puede agregar mas bebidas de las indicadas',
                        type: 'error',
                        confirmButtonText: 'ok'
                    });
                    return false;
                }


                var bebidasCombo = this.bebidasCombo[combo];

                const pieza = {
                    item : item,
                    cantidad : cantidad,
                };

                bebidasCombo.push(pieza);

                this.bebidasCombo.splice( combo, 1, bebidasCombo);

                this.bebidaAdd[combo] = {
                    item : {},
                    cantidad: 1,
                }
            },
            removerPieza(indexCombo,indexPieza){
                console.log('Remover pieza a combo: ',indexCombo,' Pieza: ',indexPieza);
                this.piezasCombo[indexCombo].splice(indexPieza,1);
            },
            removerBebida(indexCombo,indexBebida){
                console.log('Remover bebida a combo: ',indexCombo,' Pieza: ',indexBebida);
                this.bebidasCombo[indexCombo].splice(indexBebida,1);
            },
            keymonitor: function(event) {
                var cantidad = $(event.currentTarget);

                if (event.key == "Enter") {
                    event.preventDefault();
                    var div = cantidad.closest('div');
                    div.find('.btn-add-pieza').focus();
                }

            },
            cantidadPiezas(index){
                var cantidad = 0;
                $.each(this.piezasCombo[index],function (index,item) {
                    cantidad += parseInt(item.cantidad);
                })

                return cantidad;
            },
            cantidadBebidas(index){
                var cantidad = 0;
                $.each(this.bebidasCombo[index],function (index,item) {
                    cantidad += parseInt(item.cantidad);
                })

                return cantidad;
            },

            modificar(det){

                if (det.item.combo){

                    cantidad = 1;
                } else {
                    cantidad = det.cantidad;
                }

                var item = det.item;

                console.log('Editar Detalle: ' , det.id,item.nombre, ' Item: ',item.id,item.nombre);

                this.cantidadItemsModal = parseInt(cantidad);
                this.detEdit = det;
                this.itemEdit = item;
                this.loadingReceta = true;
                this.loadingAgregados =true;

                this.getRecetaDetalles(item);
                this.getAdicionalesYsalsas(item);

                $('#modalEditItem').modal('show');
            },
            modificarPiezaCombo(det,pieza,cantidad){

                if(cantidad === null){

                    cantidad = pieza.cantidad;
                }

                item = pieza.item;

                console.log('Editar Detalle: ' , det.id,det.item.nombre, ' Item: ',item.id,item.nombre);

                this.cantidadItemsModal = parseInt(cantidad);
                this.detEdit = det;
                this.itemEdit = item;
                this.loadingReceta = true;this.loadingAgregados =true;


                this.getRecetaDetalles(item);
                this.getAdicionalesYsalsas(item);

                $('#modalEditItem').modal('show');
            },
            getRecetaDetalles(item){

                console.log('getRecetaDetalles: '+item.nombre);
                var url = "{{route("item.get.ingredientes")}}";

                var params= { params: {id: item.id} };

                axios.get(url,params).then(response => {
                    //console.log('-> Res getRecetaDetalles',response.data.data);

                    this.recetaDetalles = response.data.data;
                    this.loadingReceta = false;
                })
                    .catch(error => {
                        console.log(error);
                        this.recetaDetalles = [];
                        this.loadingReceta = false;

                    });

            },
            getAdicionalesYsalsas(item){

                console.log('getAdicionalesYsalsas: '+item.nombre);

                var url = "{{route("item.get.agregados")}}";

                var params= { params: {item_id: item.id} };

                axios.get(url,params).then(response => {
                    //console.log('-> Res getAdicionalesYsalsas',response.data.data);

                    this.adicionales = response.data.data.adicionales;
                    this.salsas = response.data.data.salsas;
                    this.loadingAgregados =false;

                })
                    .catch(error => {
                        console.log(error);
                        this.adicionales = [];
                        this.salsas = [];
                        this.loadingAgregados =false;
                    });
            },
            guardarModificacion(detEdit){
                this.loadingModificaciones =true;
                var dataArray = $('#datos-item-modifica').serializeArray();
                var data = $('#datos-item-modifica').serialize();

                data = data+'&item_id='+this.itemEdit.id+'&det_id='+detEdit.id;

                var url = "{{route("venta.store.modify")}}";

                console.log('url',url+'?'+data);
                console.log(dataArray);

                axios.get(url+'?'+data).then(response => {

                    var data = response.data.data;
                    console.log(data);

                    $('#modalEditItem').modal('hide');

                    toastr.success('listo! cambios guardados.');

                    this.loadingModificaciones =false;

                    detEdit.total_modificaciones = parseFloat(data.total);


                })
                .catch(error => {
                    console.log(error);
                    toastr.error(error.response.data.message);
                    this.loadingModificaciones =false;
                    this.getModificaciones(detEdit);
                });
            }
        },
        computed: {
            nombreItemDetEdit(){
                return this.itemEdit.nombre;
            },
            total: function () {
                var t=0;
                var monto_delivery = parseFloat(this.abono_delivery);
                var monto_delivery = isNaN(monto_delivery) ? 0 : monto_delivery;
                $.each(this.detalles,function (i,det) {
                    t+=(det.sub_total + det.total_modificaciones);
                });

                if(t>0){
                    t = t + monto_delivery;
                }

                t=parseFloat(t.toFixed(2));

                return t;
            },
            totalitems: function () {
                var t=0;
                var cntCombos = 0;
                $.each(this.detalles,function (i,det) {
                    t+=(det.cantidad*1);
                    if(det.item.combo){
                        if(det.item.combo.detalles.length > 1){
                            cntCombos++;
                        }
                    }

                });

                this.cntCombos = cntCombos;

                return t;
            },
            vuelto: function () {
                var t=this.total, r=this.recibido, v=0, ai=this.abono_ini;

                v= this.credito ? r-ai : r-t;//Vuelto es igual a recibio menos total

                return v<1 ? 0 : v.toFixed(0);
            },
            habilitaProcesar: function () {

                //Si el cliente es el mismo negocio solo valida que haya algun articulo
                if(this.clienteVenta==2 || this.credito){
                    return this.totalitems>0 && !this.faltaArmarCombos;

                }
                //Cualquier otro cliente valida que el moto de la venta sea mayor a 0 y que lo recibido sea mayor o igual al moto total
                else{
                    return (this.total>0 && this.recibido>=this.total && !this.faltaArmarCombos)
                }

            },
            faltaArmarCombos(){
                return (this.cntCombos!=this.combosCompletados.length )
            }
        },
        watch: {
            piezasCombo: function(val){
                this.combosCompletados = [];

                $.each(this.detalles, (index,detalle) => {
                    if(detalle.item.combo){

                        if(detalle.item.combo.piezas==this.cantidadPiezas(index) && detalle.item.combo.bebidas==this.cantidadBebidas(index)){
                            console.log('combo',index,'Completado');
                            this.combosCompletados.push(1);
                        }else {
                            console.log('combo',index,'NO completado');
                        }
                    }

                });
            },
            bebidasCombo: function(val){
                this.combosCompletados = [];

                $.each(this.detalles, (index,detalle) => {
                    if(detalle.item.combo){
                        this.combosCompletados.splice(index,1);

                        if(detalle.item.combo.piezas==this.cantidadPiezas(index) && detalle.item.combo.bebidas==this.cantidadBebidas(index)){
                            console.log('combo',index,'Completado');
                            this.combosCompletados.push(1);
                        }else {
                            console.log('combo',index,'NO completado');
                        }
                    }

                });
            },
        }
    });

    $(function () {
//        $('#codigo').focus();
        function formatStateItems (state) {

            var imagen= state.imagen==undefined ? 'img/avatar_none.png' : state.imagen;

            var color_stock_less = (state.stock<=0) ? 'color_stock_less' : '';

            var $state = $(
                "@component('components.format_slc2_item')"+
                "@slot('imagen')"+imagen+ "@endslot"+
                "@slot('nombre')"+state.nombre+ "@endslot"+
                "@slot('marca')"+state.nombre_marca+ "@endslot"+
                "@slot('descripcion')"+ state.descripcion+"@endslot"+
                "@slot('precio')"+addComas(state.precio_venta)+"@endslot"+
                "@slot('ubicacion')"+state.ubicacion+"@endslot"+
                "@slot('stock')"+numf(state.stock_tienda)+"@endslot"+
                "@slot('codigo')"+state.codigo+"@endslot"+
                "@slot('color_stock_less')"+color_stock_less+"@endslot"+
                "@endcomponent"
            );

            return $state;
        };

        slc2item=$("#items").select2({
            language : 'es',
            maximumSelectionLength: 1,
            placeholder: "Ingrese código,nombre o descripción para la búsqueda",
            delay: 250,
            ajax: {
                url: "{{ route('api.items.index') }}",
                dataType: 'json',
                data: function (params) {
                    return {
                        search: params.term,
                        tienda: '{{session('tienda')}}'
                    };
                },
                processResults: function (data, params) {
                    //recorre todos los item q
                    var data = $.map(data.data, function (item) {

                        //recorre los atributos del item
                        $.each(item,function (index,valor) {
                            //Si no existe valor se asigan un '-' al attributo
                            item[index] = !valor ? '-' : valor;
                        });

                        return item;
                    });

                    return {
                        results: data,
                    };
                },
                cache: true
            },
            minimumInputLength: 1,
            templateResult: formatStateItems,
            templateSelection: function(data,contenedor) {

                $("#div-info-item").html(
                    "@component('components.items.show_bar')"+
                    "@slot('imagen')"+data.imagen+ "@endslot"+
                    "@slot('marca')"+ data.nombre_marca+"@endslot"+
                    "@slot('descripcion')"+ data.descripcion+"@endslot"+
                    "@slot('precio')"+ numf(data.precio_venta)+"@endslot"+
                    "@slot('ubicacion')"+data.ubicacion+"@endslot"+
                    "@slot('stock')"+numf(data.stock_tienda)+"@endslot"+
                    "@slot('precio_mayoreo')"+ numf(data.precio_mayoreo)+"@endslot"+
                    "@slot('cantidad_mayoreo')"+ data.cantidad_mayoreo+"@endslot"+
                    "@slot('um')"+ data.um+"@endslot"+
                    "@endcomponent"
                );

                vm.nuevoDetalle.item_id = data.id;
                vm.nuevoDetalle.precio = data.precio_venta;

                if(data.stock<=0){
                    $('.select2-result__stock').css('color','#d5404b');
                }

                return data.nombre;
            }
        }).on('select2:unselecting',function (e) {
            $("#div-info-item").html('');
        });


        if(vm.tipoBusqueda=='A'){
            slc2item.empty().trigger('change');
            slc2item.select2('open');
        }else{
            vm.$refs.search.focus();
        }


        $("#precio-new-det,#cant-new-det").keypress(function (e) {
            if (e.keyCode == 13) {
                e.preventDefault();
                $("#btn-agregar").focus();
            }
        }).focus(function () {
            $(this).select()
        });

        $("#recibido").keypress(function (e) {
            console.log(e);
            if (e.keyCode == 13) {
                e.preventDefault();
                $("#btn-procesar").focus();
            }
        });
//
//        $("#fecha").datepicker({
//            language : 'es'
//        });

        $('#clientes').select2({
            language: 'es',
            maximumSelectionLength: 1,
            allowClear: true
        }).on('change',function (e) {

            var cliente = parseInt(e.target.value);
            vm.clienteVenta = cliente;
        });

        $("#btn-procesar").click(function () {
            $(this).button('loading');
        });

        $('#modalAbrirCaja').modal({
            show: false,
            keyboard: false,
            backdrop: 'static'
        });

        $("#modalAbrirCaja").on('shown.bs.modal',function () {
            $("#montoCaja").focus();
        });

    })
</script>
@endpush