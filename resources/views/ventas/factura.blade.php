@extends('adminlte::layouts.app')

@section('content')
    <section class="content-header">
        <h1 class="pull-left">Venta</h1>
        <a class="btn btn-success pull-right" style="margin-top: -10px;margin-bottom: 5px" href="{!! route('ventas.create') !!}">
            <i class="fa fa-plus"></i>
            <span class="hidden-xs hidden-sm">Nueva Venta</span>
        </a>
    </section>
    <div class="content">
        <div class="row">
            <div class="col-sm-12">
                <div class="box box-default box-solid">
                    <div class="box-header with-border">
                        <h3 class="box-title">COMO DEBE EGRESAR LOS PRODUCTOS</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                        </div>
                        <!-- /.box-tools -->
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        @include('ventas.tabla_egresos',['venta' => $venta])
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->

            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
            <div class="embed-responsive embed-responsive-16by9">
                <iframe class="embed-responsive-item" id="documento" src="{{route('ventas.pdf_factura2',$venta->id)}}"  ></iframe>
            </div>
            </div>
        </div>
    </div>
@endsection
