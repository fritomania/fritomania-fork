<div class="table-responsive">
    <table width="100%"  class="table table-bordered table-xtra-condensed" id="tablaDetalle" style="margin-bottom: 2px">
        <thead>
        <tr class="bg-primary" align="center" style="font-weight: bold">
            <td width="50%">Producto</td>
            <td width="10%">Precio</td>
            <td width="10%">Cantidad</td>
            <td width="10%">Adicionales</td>
            <td width="10%">Subtotal</td>
            <td width="10%">-</td>
        </tr>
        </thead>
        <tbody v-if="detalles.length==0">
            <tr>
                <td colspan="10"><span class="help-block text-center">No se ha agregado ningún artículo</span></td>
            </tr>
        </tbody>
        <tbody v-for="(detalle,index) in detalles">
            <tr>
                <td>@{{ detalle.item.nombre }}</td>
                <td>{{dvs()}}@{{ numf(detalle.precio) }}</td>
                <td>@{{ numf(detalle.cantidad) }}</td>
                <td>{{dvs()}}@{{ numf(detalle.total_modificaciones) }}</td>
                <td>{{dvs()}}@{{ numf(detalle.sub_total + detalle.total_modificaciones) }}</td>
                <td width="10px">
                    {{--<button type="button" class="btn btn-info btn-xs" @click="editDet(detalle)">--}}
                    {{--<i class="fa fa-edit"></i>--}}
                    {{--</button>--}}
                    <button type="button" class='btn btn-danger btn-xs' @click="deleteDet(detalle,index)" :disabled="(idEliminando===detalle.id)">
                        <span v-show="(idEliminando===detalle.id)" >
                            <i  class="fa fa-sync-alt fa-spin"></i>
                        </span>
                        <span v-show="!(idEliminando===detalle.id)" >
                            <i class="fa fa-trash-alt"></i>
                        </span>
                    </button>

                    {{--Si el item tien receta es tiene asociados adicionales o salsas--}}
                    <span v-if="detalle.item.receta || detalle.item.tiene_extras || detalle.item.tiene_salsas">
                        <button type="button" class="btn btn-xs btn-danger" @click="modificar(detalle)" >
                            <i class="fa fa-edit" aria-hidden="true"></i>
                        </button>
                    </span>
                </td>
            </tr>
            <template v-if="detalle.item.combo ">
                <tr  v-if="detalle.item.combo.detalles.length > 1">
                    <td colspan="10" style="padding: 1px" class="text-left">
                        <table width="100%">
                            <tr>
                                <td>
                                    Arme el combo elija:
                                    <span v-show="cantidadPiezas(index)>0" v-text="cantidadPiezas(index)+'/'"></span>
                                    <span v-text="detalle.item.combo.piezas"></span> Piezas y
                                    <span v-show="cantidadBebidas(index)>0" v-text="cantidadBebidas(index)+'/'"></span>
                                    <span v-text="detalle.item.combo.bebidas"></span> Bebidas <br>

                                    <!-- Inputs piezas -->
                                    <div class="row" v-show="detalle.item.combo.piezas > cantidadPiezas(index)">
                                        <div class="input-group input-group-sm col-sm-7">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">Pieza</span>
                                            </div>
                                            <select v-model="piezaAdd[index].item" class="form-control" placeholder="Seleccione una pieza">
                                                <option :value="{}" selected>Seleccione una pieza</option>
                                                <option v-for="det in piezasDisponibles(detalle.item.combo.piezas_d,index)" :value="det.item">
                                                    @{{ det.item.nombre }}
                                                </option>
                                            </select>
                                        </div>
                                        <div class="input-group input-group-sm col-sm-5">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">Cantidad</span>
                                            </div>
                                            <input
                                                    type="number"
                                                    class="form-control"
                                                    v-model="piezaAdd[index].cantidad"
                                                    @focus="$event.target.select()"
                                                    @keypress="keymonitor"
                                                    min="1"
                                            >
                                            <div class="input-group-append">
                                                <button type="button" class="btn btn-danger btn-sm btn-add-pieza" @click="addPieza(index,detalle.item.combo.piezas)">
                                                    <i class="fa fa-plus" aria-hidden="true"></i> Agregar
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                    <br>

                                    {{--<!-- Inputs bebidas -->--}}
                                    <div class="row" v-show="detalle.item.combo.bebidas > cantidadBebidas(index)">
                                        <div class="input-group input-group-sm col-sm-7">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">Bebida</span>
                                            </div>
                                            <select v-model="bebidaAdd[index].item" class="form-control" placeholder="Seleccione una bebida">
                                                <option :value="{}">Seleccione una bebida</option>
                                                <option v-for="det in bebidasDisponibles(detalle.item.combo.bebidas_d,index)" :value="det.item">
                                                    @{{ det.item.nombre }}
                                                </option>
                                            </select>
                                        </div>

                                        <div class="input-group input-group-sm col-sm-5">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">Cantidad</span>
                                            </div>
                                            <input
                                                    type="number"
                                                    class="form-control"
                                                    v-model="bebidaAdd[index].cantidad"
                                                    @focus="$event.target.select()"
                                                    @keypress="keymonitor"
                                                    min="1">
                                            <div class="input-group-append">
                                                <button type="button" class="btn btn-danger btn-sm btn-add-pieza" @click="addBebida(index,detalle.item.combo.bebidas)">
                                                    <i class="fa fa-plus" aria-hidden="true"></i> Agregar
                                                </button>
                                            </div>
                                        </div>
                                    </div>

                                    <!--            Lista de items agregados
                                        ------------------------------------------------------------------------>
                                    <ul style="margin-left: 15px">
                                        <li v-for="(pieza,indexPieza) in piezasCombo[index]">
                                            <span v-text="pieza.cantidad"></span>&nbsp;
                                            <span v-text="pieza.item.nombre"></span>&nbsp;
                                            <button type="button" class="btn btn-danger btn-xs" @click="removerPieza(index,indexPieza)">
                                                <i class="fa fa-trash" aria-hidden="true"></i>
                                            </button>
                                            <span v-if="pieza.item.receta || pieza.item.tiene_extras || pieza.item.tiene_salsas">

                                                <button type="button" class="btn btn-xs btn-danger" @click="modificarPiezaCombo(detalle,pieza,null)" >
                                                    <i class="fa fa-edit" aria-hidden="true"></i> Modificar
                                                </button>

                                            </span>
                                            <input type="hidden" :name="'detalles_combo['+detalle.id+']['+pieza.item.id+']'" :value="pieza.cantidad">
                                        </li>
                                        <li v-for="(bebida,indexBebida) in bebidasCombo[index]">
                                            <span v-text="bebida.cantidad"></span>&nbsp;
                                            <span v-text="bebida.item.nombre"></span>&nbsp;
                                            <button type="button" class="btn btn-danger btn-xs" @click="removerBebida(index,indexBebida)">
                                                <i class="fa fa-trash" aria-hidden="true"></i>
                                            </button>
                                            <input type="hidden" :name="'detalles_combo['+detalle.id+']['+bebida.item.id+']'" :value="bebida.cantidad">
                                        </li>
                                    </ul>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr  v-if="detalle.item.combo.detalles.length == 1">
                    <td colspan="10" style="padding: 1px" class="text-left">
                        <table width="100%">
                            <tr>
                                <td>
                                    Este detalle contiene:
                                    <ul>
                                        <li v-for="(pieza,indexPieza) in detalle.item.combo.detalles">
                                            <span v-text="detalle.item.combo.piezas"></span>&nbsp;
                                            <span v-text="pieza.item.nombre"></span>&nbsp;
                                            <span v-if="pieza.item.receta || pieza.item.tiene_extras || pieza.item.tiene_salsas">

                                                <button type="button" class="btn btn-xs btn-danger" @click="modificarPiezaCombo(detalle,pieza,detalle.item.combo.piezas)" >
                                                    <i class="fa fa-edit" aria-hidden="true"></i> Modificar
                                                </button>

                                            </span>
                                            <input type="hidden" :name="'detalles_combo['+detalle.id+']['+pieza.item.id+']'" :value="pieza.cantidad">
                                        </li>
                                    </ul>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </template>

        </tbody>
        </tbody>
        <tfoot>
        <tr>
            <td colspan="10" >
                <b>Total</b>
                <b class="pull-right" >
                    {{dvs()}} <span v-text="numf(total.toFixed(0))"></span>
                </b>
            </td>
        </tr>
        </tfoot>

    </table>
</div>