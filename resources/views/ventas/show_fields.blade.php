<div class="row">
    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
        {!! Form::label('cliente', 'Cliente:') !!}
        {!! $venta->cliente ? $venta->cliente->full_name : ''  !!}
        <br>

        {!! Form::label('fecha', 'Fecha:') !!}
        {!! fecha($venta->fecha) !!}
        <br>

        {!! Form::label('serie', 'N/S:') !!}
        {!! $venta->serie !!}-{!! $venta->numero !!}
        <br>

        @if($venta->repartidor())
            {!! Form::label('repartidor', 'Repartidor:') !!}
            {!! $venta->repartidor()->name !!}
            <br>
        @endif

    </div>
    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
        {!! Form::label('vestado', 'Estado:') !!}
        {!! $venta->vestado->descripcion !!}
        <br>


        @if(!$venta->web)
            {!! Form::label('user', 'Usuario:') !!}
            {!! $venta->user ? $venta->user->name : '' !!}
            <br>
        @endif

        {!! Form::label('created_at', 'Creado el:') !!}
        {!! $venta->created_at->format('Y/m/d H:i:s')!!}
        <br>

        {{--{!! Form::label('updated_at', 'Actualizado el:') !!}--}}
        {{--{!! $venta->updated_at->format('Y/m/d H:i:s')!!}--}}
        {{--<br>--}}
    </div>
</div>
<br>
