<!DOCTYPE html>
<html lang="en">
<head>
    <title>Print Table</title>
    <meta charset="UTF-8">
    <meta name=description content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap CSS -->
    <link href="{{asset('css/bootstrap4.min.css')}}" rel="stylesheet">
    {{--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">--}}
    <style>
        .content {
            height: auto;
            width: 400px;
            margin-left: 10px;
            margin-right: 10px;
        }
    </style>
</head>
<body>
<body onload="window.print()">
    <div class="content">
        <h1 class="float left">Orden # {{$orden->id}}</h1>

        <table class="table table-sm table-bordered" >
            <thead>
            <tr>
                <th>Articulo</th>
                <th>Cantidad</th>
            </tr>
            </thead>
            <tbody>
            @foreach($orden->ventaDetalles as $det)
                <tr>
                    <td>{{$det->item->nombre}}</td>
                    <td>{{$det->cantidad}}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

</body>
</html>
