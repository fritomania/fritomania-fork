<table class="table table-bordered table-hover table-xtra-condensed">
    <thead>
    <tr>
        <th class="text-center">Producto</th>
        <th class="text-center">Precio</th>
        <th class="text-center">Cantidad</th>
        <th class="text-center">Subtotal</th>
    </tr>
    </thead>
    <tbody>
    @foreach($venta->ventaDetalles as $det)
        <tr class="text-sm">
            <td width="61%">{{$det->item->nombre}}</td>
            <td width="13%" class="text-right">{{ dvs() }} {{nfp($det->precio)}}</td>
            <td width="13%" class="text-right">{{nf($det->cantidad)}}</td>
            <td width="13%" class="text-right">{{ dvs() }} {{nfp($det->cantidad*$det->precio)}}</td>
        </tr>
    @endforeach

    @php
        $totalVenta = $venta->total;
    @endphp
    @if($venta->delivery)
        @php
            $totalVenta += $venta->monto_delivery;
        @endphp
        <tr class="text-right text-sm">
        <td colspan="3">Monto Delivery </td>
        <td ><b>{{ dvs() }} {{nfp($venta->monto_delivery)}}</b></td>
        </tr>
    @endif
    <tr class="text-right text-sm">
        <td colspan="3">Total </td>
        @if($venta->delivery)
            <td ><b>{{ dvs() }} {{nfp($totalVenta)}}</b></td>
        @else
            <td ><b>{{ dvs() }} {{nfp($venta->total)}}</b></td>
        @endif
    </tr>
</table>

<h4>
    <span class="badge badge-success">Recibido {{ dvs() }} {{nfp($venta->recibido)}}</span>
    <span class="badge badge-secondary">Vuelto {{ dvs() }} {{nfp($venta->recibido-$totalVenta)}}</span>
</h4>
