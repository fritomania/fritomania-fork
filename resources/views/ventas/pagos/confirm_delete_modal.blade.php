<div class="modal fade modal-warning" id="modalDeleteVpago" tabindex='-1'>
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Eliminar pago!</h5>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <div class="modal-body">

                <h3>
                    Fecha: @{{vpagoElimina.created_at}}
                    <br>
                    Monto: {{ dvs() }} @{{ vpagoElimina.monto }}
                    <br>
                    Usuario: @{{vpagoElimina.username}}
                </h3>
            </div>
            <div class="modal-footer">
                <span class="pull-left">Seguro?</span>
                <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                <button type="submit" class="btn btn-danger" @click.prevent="deleteVpago(vpagoElimina)">SI</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->