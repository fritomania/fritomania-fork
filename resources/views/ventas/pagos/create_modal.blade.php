<!-- Modal vpagos -->
<div class="modal fade" id="modalCreateVpago">
    <div class="modal-dialog ">
        <div class="modal-content">
            <form action="" @submit.prevent="createVpago" method="post" role="form" id="form-modal-vpagos">

                <div class="modal-header">
                    <h5 class="modal-title">Nuevo abono</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <!-- Cantidad Field -->
                        <div class="form-group col-sm-12">
                            <label for="cantidad">Monto</label>
                            <input type="number" v-model="nuevoVpago.monto" id="cantidad" class="form-control" step=".05">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="reset" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    <button type="submit" data-loading-text="Loading..." class="btn btn-success" autocomplete="off" :disabled="loading">
                        <i  v-show="loading" class="fa fa-spinner fa-spin"></i> Guardar
                    </button>
                </div>

            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
<!-- /. Modal vpagos -->