@push('scripts')
<!--    Scripts abonos venta credito
------------------------------------------------->
<script >
    function addComas(nStr){
        nStr += '';
        x = nStr.split('.');
        x1 = x[0];
        x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + ',' + '$2');
        }
        return x1 + x2;
    }

    var vmVpago = new Vue({
            el: '#root',
            created: function() {
                this.getVpagos();
            },
            data: {
                totalVenta: '{{number_format($venta->total,2,'.','')}}',
                vpagos: [],
                nuevoVpago: {
                    'venta_id': '{{$venta->id}}',
                    'monto': '',
                    'user_id': '{{Auth::user()->id}}'
                },
                vpagoEdit: {},
                vpagoElimina: {},
                loading: false,
            },
            methods: {
                getVpagos: function() {
                    var parms = {
                        params:{
                            venta_id: '{{$venta->id}}'
                        }
                    };

                    var url = "{{route('api.vpagos.index')}}";

                    axios.get(url,parms).then(response => {
                        this.vpagos = response.data.data
                    });
                },
                createVpago: function() {
                    this.loading= true;
                    var url = "{{route('api.vpagos.store')}}";

                    axios.post(url, this.nuevoVpago ).then(response => {
                        this.getVpagos();

                        this.nuevoVpago.monto = '';
                        $('#modalCreateVpago').modal('hide');
                        toastr.success(response.data.message); //mensaje
                        this.loading= false;
                    }).catch(error => {
                        toastr.error(error.response.data.message);
                        this.loading= false;
                    });
                },
                editVpago: function(vpago) {
                    this.vpagoEdit = vpago;

                    $('#modalEditVpago').modal('show');
                },
                updateVpago: function(id) {
                    this.loading= true;
                    var url = "{{url('api/vpagos')}}" + "/" + id;

                    axios.put(url, this.vpagoEdit).then(response => {
                        this.getVpagos();

                        this.vpagoEdit.id = '';
                        this.vpagoEdit.monto = '';

                        $('#modalEditVpago').modal('hide');
                        toastr.success(response.data.message);
                        this.loading= false;
                    }).catch(error => {
                        toastr.error(error.response.data.message);
                        this.loading= false;
                    });
                },
                confirmDelete: function(vpago) {
                    this.vpagoElimina = vpago;
                    $('#modalDeleteVpago').modal('show');
                },
                deleteVpago: function(vpago) {
                    var url = "{{url('api/vpagos')}}" + "/" + vpago.id;

                    axios.delete(url).then(response => { //eliminamos
                        this.getVpagos(); //listamos
                        $('#modalDeleteVpago').modal('hide');
                        toastr.success(response.data.message); //mensaje
                    });
                }
            },
            computed: {
                totalPagos : function () {
                    var t=0;
                    $.each(this.vpagos,function (i,det) {
                        t+=parseFloat(det.monto);
                    });

                    return t.toFixed(0);
                },
                saldo: function () {
                    var tv = parseFloat(this.totalVenta);
                    var tp = parseFloat(this.totalPagos);

                    return (tv-tp).toFixed(0);
                },
                saldoTexto: function () {
                    return addComas(this.saldo);
                }
            }
        });
</script>
@endpush