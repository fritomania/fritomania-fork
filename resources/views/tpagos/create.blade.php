@extends('adminlte::layouts.app')

@section('htmlheader_title')
	Crear Tipo de pago
@endsection

@section('content')
    <section class="content-header">
        <h1>
            Crear Tipo de pago
        </h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">

            <div class="box-body">
                <div class="row">
                    {!! Form::open(['route' => 'tpagos.store']) !!}

                        @include('tpagos.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
