@extends('adminlte::layouts.app')

@section('htmlheader_title')
	Editar Tipo de pago
@endsection

@section('content')
    <section class="content-header">
        <h1>
            Editar Tipo de pago
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($tpago, ['route' => ['tpagos.update', $tpago->id], 'method' => 'patch']) !!}

                        @include('tpagos.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection