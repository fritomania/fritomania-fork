@extends('adminlte::layouts.app')

@section('htmlheader_title')
    Tipo de pago
@endsection

@section('content')
    <section class="content-header">
        <h1>
            Tipo de pago
        </h1>
    </section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    @include('tpagos.show_fields')
                    <a href="{!! route('tpagos.index') !!}" class="btn btn-default">Regresar</a>
                </div>
            </div>
        </div>
    </div>
@endsection
