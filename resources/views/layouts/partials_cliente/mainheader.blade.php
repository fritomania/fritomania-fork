<!--Header-->
<header id="top" class="header clearfix">

    <div class="masarriba">

        <div class="row">
            <div class="col-sm-4">
                @if(isset($orden) && isset($muestraDatosCliente))
                    <table id="datos-cliente">
                        <tr>
                            <td>Cliente:</td>
                            <td>{{$orden->nombre_entrega}}</td>
                        </tr>
                        <tr>
                            <td>Teléfono:</td>
                            <td>{{$orden->telefono}}</td>
                        </tr>
                        {{--<tr>--}}
                            {{--<td>Dirección:</td>--}}
                            {{--<td>{{$orden->direccion}}</td>--}}
                        {{--</tr>--}}
                        <tr>
                            <td>Correo:</td>
                            <td>{{$orden->correo}}</td>
                        </tr>
                        <tr>
                            <td>Tipo:</td>
                            <td>
                                {{$orden->retiro_en_local ? 'Retiro en local' : 'Delivery'}}

                                @if(!config('app.solo_delivery_web'))
                                    <!-- Button trigger modal -->
                                    <a href="#madalCambioTipoPedido" class="btn btn-danger btn-xs" data-toggle="modal">
                                        Cambiar
                                    </a>
                                @endif
                            </td>
                        </tr>
                        {{--<tr>--}}
                            {{--<td>Observaciones:</td>--}}
                            {{--<td>{{$orden->observaciones}}</td>--}}
                        {{--</tr>--}}
                    </table>
                @endif
            </div>
            <div class="col-sm-4">
                <a href="{{route('locales')}}" class="btn" id="pideahora">
                    PIDE AHORA <i class="fa fa-location-arrow" style="margin-left: 20px;"></i>
                </a>
            </div>
            <div class="col-sm-4"><div class="nav-icon">
                    {{--<div class="m_search search-icon">--}}
                    {{--<a href="#" data-toggle="modal" data-target="#lightbox-search">--}}
                    {{--<i class="fa fa-search"></i>--}}
                    {{--</a>--}}
                    {{--</div>--}}
                    <div class="icon_cart">
                        <div class="m_cart-group">
                            <a href="#" type="button" class="cart dropdown-toggle dropdown-link" data-toggle="modal" data-target="#modalCart">
                                <div class="num-items-in-cart">
                                    <div class="items-cart">
                                        <div class="num-items-in-cart">
                                            <i class="fa fa-shopping-cart"></i>
                                            <span class="cart_text">
                                                                        Carro <span class="number">(<span v-text="cantidad_articulos"></span>)</span>
                                                                    </span>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="icon_accounts">
                        <div class="m_login-account">
                                                    <span class="dropdown-toggle login-icon" data-toggle="dropdown">
                                                        <i class="fa fa-ellipsis-v"></i>
                                                        <i class="sub-dropdown1 visible-sm visible-md visible-lg"></i>
                                                        <i class="sub-dropdown visible-sm visible-md visible-lg"></i>
                                                    </span>
                            <div class="m_dropdown-login dropdown-menu login-content">
                                <div class="clearfix">
                                    <div class="login-register-content">
                                        <ul class="nav nav-tabs">
                                            <li class="account-item-title active">
                                                <a href="#account-login" data-toggle="tab">
                                                    Ingresar
                                                </a>
                                            </li>
                                            <li class="account-item-title">
                                                <a href="#account-register" data-toggle="tab">
                                                    Registrarse
                                                </a>
                                            </li>
                                        </ul>
                                        <div class="tab-content group_form">
                                            <div class="tab-pane active account-item-content" id="account-login">
                                                <form method="post" action="http://demo.designshopify.com/html_fastfood/login.html" id="customer_login" accept-charset="UTF-8">
                                                    <div class="clearfix large_form form-item">
                                                        <input type="email" value="" name="customer[email]" class="form-control" placeholder="Dirección Email *" />
                                                    </div>
                                                    <div class="clearfix large_form form-password form-item">
                                                        <input type="password" value="" name="customer[password]" class="form-control password" placeholder="Contraseña *" />
                                                        <span class="cs-icon icon-eye"></span>
                                                    </div>
                                                    <div class="action_bottom">
                                                        <button class="_btn" type="submit">Ingresar</button>
                                                        <a href="login-recover.html"><span class="red"></span>¿Olvidaste tu Contraseña?</a>
                                                    </div>
                                                </form>
                                            </div>
                                            <div class="tab-pane account-item-content " id="account-register">
                                                <form method="post" action="http://demo.designshopify.com/html_fastfood/register.html" id="create_customer" accept-charset="UTF-8">
                                                    <div class="clearfix large_form form-item">
                                                        <input placeholder="First Name" type="text" value="" name="customer[first_name]" id="first_name" class="text" size="30" />
                                                    </div>
                                                    <div class="clearfix large_form form-item">
                                                        <input placeholder="Last Name" type="text" value="" name="customer[last_name]" id="last_name" class="text" size="30" />
                                                    </div>
                                                    <div class="clearfix large_form form-item">
                                                        <input placeholder="Email" type="email" value="" name="customer[email]" id="email" class="text" size="30" />
                                                    </div>
                                                    <div class="clearfix large_form form-password form-item">
                                                        <input placeholder="Password" type="password" value="" name="customer[password]" id="password" class="password text" size="30" />
                                                        <span class="cs-icon icon-eye"></span>
                                                    </div>
                                                    <div class="action_bottom">
                                                        <button class="_btn" type="submit">Crear</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    <ul class="wish-compare-content">
                                        <li class="link-item"><a href="wish-list.html">Mi Lista de Deseos</a></li>
                                        <li class="link-item"><a href="compare.html">Mis Comparaciones</a></li>
                                    </ul>
                                    <ul class="currencies currencies-content">
                                        <li class="currency-USD active">
                                            <a href="javascript:;">USD</a>
                                            <input type="hidden" value="USD" />
                                        </li>
                                        <li class="currency-GBP">
                                            <a href="javascript:;">GBP</a>
                                            <input type="hidden" value="GBP" />
                                        </li>
                                        <li class="currency-EUR">
                                            <a href="javascript:;">EUR</a>
                                            <input type="hidden" value="EUR" />
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div></div>
        </div>
    </div>
    <div id="shopify-section-theme-header" class="shopify-section">
        <div data-section-id="theme-header" data-section-type="header-section">
            <section class="main-header">
                <div class="main-header-wrapper">
                    <div class="container clearfix">
                        <div class="row">
                            <div class="main-header-inner">
                                <div class="nav-logo">
                                    <a href="{{route('home')}}">
                                        <img src="{{asset('cliente/images/logo-fritomania-header.png')}}" alt="" title="Fast Food" />
                                    </a>
                                    <h1 style="display:none"><a href="http://demo.designshopify.com/">Fritomanía</a></h1>
                                </div>
                                <div class="nav-top">
                                    <div class="nav-menu">
                                        <ul class="navigation-links ">
                                            <li class="nav-item">
                                                <a href="{{route('home')}}">
                                                    <span>Inicio</span>
                                                </a>
                                            </li>

                                            <li class="nav-item">
                                                <a href="https://www.fritomania.cl/productos?category=12">
                                                    <span>Promo</span>
                                                </a>
                                            </li>
                                            <li class="nav-item">
                                                <a href="{{route('productos').'?category='.\App\Models\Icategoria::COMBO}}">
                                                    <span>Combos</span>
                                                </a>
                                            </li>
                                            <li class="nav-item">
                                                <a href="{{route('productos')}}">
                                                    <span>Productos</span>
                                                </a>
                                            </li>
                                            <li class="nav-item">
                                                <a href="#contacto">
                                                    <span>Contacto</span>
                                                </a>
                                            </li>
                                            <!-- Dropdown menu pages -->
                                        {{--<li class="nav-item dropdown navigation">--}}
                                        {{--<a href="contact.html" class="dropdown-toggle dropdown-link" data-toggle="dropdown">--}}
                                        {{--<span>Pages</span>--}}
                                        {{--<i class="fa fa-angle-down"></i>--}}
                                        {{--<i class="sub-dropdown1  visible-sm visible-md visible-lg"></i>--}}
                                        {{--<i class="sub-dropdown visible-sm visible-md visible-lg"></i>--}}
                                        {{--</a>--}}
                                        {{--<ul class="dropdown-menu">--}}
                                        {{--<li class="li-sub-mega">--}}
                                        {{--<a tabindex="-1" href="blog.html">Blogs</a>--}}
                                        {{--</li>--}}
                                        {{--</ul>--}}
                                        {{--</li>--}}
                                        <!--/ Dropdown menu pages -->
                                        </ul>
                                    </div>

                                </div>
                                <div class="navMobile-navigation">
                                    <div class="navMobile-logo">
                                        <a href="{{route('home')}}">
                                            <img class="header-logo-image" src="{{asset('cliente/images/logo-fritomania-header.png')}}" alt="" title="Fast Food" />
                                        </a>
                                    </div>
                                    <div class="group_mobile_right">
                                        <div class="nav-icon">
                                            {{--<div class="m_search search-tablet-icon">--}}
                                            {{--<span class="dropdownMobile-toggle search-dropdown">--}}
                                            {{--<span class="icon-dropdown cs-icon icon-search" data-class="cs-icon icon-search"></span>--}}
                                            {{--<i class="sub-dropdown1 visible-sm visible-md visible-lg"></i>--}}
                                            {{--<i class="sub-dropdown visible-sm visible-md visible-lg"></i>--}}
                                            {{--</span>--}}
                                            {{--<div class="m_dropdown-search dropdown-menu search-content">--}}
                                            {{--<form class="search" action="http://demo.designshopify.com/html_fastfood/search.html">--}}
                                            {{--<input type="hidden" name="type" value="product" />--}}
                                            {{--<input type="text" name="q" class="search_box" placeholder="search our store" value="" />--}}
                                            {{--<span class="search-clear cs-icon icon-close"></span>--}}
                                            {{--<button class="search-submit" type="submit">--}}
                                            {{--<span class="cs-icon icon-search"></span>--}}
                                            {{--</button>--}}
                                            {{--</form>--}}
                                            {{--</div>--}}
                                            {{--</div>--}}
                                            <div class="icon_cart">
                                                <div class="m_cart-group">
                                                    <a href="#" type="button" class="cart dropdown-toggle dropdown-link" data-toggle="modal" data-target="#modalCart">
                                                        <div class="num-items-in-cart">
                                                            <div class="items-cart">
                                                                <div class="num-items-in-cart">
                                                                    <span class="cs-icon icon-bag"></span>
                                                                    <span class="cart_text">
                                                                        <span class="number">(<span v-text="cantidad_articulos"></span>)</span>
                                                                        </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="icon_accounts">
                                                <div class="m_login-account">
                                                        <span class="dropdownMobile-toggle login-icon">
                                                            <i class="icon-dropdown cs-icon icon-ellipsis" data-class="cs-icon icon-ellipsis" aria-hidden="true"></i>
                                                            <i class="sub-dropdown1 visible-sm visible-md visible-lg"></i>
                                                            <i class="sub-dropdown visible-sm visible-md visible-lg"></i>
                                                        </span>
                                                    <div class="m_dropdown-login dropdown-menu login-content">
                                                        <div class="clearfix">
                                                            <div class="login-register-content">
                                                                <ul class="nav nav-tabs">
                                                                    <li class="account-item-title active">
                                                                        <a href="#account-login-mobile" data-toggle="tab">
                                                                            Login
                                                                        </a>
                                                                    </li>
                                                                    <li class="account-item-title">
                                                                        <a href="#account-register-mobile" data-toggle="tab">
                                                                            Register
                                                                        </a>
                                                                    </li>
                                                                </ul>
                                                                <div class="tab-content group_form">
                                                                    <div class="tab-pane active account-item-content" id="account-login-mobile">
                                                                        <form method="post" action="http://demo.designshopify.com/html_fastfood/login.html" id="customer_login_mobile" accept-charset="UTF-8">
                                                                            <div class="clearfix large_form form-item">
                                                                                <input type="email" value="" name="customer[email]" class="form-control" placeholder="Email Address *" />
                                                                            </div>
                                                                            <div class="clearfix large_form form-password form-item">
                                                                                <input type="password" value="" name="customer[password]" class="form-control password" placeholder="Password *" />
                                                                                <span class="cs-icon icon-eye"></span>
                                                                            </div>
                                                                            <div class="action_bottom">
                                                                                <button class="_btn" type="submit">Login</button>
                                                                                <a href="login-recover.html"><span class="red"></span> Forgot your password?</a>
                                                                            </div>
                                                                        </form>
                                                                    </div>
                                                                    <div class="tab-pane account-item-content " id="account-register-mobile">
                                                                        <form method="post" action="http://demo.designshopify.com/html_fastfood/register.html" id="create_customer_mobile" accept-charset="UTF-8">
                                                                            <div class="clearfix large_form form-item">
                                                                                <input placeholder="First Name" type="text" value="" name="customer[first_name]" class="text" size="30" />
                                                                            </div>
                                                                            <div class="clearfix large_form form-item">
                                                                                <input placeholder="Last Name" type="text" value="" name="customer[last_name]" class="text" size="30" />
                                                                            </div>
                                                                            <div class="clearfix large_form form-item">
                                                                                <input placeholder="Email" type="email" value="" name="customer[email]" class="text" size="30" />
                                                                            </div>
                                                                            <div class="clearfix large_form form-password form-item">
                                                                                <input placeholder="Password" type="password" value="" name="customer[password]" class="password text" size="30" />
                                                                                <span class="cs-icon icon-eye"></span>
                                                                            </div>
                                                                            <div class="action_bottom">
                                                                                <button class="_btn" type="submit">Create</button>
                                                                            </div>
                                                                        </form>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <ul class="wish-compare-content">
                                                                <li class="link-item"><a href="wish-list.html">My Wishlist</a></li>
                                                                <li class="link-item"><a href="compare.html">My Compare</a></li>
                                                            </ul>
                                                            <ul class="currencies currencies-content">
                                                                <li class="currency-USD active">
                                                                    <a href="javascript:;">USD</a>
                                                                    <input type="hidden" value="USD" />
                                                                </li>
                                                                <li class="currency-GBP">
                                                                    <a href="javascript:;">GBP</a>
                                                                    <input type="hidden" value="GBP" />
                                                                </li>
                                                                <li class="currency-EUR">
                                                                    <a href="javascript:;">EUR</a>
                                                                    <input type="hidden" value="EUR" />
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="navMobile-menu">
                                            <div class="group_navbtn">
                                                <a href="javascript:void(0)" class="dropdown-toggle-navigation">
                                                    <span class="cs-icon icon-menu"></span>
                                                    <i class="sub-dropdown1 visible-sm visible-md visible-lg"></i>
                                                    <i class="sub-dropdown visible-sm visible-md visible-lg"></i>
                                                </a>
                                                <div class="navigation_dropdown_scroll dropdown-menu">
                                                    <ul class="navigation_links_mobile">
                                                        <li class="nav-item">
                                                            <a href="{{route('home')}}">
                                                                INICIO
                                                            </a>
                                                        </li>
                                                        <li class="nav-item">
                                                            <a href="{{route('productos')}}">
                                                                PROMOS
                                                            </a>
                                                        </li>
                                                        <li class="nav-item">
                                                            <a href="{{route('productos').'?category='.\App\Models\Icategoria::COMBO}}">
                                                                COMBOS
                                                            </a>
                                                        </li>
                                                        <li class="nav-item">
                                                            <a href="{{route('productos')}}">
                                                                PRODUCTOS
                                                            </a>
                                                        </li>
                                                        <li class="nav-item">
                                                            <a href="{{route('locales')}}">
                                                                CONTACTO
                                                            </a>
                                                        </li>
                                                        {{--<li class="nav-item navigation navigation_mobile">--}}
                                                        {{--<a href="collections.html" class="menu-mobile-link">--}}
                                                        {{--Hamburger--}}
                                                        {{--</a>--}}
                                                        {{--<a href="javascript:void(0)" class="arrow_sub arrow">--}}
                                                        {{--<i class="arrow-plus"></i>--}}
                                                        {{--</a>--}}
                                                        {{--<ul class="menu-mobile-container" style="display: none;">--}}
                                                        {{--<li class=" li-sub-mega">--}}
                                                        {{--<a tabindex="-1" href="collections.html">Whopper</a>--}}
                                                        {{--</li>--}}
                                                        {{--<li class=" li-sub-mega">--}}
                                                        {{--<a tabindex="-1" href="collections.html">Chicken Burger</a>--}}
                                                        {{--</li>--}}
                                                        {{--<li class=" li-sub-mega">--}}
                                                        {{--<a tabindex="-1" href="collections.html">Beef Burger</a>--}}
                                                        {{--</li>--}}
                                                        {{--<li class=" li-sub-mega">--}}
                                                        {{--<a tabindex="-1" href="collections.html">DOUBLE WHOPPER</a>--}}
                                                        {{--</li>--}}
                                                        {{--</ul>--}}
                                                        {{--</li>--}}
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</header>