<!-- BEGIN content_for_index -->
<div id="shopify-section-1507179171162" class="shopify-section index-section index-section-slideshow">
    <div data-section-id="1507179171162" data-section-type="slideshow-section">
        <section class="home-slideshow-layout">
            <div class="home-slideshow-wrapper">
                <div class="group-home-slideshow">
                    <div class="home-slideshow-inner">
                        <div class="home-slideshow-content slideshow_1507179171162">
                            <ul>
                                <li data-transition="random-static" data-masterspeed="2000" data-saveperformance="on">
                                    <img src="cliente/images/slider1.jpg"  alt="" data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat">
                                    <div class="slideshow-caption position-left transition-fade">
                                        <div class="group">
                                            <a href="collections.html">
                                                <img src="cliente/images/slideshow-caption-1.png" alt="" />
                                            </a>
                                            <a class="_btn" href="collections.html">Ver Más</a>
                                        </div>
                                    </div>
                                </li>
                                <li data-transition="random-static" data-masterspeed="2000" data-saveperformance="on">
                                    <img src="cliente/images/slider2.jpg" alt="" data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat">
                                    <div class="slideshow-caption position-left transition-slideup">
                                        <div class="group">
                                            <a href="collections.html">
                                                <img src="cliente/images/slideshow-caption-2.png" alt="" />
                                            </a>
                                            <a class="_btn" href="collections.html">Haz Tu Pedido</a>
                                        </div>
                                    </div>
                                </li>
                                <li data-transition="slideright" data-masterspeed="2000" data-saveperformance="on">
                                    <img src="cliente/images/slider3.jpg"  alt="" data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat">
                                    <div class="slideshow-caption position-middle transition-fade">
                                        <div class="group">
                                            <a href="collections.html">
                                                <img src="cliente/images/slideshow-caption-3.png" alt="" />
                                            </a>
                                            <a class="_btn" href="collections.html">¡Ver Más!</a>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                            <div class="tp-bannertimer"></div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
<div id="shopify-section-1509012157667" class="shopify-section index-section index-section-banner">
    <div data-section-id="1509012157667" data-section-type="banner-section">
        <section class="home-banslider-layout">
            <div class="container">
                <div class="row">
                    <div class="home-banslider-inner">
                        <div class="home-banslider-content">
                            <div class="banslider-item not-animated" data-animate="zoomIn" data-delay="100">
                                <a href="collections.html">
                                    <img src="cliente/images/00.jpg" alt="" title="">
                                </a>
                            </div>
                            <div class="banslider-item not-animated" data-animate="zoomIn" data-delay="200">
                                <a href="collections.html">
                                    <img src="cliente/images/01.jpg" alt="" title="">
                                </a>
                            </div>
                            <div class="banslider-item not-animated" data-animate="zoomIn" data-delay="300">
                                <a href="collections.html">
                                    <img src="cliente/images/02.jpg" alt="" title="">
                                </a>
                            </div>
                            <div class="banslider-item not-animated" data-animate="zoomIn" data-delay="400">
                                <a href="collections.html">
                                    <img src="cliente/images/00.jpg" alt="" title="">
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
<div id="shopify-section-1509012338884" class="shopify-section index-section index-section-welcome">
    <div data-section-id="1509012338884" data-section-type="welcome-section">
        <section class="home-welcome-layout not-animated" data-animate="zoomIn" data-delay="200">
            <div class="container">
                <div class="row">
                    <div class="home-welcome-inner">
                        <h2 class="page-title">¡Bienvenido a Fritomanía!</h2>
                        <div class="home-welcome-content">
                                            <span class="welcome-caption">
                                                Aliquam dapibus tincidunt metus. Praesent justo dolor, lobortis quis, lobortis dignissim, pulvinar ac, lorem.
                                                Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Praesent vestibulum molestie lacus. Aenean nonummy hendrerit mauris. Phasellus porta. Fusce suscipit varius mi. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Fusce feugiat malesuada odio.
                                            </span>
                            <img class="welcome-banner" src="cliente/images/1_3e7313a2-24ef-4dea-b4f4-19a75f8b51fa.png" alt="" title="">
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
<div class="shopify-section index-section index-section-product">
    <div data-section-id="1509012370397" data-section-type="product-section">
        <section class="home-product-layout">
            <div class="container">
                <div class="row">
                    <div class="banner-product-title not-animated" data-animate="fadeInUp" data-delay="200" style="background-image:  url('{{asset('cliente/images/fondo_1.jpg')}}');">
                        <div class="title-content">
                            <h2>Comida Venezolana en Santiago </h2>
                        </div>
                    </div>
                    <div class="home-product-inner">
                        <div class="home-product-content">
                            <div class="content_product col-sm-3 not-animated" data-animate="fadeInUp" data-delay="100">
                                <div class="row-container product list-unstyled clearfix product-circle">
                                    <div class="row-left">
                                        <a href="product.html" class="hoverBorder container_item">
                                            <div class="hoverBorderWrapper">
                                                <img src="cliente/images/21CocaNormal.jpg" class="img-responsive front" alt="Juice Ice Tea">
                                                <div class="mask"></div>
                                            </div>
                                        </a>
                                        <div class="hover-mask">
                                            <div class="group-mask">
                                                <div class="inner-mask">
                                                    <div class="group-actionbutton">
                                                        <ul class="quickview-wishlist-wrapper">
                                                            <li class="wishlist">
                                                                <a title="Añadir a Lista de Deseos" class="wishlist wishlist-juice-ice-tea" data-wishlisthandle="juice-ice-tea"><span class="cs-icon icon-heart"></span></a>
                                                            </li>
                                                            <li class="quickview hidden-xs hidden-sm">
                                                                <div class="product-ajax-cart">
                                                                    <span class="overlay_mask"></span>
                                                                    <div data-handle="juice-ice-tea" data-target="#quick-shop-modal" class="quick_shop" data-toggle="modal">
                                                                        <a class=""><span class="cs-icon icon-eye"></span></a>
                                                                    </div>
                                                                </div>
                                                            </li>
                                                            <li class="compare">
                                                                <a title="Comparar" class="compare compare-juice-ice-tea" data-comparehandle="juice-ice-tea"><span class="cs-icon icon-retweet2"></span></a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <form action="http://demo.designshopify.com/html_fastfood/cart.html" method="post">
                                                        <div class="effect-ajax-cart">
                                                            <input type="hidden" name="quantity" value="1">
                                                            <button class="_btn add-to-cart" data-parent=".parent-fly" type="submit" name="add" title="Añadir al Carro">Añadir al Carro</button>
                                                        </div>
                                                    </form>
                                                </div>
                                                <!--inner-mask-->
                                            </div>
                                            <!--Group mask-->
                                        </div>
                                        <div class="product-label">
                                            <div class="label-element new-label">
                                                <span>Nuevo</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row-right animMix">
                                        <div class="rating-star">
                                                            <span class="spr-badge" data-rating="0.0"><span class="spr-starrating spr-badge-starrating"><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i></span><span class="spr-badge-caption">No reviews</span>
                                                            </span>
                                        </div>
                                        <div class="product-title"><a class="title-5" href="product.html">COCA COLA 1.5 Lts</a></div>
                                        <div class="product-price">
                                                            <span class="price">
                                                                <span class="money" data-currency-usd="$1500.00">$1500.00</span>
                                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="content_product col-sm-3 not-animated" data-animate="fadeInUp" data-delay="200">
                                <div class="row-container product list-unstyled clearfix product-circle">
                                    <div class="row-left">
                                        <a href="product.html" class="hoverBorder container_item">
                                            <div class="hoverBorderWrapper">
                                                <img src="cliente/images/coca-cola-tradicional-en-lata-350-ml.jpg" class="img-responsive front" alt="Coke">
                                                <div class="mask"></div>
                                            </div>
                                        </a>
                                        <div class="hover-mask">
                                            <div class="group-mask">
                                                <div class="inner-mask">
                                                    <div class="group-actionbutton">
                                                        <ul class="quickview-wishlist-wrapper">
                                                            <li class="wishlist">
                                                                <a title="Añadir a Lista de Deseos" class="wishlist wishlist-coke" data-wishlisthandle="coke"><span class="cs-icon icon-heart"></span></a>
                                                            </li>
                                                            <li class="quickview hidden-xs hidden-sm">
                                                                <div class="product-ajax-cart">
                                                                    <span class="overlay_mask"></span>
                                                                    <div data-handle="coke" data-target="#quick-shop-modal" class="quick_shop" data-toggle="modal">
                                                                        <a class=""><span class="cs-icon icon-eye"></span></a>
                                                                    </div>
                                                                </div>
                                                            </li>
                                                            <li class="compare">
                                                                <a title="Comparar" class="compare compare-coke" data-comparehandle="coke"><span class="cs-icon icon-retweet2"></span></a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <form action="http://demo.designshopify.com/html_fastfood/cart.html" method="post">
                                                        <div class="effect-ajax-cart">
                                                            <button class="_btn select-option" type="button" onclick="window.location='product.html';" title="Opciones">Opciones</button>
                                                        </div>
                                                    </form>
                                                </div>
                                                <!--inner-mask-->
                                            </div>
                                            <!--Group mask-->
                                        </div>
                                        <div class="product-label">
                                            <div class="label-element deal-label">
                                                <span>33%</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row-right animMix">
                                        <div class="rating-star">
                                                            <span class="spr-badge" data-rating="5.0"><span class="spr-starrating spr-badge-starrating"><i class="spr-icon spr-icon-star" style=""></i><i class="spr-icon spr-icon-star" style=""></i><i class="spr-icon spr-icon-star" style=""></i><i class="spr-icon spr-icon-star" style=""></i><i class="spr-icon spr-icon-star" style=""></i></span><span class="spr-badge-caption">1 review</span>
                                                            </span>
                                        </div>
                                        <div class="product-title"><a class="title-5" href="product.html">COCA COLA LATA 350 CC</a></div>
                                        <div class="product-price">
                                            <span class="price_sale"><span class="money" data-currency-usd="$10.00">$450.00</span></span>
                                            <del class="price_compare"> <span class="money" data-currency-usd="$15.00">$500.00</span></del>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="content_product col-sm-3 not-animated" data-animate="fadeInUp" data-delay="300">
                                <div class="row-container product list-unstyled clearfix product-circle">
                                    <div class="row-left">
                                        <a href="product.html" class="hoverBorder container_item">
                                            <div class="hoverBorderWrapper">
                                                <img src="cliente/images/coca-cola-ligth-en-lata-350ml.jpg" class="img-responsive front" alt="Juice Ice Tea">
                                                <div class="mask"></div>
                                            </div>
                                        </a>
                                        <div class="hover-mask">
                                            <div class="group-mask">
                                                <div class="inner-mask">
                                                    <div class="group-actionbutton">
                                                        <ul class="quickview-wishlist-wrapper">
                                                            <li class="wishlist">
                                                                <a title="Añadir a Lista de Deseos" class="wishlist wishlist-juice-ice-tea" data-wishlisthandle="juice-ice-tea"><span class="cs-icon icon-heart"></span></a>
                                                            </li>
                                                            <li class="quickview hidden-xs hidden-sm">
                                                                <div class="product-ajax-cart">
                                                                    <span class="overlay_mask"></span>
                                                                    <div data-handle="juice-ice-tea" data-target="#quick-shop-modal" class="quick_shop" data-toggle="modal">
                                                                        <a class=""><span class="cs-icon icon-eye"></span></a>
                                                                    </div>
                                                                </div>
                                                            </li>
                                                            <li class="compare">
                                                                <a title="Comparar" class="compare compare-juice-ice-tea" data-comparehandle="juice-ice-tea"><span class="cs-icon icon-retweet2"></span></a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <form action="http://demo.designshopify.com/html_fastfood/cart.html" method="post">
                                                        <div class="effect-ajax-cart">
                                                            <input type="hidden" name="quantity" value="1">
                                                            <button class="_btn add-to-cart" data-parent=".parent-fly" type="submit" name="add" title="Añadir al Carro">Añadir al Carro</button>
                                                        </div>
                                                    </form>
                                                </div>
                                                <!--inner-mask-->
                                            </div>
                                            <!--Group mask-->
                                        </div>
                                        <div class="product-label">
                                            <div class="label-element hotting-label">
                                                <span>Popular</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row-right animMix">
                                        <div class="rating-star">
                                                            <span class="spr-badge" data-rating="0.0"><span class="spr-starrating spr-badge-starrating"><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i></span><span class="spr-badge-caption">No reviews</span>
                                                            </span>
                                        </div>
                                        <div class="product-title"><a class="title-5" href="product.html">COCA COLA ZERO LATA 350 CC</a></div>
                                        <div class="product-price">
                                                            <span class="price">
                                                                <span class="money" data-currency-usd="$20.00">$450.00</span>
                                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="content_product col-sm-3 not-animated" data-animate="fadeInUp" data-delay="400">
                                <div class="row-container product list-unstyled clearfix product-circle">
                                    <div class="row-left">
                                        <a href="product.html" class="hoverBorder container_item">
                                            <div class="hoverBorderWrapper">
                                                <img src="cliente/images/tequenos.jpeg" class="img-responsive front" alt="Juice Ice Tea">
                                                <div class="mask"></div>
                                            </div>
                                        </a>
                                        <div class="hover-mask">
                                            <div class="group-mask">
                                                <div class="inner-mask">
                                                    <div class="group-actionbutton">
                                                        <ul class="quickview-wishlist-wrapper">
                                                            <li class="wishlist">
                                                                <a title="Añadir a Lista de Deseos" class="wishlist wishlist-juice-ice-tea" data-wishlisthandle="juice-ice-tea"><span class="cs-icon icon-heart"></span></a>
                                                            </li>
                                                            <li class="quickview hidden-xs hidden-sm">
                                                                <div class="product-ajax-cart">
                                                                    <span class="overlay_mask"></span>
                                                                    <div data-handle="juice-ice-tea" data-target="#quick-shop-modal" class="quick_shop" data-toggle="modal">
                                                                        <a class=""><span class="cs-icon icon-eye"></span></a>
                                                                    </div>
                                                                </div>
                                                            </li>
                                                            <li class="compare">
                                                                <a title="Comparar" class="compare compare-juice-ice-tea" data-comparehandle="juice-ice-tea"><span class="cs-icon icon-retweet2"></span></a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <form action="http://demo.designshopify.com/html_fastfood/cart.html" method="post">
                                                        <div class="effect-ajax-cart">
                                                            <input type="hidden" name="quantity" value="1">
                                                            <button class="_btn add-to-cart" data-parent=".parent-fly" type="submit" name="add" title="Añadir al Carro">Añadir al Carro</button>
                                                        </div>
                                                    </form>
                                                </div>
                                                <!--inner-mask-->
                                            </div>
                                            <!--Group mask-->
                                        </div>
                                    </div>
                                    <div class="row-right animMix">
                                        <div class="rating-star">
                                                            <span class="spr-badge" data-rating="0.0"><span class="spr-starrating spr-badge-starrating"><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i></span><span class="spr-badge-caption">No reviews</span>
                                                            </span>
                                        </div>
                                        <div class="product-title"><a class="title-5" href="product.html">COMBO 100 TEQUEÑOS</a></div>
                                        <div class="product-price">
                                                            <span class="price">
                                                                <span class="money" data-currency-usd="$20.00">$1000.00</span>
                                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
<div class="shopify-section index-section index-section-product">
    <div data-section-id="1509012370397" data-section-type="product-section">
        <section class="home-product-layout">
            <div class="container">
                <div class="row">
                    <div class="banner-product-title not-animated" data-animate="fadeInUp" data-delay="200" style="background-image:  url('{{asset('cliente/images/fondo_1.jpg')}}');">
                        <div class="title-content">
                            <h2>Martes de Delivery Gratis</h2>
                        </div>
                    </div>
                    <div class="home-product-inner">
                        <div class="home-product-content">
                            <div class="content_product col-sm-3 not-animated" data-animate="fadeInUp" data-delay="100">
                                <div class="row-container product list-unstyled clearfix product-circle">
                                    <div class="row-left">
                                        <a href="product.html" class="hoverBorder container_item">
                                            <div class="hoverBorderWrapper">
                                                <img src="cliente/images/17.jpeg" class="img-responsive front" alt="Juice Ice Tea">
                                                <div class="mask"></div>
                                            </div>
                                        </a>
                                        <div class="hover-mask">
                                            <div class="group-mask">
                                                <div class="inner-mask">
                                                    <div class="group-actionbutton">
                                                        <ul class="quickview-wishlist-wrapper">
                                                            <li class="wishlist">
                                                                <a title="Añadir a Lista de Deseos" class="wishlist wishlist-juice-ice-tea" data-wishlisthandle="juice-ice-tea"><span class="cs-icon icon-heart"></span></a>
                                                            </li>
                                                            <li class="quickview hidden-xs hidden-sm">
                                                                <div class="product-ajax-cart">
                                                                    <span class="overlay_mask"></span>
                                                                    <div data-handle="juice-ice-tea" data-target="#quick-shop-modal" class="quick_shop" data-toggle="modal">
                                                                        <a class=""><span class="cs-icon icon-eye"></span></a>
                                                                    </div>
                                                                </div>
                                                            </li>
                                                            <li class="compare">
                                                                <a title="Comparar" class="compare compare-juice-ice-tea" data-comparehandle="juice-ice-tea"><span class="cs-icon icon-retweet2"></span></a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <form action="http://demo.designshopify.com/html_fastfood/cart.html" method="post">
                                                        <div class="effect-ajax-cart">
                                                            <input type="hidden" name="quantity" value="1">
                                                            <button class="_btn add-to-cart" data-parent=".parent-fly" type="submit" name="add" title="Añadir al Carro">Añadir al Carro</button>
                                                        </div>
                                                    </form>
                                                </div>
                                                <!--inner-mask-->
                                            </div>
                                            <!--Group mask-->
                                        </div>
                                        <div class="product-label">
                                            <div class="label-element best-label">
                                                <span>Mejor</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row-right animMix">
                                        <div class="rating-star">
                                                            <span class="spr-badge" data-rating="0.0"><span class="spr-starrating spr-badge-starrating"><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i></span><span class="spr-badge-caption">No reviews</span>
                                                            </span>
                                        </div>
                                        <div class="product-title"><a class="title-5" href="product.html">COMBO FAMILIAR</a></div>
                                        <div class="product-price">
                                                            <span class="price">
                                                                <span class="money" data-currency-usd="$20.00">$6500.00</span>
                                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="content_product col-sm-3 not-animated" data-animate="fadeInUp" data-delay="200">
                                <div class="row-container product list-unstyled clearfix product-circle">
                                    <div class="row-left">
                                        <a href="product.html" class="hoverBorder container_item">
                                            <div class="hoverBorderWrapper">
                                                <img src="cliente/images/20.jpeg" class="img-responsive front" alt="Coke">
                                                <div class="mask"></div>
                                            </div>
                                        </a>
                                        <div class="hover-mask">
                                            <div class="group-mask">
                                                <div class="inner-mask">
                                                    <div class="group-actionbutton">
                                                        <ul class="quickview-wishlist-wrapper">
                                                            <li class="wishlist">
                                                                <a title="Añadir a Lista de Deseos" class="wishlist wishlist-coke" data-wishlisthandle="coke"><span class="cs-icon icon-heart"></span></a>
                                                            </li>
                                                            <li class="quickview hidden-xs hidden-sm">
                                                                <div class="product-ajax-cart">
                                                                    <span class="overlay_mask"></span>
                                                                    <div data-handle="coke" data-target="#quick-shop-modal" class="quick_shop" data-toggle="modal">
                                                                        <a class=""><span class="cs-icon icon-eye"></span></a>
                                                                    </div>
                                                                </div>
                                                            </li>
                                                            <li class="compare">
                                                                <a title="Comparar" class="compare compare-coke" data-comparehandle="coke"><span class="cs-icon icon-retweet2"></span></a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <form action="http://demo.designshopify.com/html_fastfood/cart.html" method="post">
                                                        <div class="effect-ajax-cart">
                                                            <button class="_btn select-option" type="button" onclick="window.location='product.html';" title="Opciones">Opciones</button>
                                                        </div>
                                                    </form>
                                                </div>
                                                <!--inner-mask-->
                                            </div>
                                            <!--Group mask-->
                                        </div>
                                        <div class="product-label">
                                            <div class="label-element hotting-label">
                                                <span>Popular</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row-right animMix">
                                        <div class="rating-star">
                                                            <span class="spr-badge" data-rating="5.0"><span class="spr-starrating spr-badge-starrating"><i class="spr-icon spr-icon-star" style=""></i><i class="spr-icon spr-icon-star" style=""></i><i class="spr-icon spr-icon-star" style=""></i><i class="spr-icon spr-icon-star" style=""></i><i class="spr-icon spr-icon-star" style=""></i></span><span class="spr-badge-caption">1 review</span>
                                                            </span>
                                        </div>
                                        <div class="product-title"><a class="title-5" href="product.html">COMBO HAMBURGUESA</a></div>
                                        <div class="product-price">
                                            <span class="price_sale"><span class="money" data-currency-usd="$10.00">$3000.00</span></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="content_product col-sm-3 not-animated" data-animate="fadeInUp" data-delay="300">
                                <div class="row-container product list-unstyled clearfix product-circle">
                                    <div class="row-left">
                                        <a href="product.html" class="hoverBorder container_item">
                                            <div class="hoverBorderWrapper">
                                                <img src="cliente/images/12.jpeg" class="img-responsive front" alt="Juice Ice Tea">
                                                <div class="mask"></div>
                                            </div>
                                        </a>
                                        <div class="hover-mask">
                                            <div class="group-mask">
                                                <div class="inner-mask">
                                                    <div class="group-actionbutton">
                                                        <ul class="quickview-wishlist-wrapper">
                                                            <li class="wishlist">
                                                                <a title="Añadir a Lista de Deseos" class="wishlist wishlist-juice-ice-tea" data-wishlisthandle="juice-ice-tea"><span class="cs-icon icon-heart"></span></a>
                                                            </li>
                                                            <li class="quickview hidden-xs hidden-sm">
                                                                <div class="product-ajax-cart">
                                                                    <span class="overlay_mask"></span>
                                                                    <div data-handle="juice-ice-tea" data-target="#quick-shop-modal" class="quick_shop" data-toggle="modal">
                                                                        <a class=""><span class="cs-icon icon-eye"></span></a>
                                                                    </div>
                                                                </div>
                                                            </li>
                                                            <li class="compare">
                                                                <a title="Comparar" class="compare compare-juice-ice-tea" data-comparehandle="juice-ice-tea"><span class="cs-icon icon-retweet2"></span></a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <form action="http://demo.designshopify.com/html_fastfood/cart.html" method="post">
                                                        <div class="effect-ajax-cart">
                                                            <input type="hidden" name="quantity" value="1">
                                                            <button class="_btn add-to-cart" data-parent=".parent-fly" type="submit" name="add" title="Añadir al Carro">Añadir al Carro</button>
                                                        </div>
                                                    </form>
                                                </div>
                                                <!--inner-mask-->
                                            </div>
                                            <!--Group mask-->
                                        </div>
                                        <div class="product-label">
                                            <div class="label-element deal-label">
                                                <span>33%</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row-right animMix">
                                        <div class="rating-star">
                                                            <span class="spr-badge" data-rating="0.0"><span class="spr-starrating spr-badge-starrating"><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i></span><span class="spr-badge-caption">No reviews</span>
                                                            </span>
                                        </div>
                                        <div class="product-title"><a class="title-5" href="product.html">COMBO JUNIOR</a></div>
                                        <div class="product-price">
                                            <span class="price_sale"><span class="money" data-currency-usd="$10.00">$7500.00</span></span>
                                            <del class="price_compare"> <span class="money" data-currency-usd="$15.00">$8000.00</span></del>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="content_product col-sm-3 not-animated" data-animate="fadeInUp" data-delay="400">
                                <div class="row-container product list-unstyled clearfix product-circle">
                                    <div class="row-left">
                                        <a href="product.html" class="hoverBorder container_item">
                                            <div class="hoverBorderWrapper">
                                                <img src="cliente/images/17.jpeg" class="img-responsive front" alt="Juice Ice Tea">
                                                <div class="mask"></div>
                                            </div>
                                        </a>
                                        <div class="hover-mask">
                                            <div class="group-mask">
                                                <div class="inner-mask">
                                                    <div class="group-actionbutton">
                                                        <ul class="quickview-wishlist-wrapper">
                                                            <li class="wishlist">
                                                                <a title="Añadir a Lista de Deseos" class="wishlist wishlist-juice-ice-tea" data-wishlisthandle="juice-ice-tea"><span class="cs-icon icon-heart"></span></a>
                                                            </li>
                                                            <li class="quickview hidden-xs hidden-sm">
                                                                <div class="product-ajax-cart">
                                                                    <span class="overlay_mask"></span>
                                                                    <div data-handle="juice-ice-tea" data-target="#quick-shop-modal" class="quick_shop" data-toggle="modal">
                                                                        <a class=""><span class="cs-icon icon-eye"></span></a>
                                                                    </div>
                                                                </div>
                                                            </li>
                                                            <li class="compare">
                                                                <a title="Comparar" class="compare compare-juice-ice-tea" data-comparehandle="juice-ice-tea"><span class="cs-icon icon-retweet2"></span></a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <form action="http://demo.designshopify.com/html_fastfood/cart.html" method="post">
                                                        <div class="effect-ajax-cart">
                                                            <input type="hidden" name="quantity" value="1">
                                                            <button class="_btn add-to-cart" data-parent=".parent-fly" type="submit" name="add" title="Añadir al Carro">Añadir al Carro</button>
                                                        </div>
                                                    </form>
                                                </div>
                                                <!--inner-mask-->
                                            </div>
                                            <!--Group mask-->
                                        </div>
                                        <div class="product-label">
                                            <div class="label-element new-label">
                                                <span>Nuevo</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row-right animMix">
                                        <div class="rating-star">
                                                            <span class="spr-badge" data-rating="0.0"><span class="spr-starrating spr-badge-starrating"><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i></span><span class="spr-badge-caption">No reviews</span>
                                                            </span>
                                        </div>
                                        <div class="product-title"><a class="title-5" href="product.html">COMBO MEDIO AREPA</a></div>
                                        <div class="product-price">
                                                            <span class="price">
                                                                <span class="money" data-currency-usd="$20.00">$1500.00</span>
                                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
<div class="shopify-section index-section index-section-product">
    <div data-section-id="1509012370397" data-section-type="product-section">
        <section class="home-product-layout">
            <div class="container">
                <div class="row">
                    <div class="banner-product-title not-animated" data-animate="fadeInUp" data-delay="200" style="background-image:  url('{{asset('cliente/images/fondo_1.jpg')}}');">
                        <div class="title-content">
                            <h2>Próximamente Nueva Sede en Las Condes</h2>
                        </div>
                    </div>
                    <div class="home-product-inner">
                        <div class="home-product-content">
                            <div class="content_product col-sm-3 not-animated" data-animate="fadeInUp" data-delay="100">
                                <div class="row-container product list-unstyled clearfix product-circle">
                                    <div class="row-left">
                                        <a href="product.html" class="hoverBorder container_item">
                                            <div class="hoverBorderWrapper">
                                                <img src="cliente/images/14.jpeg" class="img-responsive front" alt="Juice Ice Tea">
                                                <div class="mask"></div>
                                            </div>
                                        </a>
                                        <div class="hover-mask">
                                            <div class="group-mask">
                                                <div class="inner-mask">
                                                    <div class="group-actionbutton">
                                                        <ul class="quickview-wishlist-wrapper">
                                                            <li class="wishlist">
                                                                <a title="Añadir a Lista de Deseos" class="wishlist wishlist-juice-ice-tea" data-wishlisthandle="juice-ice-tea"><span class="cs-icon icon-heart"></span></a>
                                                            </li>
                                                            <li class="quickview hidden-xs hidden-sm">
                                                                <div class="product-ajax-cart">
                                                                    <span class="overlay_mask"></span>
                                                                    <div data-handle="juice-ice-tea" data-target="#quick-shop-modal" class="quick_shop" data-toggle="modal">
                                                                        <a class=""><span class="cs-icon icon-eye"></span></a>
                                                                    </div>
                                                                </div>
                                                            </li>
                                                            <li class="compare">
                                                                <a title="Comparar" class="compare compare-juice-ice-tea" data-comparehandle="juice-ice-tea"><span class="cs-icon icon-retweet2"></span></a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <form action="http://demo.designshopify.com/html_fastfood/cart.html" method="post">
                                                        <div class="effect-ajax-cart">
                                                            <input type="hidden" name="quantity" value="1">
                                                            <button class="_btn add-to-cart" data-parent=".parent-fly" type="submit" name="add" title="Añadir al Carro">Añadir al Carro</button>
                                                        </div>
                                                    </form>
                                                </div>
                                                <!--inner-mask-->
                                            </div>
                                            <!--Group mask-->
                                        </div>
                                    </div>
                                    <div class="row-right animMix">
                                        <div class="rating-star">
                                                            <span class="spr-badge" data-rating="0.0"><span class="spr-starrating spr-badge-starrating"><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i></span><span class="spr-badge-caption">No reviews</span>
                                                            </span>
                                        </div>
                                        <div class="product-title"><a class="title-5" href="product.html">COMBO MEDIO SENCILLO</a></div>
                                        <div class="product-price">
                                                            <span class="price">
                                                                <span class="money" data-currency-usd="$20.00">$1750.00</span>
                                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="content_product col-sm-3 not-animated" data-animate="fadeInUp" data-delay="200">
                                <div class="row-container product list-unstyled clearfix product-circle">
                                    <div class="row-left">
                                        <a href="product.html" class="hoverBorder container_item">
                                            <div class="hoverBorderWrapper">
                                                <img src="cliente/images/14.jpeg" class="img-responsive front" alt="Juice Ice Tea">
                                                <div class="mask"></div>
                                            </div>
                                        </a>
                                        <div class="hover-mask">
                                            <div class="group-mask">
                                                <div class="inner-mask">
                                                    <div class="group-actionbutton">
                                                        <ul class="quickview-wishlist-wrapper">
                                                            <li class="wishlist">
                                                                <a title="Añadir a Lista de Deseos" class="wishlist wishlist-juice-ice-tea" data-wishlisthandle="juice-ice-tea"><span class="cs-icon icon-heart"></span></a>
                                                            </li>
                                                            <li class="quickview hidden-xs hidden-sm">
                                                                <div class="product-ajax-cart">
                                                                    <span class="overlay_mask"></span>
                                                                    <div data-handle="juice-ice-tea" data-target="#quick-shop-modal" class="quick_shop" data-toggle="modal">
                                                                        <a class=""><span class="cs-icon icon-eye"></span></a>
                                                                    </div>
                                                                </div>
                                                            </li>
                                                            <li class="compare">
                                                                <a title="Comparar" class="compare compare-juice-ice-tea" data-comparehandle="juice-ice-tea"><span class="cs-icon icon-retweet2"></span></a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <form action="http://demo.designshopify.com/html_fastfood/cart.html" method="post">
                                                        <div class="effect-ajax-cart">
                                                            <input type="hidden" name="quantity" value="1">
                                                            <button class="_btn add-to-cart" data-parent=".parent-fly" type="submit" name="add" title="Añadir al Carro">Añadir al Carro</button>
                                                        </div>
                                                    </form>
                                                </div>
                                                <!--inner-mask-->
                                            </div>
                                            <!--Group mask-->
                                        </div>
                                        <div class="product-label">
                                            <div class="label-element new-label">
                                                <span>Nuevo</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row-right animMix">
                                        <div class="rating-star">
                                                            <span class="spr-badge" data-rating="0.0"><span class="spr-starrating spr-badge-starrating"><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i></span><span class="spr-badge-caption">No reviews</span>
                                                            </span>
                                        </div>
                                        <div class="product-title"><a class="title-5" href="product.html">COMBO MEDIO SUPER PATACÓN</a></div>
                                        <div class="product-price">
                                                            <span class="price">
                                                                <span class="money" data-currency-usd="$20.00">$5000.00</span>
                                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="content_product col-sm-3 not-animated" data-animate="fadeInUp" data-delay="300">
                                <div class="row-container product list-unstyled clearfix product-circle">
                                    <div class="row-left">
                                        <a href="product.html" class="hoverBorder container_item">
                                            <div class="hoverBorderWrapper">
                                                <img src="cliente/images/14.jpeg" class="img-responsive front" alt="Coke">
                                                <div class="mask"></div>
                                            </div>
                                        </a>
                                        <div class="hover-mask">
                                            <div class="group-mask">
                                                <div class="inner-mask">
                                                    <div class="group-actionbutton">
                                                        <ul class="quickview-wishlist-wrapper">
                                                            <li class="wishlist">
                                                                <a title="Añadir a Lista de Deseos" class="wishlist wishlist-coke" data-wishlisthandle="coke"><span class="cs-icon icon-heart"></span></a>
                                                            </li>
                                                            <li class="quickview hidden-xs hidden-sm">
                                                                <div class="product-ajax-cart">
                                                                    <span class="overlay_mask"></span>
                                                                    <div data-handle="coke" data-target="#quick-shop-modal" class="quick_shop" data-toggle="modal">
                                                                        <a class=""><span class="cs-icon icon-eye"></span></a>
                                                                    </div>
                                                                </div>
                                                            </li>
                                                            <li class="compare">
                                                                <a title="Comparar" class="compare compare-coke" data-comparehandle="coke"><span class="cs-icon icon-retweet2"></span></a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <form action="http://demo.designshopify.com/html_fastfood/cart.html" method="post">
                                                        <div class="effect-ajax-cart">
                                                            <button class="_btn select-option" type="button" onclick="window.location='product.html';" title="Opciones">Opciones</button>
                                                        </div>
                                                    </form>
                                                </div>
                                                <!--inner-mask-->
                                            </div>
                                            <!--Group mask-->
                                        </div>
                                    </div>
                                    <div class="row-right animMix">
                                        <div class="rating-star">
                                                            <span class="spr-badge" data-rating="5.0"><span class="spr-starrating spr-badge-starrating"><i class="spr-icon spr-icon-star" style=""></i><i class="spr-icon spr-icon-star" style=""></i><i class="spr-icon spr-icon-star" style=""></i><i class="spr-icon spr-icon-star" style=""></i><i class="spr-icon spr-icon-star" style=""></i></span><span class="spr-badge-caption">1 review</span>
                                                            </span>
                                        </div>
                                        <div class="product-title"><a class="title-5" href="product.html">COMBO MEDIO TUMBARRANCHO</a></div>
                                        <div class="product-price">
                                            <span class="price_sale"><span class="money" data-currency-usd="$10.00">$2000.00</span></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="content_product col-sm-3 not-animated" data-animate="fadeInUp" data-delay="400">
                                <div class="row-container product list-unstyled clearfix product-circle">
                                    <div class="row-left">
                                        <a href="product.html" class="hoverBorder container_item">
                                            <div class="hoverBorderWrapper">
                                                <img src="cliente/images/23.jpeg" class="img-responsive front" alt="Juice Ice Tea">
                                                <div class="mask"></div>
                                            </div>
                                        </a>
                                        <div class="hover-mask">
                                            <div class="group-mask">
                                                <div class="inner-mask">
                                                    <div class="group-actionbutton">
                                                        <ul class="quickview-wishlist-wrapper">
                                                            <li class="wishlist">
                                                                <a title="Añadir a Lista de Deseos" class="wishlist wishlist-juice-ice-tea" data-wishlisthandle="juice-ice-tea"><span class="cs-icon icon-heart"></span></a>
                                                            </li>
                                                            <li class="quickview hidden-xs hidden-sm">
                                                                <div class="product-ajax-cart">
                                                                    <span class="overlay_mask"></span>
                                                                    <div data-handle="juice-ice-tea" data-target="#quick-shop-modal" class="quick_shop" data-toggle="modal">
                                                                        <a class=""><span class="cs-icon icon-eye"></span></a>
                                                                    </div>
                                                                </div>
                                                            </li>
                                                            <li class="compare">
                                                                <a title="Comparar" class="compare compare-juice-ice-tea" data-comparehandle="juice-ice-tea"><span class="cs-icon icon-retweet2"></span></a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <form action="http://demo.designshopify.com/html_fastfood/cart.html" method="post">
                                                        <div class="effect-ajax-cart">
                                                            <input type="hidden" name="quantity" value="1">
                                                            <button class="_btn add-to-cart" data-parent=".parent-fly" type="submit" name="add" title="Añadir al Carro">Añadir al Carro</button>
                                                        </div>
                                                    </form>
                                                </div>
                                                <!--inner-mask-->
                                            </div>
                                            <!--Group mask-->
                                        </div>
                                        <div class="product-label">
                                            <div class="label-element hotting-label">
                                                <span>Popular</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row-right animMix">
                                        <div class="rating-star">
                                                            <span class="spr-badge" data-rating="0.0"><span class="spr-starrating spr-badge-starrating"><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i></span><span class="spr-badge-caption">No reviews</span>
                                                            </span>
                                        </div>
                                        <div class="product-title"><a class="title-5" href="product.html">COMBO PATACONES</a></div>
                                        <div class="product-price">
                                                            <span class="price">
                                                                <span class="money" data-currency-usd="$20.00">$6000.00</span>
                                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
<div class="shopify-section index-section index-section-product">
    <div data-section-id="1509012370397" data-section-type="product-section">
        <section class="home-product-layout">
            <div class="container">
                <div class="row">
                    <div class="banner-product-title not-animated" data-animate="fadeInUp" data-delay="200" style="background-image:  url('{{asset('cliente/images/fondo_1.jpg')}}');">
                        <div class="title-content">
                            <h2>Necesitas una Hamburguesa en tu Vida</h2>
                        </div>
                    </div>
                    <div class="home-product-inner">
                        <div class="home-product-content">
                            <div class="content_product col-sm-3 not-animated" data-animate="fadeInUp" data-delay="100">
                                <div class="row-container product list-unstyled clearfix product-circle">
                                    <div class="row-left">
                                        <a href="product.html" class="hoverBorder container_item">
                                            <div class="hoverBorderWrapper">
                                                <img src="cliente/images/8.jpeg" class="img-responsive front" alt="Coke">
                                                <div class="mask"></div>
                                            </div>
                                        </a>
                                        <div class="hover-mask">
                                            <div class="group-mask">
                                                <div class="inner-mask">
                                                    <div class="group-actionbutton">
                                                        <ul class="quickview-wishlist-wrapper">
                                                            <li class="wishlist">
                                                                <a title="Añadir a Lista de Deseos" class="wishlist wishlist-coke" data-wishlisthandle="coke"><span class="cs-icon icon-heart"></span></a>
                                                            </li>
                                                            <li class="quickview hidden-xs hidden-sm">
                                                                <div class="product-ajax-cart">
                                                                    <span class="overlay_mask"></span>
                                                                    <div data-handle="coke" data-target="#quick-shop-modal" class="quick_shop" data-toggle="modal">
                                                                        <a class=""><span class="cs-icon icon-eye"></span></a>
                                                                    </div>
                                                                </div>
                                                            </li>
                                                            <li class="compare">
                                                                <a title="Comparar" class="compare compare-coke" data-comparehandle="coke"><span class="cs-icon icon-retweet2"></span></a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <form action="http://demo.designshopify.com/html_fastfood/cart.html" method="post">
                                                        <div class="effect-ajax-cart">
                                                            <button class="_btn select-option" type="button" onclick="window.location='product.html';" title="Opciones">Opciones</button>
                                                        </div>
                                                    </form>
                                                </div>
                                                <!--inner-mask-->
                                            </div>
                                            <!--Group mask-->
                                        </div>
                                        <div class="product-label">
                                            <div class="label-element deal-label">
                                                <span>33%</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row-right animMix">
                                        <div class="rating-star">
                                                            <span class="spr-badge" data-rating="5.0"><span class="spr-starrating spr-badge-starrating"><i class="spr-icon spr-icon-star" style=""></i><i class="spr-icon spr-icon-star" style=""></i><i class="spr-icon spr-icon-star" style=""></i><i class="spr-icon spr-icon-star" style=""></i><i class="spr-icon spr-icon-star" style=""></i></span><span class="spr-badge-caption">1 review</span>
                                                            </span>
                                        </div>
                                        <div class="product-title"><a class="title-5" href="product.html">EMPANADAS</a></div>
                                        <div class="product-price">
                                            <span class="price_sale"><span class="money" data-currency-usd="$10.00">$600.00</span></span>
                                            <del class="price_compare"> <span class="money" data-currency-usd="$15.00">$500.00</span></del>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="content_product col-sm-3 not-animated" data-animate="fadeInUp" data-delay="200">
                                <div class="row-container product list-unstyled clearfix product-circle">
                                    <div class="row-left">
                                        <a href="product.html" class="hoverBorder container_item">
                                            <div class="hoverBorderWrapper">
                                                <img src="cliente/images/fanta-lata-350-cc.jpg" class="img-responsive front" alt="Juice Ice Tea">
                                                <div class="mask"></div>
                                            </div>
                                        </a>
                                        <div class="hover-mask">
                                            <div class="group-mask">
                                                <div class="inner-mask">
                                                    <div class="group-actionbutton">
                                                        <ul class="quickview-wishlist-wrapper">
                                                            <li class="wishlist">
                                                                <a title="Añadir a Lista de Deseos" class="wishlist wishlist-juice-ice-tea" data-wishlisthandle="juice-ice-tea"><span class="cs-icon icon-heart"></span></a>
                                                            </li>
                                                            <li class="quickview hidden-xs hidden-sm">
                                                                <div class="product-ajax-cart">
                                                                    <span class="overlay_mask"></span>
                                                                    <div data-handle="juice-ice-tea" data-target="#quick-shop-modal" class="quick_shop" data-toggle="modal">
                                                                        <a class=""><span class="cs-icon icon-eye"></span></a>
                                                                    </div>
                                                                </div>
                                                            </li>
                                                            <li class="compare">
                                                                <a title="Comparar" class="compare compare-juice-ice-tea" data-comparehandle="juice-ice-tea"><span class="cs-icon icon-retweet2"></span></a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <form action="http://demo.designshopify.com/html_fastfood/cart.html" method="post">
                                                        <div class="effect-ajax-cart">
                                                            <input type="hidden" name="quantity" value="1">
                                                            <button class="_btn add-to-cart" data-parent=".parent-fly" type="submit" name="add" title="Añadir al Carro">Añadir al Carro</button>
                                                        </div>
                                                    </form>
                                                </div>
                                                <!--inner-mask-->
                                            </div>
                                            <!--Group mask-->
                                        </div>
                                        <div class="product-label">
                                            <div class="label-element new-label">
                                                <span>Nuevo</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row-right animMix">
                                        <div class="rating-star">
                                                            <span class="spr-badge" data-rating="0.0"><span class="spr-starrating spr-badge-starrating"><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i></span><span class="spr-badge-caption">No reviews</span>
                                                            </span>
                                        </div>
                                        <div class="product-title"><a class="title-5" href="product.html">FANTA LATA 350 CC/a></div>
                                        <div class="product-price">
                                                            <span class="price">
                                                                <span class="money" data-currency-usd="$20.00">$1000.00</span>
                                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="content_product col-sm-3 not-animated" data-animate="fadeInUp" data-delay="300">
                                <div class="row-container product list-unstyled clearfix product-circle">
                                    <div class="row-left">
                                        <a href="product.html" class="hoverBorder container_item">
                                            <div class="hoverBorderWrapper">
                                                <img src="cliente/images/3-Sandwich-de-jam¢n-crudo-Deltoro-baja.jpg" class="img-responsive front" alt="Juice Ice Tea">
                                                <div class="mask"></div>
                                            </div>
                                        </a>
                                        <div class="hover-mask">
                                            <div class="group-mask">
                                                <div class="inner-mask">
                                                    <div class="group-actionbutton">
                                                        <ul class="quickview-wishlist-wrapper">
                                                            <li class="wishlist">
                                                                <a title="Añadir a Lista de Deseos" class="wishlist wishlist-juice-ice-tea" data-wishlisthandle="juice-ice-tea"><span class="cs-icon icon-heart"></span></a>
                                                            </li>
                                                            <li class="quickview hidden-xs hidden-sm">
                                                                <div class="product-ajax-cart">
                                                                    <span class="overlay_mask"></span>
                                                                    <div data-handle="juice-ice-tea" data-target="#quick-shop-modal" class="quick_shop" data-toggle="modal">
                                                                        <a class=""><span class="cs-icon icon-eye"></span></a>
                                                                    </div>
                                                                </div>
                                                            </li>
                                                            <li class="compare">
                                                                <a title="Comparar" class="compare compare-juice-ice-tea" data-comparehandle="juice-ice-tea"><span class="cs-icon icon-retweet2"></span></a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <form action="http://demo.designshopify.com/html_fastfood/cart.html" method="post">
                                                        <div class="effect-ajax-cart">
                                                            <input type="hidden" name="quantity" value="1">
                                                            <button class="_btn add-to-cart" data-parent=".parent-fly" type="submit" name="add" title="Añadir al Carro">Añadir al Carro</button>
                                                        </div>
                                                    </form>
                                                </div>
                                                <!--inner-mask-->
                                            </div>
                                            <!--Group mask-->
                                        </div>
                                    </div>
                                    <div class="row-right animMix">
                                        <div class="rating-star">
                                                            <span class="spr-badge" data-rating="0.0"><span class="spr-starrating spr-badge-starrating"><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i></span><span class="spr-badge-caption">No reviews</span>
                                                            </span>
                                        </div>
                                        <div class="product-title"><a class="title-5" href="product.html">JAMON SANDWICH SUPER</a></div>
                                        <div class="product-price">
                                                            <span class="price">
                                                                <span class="money" data-currency-usd="$20.00">$2490.00</span>
                                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="content_product col-sm-3 not-animated" data-animate="fadeInUp" data-delay="400">
                                <div class="row-container product list-unstyled clearfix product-circle">
                                    <div class="row-left">
                                        <a href="product.html" class="hoverBorder container_item">
                                            <div class="hoverBorderWrapper">
                                                <img src="cliente/images/7808709500660-leche-descremada-surlat-benecol-1l-228x228.jpg" class="img-responsive front" alt="Juice Ice Tea">
                                                <div class="mask"></div>
                                            </div>
                                        </a>
                                        <div class="hover-mask">
                                            <div class="group-mask">
                                                <div class="inner-mask">
                                                    <div class="group-actionbutton">
                                                        <ul class="quickview-wishlist-wrapper">
                                                            <li class="wishlist">
                                                                <a title="Añadir a Lista de Deseos" class="wishlist wishlist-juice-ice-tea" data-wishlisthandle="juice-ice-tea"><span class="cs-icon icon-heart"></span></a>
                                                            </li>
                                                            <li class="quickview hidden-xs hidden-sm">
                                                                <div class="product-ajax-cart">
                                                                    <span class="overlay_mask"></span>
                                                                    <div data-handle="juice-ice-tea" data-target="#quick-shop-modal" class="quick_shop" data-toggle="modal">
                                                                        <a class=""><span class="cs-icon icon-eye"></span></a>
                                                                    </div>
                                                                </div>
                                                            </li>
                                                            <li class="compare">
                                                                <a title="Comparar" class="compare compare-juice-ice-tea" data-comparehandle="juice-ice-tea"><span class="cs-icon icon-retweet2"></span></a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <form action="http://demo.designshopify.com/html_fastfood/cart.html" method="post">
                                                        <div class="effect-ajax-cart">
                                                            <input type="hidden" name="quantity" value="1">
                                                            <button class="_btn add-to-cart" data-parent=".parent-fly" type="submit" name="add" title="Añadir al Carro">Añadir al Carro</button>
                                                        </div>
                                                    </form>
                                                </div>
                                                <!--inner-mask-->
                                            </div>
                                            <!--Group mask-->
                                        </div>
                                        <div class="product-label">
                                            <div class="label-element hotting-label">
                                                <span>Popular</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row-right animMix">
                                        <div class="rating-star">
                                                            <span class="spr-badge" data-rating="0.0"><span class="spr-starrating spr-badge-starrating"><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i></span><span class="spr-badge-caption">No reviews</span>
                                                            </span>
                                        </div>
                                        <div class="product-title"><a class="title-5" href="product.html">LECHE SURLAT NATURAL1 LITRO</a></div>
                                        <div class="product-price">
                                                            <span class="price">
                                                                <span class="money" data-currency-usd="$20.00">$640.00</span>
                                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
<div id="shopify-section-1509012405077" class="shopify-section index-section index-section-banner">
    <div data-section-id="1509012405077" data-section-type="banner-section">
        <section class="home-banner-layout not-animated" data-animate="zoomIn" data-delay="200">
            <div class="container">
                <div class="row">
                    <div class="home-banner-inner">
                        <div class="home-banner-content">
                            <a class="banner-image" href="contact.html">
                                <img src="cliente/images/tequeños-resize.jpg" alt="" title="">
                            </a>
                            <a class="banner-caption" href="contact.html">
                                                <span class="banner-caption-group">
                                                    <span class="title">¡Gratis!</span>
                                                    <span class="caption">Martes de Delivery</span>
                                                </span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
<div id="shopify-section-1509012440185" class="shopify-section index-section index-section-gallery">
    <div data-section-id="1509012440185" data-section-type="banner-section">
        <section class="home-gallery-layout">
            <div class="container">
                <div class="row">
                    <h2 class="page-title">Galería</h2>
                    <div class="home-gallery-inner">
                        <div class="home-gallery-content">
                            <div class="gallery-item col-sm-4 not-animated" data-animate="fadeInUp" data-delay="100">
                                <a class="home-gallery-lookbook" rel="lookbook" href="assets/images/gallery-1-big.jpg">
                                    <img src="cliente/images/gallery-1.jpg" alt="" title="">
                                </a>
                            </div>
                            <div class="gallery-item col-sm-4 not-animated" data-animate="fadeInUp" data-delay="200">
                                <a class="home-gallery-lookbook" rel="lookbook" href="assets/images/gallery-2-big.jpg">
                                    <img src="cliente/images/gallery-2.jpg" alt="" title="">
                                </a>
                            </div>
                            <div class="gallery-item col-sm-4 not-animated" data-animate="fadeInUp" data-delay="300">
                                <a class="home-gallery-lookbook" rel="lookbook" href="assets/images/gallery-3-big.jpg">
                                    <img src="cliente/images/gallery-3.jpg" alt="" title="">
                                </a>
                            </div>
                            <div class="gallery-item col-sm-4 not-animated" data-animate="fadeInUp" data-delay="400">
                                <a class="home-gallery-lookbook" rel="lookbook" href="assets/images/gallery-4-big.jpg">
                                    <img src="cliente/images/gallery-4.jpg" alt="" title="">
                                </a>
                            </div>
                            <div class="gallery-item col-sm-4 not-animated" data-animate="fadeInUp" data-delay="500">
                                <a class="home-gallery-lookbook" rel="lookbook" href="assets/images/gallery-5-big.jpg">
                                    <img src="cliente/images/gallery-5.jpg" alt="" title="">
                                </a>
                            </div>
                            <div class="gallery-item col-sm-4 not-animated" data-animate="fadeInUp" data-delay="600">
                                <a class="home-gallery-lookbook" rel="lookbook" href="assets/images/gallery-6-big.jpg">
                                    <img src="cliente/images/gallery-6.jpg" alt="" title="">
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
<div id="shopify-section-1509012518613" class="shopify-section  index-section index-section-banner">
    <div data-section-id="1509012518613" data-section-type="banner-section">
        <section class="home-banner-layout not-animated" data-animate="zoomIn" data-delay="200">
            <div class="container">
                <div class="row">
                    <div class="home-banner-inner">
                        <div class="home-banner-content">
                            <a class="banner-image" href="super-deal.html">
                                <img src="cliente/images/bn1_53149ef0-f92a-4804-b600-02e903aa4559.png" alt="" title="">
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>