<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="canonical" href="http://demo.designshopify.com/" />
    <meta name="theme-color" content="#7796A8">
    <meta name="description" content="" />
    <title>
        Fritomanía
    </title>

    <link href="https://fonts.googleapis.com/css?family=Josefin+Sans:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Playball:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Bitter:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css" rel="stylesheet" type="text/css" media="all">
    <link href="{{asset('cliente/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css" media="all">
    <link href="{{asset('cliente/stylesheets/fonts.googleapis.css')}}" rel="stylesheet" type="text/css" media="all">
    <link href="https://cdn.linearicons.com/free/1.0.0/icon-font.min.css" rel="stylesheet" type="text/css" media="all">
    <link href="https://cdn.shopify.com/s/files/1/2487/3424/t/3/assets/social-buttons.scss.css" rel="stylesheet" type="text/css" media="all">
    <link href="{{asset('cliente/stylesheets/cs.styles.css')}}" rel="stylesheet" type="text/css" media="all">
    <link href="{{asset('cliente/stylesheets/font-icon.css')}}" rel="stylesheet" type="text/css" media="all">
    <link href="https://cdn.shopify.com/s/files/1/2487/3424/t/3/assets/owl.carousel.css" rel="stylesheet" type="text/css" media="all">
    <link href="https://cdn.shopify.com/s/files/1/2487/3424/t/3/assets/cs.animate.css" rel="stylesheet" type="text/css" media="all">
    <link href="https://cdn.shopify.com/s/files/1/2487/3424/t/3/assets/slideshow-fade.css" rel="stylesheet" type="text/css" media="all">
    <link href="https://cdn.shopify.com/s/files/1/2487/3424/t/3/assets/animations.css" rel="stylesheet" type="text/css" media="all">
    <link href="https://cdn.shopify.com/s/files/1/2487/3424/t/3/assets/themepunch.revolution.css" rel="stylesheet" type="text/css" media="all">
    <link href="https://cdn.shopify.com/s/files/1/2487/3424/t/3/assets/jquery.fancybox.scss.css" rel="stylesheet" type="text/css" media="all">
    <link rel="icon" href="{{asset('cliente/images/logo-fritomania-400.png')}}">

    <script type="text/javascript" src="{{asset('cliente/javascripts/jquery.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('cliente/javascripts/classie.js')}}"></script>
    <script type="text/javascript" src="{{asset('cliente/javascripts/application-appear.js')}}"></script>
    <script type="text/javascript" src="{{asset('cliente/javascripts/jquery.themepunch.plugins.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('cliente/javascripts/jquery.themepunch.revolution.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('cliente/javascripts/cs.script.js')}}"></script>
    <script type="text/javascript" src="{{asset('cliente/javascripts/jquery.currencies.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('cliente/javascripts/jquery.zoom.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('cliente/javascripts/linkOptionSelectors.js')}}"></script>
    <script type="text/javascript" src="{{asset('cliente/javascripts/owl.carousel.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('cliente/javascripts/scripts.js')}}"></script>
    <script type="text/javascript" src="{{asset('cliente/javascripts/social-buttons.js')}}"></script>
    <script type="text/javascript" src="{{asset('cliente/javascripts/bootstrap.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('cliente/javascripts/jquery.touchSwipe.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('cliente/javascripts/jquery.fancybox.js')}}"></script>

    <!--App css-->
    @stack('css')
    <style>
        header section.main-header .nav-top{
            padding-top: 5px;
            padding-bottom: 5px;
        }

        #oculta {
            display: none;
        }

        #oculta2 {
            display: none;
        }

        .masarriba {
            padding-top: 10px;
            padding-bottom: 10px;
            text-align: center;
            background-color: #fff;
        }

        #pideahora{
            padding: 20px 0px;
            border-radius: 6px;
            font-size: 20px;
            background-color: #9d1414 !important;
            color: #fff;
            width: 60%;
            font-weight: bold;
            margin-bottom: 10px;
        }

        .botones {
            padding: 20px 20px;
            border-radius: 6px;
            font-size: 16px !important;
            background-color: #9d1414 !important;
            color: #fff;
            width: 60%;
            font-weight: bold;
            text-align: center;
        }

        .m_cart-group .items-cart .num-items-in-cart *{
            color: #0A0A0A;
            font-size: 25px;

        }

        .header .m_login-account {
            display: none;
        }

        .omnibar-content {
            background: #F1F1F1;
        }

        .omnibar-nav-wrap {
            height: 100px;
            line-height: 100px;}

        .container {
            padding-right: 15px;
            padding-left: 15px;
            margin-right: auto;
            margin-left: auto;
        }

        #datos-cliente {
            color: #0A0A0A;
            margin-left: 20px;
        }

        #datos-cliente tr td {
            text-align: left;
            margin: 0;
            padding: 0;
            font-family: Josefin Sans !important;
            font-weight: normal;
        }

        .tipopedido {
            font-size: 16px;
            margin-top: 20px;
        }

        body:not(.index-template) main section.order-layout {
            margin-top: 20px;
        }

        body:not(.index-template) main section.order-layout .order-content .order-address .address-items .address-content {
            padding: 13px 15px;
        }

        body:not(.index-template) main section.order-layout .order-content .order-info table tbody tr:first-child td {
            padding-top: 20px;
        }

        body:not(.index-template) main section.order-layout .order-content .order-info table tbody tr:last-child td {
            padding-bottom: 20px;
        }

        td.total {
            font-weight: bold;
        }

        body:not(.index-template) main section.order-layout .order-content .order-info table tfoot td{
            font-weight: bold;
            text-transform: uppercase;
        }

        th.center {
            width: 20px;
        }

        th.total {
            width: 20px;
        }

        .order-info-mobile {
            display: none;
        }

        .table-mobile {
            background-color: #9d1414 !important;
            color: #fff;
            padding: 2%;
        }

        .product .product-title a {
            color: #444;
            font-size: 16px;
            font-family: Josefin Sans;
        }

        body:not(.index-template) .title-content {
            color: #fff;
            text-align: center;
            padding-top: 50px;
            padding-bottom: 50px;
        }

        .tipopedido {
            font-size: 26px;
        }

        textarea #direccion .form-control{
            text-transform: uppercase;
        }




        .close-butto-modal {
            font-size: 32px;
        }

        .btn-add-pieza {
            background-color: #9d1414 !important;
            color: #fff;
        }

        .product .product-price .price, .product .product-price .price_sale {
            font-family: Josefin Sans !important;
        }

        .m_cart-group .items-cart  {
            font-family: Josefin Sans !important;
        }

        #pideahora {
            font-family: Josefin Sans !important;
        }

        .modal-footer .btn+.btn {
            font-family: Josefin Sans !important;
        }

        header section.main-header .nav-logo {
            top: 5px;
        }

        body .main-header .nav-logo img {
            width: 60%;

        }

        header section.main-header .nav-top {
            padding-top: 20px;
            padding-bottom: 20px;
        }

        .product:not(._pro_banner) .effect-ajax-cart ._btn {
            background-color: #9d1414;
        }

        footer section.footer-information-block .footer-information-inner {
            margin-top: 20px;
        }

        .observationMessage {
            margin-top: 50px;
        }

        .home-slideshow-layout {
            display: none;
        }

        .page-404-layout {
            margin-top: -200px;
        }

        .google-maps-content {


        }

        body #newsletter-popup .nl-wraper-popup-inner .popup-left .caption {
            display: none;
        }

        .home-slideshow-layout {
            display:block; position:relative;
        }









        @media only screen and (max-width: 600px) {
            .input-group .form-control, .input-group-addon, .input-group-btn {
                display: block;
            }
            .order-info {
                display: none;
            }

            .order-info-mobile {
                display: block;
            }

            .tipopedido {
                font-size: 26px;
            }

            .m_cart-group .items-cart .num-items-in-cart {
                display: none;
            }

            #pideahora {
                padding: 10px 0px;

            }

            header section.main-header .navMobile-navigation .navMobile-logo {
                bottom: 70px;
                max-width: 60px;
            }

            body input[type="search"], body input[type="number"], body input[type="email"], body input[type="password"] {
                width: 100%;
            }

            .carousel-inner>.item>a>img, .carousel-inner>.item>img, .img-responsive, .thumbnail a>img, .thumbnail>img {
                height: 80px;
            }

            footer section.footer-information-block {
                text-align: center;
            }

            .logo-footer {
                margin-left: 35%;
            }

            .about-contact {
                margin-left: 25%;

            }

            footer section.footer-information-block .footer-information-content .social-payment-item .social-content {
                margin-left: 80px;
            }

            .home-slideshow-layout {
                display: none;
            }

            .home-banslider-inner {
                display: block;
            }

            .page-404-layout {
                margin-top: -100px;
            }

            .google-maps-content {
                display: none;
            }










        }
    </style>
</head>
