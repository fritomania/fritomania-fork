<!-- Modal QuickView -->
<div id="quick-shop-modal" class="modal quick-shop" role="dialog" aria-hidden="true" tabindex="-1" data-width="800" style="display: none;">
    <div class="modal-dialog fadeIn animated">
        <div class="modal-content">
            <div class="modal-header">
                <i class="close lnr lnr-cross btooltip" data-toggle="tooltip" data-placement="top" title="" data-dismiss="modal" aria-hidden="true" data-original-title="Close"></i>
            </div>
            <div class="modal-body">
                <div class="quick-shop-modal-bg" style="display: none;"></div>
                <div class="clearfix">
                    <div class="col-md-6 product-image">
                        <div id="quick-shop-image" class="product-image-wrapper">
                            <div id="featuted-image" class="main-image featured">
                                <img src="cliente/images/8.jpeg" alt="image product">
                            </div>
                            <div id="gallery_main_qs" class="product-image-thumb gallery-images-layout">
                                <div class="gallery-images-inner">
                                    <div class="show-image-load show-load-quick" style="display: none;">
                                        <div class="show-image-load-inner"><i class="fa fa-spinner fa-pulse fa-2x"></i></div>
                                    </div>
                                    <div class="qs-vertical-slider vertical-image-content">
                                        <div class="image-vertical image-thumb active">
                                            <a class="cloud-zoom-gallery" href="cliente/images/8.jpeg" data-image="./assets/images/8.jpeg" data-zoom-image="cliente/images/8.jpeg">
                                                <img src="cliente/images/8.jpeg" alt="image product">
                                            </a>
                                        </div>
                                        <div class="image-vertical image-thumb">
                                            <a class="cloud-zoom-gallery" href="cliente/images/fanta-lata-350-cc.jpg" data-image="./assets/images/fanta-lata-350-cc.jpg" data-zoom-image="cliente/images/fanta-lata-350-cc.jpg">
                                                <img src="cliente/images/fanta-lata-350-cc.jpg" alt="image product">
                                            </a>
                                        </div>
                                        <div class="image-vertical image-thumb">
                                            <a class="cloud-zoom-gallery" href="cliente/images/product_12.jpg" data-image="./assets/images/product_12.jpg" data-zoom-image="cliente/images/product_12.jpg">
                                                <img src="cliente/images/product_12.jpg" alt="image product">
                                            </a>
                                        </div>
                                        <div class="image-vertical image-thumb">
                                            <a class="cloud-zoom-gallery" href="cliente/images/product_13.jpg" data-image="./assets/images/product_13.jpg" data-zoom-image="cliente/images/product_13.jpg">
                                                <img src="cliente/images/product_13.jpg" alt="image product">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 product-information">
                        <div id="quick-shop-container">
                            <h3 id="quick-shop-title"><a href="product.html">Extra Crispy</a></h3>
                            <div class="rating-star"><span class="shopify-product-reviews-badge" data-id="6537875078"></span></div>
                            <div class="quick-shop-management">
                                <span class="management-title">Availability:</span>
                                <div class="management-description">In-Stock</div>
                            </div>
                            <div class="description">
                                <div id="quick-shop-description" class="text-left">
                                    <p>Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulp...</p>
                                </div>
                            </div>
                            <form action="http://demo.designshopify.com/html_fastfood/cart.html" method="post" class="variants" id="quick-shop-product-actions" enctype="multipart/form-data">
                                <div id="quick-shop-variants-container" class="variants-wrapper">
                                    <div class="selector-wrapper variant-wrapper-size">
                                        <label for="quick-shop-variants-213223800871-option-0">Size</label>
                                        <select class="single-option-selector" data-option="option1" id="quick-shop-variants-213223800871-option-0"><option value="Small">Small</option><option value="Medium">Medium</option><option value="Large">Large</option></select>
                                    </div>
                                    <div class="selector-wrapper variant-wrapper-topping">
                                        <label for="quick-shop-variants-213223800871-option-2">Topping</label>
                                        <select class="single-option-selector" data-option="option3" id="quick-shop-variants-213223800871-option-2"><option value="Black Bottom Cupcakes">Black Bottom Cupcakes</option><option value="Blue-Ribbon Butter Cake">Blue-Ribbon Butter Cake</option><option value="Cheesy Mcplain">Cheesy Mcplain</option><option value="Chunky Apple Cake">Chunky Apple Cake</option></select>
                                    </div>
                                </div>
                                <div class="swatch" id="show_swatch">
                                    <div id="show_swatch_detail_1" class="swatch_quick color clearfix" data-option-index="1">
                                        <label>Color</label>
                                        <div id="element-color-Black" data-value="Black" class="swatch-element color Black available active">
                                            <div class="tooltip">Black</div>
                                            <input id="swatch-quick-1-Black" type="radio" name="option-1" value="Black">
                                            <label id="label-color-Black" for="swatch-quick-1-Black" style="background-color: Black; border-color: Black; background-image: url(assets/images/black.png)"><img class="crossed-out" src="cliente/images/soldout.png" alt="image product"></label>
                                        </div>
                                        <div id="element-color-Red" data-value="Red" class="swatch-element color Red available ">
                                            <div class="tooltip">Red</div>
                                            <input id="swatch-quick-1-Red" type="radio" name="option-1" value="Red">
                                            <label id="label-color-Red" for="swatch-quick-1-Red" style="background-color: Red; border-color: Red; background-image: url(assets/images/red.png)"><img class="crossed-out" src="cliente/images/soldout.png" alt="image product"></label>
                                        </div>
                                        <div id="element-color-Blue" data-value="Blue" class="swatch-element color Blue available ">
                                            <div class="tooltip">Blue</div>
                                            <input id="swatch-quick-1-Blue" type="radio" name="option-1" value="Blue">
                                            <label id="label-color-Blue" for="swatch-quick-1-Blue" style="background-color: Blue; border-color: Blue; background-image: url(assets/images/blue.png)"><img class="crossed-out" src="cliente/images/soldout.png" alt="image product"></label>
                                        </div>
                                        <div id="element-color-yellow" data-value="yellow" class="swatch-element color yellow available ">
                                            <div class="tooltip">yellow</div>
                                            <input id="swatch-quick-1-yellow" type="radio" name="option-1" value="yellow">
                                            <label id="label-color-yellow" for="swatch-quick-1-yellow" style="background-color: yellow; border-color: yellow; background-image: url(assets/images/yellow.png)"><img class="crossed-out" src="cliente/images/soldout.png" alt="image product"></label>
                                        </div>
                                    </div>
                                </div>
                                <div id="quick-shop-price-container" class="product-price">
                                    <span class="price"><span class="money" data-currency-usd="$10.00" data-currency="USD">$10.00</span></span>
                                </div>
                                <div class="others-bottom">
                                    <div class="purchase-section">
                                        <div class="quantity-wrapper clearfix">
                                            <div class="wrapper">
                                                <input id="quantity" type="text" name="quantity" value="1" maxlength="5" size="5" class="item-quantity">
                                                <div class="qty-btn-vertical">
                                                    <span class="qty-down fa fa-chevron-down" title="Decrease" data-src="#quantity"></span>
                                                    <span class="qty-up fa fa-chevron-up" title="Increase" data-src="#quantity"></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="purchase">
                                            <button id="quick-shop-add" onclick="change_qs_quantity('-qs');" class="_btn add-to-cart" type="submit" name="add" style="opacity: 1;"><span class="cs-icon icon-cart"></span>Añadir al Carro</button>
                                        </div>
                                    </div>
                                    <div class="comWish-content">
                                        <a title="Comparar" class="_compareWishlist compare compare-extra-crispy-1" data-comparehandle="extra-crispy-1">
                                            <span class="icon-small icon-small-retweet"></span>
                                            <span class="_compareWishlist-text">Compare</span>
                                        </a>
                                        <a title="Añadir a Lista de Deseos" class="wishlist wishlist-extra-crispy-1" data-wishlisthandle="extra-crispy-1">
                                            <span class="icon-small icon-small-heart"></span>
                                            <span class="_compareWishlist-text">Wishlist</span>
                                        </a>
                                        <a title="Send email" class="send-email">
                                            <span class="icon-small icon-small-email"></span>
                                            <span class="_compareWishlist-text">Send email</span>
                                        </a>
                                    </div>
                                </div>
                            </form>
                            <div class="supports-fontface">
                                <span class="social-title">Share this</span>
                                <div class="quick-shop-social">
                                    <a target="_blank" href="#" class="share-facebook">
                                        <span class="fa fa-facebook"></span>
                                    </a>
                                    <a target="_blank" href="#" class="share-twitter">
                                        <span class="fa fa-twitter"></span>
                                    </a>
                                    <a target="_blank" href="#" class="share-pinterest">
                                        <span class="fa fa-pinterest"></span>
                                    </a>
                                    <a target="_blank" href="#" class="share-google">
                                        <!-- Cannot get Google+ share count with JS yet -->
                                        <span class="fa fa-google-plus"></span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal WishList -->
<div class="wishlist-model">
    <div class="modal fade" id="modalwishlist1" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
        <div class="modal-dialog white-modal">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span class="cs-icon icon-close"></span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="modal-note wishlist-note">
                        Added to <a href="wish-list.html">Wishlist</a>
                    </div>
                    <div class="modal-body-inner">
                        <div class="modal-left wishlist-left">
                            <div class="modal-product wishlist-product">
                                <div class="product-left">
                                    <div class="wishlist-image modal-image wishlist-image-213223800871">
                                        <img src="cliente/images/8.jpeg" alt="Extra Crispy">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-right wishlist-right">
                            <div class="wishlist-cart">
                                <form action="http://demo.designshopify.com/html_fastfood/cart.html" method="post" class="variants-form variants"  enctype="multipart/form-data">
                                    <div class="form-left">
                                        <div class="product-right">
                                            <div class="name wishlist-name"><a href="product.html">Extra Crispy</a></div>
                                            <div class="wishlist-price wishlist-price-213223800871"><span class="price"><span class="money" data-currency-usd="$10.00" data-currency="USD">$10.00</span></span>
                                            </div>
                                        </div>
                                        <div class="quantity-content">
                                            <label>QTY</label>
                                            <input type="text" size="5" class="item-quantity item-quantity-qs" name="quantity" value="1">
                                        </div>
                                    </div>
                                    <div class="form-right">
                                        <div class="others-bottom">
                                            <a class="_btn btn-quick-shop" href="wish-list.html">View Wishlist</a>
                                            <a href="cart.html" class="_btn add-to-cart">Añadir al Carro</a>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal Compare -->
<div class="wishlist-model">
    <div class="modal fade" id="modalCompare" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
        <div class="modal-dialog white-modal">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span class="cs-icon icon-close"></span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="modal-note wishlist-note">
                        Added to <a href="compare.html">Compare</a>
                    </div>
                    <div class="modal-body-inner">
                        <div class="modal-left wishlist-left">
                            <div class="modal-product wishlist-product">
                                <div class="product-left">
                                    <div class="wishlist-image modal-image wishlist-image-213223800871">
                                        <img src="cliente/images/8.jpeg" alt="Extra Crispy">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-right wishlist-right">
                            <div class="wishlist-cart">
                                <form action="http://demo.designshopify.com/html_fastfood/cart.html" method="post" class="variants-form variants" enctype="multipart/form-data">
                                    <div class="form-left">
                                        <div class="product-right">
                                            <div class="name wishlist-name"><a href="product.html">Extra Crispy</a></div>
                                            <div class="wishlist-price wishlist-price-213223800871"><span class="price"><span class="money" data-currency-usd="$10.00" data-currency="USD">$10.00</span></span>
                                            </div>
                                        </div>
                                        <div class="quantity-content">
                                            <label>QTY</label>
                                            <input type="text" size="5" class="item-quantity item-quantity-qs" name="quantity" value="1">
                                        </div>
                                    </div>
                                    <div class="form-right">
                                        <div class="others-bottom">
                                            <a class="_btn btn-quick-shop" href="wish-list.html">View Compare</a>
                                            <a href="cart.html" class="_btn add-to-cart">Añadir al Carro</a>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal Newsletter
<div id="newsletter-popup" class="modal fade in" style="display: none; padding-right: 15px;" tabindex="-1" role="dialog" aria-hidden="false">
    <div class="nl-wraper-popup bounceInDown animated">
        <div class="nl-wraper-popup-inner">
            <div class="popup-left">
                <h4>Newsletter!</h4>
                <p class="sale">Inscríbete</p>
                <p class="caption">YA</p>
                <form action="https://codespot.us5.list-manage.com/subscribe/post?u=ed73bc2d2f8ae97778246702e&amp;id=c63b4d644d" method="post" name="mc-embedded-subscribe-form" target="_blank">
                    <div class="group_input">
                        <input class="form-control" type="email" name="EMAIL" placeholder="Ingresa tu correo">
                        <button class="_btn" type="submit">Registrarse</button>
                    </div>
                </form>
            </div>
            <div class="popup-right">
                <img src="cliente/images/fondo-banner.png" alt="">
            </div>
        </div>
        <div class="nl-popup-close" onclick="$('#newsletter-popup').modal('hide')">
            <span data-dismiss="modal"></span>
        </div>
    </div>
</div>-->

<!-- Modal Search-->
<div class="modal fade" id="lightbox-search" tabindex="-1" role="dialog" aria-labelledby="lightbox-search" aria-hidden="true" style="display: none;">
    <div class="modal-dialog animated" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span class="cs-icon icon-close"></span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Search something</h4>
            </div>
            <div class="modal-body">
                <div id="search-info">
                    <form class="search" action="http://demo.designshopify.com/html_fastfood/search.html" style="position: relative;">
                        <input type="hidden" name="type" value="product">
                        <input type="text" name="q" class="search_box" placeholder="Buscar en nuestro restaurant" value="" autocomplete="off">
                        <span class="search-clear cs-icon icon-close" style="display: none;"></span>
                        <button class="search-submit" type="submit">
                            <span class="cs-icon icon-search"></span>
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>