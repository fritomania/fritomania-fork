<script type="text/javascript" src="assets/javascripts/jquery.min.js"></script>
<script type="text/javascript" src="assets/javascripts/classie.js"></script>
<script type="text/javascript" src="assets/javascripts/application-appear.js"></script>
<script type="text/javascript" src="assets/javascripts/jquery.themepunch.plugins.min.js"></script>
<script type="text/javascript" src="assets/javascripts/jquery.themepunch.revolution.min.js"></script>
<script type="text/javascript" src="assets/javascripts/cs.script.js"></script>
<script type="text/javascript" src="assets/javascripts/jquery.currencies.min.js"></script>
<script type="text/javascript" src="assets/javascripts/jquery.zoom.min.js"></script>
<script type="text/javascript" src="assets/javascripts/linkOptionSelectors.js"></script>
<script type="text/javascript" src="assets/javascripts/owl.carousel.min.js"></script>
<script type="text/javascript" src="assets/javascripts/scripts.js"></script>
<script type="text/javascript" src="assets/javascripts/social-buttons.js"></script>
<script type="text/javascript" src="assets/javascripts/bootstrap.min.js"></script>
<script type="text/javascript" src="assets/javascripts/jquery.touchSwipe.min.js"></script>
<script type="text/javascript" src="assets/javascripts/jquery.fancybox.js"></script>

<!-- Scripts inyectados-->
@stack('scripts')
@yield('scripts')
{{$scripts or ''}}