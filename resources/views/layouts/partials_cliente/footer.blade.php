    <!-- Footer -->
    <div id="contacto"></div>
    <footer class="footer">
        <div id="fritomania-section-theme-footer" class="fritomania-section">
            <section class="footer-information-block clearfix" style="background-color: #000;">
                <div class="container">
                    <div class="row">
                        <div class="footer-information-inner">
                            <div class="footer-information-content">
                                <div class="information-item col-sm-4 not-animated" data-animate="fadeInUp" data-delay="100">
                                    <div class="about-content">
                                        <div class="logo-footer">

                                            <img src="{{asset('cliente/images/logo-fritomania-400.png')}}" alt="" title="Fast Food" />
                                        </div>

                                        <div class="about-contact">
                                            <div class="item">
                                                <span class="cs-icon icon-marker"></span><address>Santa Rosa 83 - Santiago Centro</address>
                                            </div>
                                            <div class="item">
                                                <span class="cs-icon icon-phone"></span><a href="tel:+56 9 9564 7034">+56 9 9564 7034</a>
                                            </div>
                                            &nbsp;
                                            <div class="item">
                                                <span class="cs-icon icon-marker"></span><address>Sta Zita 9111 - Las Condes</address>
                                            </div>
                                            <div class="item">
                                                <span class="cs-icon icon-phone"></span><a href="tel: +56 9 4598 4076">+56 9 4598 4076</a>
                                            </div>
                                            &nbsp;<div class="item">
                                                <span class="cs-icon icon-marker"></span><address>Dardignac 68 - Recoleta - Chile</address>
                                            </div>
                                            <div class="item">
                                                <span class="cs-icon icon-phone"></span><a href="tel: +56 9 4598 4076">+56 3083 5942</a>
                                            </div>



                                        </div>
                                    </div>
                                </div>
                                <div class="information-item col-sm-4 not-animated" data-animate="fadeInUp" data-delay="100">
                                    <div class="about-content">
                                        <div class="logo-footer">


                                        </div>

                                        <div class="about-contact">

                                            <div class="item" style="margin-top: 80px;">
                                                <span class="cs-icon icon-marker"></span><address>Av. José Pedro Alessandri 821 - Ñuñoa</address>
                                            </div>
                                            <div class="item">
                                                <span class="cs-icon icon-phone"></span><a href="tel: +56 9 3028 8172">+56 9 3028 8172</a>
                                            </div>
                                            &nbsp;
                                            <div class="item">
                                                <span class="cs-icon icon-marker"></span><address>Av. Vicuña Mackenna 6491 - La Florida</address>
                                            </div>
                                            <div class="item">
                                                <span class="cs-icon icon-phone"></span><a href="tel: +56 9 8624 5705">+56 9 8624 5705</a>
                                            </div>
                                            &nbsp;


                                        </div>
                                    </div>
                                </div>




                                <div class="social-payment-item col-sm-4 not-animated" data-animate="fadeInUp" data-delay="300">
                                    <h5 class="footer-title"> Síguenos</h5>
                                    <div class="social-content">
                                        <div class="social-caption">
                                            <a href="https://www.facebook.com/La-Fritomania-de-Casa-Vieja-1891629094437298/" title="Fast Food on Facebook" class="icon-social facebook"><i class="fa fa-facebook"></i></a>


                                            <a href="https://www.instagram.com/lafritomaniadecasa/" title="Fast Food on Instagram" class="icon-social instagram"><i class="fa fa-instagram"></i></a>

                                        </div>
                                    </div>
                                    <div class="payment-content ">
                                        <h5 class="footer-title">Formas de Pago</h5>
                                        
                                        <div class="payment-caption">
                                            <img src="cliente/images/logo-web-pay-plus-2 blanco.jpg">


                                        </div>
                                    </div>


                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section class="copy-right clearfix">
                <div class="copy-right-wrapper">
                    <div class="copy-right-inner">
                       <!-- <div class="footer_links">
                            <ul>
                                <li><a href="index-2.html" title="Home">Home</a></li>
                                <li><a href="collections.html" title="Pizza">Pizza</a></li>
                                <li><a href="collections.html" title="Hamburger">Hamburger</a></li>
                                <li><a href="collections.html" title="Fast food">Fast food</a></li>
                                <li><a href="collections.html" title="Drinks">Drinks</a></li>
                                <li><a href="collections.html" title="Combo buy">Combo buy</a></li>
                                <li><a href="contact.html" title="Contact">Contact</a></li>
                                <li><a href="wish-list.html" title="Wishlist">Wishlist</a></li>
                                <li><a href="account.html" title="My account">My account</a></li>
                                <li><a href="login.html" title="Login">Login</a></li>
                                </ul>
                        </div>-->
                        <div class="footer_copyright">Desarrollado por  <a href="http://negociovirtual.cl/" title=""> Negocio Virtual</a>. &copy; 2019 Todos los Derechos Reservados </div>
                    </div>
                </div>
            </section>
        </div>
    </footer>