<!-- REQUIRED JS SCRIPTS -->
<script src="{{ url (mix('/js/app.js')) }}" type="text/javascript"></script>

<script>
    function mostrarNottifis() {
        notificaciones = this.notifis;


    }
    function addComas(nStr){
        nStr += '';
        x = nStr.split('.');
        x1 = x[0];
        x2 = x.length > 1 ? '{{ config('app.separador_decimal') }}' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + '{{ config('app.separador_miles') }}' + '$2');
        }
        return x1 + x2;
    }

    /**
     * Formatea los numeros con separador de miles, separador decimales y cantidd de decimales mediante llaves de configuración
     * @param nStr
     * @returns {*}
     */
    function numf(nStr){
        nStr += '';
        x = nStr.split('.');
        x1 = x[0];
        x2 = x.length > 1 ? '{{ config('app.separador_decimal') }}' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + '{{ config('app.separador_miles') }}' + '$2');
        }
        return x1 + x2;
    }

    $('div.alert').not('.alert-important').delay({{config('app.tiempo_oculta_alerta',3000)}}).fadeOut(350);
    $(function(){
        $('[data-toggle="tooltip"]').tooltip();

        {{--// Comprobamos si ya nos habían dado permiso--}}
        {{--if (Notification.permission === "granted") {--}}
            {{--var url = "{{route("api.notificaciones.index")}}";--}}
            {{--var params = {params: {user: '{{Auth::user()->id}}'}};--}}

            {{--axios.get(url, params).then(response => {--}}
                {{--// console.log(response.data.data);--}}
                {{--var notifys = response.data.data;--}}

                {{--$.each(notifys, function (index, notify) {--}}
                    {{--notiFyMe(notify.mensaje, notify.mensaje)--}}
                {{--})--}}
            {{--})--}}
                {{--.catch(error => {--}}
                    {{--// console.log(error.response.data);--}}
                {{--});--}}
        {{--}--}}
        function notiFyMe(titulo, mensaje){
            console.log('notiFyMe', mensaje);
            var opciones = {
                body: mensaje,
                icon: 'https://solucionesaltamirano.com/wp-content/uploads/2018/08/logo-a-no-trans.png',
                lang: 'ES',
            };
            var noti = new Notification(titulo, opciones);

            setTimeout(noti.close.bind(noti), 8000);

            noti.onclick = function(event) {
                event.preventDefault(); // prevent the browser from focusing the Notification's tab
                window.open('http://www.solucionesaltamirano.com', '_blank');
            }
        };
    })
</script>

<!--            Scripts firebase global
------------------------------------------------------------------------>
<script src="https://www.gstatic.com/firebasejs/5.0.4/firebase-app.js"></script>
<script src="https://www.gstatic.com/firebasejs/5.0.4/firebase-firestore.js"></script>
<script>
    var conf = {
        apiKey: "AIzaSyDs1Fx826HnI502dugIE72ONzUz7vuppUs",
        authDomain: "fritomaniafl-2018.firebaseapp.com",
        databaseURL: "https://fritomaniafl-2018.firebaseio.com",
        projectId: "fritomaniafl-2018",
        storageBucket: "",
        messagingSenderId: "545691642884"
    };

    const fbApp = firebase.initializeApp(conf);
    const fireStore = firebase.firestore();
    fireStore.settings({ timestampsInSnapshots: true});

    const fireBase = fbApp.firestore();
</script>

<!--            Scripts para select2-simple
------------------------------------------------------------------------>

<script>
    $(function () {
        $(".select2-simple").select2({
            // placeholder: 'Seleccione uno...',
            language: "es",
            maximumSelectionLength: 1,
            allowClear: true
        });
    })
</script>


<!-- Scripts inyectados-->
@stack('scripts')
@yield('scripts')
{{$scripts or ''}}