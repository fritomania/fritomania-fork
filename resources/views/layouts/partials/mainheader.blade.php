<!-- Navbar -->
<nav class="main-header navbar navbar-expand border-bottom navbar-dark bg-main" id="mainheader" :class="{'flash-main-header' : flashVar }">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
        <li class="nav-item">
            <a class="nav-link" data-widget="pushmenu" href="#"><i class="fa fa-bars"></i></a>
        </li>
        <li class="nav-item d-none d-sm-inline-block">
            <a href="{{route('admin.home')}}" class="nav-link">Home</a>
        </li>
        {{--<li class="nav-item d-none d-sm-inline-block">--}}
        {{--<a href="#" class="nav-link">Contact</a>--}}
        {{--</li>--}}
    </ul>



    <!-- SEARCH FORM -->
{{--<form class="form-inline ml-3">--}}
{{--<div class="input-group input-group-sm">--}}
{{--<input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">--}}
{{--<div class="input-group-append">--}}
{{--<button class="btn btn-navbar" type="submit">--}}
{{--<i class="fa fa-search"></i>--}}
{{--</button>--}}
{{--</div>--}}
{{--</div>--}}
{{--</form>--}}

<!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">

        @if(Auth::user()->isAdmin() || Auth::user()->isSuperAdmin())
            <li class="nav-item dropdown d-none d-sm-inline-block border-right" >
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Bodega: {{ $nombreTienda = \App\Models\Tienda::find(session('tienda'))->nombre }}
                </a>
                <div class="dropdown-menu dropdown-menu-md dropdown-menu-right" data-display="dynamic">
                    @foreach($listaTiendas = \App\Models\Tienda::whereNotIn('id',[session('tienda')])->get() as $lugar)
                        <a class="dropdown-item "style="color: #0A0A0A !important;" href="{{route('lugar.cambiar',$lugar->id)}}">{{$lugar->nombre}}</a>
                    @endforeach
                </div>
            </li>
        @else
            <li class="nav-item d-none d-sm-inline-block border-right">
                <a href="#" class="nav-link">Bodega: {{ Auth::user()->tienda->nombre }}</a>
            </li>
        @endif

        <li class="nav-item dropdown d-block d-sm-none">
            <a class="nav-link" data-toggle="dropdown" href="#">
                <i class="fa fa-user"></i>
            </a>
            <div class="dropdown-menu dropdown-menu-right" data-display="dynamic">
                <span class="dropdown-item-text text-bold text-center">{{ Auth::user()->name }}</span>
                <div class="dropdown-divider"></div>

                @if(Auth::user()->isAdmin() || Auth::user()->isSuperAdmin())
                    <a class="dropdown-item dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="color: #0A0A0A !important;">
                        {{ $nombreTienda }}
                    </a>

                    <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                        @foreach($listaTiendas as $lugar)
                            <a class="dropdown-item "style="color: #0A0A0A !important;" href="{{route('lugar.cambiar',$lugar->id)}}">{{$lugar->nombre}}</a>
                        @endforeach
                    </div>
                @else
                    <span class="dropdown-item-text text-center">{{ Auth::user()->tienda->nombre }}</span>
                @endif
            </div>
        </li>


        <!-- Notifications Dropdown Menu -->
        <li class="nav-item dropdown " style="color: #0A0A0A !important;" >
            <a class="nav-link" data-toggle="dropdown" href="#">
                <i class="fa fa-bell"></i>
                @if($cntNots = Auth::user()->notificaciones()->noVistas()->count())
                    <span class="badge badge-warning navbar-badge">{{$cntNots}}</span>
                @endif
            </a>
            <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right text-dark">
                <span class="dropdown-item dropdown-header text-dark">
                    @if($cntNots == 0  || $cntNots > 1)
                        {{ $cntNots }} Notificaciones
                    @else
                        1 Notificación
                    @endif
                </span>

                @if($cntNots > 0)

                    @foreach(\App\Models\NotificacionTipo::all() as $tipo)

                        @php
                            $nots = Auth::user()->notificaciones()->noVistas()->deTipo($tipo->id)->get();
                        @endphp

                        @if($nots->count()>0)
                            <div class="dropdown-divider"></div>
                            <a href="{{route('notificaciones.ver',$tipo->id)}}" class="dropdown-item " style="color: #0A0A0A !important;">
                                <i class="fa fa-{{$tipo->icono}} mr-2 "></i> {{$nots->count()}}  {{$tipo->nombre}}
                                <span class="float-right text-muted text-sm">
                                    {{Auth::user()->tiempoUltimaNotificacion($tipo->id)}}
                                </span>
                            </a>
                        @endif
                    @endforeach
                @endif
                <div class="dropdown-divider"></div>

                {{--<a href="#" class="dropdown-item dropdown-footer">See All Notifications</a>--}}
            </div>
        </li>

        <li class="nav-item">
            <a href="#"class="nav-link" data-toggle="modal" data-target="#modalLogout">
                <i class="fa fa-sign-out-alt"></i>
                <span class="d-none d-sm-inline">Salir</span>
            </a>
        </li>
        {{--<li class="nav-item">--}}
        {{--<a class="nav-link" data-widget="control-sidebar" data-slide="true" href="#"><i--}}
        {{--class="fa fa-th-large"></i></a>--}}
        {{--</li>--}}
    </ul>
</nav>
<!-- /.navbar -->

@include('layouts.partials.modal_loguot')


@push('scripts')
<script>

    vueMainHeader = new Vue({
        el: '#mainheader',
        mounted() {

            @if(app()->environment()=='production')
                console.log('## vueMainHeader ## Mounted!');

                this.getOrdenes();
                fireBase.collection("cambios")
                    .doc("tienda" + this.tienda )
                    .onSnapshot(doc => {
                        if(doc.data()){
                            this.cambios = doc.data().venta;
                        }
                    });

            @endif

        },
        created: function () {
        },
        data: {
            ordenes: [],
            api : '{{route('ordenes.datos')}}',
            lugar: '',
            tienda: '{{session('tienda')}}',
            cajero: '{{ Auth::user()->isCaja() ? 1 : 0}}',
            cambios: {
                id: '0',
                lugar: '0',
                estado: '0',
                delivery: '0',
                inicio: '1'
            },
        },
        methods: {
            getOrdenes(){
                console.log('## vueMainHeader ## getOrdenes()');
                var params= { params: {lugar: this.lugar} };

                axios.get(this.api,params).then(response => {
    //                    console.log(response.data);

                    this.ordenes = response.data.data;

                })
                .catch(error => {
                    console.log(error);
                });
            },
            transcurrido(orden){
                var currentdate = new Date();
                var fecha = new Date(orden.created_at);
                var dif = (currentdate.getTime()-fecha.getTime())/(1000*60);

                //console.log('orden',orden.id,'transcurrido',dif.toFixed(0));
                return dif;

            },
            transcurridoCocina(orden){
                //si la orden esta en estado enviada a cocina o cocinando
                if(orden.estado.id==3 || orden.estado.id==4){
                    var currentdate = new Date();
                    var fecha = new Date(orden.tiempos[3]);
                    var dif = (currentdate.getTime()-fecha.getTime())/(1000*60);

                    dif = isNaN(dif) ? null : dif.toFixed(0);

                    //console.log('orden',orden.id,'transcurrido cocina',dif,'Fecha Orden: ',fecha,'Fecha Actual: ',currentdate);
                    return dif;
                }

                return null;

            }
        },
        computed: {
            nuevas (){
                var nuevas = this.ordenes.filter(orden => {
                    //console.log('transcurrido para orden',orden.id,this.transcurrido(orden));
                    return (this.transcurrido(orden)<=2 && orden.estado.id==1);
                });

                return nuevas;
            },
            atrasadas(){

                var atrasadas = this.ordenes.filter(orden => {
//                    console.log('transcurrido een cocina orden:',orden.id,this.transcurridoCocina(orden));
                    //si lo transcurrido en cocina es mayor o igual al la suma de los tiempos de despacho de los articulos de la orden
                    // y el estado es igual a enviado a cocina (3)
                    return (this.transcurridoCocina(orden)>=orden.tiempo_despacho && orden.estado.id==3);
                });

                return atrasadas;
            },
            flashVar(){
                //si hay nuevas o atrasadas en cocina
                return (this.nuevas.length > 0 || this.atrasadas.length);
            }
        },
        watch: {
            cambios: function (newVal,oldVal) {

                console.log('## vueMainHeader ## Cambios!!');
                this.getOrdenes();

            }
        },
    });
</script>
@endpush