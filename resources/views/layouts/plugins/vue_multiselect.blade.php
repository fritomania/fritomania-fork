@push('css')
<link rel="stylesheet" href="{{asset('css/vue-multiselect.min.css')}}" />
@endpush
@push('scripts')
<script type="text/javascript" src="{{asset('js/vue-multiselect.min.js')}}"></script>

<script>
    Vue.component('multiselect', window.VueMultiselect.default)
</script>
@endpush