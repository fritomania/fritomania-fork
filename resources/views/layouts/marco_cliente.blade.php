<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
@include('layouts.partials_cliente.htmlheader')

<body class="fastfood_1 {{$body_class or ''}}" >
    <div id="root">
        @include('layouts.partials_cliente.mainheader')

        @yield('content')


        @isset($orden)
            <!-- Modal Car -->
            <div class="modal fade" id="madalCambioTipoPedido" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header" >
                            <button type="button" class="close close-butto-modal" data-dismiss="modal" aria-label="Close">
                                <i class="fa fa-times" aria-hidden="true" ></i>
                            </button>
                            <h4 class="modal-title" id="modelTitleId">
                                Cambiar tipo de pedido
                            </h4>
                        </div>

                        <div class="modal-body">
                            <div class="row">
                                <div class="col-sm-6 text-center" >
                                    <a href="{{route('orden.cambiar.tipo.pedido',['tipo' => 2])}}" class="btn btn-danger">
                                        <i class="fa fa-university fa-3x" ></i>
                                        <br>Retiro en Local
                                    </a>

                                </div>
                                <div class="col-sm-6 text-center" >
                                    <a href="{{route('orden.cambiar.tipo.pedido',['tipo' => 1])}}" class="btn btn-danger">
                                        <i class="fa fa-motorcycle fa-3x"></i>
                                        <br>Delivery
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endisset
    </div>


    @include('layouts.partials_cliente.footer')


    <script>
        function addaffix(scr) {
            if ($(window).innerWidth() >= 992) {
                if (scr > 153) {
                    if (!$('#top').hasClass('affix')) {
                        $('#top').addClass('affix').addClass('fadeInDown animated');
                    }
                } else {
                    if ($('#top').hasClass('affix')) {
                        $('#top').removeClass('affix').removeClass('fadeInDown animated');
                    }
                }
            } else if ($(window).innerWidth() < 992 && $(window).innerWidth() > 767) {
                if (scr > 95) {
                    if (!$('#top').hasClass('affix')) {
                        $('#top').addClass('affix').addClass('fadeInDown animated');
                    }
                } else {
                    if ($('#top').hasClass('affix')) {
                        $('#top').removeClass('affix').removeClass('fadeInDown animated');
                    }
                }
            } else {
                if (scr > 45) {
                    if (!$('#top').hasClass('affix')) {
                        $('#top').addClass('affix').addClass('');
                    }
                } else {
                    if ($('#top').hasClass('affix')) {
                        $('#top').removeClass('affix').removeClass('');
                    }
                }
            }
        }
        $(window).scroll(function() {
            "use strict";

            var scrollTop = $(this).scrollTop();
            addaffix(scrollTop);
        });
        $(window).resize(function() {
            "use strict";

            var scrollTop = $(this).scrollTop();
            addaffix(scrollTop);
        });

        /**
         * Formatea los numeros con separador de miles, separador decimales y cantidd de decimales mediante llaves de configuración
         * @param nStr
         * @returns {*}
         */
        function numf(nStr){
            nStr += '';
            x = nStr.split('.');
            x1 = x[0];
            x2 = x.length > 1 ? '{{ config('app.separador_decimal') }}' + x[1] : '';
            var rgx = /(\d+)(\d{3})/;
            while (rgx.test(x1)) {
                x1 = x1.replace(rgx, '$1' + '{{ config('app.separador_miles') }}' + '$2');
            }
            return x1 + x2;
        }
    </script>

    <!--            Scripts inyectados
    ------------------------------------------------------------------------>
    @stack('scripts')
</body>
</html>
