<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="UTF-8">
    <title> @yield('htmlheader_title', config('app.name')) </title>
    <link rel="shortcut icon" type="image/x-icon" href="{{asset('cliente/images/logo_nuevo.png')}}">
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link href="{{ mix('/css/app.css') }}" rel="stylesheet" type="text/css" />

    <style>
        body {
            padding-top: 5px;
            background-color: #9D1414 !important;
        }
    </style>

</head>

<body>

<!-- Page Content -->
<div class="container-fluid" id="root">

    <!-- Portfolio Item Row -->
    <div class="row">

        <div class="col-md-8 px-1">
            <div class="embed-responsive embed-responsive-16by9" >
                <iframe class="embed-responsive-item" src="{{config('app.url_videos_youtube')}}" allowfullscreen></iframe>
            </div>
        </div>

        <ordenes-venta-banner api="{{route('ordenes.datos')}}" tienda="{{session('tienda')}}" url_logo="{{asset('cliente/images/logo_nuevo.png')}}"></ordenes-venta-banner>

    </div>
    <!-- /.row -->
    {{--<hr class="m-1">--}}

    <div class="row mt-1">

        <div class="col-md-3 col-sm-6 px-1">
            <a href="#">
                <img class="img-fluid" src="{{asset('cliente/images/00.jpg')}}" alt="" style="max-height: 250px; width: 100%">
            </a>
        </div>

        <div class="col-md-3 col-sm-6 px-1">
            <a href="#">
                <img class="img-fluid" src="{{asset('cliente/images/01.jpg')}}" alt="" style="max-height: 250px; width: 100%">
            </a>
        </div>

        <div class="col-md-3 col-sm-6 px-1">
            <a href="#">
                <img class="img-fluid" src="{{asset('cliente/images/02.jpg')}}" alt="" style="max-height: 250px; width: 100%">
            </a>
        </div>

        <div class="col-md-3 col-sm-6 px-1">
            <a href="#">
                <img class="img-fluid" src="{{asset('cliente/images/mejor_precio.jpg')}}" alt="" style="max-height: 250px; width: 100%">
            </a>
        </div>

    </div>
    <!-- /.row -->

</div>
<!-- /.container -->

{{--<!-- Footer -->--}}
{{--<footer class="py-5 bg-dark">--}}
    {{--<div class="container">--}}
        {{--<p class="m-0 text-center text-white">Copyright &copy; Your Website 2018</p>--}}
    {{--</div>--}}
    {{--<!-- /.container -->--}}
{{--</footer>--}}

<script src="{{ url (mix('/js/app.js')) }}" type="text/javascript"></script>

<script>
    new Vue({
        el: '#root',
        mounted() {
            console.log('Instancia vue montada');
        },
        created() {
            console.log('Instancia vue creada');
        },
        data: {
        },
        methods: {
            getDatos(){
                console.log('Metodo Get Datos');
            }
        }
    });
</script>

</body>

</html>
