@extends('layouts.marco_cliente')

@push('css')
<style>
    .tabla-combo tr td{
        padding: 1px !important;
        text-align: left !important;
    }
    .img-cart{
        height: 100px;
        width: 150px;
        border: solid;
        border-radius: 5px;
        border-width: 1px;
    }
    @media only screen and (max-width: 600px) {
        .img-cart{
            height: 50px;
            width: 75px;
        }
    }
    .modal-header {
        padding:9px 15px;
        border-bottom:1px solid #eee;
        background-color: #9D1414;
        color: #ffffff !important;
        -webkit-border-top-left-radius: 5px;
        -webkit-border-top-right-radius: 5px;
        -moz-border-radius-topleft: 5px;
        -moz-border-radius-topright: 5px;
        border-top-left-radius: 5px;
        border-top-right-radius: 5px;
    }
</style>
@endpush

@include('layouts.plugins.toastr')

@section('content')
    <!-- Main Content -->
    <div class="page-container" id="PageContainer">
        <main class="main-content" id="MainContent" role="main">

            @include('components.web.heading',['titulo' => 'Tu Carro','ruta' => route('carro'),'tienda' => $tienda])


            <section class="collection-layout">
                <div class="collection-wrapper">
                    <div class="container">
                        @include('flash::message')
                    {{Form::open(['route' => 'carro.pagar','method' => 'post'])}}
                    <div class="row">
                        <div class="col-sm-12">
                            <!--            Tabla de articulos
                            ------------------------------------------------------------------------>
                            <table class="table table-condensed table-bordered ">
                                <thead>
                                    <th> </th>
                                    <th> Artículo</th>
                                    <th>Cantidad</th>
                                    <th>Precio</th>
                                    <th>Adicionales</th>
                                    <th>SubTotal</th>
                                    <th>Acción</th>
                                </thead>
                                <tbody v-show="items.length == 0">
                                <tr>
                                    <td colspan="10" class="text-center">
                                        No hay ningún artículo en el carro
                                    </td>
                                </tr>
                                </tbody>
                                <tbody v-for="(item,index) in items">
                                <tr>
                                    <td width="10%" style="padding: 0px">
                                        <img :src="item.imagen" alt="" class="img-cart" >
                                    </td>
                                    <td width="50%" v-text="item.nombre"></td>
                                    <td width="10%" valign="middle">
                                        <input
                                                @focus="$event.target.select()"
                                                type="number"
                                                :name="'cantidades['+item.id+']'"
                                                class="form-control input-sm" value="1" v-model="item.qty"
                                                min="0"
                                                max="50"
                                        >
                                        <br>
                                        {{--Solo se muestra si el item no es un combo o es un combo pero unitario--}}
                                        <span v-if="item.receta || item.tiene_extras || item.tiene_salsas">

                                            <button type="button" class="btn btn-sm btn-danger" @click="modificar(item)" >
                                                <i class="fa fa-edit" aria-hidden="true"></i> Modificar
                                            </button>

                                        </span>
                                    </td>
                                    <td width="10%" class="text-right">
                                        {{dvs()}}<span v-text="nf(item.precio_venta)"></span>
                                    </td>
                                    <td width="10%" class="text-right">
                                        {{dvs()}}<span v-text="nf(item.total_modificaciones)"></span>
                                    </td>
                                    <td width="10%" class="text-right">
                                        {{dvs()}}<span v-text="nf((item.qty * item.precio_venta) + item.total_modificaciones)"></span>
                                    </td>
                                    <td width="20%" class="text-center">

                                        <div class="btn-group">

                                            <button type="button" class="btn btn-danger btn-sm" @click="eliminar(item.id)">
                                                <span v-show="idEliminando==item.id">
                                                    <i class="fa fa-spinner fa-spin"></i>
                                                </span>
                                                <span v-show="!(idEliminando==item.id)">
                                                    <i class="fa fa-remove" aria-hidden="true"></i> quitar
                                                </span>
                                            </button>
                                        </div>

                                    </td>
                                </tr>
                                <tr  v-if="item.combo && item.combo.detalles.length > 1">
                                    <td colspan="10" class="text-left">
                                        <table class="tabla-combo">
                                            <tr >
                                                <td  >
                                                    <div class="row" style="padding-left: 15px">
                                                        <div class="col-sm-12">
                                                            <div class="row">
                                                                <div class="col-sm-12">
                                                                    Arme su combo elija:
                                                                    <span v-show="cantidadPiezas(index)>0" v-text="cantidadPiezas(index)+'/'"></span><span v-text="item.combo.piezas"></span> Piezas y
                                                                    <span v-show="cantidadBebidas(index)>0" v-text="cantidadBebidas(index)+'/'"></span><span v-text="item.combo.bebidas"></span> Bebidas <br>
                                                                </div>
                                                            </div>

                                                            <div class="row">
                                                                <!-- Inputs piezas -->
                                                                <div class="col-sm-6" v-show="item.combo.piezas > cantidadPiezas(index)">
                                                                    <div class="row">
                                                                        <div class="col-sm-7">
                                                                            <div class="input-group ">
                                                                                <span class="input-group-addon">
                                                                                    Pieza
                                                                                </span>
                                                                                <select v-model="piezaAdd[index].item" class="form-control" placeholder="Seleccione una pieza">
                                                                                    <option :value="{}" selected>Seleccione una pieza</option>
                                                                                    <option v-for="det in piezasDisponibles(item.combo.piezas_d,index)" :value="det.item" v-if="det.item.web">
                                                                                        @{{ det.item.nombre }}
                                                                                    </option>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-sm-5">
                                                                            <div class="input-group">
                                                                                <span class="input-group-addon" id="sizing-addon1">
                                                                                    Cantidad
                                                                                </span>
                                                                                <input
                                                                                        type="number"
                                                                                        class="form-control"
                                                                                        v-model="piezaAdd[index].cantidad"
                                                                                        @focus="$event.target.select()"
                                                                                        @keypress="keymonitor"
                                                                                        min="1"
                                                                                >
                                                                                <span class="input-group-btn">
                                                                                    <button type="button" class="btn btn-danger btn-sm" @click="addPieza(index,item.combo.piezas)">
                                                                                        <i class="fa fa-plus" aria-hidden="true"></i>
                                                                                    </button>
                                                                                </span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <!-- Inputs bebidas -->
                                                                <div class="col-sm-6" v-show="item.combo.bebidas > cantidadBebidas(index)" >
                                                                    <div class="row">
                                                                        <div class="col-sm-7">
                                                                            <div class="input-group">
                                                                                <span class="input-group-addon" id="sizing-addon1">
                                                                                    Bebida
                                                                                </span>
                                                                                <select v-model="bebidaAdd[index].item" class="form-control" placeholder="Seleccione una bebida">
                                                                                    <option :value="{}">Seleccione una bebida</option>
                                                                                    <option v-for="det in bebidasDisponibles(item.combo.bebidas_d,index)" :value="det.item" v-if="det.item.web">
                                                                                        @{{ det.item.nombre }}
                                                                                    </option>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-sm-5">
                                                                            <div class="input-group">
                                                                                <span class="input-group-addon" id="sizing-addon1">
                                                                                    Cantidad
                                                                                </span>
                                                                                <input
                                                                                        type="number"
                                                                                        class="form-control"
                                                                                        v-model="bebidaAdd[index].cantidad"
                                                                                        @focus="$event.target.select()"
                                                                                        @keypress="keymonitor"
                                                                                        min="1">
                                                                                <span class="input-group-btn">
                                                                                    <button type="button" class="btn btn-danger btn-sm" @click="addBebida(index,item.combo.bebidas)">
                                                                                        <i class="fa fa-plus" aria-hidden="true"></i>
                                                                                    </button>
                                                                                </span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="row" style="padding-left: 20px; padding-top: 10px">
                                                                <div class="col-sm-12">
                                                                    <!--            Lista de items agregados
                                                                    ------------------------------------------------------------------------>
                                                                    <ul style="margin-left: 20px;">
                                                                        <li v-for="(pieza,indexPieza) in piezasCombo[index]">
                                                                            <span v-text="pieza.cantidad"></span>&nbsp;
                                                                            <span v-text="pieza.item.nombre"></span>&nbsp;
                                                                            <button type="button" class="btn btn-danger btn-xs" @click="removerPieza(index,indexPieza)">
                                                                                <i class="fa fa-remove" aria-hidden="true"></i>
                                                                                {{--Remover--}}
                                                                            </button>
                                                                            <span v-if="pieza.item.receta || pieza.item.tiene_extras || pieza.item.tiene_salsas">

                                                                                <button type="button" class="btn btn-xs btn-danger" @click="modificarPiezaCombo(item,pieza,null)" >
                                                                                    <i class="fa fa-edit" aria-hidden="true"></i> Modificar
                                                                                </button>

                                                                            </span>
                                                                            <input type="hidden" :name="'detalles_combo['+item.id+']['+pieza.item.id+']'" :value="pieza.cantidad">
                                                                        </li>
                                                                        <li v-for="(bebida,indexBebida) in bebidasCombo[index]" >
                                                                            <span v-text="bebida.cantidad"></span>&nbsp;
                                                                            <span v-text="bebida.item.nombre"></span>&nbsp;
                                                                            <button type="button" class="btn btn-danger btn-xs" @click="removerBebida(index,indexBebida)">
                                                                                <i class="fa fa-remove" aria-hidden="true"></i>
                                                                                {{--Remover--}}
                                                                            </button>
                                                                            <input type="hidden" :name="'detalles_combo['+item.id+']['+bebida.item.id+']'" :value="bebida.cantidad">
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>

                                <tr  v-if="item.combo && item.combo.detalles.length == 1">
                                    <td colspan="10" class="text-left">
                                        <table class="tabla-combo">
                                            <tr >
                                                <td  >
                                                    <div class="row" style="padding-left: 15px">
                                                        <div class="col-sm-12">
                                                            <div class="row">
                                                                <div class="col-sm-12">
                                                                    Su combo contien:
                                                                    <ul>
                                                                        <li v-for="(pieza,indexPieza) in item.combo.detalles">
                                                                            <span v-text="item.combo.piezas"></span>&nbsp;
                                                                            <span v-text="pieza.item.nombre"></span>&nbsp;
                                                                            <span v-if="pieza.item.receta || pieza.item.tiene_extras || pieza.item.tiene_salsas">

                                                                                <button type="button" class="btn btn-xs btn-danger" @click="modificarPiezaCombo(item,pieza,item.combo.piezas)" >
                                                                                    <i class="fa fa-edit" aria-hidden="true"></i> Modificar
                                                                                </button>

                                                                            </span>
                                                                            <input type="hidden" :name="'detalles_combo['+item.id+']['+pieza.item.id+']'" :value="pieza.cantidad">
                                                                        </li>
                                                                    </ul>


                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>

                                </tbody>
                                <tfoot>
                                <tr v-show="delivery=='1'">
                                    <td colspan="5" style="font-weight: bold;">
                                        Sub total:
                                    </td>
                                    <td class="text-right" style="font-weight: bold;">
                                        $<span v-text="nf(subTotal)"></span>
                                    </td>
                                    <td></td>
                                </tr>
                                <tr v-show="delivery=='1'">
                                    <td colspan="5" style="font-weight: bold;">
                                        Delivery:
                                    </td>
                                    <td class="text-right" style="font-weight: bold;">
                                        $<span v-text="nf(monto_delivery)"></span>
                                    </td>
                                    <td></td>
                                </tr>
                                <tr v-show="items.length > 0">
                                    <td colspan="5" style="font-weight: bold;">
                                        Total:
                                    </td>
                                    <td class="text-right" style="font-weight: bold;">
                                        $<span v-text="nf(total)"></span>
                                    </td>
                                    <td></td>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-sm-6">
                            {!! Form::label('direccion', 'Dirección:') !!}
                            <textarea id="direccion" name="direccion" class="form-control" placeholder="Escribir aquí..." required>{{$orden->direccion}}</textarea>
                        </div>
                        <div class="form-group col-sm-6">
                            {!! Form::label('observaciones', 'Observaciones:') !!}
                            <textarea id="observaciones" name="observaciones" class="form-control"  placeholder="Escribir aquí...">{{$orden->observaciones}}</textarea>
                        </div>
                    </div>

                        <h3 style="border-bottom: solid; border-color: #9d1414;">FORMAS DE PAGO</h3>

                    <div class="row">
                        <div class="col-sm-6">

                            @if(!config('app.solo_delivery_web'))
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="forma_pago" id="forma_pago1" value="{{\App\Models\TipoPago::WEBPAY}}" checked required>
                                        Paga Online WebPay
                                        <img src="{{asset('img/webpay.png')}}" alt="imgWebPay">
                                    </label>
                                </div>
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="forma_pago" id="forma_pago2" value="{{\App\Models\TipoPago::KHIPU}}" required>
                                        Paga Online KHIPU
                                        <img src="{{asset('img/khipu.jpg')}}" alt="imgWebPay" height="34px" >
                                    </label>
                                </div>
                            @endif

                            <div class="radio {{$orden->retiro_en_local ? 'disabled' : ''}}" style="margin-top: 6px">
                                <label>
                                    <input type="radio" name="forma_pago" id="forma_pago3" value="{{\App\Models\TipoPago::EFECTIVO}}" {{$orden->retiro_en_local ? 'disabled' : ''}} required>
                                    Paga en EFECTIVO al recibir tu pedido
                                    @if($orden->retiro_en_local)
                                        <br><small class="text-muted text-danger">no disponible en modo retiro en local</small>
                                    @endif
                                </label>
                            </div>
                            <div class="radio {{$orden->retiro_en_local ? 'disabled' : ''}}" style="margin-top: 15px">
                                <label>
                                    <input type="radio" name="forma_pago" id="forma_pago4" value="{{\App\Models\TipoPago::TARJETA_CREDITO}}" {{$orden->retiro_en_local ? 'disabled' : ''}} required>
                                    Paga con TARJETA DE CRÉDITO al recibir tu pedido
                                    @if($orden->retiro_en_local)
                                    <br><small class="text-muted text-danger">no disponible en modo retiro en local</small>
                                    @endif
                                </label>
                            </div>
                            <div class="radio {{$orden->retiro_en_local ? 'disabled' : ''}}" style="margin-top: 15px">
                                <label>
                                    <input type="radio" name="forma_pago" id="forma_pago5" value="{{\App\Models\TipoPago::TARJETA_DEBITO}}" {{$orden->retiro_en_local ? 'disabled' : ''}} required>
                                    Paga con TARJETA DE DÉBITO al recibir tu pedido
                                    @if($orden->retiro_en_local)
                                        <br><small class="text-muted text-danger">no disponible en modo retiro en local</small>
                                    @endif
                                </label>
                            </div>


                        </div>
                        <div class="col-sm-6 pull-right">
                            <a href="{{route('productos')}}" type="submit" class="btn btn-lg btn-block btn-danger">
                                <i class="fa fa-shopping-cart"></i> Seguir Comprando
                            </a>
                            <br>

                            <button type="submit" class="btn btn-lg btn-block btn-danger" :disabled="disabledPagar">
                                <i class="fa fa-money"></i> Pagar</button>
                            <span v-show="items.length == 0">
                                <br>
                                <span class="text-danger">
                                    No hay ningún artículo en el carro
                                </span>
                            </span>
                            <span v-show="(total<=0 && items.length > 0)">
                                <br>
                                <span class="text-danger">
                                    El total es menor o igual a 0
                                </span>
                            </span>
                            <span v-show="faltaArmarCombos">
                                <br>
                                <span class="text-danger">
                                    *Faltan combos por armar
                                </span>
                            </span>
                            <br>
                        </div>
                    </div>
                    {{Form::close()}}
                    </div>
                </div>
            </section>

        </main>
    </div>

    <!-- Modal -->
    <div class="modal modal-danger fade" id="modalEditItem" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Editar items y agregados</h4>
                </div>
                <div class="modal-body">

                    <h1 class="text-center" v-if="loadingReceta || loadingAgregados">
                        <i class="fa fa-refresh fa-spin"></i>
                    </h1>

                    <form id="datos-item-modifica" v-else>

                        <div class="row" >
                            <div class="col-sm-6" v-for="itemNumber in cantidadItemsModal" style="padding: 2px">
                                <div class="panel panel-danger">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">
                                            <span v-text="nombreItemDetEdit+' '+itemNumber"></span>
                                        </h3>
                                    </div>
                                    <div class="panel-body">
                                        <!--            Detalles receta (ingredientes)
                                        ------------------------------------------------------------------------>
                                        <ul>
                                            <li v-for="det in recetaDetalles">
                                                <template v-if="det.opcionales.length > 0">

                                                    <div class="radio-inline" v-for="(ingrediente,index) in det.opcionales">
                                                        <label>

                                                            <input
                                                                    type="radio"
                                                                    :name="'modificaciones['+itemNumber+']['+det.id+']'"
                                                                    :checked="index===0"
                                                                    :value="ingrediente.id"
                                                            >
                                                            <span v-text="ingrediente.nombre"></span>
                                                        </label>
                                                    </div>
                                                    {{--<div class="radio-inline">--}}
                                                        {{--<label>--}}

                                                            {{--<input--}}
                                                                    {{--type="radio"--}}
                                                                    {{--:name="'modificaciones['+itemNumber+']['+det.id+']'"--}}
                                                                    {{--value="0">--}}
                                                            {{--Sin <span v-text="det.categoria"></span>--}}
                                                        {{--</label>--}}
                                                    {{--</div>--}}

                                                </template>
                                                <template v-else>

                                                    <label class="checkbox-inline">
                                                        <input
                                                                type="checkbox"
                                                                :name="'modificaciones['+itemNumber+']['+det.id+']'"
                                                                :value="det.ingrediente.id"
                                                                checked>
                                                        <span v-text="det.ingrediente.nombre"></span>
                                                    </label>
                                                </template>
                                            </li>
                                            <li v-if="recetaDetalles.length == 0">
                                                Sin ingredientes definidos
                                            </li>
                                        </ul>
                                        <hr>

                                        <!--            Adicionales
                                        ------------------------------------------------------------------------>
                                        <span>Adicionales</span>
                                        <ul>
                                            <li v-for="(adicional,aIndex) in adicionales">
                                                <label class="checkbox-inline">
                                                    <input
                                                            type="checkbox"
                                                            :id="'adicionales'+itemNumber.toString()+aIndex.toString()"
                                                            :name="'adicionales['+itemNumber+'][]'"
                                                            :value="adicional.id"
                                                    >
                                                    <span v-text="adicional.nombre"></span> /&nbsp;
                                                    {{dvs()}} <span v-text="nf(adicional.precio_venta)"></span>
                                                </label>

                                            </li>
                                            <li v-if="adicionales.length == 0">
                                                Sin adicionales asociados
                                            </li>
                                        </ul>
                                        <hr>

                                        <!--            Salsas
                                        ------------------------------------------------------------------------>
                                        <span>Salsas</span>
                                        <ul>
                                            <li v-for="(salsa,sIndex) in salsas">
                                                <label class="checkbox-inline">
                                                    <input type="checkbox"
                                                           :id="'salsas'+itemNumber.toString()+sIndex.toString()"
                                                           :name="'salsas['+itemNumber+'][]'"
                                                           :value="salsa.id"
                                                    >
                                                    <span v-text="salsa.nombre"></span> /&nbsp;
                                                    {{dvs()}}<span v-text="nf(salsa.precio_venta)"></span>
                                                </label>
                                            </li>
                                            <li v-if="salsas.length == 0">
                                                Sin salsas asociadas
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                    <button type="button" class="btn btn-primary" @click="guardarModificacion(detEdit)">
                        Guardar

                        <i class="fa fa-refresh fa-spin" v-show="loadingModificaciones"></i>
                    </button>
                </div>
            </div>
        </div>
    </div>
@endsection


@push('scripts')
@include('layouts.vue')
@include('layouts.axios')
@include('layouts.plugins.sweetalert2')
<script>

    new Vue({
        el: '#root',
        created() {
            this.getDatos();
        },
        data: {
            url : "{{route('cart.index')}}",
            urladd : "{{route('add.cart')}}",
            urldel : "{{route('remove.cart',0)}}",
            items: [],
            loading: false,
            idEliminando: '',
            piezaAdd : [],
            bebidaAdd : [],
            piezasCombo: [],
            bebidasCombo: [],
            cntCombos: false,
            combosCompletados: [],
            delivery : "{{$orden->delivery}}",
            monto_delivery : "{{$orden->monto_delivery}}",

            recetaDetalles: [],
            adicionales: [],
            salsas: [],
            cantidadItemsModal: 0,
            detEdit: {},
            itemEdit: {},
            loadingReceta : false,
            loadingAgregados : false,
            loadingModificaciones: false,
            modificaciones: {},
        },
        methods: {
            nf(cant){
                return numf(cant);
            },
            piezasDisponibles(piezas,index){

                var piezasAgregadas = this.piezasCombo[index];
                var disponibles = [];
                var temps= [];

                $.each(piezasAgregadas,function (index,item) {
                    temps.push(item.item.id);
                });

                $.each(piezas ,function (index,item) {
                    if(!($.inArray(item.item.id,temps)>=0)){
                        disponibles.push(item);
                    }
                });


                return disponibles;
            },
            bebidasDisponibles(piezas,index){

                var bebidasAgregadas = this.bebidasCombo[index];
                var disponibles = [];
                var temps= [];

                $.each(bebidasAgregadas,function (index,item) {
                    temps.push(item.item.id);
                });

                $.each(piezas ,function (index,item) {
                    if(!($.inArray(item.item.id,temps)>=0)){
                        disponibles.push(item);
                    }
                });


                return disponibles;
            },
            definePiezas(){
                //console.log('Define arrays para guardar piezas y bebidas agregadas');
                //por cada item agrega un array
                $.each(this.items,() => {
                    this.piezasCombo.push([]);
                    this.bebidasCombo.push([]);
                    this.piezaAdd.push({item: {},cantidad: 1});
                    this.bebidaAdd.push({item: {},cantidad: 1});
                });

            },
            getDatos(){
                console.log('Metodo Get Datos');
                axios.get(this.url).then(response =>{
                    this.items = response.data;
                    this.loading= false;
                    this.idEliminando= '';
                    this.definePiezas();
                }).catch(error =>{
                    console.log(error.response);
                })
            },
            add(id){
                console.log('Agregar producto',id);
                var datos = {'itemId' : id};

                axios.post(this.urladd,datos).then(response =>{
                    console.log(response.data);
                    $("#modalCart").modal('show');
                    this.getDatos();
                }).catch(error =>{
                    console.log(error.response);
                })
            },
            eliminar(id){
                this.loading=true;
                this.idEliminando= id;

                console.log('Remover producto',id);

                var url = this.urldel.replace('0',id);
                console.log(url);

                axios.get(url).then(response =>{
//                        console.log(response.data);
                    $("#modalCart").modal('show');
                    this.getDatos();
                }).catch(error =>{
                    this.idEliminando= '';
//                        console.log(error.response);
                })
            },
            addPieza(combo,tope) {
                var cantidad = parseInt(this.piezaAdd[combo].cantidad);
                var item = this.piezaAdd[combo].item;

                if(item.id == null){
                    toastr.error('Seleccionte una pieza');
                    return;
                }

                if(cantidad <= 0){
                    toastr.error('La cantidad debe ser mayor o igual a 1');
                    return;
                }

                console.log('agregar',cantidad,item.nombre,'a combo',combo,'Piezas:',this.cantidadPiezas(combo),'Bebidas:',this.cantidadBebidas(combo));

                if(cantidad+this.cantidadPiezas(combo)>tope){
                    swal({
                        title: 'Error!',
                        text: 'No puede agregar mas piezas de las indicadas',
                        type: 'error',
                        confirmButtonText: 'ok'
                    });
                    return false;
                }

                var piezasCombo = this.piezasCombo[combo];

                const pieza = {
                    item : item,
                    cantidad : cantidad,
                };

                piezasCombo.push(pieza);

                this.piezasCombo.splice( combo, 1, piezasCombo);

                this.piezaAdd[combo] = {
                    item : {},
                    cantidad: 1,
                }
            },
            addBebida(combo,tope) {
                var cantidad = parseInt(this.bebidaAdd[combo].cantidad);
                var item = this.bebidaAdd[combo].item;

                if(item.id == null){
                    toastr.error('Seleccionte una bebida');
                    return;
                }

                if(cantidad <= 0){
                    toastr.error('La cantidad debe ser mayor o igual a 1');
                    return;
                }

                console.log('agregar',cantidad,item.nombre,'a combo',combo,'Piezas:',this.cantidadPiezas(combo),'Bebidas:',this.cantidadBebidas(combo));

                if(cantidad+this.cantidadBebidas(combo)>tope){
                    swal({
                        title: 'Error!',
                        text: 'No puede agregar mas bebidas de las indicadas',
                        type: 'error',
                        confirmButtonText: 'ok'
                    });
                    return false;
                }


                var bebidasCombo = this.bebidasCombo[combo];

                const pieza = {
                    item : item,
                    cantidad : cantidad,
                };

                bebidasCombo.push(pieza);

                this.bebidasCombo.splice( combo, 1, bebidasCombo);

                this.bebidaAdd[combo] = {
                    item : {},
                    cantidad: 1,
                }
            },
            removerPieza(indexCombo,indexPieza){
                console.log('Remover pieza a combo: ',indexCombo,' Pieza: ',indexPieza);
                this.piezasCombo[indexCombo].splice(indexPieza,1);
            },
            removerBebida(indexCombo,indexBebida){
                console.log('Remover bebida a combo: ',indexCombo,' Pieza: ',indexBebida);
                this.bebidasCombo[indexCombo].splice(indexBebida,1);
            },
            keymonitor: function(event) {
                var cantidad = $(event.currentTarget);

                if (event.key == "Enter") {
                    event.preventDefault();
                    var div = cantidad.closest('div');
                    div.find('.btn-add-pieza').focus();
                }

            },
            cantidadPiezas(index){
                var cantidad = 0;
                $.each(this.piezasCombo[index],function (index,item) {
                    cantidad += parseInt(item.cantidad);
                })

                return cantidad;
            },
            cantidadBebidas(index){
                var cantidad = 0;
                $.each(this.bebidasCombo[index],function (index,item) {
                    cantidad += parseInt(item.cantidad);
                })

                return cantidad;
            },
            modificar(det){

                if (det.combo){

                    cantidad = 1;
                } else {
                    cantidad = det.qty;
                }

                console.log('Editar Detalle: ' , det.id,det.nombre, ' Item: ',det.id,det.nombre);

                this.cantidadItemsModal = parseInt(cantidad);
                this.detEdit = det;
                this.itemEdit = det;
                this.loadingReceta = true;
                this.loadingAgregados =true;

                this.getRecetaDetalles(det);
                this.getAdicionalesYsalsas(det);

                $('#modalEditItem').modal('show');
            },
            modificarPiezaCombo(det,pieza,cantidad){

                if(cantidad === null){

                    cantidad = pieza.cantidad;
                }

                item = pieza.item;

                console.log('Editar Detalle: ' , det.id,det.nombre, ' Item: ',item.id,item.nombre);

                this.cantidadItemsModal = parseInt(cantidad);
                this.detEdit = det;
                this.itemEdit = item;
                this.loadingReceta = true;this.loadingAgregados =true;


                this.getRecetaDetalles(item);
                this.getAdicionalesYsalsas(item);

                $('#modalEditItem').modal('show');
            },
            getRecetaDetalles(item){

                console.log('getRecetaDetalles: '+item.nombre);
                var url = "{{route("item.get.ingredientes")}}";

                var params= { params: {id: item.id} };

                axios.get(url,params).then(response => {
                    //console.log('-> Res getRecetaDetalles',response.data.data);

                    this.recetaDetalles = response.data.data;
                    this.loadingReceta = false;
                })
                .catch(error => {
                    console.log(error);
                    this.recetaDetalles = [];
                    this.loadingReceta = false;

                });

            },
            getAdicionalesYsalsas(item){

                console.log('getAdicionalesYsalsas: '+item.nombre);

                var url = "{{route("item.get.agregados")}}";

                var params= { params: {item_id: item.id} };

                axios.get(url,params).then(response => {
                    //console.log('-> Res getAdicionalesYsalsas',response.data.data);

                    this.adicionales = response.data.data.adicionales;
                    this.salsas = response.data.data.salsas;
                    this.loadingAgregados =false;

                })
                    .catch(error => {
                        console.log(error);
                        this.adicionales = [];
                        this.salsas = [];
                        this.loadingAgregados =false;
                    });
            },
            guardarModificacion(detEdit){
                this.loadingModificaciones =true;
                var dataArray = $('#datos-item-modifica').serializeArray();
                var data = $('#datos-item-modifica').serialize();

                data = data+'&item_id='+this.itemEdit.id+'&det_id='+detEdit.id;

                var url = "{{route("carro.store.modify")}}";

                console.log('url',url+'?'+data);
                console.log(dataArray);

                axios.get(url+'?'+data).then(response => {

                    var data = response.data.data;
                    console.log(data);

                    $('#modalEditItem').modal('hide');

                    toastr.success('listo! cambios guardados.');

                    this.loadingModificaciones =false;

                    detEdit.total_modificaciones = parseFloat(data.total);


                })
                .catch(error => {
                    console.log(error);
                    toastr.error(error.response.data.message);
                    this.loadingModificaciones =false;
                    this.getModificaciones(detEdit);
                });
            }
        },
        computed: {
            nombreItemDetEdit(){
                return this.itemEdit.nombre;
            },
            cantidad_articulos(){
                var cant=0;
                $.each(this.items,function (index,item) {
                    cant=cant+parseInt(item.qty);
                });

                return cant;

            },
            subTotal(){
                var total=0;
                $.each(this.items,function (index,item) {
                    var cantidad = isNaN(parseFloat(item.qty)) ? 0 : parseFloat(item.qty);
                    var precio = isNaN(parseFloat(item.precio_venta)) ? 0 : parseFloat(item.precio_venta);
                    var total_modificaciones = isNaN(parseFloat(item.total_modificaciones)) ? 0 : parseFloat(item.total_modificaciones);

                    total += (cantidad*precio) + total_modificaciones;
                });

                return total;
            },
            total(){
                var total=this.subTotal;

                if(this.delivery=='1'){
                    total+=parseInt(this.monto_delivery);
                }

                return total;
            },
            faltaArmarCombos(){
                return (this.cntCombos!=this.combosCompletados.length )
            },
            disabledPagar(){
                return (this.items.length == 0 || (this.total<=0) || this.faltaArmarCombos);
            }
        },
        watch: {
            piezasCombo: function(val){
                this.combosCompletados = [];

                $.each(this.items, (index,item) => {
                    if(item.combo){

                        if(item.combo.piezas==this.cantidadPiezas(index) && item.combo.bebidas==this.cantidadBebidas(index)){
                            console.log('combo',index,'Completado');
                            this.combosCompletados.push(1);
                        }else {
                            console.log('combo',index,'NO completado');
                        }
                    }

                });
            },
            bebidasCombo: function(val){
                this.combosCompletados = [];

                $.each(this.items, (index,item) => {
                    if(item.combo){
                        this.combosCompletados.splice(index,1);

                        if(item.combo.piezas==this.cantidadPiezas(index) && item.combo.bebidas==this.cantidadBebidas(index)){
                            console.log('combo',index,'Completado');
                            this.combosCompletados.push(1);
                        }else {
                            console.log('combo',index,'NO completado');
                        }
                    }

                });
            },
            cantidad_articulos: function (val) {
//                console.log('cambio en cantidad_articulos');

                var items = [];
                var cntCombos = 0;
                $.each(this.items, (index,item) => {

                    //si el articulo tiene combo y el combo tiene mas de un detalle
                    if(item.combo ){

                        var cantidad = isNaN(parseFloat(item.qty)) ? 0 : parseFloat(item.qty);

                        item.combo.piezas=cantidad*item.combo.piezas_val_o;
                        item.combo.bebidas=cantidad*item.combo.bebidas_val_o;
//                        console.log('cantidad',cantidad,'piezas',item.combo.piezas,'bebidas',item.combo.bebidas);

                        if(item.combo.detalles.length > 1){
                            cntCombos++;
                        }
                    }

                    items.push(item);
                });

                this.items = items;
                this.cntCombos = cntCombos;
            }
        }
    });
</script>
@endpush
