<!-- Modal Car -->
<div class="modal fade" id="modalCart" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        {{Form::open(['route' => 'modal.pagar','method' => 'post'])}}
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close close-butto-modal" data-dismiss="modal" aria-label="Close">
                        <i class="fa fa-times" aria-hidden="true" ></i>
                    </button>
                    <h4 class="modal-title" id="modelTitleId">
                        Carro de compras
                        @isset($tienda)
                            <small><br>Local: {{ $tienda->nombre }}</small>
                        @endisset
                    </h4>
                </div>

                <table class="table table-condensed">
                    <tbody>
                        <tr v-if="items.length == 0">
                            <td colspan="5" class="text-center text-danger">
                                <br>Ningún Artículo agregado
                            </td>
                        </tr>
                        <tr v-for="item in items" class="p-0">
                            <td width="25%" style="padding: 10px;">
                                <img :src="item.imagen" alt="" class="img-cart" >
                            </td>
                            <td width="40%" v-text="item.nombre"></td>
                            <td width="10%" valign="middle">
                                <input
                                        @focus="$event.target.select()"
                                        type="number"
                                        :name="'cantidades['+item.id+']'"
                                        class="form-control input-sm" value="1" v-model="item.qty"
                                        min="0"
                                        max="50"
                                >
                            </td>
                            <td width="10%" class="text-right">
                                {{dvs()}}<span v-text="nf(item.precio_venta*item.qty)"></span>
                            </td>
                            <td width="10%" class="text-center">
                                <button type="button" class="btn btn-danger btn-sm" @click="eliminar(item.id)">
                                    <span v-show="idEliminando==item.id">
                                        <i class="fa fa-spinner fa-spin"></i>
                                    </span>
                                    <span v-show="!(idEliminando==item.id)">
                                        <i class="fa fa-remove" aria-hidden="true"></i>
                                    </span>
                                </button>
                            </td>
                        </tr>
                        <tr v-show="items.length > 0">
                            <td colspan="3">
                                Total:
                            </td>
                            <td class="text-right">
                                $<span v-text="nf(total)"></span>
                            </td>
                            <td></td>
                        </tr>
                    </tbody>
                </table>

                <div class="modal-body" style="padding: 0">

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary pull-left" data-dismiss="modal">
                        « Seguir comprando
                    </button>
                    <button type="submit" class="btn btn-danger text-uppercase text-bold" :disabled="cantidad_articulos==0">
                        Pagar mi Pedido
                    </button>
                </div>
            </div>
        {{Form::close()}}
    </div>
</div>