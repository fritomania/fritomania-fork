<!DOCTYPE html>
<!--[if IE 8 ]>    <html lang="en" class="no-js ie8"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en" class="no-js"> <!--<![endif]-->


<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="canonical" href="http://demo.designshopify.com/" />
    <meta name="theme-color" content="#7796A8">
    <meta name="description" content="" />
    <title>
        Contact - Fast Food
    </title>

    <link href="https://fonts.googleapis.com/css?family=Josefin+Sans:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Playball:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Bitter:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

    <link href="assets/stylesheets/bootstrap.min.css" rel="stylesheet" type="text/css" media="all">
    <link href="../../maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" media="all">
    <link href="assets/stylesheets/fonts.googleapis.css" rel="stylesheet" type="text/css" media="all">
    <link href="assets/stylesheets/font-awesome.min.css" rel="stylesheet" type="text/css" media="all">
    <link href="assets/stylesheets/icon-font.min.css" rel="stylesheet" type="text/css" media="all">
    <link href="assets/stylesheets/social-buttons.css" rel="stylesheet" type="text/css" media="all">
    <link href="assets/stylesheets/cs.styles.css" rel="stylesheet" type="text/css" media="all">
    <link href="assets/stylesheets/font-icon.css" rel="stylesheet" type="text/css" media="all">
    <link href="assets/stylesheets/owl.carousel.min.css" rel="stylesheet" type="text/css" media="all">
    <link href="assets/stylesheets/spr.css" rel="stylesheet" type="text/css" media="all">
    <link href="assets/stylesheets/slideshow-fade.css" rel="stylesheet" type="text/css" media="all">
    <link href="assets/stylesheets/cs.animate.css" rel="stylesheet" type="text/css" media="all">
    <link href="assets/stylesheets/themepunch.revolution.css" rel="stylesheet" type="text/css" media="all">
    <link href="assets/stylesheets/jquery.fancybox.css" rel="stylesheet" type="text/css" media="all">

    <script type="text/javascript" src="assets/javascripts/jquery.min.js"></script>
    <script type="text/javascript" src="assets/javascripts/classie.js"></script>
    <script type="text/javascript" src="assets/javascripts/application-appear.js"></script>
    <script type="text/javascript" src="assets/javascripts/jquery.themepunch.plugins.min.js"></script>
    <script type="text/javascript" src="assets/javascripts/jquery.themepunch.revolution.min.js"></script>
    <script type="text/javascript" src="assets/javascripts/cs.script.js"></script>
    <script type="text/javascript" src="assets/javascripts/jquery.currencies.min.js"></script>
    <script type="text/javascript" src="assets/javascripts/jquery.zoom.min.js"></script>
    <script type="text/javascript" src="assets/javascripts/linkOptionSelectors.js"></script>
    <script type="text/javascript" src="assets/javascripts/owl.carousel.min.js"></script>
    <script type="text/javascript" src="assets/javascripts/scripts.js"></script>
    <script type="text/javascript" src="assets/javascripts/social-buttons.js"></script>
    <script type="text/javascript" src="assets/javascripts/bootstrap.min.js"></script>
    <script type="text/javascript" src="assets/javascripts/jquery.touchSwipe.min.js"></script>
    <script type="text/javascript" src="assets/javascripts/jquery.fancybox.js"></script>

    <script type="text/javascript" src="assets/javascripts/jquery.gmap.min.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBSxX2Un4CjYcWVyA3FOjBNcrVC1hlervk" type="text/javascript"></script>

</head>

<body class="fastfood_1" >
@include('layouts.partials_cliente.mainheader')
<!--Header-->

<div class="fix-sticky"></div>

<!-- Main Content -->
<div class="page-container" id="PageContainer">
    <main class="main-content" id="MainContent" role="main">
        <section class="heading-content">
            <div class="heading-wrapper">
                <div class="container">
                    <div class="row">
                        <div class="page-heading-inner heading-group">
                            <div class="breadcrumb-group">
                                <h1 class="hidden">Contacto</h1>
                                <div class="breadcrumb clearfix">
										<span itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><a href="index-2.html" title="Fast Food" itemprop="url"><span itemprop="title"><i class="fa fa-home"></i></span></a>
										</span>
                                    <span class="arrow-space"></span>
                                    <span itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
											<a href="contact.html" title="Contact" itemprop="url"><span itemprop="title">Contacto</span></a>
										</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="page-content">
            <div class="page-wrapper">
                <div class="container">
                    <div class="row">
                        <div class="page-inner">
                            <div id="shopify-section-contact-template" class="shopify-section">
                                <div class="contact-content">

                                    <div class="google-maps-content ">
                                        <div class="google-maps-wrapper">
                                            <div class="page_title">
                                                <h2 class="title">
                                                    <span class="first">Contáctenos</span>

                                                </h2>
                                                <p class="caption">Nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Mauris in erat justo. Nullam ac urna eu felis dapibus.</p>
                                            </div>
                                            <div class="google-maps-inner">
                                                <div id="contact_map" class="m_map"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="information_layout">
                                        <div class="information_inner">
                                            <div class="information_content">
                                                <div class="information_item col-sm-4">
                                                    <div class="page_title text_title">
                                                        <h2 class="title">Horario</h2>
                                                    </div>
                                                    <div class="text_content">
                                                        <ul>
                                                            <li>
                                                                <div class="group_text">
                                                                    <span class="day">Lun – Vie</span>
                                                                    <span class="time">9AM - 7PM</span>
                                                                </div>
                                                            </li>

                                                            <li>
                                                                <div class="group_text">
                                                                    <span class="day">Sábado</span>
                                                                    <span class="time">9AM - 7PM</span>
                                                                </div>
                                                            </li>

                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="information_item col-sm-4">
                                                    <div class="page_title text_title">
                                                        <h2 class="title">Información del Contacto</h2>
                                                    </div>
                                                    <div class="text_content">
                                                        <div class="group_contact_info">
                                                            <div class="item">
																	<span>
																		<i class="fa fa-home"></i><address>123 Mission Santiago</address>
																	</span>
                                                            </div>
                                                            <div class="item phone-fax">
																	<span>
																		<i class="fa fa-phone"></i><a href="tel:(084)0123456789">(084) 0123 456 789</a>
																	</span>
                                                                <span>
																		<i class="fa fa-fax"></i><a href="fax:(084)0123456789">(084) 0123 456 789</a>
																	</span>
                                                            </div>
                                                            <div class="item">
																	<span>
																		<i class="fa fa-envelope-o"></i><a href="mailto:info@adart.vn">contacto@fritomania.cl</a>
																	</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="information_item col-sm-4">
                                                    <div class="page_title form_title">
                                                        <h2 class="title">Escríbenos un Mensaje</h2>
                                                    </div>
                                                    <div class="form_content">
                                                        <form method="post" action="http://demo.designshopify.com/html_fastfood/contact.html" id="contact_form" class="contact-form" accept-charset="UTF-8">
                                                            <div id="contactFormWrapper" class="group_form">
                                                                <div class="col-md-6">
                                                                    <div class="form-item">
                                                                        <input type="text" id="contactFormName" name="contact[name]" placeholder="Usuario">
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <div class="form-item">
                                                                        <input type="email" id="contactFormEmail" name="contact[email]" placeholder="Email">
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-12">
                                                                    <div class="form-item">
                                                                        <textarea rows="15" cols="75" id="contactFormMessage" name="contact[body]" placeholder="Tu mensaje"></textarea>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-12">
                                                                    <div class="form-item">
                                                                        <input type="submit" id="contactFormSubmit" value="Enviar" class="_btn">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <script>
                                    $(window).ready(function($) {

                                        if (jQuery().gMap) {
                                            if ($('#contact_map').length) {
                                                $('#contact_map').gMap({
                                                    zoom: 17,
                                                    scrollwheel: false,
                                                    maptype: 'ROADMAP',
                                                    markers: [{
                                                        address: '474 Ontario St Toronto, ON M4X 1M7 Canada',
                                                        html: '_address'
                                                    }]
                                                });
                                            }
                                        }
                                    });
                                </script>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
</div>

<!-- Footer -->
<footer class="footer">
    <div id="shopify-section-theme-footer" class="shopify-section">
        <section class="footer-information-block clearfix" style="background-image:  url(assets/images/bg_footer.png);">
            <div class="container">
                <div class="row">
                    <div class="footer-information-inner">
                        <div class="footer-information-content">
                            <div class="information-item col-sm-4 not-animated" data-animate="fadeInUp" data-delay="200">
                                <div class="about-content">
                                    <div class="logo-footer">
                                        <img src="assets/images/logo_footer.png" alt="" />
                                    </div>
                                    <div class="about-caption">
                                        Skort Maison Martin Margiela knot ponytail cami texture tucked t-shirt. Black skirt razor pleats plaited gold collar.
                                        <div class="clearfix" style="margin-bottom: 5px; "></div>
                                        Crop 90s spearmint indigo seam luxe washed out. Prada Saffiano cash mere crop sneaker chignon
                                    </div>
                                    <div class="about-contact">
                                        <div class="item">
                                            <span class="cs-icon icon-marker"></span><address>No 1104 Sky Tower, Las Vegas, USA</address>
                                        </div>
                                        <div class="item">
                                            <span class="cs-icon icon-phone"></span><a href="tel:(084)0123456789">(084) 0123 456 789</a>
                                        </div>
                                        <div class="item">
                                            <span class="cs-icon icon-mail"></span><a href="mailto:contac@yourcompany.com">contac@yourcompany.com</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="blog-group col-sm-4 not-animated" data-animate="fadeInUp" data-delay="200">
                                <h5 class="footer-title">Recent Post</h5>
                                <div class="blog-content">
                                    <div class="blogs-item">
                                        <div class="blogs-left">
                                            <a class="blogs-img" href="article.html">
                                                <img src="assets/images/blog-01.jpg" alt="The art of food" />
                                            </a>
                                        </div>
                                        <div class="blogs-right">
                                            <a href="article.html" class="blogs-title clearfix">The art of food</a>
                                            <span class="date">Oct 26th</span>
                                        </div>
                                    </div>
                                    <div class="blogs-item">
                                        <div class="blogs-left">
                                            <a class="blogs-img" href="article.html">
                                                <img src="assets/images/blog-04.jpg" alt="Sports luxe sandal center part pastel loafer leather skirt sneaker" />
                                            </a>
                                        </div>
                                        <div class="blogs-right">
                                            <a href="article.html" class="blogs-title clearfix">Sports luxe sandal center part pastel loafer leather skirt sneaker</a>
                                            <span class="date">Oct 25th</span>
                                        </div>
                                    </div>
                                    <div class="blogs-item">
                                        <div class="blogs-left">
                                            <a class="blogs-img" href="article.html">
                                                <img src="assets/images/blog-03.jpg" alt="Hermes Givenchy skort chambray Cara D skirt cable knit" />
                                            </a>
                                        </div>
                                        <div class="blogs-right">
                                            <a href="article.html" class="blogs-title clearfix">Hermes Givenchy skort chambray Cara D skirt cable knit</a>
                                            <span class="date">Oct 25th</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="social-payment-item col-sm-4 not-animated" data-animate="fadeInUp" data-delay="200">
                                <h5 class="footer-title"> Follow Us</h5>
                                <div class="social-content">
                                    <div class="social-caption">
                                        <a href="https://www.facebook.com/shopify" title="Fast Food on Facebook" class="icon-social facebook"><i class="fa fa-facebook"></i></a>
                                        <a href="https://twitter.com/shopify" title="Fast Food on Twitter" class="icon-social twitter"><i class="fa fa-twitter"></i></a>
                                        <a href="https://plus.google.com/+shopify" title="Fast Food on Google+" class="icon-social google-plus"><i class="fa fa-google-plus"></i></a>
                                        <a href="https://www.pinterest.com/shopify" title="Fast Food on Pinterest" class="icon-social pinterest"><i class="fa fa-pinterest"></i></a>
                                        <a href="https://instagram.com/shopify" title="Fast Food on Instagram" class="icon-social instagram"><i class="fa fa-instagram"></i></a>
                                        <a href="https://www.youtube.com/user/shopify" title="Fast Food on Youtube" class="icon-social youtube"><i class="fa fa-youtube"></i></a>
                                    </div>
                                </div>
                                <div class="payment-content ">
                                    <h5 class="footer-title">Payment Methods</h5>
                                    <div class="payment-caption">
                                        <span class="icon-cc icon-cc-discover" title="Discover"></span>
                                        <span class="icon-cc icon-cc-american" title="Amex"></span>
                                        <span class="icon-cc icon-cc-western" title="Western Union"></span>
                                        <span class="icon-cc icon-cc-paypal" title="PayPal"></span>
                                        <span class="icon-cc icon-cc-moneybookers" title="MoneyBookers"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="copy-right clearfix">
            <div class="copy-right-wrapper">
                <div class="copy-right-inner">
                    <div class="footer_links">
                        <ul>
                            <li><a href="index-2.html" title="Home">Home</a></li>
                            <li><a href="collections.html" title="Pizza">Pizza</a></li>
                            <li><a href="collections.html" title="Hamburger">Hamburger</a></li>
                            <li><a href="collections.html" title="Fast food">Fast food</a></li>
                            <li><a href="collections.html" title="Drinks">Drinks</a></li>
                            <li><a href="collections.html" title="Combo buy">Combo buy</a></li>
                            <li><a href="contact.html" title="Contact">Contact</a></li>
                            <li><a href="wish-list.html" title="Wishlist">Wishlist</a></li>
                            <li><a href="account.html" title="My account">My account</a></li>
                            <li><a href="login.html" title="Login">Login</a></li>
                        </ul>
                    </div>
                    <div class="footer_copyright">Copyright &copy; 2017 <a href="index-2.html" title="">Fast Food</a>. All Rights Reserved</div>
                </div>
            </div>
        </section>
    </div>
</footer>

<!-- Modal Search-->
<div class="modal fade" id="lightbox-search" tabindex="-1" role="dialog" aria-labelledby="lightbox-search" aria-hidden="true" style="display: none;">
    <div class="modal-dialog animated" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span class="cs-icon icon-close"></span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Search something</h4>
            </div>
            <div class="modal-body">
                <div id="search-info">
                    <form class="search" action="http://demo.designshopify.com/html_fastfood/search.html" style="position: relative;">
                        <input type="hidden" name="type" value="product">
                        <input type="text" name="q" class="search_box" placeholder="search our store" value="" autocomplete="off">
                        <span class="search-clear cs-icon icon-close" style="display: none;"></span>
                        <button class="search-submit" type="submit">
                            <span class="cs-icon icon-search"></span>
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Float right icon -->
<div class="float-right-icon">
    <ul>
        <li>
            <div id="scroll-to-top" data-toggle="" data-placement="left" title="Scroll to Top" class="off">
                <i class="fa fa-angle-up"></i>
            </div>
        </li>
    </ul>
</div>

</body>