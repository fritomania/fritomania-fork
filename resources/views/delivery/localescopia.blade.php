<!DOCTYPE html>
<html>
<head>
    <title>Locales</title>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
    <meta charset="utf-8">
    <link rel="stylesheet" href="css/bootstrap4.min.css">

    <script src="js/jquery-3.3.1.min.js"></script>

    <style>
        /* Always set the map height explicitly to define the size of the div
         * element that contains the map. */
        #map {
            height: 500px;
            width: 450px;
        }
    </style>


    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <style>

        .restaurant-detail {
            position: relative;
            margin-bottom: 20px;
            padding-left: 40px;
        }
        * {
            box-sizing: border-box;
        }

        body {
            font: 16px Arial;
        }

        .autocomplete {
            /*the container must be positioned relative:*/
            position: relative;
            display: inline-block;
        }

        input {
            border: 1px solid transparent;
            background-color: #f1f1f1;
            padding: 10px;
            font-size: 16px;
        }

        input[type=submit] {
            background-color: DodgerBlue;
            color: #fff;
            cursor: pointer;
        }

        .autocomplete-items {
            position: absolute;
            border: 1px solid #d4d4d4;
            border-bottom: none;
            border-top: none;
            z-index: 99;
            /*position the autocomplete items to be the same width as the container:*/
            top: 100%;
            left: 0;
            right: 0;
        }

        .autocomplete-items div {
            padding: 10px;
            cursor: pointer;
            background-color: #fff;
            border-bottom: 1px solid #d4d4d4;
        }

        .autocomplete-items div:hover {
            /*when hovering an item:*/
            background-color: #e9e9e9;
        }

        .autocomplete-active {
            /*when navigating through the items using the arrow keys:*/
            background-color: DodgerBlue !important;
            color: #ffffff;
        }

        .restaurant-name{
            color:green;
            font-size:18px;
            font-weight:bold;
        }
        select.input-lg{
            height: 56px !important;
        }
    </style>


    <link href="https://fonts.googleapis.com/css?family=Josefin+Sans:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Playball:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Bitter:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

    <link href="cliente/stylesheets/bootstrap.min.css" rel="stylesheet" type="text/css" media="all">
    <link href="../../maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" media="all">
    <link href="cliente/stylesheets/fonts.googleapis.css" rel="stylesheet" type="text/css" media="all">
    <link href="cliente/stylesheets/font-awesome.min.css" rel="stylesheet" type="text/css" media="all">
    <link href="cliente/stylesheets/icon-font.min.css" rel="stylesheet" type="text/css" media="all">
    <link href="cliente/stylesheets/social-buttons.css" rel="stylesheet" type="text/css" media="all">
    <link href="cliente/stylesheets/cs.styles.css" rel="stylesheet" type="text/css" media="all">
    <link href="cliente/stylesheets/font-icon.css" rel="stylesheet" type="text/css" media="all">
    <link href="cliente/stylesheets/owl.carousel.min.css" rel="stylesheet" type="text/css" media="all">
    <link href="cliente/stylesheets/spr.css" rel="stylesheet" type="text/css" media="all">
    <link href="cliente/stylesheets/slideshow-fade.css" rel="stylesheet" type="text/css" media="all">
    <link href="cliente/stylesheets/cs.animate.css" rel="stylesheet" type="text/css" media="all">
    <link href="cliente/stylesheets/themepunch.revolution.css" rel="stylesheet" type="text/css" media="all">
    <link href="cliente/stylesheets/jquery.fancybox.css" rel="stylesheet" type="text/css" media="all">

    <script type="text/javascript" src="cliente/javascripts/jquery.min.js"></script>
    <script type="text/javascript" src="cliente/javascripts/classie.js"></script>
    <script type="text/javascript" src="cliente/javascripts/application-appear.js"></script>
    <script type="text/javascript" src="cliente/javascripts/jquery.themepunch.plugins.min.js"></script>
    <script type="text/javascript" src="cliente/javascripts/jquery.themepunch.revolution.min.js"></script>
    <script type="text/javascript" src="cliente/javascripts/cs.script.js"></script>
    <script type="text/javascript" src="cliente/javascripts/jquery.currencies.min.js"></script>
    <script type="text/javascript" src="cliente/javascripts/jquery.zoom.min.js"></script>
    <script type="text/javascript" src="cliente/javascripts/linkOptionSelectors.js"></script>
    <script type="text/javascript" src="cliente/javascripts/owl.carousel.min.js"></script>
    <script type="text/javascript" src="cliente/javascripts/scripts.js"></script>
    <script type="text/javascript" src="cliente/javascripts/social-buttons.js"></script>
    <script type="text/javascript" src="cliente/javascripts/bootstrap.min.js"></script>
    <script type="text/javascript" src="cliente/javascripts/jquery.touchSwipe.min.js"></script>
    <script type="text/javascript" src="cliente/javascripts/jquery.fancybox.js"></script>
</head>
<body>
@include('layouts.partials_cliente.mainheader')
<div class="col-xs-12 restaurant-search-panel">
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane active in" id="Delivery">
            <div class="row">
                <div class="col-xs-12 col-md-5 form-group">
                    <form id="searchPlaces" autocomplete="off" data-toggle="validator" role="form">
                        <div id="deliverySearchForm" style="padding-left: 60px;padding-top: 60px;">
                            <div class="col-xs-12 ">
                                <label class="control-label">Comuna<sup>*</sup></label>
                                <select onchange="dllInitAutocomplete(this);" class="form-control input-lg valid" id="ddlState" name="StateCode">
                                    <option selected="selected" value="Buin">Buin</option>
                                    <option value="Colina">Colina</option>
                                    <option value="Concepción">Concepción</option>
                                    <option value="Conchali">Conchali</option>
                                    <option value="Concón">Concón</option>
                                    <option value="Estacion Central">Estacion Central</option>
                                    <option value="Huechuraba">Huechuraba</option>
                                    <option value="Independencia">Independencia</option>
                                    <option value="La Cisterna">La Cisterna</option>
                                    <option value="La Florida">La Florida</option>
                                    <option value="La Reina">La Reina</option>
                                    <option value="La Serena">La Serena</option>
                                    <option value="Las Condes">Las Condes</option>
                                    <option value="Linares">Linares</option>
                                    <option value="Lo Barnechea">Lo Barnechea</option>
                                    <option value="Los Andes">Los Andes</option>
                                    <option value="Los Angeles">Los Angeles</option>
                                    <option value="Machalí">Machalí</option>
                                    <option value="Macul">Macul</option>
                                    <option value="Maipú">Maipú</option>
                                    <option value="Melipilla">Melipilla</option>
                                    <option value="Ñuñoa">Ñuñoa</option>
                                    <option value="Osorno">Osorno</option>
                                    <option value="Peñalolén">Peñalolén</option>
                                    <option value="Providencia">Providencia</option>
                                    <option value="Pudahuel">Pudahuel</option>
                                    <option value="Puente Alto">Puente Alto</option>
                                    <option value="Puerto Montt">Puerto Montt</option>
                                    <option value="Quilicura">Quilicura</option>
                                    <option value="Quillota">Quillota</option>
                                    <option value="Quilpue">Quilpue</option>
                                    <option value="Quinta Normal">Quinta Normal</option>
                                    <option value="Rancagua">Rancagua</option>
                                    <option value="Recoleta">Recoleta</option>
                                    <option value="San Bernardo">San Bernardo</option>
                                    <option value="San Miguel">San Miguel</option>
                                    <option value="San Pedro De La Paz">San Pedro De La Paz</option>
                                    <option value="Santiago">Santiago</option>
                                    <option value="Talca">Talca</option>
                                    <option value="Talcahuano">Talcahuano</option>
                                    <option value="Temuco">Temuco</option>
                                    <option value="Valdivia">Valdivia</option>
                                    <option value="Valparaíso">Valparaíso</option>
                                    <option value="Villa Alemana">Villa Alemana</option>
                                    <option value="Viña del mar">Viña del mar</option>
                                    <option value="Vitacura">Vitacura</option>
                                </select>
                            </div>

                            <div class="clearfix"></div>
                            <div class="col-xs-12 autocomplete">
                                <label class="control-label">Calle<sup>*</sup></label>
                                <input autocomplete="off" class="form-control input-lg typeahead" id="txtStreetName" name="StreetName" placeholder="Calle" type="text" value=""  validMessage="Por favor escriba el nombre de la calle.">
                                <span id="streetValidMessage" class="field-validation-valid text-danger help-block"></span>
                            </div>

                            <div class="col-xs-12 ">
                                <label class="control-label">Número Domicilio<sup>*</sup></label>
                                <input class="form-control input-lg" id="txtStreetNumber" max="9999999" min="1" name="StreetNumber" placeholder="Número Domicilio" type="number" value="" validMessage="Por favor escriba el número." maxValueMessage="Por favor ingresar un valor menor o igual a 9999999">
                                <span id="streetNumberValidMessage" class="field-validation-valid text-danger help-block"></span>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-xs-12 ">
                                <label class="control-label">Depto</label>
                                <input class="form-control input-lg" data-val="true" maxValueMessage="Límite de 80 caracteres" id="txtApartment" name="AddressLine1" placeholder="Depto" type="text" value="">
                                <span id="apartmentValidMessage" class="field-validation-valid text-danger help-block"></span>
                            </div>

                            <div class="col-xs-12 ">
                                <label class="control-label">Observaciones de entrega</label>
                                <input class="form-control input-lg" maxValueMessage="Límite de 80 caracteres" id="txtComments" name="Comments" placeholder="Observaciones de entrega" type="text" value="">
                                <div class="observationMessage">(*No es posible programar pedidos para más adelante ni solicitar descuentos en este cuadro. Por favor no solicitarlos.)</div>
                                <span id="commentsValidMessage" class="field-validation-valid text-danger help-block"></span>
                            </div>
                            <div><input data-val="true" data-val-required="The IsReqdValidation field is required." id="IsReqdValidation" name="IsReqdValidation" type="hidden" value="True"></div>
                            <div><input data-val="true" data-val-length="Límite de 80 caracteres" data-val-length-max="80" data-val-requiredif="Por favor introduce el nombre del lugar." data-val-requiredif-dependentproperty="IsReqdValidation" data-val-requiredif-desiredvalue="true" id="AddressTitle" name="AddressTitle" type="hidden" value="DeliverySearch"></div>
                            <div class="col-xs-12 col-md-6 ">
                                <input type="button" onclick="searchUserLocation();" class="btn btn-lg btn-block btn-secondary" value="Encontrar Local"/>
                            </div>
                        </div>
                    </form>

                    <script>
                        function autocomplete(inp, arr) {
                            var currentFocus;
                            inp.addEventListener("input", function(e) {
                                if(!arr) return false;
                                var a, b, i, val = this.value;
                                closeAllLists();
                                if (!val) { return false;}
                                currentFocus = -1;
                                a = document.createElement("DIV");
                                a.setAttribute("id", this.id + "autocomplete-list");
                                a.setAttribute("class", "autocomplete-items");
                                this.parentNode.appendChild(a);
                                for (i = 0; i < arr.length; i++) {
                                    if (arr[i].substr(0, val.length).toUpperCase() == val.toUpperCase()) {
                                        b = document.createElement("DIV");
                                        b.innerHTML = "<strong>" + arr[i].substr(0, val.length) + "</strong>";
                                        b.innerHTML += arr[i].substr(val.length);
                                        b.innerHTML += "<input type='hidden' value='" + arr[i] + "'>";
                                        b.addEventListener("click", function(e) {
                                            inp.value = this.getElementsByTagName("input")[0].value;
                                            closeAllLists();
                                        });
                                        a.appendChild(b);
                                    }
                                }
                            });
                            inp.addEventListener("keydown", function(e) {
                                var x = document.getElementById(this.id + "autocomplete-list");
                                if (x) x = x.getElementsByTagName("div");
                                if (e.keyCode == 40) {
                                    currentFocus++;
                                    addActive(x);
                                } else if (e.keyCode == 38) {
                                    currentFocus--;
                                    addActive(x);
                                } else if (e.keyCode == 13) {
                                    e.preventDefault();
                                    if (currentFocus > -1) {
                                        if (x) x[currentFocus].click();
                                    }
                                }
                            });
                            function addActive(x) {
                                if (!x) return false;
                                removeActive(x);
                                if (currentFocus >= x.length) currentFocus = 0;
                                if (currentFocus < 0) currentFocus = (x.length - 1);
                                x[currentFocus].classList.add("autocomplete-active");
                            }
                            function removeActive(x) {
                                for (var i = 0; i < x.length; i++) {
                                    x[i].classList.remove("autocomplete-active");
                                }
                            }
                            function closeAllLists(elmnt) {
                                var x = document.getElementsByClassName("autocomplete-items");
                                for (var i = 0; i < x.length; i++) {
                                    if (elmnt != x[i] && elmnt != inp) {
                                        x[i].parentNode.removeChild(x[i]);
                                    }
                                }
                            }
                            document.addEventListener("click", function (e) {
                                closeAllLists(e.target);
                            });
                        }
                        autocomplete(document.getElementById("txtStreetName"), streetsJSON);
                    </script>
                </div>
                <div class="row saved-address-information">
                    <div class="col-xs-12">
                        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBlKTwub-U-QlBh4s8bv-9fPAICyYFh3xc&libraries=places,geometry" async defer></script>
                        <div>
                            <div>
                                <div id="map"></div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>

    </div>
    <div id="divStoreSearchResultError" class="col-xs-12 restaurant-search-result-error" style="display:none;">
        <h2 class="text-danger">Lo sentimos. Por ahora no tenemos delivery hasta tu dirección.</h2>
        <div class="clearfix divider"><div></div></div>
    </div>


    <div id="divResultDetails" class="col-xs-12 restaurant-detail" style="display:none;">
        <div class="col-xs-12 col-lg-6">
            <span class="icon icon-location"></span>
            <h3 class="restaurant-name"></h3>
            <p class="restaurant-address">
            </p>
            <p class="restaurant-phone">
            </p>
        </div>
        <div class="col-xs-12 col-lg-6 restaurant-order">
            <div class="row">
                <div class="col-xs-12 col-md-6 col-lg-6 search-result-btn">
                    <input type="button" class="btn btn-lg btn-block btn-secondary" value="Ver Detalles">
                    <input type="hidden" name="OrderMode" id="OrderMode">

                </div>
                <div class="col-xs-12 col-md-6 col-lg-6 search-result-btn">
                    <input type="button" class="btn btn-lg btn-block btn-secondary" value="Pedir Ahora">
                </div>
            </div>
        </div>

    </div>
    <script>
        var map;
        var service;
        var infowindow;
        var userLoc;
        var myLatLng;
        var streetsJSON;

        $(document).ready(function(){
            dllInitAutocomplete({'value':'Buin'});
        });
        //Loc Santiago default
        var defLoc = {lat: -33.460450, lng: -70.760730};
        function initMap() {
            map = new google.maps.Map(document.getElementById('map'), {
                center: defLoc,
                zoom: 10,
                disableDefaultUI: true,
                zoomControl: true,
                fullscreenControl: true,
                zoomControlOptions: {
                    position: google.maps.ControlPosition.LEFT_BOTTOM
                },
            });

            infowindow = new google.maps.InfoWindow();
            var service = new google.maps.places.PlacesService(map);
            service.textSearch({
                location: userLoc,
                //query:"papa john's",
                query: "fritomanía"
            }, callback);
        }
        function callback(results, status) {
            if (status === google.maps.places.PlacesServiceStatus.OK) {
                map.zoom = 17;
                var posMinDistance = 0;
                var minDistance = 0;
                for (var i = 0; i < results.length; i++) {
                    //createMarker(results[i]);
                    var distance = Math.round(google.maps.geometry.spherical.computeDistanceBetween(userLoc, results[i].geometry.location));

                    if(i==0){
                        minDistance = distance;
                    }
                    else if(distance < minDistance){
                        posMinDistance = i;
                        minDistance = distance;
                    }
                }
                createMarker(results[posMinDistance]);
                var placeLoc = {lat: results[posMinDistance].geometry.location.lat(),lng:results[posMinDistance].geometry.location.lng()};
                map.setCenter(placeLoc);
            }
        }

        function createMarker(place) {
            var placeLoc = place.geometry.location;
            var marker = new google.maps.Marker({
                map: map,
                position: place.geometry.location
            });

            var service = new google.maps.places.PlacesService(map);
            var request = { reference: place.reference };
            service.getDetails(request, function(details, status) {
                google.maps.event.addListener(marker, 'click', function() {
                    infowindow.setContent("<span class='restaurant-name'>"+place.name+"</span><br/>"
                        +"<br/><table style='border-bottom:1pt solid black;'><tr><th rowspan='3'><img style='width:80px;height:40px;' src='https://www.papajohns.cl/Content/OnlineOrderingImages/Shared/pji-logo-small.png'/></th><th align='left'><b>Horario</b></th><th/></tr>"
                        +"<tr><td>Lunes a Viernes</td><td width=10/><td>12:00 pm - 12:00 am<td></tr>"
                        +"<br/><tr><td>Sabados y Domingos</td><td width=10/><td>11:00 am - 12:00 am<td></tr><tr height=10></tr>"
                        +"</table>"
                        +"<br/><span style='color:red;font-weight:bold;'>El valor mínimo para delivery es $6.000</span>");
                    infowindow.open(map, this);
                });
                $(".restaurant-name").html(place.name);
                $(".restaurant-address").html(place.formatted_address);

                $(".restaurant-phone").html("<a href='tel:"+details.formatted_phone_number+"'>"+details.formatted_phone_number+"</a>");

            });
        }

        //Logica del boton
        function searchUserLocation(){
            if(!validateForm()){
                return false;
            }
            var address = $("#ddlState").val()+", "+$("#txtStreetName").val()+" "+$("#txtStreetNumber").val();
            var geocoder = new google.maps.Geocoder();
            geocoder.geocode({'address': address}, function(results, status) {
                if (status === 'OK') {
                    $("#divStoreSearchResultError").hide();
                    $("#divResultDetails").show();
                    userLoc = results[0].geometry.location;
                    initMap();

                }
                else {
                    //alert('Geocode was not successful for the following reason: ' + status);
                    $("#divStoreSearchResultError").show();
                    $("#divResultDetails").hide();
                }
            });
        }

        function dllInitAutocomplete(item){
            $.getJSON("js/streets/chileLocation_"+$(item)[0].value+".json", function(data){
                streetsJSON = data.STREETNAME;
                $("#txtStreetName").val("");
                autocomplete(document.getElementById("txtStreetName"), streetsJSON);
            });
        }
        function validateForm(){
            var valid = true;
            if($("#txtStreetName").val().trim() == ""){
                $("#streetValidMessage").text($("#txtStreetName").attr("validMessage"));
                valid = false;
            }
            else{
                $("#streetValidMessage").html("");
            }
            if($("#txtStreetNumber").val().trim() == ""){
                $("#streetNumberValidMessage").text($("#txtStreetNumber").attr("validMessage"));
                valid = false;
            }
            else{
                if(parseInt($("#txtStreetNumber").val()) > 9999999 ){
                    $("#streetNumberValidMessage").text($("#txtStreetNumber").attr("maxValueMessage"));
                    valid = false;
                }
                else{
                    $("#streetNumberValidMessage").html("");
                }
            }

            if($("#txtApartment").val().trim() != "" && $("#txtApartment").val().length > 80 ){
                $("#apartmentValidMessage").text($("#txtApartment").attr("maxValueMessage"));
                valid = false;
            }
            else{
                $("#apartmentValidMessage").html("");
            }
            if($("#txtComments").val().trim() != "" && $("#txtComments").val().length > 80 ){
                $("#commentsValidMessage").text($("#txtComments").attr("maxValueMessage"));
                valid = false;
            }
            else{
                $("#commentsValidMessage").html("");
            }



            return valid;
        }

    </script>
</body>
</html>