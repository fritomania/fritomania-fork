@extends('layouts.marco_cliente')

@push('css')
<link href="{{asset('cliente/css/locales.css')}}" rel="stylesheet" type="text/css" media="all">
@endpush

@section('content')

    <div class="page-container" id="PageContainer">

        <main class="main-content" id="MainContent" role="main">

            <section class="page-404-layout">

                <div class="page-404-wrapper">

                    <div class="container">
                        <div class="row">

                            <div class="page-404-inner">
                                <div class="page-404-content">

                                    <div class="page-left col-sm-6">
                                        <h3 class="caption">Página no encontrada</h3>
                                        <p class="caption">¡Lo sentimos!</p>
                                        <p class="subtext">Parece que la página a la que intentabas acceder no existe. Por favor revisa la URL y intenta de nuevo</p>
                                        <a class="_btn" href="{{route('home')}}">Volver al home</a>
                                    </div>
                                    <div class="page-right col-sm-6">
                                        <img src="cliente/images/018.jpg" alt="">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </main>
    </div>


@endsection