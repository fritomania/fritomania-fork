@extends('layouts.marco_cliente')

@push('css')
<style>

</style>
@endpush
@section('content')

    <!-- Main Content -->
    <div class="page-container" id="PageContainer">
        <main class="main-content" id="MainContent" role="main">
            <section class="heading-content">
                <div class="heading-wrapper">
                    <div class="container">
                        <div class="row">
                            <div class="page-heading-inner heading-group">
                                <div class="breadcrumb-group">
                                    <h1 class="hidden">Mi Cuenta</h1>
                                    <div class="breadcrumb clearfix">
										<span itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><a href="#" title="Fast Food" itemprop="url"><span itemprop="title"><i class="fa fa-home"></i></span></a>
										</span>
                                        <span class="arrow-space"></span>
                                        <span itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
											<a href="account.html" title="My Account" itemprop="url"><span itemprop="title">Mi Cuenta</span></a>
										</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section class="account-layout">
                <div class="account-wrapper">
                    <div class="container">
                        <div class="row">
                            <div class="account-inner">
                                <div class="account-content">
                                    <div class="account-info">
                                        <div class="account-details col-sm-6">
                                            <h3 class="details-title">DETALLES DE LA CUENTA</h3>
                                            <div class="details-content">
                                                <div class="details-item name">
                                                    <span class="title">Nombre:</span>
                                                    <span class="content">{{$cliente->nombres.' '.$cliente->apellidos}}</span>

                                                </div>
                                                <div class="details-item">
                                                    <span class="title">E-mail:</span>
                                                    <a class="content" href="jin%40gmail.html">{{$cliente->email}}</a>
                                                </div>
                                                <div class="details-item">
                                                    <span class="title">Tu dirección:</span>
                                                    <address class="content">{{$cliente->direccion}}</address>
                                                </div>
                                                <div class="details-item">
                                                    <span class="title">Teléfono:</span>
                                                    <a class="content" href="tel:11111">{{$cliente->telefono}}</a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="account-link col-sm-6">
                                            <h3 class="link-title">Mi Cuenta</h3>
                                            <div class="link-content">
                                                <ul class="link-list">
                                                    <li class="item">
                                                        <a href="index-2.html">Cerrar sesión</a>
                                                    </li>
                                                    <li class="item">
                                                        <a href="addresses.html">Modificar tu dirección</a>
                                                    </li>
                                                    <li class="item">
                                                        <a href="wish-list.html">Modificar tu lista de deseos</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="account-banner col-sm-6">
                                            <a href="#"><img src="assets/images/banner_cuenta.jpg" alt=""></a>
                                        </div>
                                        <div class="account-banner col-sm-6">
                                            <a href="#"><img src="assets/images/banner_cuenta_2.jpg" alt=""></a>
                                        </div>
                                    </div>
                                    <div class="account-orders">
                                        <div class="account-orders-inner">
                                            <table>
                                                <thead>
                                                <tr>
                                                    <th class="order_number">Pedido</th>
                                                    <th class="date">Fecha</th>
                                                    <th class="payment_status">Estado del Pago</th>
                                                    <th class="fulfillment_status">Estado de la Entrega</th>
                                                    <th class="total">Total</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($cliente->ventas as $row)
                                                <tr class="odd cancelled_order">
                                                    <td class="td-name"><a href="order.html" title="">{{$row->id}}</a></td>
                                                    <td class="td-note"><span class="note">{{$row->created_at}}</span></td>
                                                    <td class="td-authorized"><span class="status_voided">No Pagado</span></td>
                                                    <td class="td-unfulfilled"><span class="status_unfulfilled">No Entregado</span></td>
                                                    <td class="td-total">
                                                        <span class="total">
                                                            <span class="money" data-currency-usd="$6,277.80">
                                                                {{$row->total}}
                                                            </span>
                                                        </span>
                                                    </td>
                                                </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </main>

    </div>

@endsection
@push('scripts')
<script>

</script>
@endpush
