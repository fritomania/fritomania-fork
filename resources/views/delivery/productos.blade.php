@extends('layouts.marco_cliente')
@include('layouts.plugins.carouser')

@push('css')
    <style>
        .img-cart{
            height: 100px;
            width: 150px;
            border: solid;
            border-radius: 5px;
            border-width: 1px;
        }

    </style>
@endpush
@section('content')

        <!-- Main Content -->
        <div class="page-container" id="PageContainer">
            <main class="main-content" id="MainContent" role="main">
                <section class="heading-content">
                    <div class="heading-wrapper">
                        <div class="container">
                            <div class="row">
                                <div class="page-heading-inner heading-group">
                                    <div class="breadcrumb-group">
                                        <h1 class="hidden">Productos</h1>
                                        <div class="breadcrumb clearfix">
                                                <span itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><a href="#" title="Fast Food" itemprop="url"><span itemprop="title"><i class="fa fa-home"></i></span></a>
                                                </span>
                                            <span class="arrow-space"></span>
                                            <span itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
                                                    <a href="#" title="Collections" itemprop="url"><span itemprop="title">Productos</span></a>
                                                </span>
                                        </div>
                                        @isset($tienda)
                                        <span class="pull-right">{{$tienda->nombre}}</span>
                                            @endisset
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

                <section class="collection-layout">
                    <div class="collection-wrapper">
                        <div class="container">
                            @include('flash::message')
                            <div class="row">
                                <div id="shopify-section-collection-template" class="shopify-section">
                                    <div class="collection-inner collection-sidebar">
                                        <!-- Tags loading -->
                                        <div id="tags-load" style="display:none;"><i class="fa fa-spinner fa-pulse fa-2x"></i></div>
                                        <div id="collection">
                                            <div class="collection-content">

                                                @include('delivery.partials.productos.sidebar')

                                                <div class="collection-mainarea  col-sm-9 clearfix">


                                                    @forelse($articulos as $fila)
                                                    <div class="collection-items clearfix">

                                                        <div class="home-product-inner">
                                                            <div class="home-product-content">
                                                                @foreach($fila as $item)
                                                                <div class="content_product col-sm-3 not-animated" data-animate="fadeInUp"
                                                                     data-delay="0">
                                                                    <div class="row-container product list-unstyled clearfix product-circle">
                                                                        <div class="row-left">
                                                                            <a href="#" class="hoverBorder container_item" @click="add('{{ $item->id }}')">
                                                                                <div class="hoverBorderWrapper">
                                                                                    <img src="{{asset($item->imagen)}}" class="img-responsive front" alt="Juice Ice Tea">
                                                                                    <div class="mask"></div>
                                                                                </div>
                                                                            </a>
                                                                            <div class="hover-mask">
                                                                                <div class="group-mask">
                                                                                    <div class="inner-mask">
                                                                                        <div class="group-actionbutton">
                                                                                            <ul class="quickview-wishlist-wrapper">
                                                                                                <li class="wishlist">
                                                                                                    <a title="Añadir a Lista de Deseos" class="wishlist wishlist-juice-ice-tea" data-wishlisthandle="juice-ice-tea"><span class="cs-icon icon-heart"></span></a>
                                                                                                </li>
                                                                                                <li class="quickview hidden-xs hidden-sm">
                                                                                                    <div class="product-ajax-cart">
                                                                                                        <span class="overlay_mask"></span>
                                                                                                        <div data-handle="juice-ice-tea" data-target="#quick-shop-modal" class="quick_shop" data-toggle="modal">
                                                                                                            <a class=""><span class="cs-icon icon-eye"></span></a>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </li>
                                                                                                <li class="compare">
                                                                                                    <a title="Comparar" class="compare compare-juice-ice-tea" data-comparehandle="juice-ice-tea"><span class="cs-icon icon-retweet2"></span></a>
                                                                                                </li>
                                                                                            </ul>
                                                                                        </div>

                                                                                        <div class="effect-ajax-cart">
                                                                                            <input type="hidden" name="quantity" value="1">
                                                                                            <button type="button" class="_btn add-to-cart" title="Añadir al Carro"
                                                                                                @click="add('{{ $item->id }}')">
                                                                                                Pedir
                                                                                            </button>
                                                                                        </div>
                                                                                    </div>
                                                                                    <!--inner-mask-->
                                                                                </div>
                                                                                <!--Group mask-->
                                                                            </div>
                                                                            <!-- Si la diferencia en días de la fecha actual con la fecha de creación del item es menor o igual a 5 Días -->
                                                                            @if($item->created_at->diffInDays(\Carbon\Carbon::now()) <= config('app.dias_producto_es_nuevo',30))
                                                                            <div class="product-label">
                                                                                <div class="label-element new-label">
                                                                                    <span>Nuevo</span>
                                                                                </div>
                                                                            </div>
                                                                            @endif
                                                                        </div>
                                                                        <div class="row-right animMix">
                                                                            <div class="rating-star">
                                                                <span class="spr-badge" data-rating="0.0"><span class="spr-starrating spr-badge-starrating"><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i></span><span class="spr-badge-caption">No reviews</span>
                                                                </span>
                                                                            </div>
                                                                            <div class="product-title">
                                                                                <a class="title-5" href="#" @click="add('{{ $item->id }}')">
                                                                                    {{$item->nombre}}
                                                                                </a>
                                                                            </div>
                                                                            <div class="product-price">
                                                                                <span class="price">
                                                                                    <span class="money" data-currency-usd="$1500.00">${{nfp($item->precio_venta)}}</span>
                                                                                </span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                @endforeach
                                                            </div>

                                                        </div>
                                                    </div>
                                                    @empty
                                                        <h3 class="text-center">No hay articulos en esta categoría</h3>
                                                    @endforelse

                                                    <!-- Paginacion articulos -->
                                                    {{--<div class="collection-bottom-toolbar">--}}
                                                        {{--<div class="product-counter col-sm-6">--}}
                                                            {{--Items 1 to 18 of 20 total--}}
                                                        {{--</div>--}}
                                                        {{--<div class="product-pagination col-sm-6">--}}
                                                            {{--<div class="pagination_group">--}}
                                                                {{--<ul class="pagination pagination-collection">--}}
                                                                    {{--<li class="prev"><a class="disabled" href="collections.html">Atrás</a></li>--}}
                                                                    {{--<li class="active"><a href="collections.html">1</a></li>--}}
                                                                    {{--<li><a href="collections.html">2</a></li>--}}
                                                                    {{--<li class="next"><a href="collections.html">Adelante</a></li>--}}
                                                                {{--</ul>--}}
                                                            {{--</div>--}}
                                                        {{--</div>--}}
                                                    {{--</div>--}}
                                                    <!-- /Paginacion articulos -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </main>

            @include('delivery.modal_car',['tienda' => $tienda])
        </div>

@endsection

@push('scripts')
    @include('layouts.vue')
    @include('layouts.axios')
    <script>
        new Vue({
            el: '#root',
            created() {
                this.getDatos();
            },
            data: {
                url : "{{route('cart.index')}}",
                urladd : "{{route('add.cart')}}",
                urldel : "{{route('remove.cart',0)}}",
                items: [],
                loading: false,
                idEliminando: ''
            },
            methods: {
                nf(cant){
                    return numf(cant);
                },
                getDatos(){
                    console.log('Metodo Get Datos');
                    axios.get(this.url).then(response =>{
                        this.items = response.data;
                        this.loading= false;
                        this.idEliminando= '';
                    }).catch(error =>{
                        console.log(error.response);
                    })
                },
                add(id){
                    console.log('Agregar producto',id);
                    var datos = {
                        'id' : id,
                        'items' : this.items
                    };

                    axios.post(this.urladd,datos).then(response =>{
                        console.log(response.data);
                        $("#modalCart").modal('show');
                        this.getDatos();
                    }).catch(error =>{
                        console.log(error.response);
                    })
                },
                eliminar(id){
                    this.loading=true;
                    this.idEliminando= id;

                    console.log('Remover producto',id);

                    var url = this.urldel.replace('0',id);
                    console.log(url);

                    axios.get(url).then(response =>{
//                        console.log(response.data);
                        $("#modalCart").modal('show');
                        this.getDatos();
                    }).catch(error =>{
                        this.idEliminando= '';
//                        console.log(error.response);
                    })
                }
            },
            computed: {
                cantidad_articulos(){
                    var cant=0;
                    $.each(this.items,function (index,item) {
                        cant=cant+parseInt(item.qty);
                    });

                    return cant;

                },
                total(){
                    var total=0;
                    $.each(this.items,function (index,item) {
                        var cantidad = isNaN(parseFloat(item.qty)) ? 0 : parseFloat(item.qty);
                        var precio = isNaN(parseFloat(item.precio_venta)) ? 0 : parseFloat(item.precio_venta);

                        total += cantidad*precio;
                    });

                    return total;
                },
            },
            watch: {
                items: function (val) {
                }
            }
        });
    </script>
@endpush
