
        <!DOCTYPE html>
<!--[if IE 8 ]>    <html lang="en" class="no-js ie8"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en" class="no-js"> <!--<![endif]-->



<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="canonical" href="http://demo.designshopify.com/" />
    <meta name="theme-color" content="#7796A8">
    <meta name="description" content="" />
    <title>
        Producto
    </title>

    <link href="https://fonts.googleapis.com/css?family=Josefin+Sans:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Playball:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Bitter:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

    <link href="cliente/stylesheets/bootstrap.min.css" rel="stylesheet" type="text/css" media="all">
    <link href="../../maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" media="all">
    <link href="cliente/stylesheets/fonts.googleapis.css" rel="stylesheet" type="text/css" media="all">
    <link href="cliente/stylesheets/font-awesome.min.css" rel="stylesheet" type="text/css" media="all">
    <link href="cliente/stylesheets/icon-font.min.css" rel="stylesheet" type="text/css" media="all">
    <link href="cliente/stylesheets/social-buttons.css" rel="stylesheet" type="text/css" media="all">
    <link href="cliente/stylesheets/cs.styles.css" rel="stylesheet" type="text/css" media="all">
    <link href="cliente/stylesheets/font-icon.css" rel="stylesheet" type="text/css" media="all">
    <link href="cliente/stylesheets/owl.carousel.min.css" rel="stylesheet" type="text/css" media="all">
    <link href="cliente/stylesheets/spr.css" rel="stylesheet" type="text/css" media="all">
    <link href="cliente/stylesheets/slideshow-fade.css" rel="stylesheet" type="text/css" media="all">
    <link href="cliente/stylesheets/cs.animate.css" rel="stylesheet" type="text/css" media="all">
    <link href="cliente/stylesheets/themepunch.revolution.css" rel="stylesheet" type="text/css" media="all">
    <link href="cliente/stylesheets/jquery.fancybox.css" rel="stylesheet" type="text/css" media="all">

    <script type="text/javascript" src="cliente/javascripts/jquery.min.js"></script>
    <script type="text/javascript" src="cliente/javascripts/classie.js"></script>
    <script type="text/javascript" src="cliente/javascripts/application-appear.js"></script>
    <script type="text/javascript" src="cliente/javascripts/jquery.themepunch.plugins.min.js"></script>
    <script type="text/javascript" src="cliente/javascripts/jquery.themepunch.revolution.min.js"></script>
    <script type="text/javascript" src="cliente/javascripts/cs.script.js"></script>
    <script type="text/javascript" src="cliente/javascripts/jquery.currencies.min.js"></script>
    <script type="text/javascript" src="cliente/javascripts/jquery.zoom.min.js"></script>
    <script type="text/javascript" src="cliente/javascripts/linkOptionSelectors.js"></script>
    <script type="text/javascript" src="cliente/javascripts/owl.carousel.min.js"></script>
    <script type="text/javascript" src="cliente/javascripts/scripts.js"></script>
    <script type="text/javascript" src="cliente/javascripts/social-buttons.js"></script>
    <script type="text/javascript" src="cliente/javascripts/bootstrap.min.js"></script>
    <script type="text/javascript" src="cliente/javascripts/jquery.touchSwipe.min.js"></script>
    <script type="text/javascript" src="cliente/javascripts/jquery.fancybox.js"></script>

</head>
<body>
@include('layouts.partials_cliente.mainheader')
<div class="page-container" id="PageContainer">
    <main class="main-content" id="MainContent" role="main">
        <section class="heading-content">
            <div class="heading-wrapper">
                <div class="container">
                    <div class="row">
                        <div class="page-heading-inner heading-group">
                            <div class="breadcrumb-group">
                                <h1 class="hidden">Producto</h1>
                                <div class="breadcrumb clearfix">
										<span itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><a href="index-2.html" title="Fast Food" itemprop="url"><span itemprop="title"><i class="fa fa-home"></i></span></a>
										</span>
                                    <span class="arrow-space"></span>
                                    <span itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
											<a href="product.html" title="Products" itemprop="url"><span itemprop="title">Producto</span></a>
										</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="product-detail-layout">
            <div class="product-detail-wrapper">
                <div class="container">
                    <div class="row">
                        <div id="shopify-section-product-template" class="shopify-section">
                            <div class="product-detail-inner" itemscope="" itemtype="http://schema.org/Product">
                                <meta itemprop="name" content="Extra Crispy">
                                <meta itemprop="url" content="./product.html">
                                <meta itemprop="image" content="./assets/images/21CocaNormal.jpg">
                                <div class="product-detail-content col-sm-9">
                                    <div id="product" class="extra-crispy-1 detail-content">
                                        <div class="col-md-12 info-detail-pro clearfix">
                                            <div class="col-md-6" id="product-image">
                                                <div id="featuted-image" class="image featured" style="position: static; overflow: visible;">
                                                    <img src="assets/images/21CocaNormal.jpg" class="zoomImg" alt="">
                                                </div>
                                                <div id="featuted-image-mobile" class="image featured-mobile">
                                                    <div class="image-item">
                                                        <a href="#" class="thumbnail" data-toggle="modal" data-target="#lightbox" >
                                                            <img src="assets/images/21CocaNormal.jpg" alt="Extra Crispy" data-item="1" />
                                                        </a>
                                                    </div>
                                                    <div class="image-item">
                                                        <a href="#" class="thumbnail" data-toggle="modal" data-target="#lightbox" >
                                                            <img src="assets/images/product_11.jpg" alt="Extra Crispy" data-item="2" />
                                                        </a>
                                                    </div>
                                                    <div class="image-item">
                                                        <a href="#" class="thumbnail" data-toggle="modal" data-target="#lightbox" >
                                                            <img src="assets/images/product_12.jpg" alt="Extra Crispy" data-item="3" />
                                                        </a>
                                                    </div>
                                                    <div class="image-item">
                                                        <a href="#" class="thumbnail" data-toggle="modal" data-target="#lightbox" >
                                                            <img src="assets/images/product_14.jpg" alt="Extra Crispy" data-item="4" />
                                                        </a>
                                                    </div>
                                                </div>
                                                <div id="gallery-images" class="thumbs clearfix gallery-images-layout">
                                                    <div class="gallery-images-inner">
                                                        <div class="show-image-load show-load-detail" style="display: none;">
                                                            <div class="show-image-load-inner">
                                                                <i class="fa fa-spinner fa-pulse fa-2x"></i>
                                                            </div>
                                                        </div>
                                                        <div class="slider-3itemsc vertical-image-content">
                                                            <div class="image-vertical image active">
                                                                <a href="assets/images/21CocaNormal.jpg" class="cloud-zoom-gallery">
                                                                    <img src="assets/images/21CocaNormal.jpg" alt="Extra Crispy">
                                                                </a>
                                                            </div>
                                                            <div class="image-vertical image">
                                                                <a href="assets/images/coca2.jpg" class="cloud-zoom-gallery">
                                                                    <img src="assets/images/coca2.jpg" alt="Extra Crispy">
                                                                </a>
                                                            </div>
                                                            <div class="image-vertical image">
                                                                <a href="assets/images/coca3.jpg" class="cloud-zoom-gallery">
                                                                    <img src="assets/images/coca3.jpg" alt="Extra Crispy">
                                                                </a>
                                                            </div>
                                                            <div class="image-vertical image">
                                                                <a href="assets/images/coca4.jpg" class="cloud-zoom-gallery">
                                                                    <img src="assets/images/coca4.jpg" alt="Extra Crispy">
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6" id="product-information">
                                                <h1 itemprop="name" class="title">COCA COLA 1.5 Lts</h1>
                                                <div class="rating-content">
                                                    <div class="rating-description">
															<span class="spr-badge" data-rating="5.0"><span class="spr-starrating spr-badge-starrating"><i class="spr-icon spr-icon-star" style=""></i><i class="spr-icon spr-icon-star" style=""></i><i class="spr-icon spr-icon-star" style=""></i><i class="spr-icon spr-icon-star" style=""></i><i class="spr-icon spr-icon-star" style=""></i></span><span class="spr-badge-caption">1 reseña</span>
															</span>
                                                        <span class="review-link"><a href="#review">Escribir una reseña</a></span>
                                                    </div>
                                                </div>
                                                <script>
                                                    $('.review-link a').click(function(e) {
                                                        e.preventDefault();
                                                        $('html, body').animate({
                                                            scrollTop: $("#tabs-information").offset().top - 120
                                                        }, 800);
                                                        $('.nav-tabs a[href="#review"]').tab('show');
                                                        return false;
                                                    });
                                                </script>
                                                <form id="add-item-form" action="http://demo.designshopify.com/html_fastfood/cart.html" method="post" class="variants">
                                                    <div class="product-options " itemprop="offers" itemscope="" itemtype="http://schema.org/Offer">
                                                        <meta itemprop="priceCurrency" content="USD">
                                                        <link itemprop="availability" href="http://schema.org/InStock">
                                                        <div class="product-type">
                                                            <div class="select clearfix">
                                                                <div class="selector-wrapper variant-wrapper-size">
                                                                    <label for="product-select-option-0">Tamaño</label>
                                                                    <select class="single-option-selector" data-option="option1" id="product-select-option-0"><option value="Small">Pequeño</option><option value="Medium">Mediano</option><option value="Large">Grande</option></select>
                                                                </div>
                                                                <div class="selector-wrapper variant-wrapper-topping">
                                                                    <label for="product-select-option-2">Variaciones</label>
                                                                    <select class="single-option-selector" data-option="option3" id="quick-shop-variants-213223800871-option-2"><option value="Black Bottom Cupcakes">Black Bottom Cupcakes</option><option value="Blue-Ribbon Butter Cake">Blue-Ribbon Butter Cake</option><option value="Cheesy Mcplain">Cheesy Mcplain</option><option value="Chunky Apple Cake">Chunky Apple Cake</option></select>
                                                                </div>
                                                            </div>
                                                            <div class="swatch-variant swatch color clearfix" data-option-index="1">
                                                                <label>Color</label>
                                                                <div data-value="Black" class="swatch-element color black available active">
                                                                    <input id="swatch-1-black" type="radio" name="option-1" value="Black">
                                                                    <label data-toggle="tooltip" data-placement="top" data-original-title="Black" for="swatch-1-black" style="background-color: black; border-color: black; background-image: url(assets/images/black.png)">
                                                                        <img class="crossed-out" src="assets/images/soldout.png" alt="">
                                                                    </label>
                                                                </div>
                                                                <div data-value="Red" class="swatch-element color red available">
                                                                    <input id="swatch-1-red" type="radio" name="option-1" value="Red">
                                                                    <label data-toggle="tooltip" data-placement="top" data-original-title="Red" for="swatch-1-red" style="background-color: red; border-color: red; background-image: url(assets/images/red.png)">
                                                                        <img class="crossed-out" src="assets/images/soldout.png" alt="">
                                                                    </label>
                                                                </div>
                                                                <div data-value="Blue" class="swatch-element color blue available">
                                                                    <input id="swatch-1-blue" type="radio" name="option-1" value="Blue">
                                                                    <label data-toggle="tooltip" data-placement="top" data-original-title="Blue" for="swatch-1-blue" style="background-color: blue; border-color: blue; background-image: url(assets/images/blue.png)">
                                                                        <img class="crossed-out" src="assets/images/soldout.png" alt="">
                                                                    </label>
                                                                </div>
                                                                <div data-value="yellow" class="swatch-element color yellow available soldout" style="display: none;">
                                                                    <input id="swatch-1-yellow" type="radio" name="option-1" value="yellow" disabled="disabled">
                                                                    <label data-toggle="tooltip" data-placement="top" data-original-title="Yellow" for="swatch-1-yellow" style="background-color: yellow; border-color: yellow; background-image: url(assets/images/yellow.png)">
                                                                        <img class="crossed-out" src="assets/images/soldout.png" alt="">
                                                                    </label>
                                                                </div>
                                                                <script>
                                                                    $(function() {
                                                                        $('.swatch-element').hover(
                                                                            function() {
                                                                                $(this).addClass("hovered");
                                                                            },
                                                                            function() {
                                                                                $(this).removeClass("hovered");
                                                                            });

                                                                        $(".swatch-element").click(function() {
                                                                            if (!$(this).hasClass('active')) {
                                                                                $(this).parent().find(".swatch-element.active").removeClass("active");
                                                                                $(this).addClass("active");
                                                                            }
                                                                        });
                                                                    });
                                                                </script>
                                                            </div>
                                                        </div>
                                                        <div class="clearfix">
                                                            <div class="quantity-wrapper">
                                                                <label class="wrapper-title">Cant</label>
                                                                <div class="wrapper">
                                                                    <input id="quantity" type="text" name="quantity" value="1" maxlength="5" size="5" class="item-quantity">
                                                                    <div class="qty-btn-vertical">
																			<span class="qty-down fa fa-chevron-down" title="Decrease" data-src="#quantity">
																			</span>
                                                                        <span class="qty-up fa fa-chevron-up" title="Increase" data-src="#quantity">
																			</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="product-price">
                                                            <meta itemprop="price" content="10.00">
                                                            <h2 class="price" id="price-preview"><span class="money" data-currency-usd="$10.00" data-currency="USD">$1500.00</span></h2>
                                                        </div>
                                                        <div class="purchase-section multiple">
                                                            <div class="purchase">
                                                                <button id="add-to-cart" onclick="change_qs_quantity('');" class="_btn add-to-cart" type="submit" name="add"><span><i class="cs-icon icon-cart"></i>Hacer Pedido</span></button>
                                                                <div id="cart-animation" style="display:none">1</div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                                <div class="comWish-content">
                                                    <a title="comparar" class="_compareWishlist compare compare-extra-crispy-1" data-comparehandle="extra-crispy-1">
                                                        <span class="icon-small icon-small-retweet"></span>
                                                        <span class="_compareWishlist-text">Comparar</span>
                                                    </a>
                                                    <a title="agregar a lista de deseos" class="wishlist-extra-crispy-1" data-wishlisthandle="extra-crispy-1" href="http://demo.designshopify.com/pages/wish-list">
                                                        <span class="icon-small icon-small-heart"></span>
                                                        <span class="_compareWishlist-text">Lista de Deseos</span>
                                                    </a>
                                                    <a title="Send email" class="send-email" href="mailto:contac@yourcompany.com">
                                                        <span class="icon-small icon-small-email"></span>
                                                        <span class="_compareWishlist-text">Email</span>
                                                    </a>
                                                </div>
                                                <div class="supports-fontface">
                                                    <span class="social-title">Compartir</span>
                                                    <div class="social-sharing is-clean" data-permalink="https://cs-fastfood.myshopify.com/products/extra-crispy-1">
                                                        <a target="_blank" href="http://www.facebook.com/sharer.php?u=https://cs-fastfood.myshopify.com/products/extra-crispy-1" class="share-facebook">
                                                            <span class="fa fa-facebook"></span>
                                                        </a>
                                                        <a target="_blank" href="http://twitter.com/share?text=Extra%20Crispy&amp;url=https://cs-fastfood.myshopify.com/products/extra-crispy-1" class="share-twitter">
                                                            <span class="fa fa-twitter"></span>
                                                        </a>
                                                        <a target="_blank" href="../../pinterest.com/pin/create/button/indexb5c7.html?url=https://cs-fastfood.myshopify.com/products/extra-crispy-1&amp;media=http://cdn.shopify.com/s/files/1/2487/3424/products/13_1024x1024.jpg?v=1508991313&amp;description=Extra%20Crispy" class="share-pinterest">
                                                            <span class="fa fa-pinterest"></span>
                                                        </a>
                                                        <a target="_blank" href="http://plus.google.com/share?url=https://cs-fastfood.myshopify.com/products/extra-crispy-1" class="share-google">
                                                            <!-- Cannot get Google+ share count with JS yet -->
                                                            <span class="fa fa-google-plus"></span>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="may-like-products col-sm-12">
                                            <div class="sub-title">
                                                <h2>También te puede gustar</h2>
                                            </div>
                                            <div class="may-like-content-inner">
                                                <div class="may-like-content">
                                                    <div class="content_product">
                                                        <div class="row-container product list-unstyled clearfix product-circle">
                                                            <div class="row-left">
                                                                <a href="product.html" class="hoverBorder container_item">
                                                                    <div class="hoverBorderWrapper">
                                                                        <img src="assets/images/coca-cola-tradicional-en-lata-350-ml.jpg" class="img-responsive front" alt="Juice Ice Tea">
                                                                        <div class="mask"></div>
                                                                    </div>
                                                                </a>
                                                                <div class="hover-mask">
                                                                    <div class="group-mask">
                                                                        <div class="inner-mask">
                                                                            <div class="group-actionbutton">
                                                                                <ul class="quickview-wishlist-wrapper">
                                                                                    <li class="wishlist">
                                                                                        <a title="agregar a lista de deseos" class="wishlist wishlist-juice-ice-tea" data-wishlisthandle="juice-ice-tea"><span class="cs-icon icon-heart"></span></a>
                                                                                    </li>
                                                                                    <li class="quickview hidden-xs hidden-sm">
                                                                                        <div class="product-ajax-cart">
                                                                                            <span class="overlay_mask"></span>
                                                                                            <div data-handle="juice-ice-tea" data-target="#quick-shop-modal" class="quick_shop" data-toggle="modal">
                                                                                                <a class=""><span class="cs-icon icon-eye"></span></a>
                                                                                            </div>
                                                                                        </div>
                                                                                    </li>
                                                                                    <li class="compare">
                                                                                        <a title="comparar" class="compare compare-juice-ice-tea" data-comparehandle="juice-ice-tea"><span class="cs-icon icon-retweet2"></span></a>
                                                                                    </li>
                                                                                </ul>
                                                                            </div>
                                                                            <form action="http://demo.designshopify.com/html_fastfood/cart.html" method="post">
                                                                                <div class="effect-ajax-cart">
                                                                                    <input type="hidden" name="quantity" value="1">
                                                                                    <button class="_btn add-to-cart" data-parent=".parent-fly" type="submit" name="add" title="hacer pedido">hacer pedido</button>
                                                                                </div>
                                                                            </form>
                                                                        </div>
                                                                        <!--inner-mask-->
                                                                    </div>
                                                                    <!--Group mask-->
                                                                </div>
                                                                <div class="product-label">
                                                                    <div class="label-element new-label">
                                                                        <span>Nuevo</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row-right animMix">
                                                                <div class="rating-star">
																		<span class="spr-badge" data-rating="0.0"><span class="spr-starrating spr-badge-starrating"><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i></span><span class="spr-badge-caption">No reviews</span>
																		</span>
                                                                </div>
                                                                <div class="product-title"><a class="title-5" href="product.html">COCA COLA LATA 350 CC</a></div>
                                                                <div class="product-price">
																		<span class="price">
																			<span class="money" data-currency-usd="$20.00">$450.00</span>
																		</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="content_product">
                                                        <div class="row-container product list-unstyled clearfix product-circle">
                                                            <div class="row-left">
                                                                <a href="product.html" class="hoverBorder container_item">
                                                                    <div class="hoverBorderWrapper">
                                                                        <img src="assets/images/coca-cola-ligth-en-lata-350ml.jpg" class="img-responsive front" alt="Coke">
                                                                        <div class="mask"></div>
                                                                    </div>
                                                                </a>
                                                                <div class="hover-mask">
                                                                    <div class="group-mask">
                                                                        <div class="inner-mask">
                                                                            <div class="group-actionbutton">
                                                                                <ul class="quickview-wishlist-wrapper">
                                                                                    <li class="wishlist">
                                                                                        <a title="agregar a lista de deseos" class="wishlist wishlist-coke" data-wishlisthandle="coke"><span class="cs-icon icon-heart"></span></a>
                                                                                    </li>
                                                                                    <li class="quickview hidden-xs hidden-sm">
                                                                                        <div class="product-ajax-cart">
                                                                                            <span class="overlay_mask"></span>
                                                                                            <div data-handle="coke" data-target="#quick-shop-modal" class="quick_shop" data-toggle="modal">
                                                                                                <a class=""><span class="cs-icon icon-eye"></span></a>
                                                                                            </div>
                                                                                        </div>
                                                                                    </li>
                                                                                    <li class="compare">
                                                                                        <a title="comparar" class="compare compare-coke" data-comparehandle="coke"><span class="cs-icon icon-retweet2"></span></a>
                                                                                    </li>
                                                                                </ul>
                                                                            </div>
                                                                            <form action="http://demo.designshopify.com/html_fastfood/cart.html" method="post">
                                                                                <div class="effect-ajax-cart">
                                                                                    <button class="_btn select-option" type="button" onclick="window.location='product.html';" title="Select Option">hacer pedido</button>
                                                                                </div>
                                                                            </form>
                                                                        </div>
                                                                        <!--inner-mask-->
                                                                    </div>
                                                                    <!--Group mask-->
                                                                </div>
                                                                <div class="product-label">
                                                                    <div class="label-element deal-label">
                                                                        <span>33%</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row-right animMix">
                                                                <div class="rating-star">
																		<span class="spr-badge" data-rating="5.0"><span class="spr-starrating spr-badge-starrating"><i class="spr-icon spr-icon-star" style=""></i><i class="spr-icon spr-icon-star" style=""></i><i class="spr-icon spr-icon-star" style=""></i><i class="spr-icon spr-icon-star" style=""></i><i class="spr-icon spr-icon-star" style=""></i></span><span class="spr-badge-caption">1 review</span>
																		</span>
                                                                </div>
                                                                <div class="product-title"><a class="title-5" href="product.html">COCA COLA ZERO LATA 350 CC</a></div>
                                                                <div class="product-price">
                                                                    <span class="price_sale"><span class="money" data-currency-usd="$10.00">$450.00</span></span>
                                                                    <del class="price_compare"> <span class="money" data-currency-usd="$15.00">$500.00</span></del>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="content_product">
                                                        <div class="row-container product list-unstyled clearfix product-circle">
                                                            <div class="row-left">
                                                                <a href="product.html" class="hoverBorder container_item">
                                                                    <div class="hoverBorderWrapper">
                                                                        <img src="assets/images/tequenos.jpeg" class="img-responsive front" alt="Juice Ice Tea">
                                                                        <div class="mask"></div>
                                                                    </div>
                                                                </a>
                                                                <div class="hover-mask">
                                                                    <div class="group-mask">
                                                                        <div class="inner-mask">
                                                                            <div class="group-actionbutton">
                                                                                <ul class="quickview-wishlist-wrapper">
                                                                                    <li class="wishlist">
                                                                                        <a title="agregar a lista de deseos" class="wishlist wishlist-juice-ice-tea" data-wishlisthandle="juice-ice-tea"><span class="cs-icon icon-heart"></span></a>
                                                                                    </li>
                                                                                    <li class="quickview hidden-xs hidden-sm">
                                                                                        <div class="product-ajax-cart">
                                                                                            <span class="overlay_mask"></span>
                                                                                            <div data-handle="juice-ice-tea" data-target="#quick-shop-modal" class="quick_shop" data-toggle="modal">
                                                                                                <a class=""><span class="cs-icon icon-eye"></span></a>
                                                                                            </div>
                                                                                        </div>
                                                                                    </li>
                                                                                    <li class="compare">
                                                                                        <a title="comparar" class="compare compare-juice-ice-tea" data-comparehandle="juice-ice-tea"><span class="cs-icon icon-retweet2"></span></a>
                                                                                    </li>
                                                                                </ul>
                                                                            </div>
                                                                            <form action="http://demo.designshopify.com/html_fastfood/cart.html" method="post">
                                                                                <div class="effect-ajax-cart">
                                                                                    <input type="hidden" name="quantity" value="1">
                                                                                    <button class="_btn add-to-cart" data-parent=".parent-fly" type="submit" name="add" title="hacer pedido">hacer pedido</button>
                                                                                </div>
                                                                            </form>
                                                                        </div>
                                                                        <!--inner-mask-->
                                                                    </div>
                                                                    <!--Group mask-->
                                                                </div>
                                                                <div class="product-label">
                                                                    <div class="label-element hotting-label">
                                                                        <span>Popular</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row-right animMix">
                                                                <div class="rating-star">
																		<span class="spr-badge" data-rating="0.0"><span class="spr-starrating spr-badge-starrating"><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i></span><span class="spr-badge-caption">No reviews</span>
																		</span>
                                                                </div>
                                                                <div class="product-title"><a class="title-5" href="product.html">COMBO 100 TEQUEÑOS</a></div>
                                                                <div class="product-price">
																		<span class="price">
																			<span class="money" data-currency-usd="$20.00">$1000.00</span>
																		</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="content_product">
                                                        <div class="row-container product list-unstyled clearfix product-circle">
                                                            <div class="row-left">
                                                                <a href="product.html" class="hoverBorder container_item">
                                                                    <div class="hoverBorderWrapper">
                                                                        <img src="assets/images/17.jpeg" class="img-responsive front" alt="Juice Ice Tea">
                                                                        <div class="mask"></div>
                                                                    </div>
                                                                </a>
                                                                <div class="hover-mask">
                                                                    <div class="group-mask">
                                                                        <div class="inner-mask">
                                                                            <div class="group-actionbutton">
                                                                                <ul class="quickview-wishlist-wrapper">
                                                                                    <li class="wishlist">
                                                                                        <a title="agregar a lista de deseos" class="wishlist wishlist-juice-ice-tea" data-wishlisthandle="juice-ice-tea"><span class="cs-icon icon-heart"></span></a>
                                                                                    </li>
                                                                                    <li class="quickview hidden-xs hidden-sm">
                                                                                        <div class="product-ajax-cart">
                                                                                            <span class="overlay_mask"></span>
                                                                                            <div data-handle="juice-ice-tea" data-target="#quick-shop-modal" class="quick_shop" data-toggle="modal">
                                                                                                <a class=""><span class="cs-icon icon-eye"></span></a>
                                                                                            </div>
                                                                                        </div>
                                                                                    </li>
                                                                                    <li class="compare">
                                                                                        <a title="comparar" class="compare compare-juice-ice-tea" data-comparehandle="juice-ice-tea"><span class="cs-icon icon-retweet2"></span></a>
                                                                                    </li>
                                                                                </ul>
                                                                            </div>
                                                                            <form action="http://demo.designshopify.com/html_fastfood/cart.html" method="post">
                                                                                <div class="effect-ajax-cart">
                                                                                    <input type="hidden" name="quantity" value="1">
                                                                                    <button class="_btn add-to-cart" data-parent=".parent-fly" type="submit" name="add" title="hacer pedido">hacer pedido</button>
                                                                                </div>
                                                                            </form>
                                                                        </div>
                                                                        <!--inner-mask-->
                                                                    </div>
                                                                    <!--Group mask-->
                                                                </div>
                                                            </div>
                                                            <div class="row-right animMix">
                                                                <div class="rating-star">
																		<span class="spr-badge" data-rating="0.0"><span class="spr-starrating spr-badge-starrating"><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i></span><span class="spr-badge-caption">No reviews</span>
																		</span>
                                                                </div>
                                                                <div class="product-title"><a class="title-5" href="product.html">COMBO FAMILIAR</a></div>
                                                                <div class="product-price">
																		<span class="price">
																			<span class="money" data-currency-usd="$20.00">$6500.00</span>
																		</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="tabs-information" class="col-md-12 tabs-information">
                                            <div class="col-md-12 tabs-title">
                                                <ul class="nav nav-tabs tabs-left sideways">
                                                    <li class="active"><a href="#desc" data-toggle="tab">Descripción</a></li>
                                                    <li><a href="#review" data-toggle="tab">Reseñas</a></li>
                                                    <li><a href="#shipping" data-toggle="tab">Detalles de Envío</a></li>
                                                    <li><a href="#payment" data-toggle="tab">Formas de Pago</a></li>
                                                </ul>
                                            </div>
                                            <div class="col-md-12 tabs-content">
                                                <div class="tab-content">
                                                    <div class="tab-pane active" id="desc">
                                                        <p>Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora torquent per conubia nostra. Version of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio.</p>
                                                        <ul class="angle-right">
                                                            <li>Version of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet.</li>
                                                            <li>Aenean sollicitudin, lorem quis bibendum auctor</li>
                                                            <li>Nisi elit consequat ipsum, nec sagittis sem nibh id elit</li>
                                                            <li>Duis sed odio sit amet nibh vulputate cursus a sit amet mauris</li>
                                                        </ul>
                                                    </div>
                                                    <div class="tab-pane fade " id="review">
                                                        <div id="customer_review">
                                                            <div class="preview_content">
                                                                <div id="shopify-product-reviews" data-id="6537875078">
                                                                    <div class="spr-container">
                                                                        <div class="spr-header">
                                                                            <h2 class="spr-header-title">Customer Reviews</h2>
                                                                            <div class="spr-summary" itemscope="" itemtype="http://data-vocabulary.org/Review-aggregate">
                                                                                <meta itemprop="itemreviewed" content="Chanel, the cheetah">
                                                                                <meta itemprop="votes" content="1">
                                                                                <span itemprop="rating" itemscope="" itemtype="http://data-vocabulary.org/Rating" class="spr-starrating spr-summary-starrating">
																						<meta itemprop="average" content="5.0">
																						<meta itemprop="best" content="5">
																						<meta itemprop="worst" content="1">
																						<i class="spr-icon spr-icon-star" style=""></i><i class="spr-icon spr-icon-star" style=""></i><i class="spr-icon spr-icon-star" style=""></i><i class="spr-icon spr-icon-star" style=""></i><i class="spr-icon spr-icon-star" style=""></i>
																					</span>
                                                                                <span class="spr-summary-caption">
																						<span class="spr-summary-actions-togglereviews">Basado en 1 reseña</span>
																					</span>
                                                                                <span class="spr-summary-actions">
																						<a href="#" class="spr-summary-actions-newreview" onclick="active_review_form();return false">Escribir una reseña</a>
																					</span>
                                                                            </div>
                                                                        </div>
                                                                        <div class="spr-content">
                                                                            <div class="spr-form" id="form_6537875078" style="display: none;">
                                                                                <form method="post" action="http://demo.designshopify.com/html_fastfood/product.html" id="new-review-form_6537875078" class="new-review-form" onsubmit="SPR.submitForm(this);return false;"><input type="hidden" name="review[rating]"><input type="hidden" name="product_id" value="6537875078">
                                                                                    <h3 class="spr-form-title">Write a review</h3>
                                                                                    <fieldset class="spr-form-contact">
                                                                                        <div class="spr-form-contact-name">
                                                                                            <label class="spr-form-label" for="review_author_6537875078">Name</label>
                                                                                            <input class="spr-form-input spr-form-input-text " id="review_author_6537875078" type="text" name="review[author]" value="" placeholder="Enter your name">
                                                                                        </div>
                                                                                        <div class="spr-form-contact-email">
                                                                                            <label class="spr-form-label" for="review_email_6537875078">Email</label>
                                                                                            <input class="spr-form-input spr-form-input-email " id="review_email_6537875078" type="email" name="review[email]" value="" placeholder="john.smith@example.com">
                                                                                        </div>
                                                                                    </fieldset>
                                                                                    <fieldset class="spr-form-review">
                                                                                        <div class="spr-form-review-rating">
                                                                                            <label class="spr-form-label">Rating</label>
                                                                                            <div class="spr-form-input spr-starrating ">
                                                                                                <a href="#" class="spr-icon spr-icon-star spr-icon-star-empty" data-value="1">&nbsp;</a>
                                                                                                <a href="#" class="spr-icon spr-icon-star spr-icon-star-empty" data-value="2">&nbsp;</a>
                                                                                                <a href="#" class="spr-icon spr-icon-star spr-icon-star-empty" data-value="3">&nbsp;</a>
                                                                                                <a href="#" class="spr-icon spr-icon-star spr-icon-star-empty" data-value="4">&nbsp;</a>
                                                                                                <a href="#" class="spr-icon spr-icon-star spr-icon-star-empty" data-value="5">&nbsp;</a>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="spr-form-review-title">
                                                                                            <label class="spr-form-label" for="review_title_6537875078">Review Title</label>
                                                                                            <input class="spr-form-input spr-form-input-text " id="review_title_6537875078" type="text" name="review[title]" value="" placeholder="Give your review a title">
                                                                                        </div>
                                                                                        <div class="spr-form-review-body">
                                                                                            <label class="spr-form-label" for="review_body_6537875078">Body of Review <span class="spr-form-review-body-charactersremaining">(1500)</span></label>
                                                                                            <div class="spr-form-input">
                                                                                                <textarea class="spr-form-input spr-form-input-textarea " id="review_body_6537875078" data-product-id="6537875078" name="review[body]" rows="10" placeholder="Write your comments here"></textarea>

                                                                                            </div>
                                                                                        </div>
                                                                                    </fieldset>
                                                                                    <fieldset class="spr-form-actions">
                                                                                        <input type="submit" class="spr-button spr-button-primary button button-primary btn btn-primary" value="Submit Review">
                                                                                    </fieldset>
                                                                                </form>
                                                                            </div>
                                                                            <div class="spr-reviews" id="reviews_6537875078">
                                                                                <div class="spr-review" id="spr-review-7003642">
                                                                                    <div class="spr-review-header">
                                                                                        <span class="spr-starratings spr-review-header-starratings"><i class="spr-icon spr-icon-star" style=""></i><i class="spr-icon spr-icon-star" style=""></i><i class="spr-icon spr-icon-star" style=""></i><i class="spr-icon spr-icon-star" style=""></i><i class="spr-icon spr-icon-star" style=""></i></span>
                                                                                        <h3 class="spr-review-header-title">JUAN PEREZ</h3>
                                                                                        <span class="spr-review-header-byline">10 de Junio de 2017</strong></span>
                                                                                    </div>
                                                                                    <div class="spr-review-content">
                                                                                        <p class="spr-review-content-body">¡Super rico!</p>
                                                                                    </div>
                                                                                    <div class="spr-review-footer">
                                                                                        <a href="#" class="spr-review-reportreview" onclick="SPR.reportReview(7003642);return false" id="report_7003642" data-msg="This review has been reported">Reportar como Inapropiado</a>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="tab-pane fade " id="shipping">
                                                        <div class="shipping-item">
                                                            <p class="item-title">Política de Devoluciones</p>
                                                            <p class="item-desc">
                                                                Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora torquent per conubia nostra.
                                                                <br> Version of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora torquent per conubia nostra.
                                                            </p>
                                                        </div>
                                                        <div class="shipping-item">
                                                            <p class="item-title">Despacho</p>
                                                            <p class="item-desc">
                                                                Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora torquent per conubia nostra. Version of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non mauris vitae erat consequat auctor eu in elit.
                                                            </p>
                                                        </div>
                                                    </div>
                                                    <div class="tab-pane fade " id="payment">
                                                        Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora torquent per conubia nostra. Version of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio.
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="related-products col-sm-12">
                                            <div class="sub-title">
                                                <h2>Productos relacionados</h2>
                                            </div>
                                            <div class="group-related">
                                                <div class="group-related-inner">
                                                    <div class="rp-slider">
                                                        <div class="row-container product list-unstyled clearfix product-circle">
                                                            <div class="row-left">
                                                                <a href="product.html" class="hoverBorder container_item">
                                                                    <div class="hoverBorderWrapper">
                                                                        <img src="assets/images/product_10.jpg" class="img-responsive front" alt="Coke">
                                                                        <div class="mask"></div>
                                                                    </div>
                                                                </a>
                                                                <div class="hover-mask">
                                                                    <div class="group-mask">
                                                                        <div class="inner-mask">
                                                                            <div class="group-actionbutton">
                                                                                <ul class="quickview-wishlist-wrapper">
                                                                                    <li class="wishlist">
                                                                                        <a title="agregar a lista de deseos" class="wishlist wishlist-coke" data-wishlisthandle="coke"><span class="cs-icon icon-heart"></span></a>
                                                                                    </li>
                                                                                    <li class="quickview hidden-xs hidden-sm">
                                                                                        <div class="product-ajax-cart">
                                                                                            <span class="overlay_mask"></span>
                                                                                            <div data-handle="coke" data-target="#quick-shop-modal" class="quick_shop" data-toggle="modal">
                                                                                                <a class=""><span class="cs-icon icon-eye"></span></a>
                                                                                            </div>
                                                                                        </div>
                                                                                    </li>
                                                                                    <li class="compare">
                                                                                        <a title="comparar" class="compare compare-coke" data-comparehandle="coke"><span class="cs-icon icon-retweet2"></span></a>
                                                                                    </li>
                                                                                </ul>
                                                                            </div>
                                                                            <form action="http://demo.designshopify.com/html_fastfood/cart.html" method="post">
                                                                                <div class="effect-ajax-cart">
                                                                                    <button class="_btn select-option" type="button" onclick="window.location='product.html';" title="Select Option">hacer pedido</button>
                                                                                </div>
                                                                            </form>
                                                                        </div>
                                                                        <!--inner-mask-->
                                                                    </div>
                                                                    <!--Group mask-->
                                                                </div>
                                                                <div class="product-label">
                                                                    <div class="label-element deal-label">
                                                                        <span>33%</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row-right animMix">
                                                                <div class="rating-star">
																		<span class="spr-badge" data-rating="5.0"><span class="spr-starrating spr-badge-starrating"><i class="spr-icon spr-icon-star" style=""></i><i class="spr-icon spr-icon-star" style=""></i><i class="spr-icon spr-icon-star" style=""></i><i class="spr-icon spr-icon-star" style=""></i><i class="spr-icon spr-icon-star" style=""></i></span><span class="spr-badge-caption">1 review</span>
																		</span>
                                                                </div>
                                                                <div class="product-title"><a class="title-5" href="product.html">Coke</a></div>
                                                                <div class="product-price">
                                                                    <span class="price_sale"><span class="money" data-currency-usd="$10.00">$10.00</span></span>
                                                                    <del class="price_compare"> <span class="money" data-currency-usd="$15.00">$15.00</span></del>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row-container product list-unstyled clearfix product-circle">
                                                            <div class="row-left">
                                                                <a href="product.html" class="hoverBorder container_item">
                                                                    <div class="hoverBorderWrapper">
                                                                        <img src="assets/images/product_11.jpg" class="img-responsive front" alt="Juice Ice Tea">
                                                                        <div class="mask"></div>
                                                                    </div>
                                                                </a>
                                                                <div class="hover-mask">
                                                                    <div class="group-mask">
                                                                        <div class="inner-mask">
                                                                            <div class="group-actionbutton">
                                                                                <ul class="quickview-wishlist-wrapper">
                                                                                    <li class="wishlist">
                                                                                        <a title="agregar a lista de deseos" class="wishlist wishlist-juice-ice-tea" data-wishlisthandle="juice-ice-tea"><span class="cs-icon icon-heart"></span></a>
                                                                                    </li>
                                                                                    <li class="quickview hidden-xs hidden-sm">
                                                                                        <div class="product-ajax-cart">
                                                                                            <span class="overlay_mask"></span>
                                                                                            <div data-handle="juice-ice-tea" data-target="#quick-shop-modal" class="quick_shop" data-toggle="modal">
                                                                                                <a class=""><span class="cs-icon icon-eye"></span></a>
                                                                                            </div>
                                                                                        </div>
                                                                                    </li>
                                                                                    <li class="compare">
                                                                                        <a title="comparar" class="compare compare-juice-ice-tea" data-comparehandle="juice-ice-tea"><span class="cs-icon icon-retweet2"></span></a>
                                                                                    </li>
                                                                                </ul>
                                                                            </div>
                                                                            <form action="http://demo.designshopify.com/html_fastfood/cart.html" method="post">
                                                                                <div class="effect-ajax-cart">
                                                                                    <input type="hidden" name="quantity" value="1">
                                                                                    <button class="_btn add-to-cart" data-parent=".parent-fly" type="submit" name="add" title="hacer pedido">hacer pedido</button>
                                                                                </div>
                                                                            </form>
                                                                        </div>
                                                                        <!--inner-mask-->
                                                                    </div>
                                                                    <!--Group mask-->
                                                                </div>
                                                                <div class="product-label">
                                                                    <div class="label-element new-label">
                                                                        <span>Nuevo</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row-right animMix">
                                                                <div class="rating-star">
																		<span class="spr-badge" data-rating="0.0"><span class="spr-starrating spr-badge-starrating"><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i></span><span class="spr-badge-caption">No reviews</span>
																		</span>
                                                                </div>
                                                                <div class="product-title"><a class="title-5" href="product.html">Juice Ice Tea</a></div>
                                                                <div class="product-price">
																		<span class="price">
																			<span class="money" data-currency-usd="$20.00">$20.00</span>
																		</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row-container product list-unstyled clearfix product-circle">
                                                            <div class="row-left">
                                                                <a href="product.html" class="hoverBorder container_item">
                                                                    <div class="hoverBorderWrapper">
                                                                        <img src="assets/images/product_13.jpg" class="img-responsive front" alt="Juice Ice Tea">
                                                                        <div class="mask"></div>
                                                                    </div>
                                                                </a>
                                                                <div class="hover-mask">
                                                                    <div class="group-mask">
                                                                        <div class="inner-mask">
                                                                            <div class="group-actionbutton">
                                                                                <ul class="quickview-wishlist-wrapper">
                                                                                    <li class="wishlist">
                                                                                        <a title="agregar a lista de deseos" class="wishlist wishlist-juice-ice-tea" data-wishlisthandle="juice-ice-tea"><span class="cs-icon icon-heart"></span></a>
                                                                                    </li>
                                                                                    <li class="quickview hidden-xs hidden-sm">
                                                                                        <div class="product-ajax-cart">
                                                                                            <span class="overlay_mask"></span>
                                                                                            <div data-handle="juice-ice-tea" data-target="#quick-shop-modal" class="quick_shop" data-toggle="modal">
                                                                                                <a class=""><span class="cs-icon icon-eye"></span></a>
                                                                                            </div>
                                                                                        </div>
                                                                                    </li>
                                                                                    <li class="compare">
                                                                                        <a title="comparar" class="compare compare-juice-ice-tea" data-comparehandle="juice-ice-tea"><span class="cs-icon icon-retweet2"></span></a>
                                                                                    </li>
                                                                                </ul>
                                                                            </div>
                                                                            <form action="http://demo.designshopify.com/html_fastfood/cart.html" method="post">
                                                                                <div class="effect-ajax-cart">
                                                                                    <input type="hidden" name="quantity" value="1">
                                                                                    <button class="_btn add-to-cart" data-parent=".parent-fly" type="submit" name="add" title="hacer pedido">hacer pedido</button>
                                                                                </div>
                                                                            </form>
                                                                        </div>
                                                                        <!--inner-mask-->
                                                                    </div>
                                                                    <!--Group mask-->
                                                                </div>
                                                            </div>
                                                            <div class="row-right animMix">
                                                                <div class="rating-star">
																		<span class="spr-badge" data-rating="0.0"><span class="spr-starrating spr-badge-starrating"><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i></span><span class="spr-badge-caption">No reviews</span>
																		</span>
                                                                </div>
                                                                <div class="product-title"><a class="title-5" href="product.html">Juice Ice Tea</a></div>
                                                                <div class="product-price">
																		<span class="price">
																			<span class="money" data-currency-usd="$20.00">$20.00</span>
																		</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row-container product list-unstyled clearfix product-circle">
                                                            <div class="row-left">
                                                                <a href="product.html" class="hoverBorder container_item">
                                                                    <div class="hoverBorderWrapper">
                                                                        <img src="assets/images/product_12.jpg" class="img-responsive front" alt="Juice Ice Tea">
                                                                        <div class="mask"></div>
                                                                    </div>
                                                                </a>
                                                                <div class="hover-mask">
                                                                    <div class="group-mask">
                                                                        <div class="inner-mask">
                                                                            <div class="group-actionbutton">
                                                                                <ul class="quickview-wishlist-wrapper">
                                                                                    <li class="wishlist">
                                                                                        <a title="agregar a lista de deseos" class="wishlist wishlist-juice-ice-tea" data-wishlisthandle="juice-ice-tea"><span class="cs-icon icon-heart"></span></a>
                                                                                    </li>
                                                                                    <li class="quickview hidden-xs hidden-sm">
                                                                                        <div class="product-ajax-cart">
                                                                                            <span class="overlay_mask"></span>
                                                                                            <div data-handle="juice-ice-tea" data-target="#quick-shop-modal" class="quick_shop" data-toggle="modal">
                                                                                                <a class=""><span class="cs-icon icon-eye"></span></a>
                                                                                            </div>
                                                                                        </div>
                                                                                    </li>
                                                                                    <li class="compare">
                                                                                        <a title="comparar" class="compare compare-juice-ice-tea" data-comparehandle="juice-ice-tea"><span class="cs-icon icon-retweet2"></span></a>
                                                                                    </li>
                                                                                </ul>
                                                                            </div>
                                                                            <form action="http://demo.designshopify.com/html_fastfood/cart.html" method="post">
                                                                                <div class="effect-ajax-cart">
                                                                                    <input type="hidden" name="quantity" value="1">
                                                                                    <button class="_btn add-to-cart" data-parent=".parent-fly" type="submit" name="add" title="hacer pedido">hacer pedido</button>
                                                                                </div>
                                                                            </form>
                                                                        </div>
                                                                        <!--inner-mask-->
                                                                    </div>
                                                                    <!--Group mask-->
                                                                </div>
                                                                <div class="product-label">
                                                                    <div class="label-element hotting-label">
                                                                        <span>Hot</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row-right animMix">
                                                                <div class="rating-star">
																		<span class="spr-badge" data-rating="0.0"><span class="spr-starrating spr-badge-starrating"><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i></span><span class="spr-badge-caption">No reviews</span>
																		</span>
                                                                </div>
                                                                <div class="product-title"><a class="title-5" href="product.html">Extra Crispy</a></div>
                                                                <div class="product-price">
																		<span class="price">
																			<span class="money" data-currency-usd="$20.00">$20.00</span>
																		</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--END -->
                                        </div>
                                    </div>
                                </div>
                                <div class="collection-leftsidebar sidebar col-sm-3 clearfix">
                                    <div class="collection-leftsidebar-group">
                                        <div class="sidebar-block collection-block">
                                            <h3 class="sidebar-title">
                                                <span class="text">Categorías</span>
                                                <span class="cs-icon icon-minus"></span>
                                            </h3>
                                            <div class="sidebar-content">
                                                <ul class="list-cat">
                                                    <li class="active"><a href="collections.html">Todo </a></li>
                                                    <li><a href="collections.html">Drinks </a></li>
                                                    <li><a href="collections.html">Fast food </a></li>
                                                    <li><a href="collections.html">French Fries </a></li>
                                                    <li><a href="collections.html">Patacones </a></li>
                                                    <li><a href="collections.html">Pizza </a></li>
                                                    <li><a href="collections.html">Tasty Flyers </a></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="sidebar-block latest-block">
                                            <h3 class="sidebar-title">
                                                <span class="text">Lo último</span>
                                                <span class="cs-icon icon-minus"></span>
                                            </h3>
                                            <div class="sidebar-content">
                                                <div class="latest-inner">
                                                    <div class="products-item">
                                                        <div class="row-container product-circle">
                                                            <div class="product home_product">
                                                                <div class="row-left">
                                                                    <a href="product.html" class="container_item">
                                                                        <div class="hoverBorderWrapper">
                                                                            <img src="cliente/images/21CocaNormal.jpg" class="not-rotation img-responsive front" alt="Coke">
                                                                            <div class="mask"></div>
                                                                        </div>
                                                                    </a>
                                                                </div>
                                                                <div class="row-right">
                                                                    <div class="product-title"><a class="title-5" href="product.html">COCA COLA 1.5 Lts</a></div>
                                                                    <div class="rating-star">
																					<span class="spr-badge" data-rating="5.0"><span class="spr-starrating spr-badge-starrating"><i class="spr-icon spr-icon-star" style=""></i><i class="spr-icon spr-icon-star" style=""></i><i class="spr-icon spr-icon-star" style=""></i><i class="spr-icon spr-icon-star" style=""></i><i class="spr-icon spr-icon-star" style=""></i></span><span class="spr-badge-caption">1 review</span>
																					</span>

                                                                    </div>
                                                                    <div class="product-price">
                                                                        <span class="price_sale"><span class="money" data-currency-usd="$10.00" data-currency="USD">$1500.00</span></span>
                                                                        <del class="price_compare"> <span class="money" data-currency-usd="$15.00" data-currency="USD">$2000.000</span></del>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="products-item">
                                                        <div class="row-container product-circle">
                                                            <div class="product home_product">
                                                                <div class="row-left">
                                                                    <a href="product.html" class="container_item">
                                                                        <div class="hoverBorderWrapper">
                                                                            <img src="cliente/images/coca-cola-tradicional-en-lata-350-ml.jpg" class="not-rotation img-responsive front" alt="Coke">
                                                                            <div class="mask"></div>
                                                                        </div>
                                                                    </a>
                                                                </div>
                                                                <div class="row-right">
                                                                    <div class="product-title"><a class="title-5" href="product.html">COCA COLA LATA 350 CC</a></div>
                                                                    <div class="rating-star">
																					<span class="spr-badge" data-rating="5.0"><span class="spr-starrating spr-badge-starrating"><i class="spr-icon spr-icon-star" style=""></i><i class="spr-icon spr-icon-star" style=""></i><i class="spr-icon spr-icon-star" style=""></i><i class="spr-icon spr-icon-star" style=""></i><i class="spr-icon spr-icon-star" style=""></i></span><span class="spr-badge-caption">1 review</span>
																					</span>

                                                                    </div>
                                                                    <div class="product-price">
                                                                        <span class="price_sale"><span class="money" data-currency-usd="$10.00" data-currency="USD">$450.00</span></span>
                                                                        <del class="price_compare"> <span class="money" data-currency-usd="$15.00" data-currency="USD">$500.00</span></del>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="products-item">
                                                        <div class="row-container product-circle">
                                                            <div class="product home_product">
                                                                <div class="row-left">
                                                                    <a href="product.html" class="container_item">
                                                                        <div class="hoverBorderWrapper">
                                                                            <img src="cliente/images/coca-cola-ligth-en-lata-350ml.jpg" class="not-rotation img-responsive front" alt="Coke">
                                                                            <div class="mask"></div>
                                                                        </div>
                                                                    </a>
                                                                </div>
                                                                <div class="row-right">
                                                                    <div class="product-title"><a class="title-5" href="product.html">COCA COLA ZERO LATA 350 CC</a></div>
                                                                    <div class="rating-star">
																					<span class="spr-badge" data-rating="5.0"><span class="spr-starrating spr-badge-starrating"><i class="spr-icon spr-icon-star" style=""></i><i class="spr-icon spr-icon-star" style=""></i><i class="spr-icon spr-icon-star" style=""></i><i class="spr-icon spr-icon-star" style=""></i><i class="spr-icon spr-icon-star" style=""></i></span><span class="spr-badge-caption">1 review</span>
																					</span>
                                                                    </div>
                                                                    <div class="product-price">
                                                                        <span class="price_sale"><span class="money" data-currency-usd="$450.00" data-currency="USD">$10.00</span></span>
                                                                        <del class="price_compare"> <span class="money" data-currency-usd="$500.00" data-currency="USD">$15.00</span></del>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- filter tags group -->
                                        <div class="sidebar-block filter-block">
                                            <h3 class="sidebar-title">
                                                <span class="text">Color</span>
                                                <span class="cs-icon icon-minus"></span>
                                            </h3>
                                            <div class="sidebar-content">
                                                <ul class="filter-content filter-color">
                                                    <li data-handle="green" class="swatch-tag "><a href="collections.html" title="Narrow selection to products matching tag Green"><span style="background-color: green; background-image: url(assets/images/green.png);" class="color-swatch btooltip" title="Green"></span></a></li>
                                                    <li data-handle="black" class="swatch-tag "><a href="collections.html" title="Narrow selection to products matching tag Black"><span style="background-color: black; background-image: url(assets/images/black.png);" class="color-swatch btooltip" title="Black"></span></a></li>
                                                    <li data-handle="blue" class="swatch-tag "><a href="collections.html" title="Narrow selection to products matching tag Blue"><span style="background-color: blue; background-image: url(assets/images/blue.png);" class="color-swatch btooltip" title="Blue"></span></a></li>
                                                    <li data-handle="red" class="swatch-tag "><a href="collections.html" title="Narrow selection to products matching tag Red"><span style="background-color: red; background-image: url(assets/images/red.png);" class="color-swatch btooltip" title="Red"></span></a></li>
                                                    <li data-handle="yellow" class="swatch-tag "><a href="collections.html" title="Narrow selection to products matching tag Yellow"><span style="background-color: yellow; background-image: url(assets/images/yellow.png);" class="color-swatch btooltip" title="Yellow"></span></a></li>
                                                    <li data-handle="brown" class="swatch-tag "><a href="collections.html" title="Narrow selection to products matching tag Brown"><span style="background-color: brown; background-image: url(assets/images/brown.png);" class="color-swatch btooltip" title="Brown"></span></a></li>
                                                    <li data-handle="silver" class="swatch-tag "><a href="collections.html" title="Narrow selection to products matching tag Silver"><span style="background-color: silver; background-image: url(assets/images/silver.png);" class="color-swatch btooltip" title="Silver"></span></a></li>
                                                    <li data-handle="gray" class="swatch-tag "><a href="collections.html" title="Narrow selection to products matching tag Gray"><span style="background-color: gray; background-image: url(assets/images/gray.png);" class="color-swatch btooltip" title="Gray"></span></a></li>
                                                    <li data-handle="purple" class="swatch-tag "><a href="collections.html" title="Narrow selection to products matching tag Purple"><span style="background-color: purple; background-image: url(assets/images/purple.png);" class="color-swatch btooltip" title="Purple"></span></a></li>
                                                    <li data-handle="orange" class="swatch-tag "><a href="collections.html" title="Narrow selection to products matching tag Orange"><span style="background-color: orange; background-image: url(assets/images/orange.png);" class="color-swatch btooltip" title="Orange"></span></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="sidebar-block filter-block">
                                            <h3 class="sidebar-title">
                                                <span class="text">Tamaño</span>
                                                <span class="cs-icon icon-minus"></span>
                                            </h3>
                                            <div class="sidebar-content">
                                                <ul class="filter-content ">
                                                    <li data-handle="small"><a href="collections.html" title="Narrow selection to products matching tag Small"><span class="fe-checkbox"></span> Pequeño</a></li>
                                                    <li data-handle="medium"><a href="collections.html" title="Narrow selection to products matching tag Medium"><span class="fe-checkbox"></span> Mediano</a></li>
                                                    <li data-handle="large"><a href="collections.html" title="Narrow selection to products matching tag Large"><span class="fe-checkbox"></span> Grande</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="sidebar-block filter-block">
                                            <h3 class="sidebar-title">
                                                <span class="text">Precio</span>
                                                <span class="cs-icon icon-minus"></span>
                                            </h3>
                                            <div class="sidebar-content">
                                                <ul class="filter-content ">
                                                    <li data-handle="under-100"><a href="collections.html" title="Narrow selection to products matching tag Under $100"><span class="fe-checkbox"></span> Bajo $100</a></li>
                                                    <li data-handle="100-200"><a href="collections.html" title="Narrow selection to products matching tag $100 - $200"><span class="fe-checkbox"></span> $100 - $200</a></li>
                                                    <li data-handle="above-200"><a href="collections.html" title="Narrow selection to products matching tag Above $200"><span class="fe-checkbox"></span> Sobre $200</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="sidebar-block filter-block">
                                            <h3 class="sidebar-title">
                                                <span class="text">Cubierta</span>
                                                <span class="cs-icon icon-minus"></span>
                                            </h3>
                                            <div class="sidebar-content">
                                                <ul class="filter-content ">
                                                    <li data-handle="cheesy-mcplain"><a href="collections.html" title="Narrow selection to products matching tag Cheesy McPlain"><span class="fe-checkbox"></span> Cheesy McPlain</a></li>
                                                    <li data-handle="the-gardener"><a href="collections.html" title="Narrow selection to products matching tag The Gardener"><span class="fe-checkbox"></span> The Gardener</a></li>
                                                    <li data-handle="dr-pepperoni"><a href="collections.html" title="Narrow selection to products matching tag Dr.Pepperoni"><span class="fe-checkbox"></span> Dr.Pepperoni</a></li>
                                                    <li data-handle="the-carnivore"><a href="collections.html" title="Narrow selection to products matching tag The Carnivore"><span class="fe-checkbox"></span> The Carnivore</a></li>
                                                    <li data-handle="the-vacationer"><a href="collections.html" title="Narrow selection to products matching tag The Vacationer"><span class="fe-checkbox"></span> The Vacationer</a></li>
                                                    <li data-handle="the-smurf"><a href="collections.html" title="Narrow selection to products matching tag The Smurf"><span class="fe-checkbox"></span> The Smurf</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="sidebar-block type-block">
                                            <h3 class="sidebar-title">
                                                <span class="text">Lista Tipo</span>
                                                <span class="cs-icon icon-minus"></span>
                                            </h3>
                                            <div class="sidebar-content">
                                                <ul class="type-content">
                                                    <!--type-->
                                                    <li class="">
                                                        <a href="collections.html" title="Drink">Bebidas</a>
                                                    </li>
                                                    <li class="">
                                                        <a href="collections.html" title="Fast food">Combos</a>
                                                    </li>
                                                    <li class="">
                                                        <a href="collections.html" title="Hamburger">Patacones</a>
                                                    </li>
                                                    <li class="">
                                                        <a href="collections.html" title="Pizza">Empanadas</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="sidebar-block vendor-block">
                                            <h3 class="sidebar-title">
                                                <span class="text">Proveedores</span>
                                                <span class="cs-icon icon-minus"></span>
                                            </h3>
                                            <div class="sidebar-content">
                                                <ul class="vendor-content">
                                                    <!--vendor-->
                                                    <li class="">
                                                        <a href="collections.html" title="Vender 1">Proveedor 1</a>
                                                    </li>
                                                    <li class="">
                                                        <a href="collections.html" title="Vender 2">Proveedor 2</a>
                                                    </li>
                                                    <li class="">
                                                        <a href="collections.html" title="Vender 3">Proveedor 3</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="sidebar-block banner-block">
                                            <div class="sidebar-content">
                                                <a href="collections.html">
                                                    <img src="cliente/images/miercoles.jpg" alt="image product" title="">
                                                </a>
                                            </div>
                                        </div>
                                        <div class="sidebarMobile sidebar-bottom">
                                            <button class="sidebarMobile-clear _btn">
                                                Clear All
                                            </button>
                                            <button class="sidebarMobile-close _btn">
                                                Apply &amp; Close
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <script type="text/javascript">
                                function active_review_form(){
                                    if($("#form_6537875078").attr('style')=='display: none;'){
                                        $("#form_6537875078").css('display','block');
                                    }
                                    else {
                                        $("#form_6537875078").css('display','none');
                                    }
                                }
                                jQuery(document).ready(function($){
                                    $('#gallery-images div.image').on('click', function() {
                                        var $this = $(this);
                                        var parent = $this.parents('#gallery-images');
                                        parent.find('.image').removeClass('active');
                                        $this.addClass('active');
                                    });
                                });
                            </script>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
</div>
@include('layouts.partials_cliente.footer')

@include('layouts.partials_cliente.modals_home')
<div class="float-right-icon">
    <ul>
        <li>
            <div id="scroll-to-top" data-toggle="" data-placement="left" title="Scroll to Top" class="off">
                <i class="fa fa-angle-up"></i>
            </div>
        </li>
    </ul>
</div>
</body>
</html>