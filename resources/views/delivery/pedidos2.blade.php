
@extends('layouts.marco_cliente');
@push('css')
<link href="{{asset('cliente/css/locales.css')}}" rel="stylesheet" type="text/css" media="all">
@endpush
@section('content')

	<head>
		<title>Locales</title>
		<meta name="viewport" content="initial-scale=1.0, user-scalable=no">
		<meta charset="utf-8">
		<link rel="stylesheet" href="css/bootstrap.min.css">
		<!--<link rel="stylesheet" href="css/font-awesome.min.css">-->
		<script src="js/jquery-3.3.1.min.js"></script>

		<style>
/* Always set the map height explicitly to define the size of the div
 * element that contains the map. */
#map {
height: 500px;
			width: 450px;
		  }
		</style>


		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<style>

		.restaurant-detail {
    position: relative;
    margin-bottom: 20px;
			padding-left: 40px;
		}
		* {
    box-sizing: border-box;
		}

		body {
    font: 16px Arial;
		}

		.autocomplete {
    /*the container must be positioned relative:*/
    position: relative;
    display: inline-block;
}

		input {
    border: 1px solid transparent;
		  background-color: #f1f1f1;
		  padding: 10px;
		  font-size: 16px;
		}

		input[type=submit] {
    background-color: DodgerBlue;
		  color: #fff;
		  cursor: pointer;
		}

		.autocomplete-items {
    position: absolute;
    border: 1px solid #d4d4d4;
		  border-bottom: none;
		  border-top: none;
		  z-index: 99;
		  /*position the autocomplete items to be the same width as the container:*/
		  top: 100%;
		  left: 0;
		  right: 0;
		}

		.autocomplete-items div {
    padding: 10px;
		  cursor: pointer;
		  background-color: #fff;
		  border-bottom: 1px solid #d4d4d4;
		}

		.autocomplete-items div:hover {
    /*when hovering an item:*/
    background-color: #e9e9e9;
		}

		.autocomplete-active {
    /*when navigating through the items using the arrow keys:*/
    background-color: DodgerBlue !important;
		  color: #ffffff;
		}

		.restaurant-name{
    color:green;
    font-size:18px;
			font-weight:bold;
		}
		</style>
		<script>
			var map;
			var service;
			var infowindow;
			var userLoc;
			var myLatLng;
			var streetsJSON;
			var nearLocationObject;

			$(document).ready(function(){
                dllInitAutocomplete({'value':'Buin'});
				$(".close-link").click(function() {
                    $("#sectionDeliveryInformation").hide(1000);
                });
			});
			//Loc Santiago default
			var defLoc = {lat: -33.460450, lng: -70.760730};
			function initMap() {
                map = new google.maps.Map(document.getElementById('map'), {
					center: defLoc,
					zoom: 10,
					disableDefaultUI: true,
					zoomControl: true,
					fullscreenControl: true,
					zoomControlOptions: {
                    position: google.maps.ControlPosition.LEFT_BOTTOM
					},
				});

				infowindow = new google.maps.InfoWindow();
				var service = new google.maps.places.PlacesService(map);
				service.textSearch({
					location: userLoc,
					//query:"papa john's",
					query: "fritomania restaurantes"
				}, callback);
			}
			function callback(results, status) {
                if (status === google.maps.places.PlacesServiceStatus.OK) {
                    map.zoom = 17;
                    var posMinDistance = 0;
                    var minDistance = 0;
                    for (var i = 0; i < results.length; i++) {
                        if(results[i].name == "Fritomania")
                        {
                            //createMarker(results[i]);
                            var distance = Math.round(google.maps.geometry.spherical.computeDistanceBetween(userLoc, results[i].geometry.location));

                            if(i==0){
                                minDistance = distance;
                            }
                            else if(distance < minDistance){
                                posMinDistance = i;
                                minDistance = distance;
                            }
                        }
                    }
					if(Math.round(google.maps.geometry.spherical.computeDistanceBetween(userLoc, results[posMinDistance].geometry.location)) <= 50000)
                    {
                        createMarker(results[posMinDistance]);
                        var placeLoc = {lat: results[posMinDistance].geometry.location.lat(),lng:results[posMinDistance].geometry.location.lng()};
						map.setCenter(placeLoc);
						$("#divStoreSearchResultError").hide();
						$("#divResultDetails").show();
					}
                    else{
                        $("#divStoreSearchResultError").show();
                        $("#divResultDetails").hide();
                    }

				}
            }

			function createMarker(place) {
                var placeLoc = place.geometry.location;
                var marker = new google.maps.Marker({
				  map: map,
				  position: place.geometry.location
				});

				var service = new google.maps.places.PlacesService(map);
				var request = { reference: place.reference };
				service.getDetails(request, function(details, status) {
                    google.maps.event.addListener(marker, 'click', function() {
                        infowindow.setContent("<span class='restaurant-name'>"+place.name+"</span><br/>"
                            +"<br/><table style='border-bottom:1pt solid black;'><tr><th rowspan='3'><img style='width:80px;height:40px;' src='https://fritomania.cl/cliente/images/logo_nuevo.png'/></th><th align='left'><b>Horario</b></th><th/></tr>"
                            +"<tr><td>Lunes a Viernes</td><td width=10/><td>12:00 pm - 12:00 am<td></tr>"
                            +"<br/><tr><td>Sabados y Domingos</td><td width=10/><td>11:00 am - 12:00 am<td></tr><tr height=10></tr>"
                            +"</table>"
                            +"<br/><span style='color:red;font-weight:bold;'>El valor mínimo para delivery es ???</span>");
                        infowindow.open(map, this);
                    });
                    $(".restaurant-name").html(place.name);
                    $(".restaurant-address").html(place.formatted_address);

                    if(details.formatted_phone_number)
                    {
                        $(".restaurant-phone").html("<a href='tel:"+details.formatted_phone_number+"'>"+details.formatted_phone_number+"</a>");
                    }

                    var openingHours = "";

                    if(details.opening_hours && details.opening_hours.weekday_text){
                        var dayOfWeek = new Date().getDay();
                        if(dayOfWeek == 0)dayOfWeek = 6;else dayOfWeek = dayOfWeek-1;
                        openingHours = details.opening_hours.weekday_text[dayOfWeek];
                    }
                    addressDetail = {
                        street:$("<div>"+details.adr_address+"</div>").find("span.street-address").text(),
					locality:$("<div>"+details.adr_address+"</div>").find("span.locality").text(),
					region:$("<div>"+details.adr_address+"</div>").find("span.region").text(),
					country:$("<div>"+details.adr_address+"</div>").find("span.country-name").text(),
				  };
				  nearLocationObject = {
                        phone:details.formatted_phone_number,
					name:place.name,
					address:addressDetail,
					openingHours: openingHours,
					streetInput : $("#txtStreetName").val(),
					districtInput: $("#ddlState").val(),
					streetNumberInput: $("#txtStreetNumber").val(),
					apartmentInput: $("#txtApartment").val(),
					url: details.url
				  };
				  // Store
				  localStorage.setItem("nearLocationObject", JSON.stringify(nearLocationObject));
				});
			}

			//Logica del boton
			function searchUserLocation(){
                if(!validateForm()){
                    return false;
                }
                var address = $("#ddlState").val()+", "+$("#txtStreetName").val()+" "+$("#txtStreetNumber").val();
                var geocoder = new google.maps.Geocoder();
                geocoder.geocode({'address': address}, function(results, status) {
                    if (status === 'OK') {
                        //$("#divStoreSearchResultError").hide();
                        //$("#divResultDetails").show();
                        userLoc = results[0].geometry.location;
                        initMap();

                    }
                    else {
                        //alert('Geocode was not successful for the following reason: ' + status);
                        $("#divStoreSearchResultError").show();
                        $("#divResultDetails").hide();
                    }
                });
			}

			function dllInitAutocomplete(item){
                $.getJSON("js/streets/chileLocation_"+$(item)[0].value+".json", function(data){
                    streetsJSON = data.STREETNAME;
                    $("#txtStreetName").val("");
                    autocomplete(document.getElementById("txtStreetName"), streetsJSON);
                });
            }
			function validateForm(){
                var valid = true;
                if($("#txtStreetName").val().trim() == ""){
                    $("#streetValidMessage").text($("#txtStreetName").attr("validMessage"));
                    valid = false;
                }
                else{
                    $("#streetValidMessage").html("");
                }
                if($("#txtStreetNumber").val().trim() == ""){
                    $("#streetNumberValidMessage").text($("#txtStreetNumber").attr("validMessage"));
                    valid = false;
                }
                else{
                    if(parseInt($("#txtStreetNumber").val()) > 9999999 ){
                        $("#streetNumberValidMessage").text($("#txtStreetNumber").attr("maxValueMessage"));
                        valid = false;
                    }
                    else{
                        $("#streetNumberValidMessage").html("");
                    }
                }

                if($("#txtApartment").val().trim() != "" && $("#txtApartment").val().length > 80 ){
                    $("#apartmentValidMessage").text($("#txtApartment").attr("maxValueMessage"));
                    valid = false;
                }
                else{
                    $("#apartmentValidMessage").html("");
                }
                if($("#txtComments").val().trim() != "" && $("#txtComments").val().length > 80 ){
                    $("#commentsValidMessage").text($("#txtComments").attr("maxValueMessage"));
                    valid = false;
                }
                else{
                    $("#commentsValidMessage").html("");
                }



                return valid;
            }

			function showMyDeliveryInformation(){
                var getNearLocationObject = JSON.parse(localStorage.getItem("nearLocationObject"));

                var fullMyDeliveryInformation = getNearLocationObject.streetInput+", "+getNearLocationObject.streetNumberInput+", "+getNearLocationObject.districtInput;
                $("#streetInformation").html(getNearLocationObject.streetInput+", "+getNearLocationObject.streetNumberInput);
                $("#districtInformation").html(getNearLocationObject.apartmentInput.trim() == "" ? getNearLocationObject.districtInput : "Depto "+getNearLocationObject.apartmentInput+", "+getNearLocationObject.districtInput);
                $("#localNameInformation").html(getNearLocationObject.name);
                $("#address-street").html(getNearLocationObject.address.street);
                $("#localityRegion").html(getNearLocationObject.address.locality);
                $("#cellphoneInformation").attr("href",getNearLocationObject.phone);
                $("#cellphoneInformation").html(getNearLocationObject.phone);
                $("#horaryInformation").html(getNearLocationObject.openingHours);
                $("#fullMyDeliveryInformation").html(fullMyDeliveryInformation);
                $("#showAddressInformation").attr("href",getNearLocationObject.url);

                $("#sectionDeliveryInformation").show(1000);
                $("#myDeliveryInformation").show();

            }
		</script>
	</head>

	<body>

		<header>
			<section class="omnibar-wrap" id="sectionDeliveryInformation" style="display:none;">
                @include('components.web.heading',['ruta' => route('locales'),'titulo' => 'Buscar Local','tienda' => ''])
                @include('flash::message')
				<a href="#" class="close-link">
					<i class="fa fa-close close-link"></i>
cerrar
				</a>

				<div class="row">
					<div class="col-xs-5 col-md-6 col-lg-5 first-block">
						<span><span class="fa fa-map-marker"></span>Tu dirección de entrega</span>
						<h3 id="streetInformation"></h3><p id="districtInformation"></p>
						<div class="clearfix"></div>
						<a href="/Location" class="btn btn-secondary btn-lg">Cambiar dirección</a>
						<button name="command" value="c" class="btn btn-secondary btn-lg">
Retiro en local
</button>
						<div class="clearfix"></div>
						<a class="btn btn-lg btn-primary" href="/Menu">Ver Menú →</a>
					</div>

					<div class="col-xs-2 second-block">
						<!--<h4 id="localNameInformation">Papa John's - <p>ESTACIÓN CENTRAL</p></h4>-->
						<h4 id="localNameInformation"></h4>
						<span>
							<p id="address-street"></p>
							<p id="localityRegion"></p>
							<a id="cellphoneInformation" class="weight-size"></a>
						</span>
					</div>

					<div class="col-xs-2 third-block">
						<a id="showAddressInformation" href="#" target="_blank">Ver dirección en mapa</a>
						<p>Horario de hoy</p>
						<strong id="horaryInformation"></strong>
					</div>
				</div>
			</section>
			<section id="myDeliveryInformation" style="display:none;">
				<ul>
				<li class="col-xs-3 col-xs-offset-2 omnibar-nav-item first-child hidden-xs hidden-sm active">
                    <a href="javascript:void(0);" onclick="$('#sectionDeliveryInformation').toggle(1000);" class="carryout-link active" data-trigger="delivery">
<span>DELIVERY:</span> <p id="fullMyDeliveryInformation"></p>
                        <span class="fa fa-motorcycle"></span><span class="icon icon-expand"></span>
                    </a>
                </li>

				</ul>
			</section>
		</header>
		<div class="col-xs-12 restaurant-search-panel">
			<div class="tab-content">
                <!-- Fila selecciona tipo pedido -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="row tipopedido">Selecciona tu tipo de pedido</div>
                        <div class="row tipopedido">
                            <div class="col-sm-6 ">
                                <a href="javascript:void(0)" id="btnAbreRetiroLocal">
                                    <i class="fa fa-university" aria-hidden="true"></i><br>Retiro en Local
                                </a>
                            </div>
                            <div class="col-sm-6">
                                <a href="javascript:void(0)" id="btnAbreDelivery">
                                    <i class="fa fa-motorcycle" aria-hidden="true"></i><br>Delivery
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
				<div role="tabpanel" class="tab-pane active in" id="Delivery">
					<div class="row">
						<div class="col-xs-12 col-md-5 form-group">
							<form id="searchPlaces" autocomplete="off" data-toggle="validator" role="form">
								<div id="deliverySearchForm" style="padding-left: 60px;padding-top: 60px;">
									<div class="col-xs-12 ">
										<label class="control-label">Comuna<sup>*</sup></label>
										<select onchange="dllInitAutocomplete(this);" class="form-control input-lg valid" id="ddlState" name="StateCode">
											<option selected="selected" value="Buin">Buin</option>
											<option value="Colina">Colina</option>
											<option value="Concepción">Concepción</option>
											<option value="Conchali">Conchali</option>
											<option value="Concón">Concón</option>
											<option value="Estacion Central">Estacion Central</option>
											<option value="Huechuraba">Huechuraba</option>
											<option value="Independencia">Independencia</option>
											<option value="La Cisterna">La Cisterna</option>
											<option value="La Florida">La Florida</option>
											<option value="La Reina">La Reina</option>
											<option value="La Serena">La Serena</option>
											<option value="Las Condes">Las Condes</option>
											<option value="Linares">Linares</option>
											<option value="Lo Barnechea">Lo Barnechea</option>
											<option value="Los Andes">Los Andes</option>
											<option value="Los Angeles">Los Angeles</option>
											<option value="Machalí">Machalí</option>
											<option value="Macul">Macul</option>
											<option value="Maipú">Maipú</option>
											<option value="Melipilla">Melipilla</option>
											<option value="Ñuñoa">Ñuñoa</option>
											<option value="Osorno">Osorno</option>
											<option value="Peñalolén">Peñalolén</option>
											<option value="Providencia">Providencia</option>
											<option value="Pudahuel">Pudahuel</option>
											<option value="Puente Alto">Puente Alto</option>
											<option value="Puerto Montt">Puerto Montt</option>
											<option value="Quilicura">Quilicura</option>
											<option value="Quillota">Quillota</option>
											<option value="Quilpue">Quilpue</option>
											<option value="Quinta Normal">Quinta Normal</option>
											<option value="Rancagua">Rancagua</option>
											<option value="Recoleta">Recoleta</option>
											<option value="San Bernardo">San Bernardo</option>
											<option value="San Miguel">San Miguel</option>
											<option value="San Pedro De La Paz">San Pedro De La Paz</option>
											<option value="Santiago">Santiago</option>
											<option value="Talca">Talca</option>
											<option value="Talcahuano">Talcahuano</option>
											<option value="Temuco">Temuco</option>
											<option value="Valdivia">Valdivia</option>
											<option value="Valparaíso">Valparaíso</option>
											<option value="Villa Alemana">Villa Alemana</option>
											<option value="Viña del mar">Viña del mar</option>
											<option value="Vitacura">Vitacura</option>
										</select>
									</div>

									<div class="clearfix"></div>
									<div class="col-xs-12 autocomplete">
										<label class="control-label">Calle<sup>*</sup></label>
										<input autocomplete="off" class="form-control input-lg typeahead" id="txtStreetName" name="StreetName" placeholder="Calle" type="text" value=""  validMessage="Por favor escriba el nombre de la calle.">
										<span id="streetValidMessage" class="field-validation-valid text-danger help-block"></span>
									</div>

									<div class="col-xs-12 ">
										<label class="control-label">Número Domicilio<sup>*</sup></label>
										<input class="form-control input-lg" id="txtStreetNumber" max="9999999" min="1" name="StreetNumber" placeholder="Número Domicilio" type="number" value="" validMessage="Por favor escriba el número." maxValueMessage="Por favor ingresar un valor menor o igual a 9999999">
										<span id="streetNumberValidMessage" class="field-validation-valid text-danger help-block"></span>
									</div>
									<div class="clearfix"></div>
									<div class="col-xs-12 ">
										<label class="control-label">Depto</label>
										<input class="form-control input-lg" data-val="true" maxValueMessage="Límite de 80 caracteres" id="txtApartment" name="AddressLine1" placeholder="Depto" type="text" value="">
										<span id="apartmentValidMessage" class="field-validation-valid text-danger help-block"></span>
									</div>

									<div class="col-xs-12 ">
										<label class="control-label">Observaciones de entrega</label>
										<input class="form-control input-lg" maxValueMessage="Límite de 80 caracteres" id="txtComments" name="Comments" placeholder="Observaciones de entrega" type="text" value="">
										<div class="observationMessage">(*No es posible programar pedidos para más adelante ni solicitar descuentos en este cuadro. Por favor no solicitarlos.)</div>
										<span id="commentsValidMessage" class="field-validation-valid text-danger help-block"></span>
									</div>
									<div><input data-val="true" data-val-required="The IsReqdValidation field is required." id="IsReqdValidation" name="IsReqdValidation" type="hidden" value="True"></div>
									<div><input data-val="true" data-val-length="Límite de 80 caracteres" data-val-length-max="80" data-val-requiredif="Por favor introduce el nombre del lugar." data-val-requiredif-dependentproperty="IsReqdValidation" data-val-requiredif-desiredvalue="true" id="AddressTitle" name="AddressTitle" type="hidden" value="DeliverySearch"></div>
									<div class="col-xs-12 col-md-6 ">
										<input type="button" onclick="searchUserLocation();" class="btn btn-lg btn-block btn-secondary" value="Encontrar Local"/>
									</div>
								</div>
							</form>
							<script>
								function autocomplete(inp, arr) {
                                    var currentFocus;
                                    inp.addEventListener("input", function(e) {
                                        if(!arr) return false;
                                        var a, b, i, val = this.value;
									  closeAllLists();
									  if (!val) { return false;}
									  currentFocus = -1;
									  a = document.createElement("DIV");
									  a.setAttribute("id", this.id + "autocomplete-list");
									  a.setAttribute("class", "autocomplete-items");
									  this.parentNode.appendChild(a);
									  for (i = 0; i < arr.length; i++) {
                                          if (arr[i].substr(0, val.length).toUpperCase() == val.toUpperCase()) {
                                              b = document.createElement("DIV");
                                              b.innerHTML = "<strong>" + arr[i].substr(0, val.length) + "</strong>";
                                              b.innerHTML += arr[i].substr(val.length);
                                              b.innerHTML += "<input type='hidden' value='" + arr[i] + "'>";
                                              b.addEventListener("click", function(e) {
                                                  inp.value = this.getElementsByTagName("input")[0].value;
                                                  closeAllLists();
                                              });
                                              a.appendChild(b);
                                          }
                                      }
								  });
                                    inp.addEventListener("keydown", function(e) {
                                        var x = document.getElementById(this.id + "autocomplete-list");
                                        if (x) x = x.getElementsByTagName("div");
                                        if (e.keyCode == 40) {
                                            currentFocus++;
                                            addActive(x);
                                        } else if (e.keyCode == 38) {
                                            currentFocus--;
                                            addActive(x);
                                        } else if (e.keyCode == 13) {
                                            e.preventDefault();
                                            if (currentFocus > -1) {
                                                if (x) x[currentFocus].click();
                                            }
                                        }
                                    });
                                    function addActive(x) {
                                        if (!x) return false;
                                        removeActive(x);
                                        if (currentFocus >= x.length) currentFocus = 0;
                                        if (currentFocus < 0) currentFocus = (x.length - 1);
                                        x[currentFocus].classList.add("autocomplete-active");
                                    }
                                    function removeActive(x) {
                                        for (var i = 0; i < x.length; i++) {
                                            x[i].classList.remove("autocomplete-active");
                                        }
								  }
                                    function closeAllLists(elmnt) {
                                        var x = document.getElementsByClassName("autocomplete-items");
                                        for (var i = 0; i < x.length; i++) {
                                            if (elmnt != x[i] && elmnt != inp) {
                                                x[i].parentNode.removeChild(x[i]);
                                            }
                                        }
								  }
                                    document.addEventListener("click", function (e) {
                                        closeAllLists(e.target);
                                    });
                                }
								autocomplete(document.getElementById("txtStreetName"), streetsJSON);
							</script>
						</div>
						<div class="row saved-address-information">
							<div class="col-xs-12">
								<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBlKTwub-U-QlBh4s8bv-9fPAICyYFh3xc&libraries=places,geometry" async defer></script>
								<div>
								  <div>
									<div id="map"></div>
								  </div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div id="divStoreSearchResultError" class="col-xs-12 restaurant-search-result-error" style="display:none;">
			<h2 class="text-danger">Lo sentimos. Por ahora no tenemos delivery hasta tu dirección.</h2>
			<div class="clearfix divider"><div></div></div>
		</div>


		<div id="divResultDetails" class="col-xs-12 restaurant-detail" style="display:none;">
            <div class="col-xs-12 col-lg-6">
                <span class="icon icon-location"></span>
                <h3 class="restaurant-name"></h3>
                <p class="restaurant-address">
                </p>
				<p class="restaurant-phone">
                </p>
            </div>
            <div class="col-xs-12 col-lg-6 restaurant-order">
                <div class="row">
                    <div class="col-xs-12 col-md-6 col-lg-6 search-result-btn">
                        <input type="button" class="btn btn-lg btn-block btn-secondary" value="Ver Detalles">
                        <input type="hidden" name="OrderMode" id="OrderMode">

                    </div>
                    <div class="col-xs-12 col-md-6 col-lg-6 search-result-btn">
                        <input type="hidden" id="tienda_id" name="tienda_id" value="">
                        <button type="submit" class="btn btn-lg btn-block btn-default btn-pedir">
                            Pedir Ahora
                        </button>
                    </div>
                </div>
            </div>
        </div>

	</body>
</html>
@endsection

