<div class="collection-leftsidebar sidebar col-sm-3 clearfix">
    <div class="collection-leftsidebar-group">
        <div class="sidebar-block collection-block">
            <h3 class="sidebar-title">
                <span class="text">Categorías</span>
                <span class="cs-icon icon-minus"></span>
            </h3>
            <div class="sidebar-content">
                <ul class="list-cat">
                    <li>
                        <a href="{{route('productos')}}">Todos</a>
                    </li>
                    @foreach(\App\Models\Icategoria::web()->get() as $cat)
                        <li>
                            <a href="{{route('productos').'?category='.$cat->id}}">{{$cat->nombre}}</a>
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>
        <div class="sidebar-block latest-block">
            <h3 class="sidebar-title">
                <span class="text">Lo último</span>
                <span class="cs-icon icon-minus"></span>
            </h3>
            <div class="sidebar-content">
                <div class="latest-inner">
                    @foreach(\App\Models\Item::topTen()->take(3)->get() as $item)
                    <div class="products-item">
                        <div class="row-container product-circle">
                            <div class="product home_product">
                                <div class="row-left">
                                    <div class="hoverBorderWrapper">
                                        <img src="{{$item->imagen}}" class="not-rotation img-responsive front" alt="Coke">
                                        <div class="mask"></div>
                                    </div>
                                </div>
                                <div class="row-right">
                                    <div class="product-title">
                                        {{$item->nombre}}
                                    </div>
                                    <div class="product-price">
                                        <span class="price_sale">
                                            <span class="money" >{{dvs().nfp($item->precio_venta)}}</span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                        @endforeach
                </div>
            </div>
        </div>


        <div class="sidebar-block banner-block">
            <div class="sidebar-content">
                    <img src="cliente/images/miercoles.jpg" alt="image product" title="">
            </div>
        </div>
        <div class="sidebarMobile sidebar-bottom">
            <button class="sidebarMobile-clear _btn">
                Clear All
            </button>
            <button class="sidebarMobile-close _btn">
                Apply &amp; Close
            </button>
        </div>
    </div>
</div>