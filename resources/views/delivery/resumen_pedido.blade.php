<!DOCTYPE html>
<!--[if IE 8 ]>    <html lang="en" class="no-js ie8"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en" class="no-js"> <!--<![endif]-->


<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="canonical" href="http://demo.designshopify.com/" />
    <meta name="theme-color" content="#7796A8">
    <meta name="description" content="" />
    <title>
        Resumen del Pedido - Fritomanía
    </title>

    <link href="https://fonts.googleapis.com/css?family=Josefin+Sans:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Playball:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Bitter:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

    <link href="assets/stylesheets/bootstrap.min.css" rel="stylesheet" type="text/css" media="all">
    <link href="../../maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" media="all">
    <link href="assets/stylesheets/fonts.googleapis.css" rel="stylesheet" type="text/css" media="all">
    <link href="assets/stylesheets/font-awesome.min.css" rel="stylesheet" type="text/css" media="all">
    <link href="assets/stylesheets/icon-font.min.css" rel="stylesheet" type="text/css" media="all">
    <link href="assets/stylesheets/social-buttons.css" rel="stylesheet" type="text/css" media="all">
    <link href="assets/stylesheets/cs.styles.css" rel="stylesheet" type="text/css" media="all">
    <link href="assets/stylesheets/font-icon.css" rel="stylesheet" type="text/css" media="all">
    <link href="assets/stylesheets/owl.carousel.min.css" rel="stylesheet" type="text/css" media="all">
    <link href="assets/stylesheets/spr.css" rel="stylesheet" type="text/css" media="all">
    <link href="assets/stylesheets/slideshow-fade.css" rel="stylesheet" type="text/css" media="all">
    <link href="assets/stylesheets/cs.animate.css" rel="stylesheet" type="text/css" media="all">
    <link href="assets/stylesheets/themepunch.revolution.css" rel="stylesheet" type="text/css" media="all">
    <link href="assets/stylesheets/jquery.fancybox.css" rel="stylesheet" type="text/css" media="all">

    <script type="text/javascript" src="assets/javascripts/jquery.min.js"></script>
    <script type="text/javascript" src="assets/javascripts/classie.js"></script>
    <script type="text/javascript" src="assets/javascripts/application-appear.js"></script>
    <script type="text/javascript" src="assets/javascripts/jquery.themepunch.plugins.min.js"></script>
    <script type="text/javascript" src="assets/javascripts/jquery.themepunch.revolution.min.js"></script>
    <script type="text/javascript" src="assets/javascripts/cs.script.js"></script>
    <script type="text/javascript" src="assets/javascripts/jquery.currencies.min.js"></script>
    <script type="text/javascript" src="assets/javascripts/jquery.zoom.min.js"></script>
    <script type="text/javascript" src="assets/javascripts/linkOptionSelectors.js"></script>
    <script type="text/javascript" src="assets/javascripts/owl.carousel.min.js"></script>
    <script type="text/javascript" src="assets/javascripts/scripts.js"></script>
    <script type="text/javascript" src="assets/javascripts/social-buttons.js"></script>
    <script type="text/javascript" src="assets/javascripts/bootstrap.min.js"></script>
    <script type="text/javascript" src="assets/javascripts/jquery.touchSwipe.min.js"></script>
    <script type="text/javascript" src="assets/javascripts/jquery.fancybox.js"></script>

</head>

<body class="fastfood_1" >
@include('layouts.partials_cliente.mainheader')
<!--Header-->

<div class="fix-sticky"></div>

<!-- Main Content -->
<div class="page-container" id="PageContainer">
    <main class="main-content" id="MainContent" role="main">
        <section class="heading-content">
            <div class="heading-wrapper">
                <div class="container">
                    <div class="row">
                        <div class="page-heading-inner heading-group">
                            <div class="breadcrumb-group">
                                <h1 class="hidden">Resumen del Pedido</h1>
                                <div class="breadcrumb clearfix">
										<span itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><a href="index-2.html" title="Fast Food" itemprop="url"><span itemprop="title"><i class="fa fa-home"></i></span></a>
										</span>
                                    <span class="arrow-space"></span>
                                    <span itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
											<a href="order.html" title="Order History" itemprop="url"><span itemprop="title">Resumen del Pedido</span></a>
										</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="order-layout">
            <div class="order-wrapper">
                <div class="container">
                    <div class="row">
                        <div class="order-inner">
                            <div class="order-content">
                                <div class="order-id">
                                    <h2>Pedido #1002</h2>
                                    <span class="date">8 Ago 15:15</span>
                                    <div id="order-cancelled" class="flash notice">
                                        <h5 id="order-cancelled-title">Pedido Cancelado el <span class="note">8 Ago 15:30</span></h5>
                                        <span class="note">Motivo: cliente</span>
                                    </div>
                                </div>
                                <div class="order-address">
                                    <div id="order_payment" class="col-md-6 address-items">
                                        <h2 class="address-title">Datos del Pedido</h2>
                                        <div class="address-content">
                                            <div class="address-item">
                                                <span class="title">Estado:</span>
                                                <span class="content">Pagado</span>
                                            </div>
                                            <div class="address-item name">
                                                <span class="title">Nombre:</span>
                                                <span class="content">Juán Perez</span>
                                            </div>
                                            <div class="address-item">
                                                <span class="title">Teléfono:</span>
                                                <span class="content">11111</span>
                                            </div>
                                            <div class="address-item">
                                                <span class="title">Dirección:</span>
                                                <span class="content">Đường trục, Binh Thanh</span>
                                            </div>
                                            <div class="address-item">
                                                <span class="title">Comuna:</span>
                                                <span class="content">Las Condes</span>
                                            </div>
                                            <div class="address-item">
                                                <span class="title">Zip / Código Postal:</span>
                                                <span class="content">2340000</span>
                                            </div>
                                        </div>
                                    </div>


                                </div>
                                <div class="order-info">
                                    <div class="order-info-inner">
                                        <table id="order_details">
                                            <thead>
                                            <tr>
                                                <th>Producto</th>
                                                <th>SKU</th>
                                                <th>Precio</th>
                                                <th class="center">Cantidad</th>
                                                <th class="total">Total</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr id="10324769618" class="odd">
                                                <td class="td-product">
                                                    <a href="product.html" title="">Lorem Ipsum tizzy 6</a>
                                                </td>
                                                <td class="sku note">CA78977</td>
                                                <td class="money"><span class="money" data-currency-usd="$259.00" data-currency="USD">$259.00</span></td>
                                                <td class="quantity ">1</td>
                                                <td class="total"><span class="money" data-currency-usd="$259.00" data-currency="USD">$259.00</span></td>
                                            </tr>
                                            <tr id="10324769682" class="even">
                                                <td class="td-product">
                                                    <a href="product.html" title="">Lorem Ipsum tizzy 14</a>
                                                </td>
                                                <td class="sku note">CA78977</td>
                                                <td class="money"><span class="money" data-currency-usd="$259.00" data-currency="USD">$259.00</span></td>
                                                <td class="quantity ">2</td>
                                                <td class="total"><span class="money" data-currency-usd="$518.00" data-currency="USD">$518.00</span></td>
                                            </tr>
                                            <tr id="10324769746" class="odd">
                                                <td class="td-product">
                                                    <a href="product.html" title="">Lorem Ipsum tizzy 1</a>
                                                </td>
                                                <td class="sku note">CA78977</td>
                                                <td class="money"><span class="money" data-currency-usd="$259.00" data-currency="USD">$259.00</span></td>
                                                <td class="quantity ">2</td>
                                                <td class="total"><span class="money" data-currency-usd="$518.00" data-currency="USD">$518.00</span></td>
                                            </tr>
                                            <tr id="10324769810" class="even">
                                                <td class="td-product">
                                                    <a href="product.html" title="">Lorem Ipsum tizzy 12</a>
                                                </td>
                                                <td class="sku note">CA78977</td>
                                                <td class="money"><span class="money" data-currency-usd="$259.00" data-currency="USD">$259.00</span></td>
                                                <td class="quantity ">2</td>
                                                <td class="total"><span class="money" data-currency-usd="$518.00" data-currency="USD">$518.00</span></td>
                                            </tr>
                                            <tr id="10324769874" class="odd">
                                                <td class="td-product">
                                                    <a href="product.html" title="">Lorem Ipsum tizzy 7 - S / Black</a>
                                                </td>
                                                <td class="sku note">CA78963</td>
                                                <td class="money"><span class="money" data-currency-usd="$259.00" data-currency="USD">$259.00</span></td>
                                                <td class="quantity ">2</td>
                                                <td class="total"><span class="money" data-currency-usd="$518.00" data-currency="USD">$518.00</span></td>
                                            </tr>
                                            <tr id="10324769938" class="even">
                                                <td class="td-product">
                                                    <a href="product.html" title="">Lorem Ipsum tizzy 11</a>
                                                </td>
                                                <td class="sku note">CA78977</td>
                                                <td class="money"><span class="money" data-currency-usd="$259.00" data-currency="USD">$259.00</span></td>
                                                <td class="quantity ">3</td>
                                                <td class="total"><span class="money" data-currency-usd="$777.00" data-currency="USD">$777.00</span></td>
                                            </tr>
                                            <tr id="10324770002" class="odd">
                                                <td class="td-product">
                                                    <a href="product.html" title="">Lorem Ipsum tizzy 8</a>
                                                </td>
                                                <td class="sku note">CA78977</td>
                                                <td class="money"><span class="money" data-currency-usd="$259.00" data-currency="USD">$259.00</span></td>
                                                <td class="quantity ">3</td>
                                                <td class="total"><span class="money" data-currency-usd="$777.00" data-currency="USD">$777.00</span></td>
                                            </tr>
                                            <tr id="10324770066" class="even">
                                                <td class="td-product">
                                                    <a href="product.html" title="">Lorem Ipsum tizzy 9 - S / Black</a>
                                                </td>
                                                <td class="sku note">CA78963</td>
                                                <td class="money"><span class="money" data-currency-usd="$259.00" data-currency="USD">$259.00</span></td>
                                                <td class="quantity ">7</td>
                                                <td class="total"><span class="money" data-currency-usd="$1,813.00" data-currency="USD">$1,813.00</span></td>
                                            </tr>
                                            </tbody>
                                            <tfoot>
                                            <tr class="order_summary note">
                                                <td class="td-label" colspan="4">Subtotal</td>
                                                <td class="subtotal"><span class="money" data-currency-usd="$5,698.00" data-currency="USD">$5,698.00</span></td>
                                            </tr>

                                            <tr class="order_summary note">
                                                <td class="td-label" colspan="4">IVA:</td>
                                                <td class="vat"><span class="money" data-currency-usd="$569.80" data-currency="USD">$569.80</span></td>
                                            </tr>
                                            <tr class="order_summary order_total">
                                                <td class="td-label" colspan="4">Total</td>
                                                <td class="total"><span class="money" data-currency-usd="$6,277.80" data-currency="USD">$6,277.80</span> </td>
                                            </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
</div>

<!-- Footer -->


<!-- Modal Search-->
<div class="modal fade" id="lightbox-search" tabindex="-1" role="dialog" aria-labelledby="lightbox-search" aria-hidden="true" style="display: none;">
    <div class="modal-dialog animated" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span class="cs-icon icon-close"></span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Search something</h4>
            </div>
            <div class="modal-body">
                <div id="search-info">
                    <form class="search" action="http://demo.designshopify.com/html_fastfood/search.html" style="position: relative;">
                        <input type="hidden" name="type" value="product">
                        <input type="text" name="q" class="search_box" placeholder="search our store" value="" autocomplete="off">
                        <span class="search-clear cs-icon icon-close" style="display: none;"></span>
                        <button class="search-submit" type="submit">
                            <span class="cs-icon icon-search"></span>
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@include('layouts.partials_cliente.footer')

@include('layouts.partials_cliente.modals_home')

<!-- Float right icon -->
<div class="float-right-icon">
    <ul>
        <li>
            <div id="scroll-to-top" data-toggle="" data-placement="left" title="Scroll to Top" class="off">
                <i class="fa fa-angle-up"></i>
            </div>
        </li>
    </ul>
</div>



</body>