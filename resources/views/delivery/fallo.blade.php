@extends('layouts.marco_cliente')

@push('css')
<link href="{{asset('cliente/css/locales.css')}}" rel="stylesheet" type="text/css" media="all">
@endpush

@section('content')
    <div class="page-container" id="PageContainer">
        <main class="main-content" id="MainContent" role="main">
            @include('components.web.heading',['ruta' => route('locales'),'titulo' => 'Fallo','tienda' => ''])

            @include('flash::message')
            <section class="collection-layout">
                <div class="collection-wrapper">
                    {!! Form::open(['route' => 'pedir.ahora','method' =>'post']) !!}
                    <div class="container">


                        <div class="row">
                            <div class="col-sm-12">
                                <div class="row tipopedido">Error en la compra
                                    <p style="font-size: 16px;">(El error con el código va aquí)</p>
                                </div>

                            </div>
                        </div>


                        <!--            Resumen compra
                        ------------------------------------------------------------------------>

                        <div class="row">
                            <div class="col-sm-4"></div>
                            <div class="col-sm-4"></div>
                            <div class="col-sm-4"><button class="btn botones" id="pideahora"> INTENTAR DE NUEVO </button></div>
                        </div>

                        <!--            Fila tipo pedido retiro local
                        ------------------------------------------------------------------------>







                    </div>
                    {!! Form::close() !!}
                </div>
            </section>
        </main>

    </div>

    @include('layouts.axios')



@endsection