@extends('layouts.marco_cliente')

@push('css')
<link href="{{asset('cliente/css/locales.css')}}" rel="stylesheet" type="text/css" media="all">
@endpush

@section('content')
    <div class="page-container" id="PageContainer">
        <main class="main-content" id="MainContent" role="main">
            @include('components.web.heading',['ruta' => route('locales'),'titulo' => 'Buscar Local','tienda' => ''])


            <section class="collection-layout">
                <div class="collection-wrapper">
                    {!! Form::open(['route' => 'pedir.ahora','method' =>'post']) !!}
                    <div class="container">
                        @include('flash::message')

                        <!-- Fila selecciona tipo pedido -->
                        @if(!config('app.solo_delivery_web'))
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="row tipopedido" style="font-weight: bold;">Selecciona tu tipo de pedido</div>
                                <div class="row tipopedido">
                                    <div class="col-sm-6" style="margin: 50px 0px;">
                                        <a href="javascript:void(0)" id="btnAbreRetiroLocal" class="btn btn-danger">
                                            <i class="fa fa-university" aria-hidden="true" style="font-size: 48px;"></i><br>Retiro en Local
                                        </a>

                                    </div>
                                    <div class="col-sm-6" style="margin: 50px 0px;">
                                        <a href="javascript:void(0)" id="btnAbreDelivery" class="btn btn-danger">
                                            <i class="fa fa-motorcycle" aria-hidden="true" style="font-size: 48px; padding:0 20px;"></i><br>Delivery
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                            @else
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="row tipopedido" style="font-weight: bold;">Ingrese sus datos</div>
                                    </div>
                                </div>
                            @endif



                        <div class="row" >

                            <!--            Fila tipo pedido delivery
                            ------------------------------------------------------------------------>
                            <div id="divDelivery" class="col-sm-6" style="display: none;">
                                    <div id="deliverySearchForm">
                                        <!-- Comunas -->
                                        <div class="col-xs-12 ">
                                            <label class="control-label">Comuna<sup>*</sup></label>
                                            <select onchange="dllInitAutocomplete(this);" class="form-control valid" id="ddlState" name="comuna">
                                                <option selected="selected" value="Buin">Buin</option>
                                                <option value="Colina">Colina</option>
                                                <option value="Concepción">Concepción</option>
                                                <option value="Conchali">Conchali</option>
                                                <option value="Concón">Concón</option>
                                                <option value="Estacion Central">Estacion Central</option>
                                                <option value="Huechuraba">Huechuraba</option>
                                                <option value="Independencia">Independencia</option>
                                                <option value="La Cisterna">La Cisterna</option>
                                                <option value="La Florida">La Florida</option>
                                                <option value="La Reina">La Reina</option>
                                                <option value="La Serena">La Serena</option>
                                                <option value="Las Condes">Las Condes</option>
                                                <option value="Linares">Linares</option>
                                                <option value="Lo Barnechea">Lo Barnechea</option>
                                                <option value="Los Andes">Los Andes</option>
                                                <option value="Los Angeles">Los Angeles</option>
                                                <option value="Machalí">Machalí</option>
                                                <option value="Macul">Macul</option>
                                                <option value="Maipú">Maipú</option>
                                                <option value="Melipilla">Melipilla</option>
                                                <option value="Ñuñoa">Ñuñoa</option>
                                                <option value="Osorno">Osorno</option>
                                                <option value="Peñalolén">Peñalolén</option>
                                                <option value="Providencia">Providencia</option>
                                                <option value="Pudahuel">Pudahuel</option>
                                                <option value="Puente Alto">Puente Alto</option>
                                                <option value="Puerto Montt">Puerto Montt</option>
                                                <option value="Quilicura">Quilicura</option>
                                                <option value="Quillota">Quillota</option>
                                                <option value="Quilpue">Quilpue</option>
                                                <option value="Quinta Normal">Quinta Normal</option>
                                                <option value="Rancagua">Rancagua</option>
                                                <option value="Recoleta">Recoleta</option>
                                                <option value="San Bernardo">San Bernardo</option>
                                                <option value="San Miguel">San Miguel</option>
                                                <option value="San Pedro De La Paz">San Pedro De La Paz</option>
                                                <option value="Santiago">Santiago</option>
                                                <option value="Talca">Talca</option>
                                                <option value="Talcahuano">Talcahuano</option>
                                                <option value="Temuco">Temuco</option>
                                                <option value="Valdivia">Valdivia</option>
                                                <option value="Valparaíso">Valparaíso</option>
                                                <option value="Villa Alemana">Villa Alemana</option>
                                                <option value="Viña del mar">Viña del mar</option>
                                                <option value="Vitacura">Vitacura</option>
                                            </select>
                                        </div>
                                        
                                        <!-- Calles -->
                                        <div class="col-xs-12 autocomplete">
                                            <label class="control-label">Calle<sup>*</sup></label>
                                            <input autocomplete="off" class="form-control typeahead" id="txtStreetName" placeholder="Calle" type="text" value=""  validMessage="Por favor escriba el nombre de la calle.">
                                            <input type="hidden" id="calle" name="calle" value="" min="1">
                                            <span id="streetValidMessage" class="field-validation-valid text-danger help-block"></span>
                                        </div>

                                        <!-- numero domicilio -->
                                        <div class="col-xs-12 ">
                                            <label class="control-label">Número Domicilio<sup>*</sup></label>
                                            <input class="form-control" id="txtStreetNumber" max="9999999" min="1" name="numero_domicilio" placeholder="Número Domicilio" type="number" value="" validMessage="Por favor escriba el número." maxValueMessage="Por favor ingresar un valor menor o igual a 9999999">
                                            <span id="streetNumberValidMessage" class="field-validation-valid text-danger help-block"></span>
                                        </div>

                                        <!-- depto -->
                                        <div class="col-xs-12 ">
                                            <label class="control-label">Depto/Casa</label>
                                            <input class="form-control" data-val="true" min="1"
                                                   id="txtApartment" name="departamento" placeholder="Depto" type="text" value="">
                                            <span id="apartmentValidMessage" class="field-validation-valid text-danger help-block"></span>
                                        </div>

                                        <!-- nombre -->
                                        <div class="col-xs-12 ">
                                            <label class="control-label">Nombre completo</label>
                                            <input type="text" maxlength="255" class="form-control req_delivery" id="nombre_completo" name="name" required>
                                        </div>

                                        <!-- telefono -->
                                        <div class="col-xs-12 ">
                                            <label class="control-label">Celular</label>
                                            <input data-inputmask="'mask': '+56 (9) 9999-9999'" required="" type="tel" class="form-control req_delivery"
                                                   name="telefono" data-type="tel" aria-required="true" id="celular" >
                                        </div>

                                        <!-- correo -->
                                        <div class="col-xs-12 ">
                                            <label class="control-label">Correo</label>
                                            <input type="email" name="correo" maxlength="255" class="form-control req_delivery" required id="correo">
                                        </div>

                                        <!-- observasiones -->
                                        <div class="col-xs-12">
                                            <label class="control-label">Observaciones de entrega</label>
                                            <input class="form-control" maxValueMessage="Límite de 80 caracteres" id="txtComments" name="observaciones" placeholder="Observaciones de entrega" type="text" value="">
                                            <div class="observationMessage">(*No es posible programar pedidos para más adelante ni solicitar descuentos en este cuadro. Por favor no solicitarlos.)</div>
                                            <span id="commentsValidMessage" class="field-validation-valid text-danger help-block"></span>
                                        </div>
                                        <div>
                                            <input data-val="true" data-val-required="The IsReqdValidation field is required."  type="hidden" value="True">
                                        </div>
                                        <div>
                                            <input data-val="true" data-val-length="Límite de 80 caracteres" data-val-length-max="80"
                                                   data-val-requiredif="Por favor introduce el nombre del lugar."
                                                   data-val-requiredif-dependentproperty="IsReqdValidation"
                                                   data-val-requiredif-desiredvalue="true"
                                                   type="hidden" value="DeliverySearch">
                                        </div>
                                        <div class="col-xs-12 col-md-6 ">
                                            <input type="button" onclick="searchUserLocation();" class="btn btn-lg btn-block btn-danger" value="Encontrar Local"/>
                                        </div>
                                    </div>
                            </div>

                            <!--            Fila tipo pedido retiro local
                            ------------------------------------------------------------------------>
                            <div id="divRetiroLocal" class="col-sm-6" style="display: none;">
                                <form autocomplete="off" data-toggle="validator" role="form">
                                    <div id="deliverySearchForm" >

                                        <div class="col-sm-12 ">
                                            <label class="control-label">Calle, Número o Comuna<sup>*</sup></label>
                                            <input autocomplete="off" class="form-control typeahead" id="direccion_retiro_local"  name="direccion_retiro_local" placeholder="Calle" type="text" value="" maxlength="255"  validMessage="Por favor escriba el nombre de la calle.">
                                            <span id="streetValidMessage" class="field-validation-valid text-danger help-block"></span>
                                        </div>

                                        <!-- nombre -->
                                        <div class="col-sm-12 ">
                                            <label class="control-label">Nombre completo</label>
                                            <input type="text" maxlength="255" class="form-control req_retiro" name="name_rl" required>
                                        </div>

                                        <!-- telefono -->
                                        <div class="col-sm-12 ">
                                            <label class="control-label">Celular</label>
                                            <input data-inputmask="'mask': '+56 (9) 9999-9999'" required="" type="tel" class="form-control req_retiro"
                                                   name="telefono_rl" data-type="tel" aria-required="true" >
                                        </div>

                                        <!-- correo -->
                                        <div class="col-sm-12 ">
                                            <label class="control-label">Correo</label>
                                            <input type="email" name="correo_rl" maxlength="255" class="form-control req_retiro" required>
                                        </div>

                                        <!-- observasiones -->
                                        <div class="col-sm-12 ">
                                            <label class="control-label">Observaciones de entrega</label>
                                            <input class="form-control" maxValueMessage="Límite de 80 caracteres" name="observaciones_rl" placeholder="Observaciones de entrega" type="text" value="">
                                            <div class="observationMessage">(*No es posible programar pedidos para más adelante ni solicitar descuentos en este cuadro. Por favor no solicitarlos.)</div>
                                            <span id="commentsValidMessage" class="field-validation-valid text-danger help-block"></span>
                                        </div>


                                        <div><input data-val="true" data-val-required="The IsReqdValidation field is required."  type="hidden" value="True"></div>
                                        <div><input data-val="true" data-val-length="Límite de 80 caracteres" data-val-length-max="80" data-val-requiredif="Por favor introduce el nombre del lugar." data-val-requiredif-dependentproperty="IsReqdValidation" data-val-requiredif-desiredvalue="true" type="hidden" value="DeliverySearch"></div>
                                        <div class="col-xs-12 col-md-6" style="margin-top:20px;">
                                            <input type="button" onclick="searchUserLocation2();" class="btn btn-lg btn-block btn-danger" value="Encontrar Local"/>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="col-sm-6 saved-address-information" >
                                <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBlKTwub-U-QlBh4s8bv-9fPAICyYFh3xc&libraries=places,geometry" async defer></script>
                                <div>
                                    <div>
                                        <div id="map"></div>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div id="divStoreSearchResultError" class="col-xs-12 restaurant-search-result-error" style="display:none;">
                            <h2 class="text-danger">Lo sentimos. Por ahora no tenemos delivery hasta tu dirección.</h2>
                            <div class="clearfix divider"><div></div></div>
                        </div>

                        <hr>
                        <div id="divResultDetails" class="row" style="display:none;">
                            <div class="col-sm-6">
                                <span class="icon icon-location"></span>
                                <h3 class="restaurant-name"></h3>
                                <p class="restaurant-address"></p>
                                <p class="restaurant-phone"></p>
                                <p id="distanceLocal"></p>
                            </div>
                            <div class="col-sm-6">
                                <div class="row">
                                    <div class="col-xs-12 col-md-6 col-lg-6 search-result-btn">

                                        <a href="{{route('local.detalle',0)}}"  id="btnDetalleLocal" class="btn btn-lg btn-block btn-danger" style="margin-top: 20px;" >Ver Detalles</a>
                                        <input type="hidden" name="OrderMode" id="OrderMode" value="1">
                                        <input type="hidden" name="distancia" id="InputDistanciaLocal" value="0">
                                    </div>
                                    <div class="col-xs-12 col-md-6 col-lg-6 search-result-btn" style="margin-top: 20px;">
                                        <input type="hidden" id="tienda_id" name="tienda_id" value="" >
                                        <button type="submit" class="btn btn-lg btn-block btn-danger" id="btnPedirAhora">
                                            Pedir Ahora
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                    {!! Form::close() !!}
                </div>
            </section>
        </main>

    </div>

    @include('layouts.axios')
    @include('layouts.inputmask')
    <script>
        var streetsJSON;
        function autocomplete(inp, arr) {
            var currentFocus;
            inp.addEventListener("input", function(e) {
                if(!arr) return false;
                var a, b, i, val = this.value;
                closeAllLists();
                if (!val) { return false;}
                currentFocus = -1;
                a = document.createElement("DIV");
                a.setAttribute("id", this.id + "autocomplete-list");
                a.setAttribute("class", "autocomplete-items");
                this.parentNode.appendChild(a);
                for (i = 0; i < arr.length; i++) {
                    if (arr[i].substr(0, val.length).toUpperCase() == val.toUpperCase()) {
                        b = document.createElement("DIV");
                        b.innerHTML = "<strong>" + arr[i].substr(0, val.length) + "</strong>";
                        b.innerHTML += arr[i].substr(val.length);
                        b.innerHTML += "<input type='hidden' value='" + arr[i] + "'>";
                        b.addEventListener("click", function(e) {
                            inp.value = this.getElementsByTagName("input")[0].value;
                            closeAllLists();
                        });
                        a.appendChild(b);
                    }
                }
            });
            inp.addEventListener("keydown", function(e) {
                var x = document.getElementById(this.id + "autocomplete-list");
                if (x) x = x.getElementsByTagName("div");
                if (e.keyCode == 40) {
                    currentFocus++;
                    addActive(x);
                } else if (e.keyCode == 38) {
                    currentFocus--;
                    addActive(x);
                } else if (e.keyCode == 13) {
                    e.preventDefault();
                    if (currentFocus > -1) {
                        if (x) x[currentFocus].click();
                    }
                }
            });
            function addActive(x) {
                if (!x) return false;
                removeActive(x);
                if (currentFocus >= x.length) currentFocus = 0;
                if (currentFocus < 0) currentFocus = (x.length - 1);
                    x[currentFocus].classList.add("autocomplete-active");
            }
            function removeActive(x) {
                for (var i = 0; i < x.length; i++) {
                    x[i].classList.remove("autocomplete-active");
                }
            }
            function closeAllLists(elmnt) {
                var x = document.getElementsByClassName("autocomplete-items");
                for (var i = 0; i < x.length; i++) {
                    if (elmnt != x[i] && elmnt != inp) {
                        x[i].parentNode.removeChild(x[i]);
                    }
                }
            }
            document.addEventListener("click", function (e) {
                closeAllLists(e.target);
            });
        }
        autocomplete(document.getElementById("txtStreetName"), streetsJSON);
    </script>
    <script>
        var map;
        var service;
        var infowindow;
        var userLoc;
        var myLatLng;
        var streetsJSON;
        var nearLocationObject;
        var distanceLocal = 0;

        $(document).ready(function(){
            dllInitAutocomplete({'value':'Buin'});
            $(".close-link").click(function() {
                $("#sectionDeliveryInformation").hide(1000);
            });
        });
        //Loc Santiago default
        var defLoc = {lat: -33.460450, lng: -70.760730};
        function initMap() {
            map = new google.maps.Map(document.getElementById('map'), {
                center: defLoc,
                zoom: 10,
                disableDefaultUI: true,
                zoomControl: true,
                fullscreenControl: true,
                zoomControlOptions: {
                    position: google.maps.ControlPosition.LEFT_BOTTOM
                },
            });

            infowindow = new google.maps.InfoWindow();
            var service = new google.maps.places.PlacesService(map);
            service.textSearch({
                location: userLoc,
                //query:"papa john's",
                query: "fritomania restaurantes"
            }, callback);
        }
        function callback(results, status) {
            if (status === google.maps.places.PlacesServiceStatus.OK) {
                map.zoom = 17;
                var posMinDistance = 0;
                var minDistance = 0;
                for (var i = 0; i < results.length; i++) {
                    if(results[i].name == "Fritomania")
                    {
                        //createMarker(results[i]);
                        var distance = Math.round(google.maps.geometry.spherical.computeDistanceBetween(userLoc, results[i].geometry.location));

                        if(i==0){
                            minDistance = distance;
                        }
                        else if(distance < minDistance){
                            posMinDistance = i;
                            minDistance = distance;
                        }
                    }
                }
                if(minDistance>0)
                {
                    distanceLocal = parseFloat((minDistance/1000).toFixed(2));
                }
                if(Math.round(google.maps.geometry.spherical.computeDistanceBetween(userLoc, results[posMinDistance].geometry.location)) <= 50000)
                {
                    createMarker(results[posMinDistance]);
                    var placeLoc = {lat: results[posMinDistance].geometry.location.lat(),lng:results[posMinDistance].geometry.location.lng()};
                    map.setCenter(placeLoc);
                    $("#divStoreSearchResultError").hide();
                    $("#divResultDetails").show();
                }
                else{
                    $("#divStoreSearchResultError").show();
                    $("#divResultDetails").hide();
                }

            }
        }

        function createMarker(place) {
            pedirAhora(place.place_id);//no borrar
            var placeLoc = place.geometry.location;
            var marker = new google.maps.Marker({
                map: map,
                position: place.geometry.location
            });

            var service = new google.maps.places.PlacesService(map);
            var request = { reference: place.reference };
            service.getDetails(request, function(details, status) {
                google.maps.event.addListener(marker, 'click', function() {
                    infowindow.setContent("<span class='restaurant-name'>"+place.name+"</span><br/>"
                        +"<br/><table style='border-bottom:1pt solid black;'><tr><th rowspan='3'><img style='width:80px;height:40px;' src='https://fritomania.cl/cliente/images/logo_nuevo.png'/></th><th align='left'><b>Horario</b></th><th/></tr>"
                        +"<tr><td>Lunes a Viernes</td><td width=10/><td>12:00 pm - 12:00 am<td></tr>"
                        +"<br/><tr><td>Sabados y Domingos</td><td width=10/><td>11:00 am - 12:00 am<td></tr><tr height=10></tr>"
                        +"</table>"
                        +"<br/><span style='color:red;font-weight:bold;'>El valor mínimo para delivery es ???</span>");
                    infowindow.open(map, this);
                });
                $(".restaurant-name").html(place.name);
                $(".restaurant-address").html(place.formatted_address);
                $("#distanceLocal").html("a "+distanceLocal+" KM.");
                $("#InputDistanciaLocal").val(distanceLocal);

                if(details.formatted_phone_number)
                {
                    $(".restaurant-phone").html("<a href='tel:"+details.formatted_phone_number+"'>"+details.formatted_phone_number+"</a>");
                }

                var openingHours = "";

                if(details.opening_hours && details.opening_hours.weekday_text){
                    var dayOfWeek = new Date().getDay();
                    if(dayOfWeek == 0)dayOfWeek = 6;else dayOfWeek = dayOfWeek-1;
                    openingHours = details.opening_hours.weekday_text[dayOfWeek];
                }
                addressDetail = {
                    street:$("<div>"+details.adr_address+"</div>").find("span.street-address").text(),
                    locality:$("<div>"+details.adr_address+"</div>").find("span.locality").text(),
                    region:$("<div>"+details.adr_address+"</div>").find("span.region").text(),
                    country:$("<div>"+details.adr_address+"</div>").find("span.country-name").text(),
                };
                nearLocationObject = {
                    phone:details.formatted_phone_number,
                    name:place.name,
                    address:addressDetail,
                    openingHours: openingHours,
                    streetInput : $("#txtStreetName").val(),
                    districtInput: $("#ddlState").val(),
                    streetNumberInput: $("#txtStreetNumber").val(),
                    apartmentInput: $("#txtApartment").val(),
                    url: details.url,
                    distanceLocal: "a "+distanceLocal+" KM."
                };
                // Store
                localStorage.setItem("nearLocationObject", JSON.stringify(nearLocationObject));
            });
        }

        //Logica del boton
        function searchUserLocation(){
            console.log('searchUserLocation');
            if(!validateForm()){
                return false;
            }
            var address = $("#ddlState").val()+", "+$("#txtStreetName").val()+" "+$("#txtStreetNumber").val();
            var geocoder = new google.maps.Geocoder();
            geocoder.geocode({'address': address}, function(results, status) {
                if (status === 'OK') {
                    //$("#divStoreSearchResultError").hide();
                    //$("#divResultDetails").show();
                    userLoc = results[0].geometry.location;
                    initMap();

                }
                else {
                    //alert('Geocode was not successful for the following reason: ' + status);
                    $("#divStoreSearchResultError").show();
                    $("#divResultDetails").hide();
                }
            });
        }

        function searchUserLocation2(){
            console.log('searchUserLocation2');

            var address = $("#direccion_retiro_local").val();
            var geocoder = new google.maps.Geocoder();
            geocoder.geocode({'address': address}, function(results, status) {
                if (status === 'OK') {
                    //$("#divStoreSearchResultError").hide();
                    //$("#divResultDetails").show();
                    userLoc = results[0].geometry.location;
                    initMap();

                }
                else {
                    //alert('Geocode was not successful for the following reason: ' + status);
                    $("#divStoreSearchResultError").show();
                    $("#divResultDetails").hide();
                }
            });
        }

        function dllInitAutocomplete(item){
            $.getJSON("js/streets/chileLocation_"+$(item)[0].value+".json", function(data){
                streetsJSON = data.STREETNAME;
                $("#txtStreetName").val("");
                autocomplete(document.getElementById("txtStreetName"), streetsJSON);
            });
        }
        function validateForm(){
            var valid = true;
            if($("#txtStreetName").val().trim() == ""){
                $("#streetValidMessage").text($("#txtStreetName").attr("validMessage"));
                valid = false;
            }
            else{
                $("#streetValidMessage").html("");
            }
            if($("#txtStreetNumber").val().trim() == ""){
                $("#streetNumberValidMessage").text($("#txtStreetNumber").attr("validMessage"));
                valid = false;
            }
            else{
                if(parseInt($("#txtStreetNumber").val()) > 9999999 ){
                    $("#streetNumberValidMessage").text($("#txtStreetNumber").attr("maxValueMessage"));
                    valid = false;
                }
                else{
                    $("#streetNumberValidMessage").html("");
                }
            }

            if($("#txtApartment").val().trim() != "" && $("#txtApartment").val().length > 80 ){
                $("#apartmentValidMessage").text($("#txtApartment").attr("maxValueMessage"));
                valid = false;
            }
            else{
                $("#apartmentValidMessage").html("");
            }
            if($("#txtComments").val().trim() != "" && $("#txtComments").val().length > 80 ){
                $("#commentsValidMessage").text($("#txtComments").attr("maxValueMessage"));
                valid = false;
            }
            else{
                $("#commentsValidMessage").html("");
            }



            return valid;
        }

        function showMyDeliveryInformation(){
            var getNearLocationObject = JSON.parse(localStorage.getItem("nearLocationObject"));

            var fullMyDeliveryInformation = getNearLocationObject.streetInput+", "+getNearLocationObject.streetNumberInput+", "+getNearLocationObject.districtInput;
            $("#streetInformation").html(getNearLocationObject.streetInput+", "+getNearLocationObject.streetNumberInput);
            $("#districtInformation").html(getNearLocationObject.apartmentInput.trim() == "" ? getNearLocationObject.districtInput : "Depto "+getNearLocationObject.apartmentInput+", "+getNearLocationObject.districtInput);
            $("#localNameInformation").html(getNearLocationObject.name);
            $("#address-street").html(getNearLocationObject.address.street);
            $("#localityRegion").html(getNearLocationObject.address.locality);
            $("#cellphoneInformation").attr("href",getNearLocationObject.phone);
            $("#cellphoneInformation").html(getNearLocationObject.phone);
            $("#horaryInformation").html(getNearLocationObject.openingHours);
            $("#fullMyDeliveryInformation").html(fullMyDeliveryInformation);
            $("#showAddressInformation").attr("href",getNearLocationObject.url);

            $("#sectionDeliveryInformation").show(1000);
            $("#myDeliveryInformation").show();

        }
    </script>
    <script>
        $(function () {

            function modoDelivery() {
                $("#divRetiroLocal").hide('fast');
                $("#divDelivery").show('slow');
                $("#OrderMode").val(1);

                //vuelve no requeridos los campos de retiro en locala
                $(".req_retiro").attr('required',false);
                //vuelve requeridos los campos de delivery
                $(".req_delivery").attr('required',true);

            }


            function modoRetiroLocal(){
                $("#divDelivery").hide('fast');
                $("#divRetiroLocal").show('slow');
                $("#OrderMode").val(2);

                //vuelve requeridos los campos de retiro en locala
                $(".req_retiro").attr('required',true);
                //vuelve no requeridos los campos de delivery
                $(".req_delivery").attr('required',false);
            }

            @if(config('app.solo_delivery_web'))
                modoDelivery();
            @endif

            $("#btnAbreDelivery").click(function () {
                console.log('Abre div delivery');
                modoDelivery();
            });

            $("#btnAbreRetiroLocal").click(function () {
                console.log('Abre div delivery');
                modoRetiroLocal();
            });
        });


        $('#btnPedirAhora').button('loading');
        $('#btnDetalleLocal').button('loading');

        function  pedirAhora(id_map){
            console.log('Fn pedir ahora',id_map);
            var localSys = {};
            var url = "{{route("get.tienda.map")}}";
            var params= { params: {id_map: id_map} };

            axios.get(url,params).then(response => {
               console.log(response.data);
                localSys = response.data.data;

                $('#tienda_id').val(localSys.id)

                var urlBtnDetalleLocal = $('#btnDetalleLocal').attr('href');
                $('#btnDetalleLocal').attr('href', urlBtnDetalleLocal.replace('0',localSys.id));



                var calle = $("#txtStreetName").val();
                $('#calle').val(calle);
                $('#btnPedirAhora').button('reset');
                $('#btnDetalleLocal').button('reset');

            })
            .catch(error => {
                console.log(error);
            });
        }
    </script>
@endsection