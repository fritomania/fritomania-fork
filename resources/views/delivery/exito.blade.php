@extends('layouts.marco_cliente')

@push('css')
<link href="{{asset('cliente/css/locales.css')}}" rel="stylesheet" type="text/css" media="all">
@endpush

@section('content')
    <div class="page-container" id="PageContainer">
        <main class="main-content" id="MainContent" role="main">
            @include('components.web.heading',['ruta' => '#','titulo' => 'Exito','tienda' => ''])

            <section class="collection-layout">
                <div class="collection-wrapper">
                    {!! Form::open(['route' => 'pedir.ahora','method' =>'post']) !!}
                    <div class="container">


                        <section class="order-layout">
                            <div class="row">
                                <img src="https://fritomania.cl/cliente/images/bienvenido.jpg" alt="">
                            </div>
                            <div class="row tipopedido">Su pedido N° {{$orden->id}} ha finalizado con éxito</div>
                            <div class="order-wrapper">
                                <div class="container">
                                    <div class="row">
                                        <div class="order-inner">
                                            <div class="order-content">
                                                <div class="order-id">
                                                    <h2>Pedido N° {{$orden->id}} <span class="pull-right">Tiempo de despacho: {{$orden->tiempoDespacho()}} min</span></h2>
                                                    <span class="date">{{$orden->created_at->format('d-m-Y')}} &nbsp; {{$orden->created_at->format('H:i:s')}}</span>
                                                    <div id="order-cancelled" class="flash notice">
                                                        <h5 id="order-cancelled-title">
                                                            Pedido pagado el
                                                            <span class="date">{{$orden->updated_at->format('d-m-Y')}} &nbsp; {{$orden->updated_at->format('H:i:s')}}</span>
                                                        </h5>
                                                    </div>
                                                </div>
                                                <div class="order-address">
                                                    <div id="order_payment" class="col-md-6 address-items">
                                                        <h2 class="address-title">Datos del Pedido</h2>
                                                        <div class="address-content">
                                                            <div class="address-item">
                                                                <span class="title">Estado:</span>
                                                                <span class="content">{{$orden->estado->nombre}}</span>
                                                            </div>
                                                            <div class="address-item name">
                                                                <span class="title">Nombre:</span>
                                                                <span class="content">{{$orden->nombre_entrega}}</span>
                                                            </div>
                                                            <div class="address-item">
                                                                <span class="title">Teléfono:</span>
                                                                <span class="content">{{$orden->telefono}}</span>
                                                            </div>
                                                            <div class="address-item">
                                                                <span class="title">Dirección:</span>
                                                                <span class="content">{{$orden->direccion}}</span>
                                                            </div>
                                                            <div class="address-item">
                                                                <span class="title">Cod WebPay:</span>
                                                                <span class="content">{{$orden->cod_web_pay}}</span>
                                                            </div>
                                                            <div class="address-item">
                                                                <span class="title">Local:</span>
                                                                <span class="content">{{$orden->local->nombre}}</span>
                                                            </div>
                                                            {{--<div class="address-item">--}}
                                                                {{--<span class="title">Zip / Código Postal:</span>--}}
                                                                {{--<span class="content">2340000</span>--}}
                                                            {{--</div>--}}
                                                        </div>
                                                    </div>

                                                </div>
                                                <div class="order-info">
                                                    <div class="order-info-inner">
                                                        <table id="order_details">
                                                            <thead>
                                                            <tr>
                                                                <th style="width: 20px;">Producto</th>
                                                                <th style="width: 20px;">Precio</th>
                                                                <th  class="center">Cantidad</th>
                                                                <th  class="total"><b>TOTAL</b></th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            @foreach($orden->detalles as $det)
                                                            <tr>
                                                                <td class="td-product">
                                                                    @if(!$det->item->esExtra())
                                                                    <b>
                                                                        {{$det->item->nombre}} {{$det->num_item ?? ''}}
                                                                    </b>
                                                                    @else
                                                                        {{$det->item->nombre}} {{$det->num_item ?? ''}}
                                                                    @endif

                                                                    @if($det->ingredientes)
                                                                        <br>
                                                                        {{$det->ingredientes}}
                                                                    @endif
                                                                </td>
                                                                <td class="money">
                                                                    {{dvs().nfp($det->precio)}}
                                                                </td>
                                                                <td class="quantity ">
                                                                    {{number_format($det->cantidad,0,',','.')}}
                                                                </td>
                                                                <td class="total">
                                                                    {{dvs().nfp($det->precio*$det->cantidad)}}
                                                                </td>
                                                            </tr>
                                                            @endforeach
                                                            </tbody>
                                                            <tfoot>
                                                            @if($orden->delivery)
                                                                <tr class="order_summary note">
                                                                    <td class="td-label" colspan="3">SubTotal</td>
                                                                    <td class="total">
                                                                        <span class="money" >{{dvs().nfp($orden->sub_total)}}</span>
                                                                    </td>
                                                                </tr>

                                                                <tr class="order_summary note">
                                                                    <td class="td-label" colspan="3">Delivery</td>
                                                                    <td class="total">
                                                                        <span class="money" >{{dvs().nfp($orden->monto_delivery)}}</span>
                                                                    </td>
                                                                </tr>
                                                            @endif
                                                            <tr class="order_summary order_total">
                                                                <td class="td-label" colspan="3">Total</td>
                                                                <td class="total">
                                                                    <span class="money" >{{dvs().nfp($orden->total)}}</span>
                                                                </td>
                                                            </tr>
                                                            </tfoot>
                                                        </table>
                                                    </div>
                                                </div>
                                                <div class="order-info-mobile" >
                                                    <div class="order-info-inner">
                                                        <table id="order_details">
                                                            <thead>
                                                            <tr>
                                                                <th class="table-mobile">Producto</th>
                                                                <th class="table-mobile">Precio</th>
                                                                <th class="table-mobile" >Cantidad</th>
                                                                <th class="table-mobile" ><b>TOTAL</b></th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            @foreach($orden->detalles as $det)
                                                                <tr>
                                                                    <td >
                                                                        {{$det->item->nombre}}
                                                                    </td>
                                                                    <td >
                                                                        {{dvs().nfp($det->precio)}}
                                                                    </td>
                                                                    <td >
                                                                        {{number_format($det->cantidad,0,',','.')}}
                                                                    </td>
                                                                    <td >
                                                                        {{dvs().nfp($det->precio*$det->cantidad)}}
                                                                    </td>
                                                                </tr>
                                                            @endforeach
                                                            </tbody>
                                                            <tfoot>
                                                            @if($orden->delivery)
                                                            <tr class="order_summary note">
                                                                <td class="td-label" colspan="3">SubTotal</td>
                                                                <td class="total">
                                                                    <span class="money" >{{dvs().nfp($orden->total)}}</span>
                                                                </td>
                                                            </tr>

                                                            <tr class="order_summary note">
                                                                <td class="td-label" colspan="3">Delivery</td>
                                                                <td class="total">
                                                                    <span class="money" >{{dvs().nfp($orden->monto_delivery)}}</span>
                                                                </td>
                                                            </tr>
                                                            @endif
                                                            <tr class="order_summary order_total">
                                                                <td class="td-label" colspan="3">Total</td>
                                                                <td class="total">
                                                                    <span class="money" >{{dvs().nfp($orden->total+$orden->monto_delivery)}}</span>
                                                                </td>
                                                            </tr>
                                                            </tfoot>
                                                        </table>

                                                    </div>


                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <div class="row">
                            <div class="col-sm-4"></div>
                            <div class="col-sm-4"></div>
                            <div class="col-sm-4">
                                <a href="{{route('seguir.comprando',$orden->id)}}" class="btn botones" id="pideahora"> SEGUIR COMPRANDO </a>
                            </div>
                        </div>

                    </div>
                    {!! Form::close() !!}
                </div>
            </section>
        </main>

    </div>

@endsection

@push('scripts')
{{--<script src="{{asset('js/jquery-3.3.1.min.js')}}"></script>--}}
<script src="https://www.gstatic.com/firebasejs/5.0.4/firebase-app.js"></script>
<script src="https://www.gstatic.com/firebasejs/5.0.4/firebase-firestore.js"></script>

<script>
    $(function () {
        console.log('Actualizar fire base');


        var config = {
            apiKey: "AIzaSyDs1Fx826HnI502dugIE72ONzUz7vuppUs",
            authDomain: "fritomaniafl-2018.firebaseapp.com",
            databaseURL: "https://fritomaniafl-2018.firebaseio.com",
            projectId: "fritomaniafl-2018",
            storageBucket: "",
            messagingSenderId: "545691642884"
        };

        const firebaseApp = firebase.initializeApp(config);
        const firestore = firebase.firestore();
        var settings = { timestampsInSnapshots: true};
        firestore.settings(settings);

        const db = firebaseApp.firestore();

        {{--db.collection("cambios")--}}
            {{--.doc("tienda"+"{{$orden->local->id}}")--}}
            {{--.onSnapshot(doc => {--}}
                {{--if(doc.data()){--}}
                    {{--console.log(doc.data().venta)--}}
                {{--}--}}
            {{--});--}}

        db.collection("cambios").doc("tienda"+"{{$orden->local->id}}").set({
            venta:{
                id: '{{$orden->id}}',
                lugar: 'caja',
                estado: '{{$orden->estado->id}}',
                delivery: '{{$orden->delivery}}',
            }
        })
        .then(function() {
            console.log("Se ha actualizado con exito!");
        })
        .catch(function(error) {
            console.error("Error al actualizar: ", error);
        });
    })
</script>
@endpush