<!doctype html>
<html>
<head>
    <meta name="viewport" content="width=device-width" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Simple Transactional Email</title>
    <style>
        /* -------------------------------------
            GLOBAL RESETS
        ------------------------------------- */
        img {
            border: none;
            -ms-interpolation-mode: bicubic;
            max-width: 100%; }

        body {
            background-color: #f6f6f6;
            font-family: sans-serif;
            -webkit-font-smoothing: antialiased;
            font-size: 14px;
            line-height: 1.4;
            margin: 0;
            padding: 0;
            -ms-text-size-adjust: 100%;
            -webkit-text-size-adjust: 100%; }

        table {
            border-collapse: separate;
            mso-table-lspace: 0pt;
            mso-table-rspace: 0pt;
            width: 100%; }
        table td {
            font-family: sans-serif;
            font-size: 14px;
            vertical-align: top; }

        /* -------------------------------------
            BODY & CONTAINER
        ------------------------------------- */

        .body {
            background-color: #f6f6f6;
            width: 100%; }

        /* Set a max-width, and make it display as block so it will automatically stretch to that width, but will also shrink down on a phone or something */
        .container {
            display: block;
            Margin: 0 auto !important;
            /* makes it centered */
            max-width: 580px;
            padding: 10px;
            width: 580px; }

        /* This should also be a block element, so that it will fill 100% of the .container */
        .content {
            box-sizing: border-box;
            display: block;
            Margin: 0 auto;
            max-width: 580px;
            padding: 10px; }

        /* -------------------------------------
            HEADER, FOOTER, MAIN
        ------------------------------------- */
        .main {
            background: #ffffff;
            border-radius: 3px;
            width: 100%; }

        .wrapper {
            box-sizing: border-box;
            padding: 20px; }

        .content-block {
            padding-bottom: 10px;
            padding-top: 10px;
        }

        .footer {
            clear: both;
            Margin-top: 10px;
            text-align: center;
            width: 100%; }
        .footer td,
        .footer p,
        .footer span,
        .footer a {
            color: #999999;
            font-size: 12px;
            text-align: center; }

        /* -------------------------------------
            TYPOGRAPHY
        ------------------------------------- */
        h1,
        h2,
        h3,
        h4 {
            color: #000000;
            font-family: sans-serif;
            font-weight: 400;
            line-height: 1.4;
            margin: 0;
            Margin-bottom: 30px; }

        h1 {
            font-size: 35px;
            font-weight: 300;
            text-align: center;
            text-transform: capitalize; }

        p,
        ul,
        ol {
            font-family: sans-serif;
            font-size: 14px;
            font-weight: normal;
            margin: 0;
            Margin-bottom: 15px; }
        p li,
        ul li,
        ol li {
            list-style-position: inside;
            margin-left: 5px; }

        a {
            color: #9d1414;
            text-decoration: underline; }

        /* -------------------------------------
            BUTTONS
        ------------------------------------- */
        .btn {
            box-sizing: border-box;
            width: 100%; }
        .btn > tbody > tr > td {
            padding-bottom: 15px; }
        .btn table {
            width: auto; }
        .btn table td {
            background-color: #ffffff;
            border-radius: 5px;
            text-align: center; }
        .btn a {
            background-color: #ffffff;
            border: solid 1px #9d1414;
            border-radius: 5px;
            box-sizing: border-box;
            color: #9d1414;
            cursor: pointer;
            display: inline-block;
            font-size: 14px;
            font-weight: bold;
            margin: 0;
            padding: 12px 25px;
            text-decoration: none;
            text-transform: capitalize; }

        .btn-primary table td {
            background-color: #fff; }

        .btn-primary a {
            background-color: #9d1414;
            border-color: #9d1414;
            color: #ffffff; }

        /* -------------------------------------
            OTHER STYLES THAT MIGHT BE USEFUL
        ------------------------------------- */
        .last {
            margin-bottom: 0; }

        .first {
            margin-top: 0; }

        .align-center {
            text-align: center; }

        .align-right {
            text-align: right; }

        .align-left {
            text-align: left; }

        .clear {
            clear: both; }

        .mt0 {
            margin-top: 0; }

        .mb0 {
            margin-bottom: 0; }

        .preheader {
            color: transparent;
            display: none;
            height: 0;
            max-height: 0;
            max-width: 0;
            opacity: 0;
            overflow: hidden;
            mso-hide: all;
            visibility: hidden;
            width: 0; }

        .powered-by a {
            text-decoration: none; }

        hr {
            border: 0;
            border-bottom: 1px solid #f6f6f6;
            Margin: 20px 0; }

        /* -------------------------------------
            RESPONSIVE AND MOBILE FRIENDLY STYLES
        ------------------------------------- */
        @media only screen and (max-width: 620px) {
            table[class=body] h1 {
                font-size: 28px !important;
                margin-bottom: 10px !important; }
            table[class=body] p,
            table[class=body] ul,
            table[class=body] ol,
            table[class=body] td,
            table[class=body] span,
            table[class=body] a {
                font-size: 16px !important; }
            table[class=body] .wrapper,
            table[class=body] .article {
                padding: 10px !important; }
            table[class=body] .content {
                padding: 0 !important; }
            table[class=body] .container {
                padding: 0 !important;
                width: 100% !important; }
            table[class=body] .main {
                border-left-width: 0 !important;
                border-radius: 0 !important;
                border-right-width: 0 !important; }
            table[class=body] .btn table {
                width: 100% !important; }
            table[class=body] .btn a {
                width: 100% !important; }
            table[class=body] .img-responsive {
                height: auto !important;
                max-width: 100% !important;
                width: auto !important; }}

        /* -------------------------------------
            PRESERVE THESE STYLES IN THE HEAD
        ------------------------------------- */
        @media all {
            .ExternalClass {
                width: 100%; }
            .ExternalClass,
            .ExternalClass p,
            .ExternalClass span,
            .ExternalClass font,
            .ExternalClass td,
            .ExternalClass div {
                line-height: 100%; }
            .apple-link a {
                color: inherit !important;
                font-family: inherit !important;
                font-size: inherit !important;
                font-weight: inherit !important;
                line-height: inherit !important;
                text-decoration: none !important; }
            .btn-primary table td:hover {
                background-color: #34495e !important; }
            .btn-primary a:hover {
                background-color: #34495e !important;
                border-color: #34495e !important; } }

    </style>
</head>
<body class="">
<table border="0" cellpadding="0" cellspacing="0" class="body" style="text-align: center;">
    <tr>
        <td>&nbsp;</td>
        <td class="container">
            <div class="content">

                <!-- START CENTERED WHITE CONTAINER -->
                <span class="preheader">Gracias por su compra.</span>
                <table class="main">

                    <!-- START MAIN CONTENT AREA -->
                    <tr>
                        <td class="wrapper">
                            <table border="0" cellpadding="0" cellspacing="0">
                                <tr >
                                    <td >
                                        <img src="https://fritomania.cl/cliente/images/logo_nuevo.png" alt="">
                                        <p style="text-align: center; background-color: #9d1414; color: #ffffff; padding: 5%; font-weight: bold; font-size: 14px;">Gracias por su compra</p>
                                        <p>Su pedido n° ((variable)) ha sido recibido con éxito</p>
                                        <table border="0" cellpadding="0" cellspacing="0" class="btn btn-primary">
                                            <tbody>
                                            <tr>
                                                <td align="center">
                                                    <section class="order-layout">
                                                        <div class="order-wrapper">
                                                            <div class="container">
                                                                <div class="row">
                                                                    <div class="order-inner">
                                                                        <div class="order-content">
                                                                            <div class="order-id">
                                                                                <h2>Pedido #1002</h2>
                                                                                <span class="date">8 Ago 15:15</span>
                                                                                <div id="order-cancelled" class="flash notice">
                                                                                    <h5 id="order-cancelled-title">Pedido Cancelado el <span class="note">8 Ago 15:30</span></h5>
                                                                                    <span class="note">Motivo: cliente</span>
                                                                                </div>
                                                                            </div>



                                                                        </div>
                                                                        <div class="order-info">
                                                                            <div class="order-info-inner">
                                                                                <table id="order_details" border="1">
                                                                                    <thead>
                                                                                    <tr>
                                                                                        <th>Producto</th>
                                                                                        <th>SKU</th>
                                                                                        <th>Precio</th>
                                                                                        <th class="center">Cantidad</th>
                                                                                        <th class="total">Total</th>
                                                                                    </tr>
                                                                                    </thead>
                                                                                    <tbody>
                                                                                    <tr id="10324769618" class="odd">
                                                                                        <td class="td-product">
                                                                                            Lorem Ipsum tizzy 6
                                                                                        </td>
                                                                                        <td class="sku note">CA78977</td>
                                                                                        <td class="money"><span class="money" data-currency-usd="$259.00" data-currency="USD">$259.00</span></td>
                                                                                        <td class="quantity ">1</td>
                                                                                        <td class="total"><span class="money" data-currency-usd="$259.00" data-currency="USD">$259.00</span></td>
                                                                                    </tr>
                                                                                    <tr id="10324769682" class="even">
                                                                                        <td class="td-product">
                                                                                            Lorem Ipsum tizzy 14
                                                                                        </td>
                                                                                        <td class="sku note">CA78977</td>
                                                                                        <td class="money"><span class="money" data-currency-usd="$259.00" data-currency="USD">$259.00</span></td>
                                                                                        <td class="quantity ">2</td>
                                                                                        <td class="total"><span class="money" data-currency-usd="$518.00" data-currency="USD">$518.00</span></td>
                                                                                    </tr>
                                                                                    <tr id="10324769746" class="odd">
                                                                                        <td class="td-product">
                                                                                            Lorem Ipsum tizzy 1
                                                                                        </td>
                                                                                        <td class="sku note">CA78977</td>
                                                                                        <td class="money"><span class="money" data-currency-usd="$259.00" data-currency="USD">$259.00</span></td>
                                                                                        <td class="quantity ">2</td>
                                                                                        <td class="total"><span class="money" data-currency-usd="$518.00" data-currency="USD">$518.00</span></td>
                                                                                    </tr>
                                                                                    <tr id="10324769810" class="even">
                                                                                        <td class="td-product">
                                                                                            Lorem Ipsum tizzy 12
                                                                                        </td>
                                                                                        <td class="sku note">CA78977</td>
                                                                                        <td class="money"><span class="money" data-currency-usd="$259.00" data-currency="USD">$259.00</span></td>
                                                                                        <td class="quantity ">2</td>
                                                                                        <td class="total"><span class="money" data-currency-usd="$518.00" data-currency="USD">$518.00</span></td>
                                                                                    </tr>
                                                                                    <tr id="10324769874" class="odd">
                                                                                        <td class="td-product">
                                                                                            Lorem Ipsum tizzy 7 - S / Black
                                                                                        </td>
                                                                                        <td class="sku note">CA78963</td>
                                                                                        <td class="money"><span class="money" data-currency-usd="$259.00" data-currency="USD">$259.00</span></td>
                                                                                        <td class="quantity ">2</td>
                                                                                        <td class="total"><span class="money" data-currency-usd="$518.00" data-currency="USD">$518.00</span></td>
                                                                                    </tr>
                                                                                    <tr id="10324769938" class="even">
                                                                                        <td class="td-product">
                                                                                           Lorem Ipsum tizzy 11
                                                                                        </td>
                                                                                        <td class="sku note">CA78977</td>
                                                                                        <td class="money"><span class="money" data-currency-usd="$259.00" data-currency="USD">$259.00</span></td>
                                                                                        <td class="quantity ">3</td>
                                                                                        <td class="total"><span class="money" data-currency-usd="$777.00" data-currency="USD">$777.00</span></td>
                                                                                    </tr>
                                                                                    <tr id="10324770002" class="odd">
                                                                                        <td class="td-product">
                                                                                            Lorem Ipsum tizzy 8
                                                                                        </td>
                                                                                        <td class="sku note">CA78977</td>
                                                                                        <td class="money"><span class="money" data-currency-usd="$259.00" data-currency="USD">$259.00</span></td>
                                                                                        <td class="quantity ">3</td>
                                                                                        <td class="total"><span class="money" data-currency-usd="$777.00" data-currency="USD">$777.00</span></td>
                                                                                    </tr>
                                                                                    <tr id="10324770066" class="even">
                                                                                        <td class="td-product">
                                                                                            Lorem Ipsum tizzy 9 - S / Black
                                                                                        </td>
                                                                                        <td class="sku note">CA78963</td>
                                                                                        <td class="money"><span class="money" data-currency-usd="$259.00" data-currency="USD">$259.00</span></td>
                                                                                        <td class="quantity ">7</td>
                                                                                        <td class="total"><span class="money" data-currency-usd="$1,813.00" data-currency="USD">$1,813.00</span></td>
                                                                                    </tr>
                                                                                    </tbody>
                                                                                    <tfoot>
                                                                                    <tr class="order_summary note">
                                                                                        <td class="td-label" colspan="4">Subtotal</td>
                                                                                        <td class="subtotal"><span class="money" data-currency-usd="$5,698.00" data-currency="USD">$5,698.00</span></td>
                                                                                    </tr>

                                                                                    <tr class="order_summary note">
                                                                                        <td class="td-label" colspan="4">IVA:</td>
                                                                                        <td class="vat"><span class="money" data-currency-usd="$569.80" data-currency="USD">$569.80</span></td>
                                                                                    </tr>
                                                                                    <tr class="order_summary order_total">
                                                                                        <td class="td-label" colspan="4">TOTAL</td>
                                                                                        <td class="total"><span class="money" data-currency-usd="$6,277.80" data-currency="USD">$6,277.80</span> </td>
                                                                                    </tr>
                                                                                    </tfoot>
                                                                                </table>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
            </div>
            </section>
            <table border="0" cellpadding="0" cellspacing="0">
                <tbody>
                <tr>
                    <td> <a href="https://fritomania.cl/locales" target="_blank">Seguir comprando</a> </td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    </tbody>
</table>

</td>
</tr>
</table>
</td>
</tr>

<!-- END MAIN CONTENT AREA -->
</table>

<!-- START FOOTER -->
<div class="footer">
    <table border="0" cellpadding="0" cellspacing="0" style="text-align: center !important;">
        <tr>
            <td class="content-block">
                <span class="apple-link">Av. Nueva Providencia 1881, oficina 1407, Providencia, Región Metropolitana, Chile</span>
                <br> ¿No quiere seguir recibiendo estos mails? <a href="http://i.imgur.com/CScmqnj.gif">Darme de alta</a>.
            </td>
        </tr>
        <tr>
            <td class="content-block powered-by">
                Copyright 2018 | Desarrollado por <a href="http://negociovirtual.cl/">Negocio Virtual</a>.
            </td>
        </tr>
    </table>
</div>
<!-- END FOOTER -->

<!-- END CENTERED WHITE CONTAINER -->
</div>
</td>
<td>&nbsp;</td>
</tr>
</table>
</body>
</html>
