    @extends('layouts.marco_cliente')

@push('css')
<link href="{{asset('cliente/css/locales.css')}}" rel="stylesheet" type="text/css" media="all">
@endpush

@section('content')
    <main class="main-content" id="MainContent" role="main">
        <section class="heading-content">
            <div class="heading-wrapper">
                <div class="container">

                </div>
            </div>
        </section>
        <section class="page-content">
            <div class="page-wrapper">
                <div class="container">
                    <div class="row">
                        <div class="page-inner">
                            <div id="shopify-section-contact-template" class="shopify-section">
                                <div class="contact-content">

                                    <div class="google-maps-content">
                                        <div class="google-maps-wrapper">
                                            <div class="page_title">
                                                <h2 class="title">
                                                    <span class="first">Local</span>
                                                    <span class="last">{{$local->nombre}}</span>
                                                </h2>
                                                <p class="caption">
                                                    {{$local->descripcion ?? 'Nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Mauris in erat justo. Nullam ac urna eu felis dapibus.'}}
                                                </p>
                                            </div>

                                            <div class="google-maps-inner">
                                                {!!  $local->mapa !!}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="information_layout">
                                        <div class="information_inner">
                                            <div class="information_content">
                                                <div class="information_item col-sm-4">
                                                    <div class="page_title text_title">
                                                        <h2 class="title">Horario</h2>
                                                    </div>
                                                    <div class="text_content">
                                                        <ul>
                                                            <li>
                                                                <div class="group_text">
                                                                    <span class="day">Lun – Jue</span>
                                                                    <span class="time">6AM - 7PM</span>
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <div class="group_text">
                                                                    <span class="day">Viernes</span>
                                                                    <span class="time">6AM - 9PM</span>
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <div class="group_text">
                                                                    <span class="day">Sábado</span>
                                                                    <span class="time">6AM - 9PM</span>
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <div class="group_text">
                                                                    <span class="day">Domingo</span>
                                                                    <span class="time">7AM - 7PM</span>
                                                                </div>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="information_item col-sm-4">
                                                    <div class="page_title text_title">
                                                        <h2 class="title">Información de Contacto</h2>
                                                    </div>
                                                    <div class="text_content">
                                                        <div class="group_contact_info">
                                                            <div class="item">
																	<span>
																		<i class="fa fa-home"></i><address>{{$local->direccion}}</address>
																	</span>
                                                            </div>
                                                            <div class="item phone-fax">
																	<span>
																		<i class="fa fa-phone"></i><a href="tel:9 9564 7034">{{$local->telefono}}</a>
																	</span>
                                                                {{--<span>--}}
																		{{--<i class="fa fa-fax"></i><a href="fax:(084)0123456789">(084) 0123 456 789</a>--}}
																	{{--</span>--}}
                                                            </div>
                                                            <div class="item">
																	<span>
																		<i class="fa fa-envelope-o"></i><a href="mailto:contacto@fritomania.cl">contacto@fritomania.cl</a>
																	</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="information_item col-sm-4">
                                                    <div class="page_title form_title">
                                                        <img src="cliente/images/017.jpg" alt="">
                                                    </div>
                                                    <div class="form_content">

                                                            <div id="contactFormWrapper" class="group_form">

                                                                    <div class="form-item">
                                                                        <a href="{{route('locales')}}" class="btn btn-lg btn-block btn-danger">
                                                                            Pedir Ahora
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
@endsection