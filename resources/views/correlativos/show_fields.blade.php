<!-- Id Field -->
{!! Form::label('id', 'Id:') !!}
{!! $correlativo->id !!}<br>


<!-- Tabla Field -->
{!! Form::label('tabla', 'Tabla:') !!}
{!! $correlativo->tabla !!}<br>


<!-- Anio Field -->
{!! Form::label('anio', 'Anio:') !!}
{!! $correlativo->anio !!}<br>


<!-- Max Field -->
{!! Form::label('max', 'Max:') !!}
{!! $correlativo->max !!}<br>


<!-- Created At Field -->
{!! Form::label('created_at', 'Creado el:') !!}
{!! $correlativo->created_at->format('Y/m/d H:i:s')!!}<br>


<!-- Updated At Field -->
{!! Form::label('updated_at', 'Actualizado el:') !!}
{!! $correlativo->updated_at->format('Y/m/d H:i:s')!!}<br>


<!-- Deleted At Field -->
{!! Form::label('deleted_at', 'Borrado el:') !!}
{!! $correlativo->deleted_at !!}<br>


