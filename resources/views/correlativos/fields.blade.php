<!-- Tabla Field -->
<div class="form-group col-sm-6">
    {!! Form::label('tabla', 'Tabla:') !!}
    {!! Form::text('tabla', null, ['class' => 'form-control']) !!}
</div>

<!-- Anio Field -->
<div class="form-group col-sm-6">
    {!! Form::label('anio', 'Anio:') !!}
    {!! Form::number('anio', null, ['class' => 'form-control']) !!}
</div>
