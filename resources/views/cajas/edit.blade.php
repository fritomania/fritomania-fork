@extends('layouts.app')

@section('htmlheader_title')
	Editar Caja
@endsection

@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row">
                <div class="col">
                    <h1 class="m-0 text-dark">
                        Editar Caja
                    </h1>
                </div><!-- /.col -->
                <div class="col">
                    <a class="btn btn-outline-info float-right"
                       href="{{route('cajas.index')}}">
                        <i class="fa fa-list" aria-hidden="true"></i>&nbsp;<span class="d-none d-sm-inline">Listado</span>
                    </a>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            @include('flash::message')
            @include('adminlte-templates::common.errors')

            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">

                        {!! Form::model($caja, ['route' => ['cajas.update', $caja->id], 'method' => 'patch']) !!}

                        <!-- Monto Apertura Field -->
                            <div class="form-group col-sm-6">
                                {!! Form::label('monto_cierre', 'Monto Cierre:') !!}
                                {!! Form::text('monto_cierre', $caja->monto_cierre, ['class' => 'form-control','disabled']) !!}
                                {!! Form::hidden('monto_cierre', $caja->monto_cierre) !!}
                                {!! Form::hidden('url_prev', URL::previous()) !!}
                            </div>

                            <!-- Submit Field -->
                            <div class="form-group col-sm-12">
                                <button type="submit" onClick="this.form.submit(); this.disabled=true;" class="btn btn-success">Guardar</button>
                                <a href="{!! URL::previous() !!}" class="btn btn-default">Cancel</a>
                            </div>

                            {!! Form::close() !!}

                        </div>
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col-md-6 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /.content -->


@endsection