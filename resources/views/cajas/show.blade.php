@extends('layouts.app')

@section('htmlheader_title')
	Caja
@endsection

@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row">
                <div class="col">
                    <h1 class="m-0 text-dark">Caja</h1>
                </div><!-- /.col -->
                <div class="col ">
                    <a class="btn btn-outline-warning float-right" href="{{route('cajas.edit',$caja->id)}}">
                        <i class="fa fa-lock" aria-hidden="true"></i>&nbsp;<span class="d-none d-sm-inline">Cerrar</span>
                    </a>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->


    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col">
                    <div class="card">
                        <div class="card-body">
                           @include('cajas.show_fields')
                        </div>
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col-md-6 -->
                <div class="col">
                    <div class="card card-default">
                        <div class="card-header with-border">
                            <h3 class="card-title">Retiros</h3>

                            <div class="card-tools pull-right">
                                <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                </button>
                            </div>
                            <!-- /.card-tools -->
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            @include('cajas.table_retiros')
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->

                </div>
            </div>
            <!-- /.row -->

            <div class="row">
                <div class="form-group col-sm-12">
                <a href="{!! route('cajas.index') !!}" class="btn btn-default">Regresar</a>
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /.content -->
@endsection
