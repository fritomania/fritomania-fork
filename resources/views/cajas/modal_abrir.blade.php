<div class="modal fade" id="modalAbrirCaja" style="z-index: 10000">
    <div class="modal-dialog">
        <div class="modal-content">
            <form method="POST" v-on:submit.prevent="abrirCaja">
                <div class="modal-header">
                    <h4 class="modal-title">Debe abrir caja antes de realizar una venta</h4>
                </div>
                <div class="modal-body">
                    <div class="row">

                        <div class="form-group col-sm-12">
                            {!! Form::label('monto_apertura', 'Monto:') !!}
                            <input type="text" name="montoCaja" id="montoCaja" class="form-control"
                                   required="required" v-model="caja.monto_apertura">

                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary" @submit.prevent="abrirCaja" :disabled="loadingBtnCaja">Guardar</button>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->