<!-- Id Field -->
{!! Form::label('id', 'Id:') !!}
{!! $caja->id !!}<br>


<!-- Monto Apertura Field -->
{!! Form::label('monto_apertura', 'Monto Apertura:') !!}
{!! $caja->monto_apertura !!}<br>


<!-- Monto Cierre Field -->
{!! Form::label('monto_cierre', 'Monto Cierre:') !!}
{!! $caja->monto_cierre !!}<br>


<!-- Fecha Cierre Field -->
{!! Form::label('fecha_cierre', 'Fecha Cierre:') !!}
{!! $caja->fecha_cierre !!}<br>


<!-- User Id Field -->
{!! Form::label('user_id', 'User Id:') !!}
{!! $caja->user_id !!}<br>


<!-- Created At Field -->
{!! Form::label('created_at', 'Creado el:') !!}
{!! $caja->created_at->format('Y/m/d H:i:s')!!}<br>


<!-- Updated At Field -->
{!! Form::label('updated_at', 'Actualizado el:') !!}
{!! $caja->updated_at->format('Y/m/d H:i:s')!!}<br>


<!-- Deleted At Field -->
{!! Form::label('deleted_at', 'Borrado el:') !!}
{!! $caja->deleted_at !!}<br>


