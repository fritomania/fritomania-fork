@if($caja->retiros->count()>0)
    <div class="table-responsive">
    <table class="table table-bordered table-hover table-condensed table-striped">
        <thead>
        <tr>
            <th>Fecha</th>
            <th>Motivo</th>
            <th>Monto</th>
        </tr>
        </thead>
        <tbody>
        @foreach($caja->retiros as $r)
        <tr>
            <td>{{$r->created_at}}</td>
            <td>{{$r->motivo}}</td>
            <td>{{ dvs() }} {{number_format($r->monto,2)}}</td>
        </tr>
            @endforeach
        </tbody>
        <tfoot>
            <th colspan="2">Total</th>
            <th>{{ dvs() }} {{number_format(array_sum(array_pluck($caja->retiros,'monto')),2)}}</th>
        </tfoot>
    </table>
</div>
@else
    <h3 class="text-warning text-center">No hay registros</h3>
@endif