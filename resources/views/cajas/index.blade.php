@extends('layouts.app')

@section('htmlheader_title')
	Cajas
@endsection

@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row">
                <div class="col">
                    <h1 class="m-0 text-dark">Cajas</h1>
                </div><!-- /.col -->
                <div class="col">
                    @if($caja=Auth::user()->cajaAbierta())
                        <a class="btn btn-danger float-right" style="margin-top: -10px;margin-bottom: 5px" href="{!! route('cajas.edit',$caja->id) !!}">Cerrar Caja</a>
                    @else
                        <a class="btn btn-primary float-right" style="margin-top: -10px;margin-bottom: 5px" href="{!! route('cajas.create') !!}">Abrir Caja</a>
                    @endif
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            @include('flash::message')

            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                           @include('cajas.table')
                        </div>
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col-md-6 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /.content -->
@endsection

