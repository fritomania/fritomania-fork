<!-- Id Field -->
{!! Form::label('id', 'Id:') !!}
{!! $marca->id !!}<br>


<!-- Nombre Field -->
{!! Form::label('nombre', 'Nombre:') !!}
{!! $marca->nombre !!}<br>


<!-- Created At Field -->
{!! Form::label('created_at', 'Creado el:') !!}
{!! $marca->created_at->format('Y/m/d H:i:s')!!}<br>


<!-- Updated At Field -->
{!! Form::label('updated_at', 'Actualizado el:') !!}
{!! $marca->updated_at->format('Y/m/d H:i:s')!!}<br>


<!-- Deleted At Field -->
{!! Form::label('deleted_at', 'Borrado el:') !!}
{!! $marca->deleted_at !!}<br>


