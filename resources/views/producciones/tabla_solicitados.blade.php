<table class="table table-sm table-striped table-bordered table-hover">
    <thead class="bg-secondary">
    <tr>
        <th scope="col">Cantidad</th>
        <th scope="col">U/M</th>
        <th scope="col">Nombre</th>
        <th scope="col">Código</th>
        <th scope="col">Pendientes</th>
        <th scope="col">Producido</th>
        <th scope="col">Estado</th>
    </tr>
    </thead>
    <tbody>
    @foreach($produccione->productoSolicitados as $det)
        <tr class="">
            <td>{{nf($det->cantidad)}}</td>
            <td>{{$det->item->unimed->nombre}}</td>
            <td>{{$det->item->nombre}}</td>
            <td>{{$det->item->codigo}}</td>
            <td>{{nf($det->pendientes)}}</td>
            <td>{{nf($det->producido)}}</td>
            <td>{{$det->estado}}</td>
        </tr>
    @endforeach
    </tbody>
</table>