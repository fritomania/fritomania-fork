@extends('layouts.app')

@section('htmlheader_title')
	Nueva Solicitud de Producción
@endsection

@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row">
                <div class="col">
                    <h1 class="m-0 text-dark">
                        Nueva Solicitud de Producción
                    </h1>
                </div><!-- /.col -->
                <div class="col ">
                    <a class="btn btn-outline-info float-right"
                       href="{{route('producciones.index')}}">
                        <i class="fa fa-list" aria-hidden="true"></i>&nbsp;<span class="d-none d-sm-inline">Listado</span>
                    </a>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content" id="root">
        <div class="container-fluid">
            @include('flash::message')
            @include('adminlte-templates::common.errors')

            {!! Form::open(['route' => 'producciones.store']) !!}

            <!--            Producto solicitado
            ------------------------------------------------------------------------>
            <div class="row">
                <div class="col">
                    <div class="card card-outline card-danger">
                        <div class="card-header">
                            <h3 class="card-title">Producto Terminado</h3>

                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                </button>
                            </div>
                            <!-- /.card-tools -->
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="row">
                                <div class="form-group col-sm-8">
                                    {!! Form::label('item_select','Artículo:') !!}

                                    <v-select v-model="item_select"
                                              :options="{{ formatVueSelect(\App\Models\Item::productoFinal()->conReceta()->get(),'text') }}"
                                              placeholder="Seleccione un artículo.." ></v-select>
                                </div>

                                <div class="form-group col-sm-4">
                                    {!! Form::label('cantidad', 'Cantidad:') !!}
                                    <div class="input-group">
                                        <input type="number" class="form-control" v-model="cantidad" name="cantidad">
                                        <div class="input-group-append">
                                            <button type="button" class="btn btn-danger" @click="add()">
                                                <i class="fa fa-plus" aria-hidden="true" v-show="!loading"></i>
                                                <i class="fas fa-sync-alt fa-spin" aria-hidden="true" v-show="loading"></i>
                                                Agregar
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="table-responsive">
                                <table class="table table-sm table-hover table-striped">
                                    <thead>
                                    <tr class="bg-secondary">
                                        <th>Cantidad</th>
                                        <th>U/M</th>
                                        <th>Articulo</th>
                                        <th>Accion</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr v-if="solicitados.length == 0">
                                        <td colspan="5" class="text-center">Ningún Registro agregado</td>
                                    </tr>
                                    <tr v-for="item in solicitados">
                                        <td v-text="nf(item.qty)"></td>
                                        <td v-text="item.unimed.nombre"></td>
                                        <td v-text="item.nombre"></td>
                                        <td>
                                            <button type="button"
                                                    class="btn btn-outline-danger btn-xs"
                                                    @click="eliminar(item)"
                                                    :disabled="(idEliminando==item.id)"
                                                    v-tooltip="'Eliminar detalle'"
                                            >
                                                <i v-show="(idEliminando==item.id)" class="fas fa-sync-alt fa-spin" aria-hidden="true"></i>
                                                <i v-show="!(idEliminando==item.id)" class="fa fa-trash-alt" aria-hidden="true"></i>
                                            </button>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <div class="overlay" v-show="loading">
                            <i class="fa fa-sync fa-spin"></i>
                        </div>
                    </div>
                </div>
            </div>

            <!--            Producto que se consume
            ------------------------------------------------------------------------>
            <div class="row">
                <div class="col">
                    <div class="card card-outline card-danger">
                        <div class="card-header">
                            <h3 class="card-title">Materia Prima que se debierá utilizar segun receta</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <table class="table table-sm  table-hover">
                                <thead>
                                <tr class="bg-secondary">
                                    <th>Cantidad</th>
                                    <th>U/M</th>
                                    <th>Articulo</th>
                                    <th>Stock Actual</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr v-if="consumidos.length == 0">
                                    <td colspan="5" class="text-center">Ningún Registro agregado</td>
                                </tr>
                                <tr v-for="det in consumidos" :class="[(det.stock < det.cantidad) ? 'table-danger' : '']">
                                    <td v-text="nf(det.cantidad)"></td>
                                    <td v-text="det.item.unimed.nombre"></td>
                                    <td v-text="det.item.nombre"></td>
                                    <td v-text="nf(det.stock)"></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.card-body -->
                        <div class="overlay" v-show="loadingMateriaPrima">
                            <i class="fa fa-sync fa-spin"></i>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Submit Field -->
            <div class="row mt-3">
                <div class="form-group col-sm-12">
                    <button type="submit" onClick="this.form.submit(); this.disabled=true;"
                            :disabled="stockInsuficiente || (solicitados.length==0 && consumidos.length==0) || loadingMateriaPrima" class="btn btn-outline-danger">
                        Solicita a cocina la producción
                    </button>
                    <span class="text-danger" v-show="stockInsuficiente">
                        Stock insuficiente para realizar la producción
                    </span>
                    <a href="{!! route('recetas.index') !!}" class="btn btn-outline-default">Cancelar</a>
                </div>
            </div>
            
            {!! Form::close() !!}
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /.content -->


@endsection
@push('scripts')
<script>
    new Vue({
        el: '#root',
        mounted() {
            // console.log('Instancia vue montada');
        },
        created() {
            // console.log('Instancia vue creada');
            this.getProducidos();
        },
        data: {
            'item_select': null,
            'cantidad': 1,
            'item': {
                unimed: {

                }
            },
            'solicitados': [],
            'consumidos': [],
            'loading': false,
            'idEliminando': null,
            'loadingMateriaPrima': false
        },
        methods: {
            nf(cant){
                cant = parseFloat(cant).toFixed(4)
                return numf(cant);
            },
            getProducidos(){
                var url = "{{route("get.solicitados.session")}}";

                axios.get(url).then(response => {
                    // console.log(response.data.data);
                    this.solicitados = response.data.data;
                })
                .catch(error => {
                    // console.log(error.response.data);
                });
            },
            getConsumidos(){
                this.loadingMateriaPrima = true;

                var url = "{{route("produccion.get.materia.session")}}";

                axios.get(url).then(response => {
                    // console.log(response.data.data);
                    this.consumidos = response.data.data;
                    this.loadingMateriaPrima = false;
                })
                .catch(error => {
                    // console.log(error.response.data);
                    this.loadingMateriaPrima = false;
                });
            },
            add(){
                if(!this.item_select){
                    toastr.error('Seleccione un artículo!');
                    return;
                }
                console.log('Agregar producto final');
                this.loading = true;

                var url = "{{route("produccion.add")}}";

                var params = {
                    params:{
                        'item_select' : this.item_select.id,
                        'cantidad' : this.cantidad,
                    }
                };

                axios.get(url,params).then(response => {
                    // console.log(response.data);

                    this.solicitados = response.data.data;
                    // toastr.success(response.data.message);
                    this.loading = false;
                })
                .catch(error => {
                    toastr.success(error.response.data.message);
                    // console.log(error.response);
                    this.loading = false;
                });

            },
            eliminar(item){
                this.idEliminando = item.id;
                // console.log('Eliminar detalle');

                var url = "{{route("produccion.remove")}}";

                var params = { params:{'item_id' : item.id, }};

                axios.get(url,params).then(response => {
                    // console.log(response.data);

                    this.solicitados = response.data.data;
                })
                .catch(error => {
                    // console.log(error.response);
                });
            }
        },
        computed:{
            stockInsuficiente(){
                var result = false;
                $.each(this.consumidos,function (index,det) {
                    if(det.cantidad > det.stock){
                        console.log('Stock insuficiente para realizar produccion');
                        result = true;
                        return false;
                    }
                });

                return result;
            }
        },
        watch:{
            //si hay un cambio en lo producido (agregar o quitar un articulo) se debe recalcular lo consumido
            solicitados: function (val) {
                // console.log('cambio en solicitados');
                this.getConsumidos();
                this.idEliminando = null;
            }
        }
    });
</script>
@endpush