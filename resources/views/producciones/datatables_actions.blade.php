@php
    $produccion = \App\Models\Produccione::find($id);
@endphp

<a href="{{ route('producciones.show', $id) }}" class='btn btn-default btn-xs' data-toggle="tooltip" title="Ver">
    <i class="fa fa-eye"></i>
</a>

@if($produccion->estado->id == \App\Models\ProduccionEstado::SOLICITADA)
<span data-toggle="tooltip" title="Cancelar Producción">
    <a href="#modal-cancelar-{{$id}}" data-toggle="modal" class='btn btn-warning btn-xs'>
        <i class="fas fa-ban" ></i>
    </a>
</span>
@endif

@if($produccion->enProceso())
    {{--<span data-toggle="tooltip" title="Anular Producción">--}}
        {{--<a href="#modal-anular-{{$id}}" data-toggle="modal" class='btn btn-danger btn-xs'>--}}
            {{--<i class="fas fa-times"></i>--}}
        {{--</a>--}}
    {{--</span>--}}
@endif

<!-- Modal-->
<div class="modal fade" id="modal-cancelar-{{$id}}" tabindex="-1" role="dialog" aria-labelledby="modalLogoutLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content ">
            <div class="modal-header">
                <h5 class="modal-title" >
                    <i class="fa fa-exclamation-triangle text-warning fa-2x" aria-hidden="true"></i> &nbsp;¿Cancelar Solicitud?
                </h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Este proceso hará que el stock que se traslado a cocina para esta producción regrese a bodega de producción.</p>
                <p>¿Esta seguro de Anular?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                {!! Form::open(['route' => ['produccion.cancelar', $id], 'method' => 'post']) !!}
                    <button type="submit" class="btn btn-danger">SI</button>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
<!-- /.modal -->

<!-- Modal-->
<div class="modal fade" id="modal-anular-{{$id}}" tabindex="-1" role="dialog" aria-labelledby="modalLogoutLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content ">
            <div class="modal-header">
                <h5 class="modal-title" >
                    <i class="fa fa-warning text-warning fa-2x" aria-hidden="true"></i> &nbsp;¿Anular?
                </h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">Seleccione "SI" a continuación si está seguro de Anular la producción.</div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                {!! Form::open(['route' => ['produccion.anular', $id], 'method' => 'get']) !!}
                <button type="submit" class="btn btn-danger">SI</button>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
<!-- /.modal -->
