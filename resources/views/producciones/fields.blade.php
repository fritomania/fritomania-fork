<!-- Numero Field -->
<div class="form-group col-sm-6">
    {!! Form::label('numero', 'Numero:') !!}
    {!! Form::text('numero', null, ['class' => 'form-control']) !!}
</div>

<!-- Produccion Tipo Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('produccion_tipo_id', 'Produccion Tipo Id:') !!}
    {!! Form::number('produccion_tipo_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Produccion Estado Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('produccion_estado_id', 'Produccion Estado Id:') !!}
    {!! Form::number('produccion_estado_id', null, ['class' => 'form-control']) !!}
</div>

<!-- User Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('user_id', 'User Id:') !!}
    {!! Form::number('user_id', null, ['class' => 'form-control']) !!}
</div>