<div class="row">
    <div class="col">
        <div class="card">
            <h5 class="card-header">Datos de la Producción</h5>
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-6">
                        <!-- Id Field -->
                        {!! Form::label('id', 'Correlativo:') !!}
                        {!! $produccione->id !!}<br>


                        <!-- Numero Field -->
                        {!! Form::label('numero', 'Numero Producción:') !!}
                        {!! $produccione->numero !!}<br>


                        <!-- Produccion Tipo Id Field -->
                        {!! Form::label('produccion_tipo_id', 'Tipo Producción:') !!}
                        {!! $produccione->produccionTipo->nombre !!}<br>


                        <!-- Produccion Estado Id Field -->
                        {!! Form::label('produccion_estado_id', 'Estado:') !!}
                        <span class="badge badge-info text-lg">
                            {!! $produccione->produccionEstado->nombre !!}<br>
                        </span>
                    </div>
                    <div class="col-sm-6">
                        <!-- User Id Field -->
                        {!! Form::label('user_id', 'Usuario:') !!}
                        {!! $produccione->user->name !!}<br>


                        <!-- Created At Field -->
                        {!! Form::label('created_at', 'Creado el:') !!}
                        {!! $produccione->created_at->format('Y/m/d H:i:s')!!}<br>


                        <!-- Updated At Field -->
                        {!! Form::label('updated_at', 'Actualizado el:') !!}
                        {!! $produccione->updated_at->format('Y/m/d H:i:s')!!}<br>


                        <!-- Deleted At Field -->
                        {{--{!! Form::label('deleted_at', 'Borrado el:') !!}--}}
                        {{--{!! $produccione->deleted_at !!}<br>--}}
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col">
        <div class="card">
            <h5 class="card-header">Producto Solicitado</h5>
            <div class="card-body p-2">
                @include('producciones.tabla_solicitados')
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-6">
        <div class="card">
            <h5 class="card-header">Producto Terminado</h5>
            <div class="card-body p-2">
                {{--Tabla de ingredientes--}}
                <table class="table table-sm table-striped table-bordered table-hover">
                    <thead class="bg-secondary">
                    <tr>
                        <th scope="col">Código</th>
                        <th scope="col">Nombre</th>
                        <th scope="col">Cantidad</th>
                        <th scope="col">Unidad de Medida</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($produccione->productoRealizados as $det)
                        <tr class="">
                            <td>{{$det->item->codigo}}</td>
                            <td>{{$det->item->nombre}}</td>
                            <td>{{nf($det->cantidad)}}</td>
                            <td>{{$det->item->unimed->nombre}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="card">
            <h5 class="card-header">Materia Prima</h5>
            <div class="card-body p-2">
                {{--Tabla de ingredientes--}}
                <table class="table table-sm table-striped table-bordered table-hover">
                    <thead class="bg-secondary">
                    <tr>
                        <th scope="col">Código</th>
                        <th scope="col">Nombre</th>
                        <th scope="col">Unidad de Medida</th>
                        <th scope="col">Cantidad Receta</th>
                        <th scope="col">Cantidad Usada</th>
                    </tr>
                    </thead>
                    <tbody >
                    @foreach($produccione->productoConsumidos as $det)
                    <tr class="">
                        <td>{{$det->item->codigo}}</td>
                        <td>{{$det->item->nombre}}</td>
                        <td>{{$det->item->unimed->nombre}}</td>
                        <td>{{nf($det->cantidad_receta)}}</td>
                        <td>{{nf($det->cantidad_ingresada)}}</td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>