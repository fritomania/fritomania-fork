@extends('layouts.app')

@section('htmlheader_title')
	Atender Producción
@endsection

@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-12">
                    <h1 class="m-0 text-dark">Atender Producción</h1>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->


    <!-- Main content -->
    <div class="content" id="root">
        <div class="container-fluid">
            @include('flash::message')

            <!--            Datos de la produccion
            ------------------------------------------------------------------------>
            <div class="row">
                <div class="col-sm-4">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Datos de la Producción</h3>

                            <div class="card-tools">

                            </div>
                            <!-- /.card-tools -->
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col">
                                    <!-- Numero Field -->
                                    {!! Form::label('numero', 'Numero:') !!}
                                    {!! $produccione->numero !!}<br>
                                    <!-- User Id Field -->
                                    {!! Form::label('user_id', 'Usuario:') !!}
                                    {!! $produccione->user->name !!}<br>

                                    <!-- Created At Field -->
                                    {!! Form::label('created_at', 'Solicitada el:') !!}
                                    {!! $produccione->created_at->format('Y/m/d H:i:s')!!}<br>

                                    {!! Form::label('estado', 'Estado:') !!}
                                    <span class="badge badge-info" id="estado" style="font-size: 13px">
                                        {!! $produccione->produccionEstado->nombre !!}<br>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-8">
                    <div class="card">
                        <h5 class="card-header">Producto Solicitado</h5>
                        <div class="card-body p-2">
                            @include('producciones.tabla_solicitados')
                        </div>
                    </div>
                </div>
            </div>

            <form action="" method="patch"></form>
            {!! Form::model($produccione, ['route' => ['produccion.cocina.store', $produccione->id], 'method' => 'patch']) !!}
            <div class="row">
                <!--            Producto Terminado
                ------------------------------------------------------------------------>
                <div class="col-sm-12">
                    <div class="card">
                        <h5 class="card-header">Producto Terminado</h5>
                        <div class="card-body p-2">
                            <table class="table table-sm table-striped table-bordered table-hover">
                                <thead class="bg-secondary">
                                <tr>
                                    <th scope="col">Código</th>
                                    <th scope="col">Nombre</th>
                                    <th scope="col">Cantidad</th>
                                    <th scope="col">Unidad de Medida</th>
                                    <th scope="col">Fecha Producción</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($produccione->productoRealizados as $det)
                                    <tr class="">
                                        <td>{{$det->item->codigo}}</td>
                                        <td>{{$det->item->nombre}}</td>
                                        <td>{{nf($det->cantidad)}}</td>
                                        <td>{{$det->item->unimed->nombre}}</td>
                                        <td>{{$det->created_at}}</td>
                                    </tr>
                                @endforeach

                                <tr v-for="(det,index) in solicitados" v-show="det.estado!=estado_completado">
                                    <td v-text="det.item.codigo"></td>
                                    <td v-text="det.item.nombre"></td>
                                    <td width="15%" class="p-0">
                                        <table width="100%">
                                            <tr>
                                                <td><input
                                                           :name="'realizado['+det.item.id+']'"
                                                           type="number" value="0"
                                                           class="form-control form-control-sm"
                                                           v-model="det.cant_ingresada"
                                                           min="0"
                                                           step="0.0001"
                                                           :max="det.pendiente"
                                                    ></td>
                                                <td v-show="det.cant_ingresada > det.pendientes">
                                                    <span v-tooltip="'La cantidad ingresada es mayor a lo pendiente o solicitado'">
                                                        <i class="fa fa-exclamation-triangle text-warning" aria-hidden="true"></i>
                                                    </span>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td v-text="det.item.unimed.nombre"></td>
                                    <td >pendiente</td>
                                </tr>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                <!--            Materia prima
                ------------------------------------------------------------------------>
                <div class="col-sm-12">
                    <div class="card">
                        <h5 class="card-header">Materia Prima utilizada</h5>
                        <div class="card-body p-2">
                            {{--Tabla de ingredientes--}}
                            <table class="table table-sm table-striped table-bordered table-hover">
                                <thead class="bg-secondary">
                                <tr>
                                    <th scope="col">Código</th>
                                    <th scope="col">Nombre</th>
                                    <th scope="col">Stock</th>
                                    <th scope="col">Debiera Utilizar</th>
                                    <th scope="col">Utilizo</th>
                                    <th scope="col">U/M</th>
                                    <th scope="col">Fecha consumo</th>
                                </tr>
                                </thead>
                                <tbody >
                                    @foreach($produccione->productoConsumidos as $det)
                                        <tr class="">
                                            <td>{{$det->item->codigo}}</td>
                                            <td>{{$det->item->nombre}}</td>
                                            <td>-</td>
                                            <td>{{nf($det->cantidad_receta,4)}}</td>
                                            <td>{{nf($det->cantidad_ingresada,4)}}</td>
                                            <td>{{$det->item->unimed->nombre}}</td>
                                            <td>{{$det->created_at}}</td>
                                        </tr>
                                    @endforeach
                                    <tr v-for="det in materia_prima">
                                        <td v-text="det.item.codigo"></td>
                                        <td v-text="det.item.nombre"></td>
                                        <td v-text="nf(det.item.stock_tienda)"></td>
                                        <td v-text="nf(det.cantidad)"></td>
                                        <td v-show="">
                                            <input value="0"
                                                   :name="'consumidos['+det.item.id+']'"
                                                type="number" value="0"
                                                class="form-control form-control-sm"
                                                {{--v-model="det.cantidad"--}}
                                                    min="0"
                                                   step="0.0001"
                                                :max="det.cantidad"
                                            >
                                            <input type="hidden" :name="'recetas['+det.item.id+']'" :value="det.cantidad">
                                        </td>
                                        <td>
                                            <input value="0"
                                                   :name="'consumidos['+det.item.id+']'"
                                                   type="number" value="0"
                                                   class="form-control form-control-sm"
                                                   {{--v-model="det.cantidad"--}}
                                                   min="0"
                                                   step="0.0001"
                                                   :max="det.item.stock_tienda"
                                            >
                                        </td>
                                        <td v-text="det.item.unimed.nombre"></td>
                                        <td >Pendiente</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="form-group col-sm-12">
                    @if($produccione->estado->id!=\App\Models\ProduccionEstado::COMPLETADA)
                    <button type="submit" class="btn btn-outline-success">
                        Guardar
                    </button>
                    @endif
                    <a href="{!! route('produccion.cocina.listado') !!}" class="btn btn-default">Regresar</a>
                </div>
            </div>
            {{Form::close()}}
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /.content -->
@endsection
@push('scripts')
<script>
    new Vue({
        el: '#root',
        mounted() {
            // console.log('Instancia vue montada');
        },
        created() {
            // console.log('Instancia vue creada');
            this.getSolicitados();
            this.getMateriaPrima();
        },
        data: {
            'estado_completado': '{{\App\Models\ProductoSolicitado::COMPLETADO}}',
            'solicitados': [],
            'materia_prima': [],
            'loading': false,
            'idEliminando': null,
            'loadingMateriaPrima': false,
            'cantidades': []
        },
        methods: {
            nf(cant){
                cant = parseFloat(cant).toFixed(4)
                return numf(cant);
            },
            getSolicitados(){
                var url = "{{route("get.solicitados",$produccione->id)}}";

                axios.get(url).then(response => {
                    // console.log(response.data.data);
                    this.solicitados = response.data.data;
                })
                    .catch(error => {
                        // console.log(error.response.data);
                    });
            },
            getMateriaPrima(){
                console.log('Get Materia prima');
                this.loadingMateriaPrima = true;

                var url = "{{route("produccion.get.materia",$produccione->id)}}";

                var params = {params:{'solicitados' : this.solicitados}};

                axios.get(url,params).then(response => {
                    //console.log(response.data.data);
                    this.materia_prima = response.data.data;
                    this.loadingMateriaPrima = false;
                })
                    .catch(error => {
                        // console.log(error.response.data);
                        this.loadingMateriaPrima = false;
                    });
            },
            eliminar(item){
                this.idEliminando = item.id;
                // console.log('Eliminar detalle');

                var url = "{{route("produccion.remove")}}";

                var params = { params:{'item_id' : item.id, }};

                axios.get(url,params).then(response => {
                    // console.log(response.data);

                    this.solicitados = response.data.data;
                })
                    .catch(error => {
                        // console.log(error.response);
                    });
            }
        },
        computed:{
            stockInsuficiente(){
                var result = false;
                $.each(this.materia_prima,function (index,det) {
                    if(det.cantidad > det.stock){
                        console.log('Stock insuficiente para realizar produccion');
                        result = true;
                        return false;
                    }
                });

                return result;
            }
        },
        watch:{

        }
    });
</script>
@endpush