<!-- Nombres Field -->
<div class="form-group col-sm-6">
    {!! Form::label('nombres', 'Nombres:') !!}
    {!! Form::text('nombres', null, ['class' => 'form-control']) !!}
</div>

<!-- Apellidos Field -->
<div class="form-group col-sm-6">
    {!! Form::label('apellidos', 'Apellidos:') !!}
    {!! Form::text('apellidos', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-3">
    <label for="tienda_id" class="control-label">
        Tienda:
        {{--<a class="success" data-toggle="modal" href="#modal-form-marcas" tabindex="1000">nueva</a>--}}
    </label>
    {!! Form::select('tienda_id', $tiendas, 1, ['class' => 'form-control','id'=>'tiendas','multiple'=>"multiple",'style'=>'width: 100%']) !!}
</div>

<!-- Telefono Field -->
<div class="form-group col-sm-2">
    {!! Form::label('telefono', 'Telefono:') !!}
    {!! Form::text('telefono', null, ['class' => 'form-control']) !!}
</div>

<!-- Correo Field -->
<div class="form-group col-sm-3">
    {!! Form::label('correo', 'Correo:') !!}
    {!! Form::email('correo', null, ['class' => 'form-control']) !!}
</div>

<!-- Sueldo Diario Field -->
<div class="form-group col-sm-2">
    {!! Form::label('sueldo_diario', 'Sueldo Diario:') !!}
    {!! Form::text('sueldo_diario', null, ['class' => 'form-control']) !!}
</div>

<!-- Genero Field -->
<div class="form-group col-sm-1">
    {!! Form::label('genero', 'Genero:') !!}
    <div class="btn-group" data-toggle="buttons" style="width: 100%">
        @if(isset($empleado))
            <label class="btn btn-default {{$empleado->genero=='M' ? 'active' : ''}}">
                <input type="radio" name="genero" id="option2" value="M" autocomplete="off" {{$empleado->genero=='M' ? 'checked' : ''}}> M
            </label>
            <label class="btn btn-default {{$empleado->genero=='F' ? 'active' : ''}}">
                <input type="radio" name="genero" id="option3" value="F" autocomplete="off" {{$empleado->genero=='F' ? 'checked' : ''}}> F
            </label>
        @else
            <label class="btn btn-default active">
                <input type="radio" name="genero" id="option2" value="M" autocomplete="off" checked> M
            </label>
            <label class="btn btn-default">
                <input type="radio" name="genero" id="option3" value="F" autocomplete="off" > F
            </label>
        @endif
    </div>
</div>


<div class="form-group col-sm-12">
    <div class="card card-outline card-secondary">
        <div class="card-header">
            <h3 class="card-title">Datos adicionales</h3>

            <div class="card-tools">
                <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
            </div>
            <!-- /.card-tools -->
        </div>
        <!-- /.card-header -->
        <div class="card-body">
            <div class="form-row">
                <!-- Fecha Nacimiento Field -->
                <div class="form-group col-sm-2">
                    {!! Form::label('fecha_nacimiento', 'Fecha Nacimiento:') !!}
                    <div class="input-group date">
                        {!! Form::text('fecha_nacimiento', null, ['class' => 'form-control datepicker']) !!}
                        <span class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar"></span>
                </span>
                    </div>
                </div>

                <!-- Fecha Contratacion Field -->
                <div class="form-group col-sm-2">
                    {!! Form::label('fecha_contratacion', 'Fecha Contratacion:') !!}
                    <div class='input-group date' >
                        {!! Form::text('fecha_contratacion', null, ['class' => 'form-control datepicker']) !!}
                        <span class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar"></span>
                </span>
                    </div>
                </div>

                <!-- Puesto Field -->
                <div class="form-group col-sm-6">
                    {!! Form::label('puesto', 'Puesto:') !!}
                    {!! Form::text('puesto', null, ['class' => 'form-control']) !!}
                </div>

                <!-- Porcentaje Comision Field -->
                <div class="form-group col-sm-2">
                    {!! Form::label('porcentaje_comision', '% Comisión:') !!}
                    {!! Form::text('porcentaje_comision', 0, ['class' => 'form-control']) !!}
                </div>

                <!-- Direccion Field -->
                <div class="form-group col-sm-12 col-lg-12">
                    {!! Form::label('direccion', 'Direccion:') !!}
                    {!! Form::textarea('direccion', null, ['class' => 'form-control','rows' => 2]) !!}
                </div>
            </div>
        </div>
        <!-- /.card-body -->
    </div>
</div>

@push('scripts')
    <script>
        $(function () {
            $(".date").datetimepicker({
                format: 'DD/MM/YYYY'
            })

            $('#tiendas').select2({
                language: 'es',
                maximumSelectionLength: 1,
                allowClear: true
            })
        })
    </script>
@endpush
