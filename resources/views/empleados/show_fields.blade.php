<!-- Id Field -->
{!! Form::label('id', 'Id:') !!}
{!! $empleado->id !!}<br>


<!-- Tienda Id Field -->
{!! Form::label('tienda_id', 'Tienda Id:') !!}
{!! $empleado->tienda_id !!}<br>


<!-- Nombres Field -->
{!! Form::label('nombres', 'Nombres:') !!}
{!! $empleado->nombres !!}<br>


<!-- Apellidos Field -->
{!! Form::label('apellidos', 'Apellidos:') !!}
{!! $empleado->apellidos !!}<br>


<!-- Telefono Field -->
{!! Form::label('telefono', 'Telefono:') !!}
{!! $empleado->telefono !!}<br>


<!-- Correo Field -->
{!! Form::label('correo', 'Correo:') !!}
{!! $empleado->correo !!}<br>


<!-- Sueldo Diario Field -->
{!! Form::label('sueldo_diario', 'Sueldo Diario:') !!}
{!! $empleado->sueldo_diario !!}<br>


<!-- Porcentaje Comision Field -->
{!! Form::label('porcentaje_comision', 'Porcentaje Comision:') !!}
{!! $empleado->porcentaje_comision !!}<br>


<!-- Genero Field -->
{!! Form::label('genero', 'Genero:') !!}
{!! $empleado->genero !!}<br>


<!-- Puesto Field -->
{!! Form::label('puesto', 'Puesto:') !!}
{!! $empleado->puesto !!}<br>


<!-- Fecha Contratacion Field -->
{!! Form::label('fecha_contratacion', 'Fecha Contratacion:') !!}
{!! $empleado->fecha_contratacion !!}<br>


<!-- Direccion Field -->
{!! Form::label('direccion', 'Direccion:') !!}
{!! $empleado->direccion !!}<br>


<!-- Fecha Nacimiento Field -->
{!! Form::label('fecha_nacimiento', 'Fecha Nacimiento:') !!}
{!! $empleado->fecha_nacimiento !!}<br>


<!-- Created At Field -->
{!! Form::label('created_at', 'Creado el:') !!}
{!! $empleado->created_at->format('Y/m/d H:i:s')!!}<br>


<!-- Updated At Field -->
{!! Form::label('updated_at', 'Actualizado el:') !!}
{!! $empleado->updated_at->format('Y/m/d H:i:s')!!}<br>


<!-- Deleted At Field -->
{!! Form::label('deleted_at', 'Borrado el:') !!}
{!! $empleado->deleted_at !!}<br>


