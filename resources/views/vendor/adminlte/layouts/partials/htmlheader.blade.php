<head>
    <meta charset="UTF-8">
    <title> @yield('htmlheader_title', config('app.nombre_negocio')) </title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="stylesheet" href="{{asset("/css/AdminLTE.min.css")}}">
    <link rel="stylesheet" href="{{asset("css/skins/skin-".config('app.color_skin','blue').".min.css")}}">
    <link rel="stylesheet" href="{{asset("/bower/font-awesome/css/font-awesome.min.css")}}">
    {{--<link rel="stylesheet" href="{{asset("css/ionicons.min.css")}}">--}}
    <link rel="stylesheet" href="{{asset("/css/bootstrap.min.css")}}">
    <link rel="stylesheet" href="{{asset("/bower/toastr/toastr.min.css")}}">

    <!-- bootstrap-toggle -->
    <link rel="stylesheet" href="{{ asset('bower/bootstrap-toggle/css/bootstrap-toggle.min.css') }}">

    <!--App css-->
    @yield('css')
    @stack('css')
    {{ $css or ''}}

    <!--App css -->
    <style>
        .select2-container--default .select2-selection--multiple .select2-selection__choice{
            color: #0A0A0A;
        }
        .alert {
            margin-bottom: 10px;
        }
        #toast-container > div {
            opacity:1;
        }
        .toggle.ios, .toggle-on.ios, .toggle-off.ios { border-radius: 20px; }
        .toggle.ios .toggle-handle { border-radius: 20px; }
    </style>

    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>

    {{--<script>--}}
        {{--//See https://laracasts.com/discuss/channels/vue/use-trans-in-vuejs--}}
        {{--window.trans = @php--}}
            {{--// copy all translations from /resources/lang/CURRENT_LOCALE/* to global JS variable--}}
            {{--$lang_files = File::files(resource_path() . '/lang/' . App::getLocale());--}}
            {{--$trans = [];--}}
            {{--foreach ($lang_files as $f) {--}}
                {{--$filename = pathinfo($f)['filename'];--}}
                {{--$trans[$filename] = trans($filename);--}}
            {{--}--}}
            {{--$trans['adminlte_lang_message'] = trans('adminlte_lang::message');--}}
            {{--echo json_encode($trans);--}}
        {{--@endphp--}}
    {{--</script>--}}

    <link rel="shortcut icon" type="image/x-icon" href="{{asset('img/favicon.png')}}">
</head>
