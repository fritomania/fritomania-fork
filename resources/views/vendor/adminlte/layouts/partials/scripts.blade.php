{{--<!-- REQUIRED JS SCRIPTS -->--}}

{{--<!-- JQuery and bootstrap are required by Laravel 5.3 in resources/assets/js/bootstrap.js-->--}}
{{--<!-- Laravel App -->--}}
{{--<script src="{{ mix('/js/app.js') }}" type="text/javascript"></script>--}}

{{--<!-- Optionally, you can add Slimscroll and FastClick plugins.--}}
      {{--Both of these plugins are recommended to enhance the--}}
      {{--user experience. Slimscroll is required when using the--}}
      {{--fixed layout. -->--}}
{{--<script>--}}
    {{--window.Laravel = {!! json_encode([--}}
        {{--'csrfToken' => csrf_token(),--}}
    {{--]) !!};--}}
{{--</script>--}}
<!-- jQuery 2.2.3 -->
<script src="{{asset("plugins/jQuery/jquery-2.2.3.min.js")}}"></script>
<!-- jQuery UI 1.11.4 -->
<script src="{{asset("plugins/jQueryUI/jquery-ui.min.js")}}"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
    $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.6 -->
<script src="{{asset("/js/bootstrap.min.js")}}"></script>
<script src="{{asset("/bower/toastr/toastr.min.js")}}"></script>

<!-- bootstrap-toggle -->
<script src="{{ asset('bower/bootstrap-toggle/js/bootstrap-toggle.min.js') }}" type="text/javascript"></script>

<!-- AdminLTE App -->
<script src="{{asset("js/app.min.js")}}"></script>
<!-- Ocultar alertas flash -->
<script>
    $('div.alert').not('.alert-important').delay({{config('app.delay_fade_out_div_alert',3000)}}).fadeOut(350);
</script>

<!-- App scripts -->
<script src="{{asset("js/fnc.js")}}"></script>

{{--<script src="{{mix('js/app.js')}}"></script>--}}

@stack('scripts')
@yield('scripts')
{{$scripts or ''}}
