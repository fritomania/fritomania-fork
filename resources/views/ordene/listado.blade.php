@extends('layouts.app')

@section('htmlheader_title')
    Pedidos {{$lugar}}
@endsection

@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row">
                <div class="col">
                    <h1 class="m-0 text-dark">Pedidos {{$lugar}}</h1>
                </div><!-- /.col -->
                {{--<div class="col">--}}
                {{--<a class="btn btn-outline-info float-right" href="{!! route('home') !!}">--}}
                {{--<i class="fa fa-list"></i>--}}
                {{--<span class="d-none d-sm-inline">Listado</span>--}}
                {{--</a>--}}
                {{--</div><!-- /.col -->--}}
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content" id="root">
        <div class="container-fluid">
            @include('flash::message')

            <ordenes-venta
                    api="{{route("ordenes.datos")}}"
                    url_cambio_estado="{{route('ordenes.cambio.estado')}}"
                    lugar="{{$lugar}}"
                    tienda="{{ session('tienda') }}"
                    url_repartidores ="{{ route('get.repartidores.ajax') }}"
                    url_attach="{{ route('orden.attach') }}"
                    url_save_notify="{{route('api.notificaciones.store')}}"
                    url_imprime="{{route('orden.print.cocina',0)}}"
                    url_anulacion="{{route('ventas.anular.ajax')}}"
                    url_edit="{{route('ventas.edit',0)}}"
                    tipo_notify="{{\App\Models\NotificacionTipo::DELIVERY}}"
            ></ordenes-venta>


        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /.content -->
@endsection

@push('scripts')
<script>
    var EventBus = new Vue;

    new Vue({
        el: '#root',
        mounted() {
            //console.log('Instancia vue montada');
        },
        created() {
            //console.log('Instancia vue creada');
        },
        data: {

        },
        methods: {

        }
    });
</script>
@endpush