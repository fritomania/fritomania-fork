<!-- Admin Field -->
<div class="form-group col-sm-6">
    {!! Form::label('admin','Usuario:') !!}
    {!!
        Form::select(
            'admin',
            \App\User::pluck('name', 'id')->toArray()
            , null
            , ['id'=>'admins','class' => 'form-control','style'=>'width: 100%']
        )
    !!}
</div>

<!-- Nombre Field -->
<div class="form-group col-sm-6">
    {!! Form::label('nombre', 'Nombre:') !!}
    {!! Form::text('nombre', null, ['class' => 'form-control']) !!}
</div>

<!-- Direccion Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('direccion', 'Direccion:') !!}
    {!! Form::textarea('direccion', null, ['class' => 'form-control','rows' => '2']) !!}
</div>

<!-- Telefono Field -->
<div class="form-group col-sm-6">
    {!! Form::label('telefono', 'Telefono:') !!}
    {!! Form::text('telefono', null, ['class' => 'form-control']) !!}
</div>

<!-- ID GOOGLE MAPS -->
<div class="form-group col-sm-6">
    {!! Form::label('id_map', 'ID GOOGLE MAPS: ') !!}&nbsp;<a href="https://developers.google.com/places/place-id" target="_blank">Verificar</a>
    {!! Form::text('id_map', null, ['class' => 'form-control']) !!}
</div>

@push('scripts')
<script>
    $(function () {
        $('#empleados').select2({
            language: 'es',
            maximumSelectionLength: 1,
            allowClear: true
        })

    })
</script>

@endpush