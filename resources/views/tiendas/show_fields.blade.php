<!-- Id Field -->
{!! Form::label('id', 'Id:') !!}
{!! $tienda->id !!}<br>


<!-- Admin Field -->
{!! Form::label('admin', 'Admin:') !!}
{!! $tienda->admin !!}<br>


<!-- Nombre Field -->
{!! Form::label('nombre', 'Nombre:') !!}
{!! $tienda->nombre !!}<br>


<!-- Direccion Field -->
{!! Form::label('direccion', 'Direccion:') !!}
{!! $tienda->direccion !!}<br>


<!-- Telefono Field -->
{!! Form::label('telefono', 'Telefono:') !!}
{!! $tienda->telefono !!}<br>

<!-- ID GOOGLE MAPS -->
{!! Form::label('id_map', 'ID GOOGLE MAPS:') !!}
{!! $tienda->id_map !!}&nbsp;<a href="https://developers.google.com/places/place-id" target="_blank">Verificar</a><br>

<!-- Created At Field -->
{!! Form::label('created_at', 'Creado el:') !!}
{!! $tienda->created_at->format('Y/m/d H:i:s')!!}<br>


<!-- Updated At Field -->
{!! Form::label('updated_at', 'Actualizado el:') !!}
{!! $tienda->updated_at->format('Y/m/d H:i:s')!!}<br>





