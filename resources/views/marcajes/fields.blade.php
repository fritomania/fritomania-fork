<!-- User Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('user_id','Repartidor:') !!}
    {!!
        Form::select(
            'user_id',
            array_prepend(
                \App\User::deTienda()->repartidor()->pluck('name', 'id')->toArray(),
                'Seleccione un Repartidor',
                ''
            ), null
            , ['id'=>'users','class' => 'form-control','style'=>'width: 100%']
        )
    !!}
</div>

<!-- Tipo Marcaje Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('tipo_marcaje_id','Evento Marcaje:') !!}
    {!!
        Form::select(
            'tipo_marcaje_id',
            array_prepend(
                 \App\Models\TipoMarcaje::pluck('nombre', 'id')->toArray(),
                'Seleccione un Evento',
                ''
            ), null
            , ['id'=>'tipo_marcajes','class' => 'form-control','style'=>'width: 100%']
        )
    !!}
</div>

<!-- Fecha Field -->
<div class="form-group col-sm-6">
    {!! Form::label('fecha', 'Fecha:') !!}
    {!! Form::date('fecha', hoyDb(), ['class' => 'form-control']) !!}
</div>

<!-- Hora Field -->
<div class="form-group col-sm-6">
    {!! Form::label('hora', 'Hora:') !!}
    {!! Form::time('hora', hoyDB(), ['class' => 'form-control']) !!}
</div>