<!-- Magnitude Id Field -->
<div class="form-group col-sm-6">
    <label for="magnitude_id" class="control-label">
        Magnitud:
        {{--<a class="success" data-toggle="modal" href="#modal-form-magnitudes" tabindex="1000">nueva</a>--}}
    </label>
    {!! Form::select('magnitude_id', \App\Models\Magnitude::pluck('nombre','id')->toArray(), null, ['class' => 'form-control','id'=>'magnitudes','multiple'=>"multiple",'style'=>'width: 100%']) !!}
</div>

<!-- Simbolo Field -->
<div class="form-group col-sm-6">
    {!! Form::label('simbolo', 'Simbolo:') !!}
    {!! Form::text('simbolo', null, ['class' => 'form-control']) !!}
</div>

<!-- Nombre Field -->
<div class="form-group col-sm-6">
    {!! Form::label('nombre', 'Nombre:') !!}
    {!! Form::text('nombre', null, ['class' => 'form-control']) !!}
</div>

@push('scripts')
<script>
    $(function () {

        $("#magnitudes").select2({
            placeholder: 'Seleccione uno...',
            language: "es",
            maximumSelectionLength: 1,
            allowClear: true

        });
    })
</script>
@endpush