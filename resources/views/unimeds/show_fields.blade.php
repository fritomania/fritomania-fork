<!-- Id Field -->
{!! Form::label('id', 'Id:') !!}
{!! $unimed->id !!}<br>


<!-- Magnitude Id Field -->
{!! Form::label('magnitude_id', 'Magnitude Id:') !!}
{!! $unimed->magnitude_id !!}<br>


<!-- Simbolo Field -->
{!! Form::label('simbolo', 'Simbolo:') !!}
{!! $unimed->simbolo !!}<br>


<!-- Nombre Field -->
{!! Form::label('nombre', 'Nombre:') !!}
{!! $unimed->nombre !!}<br>


<!-- Created At Field -->
{!! Form::label('created_at', 'Creado el:') !!}
{!! $unimed->created_at->format('Y/m/d H:i:s')!!}<br>


<!-- Updated At Field -->
{!! Form::label('updated_at', 'Actualizado el:') !!}
{!! $unimed->updated_at->format('Y/m/d H:i:s')!!}<br>


<!-- Deleted At Field -->
{!! Form::label('deleted_at', 'Borrado el:') !!}
{!! $unimed->deleted_at !!}<br>


