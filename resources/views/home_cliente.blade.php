@extends('layouts.marco_cliente')
@include('layouts.plugins.carouser')

@section('content')
    <div class="page-container" id="PageContainer">
        <main class="main-content" id="MainContent" role="main">
            <!-- BEGIN content_for_index -->
            <div id="shopify-section-1507179171162" class="shopify-section index-section index-section-slideshow">
                <div data-section-id="1507179171162" data-section-type="slideshow-section" >
                    <section class="home-slideshow-layout" style="">
                        <div class="home-slideshow-wrapper">
                            <div class="group-home-slideshow">
                                <div class="home-slideshow-inner">
                                    <div class="home-slideshow-content slideshow_1507179171162" >
                                        <ul>
                                            <li data-transition="random-static" data-masterspeed="2000" data-saveperformance="on">
                                                <img src="cliente/images/banner-1-2.jpg"  alt="" data-bgposition="center top" data-bgfit="auto data-bgrepeat="no-repeat">
                                                <div class="slideshow-caption position-left transition-fade">
                                                    <div class="group">
                                                        <a href="{{route('productos')}}">
                                                            <img src="cliente/images/slideshow-caption-1.png" alt="" />
                                                        </a>
                                                        <a class="_btn" href="{{route('productos')}}">Ver Más</a>
                                                    </div>
                                                </div>
                                            </li>
                                            <li data-transition="random-static" data-masterspeed="2000" data-saveperformance="on">
                                                <img src="cliente/images/banner-2.png" alt="" data-bgposition="center top" data-bgfit="auto" data-bgrepeat="no-repeat">
                                                <div class="slideshow-caption position-left transition-slideup">
                                                    <div class="group">
                                                        <a href="{{route('productos')}}">
                                                            <img src="cliente/images/slideshow-caption-2.png" alt="" />
                                                        </a>
                                                        <a class="_btn" href="{{route('productos')}}">Haz Tu Pedido</a>
                                                    </div>
                                                </div>
                                            </li>
                                            <li data-transition="slideright" data-masterspeed="2000" data-saveperformance="on">
                                                <img src="cliente/images/banner-3.jpg"  alt="" data-bgposition="center top" data-bgfit="auto" data-bgrepeat="no-repeat">
                                                <div class="slideshow-caption position-middle transition-fade">
                                                    <div class="group">
                                                        <a href="{{route('productos')}}">
                                                            <img src="cliente/images/slideshow-caption-3.png" alt="" />
                                                        </a>
                                                        <a class="_btn" href="{{route('productos')}}">¡Ver Más!</a>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                        <div class="tp-bannertimer"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
            <div id="shopify-section-1509012157667" class="shopify-section index-section index-section-banner">

            </div>
            <div id="shopify-section-1509012338884" class="shopify-section index-section index-section-welcome">
                <div data-section-id="1509012338884" data-section-type="welcome-section">
                    <section class="home-welcome-layout not-animated" data-animate="zoomIn" data-delay="200">
                        <div class="container">
                            <div class="row">
                                <div class="home-welcome-inner">
                                    <h2 class="page-title">¡Bienvenido a Fritomanía!</h2>
                                    <div class="home-welcome-content">
                                            <span class="welcome-caption">
                                                <h4>Homenaje a Omedes Tapia</h4>
                                                13 con 69 en Maracaibo nació Empanadas Casa Vieja, que Omedes Tapia fundó hoy trasciende las fronteras llevando el sabor Zuliano con Luis Tapia de la mano el aquí en Chile te espera.
                                            </span>
                                            <br>
                                            <br>
                                        <img class="welcome-banner" src="cliente/images/banner-5.png" alt="" title="">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
            <div class="shopify-section index-section index-section-product">
                <div data-section-id="1509012370397" data-section-type="product-section">
                    <section class="home-product-layout">
                        <div class="container">
                            <div class="row">
                                <div class="banner-product-title not-animated" data-animate="fadeInUp" data-delay="200" style="background-image:  url('{{asset('cliente/images/fondo_1.jpg')}}');">
                                    <div class="title-content">
                                        <h2>Comida Venezolana en Chile</h2>
                                    </div>
                                </div>
                                <div class="home-product-inner">
                                    <div class="home-product-content">
                                        <div class="content_product col-sm-3 not-animated" data-animate="fadeInUp" data-delay="100">
                                            <div class="row-container product list-unstyled clearfix product-circle">
                                                <div class="row-left">
                                                    <a href="{{route('productos')}}" class="hoverBorder container_item">
                                                        <div class="hoverBorderWrapper">
                                                            <img src="cliente/images/21CocaNormal.jpg" class="img-responsive front" alt="Juice Ice Tea">
                                                            <div class="mask"></div>
                                                        </div>
                                                    </a>
                                                    <div class="hover-mask">
                                                        <div class="group-mask">
                                                            <div class="inner-mask">
                                                                <div class="group-actionbutton">
                                                                    <ul class="quickview-wishlist-wrapper">
                                                                        <li class="wishlist">
                                                                            <a title="Añadir a Lista de Deseos" class="wishlist wishlist-juice-ice-tea" data-wishlisthandle="juice-ice-tea"><span class="cs-icon icon-heart"></span></a>
                                                                        </li>
                                                                        <li class="quickview hidden-xs hidden-sm">
                                                                            <div class="product-ajax-cart">
                                                                                <span class="overlay_mask"></span>
                                                                                <div data-handle="juice-ice-tea" data-target="#quick-shop-modal" class="quick_shop" data-toggle="modal">
                                                                                    <a class=""><span class="cs-icon icon-eye"></span></a>
                                                                                </div>
                                                                            </div>
                                                                        </li>
                                                                        <li class="compare">
                                                                            <a title="Comparar" class="compare compare-juice-ice-tea" data-comparehandle="juice-ice-tea"><span class="cs-icon icon-retweet2"></span></a>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                                <form action="{{route('productos')}}" method="post">
                                                                    <div class="effect-ajax-cart">
                                                                        <input type="hidden" name="quantity" value="1">
                                                                        <button class="_btn add-to-cart" data-parent=".parent-fly" type="submit" name="add" title="pedir">pedir</button>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                            <!--inner-mask-->
                                                        </div>
                                                        <!--Group mask-->
                                                    </div>

                                                </div>
                                                <div class="row-right animMix">
                                                    <div class="rating-star">
                                                            <span class="spr-badge" data-rating="0.0"><span class="spr-starrating spr-badge-starrating"><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i></span><span class="spr-badge-caption">No reviews</span>
                                                            </span>
                                                    </div>
                                                    <div class="product-title"><a class="title-5" href="{{route('productos')}}">COCA COLA 1.5 Lts</a></div>
                                                    <div class="product-price">
                                                            <span class="price">
                                                                <span class="money" data-currency-usd="$1500.00">$1.500</span>
                                                            </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="content_product col-sm-3 not-animated" data-animate="fadeInUp" data-delay="200">
                                            <div class="row-container product list-unstyled clearfix product-circle">
                                                <div class="row-left">
                                                    <a href="{{route('productos')}}" class="hoverBorder container_item">
                                                        <div class="hoverBorderWrapper">
                                                            <img src="cliente/images/coca-cola-tradicional-en-lata-350-ml.jpg" class="img-responsive front" alt="Coke">
                                                            <div class="mask"></div>
                                                        </div>
                                                    </a>
                                                    <div class="hover-mask">
                                                        <div class="group-mask">
                                                            <div class="inner-mask">
                                                                <div class="group-actionbutton">
                                                                    <ul class="quickview-wishlist-wrapper">
                                                                        <li class="wishlist">
                                                                            <a title="Añadir a Lista de Deseos" class="wishlist wishlist-coke" data-wishlisthandle="coke"><span class="cs-icon icon-heart"></span></a>
                                                                        </li>
                                                                        <li class="quickview hidden-xs hidden-sm">
                                                                            <div class="product-ajax-cart">
                                                                                <span class="overlay_mask"></span>
                                                                                <div data-handle="coke" data-target="#quick-shop-modal" class="quick_shop" data-toggle="modal">
                                                                                    <a class=""><span class="cs-icon icon-eye"></span></a>
                                                                                </div>
                                                                            </div>
                                                                        </li>
                                                                        <li class="compare">
                                                                            <a title="Comparar" class="compare compare-coke" data-comparehandle="coke"><span class="cs-icon icon-retweet2"></span></a>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                                <form action="{{route('productos')}}" method="post">
                                                                    <div class="effect-ajax-cart">
                                                                        <button class="_btn select-option" type="button" onclick="window.location='{{route('productos')}}';" title="Opciones">Pedir</button>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                            <!--inner-mask-->
                                                        </div>
                                                        <!--Group mask-->
                                                    </div>

                                                </div>
                                                <div class="row-right animMix">
                                                    <div class="rating-star">
                                                            <span class="spr-badge" data-rating="5.0"><span class="spr-starrating spr-badge-starrating"><i class="spr-icon spr-icon-star" style=""></i><i class="spr-icon spr-icon-star" style=""></i><i class="spr-icon spr-icon-star" style=""></i><i class="spr-icon spr-icon-star" style=""></i><i class="spr-icon spr-icon-star" style=""></i></span><span class="spr-badge-caption">1 review</span>
                                                            </span>
                                                    </div>
                                                    <div class="product-title"><a class="title-5" href="{{route('productos')}}">COCA COLA LATA 350 CC</a></div>
                                                    <div class="product-price">
                                                        <span class="price_sale"><span class="money" data-currency-usd="$10.00">$700</span></span>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="content_product col-sm-3 not-animated" data-animate="fadeInUp" data-delay="300">
                                            <div class="row-container product list-unstyled clearfix product-circle">
                                                <div class="row-left">
                                                    <a href="{{route('productos')}}" class="hoverBorder container_item">
                                                        <div class="hoverBorderWrapper">
                                                            <img src="cliente/images/coca-cola-ligth-en-lata-350ml.jpg" class="img-responsive front" alt="Juice Ice Tea">
                                                            <div class="mask"></div>
                                                        </div>
                                                    </a>
                                                    <div class="hover-mask">
                                                        <div class="group-mask">
                                                            <div class="inner-mask">
                                                                <div class="group-actionbutton">
                                                                    <ul class="quickview-wishlist-wrapper">
                                                                        <li class="wishlist">
                                                                            <a title="Añadir a Lista de Deseos" class="wishlist wishlist-juice-ice-tea" data-wishlisthandle="juice-ice-tea"><span class="cs-icon icon-heart"></span></a>
                                                                        </li>
                                                                        <li class="quickview hidden-xs hidden-sm">
                                                                            <div class="product-ajax-cart">
                                                                                <span class="overlay_mask"></span>
                                                                                <div data-handle="juice-ice-tea" data-target="#quick-shop-modal" class="quick_shop" data-toggle="modal">
                                                                                    <a class=""><span class="cs-icon icon-eye"></span></a>
                                                                                </div>
                                                                            </div>
                                                                        </li>
                                                                        <li class="compare">
                                                                            <a title="Comparar" class="compare compare-juice-ice-tea" data-comparehandle="juice-ice-tea"><span class="cs-icon icon-retweet2"></span></a>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                                <form action="{{route('productos')}}" method="post">
                                                                    <div class="effect-ajax-cart">
                                                                        <input type="hidden" name="quantity" value="1">
                                                                        <button class="_btn add-to-cart" data-parent=".parent-fly" type="submit" name="add" title="pedir">pedir</button>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                            <!--inner-mask-->
                                                        </div>
                                                        <!--Group mask-->
                                                    </div>

                                                </div>
                                                <div class="row-right animMix">
                                                    <div class="rating-star">
                                                            <span class="spr-badge" data-rating="0.0"><span class="spr-starrating spr-badge-starrating"><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i></span><span class="spr-badge-caption">No reviews</span>
                                                            </span>
                                                    </div>
                                                    <div class="product-title"><a class="title-5" href="{{route('productos')}}">COCA COLA ZERO LATA 350 CC</a></div>
                                                    <div class="product-price">
                                                            <span class="price">
                                                                <span class="money" data-currency-usd="$20.00">$700</span>
                                                            </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="content_product col-sm-3 not-animated" data-animate="fadeInUp" data-delay="400">
                                            <div class="row-container product list-unstyled clearfix product-circle">
                                                <div class="row-left">
                                                    <a href="{{route('productos')}}" class="hoverBorder container_item">
                                                        <div class="hoverBorderWrapper">
                                                            <img src="cliente/images/tequenos.jpeg" class="img-responsive front" alt="Juice Ice Tea">
                                                            <div class="mask"></div>
                                                        </div>
                                                    </a>
                                                    <div class="hover-mask">
                                                        <div class="group-mask">
                                                            <div class="inner-mask">
                                                                <div class="group-actionbutton">
                                                                    <ul class="quickview-wishlist-wrapper">
                                                                        <li class="wishlist">
                                                                            <a title="Añadir a Lista de Deseos" class="wishlist wishlist-juice-ice-tea" data-wishlisthandle="juice-ice-tea"><span class="cs-icon icon-heart"></span></a>
                                                                        </li>
                                                                        <li class="quickview hidden-xs hidden-sm">
                                                                            <div class="product-ajax-cart">
                                                                                <span class="overlay_mask"></span>
                                                                                <div data-handle="juice-ice-tea" data-target="#quick-shop-modal" class="quick_shop" data-toggle="modal">
                                                                                    <a class=""><span class="cs-icon icon-eye"></span></a>
                                                                                </div>
                                                                            </div>
                                                                        </li>
                                                                        <li class="compare">
                                                                            <a title="Comparar" class="compare compare-juice-ice-tea" data-comparehandle="juice-ice-tea"><span class="cs-icon icon-retweet2"></span></a>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                                <form action="{{route('productos')}}" method="post">
                                                                    <div class="effect-ajax-cart">
                                                                        <input type="hidden" name="quantity" value="1">
                                                                        <button class="_btn add-to-cart" data-parent=".parent-fly" type="submit" name="add" title="pedir">pedir</button>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                            <!--inner-mask-->
                                                        </div>
                                                        <!--Group mask-->
                                                    </div>
                                                </div>
                                                <div class="row-right animMix">
                                                    <div class="rating-star">
                                                            <span class="spr-badge" data-rating="0.0"><span class="spr-starrating spr-badge-starrating"><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i></span><span class="spr-badge-caption">No reviews</span>
                                                            </span>
                                                    </div>
                                                    <div class="product-title"><a class="title-5" href="{{route('productos')}}">COMBO 100 TEQUEÑOS</a></div>
                                                    <div class="product-price">
                                                            <span class="price">
                                                                <span class="money" data-currency-usd="$20.00">$17.000</span>
                                                            </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
            <div class="shopify-section index-section index-section-product">
                <div data-section-id="1509012370397" data-section-type="product-section">
                    <section class="home-product-layout">
                        <div class="container">
                            <div class="row">
                                <div class="banner-product-title not-animated" data-animate="fadeInUp" data-delay="200" style="background-image:  url('{{asset('cliente/images/fondo_1.jpg')}}');">
                                    <div class="title-content">
                                        <h2>Martes de Delivery Gratis</h2>
                                    </div>
                                </div>
                                <div class="home-product-inner">
                                    <div class="home-product-content">
                                        <div class="content_product col-sm-3 not-animated" data-animate="fadeInUp" data-delay="100">
                                            <div class="row-container product list-unstyled clearfix product-circle">
                                                <div class="row-left">
                                                    <a href="{{route('productos')}}" class="hoverBorder container_item">
                                                        <div class="hoverBorderWrapper">
                                                            <img src="cliente/images/combo-2-patacones-225.jpg" class="img-responsive front" alt="Juice Ice Tea">
                                                            <div class="mask"></div>
                                                        </div>
                                                    </a>
                                                    <div class="hover-mask">
                                                        <div class="group-mask">
                                                            <div class="inner-mask">
                                                                <div class="group-actionbutton">
                                                                    <ul class="quickview-wishlist-wrapper">
                                                                        <li class="wishlist">
                                                                            <a title="Añadir a Lista de Deseos" class="wishlist wishlist-juice-ice-tea" data-wishlisthandle="juice-ice-tea"><span class="cs-icon icon-heart"></span></a>
                                                                        </li>
                                                                        <li class="quickview hidden-xs hidden-sm">
                                                                            <div class="product-ajax-cart">
                                                                                <span class="overlay_mask"></span>
                                                                                <div data-handle="juice-ice-tea" data-target="#quick-shop-modal" class="quick_shop" data-toggle="modal">
                                                                                    <a class=""><span class="cs-icon icon-eye"></span></a>
                                                                                </div>
                                                                            </div>
                                                                        </li>
                                                                        <li class="compare">
                                                                            <a title="Comparar" class="compare compare-juice-ice-tea" data-comparehandle="juice-ice-tea"><span class="cs-icon icon-retweet2"></span></a>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                                <form action="{{route('productos')}}" method="post">
                                                                    <div class="effect-ajax-cart">
                                                                        <input type="hidden" name="quantity" value="1">
                                                                        <button class="_btn add-to-cart" data-parent=".parent-fly" type="submit" name="add" title="pedir">pedir</button>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                            <!--inner-mask-->
                                                        </div>
                                                        <!--Group mask-->
                                                    </div>

                                                </div>
                                                <div class="row-right animMix">
                                                    <div class="rating-star">
                                                            <span class="spr-badge" data-rating="0.0"><span class="spr-starrating spr-badge-starrating"><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i></span><span class="spr-badge-caption">No reviews</span>
                                                            </span>
                                                    </div>
                                                    <div class="product-title"><a class="title-5" href="{{route('productos')}}">COMBO PATACON X2</a></div>
                                                    <div class="product-price">
                                                            <span class="price">
                                                                <span class="money" data-currency-usd="$20.00">$6.500</span>
                                                            </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="content_product col-sm-3 not-animated" data-animate="fadeInUp" data-delay="200">
                                            <div class="row-container product list-unstyled clearfix product-circle">
                                                <div class="row-left">
                                                    <a href="{{route('productos')}}" class="hoverBorder container_item">
                                                        <div class="hoverBorderWrapper">
                                                            <img src="cliente/images/combo-22-225.jpg" class="img-responsive front" alt="Coke">
                                                            <div class="mask"></div>
                                                        </div>
                                                    </a>
                                                    <div class="hover-mask">
                                                        <div class="group-mask">
                                                            <div class="inner-mask">
                                                                <div class="group-actionbutton">
                                                                    <ul class="quickview-wishlist-wrapper">
                                                                        <li class="wishlist">
                                                                            <a title="Añadir a Lista de Deseos" class="wishlist wishlist-coke" data-wishlisthandle="coke"><span class="cs-icon icon-heart"></span></a>
                                                                        </li>
                                                                        <li class="quickview hidden-xs hidden-sm">
                                                                            <div class="product-ajax-cart">
                                                                                <span class="overlay_mask"></span>
                                                                                <div data-handle="coke" data-target="#quick-shop-modal" class="quick_shop" data-toggle="modal">
                                                                                    <a class=""><span class="cs-icon icon-eye"></span></a>
                                                                                </div>
                                                                            </div>
                                                                        </li>
                                                                        <li class="compare">
                                                                            <a title="Comparar" class="compare compare-coke" data-comparehandle="coke"><span class="cs-icon icon-retweet2"></span></a>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                                <form action="{{route('productos')}}" method="post">
                                                                    <div class="effect-ajax-cart">
                                                                        <button class="_btn select-option" type="button" onclick="window.location='{{route('productos')}}';" title="Opciones">Pedir</button>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                            <!--inner-mask-->
                                                        </div>
                                                        <!--Group mask-->
                                                    </div>

                                                </div>
                                                <div class="row-right animMix">
                                                    <div class="rating-star">
                                                            <span class="spr-badge" data-rating="5.0"><span class="spr-starrating spr-badge-starrating"><i class="spr-icon spr-icon-star" style=""></i><i class="spr-icon spr-icon-star" style=""></i><i class="spr-icon spr-icon-star" style=""></i><i class="spr-icon spr-icon-star" style=""></i><i class="spr-icon spr-icon-star" style=""></i></span><span class="spr-badge-caption">1 review</span>
                                                            </span>
                                                    </div>
                                                    <div class="product-title"><a class="title-5" href="{{route('productos')}}">COMBO FAMILIAR</a></div>
                                                    <div class="product-price">
                                                        <span class="price_sale"><span class="money" data-currency-usd="$10.00">$13.000</span></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="content_product col-sm-3 not-animated" data-animate="fadeInUp" data-delay="200">
                                            <div class="row-container product list-unstyled clearfix product-circle">
                                                <div class="row-left">
                                                    <a href="{{route('productos')}}" class="hoverBorder container_item">
                                                        <div class="hoverBorderWrapper">
                                                            <img src="cliente/images/combo-100-225.jpg" class="img-responsive front" alt="Coke">
                                                            <div class="mask"></div>
                                                        </div>
                                                    </a>
                                                    <div class="hover-mask">
                                                        <div class="group-mask">
                                                            <div class="inner-mask">
                                                                <div class="group-actionbutton">
                                                                    <ul class="quickview-wishlist-wrapper">
                                                                        <li class="wishlist">
                                                                            <a title="Añadir a Lista de Deseos" class="wishlist wishlist-coke" data-wishlisthandle="coke"><span class="cs-icon icon-heart"></span></a>
                                                                        </li>
                                                                        <li class="quickview hidden-xs hidden-sm">
                                                                            <div class="product-ajax-cart">
                                                                                <span class="overlay_mask"></span>
                                                                                <div data-handle="coke" data-target="#quick-shop-modal" class="quick_shop" data-toggle="modal">
                                                                                    <a class=""><span class="cs-icon icon-eye"></span></a>
                                                                                </div>
                                                                            </div>
                                                                        </li>
                                                                        <li class="compare">
                                                                            <a title="Comparar" class="compare compare-coke" data-comparehandle="coke"><span class="cs-icon icon-retweet2"></span></a>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                                <form action="{{route('productos')}}" method="post">
                                                                    <div class="effect-ajax-cart">
                                                                        <button class="_btn select-option" type="button" onclick="window.location='{{route('productos')}}';" title="Opciones">Pedir</button>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                            <!--inner-mask-->
                                                        </div>
                                                        <!--Group mask-->
                                                    </div>

                                                </div>
                                                <div class="row-right animMix">
                                                    <div class="rating-star">
                                                            <span class="spr-badge" data-rating="5.0"><span class="spr-starrating spr-badge-starrating"><i class="spr-icon spr-icon-star" style=""></i><i class="spr-icon spr-icon-star" style=""></i><i class="spr-icon spr-icon-star" style=""></i><i class="spr-icon spr-icon-star" style=""></i><i class="spr-icon spr-icon-star" style=""></i></span><span class="spr-badge-caption">1 review</span>
                                                            </span>
                                                    </div>
                                                    <div class="product-title"><a class="title-5" href="{{route('productos')}}">PASAPALOS X100 TEQUEÑOS</a></div>
                                                    <div class="product-price">
                                                        <span class="price_sale"><span class="money" data-currency-usd="$10.00">$17.000</span></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="content_product col-sm-3 not-animated" data-animate="fadeInUp" data-delay="400">
                                            <div class="row-container product list-unstyled clearfix product-circle">
                                                <div class="row-left">
                                                    <a href="{{route('productos')}}" class="hoverBorder container_item">
                                                        <div class="hoverBorderWrapper">
                                                            <img src="cliente/images/combo-junior-225.jpg" class="img-responsive front" alt="Juice Ice Tea">
                                                            <div class="mask"></div>
                                                        </div>
                                                    </a>
                                                    <div class="hover-mask">
                                                        <div class="group-mask">
                                                            <div class="inner-mask">
                                                                <div class="group-actionbutton">
                                                                    <ul class="quickview-wishlist-wrapper">
                                                                        <li class="wishlist">
                                                                            <a title="Añadir a Lista de Deseos" class="wishlist wishlist-juice-ice-tea" data-wishlisthandle="juice-ice-tea"><span class="cs-icon icon-heart"></span></a>
                                                                        </li>
                                                                        <li class="quickview hidden-xs hidden-sm">
                                                                            <div class="product-ajax-cart">
                                                                                <span class="overlay_mask"></span>
                                                                                <div data-handle="juice-ice-tea" data-target="#quick-shop-modal" class="quick_shop" data-toggle="modal">
                                                                                    <a class=""><span class="cs-icon icon-eye"></span></a>
                                                                                </div>
                                                                            </div>
                                                                        </li>
                                                                        <li class="compare">
                                                                            <a title="Comparar" class="compare compare-juice-ice-tea" data-comparehandle="juice-ice-tea"><span class="cs-icon icon-retweet2"></span></a>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                                <form action="{{route('productos')}}" method="post">
                                                                    <div class="effect-ajax-cart">
                                                                        <input type="hidden" name="quantity" value="1">
                                                                        <button class="_btn add-to-cart" data-parent=".parent-fly" type="submit" name="add" title="pedir">pedir</button>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                            <!--inner-mask-->
                                                        </div>
                                                        <!--Group mask-->
                                                    </div>

                                                </div>
                                                <div class="row-right animMix">
                                                    <div class="rating-star">
                                                            <span class="spr-badge" data-rating="0.0"><span class="spr-starrating spr-badge-starrating"><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i></span><span class="spr-badge-caption">No reviews</span>
                                                            </span>
                                                    </div>
                                                    <div class="product-title"><a class="title-5" href="{{route('productos')}}">COMBO JUNIOR</a></div>
                                                    <div class="product-price">
                                                            <span class="price">
                                                                <span class="money" data-currency-usd="$20.00">$7.500</span>
                                                            </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
            <div class="shopify-section index-section index-section-product">
                <div data-section-id="1509012370397" data-section-type="product-section">
                    <section class="home-product-layout">
                        <div class="container">
                            <div class="row">
                                <div class="banner-product-title not-animated" data-animate="fadeInUp" data-delay="200" style="background-image:  url('{{asset('cliente/images/fondo_1.jpg')}}');">
                                    <div class="title-content">
                                        <h2>¡Ven a probar el auténtico sabor de la comida zuliana!</h2>
                                    </div>
                                </div>
                                <div class="home-product-inner">
                                    <div class="home-product-content">
                                        <div class="content_product col-sm-3 not-animated" data-animate="fadeInUp" data-delay="100">
                                            <div class="row-container product list-unstyled clearfix product-circle">
                                                <div class="row-left">
                                                    <a href="{{route('productos')}}" class="hoverBorder container_item">
                                                        <div class="hoverBorderWrapper">
                                                            <img src="cliente/images/super-hamburguesa-225.jpg" class="img-responsive front" alt="Juice Ice Tea">
                                                            <div class="mask"></div>
                                                        </div>
                                                    </a>
                                                    <div class="hover-mask">
                                                        <div class="group-mask">
                                                            <div class="inner-mask">
                                                                <div class="group-actionbutton">
                                                                    <ul class="quickview-wishlist-wrapper">
                                                                        <li class="wishlist">
                                                                            <a title="Añadir a Lista de Deseos" class="wishlist wishlist-juice-ice-tea" data-wishlisthandle="juice-ice-tea"><span class="cs-icon icon-heart"></span></a>
                                                                        </li>
                                                                        <li class="quickview hidden-xs hidden-sm">
                                                                            <div class="product-ajax-cart">
                                                                                <span class="overlay_mask"></span>
                                                                                <div data-handle="juice-ice-tea" data-target="#quick-shop-modal" class="quick_shop" data-toggle="modal">
                                                                                    <a class=""><span class="cs-icon icon-eye"></span></a>
                                                                                </div>
                                                                            </div>
                                                                        </li>
                                                                        <li class="compare">
                                                                            <a title="Comparar" class="compare compare-juice-ice-tea" data-comparehandle="juice-ice-tea"><span class="cs-icon icon-retweet2"></span></a>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                                <form action="{{route('productos')}}" method="post">
                                                                    <div class="effect-ajax-cart">
                                                                        <input type="hidden" name="quantity" value="1">
                                                                        <button class="_btn add-to-cart" data-parent=".parent-fly" type="submit" name="add" title="pedir">pedir</button>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                            <!--inner-mask-->
                                                        </div>
                                                        <!--Group mask-->
                                                    </div>
                                                </div>
                                                <div class="row-right animMix">
                                                    <div class="rating-star">
                                                            <span class="spr-badge" data-rating="0.0"><span class="spr-starrating spr-badge-starrating"><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i></span><span class="spr-badge-caption">No reviews</span>
                                                            </span>
                                                    </div>
                                                    <div class="product-title"><a class="title-5" href="{{route('productos')}}">SUPER DOBLE HAMBURGUESA</a></div>
                                                    <div class="product-price">
                                                            <span class="price">
                                                                <span class="money" data-currency-usd="$20.00">$9.500</span>
                                                            </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="content_product col-sm-3 not-animated" data-animate="fadeInUp" data-delay="200">
                                            <div class="row-container product list-unstyled clearfix product-circle">
                                                <div class="row-left">
                                                    <a href="{{route('productos')}}" class="hoverBorder container_item">
                                                        <div class="hoverBorderWrapper">
                                                            <img src="cliente/images/tequeño-pastel-225.jpg" class="img-responsive front" alt="Juice Ice Tea">
                                                            <div class="mask"></div>
                                                        </div>
                                                    </a>
                                                    <div class="hover-mask">
                                                        <div class="group-mask">
                                                            <div class="inner-mask">
                                                                <div class="group-actionbutton">
                                                                    <ul class="quickview-wishlist-wrapper">
                                                                        <li class="wishlist">
                                                                            <a title="Añadir a Lista de Deseos" class="wishlist wishlist-juice-ice-tea" data-wishlisthandle="juice-ice-tea"><span class="cs-icon icon-heart"></span></a>
                                                                        </li>
                                                                        <li class="quickview hidden-xs hidden-sm">
                                                                            <div class="product-ajax-cart">
                                                                                <span class="overlay_mask"></span>
                                                                                <div data-handle="juice-ice-tea" data-target="#quick-shop-modal" class="quick_shop" data-toggle="modal">
                                                                                    <a class=""><span class="cs-icon icon-eye"></span></a>
                                                                                </div>
                                                                            </div>
                                                                        </li>
                                                                        <li class="compare">
                                                                            <a title="Comparar" class="compare compare-juice-ice-tea" data-comparehandle="juice-ice-tea"><span class="cs-icon icon-retweet2"></span></a>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                                <form action="{{route('productos')}}" method="post">
                                                                    <div class="effect-ajax-cart">
                                                                        <input type="hidden" name="quantity" value="1">
                                                                        <button class="_btn add-to-cart" data-parent=".parent-fly" type="submit" name="add" title="pedir">pedir</button>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                            <!--inner-mask-->
                                                        </div>
                                                        <!--Group mask-->
                                                    </div>

                                                </div>
                                                <div class="row-right animMix">
                                                    <div class="rating-star">
                                                            <span class="spr-badge" data-rating="0.0"><span class="spr-starrating spr-badge-starrating"><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i></span><span class="spr-badge-caption">No reviews</span>
                                                            </span>
                                                    </div>
                                                    <div class="product-title"><a class="title-5" href="{{route('productos')}}">TEQUEÑO O PASTEL</a></div>
                                                    <div class="product-price">
                                                            <span class="price">
                                                                <span class="money" data-currency-usd="$20.00">$600</span>
                                                            </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="content_product col-sm-3 not-animated" data-animate="fadeInUp" data-delay="300">
                                            <div class="row-container product list-unstyled clearfix product-circle">
                                                <div class="row-left">
                                                    <a href="{{route('productos')}}" class="hoverBorder container_item">
                                                        <div class="hoverBorderWrapper">
                                                            <img src="cliente/images/patacon-225.jpg" class="img-responsive front" alt="Coke">
                                                            <div class="mask"></div>
                                                        </div>
                                                    </a>
                                                    <div class="hover-mask">
                                                        <div class="group-mask">
                                                            <div class="inner-mask">
                                                                <div class="group-actionbutton">
                                                                    <ul class="quickview-wishlist-wrapper">
                                                                        <li class="wishlist">
                                                                            <a title="Añadir a Lista de Deseos" class="wishlist wishlist-coke" data-wishlisthandle="coke"><span class="cs-icon icon-heart"></span></a>
                                                                        </li>
                                                                        <li class="quickview hidden-xs hidden-sm">
                                                                            <div class="product-ajax-cart">
                                                                                <span class="overlay_mask"></span>
                                                                                <div data-handle="coke" data-target="#quick-shop-modal" class="quick_shop" data-toggle="modal">
                                                                                    <a class=""><span class="cs-icon icon-eye"></span></a>
                                                                                </div>
                                                                            </div>
                                                                        </li>
                                                                        <li class="compare">
                                                                            <a title="Comparar" class="compare compare-coke" data-comparehandle="coke"><span class="cs-icon icon-retweet2"></span></a>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                                <form action="{{route('productos')}}" method="post">
                                                                    <div class="effect-ajax-cart">
                                                                        <button class="_btn select-option" type="button" onclick="window.location='{{route('productos')}}';" title="Opciones">Pedir</button>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                            <!--inner-mask-->
                                                        </div>
                                                        <!--Group mask-->
                                                    </div>
                                                </div>
                                                <div class="row-right animMix">
                                                    <div class="rating-star">
                                                            <span class="spr-badge" data-rating="5.0"><span class="spr-starrating spr-badge-starrating"><i class="spr-icon spr-icon-star" style=""></i><i class="spr-icon spr-icon-star" style=""></i><i class="spr-icon spr-icon-star" style=""></i><i class="spr-icon spr-icon-star" style=""></i><i class="spr-icon spr-icon-star" style=""></i></span><span class="spr-badge-caption">1 review</span>
                                                            </span>
                                                    </div>
                                                    <div class="product-title"><a class="title-5" href="{{route('productos')}}">TUMBARRANCHO X2</a></div>
                                                    <div class="product-price">
                                                        <span class="price_sale"><span class="money" data-currency-usd="$10.00">$5.000</span></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="content_product col-sm-3 not-animated" data-animate="fadeInUp" data-delay="400">
                                            <div class="row-container product list-unstyled clearfix product-circle">
                                                <div class="row-left">
                                                    <a href="{{route('productos')}}" class="hoverBorder container_item">
                                                        <div class="hoverBorderWrapper">
                                                            <img src="cliente/images/tequeyoyo-225.jpg" class="img-responsive front" alt="Juice Ice Tea">
                                                            <div class="mask"></div>
                                                        </div>
                                                    </a>
                                                    <div class="hover-mask">
                                                        <div class="group-mask">
                                                            <div class="inner-mask">
                                                                <div class="group-actionbutton">
                                                                    <ul class="quickview-wishlist-wrapper">
                                                                        <li class="wishlist">
                                                                            <a title="Añadir a Lista de Deseos" class="wishlist wishlist-juice-ice-tea" data-wishlisthandle="juice-ice-tea"><span class="cs-icon icon-heart"></span></a>
                                                                        </li>
                                                                        <li class="quickview hidden-xs hidden-sm">
                                                                            <div class="product-ajax-cart">
                                                                                <span class="overlay_mask"></span>
                                                                                <div data-handle="juice-ice-tea" data-target="#quick-shop-modal" class="quick_shop" data-toggle="modal">
                                                                                    <a class=""><span class="cs-icon icon-eye"></span></a>
                                                                                </div>
                                                                            </div>
                                                                        </li>
                                                                        <li class="compare">
                                                                            <a title="Comparar" class="compare compare-juice-ice-tea" data-comparehandle="juice-ice-tea"><span class="cs-icon icon-retweet2"></span></a>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                                <form action="{{route('productos')}}" method="post">
                                                                    <div class="effect-ajax-cart">
                                                                        <input type="hidden" name="quantity" value="1">
                                                                        <button class="_btn add-to-cart" data-parent=".parent-fly" type="submit" name="add" title="pedir">pedir</button>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                            <!--inner-mask-->
                                                        </div>
                                                        <!--Group mask-->
                                                    </div>

                                                </div>
                                                <div class="row-right animMix">
                                                    <div class="rating-star">
                                                            <span class="spr-badge" data-rating="0.0"><span class="spr-starrating spr-badge-starrating"><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i></span><span class="spr-badge-caption">No reviews</span>
                                                            </span>
                                                    </div>
                                                    <div class="product-title"><a class="title-5" href="{{route('productos')}}">TEQUEYOYO</a></div>
                                                    <div class="product-price">
                                                            <span class="price">
                                                                <span class="money" data-currency-usd="$20.00">$800</span>
                                                            </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
            <div class="shopify-section index-section index-section-product">
                <div data-section-id="1509012370397" data-section-type="product-section">
                    <section class="home-product-layout">
                        <div class="container">
                            <div class="row">
                                <div class="banner-product-title not-animated" data-animate="fadeInUp" data-delay="200" style="background-image:  url('{{asset('cliente/images/fondo_1.jpg')}}');">
                                    <div class="title-content">
                                        <h2>Necesitas una Arepa en tu Vida</h2>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </section>
                </div>
            </div>
            <div id="shopify-section-1509012405077" class="shopify-section index-section index-section-banner">
                <div data-section-id="1509012405077" data-section-type="banner-section">
                    <section class="home-banner-layout not-animated" data-animate="zoomIn" data-delay="200">
                        <div class="container">
                            <div class="row">
                                <div class="home-banner-inner">
                                    <div class="home-banner-content">

                                            <img src="cliente/images/banner-4.jpg" alt="" title="">


                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
            <div id="shopify-section-1509012440185" class="shopify-section index-section index-section-gallery">
                <div data-section-id="1509012440185" data-section-type="banner-section">
                    <section class="home-gallery-layout">
                        <div class="container">
                            <div class="row">
                                <h2 class="page-title">Galería</h2>
                                <div class="home-gallery-inner">
                                    <div class="home-gallery-content">
                                        <div class="gallery-item col-sm-4 not-animated" data-animate="fadeInUp" data-delay="100">
                                            <a class="home-gallery-lookbook" rel="lookbook" href="cliente/images/local-3-370.jpg">
                                                <img src="cliente/images/local-3-370.jpg" alt="" title="">
                                            </a>
                                        </div>
                                        <div class="gallery-item col-sm-4 not-animated" data-animate="fadeInUp" data-delay="200">
                                            <a class="home-gallery-lookbook" rel="lookbook" href="cliente/images/local-2-370.jpg"">
                                                <img src="cliente/images/local-2-370.jpg" alt="" title="">
                                            </a>
                                        </div>
                                        <div class="gallery-item col-sm-4 not-animated" data-animate="fadeInUp" data-delay="300">
                                            <a class="home-gallery-lookbook" rel="lookbook" href="cliente/images/franquicia-370.jpg">
                                                <img src="cliente/images/franquicia-370.jpg" alt="" title="">
                                            </a>
                                        </div>



                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
            <div id="shopify-section-1509012518613" class="shopify-section  index-section index-section-banner">
                <div data-section-id="1509012518613" data-section-type="banner-section">
                    <section class="home-banner-layout not-animated" data-animate="zoomIn" data-delay="200">
                        <div class="container">

                        </div>
                    </section>
                </div>
            </div>

            @include('layouts.partials_cliente.modals_home')
        </main>
    </div>
@endsection

