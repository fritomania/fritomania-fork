<!-- Id Field -->
{!! Form::label('id', 'Id:') !!}
{!! $proveedor->id !!}<br>


<!-- Nit Field -->
{!! Form::label('nit', 'Nit:') !!}
{!! $proveedor->nit !!}<br>


<!-- Nombre Field -->
{!! Form::label('nombre', 'Nombre:') !!}
{!! $proveedor->nombre !!}<br>


<!-- Razon Social Field -->
{!! Form::label('razon_social', 'Razon Social:') !!}
{!! $proveedor->razon_social !!}<br>


<!-- Correo Field -->
{!! Form::label('correo', 'Correo:') !!}
{!! $proveedor->correo !!}<br>


<!-- Telefono Movil Field -->
{!! Form::label('telefono_movil', 'Telefono Movil:') !!}
{!! $proveedor->telefono_movil !!}<br>


<!-- Telefono Oficina Field -->
{!! Form::label('telefono_oficina', 'Telefono Oficina:') !!}
{!! $proveedor->telefono_oficina !!}<br>


<!-- Direccion Field -->
{!! Form::label('direccion', 'Direccion:') !!}
{!! $proveedor->direccion !!}<br>


<!-- Observaciones Field -->
{!! Form::label('observaciones', 'Observaciones:') !!}
{!! $proveedor->observaciones !!}<br>


<!-- Created At Field -->
{!! Form::label('created_at', 'Creado el:') !!}
{!! $proveedor->created_at->format('Y/m/d H:i:s')!!}<br>


<!-- Updated At Field -->
{!! Form::label('updated_at', 'Actualizado el:') !!}
{!! $proveedor->updated_at->format('Y/m/d H:i:s')!!}<br>


<!-- Deleted At Field -->
{!! Form::label('deleted_at', 'Borrado el:') !!}
{!! $proveedor->deleted_at !!}<br>


