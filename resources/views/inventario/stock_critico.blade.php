@extends('layouts.app')

@section('htmlheader_title')
    Definición de stock critico
@endsection

@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row">
                <div class="col">
                    <h1 class="m-0 text-dark">Definición de stock critico</h1>
                </div><!-- /.col -->
                <div class="col">
                    <a class="btn btn-outline-info float-right" href="{!! route('home') !!}">
                        <i class="fa fa-list"></i>
                        <span class="d-none d-sm-inline">Listado</span>
                    </a>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            @include('flash::message')

            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            {{Form::open(['route' => 'stock.critico','method' => 'get'])}}
                                <div class="row">
                                    <div class="form-group col-sm-6">
                                        {!! Form::label('tienda_id','Lugar Bodega o Local:') !!}

                                        {!!
                                            Form::select(
                                                'tienda_id',
                                                \App\Models\Tienda::pluck('nombre','id')->toArray()
                                                , $tienda_id
                                                , ['id'=>'tiendas','class' => 'form-control','style'=>'width: 100%']
                                            )
                                        !!}
                                    </div>
                                    <div class="form-group col-sm-6">
                                        {!! Form::label('boton','&nbsp;') !!}
                                        <div>
                                            <button type="submit" id="boton" class="btn btn-info" value="buscar">Seleccionar</button>
                                        </div>
                                    </div>
                                </div>
                            {{Form::close()}}
                        </div>
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col-md-6 -->
            </div>
            <!-- /.row -->

            @isset($items)
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            {{Form::open(['route' => 'stock.critico.store','method' => 'post'])}}
                                <div class="row">
                                    <div class="col">
                                        <div class="row mb-2">
                                            <div class="col">
                                                Lugar seleccionado: <span class="badge badge-info text-lg"> {{$tienda->nombre}}</span><br>
                                            </div>
                                        </div>
                                        <div class="table-responsive">
                                            <table class="table table-sm table-striped table-bordered">
                                                <thead>
                                                <tr>
                                                    <th>ID</th>
                                                    <th>Código</th>
                                                    <th>Nombre</th>
                                                    <th>Stock Actual</th>
                                                    <th>Stock critico</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($items as $item)
                                                    @php
                                                        $stockCritico = isset($stocks[$item->id]) ? $stocks[$item->id] : 0;
                                                        $stockActual = $item->stockTienda($tienda->id);
                                                    @endphp
                                                    <tr class="{{ $stockCritico>0 && $stockActual<=$stockCritico ? 'bg-danger' : '' }}">
                                                        <td>{{$item->id}}</td>
                                                        <td>{{$item->codigo}}</td>
                                                        <td>{{$item->nombre}}</td>
                                                        <td>{{nf($stockActual)}}</td>
                                                        <td>
                                                            <input
                                                                    name="items[{{$item->id}}][cantidad]"
                                                                    class="form-control form-control-sm"
                                                                    type="number" value="{{$stockCritico}}" min="0">
                                                        </td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <input type="hidden" name="tienda_id" value="{{$tienda_id}}">
                                        <button type="submit" onClick="this.form.submit(); this.disabled=true;" class="btn btn-outline-success">Guardar</button>
                                    </div>
                                </div>
                            {{Form::close()}}
                        </div>
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col-md-6 -->
            </div>
                @endisset
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /.content -->
@endsection

