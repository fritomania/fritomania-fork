@extends('adminlte::layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Pestado
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($pestado, ['route' => ['pestados.update', $pestado->id], 'method' => 'patch']) !!}

                        @include('pestados.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection