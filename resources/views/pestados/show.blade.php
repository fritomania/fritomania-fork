@extends('adminlte::layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Pestado
        </h1>
    </section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    @include('pestados.show_fields')
                    <a href="{!! route('pestados.index') !!}" class="btn btn-default">Regresar</a>
                </div>
            </div>
        </div>
    </div>
@endsection
