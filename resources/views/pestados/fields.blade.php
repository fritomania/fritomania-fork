<!-- Descripcion Field -->
<div class="form-group col-sm-6">
    {!! Form::label('descripcion', 'Descripcion:') !!}
    {!! Form::text('descripcion', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    <button type="submit" onClick="this.form.submit(); this.disabled=true;" class="btn btn-success">Guardar</button>
    <a href="{!! route('pestados.index') !!}" class="btn btn-default">Cancel</a>
</div>
