@extends('adminlte::layouts.errors')

@section('htmlheader_title')
    {{ trans('adminlte_lang::message.pagenotfound') }}
@endsection

@section('main-content')

    <div class="error-page">
        <h2 class="headline text-yellow">419</h2>
        <div class="error-content">
            <h3>
                <i class="fa fa-warning text-yellow"></i>
                La página ha expirado debido a inactividad.
                Por favor, actualice y pruebe de nuevo.
            </h3>
            <p>
                {{ $exception->getMessage() }}
                @guest
                    <a href='{{ route('home') }}'>Ir al home</a>
                @else
                    <a href='{{ route('admin.home') }}'>Ir al home</a>
                @endguest
            </p>
            {{--<form class='search-form'>--}}
                {{--<div class='input-group'>--}}
                    {{--<input type="text" name="search" class='form-control' placeholder="{{ trans('adminlte_lang::message.search') }}"/>--}}
                    {{--<div class="input-group-btn">--}}
                        {{--<button type="submit" name="submit" class="btn btn-warning btn-flat"><i class="fa fa-search"></i></button>--}}
                    {{--</div>--}}
                {{--</div><!-- /.input-group -->--}}
            {{--</form>--}}
        </div><!-- /.error-content -->
    </div><!-- /.error-page -->
@endsection