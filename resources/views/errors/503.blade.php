<!DOCTYPE html>
<html lang="es-ES" class="backgroundImage">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="http://itsb.cl">

    <title>Fritomanía - Sitio en Mantención y Actualización - por negociovirtual.cl</title>

    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,700|Lemon' rel='stylesheet' type='text/css'>

    <link rel="shortcut icon" href="{{asset('mantenimiento/images/favicon.ico')}}">

    <!-- CSS -->
    <link rel="stylesheet" type="text/css" href="{{asset('mantenimiento/css/bootstrap.min.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('mantenimiento/css/font-awesome.min.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('mantenimiento/css/animate.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('mantenimiento/css/YTPlayer.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('mantenimiento/css/supersized.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('mantenimiento/css/styles.css')}}" />


</head>
<body>

<!-- CONTAINER -->
<div class="container">
    <div class="row">
        <div class="col-md-12">

            <div class="section clearfix">
                <h1 class="logo animated fadeInDown"><!--<img src="images/logo_transp_120al.png" title="RCP Propiedades" alt="Food Paal"/>-->Fritomanía</h1>

                <div id="text_slider">
                    <div class="slide clearfix"><h2>Estamos trabajando en el Nuevo Sitio de Fritomanía</h2></div>

                    <div class="slide clearfix"><h2>por negociovirtual.cl!</h2></div>
                </div>

            </div>

            <!-- COUNTDOWN -->
            <div class="section clearfix animated fadeIn" id="countdown">
                <!-- Days -->
                <div class="countdown_group">
                    <span class="countdown_value">{dnn}</span>
                    <span class="countdown_help">{dl}</span>
                </div>

                <!-- Hours -->
                <div class="countdown_group">
                    <span class="countdown_value">{hnn}</span>
                    <span class="countdown_help">{hl}</span>
                </div>

                <!-- Minutes -->
                <div class="countdown_group">
                    <span class="countdown_value">{mnn}</span>
                    <span class="countdown_help">{ml}</span>
                </div>

                <!-- Seconds -->
                <div class="countdown_group">
                    <span class="countdown_value">{snn}</span>
                    <span class="countdown_help">{sl}</span>
                </div>
            </div>
            <!-- END COUNTDOWN -->

            <div class="section clearfix animated fadeIn">
                <p>Quieres enterarte cuando esté terminado?</p>
                <form id="form_newsletter">
                    <div class="row">
                        <div class="col-md-6 col-md-offset-3">
                            <div class="input-group input-group-lg">
                                <input type="email" name="email" class="form-control" placeholder="Ingresa tu Correo Electrónico" >
                                <span class="input-group-btn">
                                            <button class="btn btn-default" type="submit"><i class="fa fa-check"></i> Suscribir!</button>
                                        </span>
                            </div>
                            <div class="clearfix"></div>
                            <div id="form_newsletter_result"></div>
                        </div>
                    </div>
                </form>

            </div>

            <div class="section clearfix animated fadeIn" id="contact">
                <a href="mailto:info@itsb.cl"><i class="fa fa-envelope-o"></i> info@negociovirtual.cl</a>
                <a href="#"><i class="fa fa-phone"></i> (+56) 2 3223 8583</a>
            </div>


            <div class="section clearfix">
                <a href="http://fb.itsb.cl" class="btn btn-transparent btn-facebook"><i class="fa fa-facebook fa-fw"></i></a>
                <a href="http://t.itsb.cl" class="btn btn-transparent btn-twitter"><i class="fa fa-twitter fa-fw"></i></a>
                <a href="http://y.itsb.cl" class="btn btn-transparent btn-youtube"><i class="fa fa-youtube fa-fw"></i></a>
                <a href="http://li.itsb.cl" class="btn btn-transparent btn-linkedin"><i class="fa fa-linkedin fa-fw"></i></a>
            </div>

        </div>
    </div>
</div>
<!-- END CONTAINER -->

<!-- FOOTER -->
<div id="footer">
    <div class="section clearfix">

        <a href="http://itsb.cl" class="open_aboutus btn btn-transparent animated fadeInUp">Conozcanos!</a>
    </div>

    <p>Diseñado por <a href="http://itsb.cl" title="ITSB - Gestión TI Chile">negociovirtual.cl</a> - <a href="mailto:info@itsb.cl">Contáctenos</a></p>
</div>
<!-- END FOOTER -->

<!-- JS -->
<script src="{{asset('mantenimiento/js/jquery.min.js')}}"></script>
<script src="{{asset('mantenimiento/js/jquery.plugin.js')}}"></script>
<script src="{{asset('mantenimiento/js/bootstrap.min.js')}}"></script>
<script src="{{asset('mantenimiento/js/jquery.countdown.min.js')}}"></script>
<script src="{{asset('mantenimiento/js/supersized.min.js')}}"></script>
<script src="{{asset('mantenimiento/js/jquery.cycle.min.js')}}"></script>
<script src="{{asset('mantenimiento/js/jquery.mb.YTPlayer.js')}}"></script>
<script src="{{asset('mantenimiento/js/scripts.js')}}"></script>

<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
<script src="{{asset('mantenimiento/js/html5shiv.js')}}"></script>
<script src="{{asset('mantenimiento/js/respond.min.js')}}"></script>
<![endif]-->
</body>
</html>