<?php
use App\Models\Receta;

include ('web_cliente.php');
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', 'HomeController@index');
Route::get('/home', 'HomeController@index')->name('home');


//Por si alguien intenta acceder a la ruta mediante get, ya que esta esta definida por defecto por post
Route::get('logout', function() {
    return redirect(route('admin.home'));
});

Route::group(['prefix' => 'admin'], function () {

    Route::get('pruebas', 'PruebaController@index');

    Route::get('login', 'Auth\LoginController@showLoginForm')->name('admin.login');

    Route::get('/', 'HomeController@indexAdmin');
    Route::get('/home', 'HomeController@indexAdmin')->name('admin.home');
    Route::get('/dashboard', 'DashBoardController@index')->name('dashboard');


    /**
     * Rutas para administración
     */
    Route::resource('rols', 'RolController');
    Route::get('rol/detach/{user}/{rol}','RolController@detach')->name('rol.detach');
    Route::resource('configurations', 'ConfigurationController');
    Route::resource('admin/empleados', 'EmpleadoController');
    Route::resource('horarios', 'HorarioController');
    Route::resource('correlativos', 'CorrelativoController');
    Route::resource('licencias', 'LicenciaController');
    Route::resource('denominaciones', 'DenominacioneController');
    Route::resource('tipoMarcajes', 'TipoMarcajeController');
    Route::resource('marcajes', 'MarcajeController');

    Route::resource('combos', 'ComboController');
    Route::get('combo/add', 'ComboController@add')->name('combos.add.det');
    Route::get('combo/remove', 'ComboController@remove')->name('combos.remove.det');
    Route::get('combo/session', 'ComboController@getCombo')->name('get.combo.session');
    Route::get('combo/cancelar', 'ComboController@cancelar')->name('combos.cancelar');

    /**
     * Rutas para usuario
     */
    Route::get('user/profile/{user}', 'UserController@editProfile')->name('user.edit.profile');;
    Route::patch('user/profile/{user}', 'UserController@updateProfile')->name('user.update.profile');
    Route::resource('users', 'UserController');
    Route::get('/admin/user/{user}/menu', 'UserController@menu')->name('user.menu');;
    Route::patch('/admin/user/menu/{user}', 'UserController@menuStore')->name('users.menuStore');

    /**
     * Rutas para administrar opciones
     */
    Route::get('/admin/option/create/{padre}', 'OptionMenuController@create')->name('admin.option.create');
    Route::get('option/orden', 'OptionMenuController@updateOrden')->name('option.order.store');
    Route::resource('/admin/option',"OptionMenuController");

    /**
     * Rutas para inventario
     */
    Route::resource('inventario/icategorias', 'IcategoriaController');
    Route::resource('inventario/unimeds', 'UnimedController');
    Route::resource('inventario/iestados', 'IestadoController');
    Route::resource('inventario/items', 'ItemController');
    Route::get('inventario/items/clonar/{item}', 'ItemController@clonar')->name('items.clonar');
    Route::get('inventario/items/precio/compra/cero', 'ItemController@precioCostoCeroView')->name('items.precio.compra.cero');
    Route::post('inventario/items/precio/compra/cero', 'ItemController@precioCostoCeroStore')->name('items.store.precio.compra');
    Route::get('inventario/items/add/ingrediente', 'ItemController@addIngrediente')->name('items.add.ingrediente');
    Route::get('inventario/items/get/ingredientes', 'ItemController@getIngredientes')->name('item.get.ingredientes');
    Route::get('inventario/items/remove/ingrediente', 'ItemController@removeIngrediente')->name('item.remover.ingredientes');
    Route::get('inventario/items/get/agregados', 'ItemController@getAgregados')->name('item.get.agregados');
    Route::get('inventario/items/attach/agregados', 'ItemController@attachAgregados')->name('item.attach.agregados');
    Route::get('inventario/items/detach/agregados', 'ItemController@detachAgregados')->name('item.detach.agregados');

    Route::resource('inventario/marcas', 'MarcaController');
    Route::resource('inventario/cajas', 'CajaController');
    Route::resource('inventario/tiendas', 'TiendaController');
    Route::resource('inventario/descuentos', 'DescuentoController');
    Route::resource('inventario/magnitudes', 'MagnitudeController');
    Route::resource('traslados', 'TrasladoController');

    /**
     * Rutas para compras
     */
    Route::resource('compras/proveedores', 'ProveedorController');
    Route::resource('compras/cestados', 'CestadoController');
    Route::resource('compras/tcomprobantes', 'TcomprobanteController');
    Route::resource('compras/compras', 'CompraController');
    Route::get('compras/cancelar/{tempCompra}', 'CompraController@cancelar')->name('compras.cancelar');
    Route::get('compras/cancelar/solicitud/{compra}', 'CompraController@cancelarSolicictud')->name('compras.cancelar.solicitud');
    Route::get('compras/anular/{compra}', 'CompraController@anular')->name('compras.anular');
    Route::get('compras/pagar', 'CompraController@pagar')->name('compras.pagar');
    Route::get('compras/abonar/{venta}', 'CompraController@abonarView')->name('compras.abonar');
    Route::get('compras/factura/pdf/{compra}', 'CompraController@pdf')->name('compra.pdf');

    /**
     * Rutas para ventas
     */
    Route::resource('ventas/vestados', 'VestadoController');
    Route::resource('ventas/clientes', 'ClienteController');
    Route::resource('ventas/ventas', 'VentaController');
    Route::get('ventas/cancelar/{tempVenta}', 'VentaController@cancelar')->name('ventas.cancelar');
    Route::get('ventas/anular/{venta}', 'VentaController@anular')->name('ventas.anular');
    Route::get('ventas/factura/pdf/{venta}', 'VentaController@pdfFactura')->name('ventas.pdf_factura');
    Route::get('ventas/factura/pdf2/{venta}', 'VentaController@factura')->name('ventas.pdf_factura2');
    Route::get('ventas/factura/{venta}', 'VentaController@facturaView')->name('ventas.factura');
    Route::get('ventas/comprobante/{venta}', 'VentaController@comprobanteView')->name('ventas.comprobante.view');
    Route::resource('ventas/tpagos', 'TpagoController');
    Route::get('ventas/cobrar', 'VentaController@cobrar')->name('ventas.por_cobrar');
    Route::get('ventas/abonar/{venta}', 'VentaController@abonarView')->name('ventas.abonar');
    Route::get('ventas/ajax/anular', 'OrdenesController@anular')->name('ventas.anular.ajax');
    Route::get('ventas/ticket/pdf/{venta}', 'VentaController@ticket')->name('ventas.ticket.pdf');
    Route::get('ventas/ticket/{venta}', 'VentaController@viewTicket')->name('ventas.ticket.view');
    Route::post('venta/store/{tempVenta}', 'VentaController@store')->name('venta.store');
    Route::get('venta/verifica/caja', 'VentaController@verificaCajaAjax')->name('venta.verifica.caja');
    Route::get('venta/get/modificaciones', 'VentaController@getModificaciones')->name('venta.get.modificaciones');
    Route::get('venta/store/modify/det', 'VentaController@storeModifyDet')->name('venta.store.modify');
    Route::get('venta/temp/detalles/{venta}', 'VentaController@getDetalles')->name('venta.temp.detalles');

    /**
     * Rutas para graficas
     */
    Route::get('graficas/ventas/dia', 'DashBoardController@graficaVentasDia')->name('graficas.ventas.dia');
    Route::get('graficas/ventas/mes', 'DashBoardController@graficaVentasMes')->name('graficas.ventas.mes');
    Route::get('graficas/ventas/anio', 'DashBoardController@graficaVentasAnio')->name('graficas.ventas.anio');

    /**
     * Rutas para reportes
     */
    Route::get('rpt/ventas/cat', 'RptVentasController@index')->name('rpt.ventas.cats');
    Route::get('rpt/ventas/cat/res', 'RptVentasController@porCatRes')->name('rpt.ventas.cats.res')->middleware(\App\Http\Middleware\SuperAdmin::class);
    Route::get('rpt/resumen', 'RptResumenController@index')->name('rpt.resumen');
    Route::get('rpt/kardex', 'RptGenController@kardex')->name('rpt.kardex');
    Route::get('rpt/gastos', 'GastoController@rpt')->name('rpt.gastos');
    Route::get('rpt/stock','StockController@rpt')->name('rpt.stock');
    Route::get('rpt/compras/','CompraController@rptComprasDiarias')->name('rpt.compras');

    /**
     * Rutas para contabilidad
     */
    Route::resource('gastos', 'GastoController');
    Route::resource('cretiros', 'CretiroController');
    Route::get('cajas/retiro', 'CretiroController@create')->name('caja.retiro');
    Route::resource('cuadres', 'CuadreController');

    /**
     * Rutas para pedidos
     */
    Route::resource('pedidos/pestados', 'PestadoController');
    Route::resource('pedidos/pedidos', 'PedidoController');
    Route::post('pedidos/procesar/{tempPedido}', 'PedidoController@procesar')->name('pedidos.procesar');
    Route::get('pedidos/cancelar/{tempPedido}', 'PedidoController@cancelar')->name('pedidos.cancelar');
    Route::get('pedidos/anular/{pedido}', 'PedidoController@anular')->name('pedidos.anular');
    Route::get('pedidos/fpdf/{pedido}', 'PedidoController@fpdf')->name('pedidos.fpdf');
    Route::get('pedidos/ingreso/{pedido}', 'PedidoController@ingreso')->name('pedidos.ingreso');
    Route::post('pedidos/ingreso/{pedido}', 'PedidoController@ingreso')->name('pedidos.ingreso');
    Route::get('pedidos/entrega/{pedido}', 'PedidoController@entrega')->name('pedidos.entrega');
    Route::post('pedidos/entrega/{pedido}', 'PedidoController@entrega')->name('pedidos.entrega');


    Route::resource('solicitudEstados', 'SolicitudEstadoController');
    Route::resource('solicitudes', 'SolicitudeController');
    Route::get('solicitudes/cancelar/{tempSolicitude}', 'SolicitudeController@cancelar')->name('solicitud.cancelar');
    Route::get('solicitudes/cancelar/solicitud/{solicitud}', 'SolicitudeController@cancelarSolicitud')->name('solicitud.cancelar.solicitud');
    Route::get('solicitudes/anular/{solicitud}', 'SolicitudeController@anular')->name('solicitud.anular');
    Route::get('mis/solicitudes', 'SolicitudeController@user')->name('mis.solicitudes');
    Route::get('lista/despachar/solicitudes', 'SolicitudeController@despacharList')->name('solicitudes.despachar');
    Route::get('despachar/solicitudes/{solicitud}', 'SolicitudeController@despachar')->name('solicitudes.despachar.store');

    Route::resource('notificacionTipos', 'NotificacionTipoController');
    Route::resource('repartidores', 'RepartidoreController');

    Route::get('user/despacha/user', 'UserController@asignar')->name('user.despacha.user');

    Route::get('pedidos', 'OrdenesController@index')->name('ordenes.index');
    Route::get('pedidos/datos', 'OrdenesController@datos')->name('ordenes.datos');
    Route::get('pedidos/cambio/estado', 'OrdenesController@cambioEstado')->name('ordenes.cambio.estado');
    Route::get('cocina', 'OrdenesController@cocina')->name('ordenes.cocina');
    Route::get('delivery', 'OrdenesController@delivery')->name('delivery.index');
    Route::get('delivery/caja', 'OrdenesController@deliveryCaja')->name('delivery.caja');
    Route::resource('ordenEstados', 'OrdenEstadoController');

    Route::resource('produccionTipos', 'ProduccionTipoController');
    Route::resource('produccionEstados', 'ProduccionEstadoController');
    Route::resource('producciones', 'ProduccioneController');
    Route::get('produccion/add', 'ProduccioneController@add')->name('produccion.add');
    Route::get('produccion/remove', 'ProduccioneController@remove')->name('produccion.remove');
    Route::get('produccion/solicitados/session', 'ProduccioneController@getSolicitadosSesion')->name('get.solicitados.session');
    Route::get('produccion/solicitados/{produccion}', 'ProduccioneController@getSolicitados')->name('get.solicitados');
    Route::get('produccion/materia/prima/sesion', 'ProduccioneController@getMateriaSesion')->name('produccion.get.materia.session');
    Route::get('produccion/materia/prima/{produccion}', 'ProduccioneController@getMateria')->name('produccion.get.materia');
    Route::get('produccion/cocina', 'ProduccioneController@cocinaList')->name('produccion.cocina.listado');
    Route::get('produccion/cocina/atender/{id}', 'ProduccioneController@cocinaAtender')->name('produccion.cocina.atender');
    Route::patch('produccion/cocina/{produccion}', 'ProduccioneController@storeCocina')->name('produccion.cocina.store');
    Route::post('produccion/cancelar/{produccion}', 'ProduccioneController@cancelar')->name('produccion.cancelar');
    Route::post('produccion/anular/{produccion}', 'ProduccioneController@anular')->name('produccion.anular');


    Route::resource('recetas', 'RecetaController');
    Route::get('receta/add', 'RecetaController@add')->name('recetas.add.det');
    Route::get('receta/remove', 'RecetaController@remove')->name('recetas.remove.det');
    Route::get('receta/session', 'RecetaController@getReceta')->name('get.receta.session');
    Route::get('receta/cancelar', 'RecetaController@cancelar')->name('receta.cancelar');

    Route::get('inventario/stock/tienda','InventarioController@index')->name('inventario.stock.tienda');
    Route::get('stock/produccion/mp','InventarioController@stockMp')->name('stock.produccion.mp');
    Route::get('stock/produccion/pf','InventarioController@stockPf')->name('stock.produccion.pf');
    Route::get('compra/ingreso/{id}','CompraController@ingreso')->name('compra.ingreso');

    Route::get('get/repartidores/ajax', 'OrdenesController@repartidores')->name('get.repartidores.ajax');
    Route::get('orden/delivery/attach', 'OrdenesController@attach')->name('orden.attach');

    Route::resource('tipoPagos', 'TipoPagoController');
    Route::get('get/denominaciones', 'DenominacioneController@getAjax')->name('get.denominaciones');
    Route::get('stock_critico', 'InventarioController@stockCriticoView')->name('stock.critico');
    Route::post('stock_critico', 'InventarioController@stockCriticoStore')->name('stock.critico.store');
    Route::get('get/total/dia', 'RptResumenController@getTotalAjax')->name('get.total.dia');
    Route::get('orden/print/cocina/{orden}', 'OrdenesController@imprimeCocina')->name('orden.print.cocina');

    Route::get('notificaciones/ver/{tipo}','NotificacionController@ver')->name('notificaciones.ver');

    Route::get('lugar/cambiar/{lugar}','TiendaController@cambiar')->name('lugar.cambiar');

    Route::get('banner','BannerClienteController@index')->name('admin.banner');

    Route::resource('ingredientes', 'IngredienteController');
});

Route::get('productos', 'OrderController@index')->name('productos');
Route::post('productos', 'OrderController@index')->name('productos');
Route::get('combos', 'OrderController@combos')->name('combos');
Route::view('producto', 'delivery.producto')->name('producto');

Route::post('carro', 'OrderController@vistaCarro')->name('modal.pagar');
Route::get('carro', 'OrderController@vistaCarro')->name('carro');

Route::group(['prefix' => 'cart'], function () {
    Route::get('/', 'CartController@index')->name('cart.index');
    Route::post('add-product', 'CartController@add')->name('add.cart');
    Route::get('remove-product/{id}', 'CartController@remove')->name('remove.cart');
    Route::post('pagar', 'OrderController@pagar')->name('carro.pagar');
    Route::post('autorizar', 'OrderController@autorizar')->name('carro.autorizar');
    Route::post('procesar/webpay', 'OrderController@procesarWebPay')->name('carro.procesar.webpay');
    Route::get('procesar/khipu', 'OrderController@procesarKhipu')->name('carro.procesar.khipu');

    Route::get('store/modify/item', 'OrderController@storeModifyItem')->name('carro.store.modify');


    Route::get('get/modificaciones', 'OrderController@getModificaciones')->name('carro.get.modificaciones');
});


Route::get('cuenta', 'AccountController@index');
Route::get('ejemplo', 'AccountController@ejemplo');

Route::get('locales', 'OrderController@locales')->name('locales');

Route::view('localescopia', 'delivery.localescopia');

Route::get('get/tienda/map', 'TiendaController@getPorIdMap')->name('get.tienda.map');

Route::get('pedir/local', 'OrderController@pedir')->name('pedir.local');
Route::post('pedir/local', 'OrderController@pedir')->name('pedir.ahora');

Route::get('cambiar/tipo/pedido/{tipo}', 'OrderController@cambiaTipoPedido')->name('orden.cambiar.tipo.pedido');

Route::post('orden/exito/{orden}', 'OrderController@exito')->name('orden.exito');
Route::get('orden/exito/{orden}', 'OrderController@exito')->name('orden.exito');
Route::get('seguir/comprando/{anterior}', 'OrderController@seguirComprando')->name('seguir.comprando');
Route::get ('local/detalles/{local}', 'OrderController@detalleLocal')->name('local.detalle');

Route::post('webhook','WebhookController@index');
Route::get('webhook','WebhookController@index');
Route::get('shell','WebhookController@shellView')->name('shell');
