<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:api');

Route::group(['prefix' => 'v1','middleware' => 'auth:api'], function () {
    //    Route::resource('task', 'TasksController');

    //Please do not remove this if you want adminlte:route and adminlte:link commands to works correctly.
    #adminlte_api_routes
});


Route::resource('proveedors', 'ProveedorAPIController');

Route::resource('tcomprobantes', 'TcomprobanteAPIController');

Route::resource('items', 'ItemAPIController');

Route::resource('compra_detalles', 'CompraDetalleAPIController');

Route::resource('clientes', 'ClienteAPIController');

Route::resource('venta_detalles', 'VentaDetalleAPIController');

Route::resource('temp_compra_detalles', 'TempCompraDetalleAPIController');
Route::get('temp/compra/detalles/{compra}', 'TempCompraDetalleAPIController@detallesCompra')->name('temp.compra.detalles');

Route::resource('temp_venta_detalles', 'TempVentaDetalleAPIController');
Route::get('temp/venta/detalles/{venta}', 'TempVentaDetalleAPIController@detallesVenta')->name('temp.venta.detalles');


Route::resource('cajas', 'CajaAPIController');

Route::resource('marcas', 'MarcaAPIController');

Route::resource('temp_pedido_detalles', 'TempPedidoDetalleAPIController');

Route::resource('pedidos', 'PedidoAPIController');
Route::get('pedidos/ingreso/{pedido}', 'PedidoAPIController@ingreso')->name('pedidos.ingreso');
Route::post('pedidos/ingreso/{pedido}', 'PedidoAPIController@ingreso')->name('pedidos.ingreso');


Route::resource('magnitudes', 'MagnitudeAPIController');

Route::resource('unimeds', 'UnimedAPIController');

Route::resource('emarcas', 'EmarcaAPIController');

Route::resource('modelos', 'ModeloAPIController');

Route::resource('etipos', 'EtipoAPIController');

Route::resource('equipos', 'EquipoAPIController');

Route::resource('cpus', 'CpuAPIController');

Route::resource('cpus', 'CpuAPIController');

Route::resource('stipos', 'StipoAPIController');

Route::resource('stipos', 'StipoAPIController');

Route::resource('sestados', 'SestadoAPIController');

Route::resource('eatributos', 'EatributoAPIController');

Route::resource('equipo_atributos', 'EquipoAtributoAPIController');

Route::resource('servicios', 'ServicioAPIController');

Route::resource('temp_files', 'TempFileAPIController');

Route::post('temp_files/multi_store/{user}/{opcion}', 'TempFileAPIController@multiStore')->name('temp_files.multi_store');

Route::resource('admin/correlativos', 'CorrelativoAPIController');

Route::resource('sacciones', 'SaccionesAPIController');

Route::resource('sacciones', 'SaccionesAPIController');

Route::resource('sbitacoras', 'SbitacoraAPIController');

Route::get('servicio/bitacora/{servicio}', 'SbitacoraAPIController@bitacoraServicio')->name('servicio.bitacora');

Route::get('caja/abierta/{user}', 'CajaAPIController@cajaAbierta')->name('caja.abierta');
Route::get('item/codigo', 'ItemAPIController@showByCod')->name('item.codigo');

Route::resource('sgastos', 'SgastoAPIController');

Route::resource('gastos', 'GastoAPIController');

Route::resource('vpagos', 'VpagoAPIController');

Route::resource('cpagos', 'CpagoAPIController');

Route::resource('cretiros', 'CretiroAPIController');

Route::resource('equivalencias', 'EquivalenciaAPIController');
Route::get('equivalencias/item/{item}', 'EquivalenciaAPIController@item')->name('equivalencia.item');

Route::resource('temp_solicitud_detalles', 'TempSolicitudDetalleAPIController');

Route::resource('users', 'UserAPIController');

Route::get('user/despacha/user/attache', 'UserAPIController@attache')->name('user.despacha.user.attache');
Route::get('user/despacha/user/detach', 'UserAPIController@detach')->name('user.despacha.user.detach');

Route::resource('notificaciones', 'NotificacioneAPIController');


Route::resource('notificaciones', 'NotificacioneAPIController');

Route::resource('ingredientes', 'IngredienteAPIController');