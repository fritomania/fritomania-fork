<?php

use App\Models\Magnitude;
use App\Repositories\MagnitudeRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class MagnitudeRepositoryTest extends TestCase
{
    use MakeMagnitudeTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var MagnitudeRepository
     */
    protected $magnitudeRepo;

    public function setUp()
    {
        parent::setUp();
        $this->magnitudeRepo = App::make(MagnitudeRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateMagnitude()
    {
        $magnitude = $this->fakeMagnitudeData();
        $createdMagnitude = $this->magnitudeRepo->create($magnitude);
        $createdMagnitude = $createdMagnitude->toArray();
        $this->assertArrayHasKey('id', $createdMagnitude);
        $this->assertNotNull($createdMagnitude['id'], 'Created Magnitude must have id specified');
        $this->assertNotNull(Magnitude::find($createdMagnitude['id']), 'Magnitude with given id must be in DB');
        $this->assertModelData($magnitude, $createdMagnitude);
    }

    /**
     * @test read
     */
    public function testReadMagnitude()
    {
        $magnitude = $this->makeMagnitude();
        $dbMagnitude = $this->magnitudeRepo->find($magnitude->id);
        $dbMagnitude = $dbMagnitude->toArray();
        $this->assertModelData($magnitude->toArray(), $dbMagnitude);
    }

    /**
     * @test update
     */
    public function testUpdateMagnitude()
    {
        $magnitude = $this->makeMagnitude();
        $fakeMagnitude = $this->fakeMagnitudeData();
        $updatedMagnitude = $this->magnitudeRepo->update($fakeMagnitude, $magnitude->id);
        $this->assertModelData($fakeMagnitude, $updatedMagnitude->toArray());
        $dbMagnitude = $this->magnitudeRepo->find($magnitude->id);
        $this->assertModelData($fakeMagnitude, $dbMagnitude->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteMagnitude()
    {
        $magnitude = $this->makeMagnitude();
        $resp = $this->magnitudeRepo->delete($magnitude->id);
        $this->assertTrue($resp);
        $this->assertNull(Magnitude::find($magnitude->id), 'Magnitude should not exist in DB');
    }
}
