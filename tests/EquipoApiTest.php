<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class EquipoApiTest extends TestCase
{
    use MakeEquipoTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateEquipo()
    {
        $equipo = $this->fakeEquipoData();
        $this->json('POST', '/api/v1/equipos', $equipo);

        $this->assertApiResponse($equipo);
    }

    /**
     * @test
     */
    public function testReadEquipo()
    {
        $equipo = $this->makeEquipo();
        $this->json('GET', '/api/v1/equipos/'.$equipo->id);

        $this->assertApiResponse($equipo->toArray());
    }

    /**
     * @test
     */
    public function testUpdateEquipo()
    {
        $equipo = $this->makeEquipo();
        $editedEquipo = $this->fakeEquipoData();

        $this->json('PUT', '/api/v1/equipos/'.$equipo->id, $editedEquipo);

        $this->assertApiResponse($editedEquipo);
    }

    /**
     * @test
     */
    public function testDeleteEquipo()
    {
        $equipo = $this->makeEquipo();
        $this->json('DELETE', '/api/v1/equipos/'.$equipo->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/equipos/'.$equipo->id);

        $this->assertResponseStatus(404);
    }
}
