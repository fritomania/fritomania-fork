<?php

use App\Models\TempPedidoDetalle;
use App\Repositories\TempPedidoDetalleRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class TempPedidoDetalleRepositoryTest extends TestCase
{
    use MakeTempPedidoDetalleTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var TempPedidoDetalleRepository
     */
    protected $tempPedidoDetalleRepo;

    public function setUp()
    {
        parent::setUp();
        $this->tempPedidoDetalleRepo = App::make(TempPedidoDetalleRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateTempPedidoDetalle()
    {
        $tempPedidoDetalle = $this->fakeTempPedidoDetalleData();
        $createdTempPedidoDetalle = $this->tempPedidoDetalleRepo->create($tempPedidoDetalle);
        $createdTempPedidoDetalle = $createdTempPedidoDetalle->toArray();
        $this->assertArrayHasKey('id', $createdTempPedidoDetalle);
        $this->assertNotNull($createdTempPedidoDetalle['id'], 'Created TempPedidoDetalle must have id specified');
        $this->assertNotNull(TempPedidoDetalle::find($createdTempPedidoDetalle['id']), 'TempPedidoDetalle with given id must be in DB');
        $this->assertModelData($tempPedidoDetalle, $createdTempPedidoDetalle);
    }

    /**
     * @test read
     */
    public function testReadTempPedidoDetalle()
    {
        $tempPedidoDetalle = $this->makeTempPedidoDetalle();
        $dbTempPedidoDetalle = $this->tempPedidoDetalleRepo->find($tempPedidoDetalle->id);
        $dbTempPedidoDetalle = $dbTempPedidoDetalle->toArray();
        $this->assertModelData($tempPedidoDetalle->toArray(), $dbTempPedidoDetalle);
    }

    /**
     * @test update
     */
    public function testUpdateTempPedidoDetalle()
    {
        $tempPedidoDetalle = $this->makeTempPedidoDetalle();
        $fakeTempPedidoDetalle = $this->fakeTempPedidoDetalleData();
        $updatedTempPedidoDetalle = $this->tempPedidoDetalleRepo->update($fakeTempPedidoDetalle, $tempPedidoDetalle->id);
        $this->assertModelData($fakeTempPedidoDetalle, $updatedTempPedidoDetalle->toArray());
        $dbTempPedidoDetalle = $this->tempPedidoDetalleRepo->find($tempPedidoDetalle->id);
        $this->assertModelData($fakeTempPedidoDetalle, $dbTempPedidoDetalle->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteTempPedidoDetalle()
    {
        $tempPedidoDetalle = $this->makeTempPedidoDetalle();
        $resp = $this->tempPedidoDetalleRepo->delete($tempPedidoDetalle->id);
        $this->assertTrue($resp);
        $this->assertNull(TempPedidoDetalle::find($tempPedidoDetalle->id), 'TempPedidoDetalle should not exist in DB');
    }
}
