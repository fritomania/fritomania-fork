<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class StipoApiTest extends TestCase
{
    use MakeStipoTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateStipo()
    {
        $stipo = $this->fakeStipoData();
        $this->json('POST', '/api/v1/stipos', $stipo);

        $this->assertApiResponse($stipo);
    }

    /**
     * @test
     */
    public function testReadStipo()
    {
        $stipo = $this->makeStipo();
        $this->json('GET', '/api/v1/stipos/'.$stipo->id);

        $this->assertApiResponse($stipo->toArray());
    }

    /**
     * @test
     */
    public function testUpdateStipo()
    {
        $stipo = $this->makeStipo();
        $editedStipo = $this->fakeStipoData();

        $this->json('PUT', '/api/v1/stipos/'.$stipo->id, $editedStipo);

        $this->assertApiResponse($editedStipo);
    }

    /**
     * @test
     */
    public function testDeleteStipo()
    {
        $stipo = $this->makeStipo();
        $this->json('DELETE', '/api/v1/stipos/'.$stipo->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/stipos/'.$stipo->id);

        $this->assertResponseStatus(404);
    }
}
