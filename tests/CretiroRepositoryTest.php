<?php

use App\Models\Cretiro;
use App\Repositories\CretiroRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class CretiroRepositoryTest extends TestCase
{
    use MakeCretiroTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var CretiroRepository
     */
    protected $cretiroRepo;

    public function setUp()
    {
        parent::setUp();
        $this->cretiroRepo = App::make(CretiroRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateCretiro()
    {
        $cretiro = $this->fakeCretiroData();
        $createdCretiro = $this->cretiroRepo->create($cretiro);
        $createdCretiro = $createdCretiro->toArray();
        $this->assertArrayHasKey('id', $createdCretiro);
        $this->assertNotNull($createdCretiro['id'], 'Created Cretiro must have id specified');
        $this->assertNotNull(Cretiro::find($createdCretiro['id']), 'Cretiro with given id must be in DB');
        $this->assertModelData($cretiro, $createdCretiro);
    }

    /**
     * @test read
     */
    public function testReadCretiro()
    {
        $cretiro = $this->makeCretiro();
        $dbCretiro = $this->cretiroRepo->find($cretiro->id);
        $dbCretiro = $dbCretiro->toArray();
        $this->assertModelData($cretiro->toArray(), $dbCretiro);
    }

    /**
     * @test update
     */
    public function testUpdateCretiro()
    {
        $cretiro = $this->makeCretiro();
        $fakeCretiro = $this->fakeCretiroData();
        $updatedCretiro = $this->cretiroRepo->update($fakeCretiro, $cretiro->id);
        $this->assertModelData($fakeCretiro, $updatedCretiro->toArray());
        $dbCretiro = $this->cretiroRepo->find($cretiro->id);
        $this->assertModelData($fakeCretiro, $dbCretiro->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteCretiro()
    {
        $cretiro = $this->makeCretiro();
        $resp = $this->cretiroRepo->delete($cretiro->id);
        $this->assertTrue($resp);
        $this->assertNull(Cretiro::find($cretiro->id), 'Cretiro should not exist in DB');
    }
}
