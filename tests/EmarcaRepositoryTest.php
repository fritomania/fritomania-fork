<?php

use App\Models\Emarca;
use App\Repositories\EmarcaRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class EmarcaRepositoryTest extends TestCase
{
    use MakeEmarcaTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var EmarcaRepository
     */
    protected $emarcaRepo;

    public function setUp()
    {
        parent::setUp();
        $this->emarcaRepo = App::make(EmarcaRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateEmarca()
    {
        $emarca = $this->fakeEmarcaData();
        $createdEmarca = $this->emarcaRepo->create($emarca);
        $createdEmarca = $createdEmarca->toArray();
        $this->assertArrayHasKey('id', $createdEmarca);
        $this->assertNotNull($createdEmarca['id'], 'Created Emarca must have id specified');
        $this->assertNotNull(Emarca::find($createdEmarca['id']), 'Emarca with given id must be in DB');
        $this->assertModelData($emarca, $createdEmarca);
    }

    /**
     * @test read
     */
    public function testReadEmarca()
    {
        $emarca = $this->makeEmarca();
        $dbEmarca = $this->emarcaRepo->find($emarca->id);
        $dbEmarca = $dbEmarca->toArray();
        $this->assertModelData($emarca->toArray(), $dbEmarca);
    }

    /**
     * @test update
     */
    public function testUpdateEmarca()
    {
        $emarca = $this->makeEmarca();
        $fakeEmarca = $this->fakeEmarcaData();
        $updatedEmarca = $this->emarcaRepo->update($fakeEmarca, $emarca->id);
        $this->assertModelData($fakeEmarca, $updatedEmarca->toArray());
        $dbEmarca = $this->emarcaRepo->find($emarca->id);
        $this->assertModelData($fakeEmarca, $dbEmarca->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteEmarca()
    {
        $emarca = $this->makeEmarca();
        $resp = $this->emarcaRepo->delete($emarca->id);
        $this->assertTrue($resp);
        $this->assertNull(Emarca::find($emarca->id), 'Emarca should not exist in DB');
    }
}
