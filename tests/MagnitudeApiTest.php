<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class MagnitudeApiTest extends TestCase
{
    use MakeMagnitudeTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateMagnitude()
    {
        $magnitude = $this->fakeMagnitudeData();
        $this->json('POST', '/api/v1/magnitudes', $magnitude);

        $this->assertApiResponse($magnitude);
    }

    /**
     * @test
     */
    public function testReadMagnitude()
    {
        $magnitude = $this->makeMagnitude();
        $this->json('GET', '/api/v1/magnitudes/'.$magnitude->id);

        $this->assertApiResponse($magnitude->toArray());
    }

    /**
     * @test
     */
    public function testUpdateMagnitude()
    {
        $magnitude = $this->makeMagnitude();
        $editedMagnitude = $this->fakeMagnitudeData();

        $this->json('PUT', '/api/v1/magnitudes/'.$magnitude->id, $editedMagnitude);

        $this->assertApiResponse($editedMagnitude);
    }

    /**
     * @test
     */
    public function testDeleteMagnitude()
    {
        $magnitude = $this->makeMagnitude();
        $this->json('DELETE', '/api/v1/magnitudes/'.$magnitude->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/magnitudes/'.$magnitude->id);

        $this->assertResponseStatus(404);
    }
}
