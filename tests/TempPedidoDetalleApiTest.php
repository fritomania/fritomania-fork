<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class TempPedidoDetalleApiTest extends TestCase
{
    use MakeTempPedidoDetalleTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateTempPedidoDetalle()
    {
        $tempPedidoDetalle = $this->fakeTempPedidoDetalleData();
        $this->json('POST', '/api/v1/tempPedidoDetalles', $tempPedidoDetalle);

        $this->assertApiResponse($tempPedidoDetalle);
    }

    /**
     * @test
     */
    public function testReadTempPedidoDetalle()
    {
        $tempPedidoDetalle = $this->makeTempPedidoDetalle();
        $this->json('GET', '/api/v1/tempPedidoDetalles/'.$tempPedidoDetalle->id);

        $this->assertApiResponse($tempPedidoDetalle->toArray());
    }

    /**
     * @test
     */
    public function testUpdateTempPedidoDetalle()
    {
        $tempPedidoDetalle = $this->makeTempPedidoDetalle();
        $editedTempPedidoDetalle = $this->fakeTempPedidoDetalleData();

        $this->json('PUT', '/api/v1/tempPedidoDetalles/'.$tempPedidoDetalle->id, $editedTempPedidoDetalle);

        $this->assertApiResponse($editedTempPedidoDetalle);
    }

    /**
     * @test
     */
    public function testDeleteTempPedidoDetalle()
    {
        $tempPedidoDetalle = $this->makeTempPedidoDetalle();
        $this->json('DELETE', '/api/v1/tempPedidoDetalles/'.$tempPedidoDetalle->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/tempPedidoDetalles/'.$tempPedidoDetalle->id);

        $this->assertResponseStatus(404);
    }
}
