<?php

use App\Models\Stipo;
use App\Repositories\StipoRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class StipoRepositoryTest extends TestCase
{
    use MakeStipoTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var StipoRepository
     */
    protected $stipoRepo;

    public function setUp()
    {
        parent::setUp();
        $this->stipoRepo = App::make(StipoRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateStipo()
    {
        $stipo = $this->fakeStipoData();
        $createdStipo = $this->stipoRepo->create($stipo);
        $createdStipo = $createdStipo->toArray();
        $this->assertArrayHasKey('id', $createdStipo);
        $this->assertNotNull($createdStipo['id'], 'Created Stipo must have id specified');
        $this->assertNotNull(Stipo::find($createdStipo['id']), 'Stipo with given id must be in DB');
        $this->assertModelData($stipo, $createdStipo);
    }

    /**
     * @test read
     */
    public function testReadStipo()
    {
        $stipo = $this->makeStipo();
        $dbStipo = $this->stipoRepo->find($stipo->id);
        $dbStipo = $dbStipo->toArray();
        $this->assertModelData($stipo->toArray(), $dbStipo);
    }

    /**
     * @test update
     */
    public function testUpdateStipo()
    {
        $stipo = $this->makeStipo();
        $fakeStipo = $this->fakeStipoData();
        $updatedStipo = $this->stipoRepo->update($fakeStipo, $stipo->id);
        $this->assertModelData($fakeStipo, $updatedStipo->toArray());
        $dbStipo = $this->stipoRepo->find($stipo->id);
        $this->assertModelData($fakeStipo, $dbStipo->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteStipo()
    {
        $stipo = $this->makeStipo();
        $resp = $this->stipoRepo->delete($stipo->id);
        $this->assertTrue($resp);
        $this->assertNull(Stipo::find($stipo->id), 'Stipo should not exist in DB');
    }
}
