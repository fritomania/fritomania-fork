<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class UnimedApiTest extends TestCase
{
    use MakeUnimedTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateUnimed()
    {
        $unimed = $this->fakeUnimedData();
        $this->json('POST', '/api/v1/unimeds', $unimed);

        $this->assertApiResponse($unimed);
    }

    /**
     * @test
     */
    public function testReadUnimed()
    {
        $unimed = $this->makeUnimed();
        $this->json('GET', '/api/v1/unimeds/'.$unimed->id);

        $this->assertApiResponse($unimed->toArray());
    }

    /**
     * @test
     */
    public function testUpdateUnimed()
    {
        $unimed = $this->makeUnimed();
        $editedUnimed = $this->fakeUnimedData();

        $this->json('PUT', '/api/v1/unimeds/'.$unimed->id, $editedUnimed);

        $this->assertApiResponse($editedUnimed);
    }

    /**
     * @test
     */
    public function testDeleteUnimed()
    {
        $unimed = $this->makeUnimed();
        $this->json('DELETE', '/api/v1/unimeds/'.$unimed->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/unimeds/'.$unimed->id);

        $this->assertResponseStatus(404);
    }
}
