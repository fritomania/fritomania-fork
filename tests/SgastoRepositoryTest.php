<?php

use App\Models\Sgasto;
use App\Repositories\SgastoRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class SgastoRepositoryTest extends TestCase
{
    use MakeSgastoTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var SgastoRepository
     */
    protected $sgastoRepo;

    public function setUp()
    {
        parent::setUp();
        $this->sgastoRepo = App::make(SgastoRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateSgasto()
    {
        $sgasto = $this->fakeSgastoData();
        $createdSgasto = $this->sgastoRepo->create($sgasto);
        $createdSgasto = $createdSgasto->toArray();
        $this->assertArrayHasKey('id', $createdSgasto);
        $this->assertNotNull($createdSgasto['id'], 'Created Sgasto must have id specified');
        $this->assertNotNull(Sgasto::find($createdSgasto['id']), 'Sgasto with given id must be in DB');
        $this->assertModelData($sgasto, $createdSgasto);
    }

    /**
     * @test read
     */
    public function testReadSgasto()
    {
        $sgasto = $this->makeSgasto();
        $dbSgasto = $this->sgastoRepo->find($sgasto->id);
        $dbSgasto = $dbSgasto->toArray();
        $this->assertModelData($sgasto->toArray(), $dbSgasto);
    }

    /**
     * @test update
     */
    public function testUpdateSgasto()
    {
        $sgasto = $this->makeSgasto();
        $fakeSgasto = $this->fakeSgastoData();
        $updatedSgasto = $this->sgastoRepo->update($fakeSgasto, $sgasto->id);
        $this->assertModelData($fakeSgasto, $updatedSgasto->toArray());
        $dbSgasto = $this->sgastoRepo->find($sgasto->id);
        $this->assertModelData($fakeSgasto, $dbSgasto->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteSgasto()
    {
        $sgasto = $this->makeSgasto();
        $resp = $this->sgastoRepo->delete($sgasto->id);
        $this->assertTrue($resp);
        $this->assertNull(Sgasto::find($sgasto->id), 'Sgasto should not exist in DB');
    }
}
