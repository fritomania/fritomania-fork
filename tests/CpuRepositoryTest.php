<?php

use App\Models\Cpu;
use App\Repositories\CpuRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class CpuRepositoryTest extends TestCase
{
    use MakeCpuTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var CpuRepository
     */
    protected $cpuRepo;

    public function setUp()
    {
        parent::setUp();
        $this->cpuRepo = App::make(CpuRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateCpu()
    {
        $cpu = $this->fakeCpuData();
        $createdCpu = $this->cpuRepo->create($cpu);
        $createdCpu = $createdCpu->toArray();
        $this->assertArrayHasKey('id', $createdCpu);
        $this->assertNotNull($createdCpu['id'], 'Created Cpu must have id specified');
        $this->assertNotNull(Cpu::find($createdCpu['id']), 'Cpu with given id must be in DB');
        $this->assertModelData($cpu, $createdCpu);
    }

    /**
     * @test read
     */
    public function testReadCpu()
    {
        $cpu = $this->makeCpu();
        $dbCpu = $this->cpuRepo->find($cpu->id);
        $dbCpu = $dbCpu->toArray();
        $this->assertModelData($cpu->toArray(), $dbCpu);
    }

    /**
     * @test update
     */
    public function testUpdateCpu()
    {
        $cpu = $this->makeCpu();
        $fakeCpu = $this->fakeCpuData();
        $updatedCpu = $this->cpuRepo->update($fakeCpu, $cpu->id);
        $this->assertModelData($fakeCpu, $updatedCpu->toArray());
        $dbCpu = $this->cpuRepo->find($cpu->id);
        $this->assertModelData($fakeCpu, $dbCpu->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteCpu()
    {
        $cpu = $this->makeCpu();
        $resp = $this->cpuRepo->delete($cpu->id);
        $this->assertTrue($resp);
        $this->assertNull(Cpu::find($cpu->id), 'Cpu should not exist in DB');
    }
}
