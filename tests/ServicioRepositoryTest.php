<?php

use App\Models\Servicio;
use App\Repositories\ServicioRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ServicioRepositoryTest extends TestCase
{
    use MakeServicioTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var ServicioRepository
     */
    protected $servicioRepo;

    public function setUp()
    {
        parent::setUp();
        $this->servicioRepo = App::make(ServicioRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateServicio()
    {
        $servicio = $this->fakeServicioData();
        $createdServicio = $this->servicioRepo->create($servicio);
        $createdServicio = $createdServicio->toArray();
        $this->assertArrayHasKey('id', $createdServicio);
        $this->assertNotNull($createdServicio['id'], 'Created Servicio must have id specified');
        $this->assertNotNull(Servicio::find($createdServicio['id']), 'Servicio with given id must be in DB');
        $this->assertModelData($servicio, $createdServicio);
    }

    /**
     * @test read
     */
    public function testReadServicio()
    {
        $servicio = $this->makeServicio();
        $dbServicio = $this->servicioRepo->find($servicio->id);
        $dbServicio = $dbServicio->toArray();
        $this->assertModelData($servicio->toArray(), $dbServicio);
    }

    /**
     * @test update
     */
    public function testUpdateServicio()
    {
        $servicio = $this->makeServicio();
        $fakeServicio = $this->fakeServicioData();
        $updatedServicio = $this->servicioRepo->update($fakeServicio, $servicio->id);
        $this->assertModelData($fakeServicio, $updatedServicio->toArray());
        $dbServicio = $this->servicioRepo->find($servicio->id);
        $this->assertModelData($fakeServicio, $dbServicio->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteServicio()
    {
        $servicio = $this->makeServicio();
        $resp = $this->servicioRepo->delete($servicio->id);
        $this->assertTrue($resp);
        $this->assertNull(Servicio::find($servicio->id), 'Servicio should not exist in DB');
    }
}
