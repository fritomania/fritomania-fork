<?php

use App\Models\Sbitacora;
use App\Repositories\SbitacoraRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class SbitacoraRepositoryTest extends TestCase
{
    use MakeSbitacoraTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var SbitacoraRepository
     */
    protected $sbitacoraRepo;

    public function setUp()
    {
        parent::setUp();
        $this->sbitacoraRepo = App::make(SbitacoraRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateSbitacora()
    {
        $sbitacora = $this->fakeSbitacoraData();
        $createdSbitacora = $this->sbitacoraRepo->create($sbitacora);
        $createdSbitacora = $createdSbitacora->toArray();
        $this->assertArrayHasKey('id', $createdSbitacora);
        $this->assertNotNull($createdSbitacora['id'], 'Created Sbitacora must have id specified');
        $this->assertNotNull(Sbitacora::find($createdSbitacora['id']), 'Sbitacora with given id must be in DB');
        $this->assertModelData($sbitacora, $createdSbitacora);
    }

    /**
     * @test read
     */
    public function testReadSbitacora()
    {
        $sbitacora = $this->makeSbitacora();
        $dbSbitacora = $this->sbitacoraRepo->find($sbitacora->id);
        $dbSbitacora = $dbSbitacora->toArray();
        $this->assertModelData($sbitacora->toArray(), $dbSbitacora);
    }

    /**
     * @test update
     */
    public function testUpdateSbitacora()
    {
        $sbitacora = $this->makeSbitacora();
        $fakeSbitacora = $this->fakeSbitacoraData();
        $updatedSbitacora = $this->sbitacoraRepo->update($fakeSbitacora, $sbitacora->id);
        $this->assertModelData($fakeSbitacora, $updatedSbitacora->toArray());
        $dbSbitacora = $this->sbitacoraRepo->find($sbitacora->id);
        $this->assertModelData($fakeSbitacora, $dbSbitacora->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteSbitacora()
    {
        $sbitacora = $this->makeSbitacora();
        $resp = $this->sbitacoraRepo->delete($sbitacora->id);
        $this->assertTrue($resp);
        $this->assertNull(Sbitacora::find($sbitacora->id), 'Sbitacora should not exist in DB');
    }
}
