<?php

use App\Models\Sacciones;
use App\Repositories\SaccionesRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class SaccionesRepositoryTest extends TestCase
{
    use MakeSaccionesTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var SaccionesRepository
     */
    protected $saccionesRepo;

    public function setUp()
    {
        parent::setUp();
        $this->saccionesRepo = App::make(SaccionesRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateSacciones()
    {
        $sacciones = $this->fakeSaccionesData();
        $createdSacciones = $this->saccionesRepo->create($sacciones);
        $createdSacciones = $createdSacciones->toArray();
        $this->assertArrayHasKey('id', $createdSacciones);
        $this->assertNotNull($createdSacciones['id'], 'Created Sacciones must have id specified');
        $this->assertNotNull(Sacciones::find($createdSacciones['id']), 'Sacciones with given id must be in DB');
        $this->assertModelData($sacciones, $createdSacciones);
    }

    /**
     * @test read
     */
    public function testReadSacciones()
    {
        $sacciones = $this->makeSacciones();
        $dbSacciones = $this->saccionesRepo->find($sacciones->id);
        $dbSacciones = $dbSacciones->toArray();
        $this->assertModelData($sacciones->toArray(), $dbSacciones);
    }

    /**
     * @test update
     */
    public function testUpdateSacciones()
    {
        $sacciones = $this->makeSacciones();
        $fakeSacciones = $this->fakeSaccionesData();
        $updatedSacciones = $this->saccionesRepo->update($fakeSacciones, $sacciones->id);
        $this->assertModelData($fakeSacciones, $updatedSacciones->toArray());
        $dbSacciones = $this->saccionesRepo->find($sacciones->id);
        $this->assertModelData($fakeSacciones, $dbSacciones->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteSacciones()
    {
        $sacciones = $this->makeSacciones();
        $resp = $this->saccionesRepo->delete($sacciones->id);
        $this->assertTrue($resp);
        $this->assertNull(Sacciones::find($sacciones->id), 'Sacciones should not exist in DB');
    }
}
