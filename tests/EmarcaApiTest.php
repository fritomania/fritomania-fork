<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class EmarcaApiTest extends TestCase
{
    use MakeEmarcaTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateEmarca()
    {
        $emarca = $this->fakeEmarcaData();
        $this->json('POST', '/api/v1/emarcas', $emarca);

        $this->assertApiResponse($emarca);
    }

    /**
     * @test
     */
    public function testReadEmarca()
    {
        $emarca = $this->makeEmarca();
        $this->json('GET', '/api/v1/emarcas/'.$emarca->id);

        $this->assertApiResponse($emarca->toArray());
    }

    /**
     * @test
     */
    public function testUpdateEmarca()
    {
        $emarca = $this->makeEmarca();
        $editedEmarca = $this->fakeEmarcaData();

        $this->json('PUT', '/api/v1/emarcas/'.$emarca->id, $editedEmarca);

        $this->assertApiResponse($editedEmarca);
    }

    /**
     * @test
     */
    public function testDeleteEmarca()
    {
        $emarca = $this->makeEmarca();
        $this->json('DELETE', '/api/v1/emarcas/'.$emarca->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/emarcas/'.$emarca->id);

        $this->assertResponseStatus(404);
    }
}
