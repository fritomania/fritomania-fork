<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class EatributoApiTest extends TestCase
{
    use MakeEatributoTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateEatributo()
    {
        $eatributo = $this->fakeEatributoData();
        $this->json('POST', '/api/v1/eatributos', $eatributo);

        $this->assertApiResponse($eatributo);
    }

    /**
     * @test
     */
    public function testReadEatributo()
    {
        $eatributo = $this->makeEatributo();
        $this->json('GET', '/api/v1/eatributos/'.$eatributo->id);

        $this->assertApiResponse($eatributo->toArray());
    }

    /**
     * @test
     */
    public function testUpdateEatributo()
    {
        $eatributo = $this->makeEatributo();
        $editedEatributo = $this->fakeEatributoData();

        $this->json('PUT', '/api/v1/eatributos/'.$eatributo->id, $editedEatributo);

        $this->assertApiResponse($editedEatributo);
    }

    /**
     * @test
     */
    public function testDeleteEatributo()
    {
        $eatributo = $this->makeEatributo();
        $this->json('DELETE', '/api/v1/eatributos/'.$eatributo->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/eatributos/'.$eatributo->id);

        $this->assertResponseStatus(404);
    }
}
