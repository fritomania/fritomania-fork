<?php

use App\Models\EquipoAtributo;
use App\Repositories\EquipoAtributoRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class EquipoAtributoRepositoryTest extends TestCase
{
    use MakeEquipoAtributoTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var EquipoAtributoRepository
     */
    protected $equipoAtributoRepo;

    public function setUp()
    {
        parent::setUp();
        $this->equipoAtributoRepo = App::make(EquipoAtributoRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateEquipoAtributo()
    {
        $equipoAtributo = $this->fakeEquipoAtributoData();
        $createdEquipoAtributo = $this->equipoAtributoRepo->create($equipoAtributo);
        $createdEquipoAtributo = $createdEquipoAtributo->toArray();
        $this->assertArrayHasKey('id', $createdEquipoAtributo);
        $this->assertNotNull($createdEquipoAtributo['id'], 'Created EquipoAtributo must have id specified');
        $this->assertNotNull(EquipoAtributo::find($createdEquipoAtributo['id']), 'EquipoAtributo with given id must be in DB');
        $this->assertModelData($equipoAtributo, $createdEquipoAtributo);
    }

    /**
     * @test read
     */
    public function testReadEquipoAtributo()
    {
        $equipoAtributo = $this->makeEquipoAtributo();
        $dbEquipoAtributo = $this->equipoAtributoRepo->find($equipoAtributo->id);
        $dbEquipoAtributo = $dbEquipoAtributo->toArray();
        $this->assertModelData($equipoAtributo->toArray(), $dbEquipoAtributo);
    }

    /**
     * @test update
     */
    public function testUpdateEquipoAtributo()
    {
        $equipoAtributo = $this->makeEquipoAtributo();
        $fakeEquipoAtributo = $this->fakeEquipoAtributoData();
        $updatedEquipoAtributo = $this->equipoAtributoRepo->update($fakeEquipoAtributo, $equipoAtributo->id);
        $this->assertModelData($fakeEquipoAtributo, $updatedEquipoAtributo->toArray());
        $dbEquipoAtributo = $this->equipoAtributoRepo->find($equipoAtributo->id);
        $this->assertModelData($fakeEquipoAtributo, $dbEquipoAtributo->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteEquipoAtributo()
    {
        $equipoAtributo = $this->makeEquipoAtributo();
        $resp = $this->equipoAtributoRepo->delete($equipoAtributo->id);
        $this->assertTrue($resp);
        $this->assertNull(EquipoAtributo::find($equipoAtributo->id), 'EquipoAtributo should not exist in DB');
    }
}
