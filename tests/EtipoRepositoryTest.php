<?php

use App\Models\Etipo;
use App\Repositories\EtipoRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class EtipoRepositoryTest extends TestCase
{
    use MakeEtipoTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var EtipoRepository
     */
    protected $etipoRepo;

    public function setUp()
    {
        parent::setUp();
        $this->etipoRepo = App::make(EtipoRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateEtipo()
    {
        $etipo = $this->fakeEtipoData();
        $createdEtipo = $this->etipoRepo->create($etipo);
        $createdEtipo = $createdEtipo->toArray();
        $this->assertArrayHasKey('id', $createdEtipo);
        $this->assertNotNull($createdEtipo['id'], 'Created Etipo must have id specified');
        $this->assertNotNull(Etipo::find($createdEtipo['id']), 'Etipo with given id must be in DB');
        $this->assertModelData($etipo, $createdEtipo);
    }

    /**
     * @test read
     */
    public function testReadEtipo()
    {
        $etipo = $this->makeEtipo();
        $dbEtipo = $this->etipoRepo->find($etipo->id);
        $dbEtipo = $dbEtipo->toArray();
        $this->assertModelData($etipo->toArray(), $dbEtipo);
    }

    /**
     * @test update
     */
    public function testUpdateEtipo()
    {
        $etipo = $this->makeEtipo();
        $fakeEtipo = $this->fakeEtipoData();
        $updatedEtipo = $this->etipoRepo->update($fakeEtipo, $etipo->id);
        $this->assertModelData($fakeEtipo, $updatedEtipo->toArray());
        $dbEtipo = $this->etipoRepo->find($etipo->id);
        $this->assertModelData($fakeEtipo, $dbEtipo->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteEtipo()
    {
        $etipo = $this->makeEtipo();
        $resp = $this->etipoRepo->delete($etipo->id);
        $this->assertTrue($resp);
        $this->assertNull(Etipo::find($etipo->id), 'Etipo should not exist in DB');
    }
}
