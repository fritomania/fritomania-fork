<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class CretiroApiTest extends TestCase
{
    use MakeCretiroTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateCretiro()
    {
        $cretiro = $this->fakeCretiroData();
        $this->json('POST', '/api/v1/cretiros', $cretiro);

        $this->assertApiResponse($cretiro);
    }

    /**
     * @test
     */
    public function testReadCretiro()
    {
        $cretiro = $this->makeCretiro();
        $this->json('GET', '/api/v1/cretiros/'.$cretiro->id);

        $this->assertApiResponse($cretiro->toArray());
    }

    /**
     * @test
     */
    public function testUpdateCretiro()
    {
        $cretiro = $this->makeCretiro();
        $editedCretiro = $this->fakeCretiroData();

        $this->json('PUT', '/api/v1/cretiros/'.$cretiro->id, $editedCretiro);

        $this->assertApiResponse($editedCretiro);
    }

    /**
     * @test
     */
    public function testDeleteCretiro()
    {
        $cretiro = $this->makeCretiro();
        $this->json('DELETE', '/api/v1/cretiros/'.$cretiro->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/cretiros/'.$cretiro->id);

        $this->assertResponseStatus(404);
    }
}
