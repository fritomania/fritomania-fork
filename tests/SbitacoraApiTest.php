<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class SbitacoraApiTest extends TestCase
{
    use MakeSbitacoraTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateSbitacora()
    {
        $sbitacora = $this->fakeSbitacoraData();
        $this->json('POST', '/api/v1/sbitacoras', $sbitacora);

        $this->assertApiResponse($sbitacora);
    }

    /**
     * @test
     */
    public function testReadSbitacora()
    {
        $sbitacora = $this->makeSbitacora();
        $this->json('GET', '/api/v1/sbitacoras/'.$sbitacora->id);

        $this->assertApiResponse($sbitacora->toArray());
    }

    /**
     * @test
     */
    public function testUpdateSbitacora()
    {
        $sbitacora = $this->makeSbitacora();
        $editedSbitacora = $this->fakeSbitacoraData();

        $this->json('PUT', '/api/v1/sbitacoras/'.$sbitacora->id, $editedSbitacora);

        $this->assertApiResponse($editedSbitacora);
    }

    /**
     * @test
     */
    public function testDeleteSbitacora()
    {
        $sbitacora = $this->makeSbitacora();
        $this->json('DELETE', '/api/v1/sbitacoras/'.$sbitacora->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/sbitacoras/'.$sbitacora->id);

        $this->assertResponseStatus(404);
    }
}
