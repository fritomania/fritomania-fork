<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class SestadoApiTest extends TestCase
{
    use MakeSestadoTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateSestado()
    {
        $sestado = $this->fakeSestadoData();
        $this->json('POST', '/api/v1/sestados', $sestado);

        $this->assertApiResponse($sestado);
    }

    /**
     * @test
     */
    public function testReadSestado()
    {
        $sestado = $this->makeSestado();
        $this->json('GET', '/api/v1/sestados/'.$sestado->id);

        $this->assertApiResponse($sestado->toArray());
    }

    /**
     * @test
     */
    public function testUpdateSestado()
    {
        $sestado = $this->makeSestado();
        $editedSestado = $this->fakeSestadoData();

        $this->json('PUT', '/api/v1/sestados/'.$sestado->id, $editedSestado);

        $this->assertApiResponse($editedSestado);
    }

    /**
     * @test
     */
    public function testDeleteSestado()
    {
        $sestado = $this->makeSestado();
        $this->json('DELETE', '/api/v1/sestados/'.$sestado->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/sestados/'.$sestado->id);

        $this->assertResponseStatus(404);
    }
}
