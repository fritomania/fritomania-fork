<?php

use App\Models\Unimed;
use App\Repositories\UnimedRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class UnimedRepositoryTest extends TestCase
{
    use MakeUnimedTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var UnimedRepository
     */
    protected $unimedRepo;

    public function setUp()
    {
        parent::setUp();
        $this->unimedRepo = App::make(UnimedRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateUnimed()
    {
        $unimed = $this->fakeUnimedData();
        $createdUnimed = $this->unimedRepo->create($unimed);
        $createdUnimed = $createdUnimed->toArray();
        $this->assertArrayHasKey('id', $createdUnimed);
        $this->assertNotNull($createdUnimed['id'], 'Created Unimed must have id specified');
        $this->assertNotNull(Unimed::find($createdUnimed['id']), 'Unimed with given id must be in DB');
        $this->assertModelData($unimed, $createdUnimed);
    }

    /**
     * @test read
     */
    public function testReadUnimed()
    {
        $unimed = $this->makeUnimed();
        $dbUnimed = $this->unimedRepo->find($unimed->id);
        $dbUnimed = $dbUnimed->toArray();
        $this->assertModelData($unimed->toArray(), $dbUnimed);
    }

    /**
     * @test update
     */
    public function testUpdateUnimed()
    {
        $unimed = $this->makeUnimed();
        $fakeUnimed = $this->fakeUnimedData();
        $updatedUnimed = $this->unimedRepo->update($fakeUnimed, $unimed->id);
        $this->assertModelData($fakeUnimed, $updatedUnimed->toArray());
        $dbUnimed = $this->unimedRepo->find($unimed->id);
        $this->assertModelData($fakeUnimed, $dbUnimed->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteUnimed()
    {
        $unimed = $this->makeUnimed();
        $resp = $this->unimedRepo->delete($unimed->id);
        $this->assertTrue($resp);
        $this->assertNull(Unimed::find($unimed->id), 'Unimed should not exist in DB');
    }
}
