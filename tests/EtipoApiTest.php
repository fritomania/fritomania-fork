<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class EtipoApiTest extends TestCase
{
    use MakeEtipoTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateEtipo()
    {
        $etipo = $this->fakeEtipoData();
        $this->json('POST', '/api/v1/etipos', $etipo);

        $this->assertApiResponse($etipo);
    }

    /**
     * @test
     */
    public function testReadEtipo()
    {
        $etipo = $this->makeEtipo();
        $this->json('GET', '/api/v1/etipos/'.$etipo->id);

        $this->assertApiResponse($etipo->toArray());
    }

    /**
     * @test
     */
    public function testUpdateEtipo()
    {
        $etipo = $this->makeEtipo();
        $editedEtipo = $this->fakeEtipoData();

        $this->json('PUT', '/api/v1/etipos/'.$etipo->id, $editedEtipo);

        $this->assertApiResponse($editedEtipo);
    }

    /**
     * @test
     */
    public function testDeleteEtipo()
    {
        $etipo = $this->makeEtipo();
        $this->json('DELETE', '/api/v1/etipos/'.$etipo->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/etipos/'.$etipo->id);

        $this->assertResponseStatus(404);
    }
}
