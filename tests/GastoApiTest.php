<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class GastoApiTest extends TestCase
{
    use MakeGastoTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateGasto()
    {
        $gasto = $this->fakeGastoData();
        $this->json('POST', '/api/v1/gastos', $gasto);

        $this->assertApiResponse($gasto);
    }

    /**
     * @test
     */
    public function testReadGasto()
    {
        $gasto = $this->makeGasto();
        $this->json('GET', '/api/v1/gastos/'.$gasto->id);

        $this->assertApiResponse($gasto->toArray());
    }

    /**
     * @test
     */
    public function testUpdateGasto()
    {
        $gasto = $this->makeGasto();
        $editedGasto = $this->fakeGastoData();

        $this->json('PUT', '/api/v1/gastos/'.$gasto->id, $editedGasto);

        $this->assertApiResponse($editedGasto);
    }

    /**
     * @test
     */
    public function testDeleteGasto()
    {
        $gasto = $this->makeGasto();
        $this->json('DELETE', '/api/v1/gastos/'.$gasto->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/gastos/'.$gasto->id);

        $this->assertResponseStatus(404);
    }
}
