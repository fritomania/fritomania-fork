<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ServicioApiTest extends TestCase
{
    use MakeServicioTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateServicio()
    {
        $servicio = $this->fakeServicioData();
        $this->json('POST', '/api/v1/servicios', $servicio);

        $this->assertApiResponse($servicio);
    }

    /**
     * @test
     */
    public function testReadServicio()
    {
        $servicio = $this->makeServicio();
        $this->json('GET', '/api/v1/servicios/'.$servicio->id);

        $this->assertApiResponse($servicio->toArray());
    }

    /**
     * @test
     */
    public function testUpdateServicio()
    {
        $servicio = $this->makeServicio();
        $editedServicio = $this->fakeServicioData();

        $this->json('PUT', '/api/v1/servicios/'.$servicio->id, $editedServicio);

        $this->assertApiResponse($editedServicio);
    }

    /**
     * @test
     */
    public function testDeleteServicio()
    {
        $servicio = $this->makeServicio();
        $this->json('DELETE', '/api/v1/servicios/'.$servicio->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/servicios/'.$servicio->id);

        $this->assertResponseStatus(404);
    }
}
