<?php

use Faker\Factory as Faker;
use App\Models\Cpu;
use App\Repositories\CpuRepository;

trait MakeCpuTrait
{
    /**
     * Create fake instance of Cpu and save it in database
     *
     * @param array $cpuFields
     * @return Cpu
     */
    public function makeCpu($cpuFields = [])
    {
        /** @var CpuRepository $cpuRepo */
        $cpuRepo = App::make(CpuRepository::class);
        $theme = $this->fakeCpuData($cpuFields);
        return $cpuRepo->create($theme);
    }

    /**
     * Get fake instance of Cpu
     *
     * @param array $cpuFields
     * @return Cpu
     */
    public function fakeCpu($cpuFields = [])
    {
        return new Cpu($this->fakeCpuData($cpuFields));
    }

    /**
     * Get fake data of Cpu
     *
     * @param array $postFields
     * @return array
     */
    public function fakeCpuData($cpuFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'etipo_id' => $fake->randomDigitNotNull,
            'nombre' => $fake->word,
            'cache' => $fake->word,
            'nucleos' => $fake->word,
            'subprocesos' => $fake->word,
            'frecuencia_basica' => $fake->word,
            'frecuencia_maxima' => $fake->word,
            'unimed_id' => $fake->randomDigitNotNull,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s'),
            'deleted_at' => $fake->date('Y-m-d H:i:s')
        ], $cpuFields);
    }
}
