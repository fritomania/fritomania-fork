<?php

use Faker\Factory as Faker;
use App\Models\Cretiro;
use App\Repositories\CretiroRepository;

trait MakeCretiroTrait
{
    /**
     * Create fake instance of Cretiro and save it in database
     *
     * @param array $cretiroFields
     * @return Cretiro
     */
    public function makeCretiro($cretiroFields = [])
    {
        /** @var CretiroRepository $cretiroRepo */
        $cretiroRepo = App::make(CretiroRepository::class);
        $theme = $this->fakeCretiroData($cretiroFields);
        return $cretiroRepo->create($theme);
    }

    /**
     * Get fake instance of Cretiro
     *
     * @param array $cretiroFields
     * @return Cretiro
     */
    public function fakeCretiro($cretiroFields = [])
    {
        return new Cretiro($this->fakeCretiroData($cretiroFields));
    }

    /**
     * Get fake data of Cretiro
     *
     * @param array $postFields
     * @return array
     */
    public function fakeCretiroData($cretiroFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'caja_id' => $fake->randomDigitNotNull,
            'motivo' => $fake->word,
            'monto' => $fake->word,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s'),
            'deleted_at' => $fake->date('Y-m-d H:i:s')
        ], $cretiroFields);
    }
}
