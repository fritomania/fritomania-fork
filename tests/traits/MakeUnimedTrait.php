<?php

use Faker\Factory as Faker;
use App\Models\Unimed;
use App\Repositories\UnimedRepository;

trait MakeUnimedTrait
{
    /**
     * Create fake instance of Unimed and save it in database
     *
     * @param array $unimedFields
     * @return Unimed
     */
    public function makeUnimed($unimedFields = [])
    {
        /** @var UnimedRepository $unimedRepo */
        $unimedRepo = App::make(UnimedRepository::class);
        $theme = $this->fakeUnimedData($unimedFields);
        return $unimedRepo->create($theme);
    }

    /**
     * Get fake instance of Unimed
     *
     * @param array $unimedFields
     * @return Unimed
     */
    public function fakeUnimed($unimedFields = [])
    {
        return new Unimed($this->fakeUnimedData($unimedFields));
    }

    /**
     * Get fake data of Unimed
     *
     * @param array $postFields
     * @return array
     */
    public function fakeUnimedData($unimedFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'magnitude_id' => $fake->randomDigitNotNull,
            'simbolo' => $fake->word,
            'nombre' => $fake->word,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s'),
            'deleted_at' => $fake->date('Y-m-d H:i:s')
        ], $unimedFields);
    }
}
