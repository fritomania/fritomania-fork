<?php

use Faker\Factory as Faker;
use App\Models\Notificacione;
use App\Repositories\NotificacioneRepository;

trait MakeNotificacioneTrait
{
    /**
     * Create fake instance of Notificacione and save it in database
     *
     * @param array $notificacioneFields
     * @return Notificacione
     */
    public function makeNotificacione($notificacioneFields = [])
    {
        /** @var NotificacioneRepository $notificacioneRepo */
        $notificacioneRepo = App::make(NotificacioneRepository::class);
        $theme = $this->fakeNotificacioneData($notificacioneFields);
        return $notificacioneRepo->create($theme);
    }

    /**
     * Get fake instance of Notificacione
     *
     * @param array $notificacioneFields
     * @return Notificacione
     */
    public function fakeNotificacione($notificacioneFields = [])
    {
        return new Notificacione($this->fakeNotificacioneData($notificacioneFields));
    }

    /**
     * Get fake data of Notificacione
     *
     * @param array $postFields
     * @return array
     */
    public function fakeNotificacioneData($notificacioneFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'mensaje' => $fake->word,
            'user_id' => $fake->randomDigitNotNull,
            'notificacion_tipo_id' => $fake->randomDigitNotNull,
            'visto' => $fake->word,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s'),
            'deleted_at' => $fake->date('Y-m-d H:i:s')
        ], $notificacioneFields);
    }
}
