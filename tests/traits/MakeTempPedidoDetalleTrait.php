<?php

use Faker\Factory as Faker;
use App\Models\TempPedidoDetalle;
use App\Repositories\TempPedidoDetalleRepository;

trait MakeTempPedidoDetalleTrait
{
    /**
     * Create fake instance of TempPedidoDetalle and save it in database
     *
     * @param array $tempPedidoDetalleFields
     * @return TempPedidoDetalle
     */
    public function makeTempPedidoDetalle($tempPedidoDetalleFields = [])
    {
        /** @var TempPedidoDetalleRepository $tempPedidoDetalleRepo */
        $tempPedidoDetalleRepo = App::make(TempPedidoDetalleRepository::class);
        $theme = $this->fakeTempPedidoDetalleData($tempPedidoDetalleFields);
        return $tempPedidoDetalleRepo->create($theme);
    }

    /**
     * Get fake instance of TempPedidoDetalle
     *
     * @param array $tempPedidoDetalleFields
     * @return TempPedidoDetalle
     */
    public function fakeTempPedidoDetalle($tempPedidoDetalleFields = [])
    {
        return new TempPedidoDetalle($this->fakeTempPedidoDetalleData($tempPedidoDetalleFields));
    }

    /**
     * Get fake data of TempPedidoDetalle
     *
     * @param array $postFields
     * @return array
     */
    public function fakeTempPedidoDetalleData($tempPedidoDetalleFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'temp_pedido_id' => $fake->randomDigitNotNull,
            'item_id' => $fake->randomDigitNotNull,
            'cantidad' => $fake->word,
            'precio' => $fake->word,
            'descuento' => $fake->word,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s'),
            'deleted_at' => $fake->date('Y-m-d H:i:s')
        ], $tempPedidoDetalleFields);
    }
}
