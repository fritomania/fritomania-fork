<?php

use Faker\Factory as Faker;
use App\Models\Correlativo;
use App\Repositories\CorrelativoRepository;

trait MakeCorrelativoTrait
{
    /**
     * Create fake instance of Correlativo and save it in database
     *
     * @param array $correlativoFields
     * @return Correlativo
     */
    public function makeCorrelativo($correlativoFields = [])
    {
        /** @var CorrelativoRepository $correlativoRepo */
        $correlativoRepo = App::make(CorrelativoRepository::class);
        $theme = $this->fakeCorrelativoData($correlativoFields);
        return $correlativoRepo->create($theme);
    }

    /**
     * Get fake instance of Correlativo
     *
     * @param array $correlativoFields
     * @return Correlativo
     */
    public function fakeCorrelativo($correlativoFields = [])
    {
        return new Correlativo($this->fakeCorrelativoData($correlativoFields));
    }

    /**
     * Get fake data of Correlativo
     *
     * @param array $postFields
     * @return array
     */
    public function fakeCorrelativoData($correlativoFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'tabla' => $fake->word,
            'anio' => $fake->randomDigitNotNull,
            'max' => $fake->randomDigitNotNull,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s'),
            'deleted_at' => $fake->date('Y-m-d H:i:s')
        ], $correlativoFields);
    }
}
