<?php

use Faker\Factory as Faker;
use App\Models\Sacciones;
use App\Repositories\SaccionesRepository;

trait MakeSaccionesTrait
{
    /**
     * Create fake instance of Sacciones and save it in database
     *
     * @param array $saccionesFields
     * @return Sacciones
     */
    public function makeSacciones($saccionesFields = [])
    {
        /** @var SaccionesRepository $saccionesRepo */
        $saccionesRepo = App::make(SaccionesRepository::class);
        $theme = $this->fakeSaccionesData($saccionesFields);
        return $saccionesRepo->create($theme);
    }

    /**
     * Get fake instance of Sacciones
     *
     * @param array $saccionesFields
     * @return Sacciones
     */
    public function fakeSacciones($saccionesFields = [])
    {
        return new Sacciones($this->fakeSaccionesData($saccionesFields));
    }

    /**
     * Get fake data of Sacciones
     *
     * @param array $postFields
     * @return array
     */
    public function fakeSaccionesData($saccionesFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'servicio_id' => $fake->randomDigitNotNull,
            'descripcion' => $fake->text,
            'user_id' => $fake->randomDigitNotNull,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s'),
            'deleted_at' => $fake->date('Y-m-d H:i:s')
        ], $saccionesFields);
    }
}
