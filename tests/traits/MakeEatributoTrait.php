<?php

use Faker\Factory as Faker;
use App\Models\Eatributo;
use App\Repositories\EatributoRepository;

trait MakeEatributoTrait
{
    /**
     * Create fake instance of Eatributo and save it in database
     *
     * @param array $eatributoFields
     * @return Eatributo
     */
    public function makeEatributo($eatributoFields = [])
    {
        /** @var EatributoRepository $eatributoRepo */
        $eatributoRepo = App::make(EatributoRepository::class);
        $theme = $this->fakeEatributoData($eatributoFields);
        return $eatributoRepo->create($theme);
    }

    /**
     * Get fake instance of Eatributo
     *
     * @param array $eatributoFields
     * @return Eatributo
     */
    public function fakeEatributo($eatributoFields = [])
    {
        return new Eatributo($this->fakeEatributoData($eatributoFields));
    }

    /**
     * Get fake data of Eatributo
     *
     * @param array $postFields
     * @return array
     */
    public function fakeEatributoData($eatributoFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'nombre' => $fake->word,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s'),
            'deleted_at' => $fake->date('Y-m-d H:i:s')
        ], $eatributoFields);
    }
}
