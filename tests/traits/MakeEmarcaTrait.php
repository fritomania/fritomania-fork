<?php

use Faker\Factory as Faker;
use App\Models\Emarca;
use App\Repositories\EmarcaRepository;

trait MakeEmarcaTrait
{
    /**
     * Create fake instance of Emarca and save it in database
     *
     * @param array $emarcaFields
     * @return Emarca
     */
    public function makeEmarca($emarcaFields = [])
    {
        /** @var EmarcaRepository $emarcaRepo */
        $emarcaRepo = App::make(EmarcaRepository::class);
        $theme = $this->fakeEmarcaData($emarcaFields);
        return $emarcaRepo->create($theme);
    }

    /**
     * Get fake instance of Emarca
     *
     * @param array $emarcaFields
     * @return Emarca
     */
    public function fakeEmarca($emarcaFields = [])
    {
        return new Emarca($this->fakeEmarcaData($emarcaFields));
    }

    /**
     * Get fake data of Emarca
     *
     * @param array $postFields
     * @return array
     */
    public function fakeEmarcaData($emarcaFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'nombre' => $fake->word,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s'),
            'deleted_at' => $fake->date('Y-m-d H:i:s')
        ], $emarcaFields);
    }
}
