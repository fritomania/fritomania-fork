<?php

use Faker\Factory as Faker;
use App\Models\Magnitude;
use App\Repositories\MagnitudeRepository;

trait MakeMagnitudeTrait
{
    /**
     * Create fake instance of Magnitude and save it in database
     *
     * @param array $magnitudeFields
     * @return Magnitude
     */
    public function makeMagnitude($magnitudeFields = [])
    {
        /** @var MagnitudeRepository $magnitudeRepo */
        $magnitudeRepo = App::make(MagnitudeRepository::class);
        $theme = $this->fakeMagnitudeData($magnitudeFields);
        return $magnitudeRepo->create($theme);
    }

    /**
     * Get fake instance of Magnitude
     *
     * @param array $magnitudeFields
     * @return Magnitude
     */
    public function fakeMagnitude($magnitudeFields = [])
    {
        return new Magnitude($this->fakeMagnitudeData($magnitudeFields));
    }

    /**
     * Get fake data of Magnitude
     *
     * @param array $postFields
     * @return array
     */
    public function fakeMagnitudeData($magnitudeFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'nombre' => $fake->word,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s'),
            'deleted_at' => $fake->date('Y-m-d H:i:s')
        ], $magnitudeFields);
    }
}
