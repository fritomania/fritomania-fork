<?php

use Faker\Factory as Faker;
use App\Models\Equipo;
use App\Repositories\EquipoRepository;

trait MakeEquipoTrait
{
    /**
     * Create fake instance of Equipo and save it in database
     *
     * @param array $equipoFields
     * @return Equipo
     */
    public function makeEquipo($equipoFields = [])
    {
        /** @var EquipoRepository $equipoRepo */
        $equipoRepo = App::make(EquipoRepository::class);
        $theme = $this->fakeEquipoData($equipoFields);
        return $equipoRepo->create($theme);
    }

    /**
     * Get fake instance of Equipo
     *
     * @param array $equipoFields
     * @return Equipo
     */
    public function fakeEquipo($equipoFields = [])
    {
        return new Equipo($this->fakeEquipoData($equipoFields));
    }

    /**
     * Get fake data of Equipo
     *
     * @param array $postFields
     * @return array
     */
    public function fakeEquipoData($equipoFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'etipo_id' => $fake->randomDigitNotNull,
            'emarca_id' => $fake->randomDigitNotNull,
            'modelo_id' => $fake->randomDigitNotNull,
            'numero_serie' => $fake->word,
            'imei' => $fake->word,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s'),
            'deleted_at' => $fake->date('Y-m-d H:i:s')
        ], $equipoFields);
    }
}
