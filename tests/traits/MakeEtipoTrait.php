<?php

use Faker\Factory as Faker;
use App\Models\Etipo;
use App\Repositories\EtipoRepository;

trait MakeEtipoTrait
{
    /**
     * Create fake instance of Etipo and save it in database
     *
     * @param array $etipoFields
     * @return Etipo
     */
    public function makeEtipo($etipoFields = [])
    {
        /** @var EtipoRepository $etipoRepo */
        $etipoRepo = App::make(EtipoRepository::class);
        $theme = $this->fakeEtipoData($etipoFields);
        return $etipoRepo->create($theme);
    }

    /**
     * Get fake instance of Etipo
     *
     * @param array $etipoFields
     * @return Etipo
     */
    public function fakeEtipo($etipoFields = [])
    {
        return new Etipo($this->fakeEtipoData($etipoFields));
    }

    /**
     * Get fake data of Etipo
     *
     * @param array $postFields
     * @return array
     */
    public function fakeEtipoData($etipoFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'nombre' => $fake->word,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s'),
            'deleted_at' => $fake->date('Y-m-d H:i:s')
        ], $etipoFields);
    }
}
