<?php

use Faker\Factory as Faker;
use App\Models\Stipo;
use App\Repositories\StipoRepository;

trait MakeStipoTrait
{
    /**
     * Create fake instance of Stipo and save it in database
     *
     * @param array $stipoFields
     * @return Stipo
     */
    public function makeStipo($stipoFields = [])
    {
        /** @var StipoRepository $stipoRepo */
        $stipoRepo = App::make(StipoRepository::class);
        $theme = $this->fakeStipoData($stipoFields);
        return $stipoRepo->create($theme);
    }

    /**
     * Get fake instance of Stipo
     *
     * @param array $stipoFields
     * @return Stipo
     */
    public function fakeStipo($stipoFields = [])
    {
        return new Stipo($this->fakeStipoData($stipoFields));
    }

    /**
     * Get fake data of Stipo
     *
     * @param array $postFields
     * @return array
     */
    public function fakeStipoData($stipoFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'nombre' => $fake->word,
            'descripcion' => $fake->text,
            'precio' => $fake->word,
            'dias' => $fake->word,
            'prioridad' => $fake->word,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s'),
            'deleted_at' => $fake->date('Y-m-d H:i:s')
        ], $stipoFields);
    }
}
