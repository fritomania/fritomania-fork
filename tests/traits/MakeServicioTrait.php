<?php

use Faker\Factory as Faker;
use App\Models\Servicio;
use App\Repositories\ServicioRepository;

trait MakeServicioTrait
{
    /**
     * Create fake instance of Servicio and save it in database
     *
     * @param array $servicioFields
     * @return Servicio
     */
    public function makeServicio($servicioFields = [])
    {
        /** @var ServicioRepository $servicioRepo */
        $servicioRepo = App::make(ServicioRepository::class);
        $theme = $this->fakeServicioData($servicioFields);
        return $servicioRepo->create($theme);
    }

    /**
     * Get fake instance of Servicio
     *
     * @param array $servicioFields
     * @return Servicio
     */
    public function fakeServicio($servicioFields = [])
    {
        return new Servicio($this->fakeServicioData($servicioFields));
    }

    /**
     * Get fake data of Servicio
     *
     * @param array $postFields
     * @return array
     */
    public function fakeServicioData($servicioFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'stipo_id' => $fake->randomDigitNotNull,
            'equipo_id' => $fake->randomDigitNotNull,
            'cliente_id' => $fake->randomDigitNotNull,
            'sestado_id' => $fake->randomDigitNotNull,
            'usuario_recibe' => $fake->randomDigitNotNull,
            'numero' => $fake->randomDigitNotNull,
            'observaciones' => $fake->word,
            'diagnostico' => $fake->text,
            'costo_cotizacion' => $fake->word,
            'costo_reparacion' => $fake->word,
            'fecha_estimada' => $fake->date('Y-m-d H:i:s'),
            'usuario_asigna' => $fake->randomDigitNotNull,
            'fecha_asigna' => $fake->date('Y-m-d H:i:s'),
            'fecha_inicio' => $fake->date('Y-m-d H:i:s'),
            'fecha_fin' => $fake->date('Y-m-d H:i:s'),
            'reparacion' => $fake->text,
            'usuario_entrega' => $fake->randomDigitNotNull,
            'fecha_entrega' => $fake->date('Y-m-d H:i:s'),
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s'),
            'deleted_at' => $fake->date('Y-m-d H:i:s')
        ], $servicioFields);
    }
}
