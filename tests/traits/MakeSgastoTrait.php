<?php

use Faker\Factory as Faker;
use App\Models\Sgasto;
use App\Repositories\SgastoRepository;

trait MakeSgastoTrait
{
    /**
     * Create fake instance of Sgasto and save it in database
     *
     * @param array $sgastoFields
     * @return Sgasto
     */
    public function makeSgasto($sgastoFields = [])
    {
        /** @var SgastoRepository $sgastoRepo */
        $sgastoRepo = App::make(SgastoRepository::class);
        $theme = $this->fakeSgastoData($sgastoFields);
        return $sgastoRepo->create($theme);
    }

    /**
     * Get fake instance of Sgasto
     *
     * @param array $sgastoFields
     * @return Sgasto
     */
    public function fakeSgasto($sgastoFields = [])
    {
        return new Sgasto($this->fakeSgastoData($sgastoFields));
    }

    /**
     * Get fake data of Sgasto
     *
     * @param array $postFields
     * @return array
     */
    public function fakeSgastoData($sgastoFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'servicio_id' => $fake->randomDigitNotNull,
            'descripcion' => $fake->word,
            'cantidad' => $fake->word,
            'precio' => $fake->word,
            'user_id' => $fake->randomDigitNotNull,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s'),
            'deleted_at' => $fake->date('Y-m-d H:i:s')
        ], $sgastoFields);
    }
}
