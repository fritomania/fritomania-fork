<?php

use Faker\Factory as Faker;
use App\Models\Gasto;
use App\Repositories\GastoRepository;

trait MakeGastoTrait
{
    /**
     * Create fake instance of Gasto and save it in database
     *
     * @param array $gastoFields
     * @return Gasto
     */
    public function makeGasto($gastoFields = [])
    {
        /** @var GastoRepository $gastoRepo */
        $gastoRepo = App::make(GastoRepository::class);
        $theme = $this->fakeGastoData($gastoFields);
        return $gastoRepo->create($theme);
    }

    /**
     * Get fake instance of Gasto
     *
     * @param array $gastoFields
     * @return Gasto
     */
    public function fakeGasto($gastoFields = [])
    {
        return new Gasto($this->fakeGastoData($gastoFields));
    }

    /**
     * Get fake data of Gasto
     *
     * @param array $postFields
     * @return array
     */
    public function fakeGastoData($gastoFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'descripcion' => $fake->word,
            'monto' => $fake->word,
            'user_id' => $fake->randomDigitNotNull,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s'),
            'deleted_at' => $fake->date('Y-m-d H:i:s')
        ], $gastoFields);
    }
}
