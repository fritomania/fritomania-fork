<?php

use Faker\Factory as Faker;
use App\Models\Sestado;
use App\Repositories\SestadoRepository;

trait MakeSestadoTrait
{
    /**
     * Create fake instance of Sestado and save it in database
     *
     * @param array $sestadoFields
     * @return Sestado
     */
    public function makeSestado($sestadoFields = [])
    {
        /** @var SestadoRepository $sestadoRepo */
        $sestadoRepo = App::make(SestadoRepository::class);
        $theme = $this->fakeSestadoData($sestadoFields);
        return $sestadoRepo->create($theme);
    }

    /**
     * Get fake instance of Sestado
     *
     * @param array $sestadoFields
     * @return Sestado
     */
    public function fakeSestado($sestadoFields = [])
    {
        return new Sestado($this->fakeSestadoData($sestadoFields));
    }

    /**
     * Get fake data of Sestado
     *
     * @param array $postFields
     * @return array
     */
    public function fakeSestadoData($sestadoFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'nombre' => $fake->word,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s'),
            'deleted_at' => $fake->date('Y-m-d H:i:s')
        ], $sestadoFields);
    }
}
