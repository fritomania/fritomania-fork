<?php

use Faker\Factory as Faker;
use App\Models\Caja;
use App\Repositories\CajaRepository;

trait MakeCajaTrait
{
    /**
     * Create fake instance of Caja and save it in database
     *
     * @param array $cajaFields
     * @return Caja
     */
    public function makeCaja($cajaFields = [])
    {
        /** @var CajaRepository $cajaRepo */
        $cajaRepo = App::make(CajaRepository::class);
        $theme = $this->fakeCajaData($cajaFields);
        return $cajaRepo->create($theme);
    }

    /**
     * Get fake instance of Caja
     *
     * @param array $cajaFields
     * @return Caja
     */
    public function fakeCaja($cajaFields = [])
    {
        return new Caja($this->fakeCajaData($cajaFields));
    }

    /**
     * Get fake data of Caja
     *
     * @param array $postFields
     * @return array
     */
    public function fakeCajaData($cajaFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'monto_apertura' => $fake->word,
            'fecha_cierre' => $fake->date('Y-m-d H:i:s'),
            'monto_cierre' => $fake->word,
            'user_id' => $fake->randomDigitNotNull,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s'),
            'deleted_at' => $fake->date('Y-m-d H:i:s')
        ], $cajaFields);
    }
}
