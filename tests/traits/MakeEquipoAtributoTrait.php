<?php

use Faker\Factory as Faker;
use App\Models\EquipoAtributo;
use App\Repositories\EquipoAtributoRepository;

trait MakeEquipoAtributoTrait
{
    /**
     * Create fake instance of EquipoAtributo and save it in database
     *
     * @param array $equipoAtributoFields
     * @return EquipoAtributo
     */
    public function makeEquipoAtributo($equipoAtributoFields = [])
    {
        /** @var EquipoAtributoRepository $equipoAtributoRepo */
        $equipoAtributoRepo = App::make(EquipoAtributoRepository::class);
        $theme = $this->fakeEquipoAtributoData($equipoAtributoFields);
        return $equipoAtributoRepo->create($theme);
    }

    /**
     * Get fake instance of EquipoAtributo
     *
     * @param array $equipoAtributoFields
     * @return EquipoAtributo
     */
    public function fakeEquipoAtributo($equipoAtributoFields = [])
    {
        return new EquipoAtributo($this->fakeEquipoAtributoData($equipoAtributoFields));
    }

    /**
     * Get fake data of EquipoAtributo
     *
     * @param array $postFields
     * @return array
     */
    public function fakeEquipoAtributoData($equipoAtributoFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'equipo_id' => $fake->randomDigitNotNull,
            'eatributo_id' => $fake->randomDigitNotNull,
            'valor' => $fake->word,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s'),
            'deleted_at' => $fake->date('Y-m-d H:i:s')
        ], $equipoAtributoFields);
    }
}
