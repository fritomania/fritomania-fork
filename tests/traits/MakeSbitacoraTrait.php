<?php

use Faker\Factory as Faker;
use App\Models\Sbitacora;
use App\Repositories\SbitacoraRepository;

trait MakeSbitacoraTrait
{
    /**
     * Create fake instance of Sbitacora and save it in database
     *
     * @param array $sbitacoraFields
     * @return Sbitacora
     */
    public function makeSbitacora($sbitacoraFields = [])
    {
        /** @var SbitacoraRepository $sbitacoraRepo */
        $sbitacoraRepo = App::make(SbitacoraRepository::class);
        $theme = $this->fakeSbitacoraData($sbitacoraFields);
        return $sbitacoraRepo->create($theme);
    }

    /**
     * Get fake instance of Sbitacora
     *
     * @param array $sbitacoraFields
     * @return Sbitacora
     */
    public function fakeSbitacora($sbitacoraFields = [])
    {
        return new Sbitacora($this->fakeSbitacoraData($sbitacoraFields));
    }

    /**
     * Get fake data of Sbitacora
     *
     * @param array $postFields
     * @return array
     */
    public function fakeSbitacoraData($sbitacoraFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'servicio_id' => $fake->randomDigitNotNull,
            'sestado_id' => $fake->randomDigitNotNull,
            'titulo' => $fake->word,
            'descripcion' => $fake->text,
            'user_id' => $fake->randomDigitNotNull,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s'),
            'deleted_at' => $fake->date('Y-m-d H:i:s')
        ], $sbitacoraFields);
    }
}
