<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class EquipoAtributoApiTest extends TestCase
{
    use MakeEquipoAtributoTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateEquipoAtributo()
    {
        $equipoAtributo = $this->fakeEquipoAtributoData();
        $this->json('POST', '/api/v1/equipoAtributos', $equipoAtributo);

        $this->assertApiResponse($equipoAtributo);
    }

    /**
     * @test
     */
    public function testReadEquipoAtributo()
    {
        $equipoAtributo = $this->makeEquipoAtributo();
        $this->json('GET', '/api/v1/equipoAtributos/'.$equipoAtributo->id);

        $this->assertApiResponse($equipoAtributo->toArray());
    }

    /**
     * @test
     */
    public function testUpdateEquipoAtributo()
    {
        $equipoAtributo = $this->makeEquipoAtributo();
        $editedEquipoAtributo = $this->fakeEquipoAtributoData();

        $this->json('PUT', '/api/v1/equipoAtributos/'.$equipoAtributo->id, $editedEquipoAtributo);

        $this->assertApiResponse($editedEquipoAtributo);
    }

    /**
     * @test
     */
    public function testDeleteEquipoAtributo()
    {
        $equipoAtributo = $this->makeEquipoAtributo();
        $this->json('DELETE', '/api/v1/equipoAtributos/'.$equipoAtributo->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/equipoAtributos/'.$equipoAtributo->id);

        $this->assertResponseStatus(404);
    }
}
