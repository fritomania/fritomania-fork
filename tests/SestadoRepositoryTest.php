<?php

use App\Models\Sestado;
use App\Repositories\SestadoRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class SestadoRepositoryTest extends TestCase
{
    use MakeSestadoTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var SestadoRepository
     */
    protected $sestadoRepo;

    public function setUp()
    {
        parent::setUp();
        $this->sestadoRepo = App::make(SestadoRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateSestado()
    {
        $sestado = $this->fakeSestadoData();
        $createdSestado = $this->sestadoRepo->create($sestado);
        $createdSestado = $createdSestado->toArray();
        $this->assertArrayHasKey('id', $createdSestado);
        $this->assertNotNull($createdSestado['id'], 'Created Sestado must have id specified');
        $this->assertNotNull(Sestado::find($createdSestado['id']), 'Sestado with given id must be in DB');
        $this->assertModelData($sestado, $createdSestado);
    }

    /**
     * @test read
     */
    public function testReadSestado()
    {
        $sestado = $this->makeSestado();
        $dbSestado = $this->sestadoRepo->find($sestado->id);
        $dbSestado = $dbSestado->toArray();
        $this->assertModelData($sestado->toArray(), $dbSestado);
    }

    /**
     * @test update
     */
    public function testUpdateSestado()
    {
        $sestado = $this->makeSestado();
        $fakeSestado = $this->fakeSestadoData();
        $updatedSestado = $this->sestadoRepo->update($fakeSestado, $sestado->id);
        $this->assertModelData($fakeSestado, $updatedSestado->toArray());
        $dbSestado = $this->sestadoRepo->find($sestado->id);
        $this->assertModelData($fakeSestado, $dbSestado->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteSestado()
    {
        $sestado = $this->makeSestado();
        $resp = $this->sestadoRepo->delete($sestado->id);
        $this->assertTrue($resp);
        $this->assertNull(Sestado::find($sestado->id), 'Sestado should not exist in DB');
    }
}
