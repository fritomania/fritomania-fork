<?php

use App\Models\Gasto;
use App\Repositories\GastoRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class GastoRepositoryTest extends TestCase
{
    use MakeGastoTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var GastoRepository
     */
    protected $gastoRepo;

    public function setUp()
    {
        parent::setUp();
        $this->gastoRepo = App::make(GastoRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateGasto()
    {
        $gasto = $this->fakeGastoData();
        $createdGasto = $this->gastoRepo->create($gasto);
        $createdGasto = $createdGasto->toArray();
        $this->assertArrayHasKey('id', $createdGasto);
        $this->assertNotNull($createdGasto['id'], 'Created Gasto must have id specified');
        $this->assertNotNull(Gasto::find($createdGasto['id']), 'Gasto with given id must be in DB');
        $this->assertModelData($gasto, $createdGasto);
    }

    /**
     * @test read
     */
    public function testReadGasto()
    {
        $gasto = $this->makeGasto();
        $dbGasto = $this->gastoRepo->find($gasto->id);
        $dbGasto = $dbGasto->toArray();
        $this->assertModelData($gasto->toArray(), $dbGasto);
    }

    /**
     * @test update
     */
    public function testUpdateGasto()
    {
        $gasto = $this->makeGasto();
        $fakeGasto = $this->fakeGastoData();
        $updatedGasto = $this->gastoRepo->update($fakeGasto, $gasto->id);
        $this->assertModelData($fakeGasto, $updatedGasto->toArray());
        $dbGasto = $this->gastoRepo->find($gasto->id);
        $this->assertModelData($fakeGasto, $dbGasto->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteGasto()
    {
        $gasto = $this->makeGasto();
        $resp = $this->gastoRepo->delete($gasto->id);
        $this->assertTrue($resp);
        $this->assertNull(Gasto::find($gasto->id), 'Gasto should not exist in DB');
    }
}
