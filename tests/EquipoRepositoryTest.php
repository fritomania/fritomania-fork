<?php

use App\Models\Equipo;
use App\Repositories\EquipoRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class EquipoRepositoryTest extends TestCase
{
    use MakeEquipoTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var EquipoRepository
     */
    protected $equipoRepo;

    public function setUp()
    {
        parent::setUp();
        $this->equipoRepo = App::make(EquipoRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateEquipo()
    {
        $equipo = $this->fakeEquipoData();
        $createdEquipo = $this->equipoRepo->create($equipo);
        $createdEquipo = $createdEquipo->toArray();
        $this->assertArrayHasKey('id', $createdEquipo);
        $this->assertNotNull($createdEquipo['id'], 'Created Equipo must have id specified');
        $this->assertNotNull(Equipo::find($createdEquipo['id']), 'Equipo with given id must be in DB');
        $this->assertModelData($equipo, $createdEquipo);
    }

    /**
     * @test read
     */
    public function testReadEquipo()
    {
        $equipo = $this->makeEquipo();
        $dbEquipo = $this->equipoRepo->find($equipo->id);
        $dbEquipo = $dbEquipo->toArray();
        $this->assertModelData($equipo->toArray(), $dbEquipo);
    }

    /**
     * @test update
     */
    public function testUpdateEquipo()
    {
        $equipo = $this->makeEquipo();
        $fakeEquipo = $this->fakeEquipoData();
        $updatedEquipo = $this->equipoRepo->update($fakeEquipo, $equipo->id);
        $this->assertModelData($fakeEquipo, $updatedEquipo->toArray());
        $dbEquipo = $this->equipoRepo->find($equipo->id);
        $this->assertModelData($fakeEquipo, $dbEquipo->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteEquipo()
    {
        $equipo = $this->makeEquipo();
        $resp = $this->equipoRepo->delete($equipo->id);
        $this->assertTrue($resp);
        $this->assertNull(Equipo::find($equipo->id), 'Equipo should not exist in DB');
    }
}
