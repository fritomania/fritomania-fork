<?php

use App\Models\Eatributo;
use App\Repositories\EatributoRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class EatributoRepositoryTest extends TestCase
{
    use MakeEatributoTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var EatributoRepository
     */
    protected $eatributoRepo;

    public function setUp()
    {
        parent::setUp();
        $this->eatributoRepo = App::make(EatributoRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateEatributo()
    {
        $eatributo = $this->fakeEatributoData();
        $createdEatributo = $this->eatributoRepo->create($eatributo);
        $createdEatributo = $createdEatributo->toArray();
        $this->assertArrayHasKey('id', $createdEatributo);
        $this->assertNotNull($createdEatributo['id'], 'Created Eatributo must have id specified');
        $this->assertNotNull(Eatributo::find($createdEatributo['id']), 'Eatributo with given id must be in DB');
        $this->assertModelData($eatributo, $createdEatributo);
    }

    /**
     * @test read
     */
    public function testReadEatributo()
    {
        $eatributo = $this->makeEatributo();
        $dbEatributo = $this->eatributoRepo->find($eatributo->id);
        $dbEatributo = $dbEatributo->toArray();
        $this->assertModelData($eatributo->toArray(), $dbEatributo);
    }

    /**
     * @test update
     */
    public function testUpdateEatributo()
    {
        $eatributo = $this->makeEatributo();
        $fakeEatributo = $this->fakeEatributoData();
        $updatedEatributo = $this->eatributoRepo->update($fakeEatributo, $eatributo->id);
        $this->assertModelData($fakeEatributo, $updatedEatributo->toArray());
        $dbEatributo = $this->eatributoRepo->find($eatributo->id);
        $this->assertModelData($fakeEatributo, $dbEatributo->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteEatributo()
    {
        $eatributo = $this->makeEatributo();
        $resp = $this->eatributoRepo->delete($eatributo->id);
        $this->assertTrue($resp);
        $this->assertNull(Eatributo::find($eatributo->id), 'Eatributo should not exist in DB');
    }
}
