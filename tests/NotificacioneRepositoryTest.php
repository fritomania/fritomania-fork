<?php

use App\Models\Notificacione;
use App\Repositories\NotificacioneRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class NotificacioneRepositoryTest extends TestCase
{
    use MakeNotificacioneTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var NotificacioneRepository
     */
    protected $notificacioneRepo;

    public function setUp()
    {
        parent::setUp();
        $this->notificacioneRepo = App::make(NotificacioneRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateNotificacione()
    {
        $notificacione = $this->fakeNotificacioneData();
        $createdNotificacione = $this->notificacioneRepo->create($notificacione);
        $createdNotificacione = $createdNotificacione->toArray();
        $this->assertArrayHasKey('id', $createdNotificacione);
        $this->assertNotNull($createdNotificacione['id'], 'Created Notificacione must have id specified');
        $this->assertNotNull(Notificacione::find($createdNotificacione['id']), 'Notificacione with given id must be in DB');
        $this->assertModelData($notificacione, $createdNotificacione);
    }

    /**
     * @test read
     */
    public function testReadNotificacione()
    {
        $notificacione = $this->makeNotificacione();
        $dbNotificacione = $this->notificacioneRepo->find($notificacione->id);
        $dbNotificacione = $dbNotificacione->toArray();
        $this->assertModelData($notificacione->toArray(), $dbNotificacione);
    }

    /**
     * @test update
     */
    public function testUpdateNotificacione()
    {
        $notificacione = $this->makeNotificacione();
        $fakeNotificacione = $this->fakeNotificacioneData();
        $updatedNotificacione = $this->notificacioneRepo->update($fakeNotificacione, $notificacione->id);
        $this->assertModelData($fakeNotificacione, $updatedNotificacione->toArray());
        $dbNotificacione = $this->notificacioneRepo->find($notificacione->id);
        $this->assertModelData($fakeNotificacione, $dbNotificacione->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteNotificacione()
    {
        $notificacione = $this->makeNotificacione();
        $resp = $this->notificacioneRepo->delete($notificacione->id);
        $this->assertTrue($resp);
        $this->assertNull(Notificacione::find($notificacione->id), 'Notificacione should not exist in DB');
    }
}
