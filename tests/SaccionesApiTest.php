<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class SaccionesApiTest extends TestCase
{
    use MakeSaccionesTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateSacciones()
    {
        $sacciones = $this->fakeSaccionesData();
        $this->json('POST', '/api/v1/sacciones', $sacciones);

        $this->assertApiResponse($sacciones);
    }

    /**
     * @test
     */
    public function testReadSacciones()
    {
        $sacciones = $this->makeSacciones();
        $this->json('GET', '/api/v1/sacciones/'.$sacciones->id);

        $this->assertApiResponse($sacciones->toArray());
    }

    /**
     * @test
     */
    public function testUpdateSacciones()
    {
        $sacciones = $this->makeSacciones();
        $editedSacciones = $this->fakeSaccionesData();

        $this->json('PUT', '/api/v1/sacciones/'.$sacciones->id, $editedSacciones);

        $this->assertApiResponse($editedSacciones);
    }

    /**
     * @test
     */
    public function testDeleteSacciones()
    {
        $sacciones = $this->makeSacciones();
        $this->json('DELETE', '/api/v1/sacciones/'.$sacciones->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/sacciones/'.$sacciones->id);

        $this->assertResponseStatus(404);
    }
}
