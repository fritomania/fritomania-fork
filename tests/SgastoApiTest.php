<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class SgastoApiTest extends TestCase
{
    use MakeSgastoTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateSgasto()
    {
        $sgasto = $this->fakeSgastoData();
        $this->json('POST', '/api/v1/sgastos', $sgasto);

        $this->assertApiResponse($sgasto);
    }

    /**
     * @test
     */
    public function testReadSgasto()
    {
        $sgasto = $this->makeSgasto();
        $this->json('GET', '/api/v1/sgastos/'.$sgasto->id);

        $this->assertApiResponse($sgasto->toArray());
    }

    /**
     * @test
     */
    public function testUpdateSgasto()
    {
        $sgasto = $this->makeSgasto();
        $editedSgasto = $this->fakeSgastoData();

        $this->json('PUT', '/api/v1/sgastos/'.$sgasto->id, $editedSgasto);

        $this->assertApiResponse($editedSgasto);
    }

    /**
     * @test
     */
    public function testDeleteSgasto()
    {
        $sgasto = $this->makeSgasto();
        $this->json('DELETE', '/api/v1/sgastos/'.$sgasto->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/sgastos/'.$sgasto->id);

        $this->assertResponseStatus(404);
    }
}
