<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class NotificacioneApiTest extends TestCase
{
    use MakeNotificacioneTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateNotificacione()
    {
        $notificacione = $this->fakeNotificacioneData();
        $this->json('POST', '/api/v1/notificaciones', $notificacione);

        $this->assertApiResponse($notificacione);
    }

    /**
     * @test
     */
    public function testReadNotificacione()
    {
        $notificacione = $this->makeNotificacione();
        $this->json('GET', '/api/v1/notificaciones/'.$notificacione->id);

        $this->assertApiResponse($notificacione->toArray());
    }

    /**
     * @test
     */
    public function testUpdateNotificacione()
    {
        $notificacione = $this->makeNotificacione();
        $editedNotificacione = $this->fakeNotificacioneData();

        $this->json('PUT', '/api/v1/notificaciones/'.$notificacione->id, $editedNotificacione);

        $this->assertApiResponse($editedNotificacione);
    }

    /**
     * @test
     */
    public function testDeleteNotificacione()
    {
        $notificacione = $this->makeNotificacione();
        $this->json('DELETE', '/api/v1/notificaciones/'.$notificacione->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/notificaciones/'.$notificacione->id);

        $this->assertResponseStatus(404);
    }
}
