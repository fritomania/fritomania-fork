<?php

use App\Models\Correlativo;
use App\Repositories\CorrelativoRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class CorrelativoRepositoryTest extends TestCase
{
    use MakeCorrelativoTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var CorrelativoRepository
     */
    protected $correlativoRepo;

    public function setUp()
    {
        parent::setUp();
        $this->correlativoRepo = App::make(CorrelativoRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateCorrelativo()
    {
        $correlativo = $this->fakeCorrelativoData();
        $createdCorrelativo = $this->correlativoRepo->create($correlativo);
        $createdCorrelativo = $createdCorrelativo->toArray();
        $this->assertArrayHasKey('id', $createdCorrelativo);
        $this->assertNotNull($createdCorrelativo['id'], 'Created Correlativo must have id specified');
        $this->assertNotNull(Correlativo::find($createdCorrelativo['id']), 'Correlativo with given id must be in DB');
        $this->assertModelData($correlativo, $createdCorrelativo);
    }

    /**
     * @test read
     */
    public function testReadCorrelativo()
    {
        $correlativo = $this->makeCorrelativo();
        $dbCorrelativo = $this->correlativoRepo->find($correlativo->id);
        $dbCorrelativo = $dbCorrelativo->toArray();
        $this->assertModelData($correlativo->toArray(), $dbCorrelativo);
    }

    /**
     * @test update
     */
    public function testUpdateCorrelativo()
    {
        $correlativo = $this->makeCorrelativo();
        $fakeCorrelativo = $this->fakeCorrelativoData();
        $updatedCorrelativo = $this->correlativoRepo->update($fakeCorrelativo, $correlativo->id);
        $this->assertModelData($fakeCorrelativo, $updatedCorrelativo->toArray());
        $dbCorrelativo = $this->correlativoRepo->find($correlativo->id);
        $this->assertModelData($fakeCorrelativo, $dbCorrelativo->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteCorrelativo()
    {
        $correlativo = $this->makeCorrelativo();
        $resp = $this->correlativoRepo->delete($correlativo->id);
        $this->assertTrue($resp);
        $this->assertNull(Correlativo::find($correlativo->id), 'Correlativo should not exist in DB');
    }
}
