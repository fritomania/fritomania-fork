<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class CorrelativoApiTest extends TestCase
{
    use MakeCorrelativoTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateCorrelativo()
    {
        $correlativo = $this->fakeCorrelativoData();
        $this->json('POST', '/api/v1/correlativos', $correlativo);

        $this->assertApiResponse($correlativo);
    }

    /**
     * @test
     */
    public function testReadCorrelativo()
    {
        $correlativo = $this->makeCorrelativo();
        $this->json('GET', '/api/v1/correlativos/'.$correlativo->id);

        $this->assertApiResponse($correlativo->toArray());
    }

    /**
     * @test
     */
    public function testUpdateCorrelativo()
    {
        $correlativo = $this->makeCorrelativo();
        $editedCorrelativo = $this->fakeCorrelativoData();

        $this->json('PUT', '/api/v1/correlativos/'.$correlativo->id, $editedCorrelativo);

        $this->assertApiResponse($editedCorrelativo);
    }

    /**
     * @test
     */
    public function testDeleteCorrelativo()
    {
        $correlativo = $this->makeCorrelativo();
        $this->json('DELETE', '/api/v1/correlativos/'.$correlativo->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/correlativos/'.$correlativo->id);

        $this->assertResponseStatus(404);
    }
}
