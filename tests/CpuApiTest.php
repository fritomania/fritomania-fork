<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class CpuApiTest extends TestCase
{
    use MakeCpuTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateCpu()
    {
        $cpu = $this->fakeCpuData();
        $this->json('POST', '/api/v1/cpus', $cpu);

        $this->assertApiResponse($cpu);
    }

    /**
     * @test
     */
    public function testReadCpu()
    {
        $cpu = $this->makeCpu();
        $this->json('GET', '/api/v1/cpus/'.$cpu->id);

        $this->assertApiResponse($cpu->toArray());
    }

    /**
     * @test
     */
    public function testUpdateCpu()
    {
        $cpu = $this->makeCpu();
        $editedCpu = $this->fakeCpuData();

        $this->json('PUT', '/api/v1/cpus/'.$cpu->id, $editedCpu);

        $this->assertApiResponse($editedCpu);
    }

    /**
     * @test
     */
    public function testDeleteCpu()
    {
        $cpu = $this->makeCpu();
        $this->json('DELETE', '/api/v1/cpus/'.$cpu->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/cpus/'.$cpu->id);

        $this->assertResponseStatus(404);
    }
}
